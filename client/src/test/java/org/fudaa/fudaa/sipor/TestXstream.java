package org.fudaa.fudaa.sipor;

import java.io.File;
import java.net.URL;

import junit.framework.TestCase;

import org.fudaa.dodico.corba.sipor.SParametresSiporGene;
import org.fudaa.fudaa.sipor.factory.SiporXstreamFactory;

public class TestXstream extends TestCase{

	
	public void testChargementXstream(){
		SiporXstreamFactory factory=new SiporXstreamFactory();
		URL fileUrl=SiporXstreamFactory.class.getResource("/etudeTest/etudeSIPOR.sipor.xml");
		System.out.println("fichier trouv�- "+fileUrl.getFile());
		//-- tentative ouverture du fichier --//
		try {
			SParametresSiporGene struct=factory.chargerProjet(new File(fileUrl.getFile()));
			assertNotNull(struct);
			//System.out.println("ok - def \n  "+factory.initParser().toXML(struct));

		} catch (Exception e) {
			e.printStackTrace();
			assertNotNull(null);
		}
	}
	
	
}
