package org.fudaa.fudaa.sipor.algorithmes;

import java.util.List;

import org.fudaa.fudaa.sipor.algorithmes.MareeDouziemes.CoupleDouzieme;

import junit.framework.TestCase;

public class MareeDouziemesTest extends TestCase{


	public void testDouzieme() {

		MareeDouziemes md = new MareeDouziemes();
		double[] tableauDouziemes = new double[]{1,2,3,3,2,1,1,2,3,3,2,1};
		float mareeBasse=2.25f;
		float mareeHaute=11.3f;
		float periodeMer=(11.16f - 5.53f)*2;
		 List<CoupleDouzieme> res = md.computeDouziemes(mareeBasse, mareeHaute, periodeMer, tableauDouziemes);

		 assertNotNull(res);
		 
		 System.out.println(res);
		 
	}

}
