package org.fudaa.fudaa.sipor.mock;

import java.io.File;
import java.io.IOException;

import org.fudaa.dodico.corba.sipor.SiporResultatListeevenementsSimu;

import org.fudaa.dodico.sipor.DResultatsSipor;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

import junit.framework.TestCase;

public class SiporMockTest extends TestCase{
	
	
	public void testLoadHis() throws IOException {
		SiporMock mock = new SiporMock();
		SiporDataSimulation data = new SiporDataSimulation(null);
		File f=File.createTempFile("sipor","data");
		mock.mockGeneral(data, f.getAbsolutePath());
		
		SiporResultatListeevenementsSimu res = DResultatsSipor.litResultatsSipor(f.getAbsolutePath() + ".his");
		assertNotNull(res);
		assertNotNull(res.listeEvenements);
		assertEquals(res.nombreNavires>100, true);
		assertEquals(res.listeEvenements.length>100, true);
	}
	
	
	
	

}
