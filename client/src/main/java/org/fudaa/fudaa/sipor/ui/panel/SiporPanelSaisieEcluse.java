package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.fu.FuLog;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporEcluse;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;
import org.fudaa.fudaa.sipor.ui.frame.MessageConstants;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieHorairesResume;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieLoiDeterministe;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieLoiJournaliere;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserEcluses;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextField;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldDuree;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldDureePM;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldFloat;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldInteger;

/**
 * Panel de saisie des principales ecluses:
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelSaisieEcluse extends JPanel {

  // loi de proba indispo******
  SiporFrameSaisieLoiJournaliere fenetreLoiJournaliere_ = null;
  SiporFrameSaisieLoiDeterministe fenetreLoideter_ = null;
  SiporTextFieldDuree dureeIndispo_ = new SiporTextFieldDuree(3);
  SiporTextFieldInteger frequenceMoyenne_ = new SiporTextFieldInteger(3);
  SiporTextFieldInteger frequenceMoyenne2_ = new SiporTextFieldInteger(3);
  String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  JComboBox loiProbaDuree_ = new JComboBox(tabloi_);
  JComboBox loiProbaFrequence_ = new JComboBox(tabloi_);
  String[] choixLoi_ = { "Erlang", "D�terministe" };
  JComboBox choixLoiFrequence = new JComboBox(choixLoi_);
  /** variable contenant le tableau des couples pour la loi deterministe */
  ArrayList loiDeterministe_ = new ArrayList();

  // ******fin loi duree indispo

  SiporTextField cNom_ = new SiporTextField(10);
  SiporTextFieldFloat cLongueur_ = new SiporTextFieldFloat(3);
  SiporTextFieldFloat cLargeur_ = new SiporTextFieldFloat(3);
  SiporTextFieldDuree ctempsEclusee_ = new SiporTextFieldDuree(3);
  SiporTextFieldDuree ctempsFausseBassinnee_ = new SiporTextFieldDuree(3);
  SiporTextField ccreneauetaleavtPMdeb_ = new SiporTextField(3);
  SiporTextField ccreneauetaleavtPMfin_ = new SiporTextField(3);
  SiporTextFieldDureePM ccreneauetaleapresPMdeb_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDureePM ccreneauetaleapresPMfin_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDuree cdureepassageEcluseEtale_ = new SiporTextFieldDuree(3);

  SiporTextField creneau1debut_ = ccreneauetaleavtPMdeb_, creneau1fin = ccreneauetaleavtPMfin_,
      creneau2debut = ccreneauetaleapresPMdeb_, creneau2fin = ccreneauetaleapresPMfin_;

  // bouttons de validation des donn�es

  final BuButton creneau_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "Cr�neau");

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  
  /**
   * Horaire qui sera saisie par l'interface de saisie des horaires
   */
  SiporHoraire horaire_ = new SiporHoraire();

  /**
   * Variable statique qui compte le nombre de fois que ce type d'objet a �t� ouvert
   */

  static int nbouverture = 0;

  /**
   * Donnees de la simulation:
   */
  SiporDataSimulation donnees_;

  /**
   * Panel de gestion des ecluses fenetre qui a tous les droits tres utile pour les modifciation , les mise a jour et
   * les supressions d'�cluses
   */
  SiporVisualiserEcluses MENUECLUSE_;

  /**
   * Mode dee la fenetre de sasie des donn�es si MODE=true alors le panek est en mode modification sinon c est en mode
   * ajout Par d�faut c est en mode ajout donc booleen initialis� a false.
   */
  public boolean UPDATE = false;

  /**
   * Indice de l ecluse a modifier dans le cas ou l on est en mode modification
   */
  int ECLUSE_A_MODIFIER_;
  
  double demiPeriodeMareeMinutes_;

  /**
   * Constructeur du panel de saisie des ecluses
   * 
   * @param d donnees de la simulation
   * @param ve panel qui permet de tout faire
   */

  public SiporPanelSaisieEcluse(final SiporDataSimulation d, final SiporVisualiserEcluses ve) {

    // parametre de trop mis de cot�:
    // this.cdureepassageEcluseEtale_.setEnabled(false);

    donnees_ = d;
    demiPeriodeMareeMinutes_ = .5 * SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(donnees_.getParams_().maree.periodeMaree);
    MENUECLUSE_ = ve;
    /**
     * Les controles des composants:
     */
    this.cNom_.setToolTipText("Saisissez le nom de l'�cluse ici: ");
    this.cNom_.setText("Ecluse " + (donnees_.getListeEcluse_().getListeEcluses_().size() + 1));
    loiProbaDuree_
        .setToolTipText("Loi de probabilit� d'Erlang, choisissez un chiffre entre 1 et 10 avec 10 �quivalent � la loi r�guli�re");
    loiProbaFrequence_
        .setToolTipText("Loi de probabilit� d'Erlang, choisissez un chiffre entre 1 et 10 avec 10 �quivalent � la loi r�guli�re");

    this.cLongueur_.setToolTipText("Saisissez la longueur en m�tre de l'�cluse");
    this.cLargeur_.setToolTipText("Saisissez la largeur en m�tre de l'�cluse");
    this.ctempsEclusee_.setToolTipText("Saisisez la dur�e de l'�clus�e ");
    this.ctempsFausseBassinnee_.setToolTipText("Saisisez la dur�e de la fausse bassin�e");
    creneau_.setToolTipText("Saisissez les horaires en cliquant sur ce bouton");

    this.ccreneauetaleavtPMdeb_
        .setToolTipText("Saisissez l'intervalle qui correspond au cr�neau d'�tale avant pleine Mer");
    this.ccreneauetaleavtPMfin_
        .setToolTipText("Saisissez l'intervalle qui correspond au cr�neau d'�tale avant pleine Mer");
    this.ccreneauetaleapresPMdeb_
        .setToolTipText("Saisissez l'intervalle qui correspond au cr�neau d'�tale apr�s pleine Mer");
    this.ccreneauetaleapresPMfin_
        .setToolTipText("Saisissez l'intervalle qui correspond au cr�neau d'�tale apr�s pleine Mer");
    this.cdureepassageEcluseEtale_
        .setToolTipText("Saisissez la dur�e correspondante au temps de passage de l'ecluse � l'�tale");

    
    this.creneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        
        // test de validit si on a rentr quelque chose
        if (!creneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1debut_.getText());

            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes_ || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes_) {

              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -" + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes_) + " et " + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes_) + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                MessageConstants.NOMBRE_N_EXISTE_PAS).activate();
            creneau1debut_.setText("");
          }
        }
      }
    });

    this.creneau1fin.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau1fin.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau1fin.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1fin.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  MessageConstants.HORAIRE_EST_NEGATIF).activate();
              creneau1fin.setText("");
            }
            
            else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(creneau1debut_.getText())) {
                donnees_.getApplication();
                new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                    "Cet horaire de fin est inf�rieur � l'horaire de d�part.").activate();
                creneau1fin.setText("");

              }
            }

          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                MessageConstants.NOMBRE_N_EXISTE_PAS).activate();
            creneau1fin.setText("");
          }
        }
      }
    });

    this.creneau2debut.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau2debut.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau2debut.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2debut.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  MessageConstants.HORAIRE_EST_NEGATIF).activate();
              creneau2debut.setText("");
            } else if (i >= 24) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  MessageConstants.COMPRIS_ENTRE_0_ET_24).activate();
              creneau2debut.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(creneau1debut_.getText()) && i < Float.parseFloat(creneau1fin.getText())) {

              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Cet horaire est inclus dans le premier cr�neau.\n Les cr�neaux sont des intervalles de temps distincts.")
                  .activate();
              creneau2debut.setText("");
            }

          } catch (final NumberFormatException nfe) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                MessageConstants.NOMBRE_N_EXISTE_PAS).activate();
            creneau2debut.setText("");
          }
        }
      }
    });

    this.creneau2fin.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau2fin.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau2fin.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2fin.getText());

            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes_ || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes_) {
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -" + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes_) + " et " + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes_) + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau2fin.setText("");
            }

          } catch (final NumberFormatException nfe) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                MessageConstants.NOMBRE_N_EXISTE_PAS).activate();
            creneau2fin.setText("");
          }
        }
      }
    });

    

   

    /**
     * Listener des boutons
     */
    this.creneau_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        
        donnees_.getApplication().addInternalFrame(new SiporFrameSaisieHorairesResume(horaire_));
      }
    });

    this.validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        
        creation_Ecluse();
      }
    });

    this.choixLoiFrequence.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        /**
         * ETAPE 1: determiner le type de loi selectionn�
         */
        final int choixLoi = choixLoiFrequence.getSelectedIndex();

        if (choixLoi == 0) {
          // Cas 0: loi d erlang
          frequenceMoyenne_.setEnabled(true);
          frequenceMoyenne2_.setEnabled(true);
          loiProbaFrequence_.setEnabled(true);

        } else if (choixLoi == 1) {
          // cas 1: loi deterministe
          frequenceMoyenne_.setEnabled(false);
          frequenceMoyenne2_.setEnabled(false);
          loiProbaFrequence_.setEnabled(false);
          

          if (fenetreLoideter_ == null) {
            FuLog.debug("interface nulle");

            fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

            // System.out.println("55555");
            fenetreLoideter_.setVisible(true);

            donnees_.getApplication().addInternalFrame(fenetreLoideter_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoideter_.isClosed()) {

              fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().addInternalFrame(fenetreLoideter_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");

              fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().activateInternalFrame(fenetreLoideter_);
              donnees_.getApplication().addInternalFrame(fenetreLoideter_);

            }
          }

        } else if (choixLoi == 2) {
          // cas 2: loi journaliere
          frequenceMoyenne_.setEnabled(false);
          frequenceMoyenne2_.setEnabled(false);
          loiProbaFrequence_.setEnabled(false);

          // lancement de la frame de saisie des creneaux de la loi journaliere
          // donnees_.getApplication().addInternalFrame(new
          // SiporFrameSaisieLoiJournaliere(donnees_,loiDeterministe_,dureeIndispo_));

          if (fenetreLoiJournaliere_ == null) {
            FuLog.debug("interface nulle");

            fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

            // System.out.println("55555");
            fenetreLoiJournaliere_.setVisible(true);
            donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoiJournaliere_.isClosed()) {

              fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");

              fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().activateInternalFrame(fenetreLoiJournaliere_);
              donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);

            }
          }

        }

      }

    });

    /**
     * Affichage des composants
     */
   // this.setLayout(new BorderLayout());
    JPanel contenu=new JPanel(new BorderLayout());
    this.setBorder(SiporBordures.ecluse);
    this.add(contenu);
    
    //panel total
    final Box total = Box.createVerticalBox();
    contenu.add(total,BorderLayout.CENTER);
    
    //general
    final JPanel se1 = new JPanel();
    se1.add(new JLabel("Nom de l'ecluse: "));
    se1.add(this.cNom_);
    se1.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_,"G�n�ral"));
    contenu.add(se1, BorderLayout.NORTH);

    
    //dimensions
    final JPanel dimensions=new JPanel(new GridLayout(1,2));
    dimensions.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_,"Dimensions"));
    total.add(dimensions);
    
    final JPanel se2 = new JPanel();
    se2.add(new JLabel("Longueur: "));
    se2.add(this.cLongueur_);
    se2.add(new JLabel("M�tres"));
    se2.setBorder(BorderFactory.createTitledBorder(SiporBordures.bordnormal_,"Longueur",0,0,null,SiporImplementation.bleuClairSipor));
    dimensions.add(se2);
    
    final JPanel se22 = new JPanel();
    se22.add(new JLabel(" Largeur: "));
    se22.add(this.cLargeur_);
    se22.add(new JLabel("M�tres"));
    se22.setBorder(BorderFactory.createTitledBorder(SiporBordures.bordnormal_,"Largeur",0,0,null,SiporImplementation.bleuClairSipor));
    dimensions.add(se22);

    
    //durees
    Box etaleEcluse=Box.createVerticalBox();
    etaleEcluse.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_,"Dur�es"));
    total.add(etaleEcluse);
    
    final JPanel se23 = new JPanel();
    se23.add(new JLabel("Dur�e de passage de l'�cluse � l'�tale: "));
    se23.add(this.cdureepassageEcluseEtale_);
    se23.add(new JLabel("Heures.Minutes"));
    se23.setBorder(SiporBordures.bordnormal_);
    etaleEcluse.add(se23);

    JPanel se331=new JPanel(new GridLayout(1,2));
    etaleEcluse.add(se331);
    
    final JPanel se3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    se3.add(new JLabel("Dur�e �clus�e:"));
    se3.add(this.ctempsEclusee_);
    se3.add(new JLabel("Heures.Minutes  "));
    se3.setBorder(SiporBordures.bordnormal_);
    se331.add(se3);
    final JPanel se31 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    se31.add(new JLabel("Dur�e fausse bassin�e: "));
    se31.add(this.ctempsFausseBassinnee_);
    se31.add(new JLabel("Heures.Minutes  "));
    se31.setBorder(SiporBordures.bordnormal_);
    se331.add(se31);

    final JPanel se4 = new JPanel();
    se4.add(new JLabel("Cr�neau �tale par rapport � l'heure de pleine mer: de"));
    se4.add(this.ccreneauetaleavtPMdeb_);
    se4.add(new JLabel("�"));
    se4.add(this.ccreneauetaleapresPMfin_);
    se4.add(new JLabel("heures.Minutes"));
    se4.setBorder(SiporBordures.bordnormal_);
    etaleEcluse.add(se4);

    
    //indispo+duree+creneau
    JPanel indispoCreneau=new JPanel(new GridLayout(1,2));
    total.add(indispoCreneau);
    
    //loi indisponibilit�s
   Box loiIndispo= Box.createVerticalBox();
   loiIndispo.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_,"Loi indisponibilit�"));
   indispoCreneau.add(loiIndispo);
    
   
    final JPanel p31 = new JPanel();
    p31.add(new JLabel("Type de loi: "));
    p31.add(choixLoiFrequence);
    p31.setBorder(SiporBordures.bordnormal_);
    loiIndispo.add(p31);

    final JPanel p32 = new JPanel();
    p32.add(new JLabel("Ecart moyen: "));
    p32.add(this.frequenceMoyenne_);
    p32.add(new JLabel("Jours"));
    p32.add(this.frequenceMoyenne2_);
    p32.add(new JLabel("Heures"));
    p32.setBorder(SiporBordures.bordnormal_);
    loiIndispo.add(p32);

    final JPanel p33 = new JPanel();
    p33.add(new JLabel("Ordre loi d'Erlang fr�quence: "));
    p33.add(this.loiProbaFrequence_);
    p33.setBorder(SiporBordures.bordnormal_);
    loiIndispo.add(p33);

    
    //durees indispo + creneaux
    Box dureeIndispoCrenaux=Box.createVerticalBox();
    indispoCreneau.add(dureeIndispoCrenaux);
    
    //durees indispo
    Box dureeIndispo=Box.createVerticalBox();
    dureeIndispo.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_,"Dur�e indisponibilit�"));
    dureeIndispoCrenaux.add(dureeIndispo);
    
    
    final JPanel p21 = new JPanel();
    p21.add(new JLabel("Dur�e moyenne:"));
    p21.add(this.dureeIndispo_);
    p21.add(new JLabel("Heures.Minutes "));
    p21.setBorder(SiporBordures.bordnormal_);
    dureeIndispo.add(p21);

    final JPanel p23 = new JPanel();
    p23.add(new JLabel("Ordre loi d'Erlang de la dur�e: "));
    p23.add(this.loiProbaDuree_);
    p23.setBorder(SiporBordures.bordnormal_);
    dureeIndispo.add(p23);
    
    
    //creneaux
    JPanel p24=new JPanel();
    p24.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_,"Cr�neaux horaires d'acc�s"));
    //p24.add(new JLabel("Creneaux d'ouverture: "));
    p24.add(this.creneau_);
    dureeIndispoCrenaux.add(p24);
    
    
    //validation
    final JPanel se6 = new JPanel();
    se6.setBorder(SiporBordures.compound_);
    se6.add(this.validation_);
    contenu.add(se6, BorderLayout.SOUTH);
    
    //add(total, BorderLayout.CENTER);
    
    
    
    setVisible(true);
  }

  /**
   * Methode de verification des coherence des donn�es
   * 
   * @return true si toutes les donn�es sont coh�rentes
   */
  boolean controle_creationEcluse() {

    if (this.cNom_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Nom manquant.")
          .activate();
      return false;
    } else if (this.UPDATE
        && donnees_.getListeEcluse_().existeDoublon(this.cNom_.getText(), this.ECLUSE_A_MODIFIER_)) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Nom d�j� pris.")
          .activate();
      return false;
    } else if (!this.UPDATE && donnees_.getListeEcluse_().existeDoublon(this.cNom_.getText(), -1)) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Nom d�j� pris.")
          .activate();
      return false;
    }

    if (this.cLongueur_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Longueur manquante.").activate();
      return false;
    }
    if (this.cLargeur_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Largeur manquante.").activate();
      return false;
    }

    if (this.cdureepassageEcluseEtale_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Dur�e de passage � l'�tale de \n l'�cluse manquante.").activate();
      return false;
    }
    else
    if (this.ctempsEclusee_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Dur�e de l'�clus�e manquante.").activate();
      return false;
    }
    else
    if (this.ctempsFausseBassinnee_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Dur�e de fausse bassin�e manquante.").activate();
      return false;
    }
    else{
      
      float passageEtale=Float.parseFloat(this.cdureepassageEcluseEtale_.getText());
      float eclusee=Float.parseFloat(this.ctempsEclusee_.getText());
      float fausseBassinnee=Float.parseFloat(this.ctempsFausseBassinnee_.getText());
      
      if(eclusee<passageEtale){
       new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
         "La dur�e d'�clus�e est inf�rieure au temps de  passage � l'�tale.").activate();
     cdureepassageEcluseEtale_.setText("");
      return false;
      }
      else
        if(eclusee<fausseBassinnee)
        {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "La dur�e d'�clus�e est inf�rieure � la duree de la fausse bassin�e.").activate();
          return false;
        }
    }
    if (this.ccreneauetaleavtPMdeb_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Horaire de d�but du cr�neau d'�tale\n avant pleine mer manquant.").activate();
      return false;
    }
    if (this.ccreneauetaleapresPMfin_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Horaire de fin du cr�neau d'�tale\n avant pleine mer manquant.").activate();
      return false;
    }

    // controle de la saisie des horaires:
    if (this.horaire_.semaineCreneau1HeureArrivee == -1 || this.horaire_.semaineCreneau1HeureDep == -1
        || this.horaire_.semaineCreneau2HeureArrivee == -1 || this.horaire_.semaineCreneau2HeureDep == -1) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Cr�neaux non saisis correctement.").activate();
      return false;

    }

    if (this.dureeIndispo_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Dur�e d'indisponibilit� manquante.").activate();
      return false;

    }

    /**
     * Cas loi de frequence= loi d erlang: il faut mettre une valeur moyenne
     */
    if (choixLoiFrequence.getSelectedIndex() == 0) {
      if (this.frequenceMoyenne_.getText().equals("") && this.frequenceMoyenne2_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
            "Fr�quence moyenne manquante.").activate();
        return false;

      }
    }

    // arriv� a ce stade de la methode tous les tests ont �chou�s donc c est du tout bon!
    return true;
  }

  /**
   * 
   */
  void creation_Ecluse() {
    if (controle_creationEcluse()) {
      // test de la bonne saisie des horaires:
      horaire_.affichage();


      SiporEcluse ecluse = null;
      
      if(!UPDATE)
    	  ecluse = new SiporEcluse();
      else
    	  ecluse = this.donnees_.getListeEcluse_().retournerEcluse(ECLUSE_A_MODIFIER_);
      ecluse.setNom_(this.cNom_.getText());
      ecluse.setLargeur_(Double.parseDouble(this.cLargeur_.getText()));
      ecluse.setLongueur_(Double.parseDouble(this.cLongueur_.getText()));
      ecluse.setTempsEclusee_(Double.parseDouble(this.ctempsEclusee_.getText()));
      ecluse.setTempsFausseBassinnee_(Double.parseDouble(this.ctempsFausseBassinnee_.getText()));
      ecluse.setDureePassageEtale_(Double.parseDouble(this.cdureepassageEcluseEtale_.getText()));
      ecluse.setCreneauEtaleApresPleineMerFin_(Double.parseDouble(this.ccreneauetaleapresPMfin_.getText()));
      ecluse.setCreneauEtaleAvantPleineMerDeb_(Double.parseDouble(this.ccreneauetaleavtPMdeb_.getText()));

      // loi indisponibilit�s
      ecluse.setDureeIndispo_(Float.parseFloat(this.dureeIndispo_.getText()));
      if (choixLoiFrequence.getSelectedIndex() == 0) {
        ecluse.setTypeLoi_(0);
        float moy=0;
        if(!this.frequenceMoyenne_.getText().equals(""))
          moy+=Float.parseFloat(this.frequenceMoyenne_.getText())*24;
        if(!this.frequenceMoyenne2_.getText().equals(""))
          moy+=Float.parseFloat(this.frequenceMoyenne2_.getText());
        ecluse.setFrequenceMoyenne_(moy);
        ecluse.setLoiFrequence_(Integer.parseInt((String) this.loiProbaFrequence_.getSelectedItem()));
      } else if (choixLoiFrequence.getSelectedIndex() == 1) {

        ecluse.setTypeLoi_(1);
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          ecluse.getLoiDeterministe_().add(c);

        }

      } else if (choixLoiFrequence.getSelectedIndex() == 2) {
        // cas loi journaliere
        ecluse.setTypeLoi_(2);
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          ecluse.getLoiDeterministe_().add(c);

        }

      }
      ecluse.setLoiIndispo_(Integer.parseInt((String) this.loiProbaDuree_.getSelectedItem()));

      ecluse.getH_().recopie(horaire_);
      
      
      

      if (!this.UPDATE) {
        /**
         * Mode ajout
         */
        donnees_.getListeEcluse_().ajout(ecluse);
        
        donnees_.getReglesRemplissageSAS().ajoutLigne(donnees_.getCategoriesNavires_().getListeNavires_().size());
        
      //-- add element in network --//
        donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
        		SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE,
        		ecluse);
        
        new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "L'�cluse "
            + this.cNom_.getText() + " a �t� ajout�e avec succ�s.").activate();
      } else {
        /**
         * Mode modification
         */
        // recuperation des informatiosn des gares amonts et avals
        ecluse.setGareAmont_(donnees_.getListeEcluse_().retournerEcluse(ECLUSE_A_MODIFIER_).getGareAmont_());
        ecluse.setGareAval_(donnees_.getListeEcluse_().retournerEcluse(ECLUSE_A_MODIFIER_).getGareAval_());

        donnees_.getListeEcluse_().modification(this.ECLUSE_A_MODIFIER_, ecluse);
        donnees_.getApplication();
        new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "L'�cluse "
            + this.cNom_.getText() + " a �t� modifi�e avec succ�s.").activate();
      }
    //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      donnees_.baisserNiveauSecurite();

      
      
      // 2)sauvegarde des donnees
      donnees_.enregistrer();

      // affichage du tableau modifi� magique!!
      this.MENUECLUSE_.pile_.first(this.MENUECLUSE_.principalPanel_);

      // changement du titre
      this.MENUECLUSE_.setTitle("Gestion des �cluses");

      // REMISE A ZERO DES COMPOSANTS...
      this.initialiser();

    }

  }

  /**
   * Methode qui d�termine le type de l'�cluse: mode modification implique le passage du booleen a true et de passer
   * l'indice du tableau d'ecluse a modifier:
   * 
   * @param numEcluse indice du tableau d ecluse a modifier
   */
  public void MODE_MODIFICATION(final int numEcluse) {

    // 1) passage en mode modification
    this.UPDATE = true;

    // 2) recuperation de l indice du quai
    this.ECLUSE_A_MODIFIER_ = numEcluse;

    // 3) recuperation de la structure de quai

    final SiporEcluse q = this.donnees_.getListeEcluse_().retournerEcluse(this.ECLUSE_A_MODIFIER_);

    // 4)remplissage des donnn�es relatives au quai:
    this.cNom_.setText(q.getNom_());
    this.cLongueur_.setText("" + (float) q.getLongueur_());
    this.cLargeur_.setText("" + (float) q.getLargeur_());
    this.ctempsEclusee_.setText("" + (float) q.getTempsEclusee_());
    this.ctempsFausseBassinnee_.setText("" + (float) q.getTempsFausseBassinnee_());
    this.ccreneauetaleapresPMfin_.setText("" + (float) q.getCreneauEtaleApresPleineMerFin_());
    this.ccreneauetaleavtPMdeb_.setText("" + (float) q.getCreneauEtaleAvantPleineMerDeb_());
    this.cdureepassageEcluseEtale_.setText("" + (float) q.getDureePassageEtale_());
    // this.cdureepassageEcluseEtale.setText(""+q.)
    this.horaire_.recopie(q.getH_());
    this.validation_.setText("Modifier");

    this.dureeIndispo_.setText("" + (float) q.getDureeIndispo_());
    this.loiProbaDuree_.setSelectedIndex(q.getLoiIndispo_() - 1);
    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.loiProbaFrequence_.setSelectedIndex(0);
    if (q.getTypeLoi_() == 0) {

      this.frequenceMoyenne_.setText("" + ((int) q.getFrequenceMoyenne_())/24);
      this.frequenceMoyenne2_.setText("" + ((int) q.getFrequenceMoyenne_())%24);
      this.loiProbaFrequence_.setSelectedIndex(q.getLoiFrequence_() - 1);
      this.choixLoiFrequence.setSelectedIndex(0);

    } else if (q.getTypeLoi_() == 1) {

      for (int i = 0; i < q.getLoiDeterministe_().size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.getLoiDeterministe_().get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(1);
    } else if (q.getTypeLoi_() == 2) {
      // cas loi journaliere

      for (int i = 0; i < q.getLoiDeterministe_().size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.getLoiDeterministe_().get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(2);

    }

    if (this.fenetreLoideter_ != null) {
      fenetreLoideter_.setVisible(false);
    }

    if (this.fenetreLoiJournaliere_ != null) {
      fenetreLoiJournaliere_.setVisible(false);
    }

  }

  /**
   * methode d'initialisation des champs de la frame
   */
  public void initialiser() {
    fenetreLoideter_ = null;
    this.horaire_ = new SiporHoraire();
    this.cNom_.setText("Ecluse " + (donnees_.getListeEcluse_().getListeEcluses_().size() + 1));
    this.cLongueur_.setText("");
    this.cLargeur_.setText("");
    this.cdureepassageEcluseEtale_.setText("");
    this.ctempsEclusee_.setText("");
    this.ctempsFausseBassinnee_.setText("");
    this.ccreneauetaleapresPMdeb_.setText("");
    this.ccreneauetaleapresPMfin_.setText("");
    this.ccreneauetaleavtPMdeb_.setText("");
    this.ccreneauetaleavtPMfin_.setText("");
    this.validation_.setText("Valider");
    // rafraichissement du tableau
    this.MENUECLUSE_.affichagePanel_.maj(donnees_);
    this.cNom_.requestFocus();
    this.cNom_.selectAll();

    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.dureeIndispo_.setText("");
    this.loiProbaDuree_.setSelectedIndex(0);
    this.loiProbaFrequence_.setSelectedIndex(0);
    this.choixLoiFrequence.setSelectedIndex(0);
    this.loiDeterministe_ = new ArrayList();
    
  }

}
