/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license GNU General Public Licence 2
 *@copyright (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeAttentesGenerales;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.fudaa.fudaa.sipor.ui.tools.SiporJFreeChartCamembert;
import org.jfree.chart.ChartPanel;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatsAttenteTrajet extends SiporInternalFrame {


	private static final long serialVersionUID = 1L;

	
/**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * Camembert des proportions
   */
  ChartPanel camembert_;

  /**
   * Panel de gestion du camembert
   */
  BuPanel panelCamembert_ = new BuPanel(new BorderLayout());

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  /** Comboliste de selection de l element de d�part */
  String[] listeElt = { "Chenal", "Cercle", "Ecluse", "Quai" };
  JComboBox ListeTypesDepart_ = new JComboBox(listeElt);
  JComboBox ListeElementDepart_ = new JComboBox();
  /** Comboliste de selection de l element d'arrivee */
  JComboBox ListeTypesArrivee_ = new JComboBox(listeElt);
  JComboBox ListeElementArrivee_ = new JComboBox();

  /** horaire de d�part */
  String[] chaine_sens = { "Entrant", "Sortant", "Les 2 sens" };
  JComboBox Sens_ = new JComboBox(chaine_sens);

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  JCheckBox choixNbNavires_ = new JCheckBox("Nombre navires", true);
  JCheckBox choixTotalAttente_ = new JCheckBox("Attente totale", true);

  JCheckBox choixMarees_ = new JCheckBox("Attentes mar�es", true);
  JCheckBox choixSecurite_ = new JCheckBox("Attentes s�curit�", true);
  JCheckBox choixAcces_ = new JCheckBox("Attentes acc�s", true);
  JCheckBox choixOccupation_ = new JCheckBox("Attentes occupation", true);
  JCheckBox choixPannes_ = new JCheckBox("Attentes Indispos", true);

  JCheckBox[] tableauChoixNavires_;

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Nb navires", "Total.Mar�e", "Moy attentes Mar�e",
      "Att. mar�e: nb. bateaux", "Att. mar�e: % bateaux", "Moy flotte Mar�e", "Total.Secu", "Moy attentes Secu", "Att. s�cu: nb. bateaux", "Att. s�cu: % bateaux",
      "Moy flotte S�cu", "Total.Acces", "Moy attentes Acc�s", "Att. acc�s: nb. bateaux", "Att. acc�s: % bateaux", "Moy/flotte acc�s",
      "Total.Occup", "Moy attentes Occup", "Att. occup: nb. bateaux", "Att. occup: % bateaux", "Moy/flotte occup ", "Total.Indispos",
      "Moy attentes Indispos", "Att. indispo: nb. bateaux", "Att. indispo: % bateaux", "Moy/flotte indispo ", "Total.Global",
      "Moy attentes Globale", "Att. globale: nb. bateaux", "Att. globale: % bateaux", "Moy/flotte globale" };
  // 26

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();
  JComboBox listeNaviresCamembert_ = new JComboBox();
  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatsAttenteTrajet(final SiporDataSimulation _donnees) {
    super("Attentes par trajet", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le chenal 0 et le quai 0 dans les 2 sens de navigation
    SiporAlgorithmeAttentesGenerales.calculTrajetDepuisListeTrajet(donnees_, 0, 0, 3, 0, 0);

    setSize(820, 600);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);

    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("Camembert", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelCamembert_);

    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipalAffichage_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    panelPrincipalAffichage_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.getCategoriesNavires_().getListeNavires_().size()];
    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom(),
          true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }

    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(2, 1));

    selectionPanel1 = new BuPanel();
    selectionPanel1.add(new JLabel("Attentes � cumuler pour le trajet entre:"));
    selectionPanel1.add(new JLabel("l'�l�ment:"));
    selectionPanel1.add(this.ListeTypesDepart_);
    selectionPanel1.add(this.ListeElementDepart_);
    selectionPanel1.add(new JLabel("et l'�l�ment:"));
    selectionPanel1.add(this.ListeTypesArrivee_);
    selectionPanel1.add(this.ListeElementArrivee_);
    selectionPanel1.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel1);

    final BuPanel selectionPanel2 = new BuPanel();
    selectionPanel2.add(new JLabel("Sens du trajet: "));
    selectionPanel2.add(this.Sens_);
    selectionPanel2.add(lancerRecherche_);

    selectionPanel2.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel2);

    this.selectionPanel_.setBorder(this.compound_);

    // listener des liste box

    final ActionListener RemplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        int selection = 0;
        if (e.getSource() == ListeTypesDepart_) {
          selection = ListeTypesDepart_.getSelectedIndex();
        } else {
          selection = ListeTypesArrivee_.getSelectedIndex();
        }
        // "gare","quai","ecluse","chenal","cercle","bassin"
        switch (selection) {

        case 3:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 2:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 0:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 1:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        }

      }
    };
    this.ListeTypesDepart_.addActionListener(RemplissageElement);
    this.ListeTypesArrivee_.addActionListener(RemplissageElement);
    this.ListeTypesDepart_.setSelectedIndex(0);
    this.ListeTypesArrivee_.setSelectedIndex(3);

    this.lancerRecherche_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    final JTabbedPane panoption = new JTabbedPane();
    this.optionPanel_.add(panoption);

    final Box bVert = Box.createVerticalBox();
    final JScrollPane pcnasc1 = new JScrollPane(bVert);
    panoption.addTab("Attentes", pcnasc1);
    bVert.add(new JLabel(""));
    bVert.add(this.choixNbNavires_);
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Options Affichage");
    bVert.setBorder(bordure1);

    bVert.add(this.choixMarees_);
    bVert.add(this.choixSecurite_);
    bVert.add(this.choixAcces_);
    bVert.add(this.choixOccupation_);
    bVert.add(this.choixPannes_);
    bVert.add(this.choixTotalAttente_);

    final Box bVert2 = Box.createVerticalBox();
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Affichage navires");
    bVert2.setBorder(bordure2);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    panoption.addTab("Navires", pcnasc);

    this.optionPanel_.setBorder(this.compound_);

    // listener des checkbox de choix des options d affichage

    this.choixAcces_.addActionListener(this);
    this.choixMarees_.addActionListener(this);
    this.choixSecurite_.addActionListener(this);
    this.choixNbNavires_.addActionListener(this);
    this.choixTotalAttente_.addActionListener(this);
    this.choixOccupation_.addActionListener(this);
    this.choixPannes_.addActionListener(this);

    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(new JLabel("S�lectionnez la cat�gorie � visualiser:   "));
    this.controlPanel_.add(this.ListeNavires_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // panel du cammbert
    camembert_ = affichageCamembert(0);
    this.panelCamembert_.add(camembert_, BorderLayout.CENTER);

    BuPanel bpc = new BuPanel();
    bpc.add(listeNaviresCamembert_);
    this.panelCamembert_.add(bpc, BorderLayout.NORTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
      this.listeNaviresCamembert_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    this.listeNaviresCamembert_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = listeNaviresCamembert_.getSelectedIndex();
        modifierCamembert(val);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatsAttenteTrajet.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    final JScrollPane panneauHisto = new JScrollPane(this.histo_);
    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(/* this.histo_ */panneauHisto, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatsAttenteTrajet.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatsAttenteTrajet.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // affichage du tableau

    // operation magique qui permet de determiner ce tableau

    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.getCategoriesNavires_().getListeNavires_().size()+3][titreTableau_.length];
    
    
    int totalSecurite=0;
    int nbTotalBateauxSecurite=0;
    int nbBateauxSecu =0;
    int nbBateauxMaree =0;
    int totalAcces=0;
    int nbTotalBateauxAcces=0;
    int totalMaree=0;
    int nbTotalBateauxMaree=0;
    int nbBateauxAcces =0;
    int totalOccupation=0;
    int nbTotalBateauxOccupation=0;
    int nbBateauxOccupation =0;
    int totalIndisponibilite=0;
    int nbTotalBateauxPannes=0;
    int nbBateauxPannes =0;
    int totaltotal=0;
    int nbTotalBateauxTotalAttente=0;
    int nbBateauxTotalAtente =0;
    
    int indiceTbaleau = 0;
    if (val < 0) {

      for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
        if (this.tableauChoixNavires_[i].isSelected()) {
          data[indiceTbaleau][0] = this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom();

          // ecriture des donn�es calcul�es pour les dur�es de parcours
          // si les cases correspondantes ont �t� coch�es:
          int indiceColonne = 1;
          if (this.choixNbNavires_.isSelected()) {
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu
                    .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal);
          }

          if (this.choixMarees_.isSelected()) {
            if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale != 0) {
              // attente totale
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                          .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale));
              // moyenne attente sur les navires qui attendent
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteMaree));
              // nombre de navires qui attendent
              nbBateauxMaree = FonctionsSimu
                      .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteMaree);
              nbTotalBateauxMaree = nbTotalBateauxMaree + nbBateauxMaree;
              data[indiceTbaleau][indiceColonne++] = (int) nbBateauxMaree;
              data[indiceTbaleau][indiceColonne++] =
                  (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteMaree
                      / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + " %";
              // moyenne attentes sur la flotte
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }
          if (this.choixSecurite_.isSelected()) {
            if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale != 0) {
              // attente totale
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                          .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale));
              // moyenne attente sur les navires qui attendent
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu));
              // nombre de navires qui attendent
              nbBateauxSecu = FonctionsSimu
                      .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu);
              nbTotalBateauxSecurite = nbTotalBateauxSecurite + nbBateauxSecu;
              data[indiceTbaleau][indiceColonne++] = (int) nbBateauxSecu;
              data[indiceTbaleau][indiceColonne++] = (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu
                      / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + " %";

              // moyenne attentes sur la flotte
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (this.choixAcces_.isSelected()) {
            if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale != 0) {
              // attente totale
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                          .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale));
              // moyenne attente sur les navires qui attendent
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces));
              // nombre de navires qui attendent
              nbBateauxAcces = FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces);
              nbTotalBateauxAcces = nbTotalBateauxAcces + nbBateauxAcces;
              data[indiceTbaleau][indiceColonne++] = (int) nbBateauxAcces;
              data[indiceTbaleau][indiceColonne++] = ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces
                      / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + " %";

              // moyenne attentes sur la flotte
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (this.choixOccupation_.isSelected()) {
            if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale != 0) {
              // attente totale
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                          .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale));
              // moyenne attente sur les navires qui attendent
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup));
              // nombre de navires qui attendent
              nbBateauxOccupation = FonctionsSimu
                      .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup);
              nbTotalBateauxOccupation = nbTotalBateauxOccupation + nbBateauxOccupation;
              data[indiceTbaleau][indiceColonne++] = (int) nbBateauxOccupation ;
              data[indiceTbaleau][indiceColonne++] = (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup
                      / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + " %";

              // moyenne attentes sur la flotte
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (this.choixPannes_.isSelected()) {
            if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale != 0) {
              // attente totale
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                          .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale));
              // moyenne attente sur les navires qui attendent
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne));
              // nombre de navires qui attendent
              nbBateauxPannes = FonctionsSimu
                      .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne);
              nbTotalBateauxPannes = nbTotalBateauxPannes + nbBateauxPannes;
              data[indiceTbaleau][indiceColonne++] = (int) nbBateauxPannes;
              data[indiceTbaleau][indiceColonne++] = ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne
                      / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + " %";

              // moyenne attentes sur la flotte
              System.out
                  .println("moyenen indispo sur flotte avant trans en h.min: "
                      + ((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale) / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));

              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2(((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale) / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (this.choixTotalAttente_.isSelected()) {
            if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale != 0) {
              // attente totale
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                          .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale));
              // moyenne attente sur les navires qui attendent
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteTotale));
              // nombre de navires qui attendent
              nbBateauxTotalAtente= FonctionsSimu
                      .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteTotale);
              nbTotalBateauxTotalAttente = nbTotalBateauxTotalAttente + nbBateauxTotalAtente;
              data[indiceTbaleau][indiceColonne++] = (int) nbBateauxTotalAtente;
              data[indiceTbaleau][indiceColonne++] =((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteTotale
                      / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + "%)";

              // moyenne attentes sur la flotte
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale / (float) this.donnees_
                          .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }
          
          totalMaree+=FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale);
          totalAcces+=FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale);
          totalSecurite+=FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale);
          totalOccupation+=FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale);
          totalIndisponibilite+=FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale);
          totaltotal+=FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale);
          
          indiceTbaleau++;

        }// fin du if si la cat�gorie a �t� selectionn�e pour etre affich�e
      }
      
      
      //-- adrien - ajout des totaux en fin de tableau -//
      int cl=2;
      data[++indiceTbaleau][0]="TOTAL: "+(indiceTbaleau-1)+" cat�gories affich�es";
      if (this.choixMarees_.isSelected()) {
          data[indiceTbaleau][cl]="Total Mar�e";
          data[indiceTbaleau+1][cl]=SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(totalMaree);	
          cl+=2;
          data[indiceTbaleau][cl]="Total Nav. Mar�e";
          data[indiceTbaleau+1][cl]= ""+nbTotalBateauxMaree;
          cl+=3;
          }
      if (this.choixSecurite_.isSelected()) {
          data[indiceTbaleau][cl]="Total Secu.";
          data[indiceTbaleau+1][cl]=SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(totalSecurite);	
          cl+=2;
          data[indiceTbaleau][cl]="Total Nav. S�cu.";
          data[indiceTbaleau+1][cl]= ""+nbTotalBateauxSecurite;
          cl+=3;
          }
      if (this.choixAcces_.isSelected()) {
	      data[indiceTbaleau][cl]="Total Acc�s";
	      data[indiceTbaleau+1][cl]=SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(totalAcces);
	      cl+=2;
	      data[indiceTbaleau][cl]="Total Nav. Acc�s";
	      data[indiceTbaleau+1][cl]= ""+nbTotalBateauxAcces;
	      cl+=3;
      }
     
      if (this.choixOccupation_.isSelected()) {
          data[indiceTbaleau][cl]="Total Occup.";
          data[indiceTbaleau+1][cl]=SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(totalOccupation);	
          cl+=2;
          data[indiceTbaleau][cl]="Total Nav. Occup.";
          data[indiceTbaleau+1][cl]= ""+nbTotalBateauxOccupation;
          cl+=3;
      }
      if (this.choixPannes_.isSelected()) {
          data[indiceTbaleau][cl]="Total Pannes";
          data[indiceTbaleau+1][cl]=SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(totalIndisponibilite);	
          cl+=2;
          data[indiceTbaleau][cl]="Total Nav. Pannes";
          data[indiceTbaleau+1][cl]= ""+nbTotalBateauxPannes;
          cl+=3;
      }
      if (this.choixTotalAttente_.isSelected()) {
          data[indiceTbaleau][cl]="Total toutes attentes";
          data[indiceTbaleau+1][cl]=SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(totaltotal);	
          cl+=2;
          data[indiceTbaleau][cl]="Total Navires toutes attentes";
          data[indiceTbaleau+1][cl]= ""+nbTotalBateauxTotalAttente;
      }
      
      

    } else if (val < this.donnees_.getCategoriesNavires_().getListeNavires_().size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data = new Object[1][this.titreTableau_.length];
      data[0][0] = this.donnees_.getCategoriesNavires_().retournerNavire(val).getNom();
      // on complete les don�nes par le tableau de resultats:
      // data[0][0]..........
      int indiceColonne = 1;
      if (this.choixNbNavires_.isSelected()) {
        data[0][indiceColonne++] = ""
            + (float) FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal);
      }

      if (this.choixMarees_.isSelected()) {
        // attente totale
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMareeTotale);
        // moyenne attente sur les navires qui attendent
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMareeTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteMaree));
        // nombre de navires qui attendent
        nbBateauxMaree= FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteMaree);
        nbTotalBateauxMaree= nbTotalBateauxMaree + nbBateauxMaree;
        data[0][indiceColonne++] =  (int) nbBateauxMaree;
        data[indiceTbaleau][indiceColonne++] = ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteMaree
                / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + " %";

        // moyenne attentes sur la flotte
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMareeTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }
      if (this.choixSecurite_.isSelected()) {
        // attente totale
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                    .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteSecuTotale));
        // moyenne attente sur les navires qui attendent
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteSecuTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteSecu));
        // nombre de navires qui attendent
        nbBateauxSecu= FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteSecu);
        nbTotalBateauxSecurite= nbTotalBateauxSecurite + nbBateauxSecu;
        data[0][indiceColonne++] = (int) nbBateauxSecu;
        data[indiceTbaleau][indiceColonne++] = ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteSecu
                / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + " %";

        // moyenne attentes sur la flotte
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteSecuTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (this.choixAcces_.isSelected()) {
        // attente totale
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                    .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteAccesTotale));
        // moyenne attente sur les navires qui attendent
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteAccesTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteAcces));
        // nombre de navires qui attendent
        nbBateauxAcces= FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteAcces);
        nbTotalBateauxAcces= nbTotalBateauxAcces + nbBateauxAcces;
        data[0][indiceColonne++] = (int) nbBateauxAcces;
        data[indiceTbaleau][indiceColonne++] =((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteAcces
                / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + " %";

        // moyenne attentes sur la flotte
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteAccesTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (this.choixOccupation_.isSelected()) {
        // attente totale
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                    .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteOccupTotale));
        // moyenne attente sur les navires qui attendent
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteOccupTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAtenteOccup));
        // nombre de navires qui attendent
        nbBateauxOccupation= FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAtenteOccup);
        nbTotalBateauxOccupation= nbTotalBateauxOccupation + nbBateauxOccupation;
        data[0][indiceColonne++] = (int) nbBateauxOccupation;
        data[indiceTbaleau][indiceColonne++] = ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAtenteOccup
                / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + " %";
        // moyenne attentes sur la flotte
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteOccupTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (this.choixPannes_.isSelected()) {
        // attente totale
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                    .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attentePanneTotale));
        // moyenne attente sur les navires qui attendent
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attentePanneTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttentePanne));
        // nombre de navires qui attendent
        nbBateauxPannes= FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttentePanne);
        nbTotalBateauxPannes= nbTotalBateauxPannes + nbBateauxPannes;
        data[0][indiceColonne++] = (int) nbBateauxPannes;
        data[indiceTbaleau][indiceColonne++] =((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttentePanne
                / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + " %";
        // moyenne attentes sur la flotte
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2(((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attentePanneTotale) / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (this.choixTotalAttente_.isSelected()) {
        // attente totale
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu
                    .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMegaTotale));
        // moyenne attente sur les navires qui attendent
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMegaTotale / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteTotale));
        // nombre de navires qui attendent
        nbBateauxTotalAtente= FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteTotale);
        nbTotalBateauxTotalAttente= nbTotalBateauxTotalAttente + nbBateauxTotalAtente;
        data[0][indiceColonne++] = (int) nbBateauxTotalAtente;
        data[indiceTbaleau][indiceColonne++] = ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteTotale
                / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + " %";
        // moyenne attentes sur la flotte
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2(((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMegaTotale) / (float) this.donnees_
                    .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    // etape 3.5: dimensionnement des colonnes du tableau
   /* for (int i = 0; i < tableau_.getModel().getColumnCount(); i++) {
      TableColumn column = tableau_.getColumnModel().getColumn(i);
      column.setPreferredWidth(150);
    }*/
    ColumnAutoSizer.sizeColumnsToFit(tableau_);

    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Attente sur le trajet " + (String) this.ListeElementDepart_.getSelectedItem() + " vers "
        + (String) this.ListeElementArrivee_.getSelectedItem();
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens entrant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens sortant";
    } else {
      g += " dans les 2 sens";
    }
    g += " \"\n";

    if (this.choixMarees_.isSelected()) {
      g += "  sous-titre \" Attentes de Mar�es \"\n";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  sous-titre \" Attentes de S�curit� \"\n";
    } else if (this.choixAcces_.isSelected()) {
      g += "  sous-titre \" Attentes d' Acc�s \"\n";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  sous-titre \" Attentes Occupation \"\n";
    } else if (this.choixPannes_.isSelected()) {
      g += "  sous-titre \" Attentes de Indispos \"\n";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  sous-titre \" Attente totale \"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" dur�es" + "\"\n";
    g += "    unite \"" + " H.Min" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(((float) SiporAlgorithmeAttentesGenerales
            .determineAttenteMaxTrajet(donnees_)))
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    g += "  courbe\n  {\n";
    g += "    titre \"";

    if (this.choixMarees_.isSelected()) {
      g += "attentes Mar�es maxi";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  attentes S�curit� maxi";
    } else if (this.choixAcces_.isSelected()) {
      g += "attentes Acc�s maxi";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  attentes Occupations maxi";
    } else if (this.choixPannes_.isSelected()) {
      g += "  attentes Indispos maxi";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  attentes totale maxi";
    }
    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur   BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BB0000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixMarees_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMaxi);
        } else if (this.choixSecurite_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi);
        } else if (this.choixAcces_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi);
        } else if (this.choixOccupation_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi);
        } else if (this.choixPannes_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi);
        } else if (this.choixTotalAttente_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi);
        }

        g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
            + "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************

    g += "  courbe\n  {\n";
    g += "    titre \"";

    if (this.choixMarees_.isSelected()) {
      g += "attentes Mar�es moyenne";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  attentes S�curit� moyenne";
    } else if (this.choixAcces_.isSelected()) {
      g += "attentes Acc�s moyenne";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  attentes Occupations moyenne";
    } else if (this.choixPannes_.isSelected()) {
      g += "  attentes Indispos moyenne";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  attentes totale moyenne";
    }

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BB8800 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixMarees_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteMaree));
        } else if (this.choixSecurite_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu));
        } else if (this.choixAcces_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces));
        } else if (this.choixOccupation_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup));
        } else if (this.choixPannes_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne));
        } else if (this.choixTotalAttente_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMegaTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale));
        }

        g += "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************

    g += "  courbe\n  {\n";
    g += "    titre \"";
    if (this.choixMarees_.isSelected()) {
      g += "attentes Mar�es mini";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  attentes S�curit� mini";
    } else if (this.choixAcces_.isSelected()) {
      g += "attentes Acc�s mini";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  attentes Occupations mini";
    } else if (this.choixPannes_.isSelected()) {
      g += "  attentes Indispos mini";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  attentes totale mini";
    }

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BBCC00 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BBC00 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;

        if (this.choixMarees_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMini);
        } else if (this.choixSecurite_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini);
        } else if (this.choixAcces_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini);
        } else if (this.choixOccupation_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini);
        } else if (this.choixPannes_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini);
        } else if (this.choixTotalAttente_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini);
        }

        g += "\n";

      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo moy************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Attente sur le trajet " + (String) this.ListeElementDepart_.getSelectedItem() + " vers "
        + (String) this.ListeElementArrivee_.getSelectedItem();
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens entrant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens sortant";
    } else {
      g += " dans les 2 sens";
    }
    g += " \"\n";

    if (this.choixMarees_.isSelected()) {
      g += "  sous-titre \" Attentes de Mar�es \"\n";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  sous-titre \" Attentes de S�curit� \"\n";
    } else if (this.choixAcces_.isSelected()) {
      g += "  sous-titre \" Attentes d' Acc�s \"\n";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  sous-titre \" Attentes Occupation \"\n";
    } else if (this.choixPannes_.isSelected()) {
      g += "  sous-titre \" Attentes de Indispos \"\n";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  sous-titre \" Attente totale \"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" dur�es" + "\"\n";
    g += "    unite \"" + " H.Min" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeAttentesGenerales
            .determineAttenteMaxTrajet(donnees_))
        // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    g += "  courbe\n  {\n";
    g += "    titre \"";

    if (this.choixMarees_.isSelected()) {
      g += "attentes Mar�es maxi";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  attentes S�curit� maxi";
    } else if (this.choixAcces_.isSelected()) {
      g += "attentes Acc�s maxi";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  attentes Occupations maxi";
    } else if (this.choixPannes_.isSelected()) {
      g += "  attentes Indispos maxi";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  attentes totale maxi";
    }
    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur   BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixMarees_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMaxi);
        } else if (this.choixSecurite_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi);
        } else if (this.choixAcces_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi);
        } else if (this.choixOccupation_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi);
        } else if (this.choixPannes_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi);
        } else if (this.choixTotalAttente_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi);
        }

        g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
            + "\n";
      }// fin du if le navire est selectionn�
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************

    g += "  courbe\n  {\n";
    g += "    titre \"";

    if (this.choixMarees_.isSelected()) {
      g += "attentes Mar�es moyenne";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  attentes S�curit� moyenne";
    } else if (this.choixAcces_.isSelected()) {
      g += "attentes Acc�s moyenne";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  attentes Occupations moyenne";
    } else if (this.choixPannes_.isSelected()) {
      g += "  attentes Indispos moyenne";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  attentes totale moyenne";
    }

    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixMarees_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteMaree));
        } else if (this.choixSecurite_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu));
        } else if (this.choixAcces_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces));
        } else if (this.choixOccupation_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup));
        } else if (this.choixPannes_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne));
        } else if (this.choixTotalAttente_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMegaTotale / (float) this.donnees_
                  .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale));
        }

        g += "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************

    g += "  courbe\n  {\n";
    g += "    titre \"";
    if (this.choixMarees_.isSelected()) {
      g += "attentes Mar�es mini";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  attentes S�curit� mini";
    } else if (this.choixAcces_.isSelected()) {
      g += "attentes Acc�s mini";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  attentes Occupations mini";
    } else if (this.choixPannes_.isSelected()) {
      g += "  attentes Indispos mini";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  attentes totale mini";
    }

    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BBCC00 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;

        if (this.choixMarees_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMini);
        } else if (this.choixSecurite_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini);
        } else if (this.choixAcces_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini);
        } else if (this.choixOccupation_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini);
        } else if (this.choixPannes_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini);
        } else if (this.choixTotalAttente_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini);
        }

        g += "\n";

      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo moy************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  public ChartPanel affichageCamembert(int i) {

    int nbAttentes = 0;

    if (this.choixMarees_.isSelected()) nbAttentes++;
    if (this.choixOccupation_.isSelected()) nbAttentes++;
    if (this.choixPannes_.isSelected()) nbAttentes++;
    if (this.choixSecurite_.isSelected()) nbAttentes++;
    if (this.choixAcces_.isSelected()) nbAttentes++;

    float[] tab = new float[nbAttentes];
    String[] libelles = new String[nbAttentes];
    int cpt = 0;

    if (this.choixMarees_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteMaree));
      libelles[cpt++] = "Maree";
    }
    if (this.choixAcces_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces));
      libelles[cpt++] = "Acces";
    }
    if (this.choixOccupation_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup));
      libelles[cpt++] = "Occup";
    }
    if (this.choixPannes_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne));
      libelles[cpt++] = "Indispo";
    }
    if (this.choixSecurite_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu));
      libelles[cpt++] = "S�curit�";
    }

    return SiporJFreeChartCamembert.creerCamembert(tab, libelles, "Attentes trajet de "
        + (String) this.ListeElementDepart_.getSelectedItem() + " � "
        + (String) this.ListeElementArrivee_.getSelectedItem() + " pour "
        + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
  }

  public void modifierCamembert(int i) {

    int nbAttentes = 0;

    if (this.choixMarees_.isSelected()) nbAttentes++;
    if (this.choixOccupation_.isSelected()) nbAttentes++;
    if (this.choixPannes_.isSelected()) nbAttentes++;
    if (this.choixSecurite_.isSelected()) nbAttentes++;
    if (this.choixAcces_.isSelected()) nbAttentes++;

    float[] tab = new float[nbAttentes];
    String[] libelles = new String[nbAttentes];
    int cpt = 0;

    if (this.choixMarees_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMareeTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteMaree));
      libelles[cpt++] = "Maree";
    }
    if (this.choixAcces_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces));
      libelles[cpt++] = "Acces";
    }
    if (this.choixOccupation_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup));
      libelles[cpt++] = "Occup";
    }
    if (this.choixPannes_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne));
      libelles[cpt++] = "Indispo";
    }
    if (this.choixSecurite_.isSelected()) {
      tab[cpt] = SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale / this.donnees_
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu));
      libelles[cpt++] = "S�curit�";
    }

    // -- Mise a jour camembert --//
    SiporJFreeChartCamembert.modifierCamembert(camembert_, tab, libelles, "Attentes trajet de "
        + (String) this.ListeElementDepart_.getSelectedItem() + " � "
        + (String) this.ListeElementArrivee_.getSelectedItem() + " pour "
        + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());

  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
    if (source == this.choixTotalAttente_ || source == this.choixNbNavires_ || source == this.choixAcces_
        || source == this.choixMarees_ || source == this.choixSecurite_ || source == this.choixOccupation_
        || source == this.choixPannes_) {
      // clic sur un checkBox:
      // construction de la colonne des titres

      if (!this.choixMarees_.isSelected() && !this.choixSecurite_.isSelected() && !this.choixAcces_.isSelected()
          && !this.choixOccupation_.isSelected() && !this.choixPannes_.isSelected()
          && !this.choixTotalAttente_.isSelected()) {
        this.choixTotalAttente_.setSelected(true);
      }

      int compteurColonnes = 0;
      if (this.choixAcces_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixMarees_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixSecurite_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixTotalAttente_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixOccupation_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixPannes_.isSelected()) {
        compteurColonnes += 4;
      }

      // String

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;
      this.titreTableau_[indiceColonne++] = "Cat�gorie";
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Nb.nav.flotte";
      }

      if (this.choixMarees_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Mar�e";
        this.titreTableau_[indiceColonne++] = "Moy attente Mar�e";
        this.titreTableau_[indiceColonne++] = "Nb nav ayant Att.Mar�e";
        this.titreTableau_[indiceColonne++] = "Moy//flotte.Mar�e";
      }
      if (this.choixSecurite_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Secu";
        this.titreTableau_[indiceColonne++] = "MoyTotal.Secu";
        this.titreTableau_[indiceColonne++] = "Nb nav ayant Att.Secu";
        this.titreTableau_[indiceColonne++] = "Moy//flotte.Secu";
      }
      if (this.choixAcces_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Acces";
        this.titreTableau_[indiceColonne++] = "Moy attente Acces";
        this.titreTableau_[indiceColonne++] = "Nb nav ayant Att.Acces";
        this.titreTableau_[indiceColonne++] = "Moy//flotte.Acces";
      }
      if (this.choixOccupation_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Occup";
        this.titreTableau_[indiceColonne++] = "Moy attente Occup";
        this.titreTableau_[indiceColonne++] = "Nb nav ayant Att.Occup";
        this.titreTableau_[indiceColonne++] = "Moy//flotte.Occup";
      }
      if (this.choixPannes_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Indispo";
        this.titreTableau_[indiceColonne++] = "Moy attente Indispo";
        this.titreTableau_[indiceColonne++] = "Nb nav ayant Att.Indispo";
        this.titreTableau_[indiceColonne++] = "Moy//flotte.Indispo";
      }
      if (this.choixTotalAttente_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "attente totale";
        this.titreTableau_[indiceColonne++] = "Moy attente totale";
        this.titreTableau_[indiceColonne++] = "Nb nav ayant Att.totale";
        this.titreTableau_[indiceColonne++] = "moyenne attentes//flotte";
      }
      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      // -- mise a jour du camembert --//
      modifierCamembert(listeNaviresCamembert_.getSelectedIndex());

    } else if (source == this.lancerRecherche_) {
      if (this.ListeElementArrivee_.getSelectedIndex() == this.ListeElementDepart_.getSelectedIndex()
          && this.ListeTypesArrivee_.getSelectedIndex() == this.ListeTypesDepart_.getSelectedIndex()) {
        donnees_.getApplication();
        new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
            "Erreur, vous avez s�lectionn� le m�me �l�ment de d�part et d'arriv�e.").activate();
        return;
      }
      /**
       * C est ici que le calcul se refait: lors de la validation du bouton de lancement de rercherche: recalcule les
       * attentes pour le nouveau trajet:
       */
      // lancement des calculs pour les dur�es de parcours:
      SiporAlgorithmeAttentesGenerales.calculTrajetDepuisListeTrajet(donnees_, this.ListeTypesDepart_
          .getSelectedIndex(), this.ListeElementDepart_.getSelectedIndex(), this.ListeTypesArrivee_.getSelectedIndex(),
          this.ListeElementArrivee_.getSelectedIndex(), this.Sens_.getSelectedIndex());

      // mise a jour des affichages:
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.valideSeuil_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
