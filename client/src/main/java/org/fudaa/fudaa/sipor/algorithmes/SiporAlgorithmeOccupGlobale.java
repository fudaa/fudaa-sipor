package org.fudaa.fudaa.sipor.algorithmes;

import java.util.ArrayList;

import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sipor.SParametresResultatsoccupationG;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

public class SiporAlgorithmeOccupGlobale {

  public static boolean est_inclus(final double a1, final double a2, final double b1, final double b2) {
    if (b1 <= a1 && a1 <= b2) {
      return true;
    }
    if (b1 <= a2 && a2 <= b2) {
      return true;
    }
    if (a1 <= b1 && b1 <= a2) {
      return true;
    }
    if (a1 <= b2 && b2 <= a2) {
      return true;
    }
    if (b1 <= a1 && a2 <= b2) {
      return true;
    }
    if (a1 <= b1 && b2 <= a2) {
      return true;
    }

    return false;
  }

  public static double[] FusionnerCreneau(final double a1, final double a2, final double b1, final double b2) {
    final double[] t = new double[2];
    if (a1 < b1) {
      t[0] = a1;
    } else {
      t[0] = b1;
    }

    if (a2 < b2) {
      t[1] = b2;
    } else {
      t[1] = a2;
    }

    return t;
  }

  /**
   * methode qui determine les occupations lineaires ainsi que les occupations � la dur�e pour chaque quais: se referer
   * a la page 31 de la notice de sipor
   * 
   * @param donnees_ donnes de la simulation
   */
  public static void calcul(final SiporDataSimulation donnees_) {

    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // allocation memoire du tableau de tailel du nombre de quais
    resultats.tableauOccupationGlobal = new SParametresResultatsoccupationG[donnees_.getlQuais_().getlQuais_().size()];

    /*******************************************************************************************************************
     * Occupation Lin�aire
     ******************************************************************************************************************/
    // initialisation
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      // allocation memoire
      resultats.tableauOccupationGlobal[i] = new SParametresResultatsoccupationG();
      resultats.tableauOccupationGlobal[i].nombreNaviresTotal = 0;
      resultats.tableauOccupationGlobal[i].occupationLineaire = 0;
    }
    // calcul des donn�es
    for (int n = 0; n < donnees_.getListeResultatsSimu_().nombreNavires; n++) {
      

    	//on recherche l'indice du quai qui se situe au milieu du tableau:
    	int indiceTableau=-1;
    	boolean trouve=false;
    	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours && !trouve;i++)
    		if(donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[i].typeElement==3)
    		{
    			indiceTableau=i;
    			trouve=true;
    		}
    	if(indiceTableau==-1){
    		System.out.println("Erreur: occupation quai: on a pas trouv�de quai! impossible...");
    	}
    	else{
    		//-- quai trouv�-//

    		final int quai = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].indiceElement;
    		final double duree = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].heureSortie
    		- donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].heureEntree;

    		final double longueurNavire = donnees_.getListeResultatsSimu_().listeEvenements[n].longueur;

    		resultats.tableauOccupationGlobal[quai].occupationLineaire += duree * longueurNavire;
    		resultats.tableauOccupationGlobal[quai].nombreNaviresTotal++;
    	}//fin du else
    	
    }
    
    // on divise la lnogueur du quai par le nombre de jours de la simulation
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      resultats.tableauOccupationGlobal[i].occupationLineaire = resultats.tableauOccupationGlobal[i].occupationLineaire
          / (donnees_.getParams_().donneesGenerales.nombreJours * 24.0 * 60.0 * donnees_.getlQuais_().retournerQuais(i).getLongueur_());
    }

    /*******************************************************************************************************************
     * Occupation a la dur�e
     ******************************************************************************************************************/
    // definition des objets
    final ArrayList[] tableau = new ArrayList[donnees_.getlQuais_().getlQuais_().size()];
    class Crenod {
      Crenod() {

      }
      double heureEntree;
      double heureSortie;
    }

    for (int q = 0; q < donnees_.getlQuais_().getlQuais_().size(); q++) {
      tableau[q] = new ArrayList();
    }

    // calcul des donn�es
    for (int n = 0; n < donnees_.getListeResultatsSimu_().nombreNavires; n++) {


    	//on recherche l'indice du quai qui se situe au milieu du tableau:
    	int indiceTableau=-1;
    	boolean trouve=false;
    	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours && !trouve;i++)
    		if(donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[i].typeElement==3)
    		{
    			indiceTableau=i;
    			trouve=true;
    		}
    	if(indiceTableau==-1){
    		System.out.println("Erreur: occupation quai: on a pas trouv� quai! impossible...");
    	}
    	else{
    		//-- quai trotrouv�/

    		final int quai = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].indiceElement;
    		final Crenod t = new Crenod();
    		t.heureEntree = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].heureEntree;
    		t.heureSortie = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].heureSortie;
    		// creation du vecteur

    		tableau[quai].add(t);
    	}
    }
    
    // on fusionne les cr�neaux qui se chevauchenten cr�neaux optimal
    for (int q = 0; q < donnees_.getlQuais_().getlQuais_().size(); q++) {
      final ArrayList v = tableau[q];
      boolean changement = true;

      while (changement) {
        changement = false;
        for (int i = 0; i < v.size(); i++) {
          for (int j = i + 1; j < v.size(); j++) {
            if (SiporAlgorithmeOccupGlobale.est_inclus(((Crenod) v.get(i)).heureEntree,
                ((Crenod) v.get(i)).heureSortie, ((Crenod) v.get(j)).heureEntree, ((Crenod) v.get(j)).heureSortie)) {
              changement = true;
              // les intervalles se chevauchent
              // on fusionne alors ces 2 intervalles, que l'on ajoute dans v(i) et on supprime v(j)
              final double[] t = SiporAlgorithmeOccupGlobale.FusionnerCreneau(((Crenod) v.get(i)).heureEntree,
                  ((Crenod) v.get(i)).heureSortie, ((Crenod) v.get(j)).heureEntree, ((Crenod) v.get(j)).heureSortie);
              final Crenod nouveauCreneau = new Crenod();
              nouveauCreneau.heureEntree = t[0];
              nouveauCreneau.heureSortie = t[1];

              // on ajoute la fusion des creneau v(i) et v(j) dans v(i) et on supprime v(j)
              v.set(i, nouveauCreneau);
              v.remove(j);
              // puisqu on supprime v(j), on effectue un decalage des valeurs a gauche dans le vecteur
              // il ne faus donc pas faire incrementer j aar sinon on loupe un creneau a comparer et a eventuellement
              // fusionner
              j--;

            }
          }
        }

      }
      /*
       * arrive a ce stade de l'algorithme, on dispose pour un quai donn�, de l'ensemble de ses cr�neaux d'occupation
       * tous distinct dans le vecteur v, donc pour avoir la somme des dur�es ou au moins un navire occupe le quai, il
       * suffit de somemr les creneaux.
       */
      resultats.tableauOccupationGlobal[q].occupationDuree = 0;
      for (int i = 0; i < v.size(); i++) {
        resultats.tableauOccupationGlobal[q].occupationDuree += ((Crenod) v.get(i)).heureSortie
            - ((Crenod) v.get(i)).heureEntree;
      }
      // on divise par le temps total de la simulation
      resultats.tableauOccupationGlobal[q].occupationDuree = resultats.tableauOccupationGlobal[q].occupationDuree
          / (donnees_.getParams_().donneesGenerales.nombreJours * 24.0 * 60.0);

    }// fin du poru global
    // resultats.tableauOccupationGlobal[n].occupationDuree=19.6;

    /*
     * METHODE PERMETTANT DE CALCULER LES TS GLOBAUX
     */
    resultats.TS = new double[donnees_.getlQuais_().getlQuais_().size()];

    // initialisation des ts des quais
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      resultats.TS[i] = 0;
    }

    // somme des temps de service des navires au quais
    for (int i = 0; i < donnees_.getListeResultatsSimu_().nombreNavires; i++) {

    	//-- on recherche l'indice du quai --//
    	int indiceTableau=-1;
    	boolean trouve=false;
    	for(int l=0;l<donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours && !trouve;l++)
    		if(donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[l].typeElement==3)
    		{
    			indiceTableau=l;
    			trouve=true;
    		}
    	if(indiceTableau==-1){
    		System.out.println("Erreur: occupation quai: on a pas  trouv� de quai! impossible...");
    	}
    	else{
    		//-- quai trouv� --//
    		resultats.TS[donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].indiceElement] += (float) (donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].heureSortie - donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].heureEntree);
    		
    		//-- adrien 3.2.6 - pour le temps de service total on retire le temps �ventuel d'attente � ce quai --//
    		double totaAttenteQuai = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].indispo + 
    				donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].marees +
    				donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].occupation +
    			    donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].acces +
    				donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].secu;
    		
    		resultats.TS[donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].indiceElement] = 
    				resultats.TS[donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[indiceTableau].indiceElement] - totaAttenteQuai;
    		
    	}
    }

  }

  public static int determineMaxNbNavires(final SiporDataSimulation d) {
    int max = 0;

    for (int i = 0; i < d.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal.length; i++) {
      if (d.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].nombreNaviresTotal > max) {
        max = d.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].nombreNaviresTotal;
      }
    }

    return (int) (max * 1.0 + 0.5 * max);
  }

  /**
   * MEthode permettant de calculer le temps de service total
   * 
   * @param d
   * @return
   */
  public static float[] determineTStotal(final SiporDataSimulation d) {
    final float[] t = new float[d.getParams_().ResultatsCompletsSimulation.TS.length];
    for (int i = 0; i < d.getParams_().ResultatsCompletsSimulation.TS.length; i++) {
      t[i] = (float) d.getParams_().ResultatsCompletsSimulation.TS[i];
    }
    return t;

  }

  /**
   * Methode qui determine le max parmi les temps de service totaux calcul�s � partir du tableau pr�c�dent contenant les
   * temps de service totaux
   * 
   * @param t
   * @return
   */
  public static float determineMaxTStotal(final float[] t) {
    int indiceMax = 0;
    float max = 0;

    for (int i = 0; i < t.length; i++) {
      if (t[i] > max) {
        max = t[i];
        indiceMax = i;
      }
    }

    return (float) (t[indiceMax] + 0.2 * t[indiceMax]);
  }

}
