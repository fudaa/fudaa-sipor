/*

 * @file         SiporImplementation.java
 * @creation     1999-10-01
 * @modification $Date: 2008-01-15 11:38:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import com.hexidec.ekit.component.JButtonNoFocus;
import com.memoire.bu.BuActionRemover;
import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuHelpFrame;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLookPreferencesPanel;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuMenuRecentFiles;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuSplashScreen;
import com.memoire.bu.BuTaskOperation;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuToolBar;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sipor.ICalculSipor;
import org.fudaa.dodico.corba.sipor.ICalculSiporHelper;
import org.fudaa.dodico.corba.sipor.IParametresSipor;
import org.fudaa.dodico.corba.sipor.IResultatsSipor;
import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.dodico.sipor.DCalculSipor;
import org.fudaa.dodico.sipor.DParametresSipor;
import org.fudaa.dodico.sipor.DResultatsSipor;
import org.fudaa.ebli.network.simulationNetwork.NetworkGraphModel;
import org.fudaa.ebli.network.simulationNetwork.ui.EditorMenuBar;
import org.fudaa.ebli.network.simulationNetwork.ui.EditorActions.SaveAction;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeAttentesGenerales;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeGenerationBateaux;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeOccupGlobale;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeOccupationsQuais;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeTOUTESAttentesTrajet;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeTOUTESDureesParcours;
import org.fudaa.fudaa.sipor.factory.MethodesUtiles;
import org.fudaa.fudaa.sipor.factory.SiporOutilsDonnees;
import org.fudaa.fudaa.sipor.factory.SiporPreferences;
import org.fudaa.fudaa.sipor.factory.SiporResource;
import org.fudaa.fudaa.sipor.mock.SiporMock;
import org.fudaa.fudaa.sipor.siporNetwork.SiporNetwork;
import org.fudaa.fudaa.sipor.siporNetwork.SiporNetworkDaoFactory;
import org.fudaa.fudaa.sipor.siporNetwork.SiporSyncrhonizeNetworkAction;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.draw.SiporDessinerPortFrame;
import org.fudaa.fudaa.sipor.ui.frame.GenarrFrameAffichage;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameGenerationRappelDonnees;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieDonneesGenerales;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieMaree;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserBassins;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserCercles;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserChenal;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserEcluses;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserGares;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserNavires;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserQuais;
import org.fudaa.fudaa.sipor.ui.implementation.SiporAssistant;
import org.fudaa.fudaa.sipor.ui.implementation.SiporLib;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelReglesGenesCercle;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelReglesNavigationCercle;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelReglesNavigationChenal;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelReglesParcoursCercle;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelReglesParcoursChenal;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelRemplissageSAS;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelTopologieBassin;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelTopologieCercle;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelTopologieChenal;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelTopologieEcluse;
import org.fudaa.fudaa.sipor.ui.panel.SiporPreferencesPanel;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporProgressFrame;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatComparaisonAttenteElement;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatComparaisonAttentetrajet;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatComparaisonDureeParcours;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatComparaisonGenerationBateaux;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatComparaisonOccupQuais;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatComparaisonOccupationGlobale;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatGenerationBateaux;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatHistorique;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatOccupationGlobales;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsAttenteGeneraleCategories;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsAttenteGeneraleElement;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsAttenteTrajet;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsCroisementsCercle;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsCroisementsChenal;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsDureesParcours;
import org.fudaa.fudaa.sipor.ui.resultat.frame.SiporResultatsOccupationQuai;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.fudaa.fudaa.sipor.ui.tools.SiporListeSimulations;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;


/**
 * L'implementation du client Sipor.
 * 
 * @version $Revision: 1.86 $ $Date: 2008-01-15 11:38:50 $ by $Author: bmarchan $
 * @author Adrien Hadoux
 */
public class SiporImplementation extends FudaaCommonImplementation {
  // public final static String LOCAL_UPDATE= ".";
  public final static boolean IS_APPLICATION = true;
  public static ICalculSipor SERVEUR_SIPOR;
  public static IConnexion CONNEXION_SIPOR;
  public static boolean MOCK_ENABLE = false;
  // private IPersonne PERSONNE= null;
  /**
   * Pointe vers parametres() de SERVEUR_SIPOR.
   */
  protected IParametresSipor siporParams_;
  /**
   * Pointe vers resultats() de SERVEUR_SIPOR.
   */
  protected IResultatsSipor siporResults_;
  public static final BuInformationsSoftware INFORMATION_SOFT = new BuInformationsSoftware();
  protected static final BuInformationsDocument idSipor_ = new BuInformationsDocument();
  // Concerne les donn�es
  protected Hashtable projets_; // Contient les projets ouverts
  protected FudaaProjet projet_; // Projet en cours
  public SiporOutilsDonnees outils_;

  //-- Definition des couleurs --//
  public static Color bleuSipor=new Color(54,138,191);
  public static Color bleuClairSipor=new Color(132,166,188);
  /**
   * Les donn�es de la simulation version 2006
   */
  SiporDataSimulation donnees_ = new SiporDataSimulation(this);

  /**
   * Fenetre de saisie des donn�es du logiciel:
   */

   /**
    * generation du reseau.
    */
  SiporNetwork networkEditor = new SiporNetwork(new NetworkGraphModel( new SiporNetworkDaoFactory(donnees_)));
  
  /**
   * Fenetre de gestion des quais
   */
  public SiporVisualiserQuais gestionQuais_ ;
  /**
   * Fenetre de gestion des cat�gories
   */
  public SiporVisualiserNavires gestionNavires_ ;
  /**
   * Fenetre de gestion des chenaux
   */
  public  SiporVisualiserChenal gestionChenaux_ ;
  /**
   * Fenetre de gestion des cercles
   */
  public SiporVisualiserCercles gestionCercles_ ;
  /**
   * Fenetre de gestion des ecluses
   */
  public SiporVisualiserGares gestionGares_ ;
  /**
   * Fenetre de gestion des bassins
   */
  public SiporVisualiserBassins gestionBassins_ ;
  /**
   * Fenetre de gestion des ecluses
   */
  public SiporVisualiserEcluses gestionEcluses_ ;

  /**
   * fenetre de gestion des mar�es:
   */
  public SiporFrameSaisieMaree gestionMarees_ ;
  /**
   * fenetre de gstion des donn�es g�n�rales
   */
  public SiporFrameSaisieDonneesGenerales gestionDonneesGenerales_ ;

  /**
   * fenetre de gestion des topologies
   */

  /**
   * fenetre de gestion des bassins
   */
  public SiporPanelTopologieBassin gestionTopoBassins_ ;
  /**
   * fenetre de gestion des ecluses
   */
  public  SiporPanelTopologieEcluse gestionTopoEcluses_ ;
  /**
   * fenetre de gestion des cheanux
   */
  public SiporPanelTopologieChenal gestionTopoChenaux_ ;
  /**
   * fenetre de gestion des cercles
   */
  public SiporPanelTopologieCercle gestionTopoCercles_ ;
  /**
   * fenetre de modelisation du port
   */
  public SiporDessinerPortFrame gestionModelisation_ ;

  /**
   * fenetre des regels et durees de parcours
   */

  /**
   * fenetre de gestion des regles de navigations pour les chenaux
   */
  public SiporPanelReglesNavigationChenal gestionNavigChenal_ ;
  /**
   * fenetre des regles de navig pour les cercles
   */
  public SiporPanelReglesNavigationCercle gestionNavigCercle_ ;
  /**
   * fenetre des dur�es de parcours des chenaux
   */
  public SiporPanelReglesParcoursChenal gestionDureeParcoursChenal_ ;
  /**
   * fenetre des dur�es de parcours dees cercles.
   */
  public  SiporPanelReglesParcoursCercle gestionDureeParcoursCercle_;

  
  public SiporPanelRemplissageSAS gestionRemplissageSAS_;
  
  /** fenetre de gestion des genes dans les cercles**/
  public  SiporPanelReglesGenesCercle gestionGenesCercle_;

  /**
   * Fenetre qui gere le rappel des donn�es.
   */
  public  SiporFrameGenerationRappelDonnees rappelDonnees_ ;

  // Outils pour acc�der aux donn�es de projet_
  // Concerne les fenetres

  protected BuBrowserFrame fRappelDonnees_;
  protected BuBrowserFrame fStatistiquesFinales_;

  protected BuAssistant assistant_;
  protected BuHelpFrame aide_;
  protected BuTaskView taches_;
  protected SiporListeSimulations liste_;
  protected BuInternalFrame fGraphiques_;
  static {
    INFORMATION_SOFT.banner = SiporResource.SIPOR.getIcon("/org/fudaa/fudaa/sipor/animation1");
    if(INFORMATION_SOFT.banner == null)
    	INFORMATION_SOFT.banner = SiporResource.SIPOR.getIcon("/resources/org/fudaa/fudaa/sipor/animation1");
    
    INFORMATION_SOFT.logo = SiporResource.SIPOR.getIcon("/org/fudaa/fudaa/sipor/logo3");
    if(INFORMATION_SOFT.logo == null)
    	 INFORMATION_SOFT.logo = SiporResource.SIPOR.getIcon("/resources/org/fudaa/fudaa/sipor/logo3");
     
    INFORMATION_SOFT.name = "Sipor";
    INFORMATION_SOFT.version = "1.1.2";
    INFORMATION_SOFT.date = "12-09-2014";
    INFORMATION_SOFT.rights = "Tous droits r�serv�s. CETMEF (c)1999,2006";
    INFORMATION_SOFT.contact = "Mederic.Fargeix@developpement-durable.gouv.fr";
    INFORMATION_SOFT.license = "GPL2";
    INFORMATION_SOFT.languages = "fr,en";
    // isSipor_.logo = SiporResource.SIPOR.getIcon("sipor_logo");
    // isSipor_.banner = SiporResource.SIPOR.getIcon("sipor_banner");
    INFORMATION_SOFT.http = "http://www.cetmef.developpement-durable.gouv.fr/fudaa/";
    INFORMATION_SOFT.update = "http://www.cetmef.developpement-durable.gouv.fr/fudaa/";
    INFORMATION_SOFT.man = "http://marina.cetmef.equipement.gouv.fr/fudaa/manuels/sipor/";
    INFORMATION_SOFT.authors = new String[] { "Adrien Hadoux" };
    INFORMATION_SOFT.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    INFORMATION_SOFT.documentors = new String[] {};
    INFORMATION_SOFT.testers = new String[] { "Alain Pourplanche", "Alain Chambreuil" };
    idSipor_.name = "Etude";
    idSipor_.version = "0.41";
    idSipor_.organization = "CETMEF";
    idSipor_.author = "Adrien Hadoux";
    idSipor_.contact = "contact@gfudaa.fr";
    idSipor_.date = FuLib.date();
    BuPrinter.INFO_LOG = INFORMATION_SOFT;
    BuPrinter.INFO_DOC = idSipor_;
  }

  
 
  protected BuButton supprimerSimu_= new BuButton(FudaaResource.FUDAA.getIcon("crystal_enlever"), "Enlever");
  protected BuButton ajouterSimu_= new BuButton(FudaaResource.FUDAA.getIcon("crystal_ajouter"), "Ajouter");
  
  
  
  
  public BuInformationsSoftware getInformationsSoftware() {
    return INFORMATION_SOFT;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return INFORMATION_SOFT;
  }

  public BuInformationsDocument getInformationsDocument() {
    return idSipor_;
  }

  public void init() {
    super.init();
    final BuSplashScreen ss = getSplashScreen();

    projet_ = null;
    aide_ = null;
    try {

      setTitle(getInformationsSoftware().name + CtuluLibString.ESPACE + getInformationsSoftware().version);
      if (ss != null) {
        ss.setText("Menus et barres d'outils compl�t�s par Sipor");
        ss.setProgression(50);
      }
      final BuMenuBar mb = getMainMenuBar();
      // FD fin

      /**
       * Ajout des menubar
       */
      mb.addMenu(construitMenuDonneesGenerales(IS_APPLICATION));
      mb.addMenu(construitMenuParametresConstruction(IS_APPLICATION));
      mb.addMenu(construitMenuTopologie(IS_APPLICATION));
      mb.addMenu(construitMenuReglesNavigations(IS_APPLICATION));
      mb.addMenu(construitMenuGeneration(IS_APPLICATION));
      mb.addMenu(construitMenuSimulation(IS_APPLICATION));
      mb.addMenu(construitMenuExploitation(IS_APPLICATION));
      mb.addMenu(construitMenuComparaisonSimulations(IS_APPLICATION));
      final BuToolBar tb = getMainToolBar();

      construitToolBar(tb);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("AIDE_INDEX", false);
      setEnabledForAction("AIDE_ASSISTANT", false);
      setEnabledForAction("TABLEAU", false);
      setEnabledForAction("GRAPHE", false);
      BuActionRemover.removeAction(getMainToolBar(), "COPIER");
      BuActionRemover.removeAction(getMainToolBar(), "COUPER");
      BuActionRemover.removeAction(getMainToolBar(), "COLLER");
      BuActionRemover.removeAction(getMainToolBar(), "DEFAIRE");
      BuActionRemover.removeAction(getMainToolBar(), "REFAIRE");
      BuActionRemover.removeAction(getMainToolBar(), "TOUTSELECTIONNER");
      BuActionRemover.removeAction(getMainToolBar(), "RANGERICONES");
      BuActionRemover.removeAction(getMainToolBar(), "RANGERPALETTES");
      BuActionRemover.removeAction(getMainToolBar(), "POINTEURAIDE");
      BuActionRemover.removeAction(getMainToolBar(), "RECHERCHER");
      BuActionRemover.removeAction(getMainToolBar(), "REMPLACER");
      BuActionRemover.removeAction(getMainToolBar(), "CONNECTER");
      
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(SiporPreferences.SIPOR);
        mr.setResource(SiporResource.SIPOR);
        mr.setEnabled(true);
      }
      
      
      if (ss != null) {
        ss.setText("Assistant, barres des t�ches");
        ss.setProgression(75);
      }
      assistant_ = new SiporAssistant();
      taches_ = new BuTaskView();
      
      //-- ajout des boutons de manipulation des simulations --//
      BuPanel csp1=new BuPanel(new BorderLayout());
      liste_ = new SiporListeSimulations(getApp());
      final BuScrollPane sp1 = new BuScrollPane(taches_);
      final BuScrollPane sp2 = new BuScrollPane(liste_);
      sp1.setPreferredSize(new Dimension(150, 80));
      sp2.setPreferredSize(new Dimension(150, 80));
      
      csp1.add(sp2,BorderLayout.CENTER);
      BuPanel b1=new BuPanel(new GridLayout(2,1,2,5));
      b1.add(ajouterSimu_);
      b1.add(supprimerSimu_);
      
      csp1.add(b1,BorderLayout.SOUTH);
    
      getMainPanel().getRightColumn().addToggledComponent(BuResource.BU.getString("Taches"), "TACHE", sp1, this);
      getMainPanel().getRightColumn().addToggledComponent("Simulations", "SIMULATIONS", csp1, this);
      
      
      ajouterSimu_.addActionListener(this);
      supprimerSimu_.addActionListener(this);
      
      if (ss != null) {
        ss.setText("Logo et barre des taches");
        ss.setProgression(95);
      }
      getMainPanel().setLogo(getInformationsSoftware().logo);
      getMainPanel().setTaskView(taches_);
      if (ss != null) {
        ss.setText("Termin�");

        ss.setProgression(100);
      }
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
    
    
   
    
  BuResource.BU.setIconFamily("crystal");
  
  
  
    
  }

  private void extendFrame() {
	      this.getFrame().setExtendedState(Frame.MAXIMIZED_BOTH);
	  
}

public void start() {
    super.start();
   
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + getInformationsSoftware().name + " "
        + getInformationsSoftware().version);
   
    final BuMainPanel mp = getMainPanel();
    mp.doLayout();
    mp.validate();
    projets_ = new Hashtable();
    outils_ = new SiporOutilsDonnees(projet_);
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er une\nnouvelle simulation\nou en ouvrir une");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    SiporPreferences.SIPOR.applyOn(this);
    
    extendFrame();
  }

  protected void construitToolBar(final BuToolBar newtb) {
    newtb.removeAll();
    newtb.addToolButton("Cr�er", "CREER", FudaaResource.FUDAA.getIcon("crystal_creer"), false);
    newtb.addToolButton("Ouvrir", "OUVRIR", FudaaResource.FUDAA.getIcon("crystal_ouvrir"), false);
    newtb.addToolButton("Enreg", "ENREGISTRER", FudaaResource.FUDAA.getIcon("crystal_enregistrer"), false);
    newtb.addToolButton("nettoyer", "NETTOYER", FudaaResource.FUDAA.getIcon("crystal_valeur-initiale"), false);

    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addToolButton("G�n�ral", "DONNEESGENERALES", FudaaResource.FUDAA.getIcon("analyser_"), false);
    newtb.addToolButton("Mar�es", "PARAMETREMAREE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addSeparator();

    newtb.addToolButton("Cat.Nav", "PARAMETRECATEGORIE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addToolButton("Quais", "PARAMETREQUAI", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addToolButton("Chenal", "PARAMETRECHENAL", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addSeparator();
    newtb.addToolButton("Topo chenal", "TOPOLOGIECHENAL", FudaaResource.FUDAA.getIcon("graphe_"), false);
    newtb.addToolButton("Mod�lisation port", "MODELISATIONTOPOLOGIE", FudaaResource.FUDAA.getIcon("crystal_colorier"),
        false);
    newtb.addSeparator();
    newtb.addToolButton("Reg.Nav chenaux", "REGLESNAVIGATIONCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    newtb.addToolButton("Dur.Parcours chenaux", "REGLESDUREEPARCOURSCHENAL",
        FudaaResource.FUDAA.getIcon("configurer_"), false);

    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addSeparator();

    newtb.addToolButton("V�rif", "VERIFICATIONDONNEES", FudaaResource.FUDAA.getIcon("crystal_analyser"), false);
    newtb.addToolButton("Dup.simu", "DUPLIQUERSIMU", FudaaResource.FUDAA.getIcon("dupliquer_"), false);
    newtb.addToolButton("Calcul", "LANCEMENTCALCUL", FudaaResource.FUDAA.getIcon("crystal_executer"), false);
    newtb.addSeparator();
    newtb.addToolButton("Gen nav", "GRAPHEGENERATIONNAV", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Dur�e parcours", "GRAPHEDUREEPARCOURS", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Occupation globale quai", "TABLEAUOCCUPGLOBAL", FudaaResource.FUDAA.getIcon("crystal_graphe"),
        false);
    newtb.addToolButton("Attente trajet", "ATTENTESPECIALISEE", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Croisements", "CROISEMENTSCHENAUXTABLEAU", FudaaResource.FUDAA.getIcon("crystal_graphe"),
        false);

  }

  /**
   * Cr�� le menu donn�es g�n�rales
   * 
   * @param _app
   * 
   */
  protected BuMenu construitMenuDonneesGenerales(final boolean _app) {
    final BuMenu r = new BuMenu("G�n�ralit�s", "GENERALITES");
    BuMenuItem b=r.addMenuItem("donn�es g�n�rales", "DONNEESGENERALES", FudaaResource.FUDAA.getIcon("analyser"), false);
    b.setMnemonic(KeyEvent.VK_O);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    r.setMnemonic(KeyEvent.VK_G);
    r.getAccessibleContext().setAccessibleDescription(
            "Saisie des donn�es g�n�rales");
    
    return r;
  }

  /**
   * Cr�� le menu simulation Ce menu permet d'entrer les parametres de saisies de la simulation -dans le menu
   * simulation, on a la possibilit� de cliquer sur les sous menus parmares quai, ecluses,chenaux.
   */
  protected BuMenu construitMenuParametresConstruction(final boolean _app) {
    final BuMenu r = new BuMenu("Param�tres", "PARAMETRESSIMULATION");

    //-- modif AH --//
   BuMenuItem b= r.addMenuItem("Gares", "PARAMETREGARE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
   b.setMnemonic(KeyEvent.VK_G);
   b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
   
    b=r.addMenuItem("Bassins", "PARAMETREBASSIN", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_B);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Mar�es", "PARAMETREMAREE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_M);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Tron�ons (chenaux)", "PARAMETRECHENAL", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_T);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Cercles", "PARAMETRECERCLE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_C);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Ecluse", "PARAMETREECLUSE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_E);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Quais", "PARAMETREQUAI", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_Q);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Cat�gories de Navire", "PARAMETRECATEGORIE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_N);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("D�lais remplissage SAS", "REGLESREMPLISSAGEECLUSE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
   
    
    r.setMnemonic(KeyEvent.VK_P);
    r.getAccessibleContext().setAccessibleDescription(
            "Saisie des param�tres du port");
    
    


    return r;
  }

  protected BuMenu  construitMenuGeneration(final boolean _app) {
    final BuMenu r = new BuMenu("G�n�ration", "GENERATION");
     
    BuMenuItem b=r.addMenuItem("G�n�ration des navires", "GENARR", FudaaResource.FUDAA.getIcon(""), false);
   
    b=r.addMenuItem("Affichage des navires", "GENARR2", FudaaResource.FUDAA.getIcon(""), false);
      
    
    return r;
  }
  
  /**
   * Cr�ation du menu projet. Ce menu permet de lancer les calculs et d'exploiter les resultats retourne le menu de
   * construction du projet:
   */

  protected BuMenu construitMenuSimulation(final boolean _app) {
    final BuMenu r = new BuMenu("Simulation", "LANCEMENTSIMULATION");

   r.addSeparator("Projet");
    
    BuMenuItem b=r.addMenuItem("Rappel de donn�es", "RAPPELDONNEES", FudaaResource.FUDAA.getIcon(""), false);
    b.setMnemonic(KeyEvent.VK_R);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    
    b=r.addMenuItem("Dupliquer simulation", "DUPLIQUERSIMU", FudaaResource.FUDAA.getIcon("dupliquer_"), false);
    b.setMnemonic(KeyEvent.VK_I);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    r.addSeparator("Simulation");
    b=r.addMenuItem("V�rification", "VERIFICATIONDONNEES", FudaaResource.FUDAA.getIcon("crystal_analyser"), false);
    b.setMnemonic(KeyEvent.VK_V);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    
    b=r.addMenuItem("Calculer", "LANCEMENTCALCUL", FudaaResource.FUDAA.getIcon("crystal_executer"), false);
    b.setMnemonic(KeyEvent.VK_L);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    r.addSeparator("Historique");
    b= r.addMenuItem("Acc�s � l'interface historique", "AUTRE2",false);
    b.setMnemonic(KeyEvent.VK_H);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Suppression fichier historique", "AUTRE1", false);
    b.setMnemonic(KeyEvent.VK_F);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    
    
    r.setMnemonic(KeyEvent.VK_U);
    r.getAccessibleContext().setAccessibleDescription(
            "Menu Simulation");
    return r;
  }

  /**
   * Cr�ation du menu projet. Ce menu permet de lancer les calculs et d'exploiter les resultats retourne le menu de
   * construction du projet:
   */

  protected BuMenu construitMenuSimulation2(final boolean _app) {
    final BuMenu r = new BuMenu("Simulation", "SIMULATION");
    r.addMenuItem("Param�tres", "PARAMETRE", false);

    r.addMenuItem("Calculer", "CALCULER", FudaaResource.FUDAA.getIcon("crystal_calculer"), false);
    r.addMenuItem("Rappel des donn�es", "TEXTE", false);
    r.addMenuItem("Exploitation de la simulation", "TABLEAU", false);
    r.addMenuItem("Exploitation du projet", "GRAPHE", false);

    return r;
  }

  /**
   * Cr�ation du menu topologie. Ce menu permet de cr�er la topologie du port a parti des diff�rents �l�ments d�ja
   * inscrits Il est neccessaire d'avoir au moins 2 gares afin de pouvoir cr��er une topologie retourne le menu de
   * topologie
   */

  protected BuMenu construitMenuTopologie(final boolean _app) {
    final BuMenu r = new BuMenu("Topologie du port", "TOPOLOGIE");

    r.addSeparator("Topologie");
    BuMenuItem b=r.addMenuItem("Bassins", "TOPOLOGIEBASSIN", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_B);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    
    b=r.addMenuItem("Tron�ons (chenaux)", "TOPOLOGIECHENAL", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_T);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    
    b=r.addMenuItem("Ecluses", "TOPOLOGIEECLUSE", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_E);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    
    b=r.addMenuItem("Cercles d'�vitage", "TOPOLOGIECERCLE", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_C);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    
    r.addSeparator("Construction");
    b=r.addMenuItem("mod�lisation port", "MODELISATIONTOPOLOGIE", FudaaResource.FUDAA.getIcon("crystal_colorier"), false);
    b.setMnemonic(KeyEvent.VK_D);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.SHIFT_MASK));
    
    
    r.setMnemonic(KeyEvent.VK_E);
    r.getAccessibleContext().setAccessibleDescription(
            "Topologie du port");
    
    return r;
  }

  /**
   * Cr�ation du menu DES REGLES DE NAVIGATION. Ce menu permet de lancer les saisies des regles de navigations retourne
   * le menu de construction du projet:
   */

  protected BuMenu construitMenuReglesNavigations(final boolean _app) {
    final BuMenu r = new BuMenu("Navigation ", "REGLESNAVIGATION");
    final BuMenu r1 = new BuMenu("R�gles de croisements ", "NAVIGATIONCROISEMENT");
    final BuMenu r2 = new BuMenu("Dur�es de parcours ", "NAVIGATIONDUREES");
    final BuMenu r3 = new BuMenu(SiporConstantes.ConstanteTitreRapportHtm, SiporConstantes.ConstanteMenuGene);
    r1.addMenuItem("Chenaux", "REGLESNAVIGATIONCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r1.addMenuItem("Cercles", "REGLESNAVIGATIONCERCLE", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r2.addMenuItem("Chenaux", "REGLESDUREEPARCOURSCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r2.addMenuItem("Cercles", "REGLESDUREEPARCOURSCERCLE", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r3.addMenuItem(SiporConstantes.ConstanteTitreRapportHtm,SiporConstantes.constanteEVENTgenes, FudaaResource.FUDAA.getIcon("configurer_"), false);
    
    r.addSubMenu(r1, false);
    r.addSubMenu(r2, false);
    r.addSubMenu(r3, false);
     r.setMnemonic(KeyEvent.VK_I);
     r.getAccessibleContext().setAccessibleDescription(
            "Navigation");
    return r;
  }

  /**
   * Methode qui permet la cr�ation de resultats des parametres.
   * 
   * @param _app
   * @return le menu des resultats.
   */
  protected BuMenu construitMenuExploitation(final boolean _app) {
    final BuMenu r = new BuMenu("Resultats", "RESULTATSSIMU");

    // final BuMenu r3= new BuMenu("Durees de parcours ", "DURR");
    final BuMenu r4 = new BuMenu("Occupation des quais", "OCC");
    final BuMenu r5 = new BuMenu("Attentes", "ATT");
    final BuMenu r6 = new BuMenu("Croisements", "CR");

    r.addMenuItem("G�n�ration de navires", "GRAPHEGENERATIONNAV", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);

    r.addMenuItem("Historique", "HISTORIQUETABLEAU", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);

    r.addMenuItem("Dur�es de parcours", "GRAPHEDUREEPARCOURS", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);

    r4.addMenuItem("Occupations globales", "TABLEAUOCCUPGLOBAL", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);
    r4.addMenuItem("Occupations par quais", "TABLEAUOCCUPQUAI", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);

    r5.addMenuItem("Attentes par �l�ment", "ATTENTEGENERALESELEMENTTABLEAU", FudaaResource.FUDAA
        .getIcon("crystal_graphe"), false);
    r5.addMenuItem("Attentes par cat�gorie", "ATTENTEGENERALESCATEGORIETABLEAU", FudaaResource.FUDAA
        .getIcon("crystal_graphe"), false);
    r5.addMenuItem("Attentes par trajet", "ATTENTESPECIALISEE", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);

    r6.addMenuItem("Chenaux", "CROISEMENTSCHENAUXTABLEAU", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);
    r6.addMenuItem("Cercles", "CROISEMENTSCERCLESTABLEAU", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);

    r.addSubMenu(r4, false);
    r.addSubMenu(r5, false);
    r.addSubMenu(r6, false);
    
    r.setMnemonic(KeyEvent.VK_R);
    r.getAccessibleContext().setAccessibleDescription(
            "r�sultats de la simulation");

    return r;
  }

  protected BuMenu construitMenuAutre(final boolean _app) {

    final BuMenu r = new BuMenu("Autre", "AUTRE");
    
    return r;
  }

  protected BuMenu construitMenuComparaisonSimulations(final boolean _app) {
    final BuMenu r = new BuMenu("Comparaison", "COMPARESIMU");
    r.addMenuItem("G�n�ration de navires", "COMPARESIMU1", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r.addMenuItem("Occupation globale des quais", "COMPARESIMU2", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r.addMenuItem("Occupation d�taill�e des quais", "COMPARESIMU4", FudaaResource.FUDAA.getIcon("crystal_graphe"),
        false);
    r.addMenuItem("dur�e de parcours", "COMPARESIMU3", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    
    final BuMenu r1 = new BuMenu("Attentes", "ACOMPARE");
    r1.addMenuItem("Attentes �l�ments", "COMPARESIMU5", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r1.addMenuItem("Attentes trajet", "COMPARESIMU6", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r.addSubMenu(r1, true);
    
    
    r.setMnemonic(KeyEvent.VK_C);
    r.getAccessibleContext().setAccessibleDescription(
            "Comparaisons de simulation");
    
    return r;
  }

  protected BuMenu construitMenuProjets(final boolean _app) {
    final BuMenu menu = new BuMenu("Projet", "PROJET");
    menu.addMenuItem("Enregistrer", "ENREGISTRER_LISTE", false);
    menu.addMenuItem("Ouvrir", "OUVRIR_LISTE", true);
    return menu;
  }

  


  
  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    String action = _evt.getActionCommand();
    String fichier= SiporLib.actionArgumentStringOnly(action);
    action = SiporLib.actionStringWithoutArgument(action);
    /**
     * signal= ihm saisies parametres
     */

    if (action.equals("DONNEESGENERALES")) {
      // lancement de l'interface de saisie des donn�es generales
      parametreDonneesGenerales();
      // this.addInternalFrame(new SiporFrameSaisieDonneesGenerales(donnees_));
    }

    if (action.equals("PARAMETREQUAI")) {
      /**
       * Test necessaire: il faut qu il existe au moins un bassin:
       */

      if (this.donnees_.getListebassin_().getListeBassins_().size() > 0) {
        parametreQuais(); // signal sur quai => lance l application de saisie des quais
        assistant_.changeAttitude(BuAssistant.ATTENTE, "Je veux plein de quais!!\n" + getInformationsSoftware().name
            + " " + getInformationsSoftware().version);
      } else {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Erreur,Il faut au moins un bassin pour cr�er un quai!! ").activate();
      }

    } else if (action.equals("PARAMETREECLUSE")) {
      parametreEcluse(); // signal sur ecluse => lance l application de saisie des ecluses
      assistant_.changeAttitude(BuAssistant.PAROLE, "Mes ecluses... \nOu les ai-je mises???\n"
          + getInformationsSoftware().name + " " + getInformationsSoftware().version);
    } else if (action.equals("PARAMETRECERCLE")) {
      parametreCercle(); // signal sur cercle => lance l application de saisie des cercles
      assistant_.changeAttitude(BuAssistant.PAROLE, "Va y avoir du traffic!\n" + getInformationsSoftware().name + " "
          + getInformationsSoftware().version);
    } else if (action.equals("PARAMETREMAREE")) {
      parametreMaree(); // signal sur chenal => lance l application de saisie des cheneaux
      assistant_.changeAttitude(BuAssistant.PAROLE, "ah non il pas l'heure de\n se baigner....\n"
          + getInformationsSoftware().name + " " + getInformationsSoftware().version);
    } else if (action.equals("PARAMETRECHENAL")) {
      parametreChenal(); // signal sur chenal => lance l application de saisie des cheneaux
      assistant_.changeAttitude(BuAssistant.PAROLE, "Par ou je passe??\n" + getInformationsSoftware().name + " "
          + getInformationsSoftware().version);
    } else if (action.equals("PARAMETRECATEGORIE")) {
      if (this.donnees_.getListeGare_().getListeGares_().size() <= 0) {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Erreur, il faut au moins une gare de d�part pour cr�er un navire.").activate();
      } else if (this.donnees_.getlQuais_().getlQuais_().size() <= 0) {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Erreur, il faut au moins un quai pr�f�rentiel pour cr�er un navire.").activate();
      } else

      {
        parametreNavire(); // signal sur categorie de navire => lance l application de saisie des cat�gories
        assistant_.changeAttitude(BuAssistant.PAROLE, "Oh mon bateau!! tu es\n le plus beau de tous\n les bateaux!\n"
            + getInformationsSoftware().name + " " + getInformationsSoftware().version);
      }

    } else if (action.equals("PARAMETREBASSIN")) {
      // donnees.testHtml();
      parametreBassin();
      assistant_.changeAttitude(BuAssistant.PAROLE, "Qu'est ce qu'on est bien\n quand on est dans son bassin!!\n"
          + getInformationsSoftware().name + " " + getInformationsSoftware().version);

    } else if (action.equals("PARAMETREGARE")) {
      parametreGare(); // signal sur quai => lance l application de saisie des quais
      assistant_.changeAttitude(BuAssistant.PAROLE, "Il en faut, il en faut!!\n" + getInformationsSoftware().name + " "
          + getInformationsSoftware().version);
    } else
    /**
     * signal = ihm de topologie du port:
     */
    if (action.equals("TOPOLOGIEBASSIN")) {
      activerCommandesTopologie(1);

    } else if (action.equals("TOPOLOGIECHENAL")) {
      activerCommandesTopologie(3);
    } else if (action.equals("TOPOLOGIEECLUSE")) {
      activerCommandesTopologie(2);
    } else if (action.equals("TOPOLOGIECERCLE")) {
      activerCommandesTopologie(4);
    } else if (action.equals("MODELISATIONTOPOLOGIE")) {
     // activerModelisation();
    	activerNetworkEditor();
    } else if (action.equals("REGLESNAVIGATIONCHENAL")) {
      activerReglesNavigation();

    } else if (action.equals("REGLESNAVIGATIONCERCLE")) {
      activerReglesNavigation2();

    }else if(action.equals("GENARR")){
      lancementGenarr();
    }else if(action.equals("GENARR2")){
      new BuTaskOperation(this, "g�n�ration navires", "affichageGenarr").start();
      
    }else if (action.equals("VERIFICATIONDONNEES")) {
    
      activerVerficiationDonnees();
    } else if (action.equals("DUPLIQUERSIMU")) {

      donnees_.dupliquerSimulation();
    } else if (action.equals("REGLESDUREEPARCOURSCHENAL")) {
        activerReglesDuree();
      } else if (action.equals("REGLESREMPLISSAGEECLUSE")) {
          activerRemplissageSAS();
      } else if (action.equals("REGLESDUREEPARCOURSCERCLE")) {
      activerReglesDuree2();
    }else if (action.equals(SiporConstantes.constanteEVENTgenes)) {
      activerReglesGenesCercles();
      }
  
    else if (action.equals("RAPPELDONNEES")) {
      activationRappelDonnees();
    } else if (action.equals("LANCEMENTCALCUL")) {
      new BuTaskOperation(this, "Calcul", "lancementCalcul").start();
      // lancementCalcul();
    } else if (action.equals("GRAPHEGENERATIONNAV")) {
      new BuTaskOperation(this, "g�n�ration navires", "activerResultatsgenerationNavires").start();
      // activerResultatsgenerationNavires();
      // resultatsSimulations();//test graphes
    } else if (action.equals("HISTORIQUETABLEAU") ||action.equals("AUTRE2")) {
      new BuTaskOperation(this, "historique", "activerResultatHistorique").start();
      // activerResultatHistorique();
    } else if (action.equals("AUTRE1")) {
        new BuTaskOperation(this, "Suppression historique", "supprimerHistoriqueSimulation").start();
        // activerResultatDureesParcours();
      }
    else if (action.equals("GRAPHEDUREEPARCOURS")) {
      new BuTaskOperation(this, "dur�es parcours", "activerResultatDureesParcours").start();
      // activerResultatDureesParcours();
    } else if (action.equals("CROISEMENTSCHENAUXTABLEAU")) {
      new BuTaskOperation(this, "croisements chenaux", "activerResultatCroisementsChenaux").start();
      // activerResultatCroisementsChenaux();
    } else if (action.equals("CROISEMENTSCERCLESTABLEAU")) {
      new BuTaskOperation(this, "croisements cercles", "activerResultatCroisementsCercles").start();
      // activerResultatCroisementsCercles();
    } else if (action.equals("ATTENTEGENERALESELEMENTTABLEAU")) {
      new BuTaskOperation(this, "attentes �l�ments", "activerResultatAttentesGeneralesElement").start();
      // activerResultatAttentesGeneralesElement();
    } else if (action.equals("ATTENTEGENERALESCATEGORIETABLEAU")) {
      new BuTaskOperation(this, "attentes cat�gories", "activerResultatAttentesGeneralesCategorie").start();
      // activerResultatAttentesGeneralesCategorie();
    } else if (action.equals("ATTENTESPECIALISEE")) {
      new BuTaskOperation(this, "attentes trajet", "activerResultatAttentesSpecialisee").start();
      // activerResultatAttentesSpecialisee();
    } else if (action.equals("TABLEAUOCCUPGLOBAL")) {
      new BuTaskOperation(this, "occupation globale", "activerResultatOccupGlobales").start();
      // activerResultatOccupGlobales();
    } else if (action.equals("TABLEAUOCCUPQUAI")) {
      new BuTaskOperation(this, "occupation detaill�e", "activerResultatOccupQuai").start();
      // activerResultatOccupQuai();
    } else if (action.equals("COMPARESIMU1")) {
      new BuTaskOperation(this, "comparaison nb navires", "activerComparaisonNombreNavires").start();
    } else if (action.equals("COMPARESIMU2")) {
      new BuTaskOperation(this, "comparaison nb navires", "activerComparaisonOccupationGlobale").start();
    } else if (action.equals("COMPARESIMU3")) {
      new BuTaskOperation(this, "comparaison dur�e de parcours", "activerComparaisonDureesParcours").start();
    } else if (action.equals("COMPARESIMU4")) {
      new BuTaskOperation(this, "comparaison d'occupation aux quais", "activerComparaisonOccupationQuais").start();
    }else if (action.equals("COMPARESIMU5")) {
      new BuTaskOperation(this, "comparaison d'occupation aux quais", "activerComparaisonAttenteGenerale").start();
    }else if (action.equals("COMPARESIMU6")) {
      new BuTaskOperation(this, "comparaison d'occupation aux quais", "activerComparaisonAttenteTrajet").start();
    }
    else if (action.equals("CREER")) {
      creer();
    } else if (action.equals("CREER2")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir(null);
      setEnabledForAction("REOUVRIR", true);
    } else if (action.equals("OUVRIR2")) {
      ouvrir(null);
      setEnabledForAction("REOUVRIR", true);
    }else if (action.equals("REOUVRIR")){
      
      ouvrir(fichier);
      System.out.println("fichier reouvrir: "+fichier);
      
    }
    else if (action.equals("NETTOYER")) {
      fermerToutesLesInterfaces();
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();

    } else if (action.equals("FERMER")) {
      fermer();
    } else if (action.equals("QUITTER")) {
      // quitter();
      exit();
    }
    else if(_evt.getSource().equals(supprimerSimu_)){
      supprimerSimulation();
    }
    else  if(_evt.getSource().equals(ajouterSimu_)){
      ouvrir(null);
    }
      

    else if (action.equals("ENREGISTRER_LISTE")) {
      liste_.enregistre();
    } else if (action.equals("OUVRIR_LISTE")) {
      liste_.ouvrir();

    } else {
      super.actionPerformed(_evt);
    }
  }

  public void oprServeurCopie() {
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }

  /**
   * Retire une �ventuelle extension au nom du fichier du projet et lance la lecture du fichier de r�sultats brutes
   * correspondants.
   */
  public void recevoirResultats() {
    projet_.addResult(SiporResource.resultats, siporResults_.litResultatsSipor());
  }

  /**
   * Methode permettant d'effectuer le lancement de al simulation envoyer les parametres au server via la classe
   * SiporParams_
   */

  public void oprCalculer() {
    
    lancementCalcul();
    
  }

  /**
   * Commandes activ�es d�s qu'une simulation est charg�e. Les commandes sont activ�es apres chargmeent d un projet
   */
  public void activerCommandesSimulation() {
    setEnabledForAction("PARAMETRE", true);
    setEnabledForAction("TEXTE", true);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("ENREGISTRER_LISTE", true);

    /**
     * Activation des donn�es generales
     */
    setEnabledForAction("DONNEESGENERALES", true);
    /**
     * Activation des menu de saisies des donn�es de la simulation
     */
    setEnabledForAction("PARAMETREQUAI", true);
    setEnabledForAction("PARAMETREECLUSE", true);
    setEnabledForAction("PARAMETREBASSIN", true);
    setEnabledForAction("PARAMETREGARE", true);
    setEnabledForAction("PARAMETRECERCLE", true);
    setEnabledForAction("PARAMETRECATEGORIE", true);
    setEnabledForAction("PARAMETRECHENAL", true);
    setEnabledForAction("PARAMETREMAREE", true);

    /**
     * Activation des menu de saisies de la topologie du porc grouink
     */
    setEnabledForAction("TOPOLOGIEBASSIN", true);
    setEnabledForAction("TOPOLOGIEECLUSE", true);
    setEnabledForAction("TOPOLOGIECHENAL", true);
    setEnabledForAction("TOPOLOGIECERCLE", true);

    /**
     * Activation du menu de modelisation du port:
     */
    setEnabledForAction("MODELISATIONTOPOLOGIE", true);

    /**
     * Activation du menu regles de navigations
     */
    setEnabledForAction("NAVIGATIONCROISEMENT", true);
    setEnabledForAction("NAVIGATIONDUREES", true);

    setEnabledForAction("REGLESNAVIGATION", true);
    setEnabledForAction("REGLESNAVIGATIONCHENAL", true);
    setEnabledForAction("REGLESNAVIGATIONCERCLE", true);
    setEnabledForAction("REGLESDUREEPARCOURSCHENAL", true);
    setEnabledForAction("REGLESREMPLISSAGEECLUSE", true);
    
    setEnabledForAction("REGLESDUREEPARCOURSCERCLE", true);
    setEnabledForAction(SiporConstantes.ConstanteMenuGene, true);
    setEnabledForAction(SiporConstantes.constanteEVENTgenes, true);
   
     
    /**
     * Activation du menu simulation:
     */
    setEnabledForAction("VERIFICATIONDONNEES", true);
    setEnabledForAction("DUPLIQUERSIMU", true);
    setEnabledForAction("NETTOYER", true);

    //-- Menu g�n�ration de navires --//
    setEnabledForAction("GENARR", true);
    setEnabledForAction("GENARR2", true);
    
    // menu resultats
    activerCommandesResultatsSimulation();
  }

  void activerCommandesResultatsSimulation() {

    if (this.donnees_.getParams_().niveau >= 1) {
      setEnabledForAction("RAPPELDONNEES", true);
    }

    // menu calcul
    if (this.donnees_.getParams_().niveau >= 1) {
      setEnabledForAction("LANCEMENTCALCUL", true);
      
      
      
      
    }

    // menu resultats
    if (this.donnees_.getParams_().niveau >= 2) {
      
      setEnabledForAction("AUTRE1", true);
      setEnabledForAction("AUTRE2", true);
      setEnabledForAction("GRAPHEGENERATIONNAV", true);
      setEnabledForAction("HISTORIQUETABLEAU", true);
      setEnabledForAction("GRAPHEDUREEPARCOURS", true);

      setEnabledForAction("ATTENTEGENERALESELEMENTTABLEAU", true);
      setEnabledForAction("ATTENTEGENERALESCATEGORIETABLEAU", true);
      setEnabledForAction("ATTENTESPECIALISEE", true);

      setEnabledForAction("TABLEAUOCCUPGLOBAL", true);
      setEnabledForAction("TABLEAUOCCUPQUAI", true);

      setEnabledForAction("CROISEMENTSCHENAUXTABLEAU", true);
      setEnabledForAction("CROISEMENTSCERCLESTABLEAU", true);

      setEnabledForAction("RESULTATSSIMU", true);
      setEnabledForAction("GN", true);
      setEnabledForAction("HIST", true);
      setEnabledForAction("DURR", true);
      setEnabledForAction("OCC", true);
      setEnabledForAction("ATT", true);
      setEnabledForAction("CR", true);

      // activer els comparaisons de simulation
      setEnabledForAction("COMPARESIMU1", true);
      setEnabledForAction("COMPARESIMU2", true);
      setEnabledForAction("COMPARESIMU3", true);
      setEnabledForAction("COMPARESIMU4", true);
      setEnabledForAction("COMPARESIMU5", true);
      setEnabledForAction("COMPARESIMU6", true);
    }

  }

  /**
   * M�thode qui active la saisie des topologie des que les conditions suivantes sont remplies: - au moins 2 gares
   * saisies par l'utilisateur dans la partie donn�e - voir avec Alain P et C pour d'autre r�gles d'activation de la
   * siaie de la topologie Les commandes sont activ�es des que les conditions ci dessus sont remplies Fonctionnement de
   * la methode:
   * 
   * @param numeroParametreConsidere un entier en parametre d'entr�e qui indique de quel �l�ment de saisie il s'agit
   *          exemple: 1 => bassin 2 => Ecluse......
   */
  protected void activerCommandesTopologie(final int numeroParametreConsidere) {

    if (numeroParametreConsidere == 1) {
      if (donnees_.getListeGare_().getListeGares_().size() >= 1) {

        /**
         * Test d activation des bassins si au moins un bassin existe
         */
        if (numeroParametreConsidere == 1 && donnees_.getListebassin_().getListeBassins_().size() >= 1) {
          this.addInternalFrame(new SiporPanelTopologieBassin(donnees_));

        } else {
          new BuDialogError(getApp(), getInformationsSoftware(), "Erreur, aucun bassin existant.").activate();
        }

      }// fin du "si il n existe aps au moins une gare"
      else {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Il faut au moins une gare pour la topologie du bassin. Veuillez d'abord cr�er une gare.").activate();

        return;
      }
    }// fin du if cas pour la saisie des topoplogie bassin

    if (numeroParametreConsidere != 1) {
      if (donnees_.getListeGare_().getListeGares_().size() >= 2) {

        /**
         * Test d activation des ecluses si au moins une ecluse existe
         */
        if (numeroParametreConsidere == 2) {
          if (donnees_.getListeEcluse_().getListeEcluses_().size() >= 1) {
            this.addInternalFrame(new SiporPanelTopologieEcluse(donnees_));
        

          } else {
            new BuDialogError(getApp(), getInformationsSoftware(), "Erreur, aucune ecluse existante.").activate();
          }
        }

        /**
         * Test d activation des cheneaux si au moins un chenal existe
         */
        if (numeroParametreConsidere == 3) {
          if (donnees_.getListeChenal_().getListeChenaux_().size() >= 1) {
            this.addInternalFrame(new SiporPanelTopologieChenal(donnees_));
          } else {
            new BuDialogError(getApp(), getInformationsSoftware(), "Erreur, aucun chenal existant. ").activate();
          }
        }

        /**
         * Test d activation des cercles si au moins un cercle existe
         */
        if (numeroParametreConsidere == 4) {
          if (donnees_.getListeCercle_().getListeCercles_().size() >= 1) {
            this.addInternalFrame(new SiporPanelTopologieCercle(donnees_));
           
          } else {
            new BuDialogError(getApp(), getInformationsSoftware(), "Erreur, aucun cercle existant.").activate();
          }
        }

      }// fin du if si au moins 2 gares ont �t� saisies
      else {
        /**
         * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
         */
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Il faut au moins deux gares pour la topologie. Veuillez d'abord cr�er suffisamment de gares.").activate();

      }

    }// fin du if si l on est dans le cas d une topologie pour les �l�ments differents du bassin

  }
  
  
  
  public void updateNetworkData() {
	  if(gestionChenaux_ != null)
		  gestionChenaux_.getAffichagePanel_().maj(donnees_);
	  if(gestionEcluses_ != null) 
		  gestionEcluses_.affichagePanel_.maj(donnees_);
	  if(gestionBassins_ != null) 
		  gestionBassins_.affichagePanel.maj(donnees_);
	   if(gestionGares_ != null) 
		   gestionGares_.affichagePanel_.maj(donnees_);				  
	   if(gestionCercles_ != null) 
		   gestionCercles_.affichagePanel_.maj(donnees_);
		
  }
  
  SiporInternalFrame networkInternalFrame = null ;
  
  public void activerNetworkEditor() {
	  
	  //-- create frame --//
	  if(networkInternalFrame == null) {
		  networkInternalFrame = new SiporInternalFrame("Mod�lisation du port",true,true,true,true);
		  networkInternalFrame.getContentPane().add(networkEditor);
		  //networkInternalFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  networkInternalFrame.setJMenuBar(new EditorMenuBar(networkEditor));
		  networkInternalFrame.setSize(900, 700);
		  JButton boutonSynchro = new JButtonNoFocus(networkEditor.bind("Re-synchronizer avec Sipor params", new SiporSyncrhonizeNetworkAction(networkEditor,this.donnees_),
					"/org/fudaa/ebli/network/simulationNetwork/images/connect.gif"));
		  boutonSynchro.setText("Re-synchronizer avec Sipor params");
		  networkEditor.getToolBar().add(boutonSynchro );
		  
		//-- try to load the graph if exist. project name + .network extension --//
		  File networkFile = new File(this.donnees_.getProjet_().getFichier() + ".network");
		  if(networkFile.exists()) {
			try {
				networkEditor.loadGraph(networkFile);
			} catch (IOException e) {
				FuLog.error(e);
				 new BuDialogError(getApp(), getInformationsSoftware(),
				          "Erreur lors du chargement du graphe. le fichier est corrompu.").activate();
			}
		  } else {
			  //-- clean the network no matter --//
			  networkEditor.cleanAllNetwork();
		  }
		  
	  }
	  addInternalFrame(networkInternalFrame);
	  
	  /*JInternalFrame frame =  networkEditor.createInternalFrame(new EditorMenuBar(networkEditor));
	  */
	  
	  
	  
	  
  }
  
  

  /**
   * Methode qui r�alise els controles et lance la fenetre de modelisation des ports
   */
@Deprecated
  public void activerModelisation() {
    if (donnees_.getListeGare_().getListeGares_().size() >= 2) {

      /**
       * ajout d'une fenetre interne
       */
      if (this.gestionModelisation_ == null) {
        FuLog.debug("interface nulle");
        gestionModelisation_ = new SiporDessinerPortFrame(this.donnees_, this);
        // System.out.println("55555");
        gestionModelisation_.setVisible(true);
        addInternalFrame(gestionModelisation_);
      } else {
        FuLog.debug("interface ferm�e");
        if (gestionModelisation_.isClosed()) {
          addInternalFrame(gestionModelisation_);

        } else {
          FuLog.debug("interface cas de figur restant autre que null et fermeture");
          activateInternalFrame(gestionModelisation_);
          addInternalFrame(gestionModelisation_);

        }

      }
      // this.addInternalFrame(new SiporDessinerPortFrame(donnees_,this));
    } else {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins deux gares pour dessiner la topologie. Veuillez d'abord cr�er suffisamment de gares.")
          .activate();

    }

  }

  /**
   * Methode qui r�alise les controles et lance la fenetre de saisies des regles de navigation.
   */
  protected void activerReglesNavigation() {
    /**
     * une patate de controles et de test doit etre effectu� afin de determiner si: 1) les tableau poru els cheneaux ont
     * d�ja �t� saisis ou non 2) plus de possibilit� de modifier les cat�gories de navires
     */

    // 1) demander confirmation de lancer cette fenetre en sachant que la saisie des navires sera bloqu�e
    // 2)lancer l'application
    if (this.donnees_.getListeChenal_().getListeChenaux_().size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins un chenal pour lancer les r�gles de navigation. Veuillez d'abord cr�er un chenal.")
          .activate();

    } else if (this.donnees_.getCategoriesNavires_().getListeNavires_().size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une cat�gorie de navire pour lancer les r�gles de navigation. Veuillez d'abord cr�er une cat�gorie.")
          .activate();
    } else

    {
      
      gestionNavigChenal_ = new SiporPanelReglesNavigationChenal(this.donnees_);
      addInternalFrame(gestionNavigChenal_);

    }

  }

  /**
   * Methode de lancement des regles de navigations du cercle.
   */
  protected void activerReglesNavigation2() {
    if (this.donnees_.getListeCercle_().getListeCercles_().size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins un cercle pour lancer les r�gles de navigation. Veuillez d'abord cr�er un cercle.")
          .activate();

    } else if (this.donnees_.getCategoriesNavires_().getListeNavires_().size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une cat�gorie de navire pour lancer les r�gles de navigation. Veuillez d'abord cr�er une cat�gorie.")
          .activate();
    } else

    {


      this.addInternalFrame(new SiporPanelReglesNavigationCercle(donnees_));
    }
  }

  /**
   * Methode de lancement des durees de parcours des cheneaux
   */
  protected void activerReglesDuree() {

    if (this.donnees_.getListeChenal_().getListeChenaux_().size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins un chenal pour lancer les r�gles de navigation. Veuillez d'abord cr�er un chenal")
          .activate();

    } else if (this.donnees_.getCategoriesNavires_().getListeNavires_().size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une cat�gorie de navire pour lancer les r�gles de navigation. Veuillez d'abord cr�er une cat�gorie")
          .activate();
    } else

    {
      this.addInternalFrame(new SiporPanelReglesParcoursChenal(donnees_));
    }
  }
  
  /**
   * Methode de lancement des remplissage des SAS
   */
  protected void activerRemplissageSAS() {

    if (this.donnees_.getListeEcluse_().getListeEcluses_().size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une �cluse pour lancer les r�gles de remplissage de SAS. Veuillez d'abord cr�er une �cluse")
          .activate();

    } else if (this.donnees_.getCategoriesNavires_().getListeNavires_().size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une cat�gorie de navire pour lancer les r�gles de remplissage de SAS. Veuillez d'abord cr�er une cat�gorie")
          .activate();
    } else

    {
      this.addInternalFrame(new SiporPanelRemplissageSAS(donnees_));
    }
  }

  
  private boolean testEnoughCircle(){
    if (this.donnees_.getListeCercle_().getListeCercles_().size() <= 0)

      {
        /**
         * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
         */
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Il faut au moins un cercle pour lancer les r�gles de navigation. Veuillez d'abord cr�er un cercle")
            .activate();
        return false;
      }
    return true;
  }
  private boolean testEnoughShip(){
    if (this.donnees_.getCategoriesNavires_().getListeNavires_().size() <= 0) {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Il faut au moins une cat�gorie de navire pour lancer les r�gles de navigation! Veuillez d'abord cr�er une cat�gorie.")
            .activate();
        return false;
      }
    return true;
  }
  /**
   * Methode de lancement des durees de parcours des cercles
   */
  protected void activerReglesDuree2() {

    if (testEnoughCircle() && testEnoughShip()) 
    this.addInternalFrame(new SiporPanelReglesParcoursCercle(donnees_));
    
  }
  /**
   * Methode de lancement des genes des cercles
   */
  protected void activerReglesGenesCercles() {

    if (testEnoughCircle() && testEnoughShip()) 
    this.addInternalFrame(new SiporPanelReglesGenesCercle(donnees_));
    
  }
  /**
   * Methode tres importante de verification des donn�es avant lancement de la simulation;
   */
  protected void activerVerficiationDonnees() {
    if (this.donnees_.verificationDonnees()) {
      // passage au niveau1: possibilit� de lancer la simulation
      if (this.donnees_.getParams_().niveau < 1) {
        this.donnees_.getParams_().niveau = 1;
      }
      // mise a jour des boutons accessibles pour la simu:
      activerCommandesResultatsSimulation();

    }
  }
  
  
  /**
   * Methode qui place le niveau de s�curite de l'application au plus bas.
   * Consequence: on doit redemarrer le test de v�rification des coh�rences des donn�es.
   * Utiliser: on utilise cette m�thode dans le cas ou l'on modifie des donn�es.
   *
   */
  public void baisserNiveauSecurite(){
    this.donnees_.getParams_().niveau=0;
   
    
        setEnabledForAction("RAPPELDONNEES", false);
        setEnabledForAction("LANCEMENTCALCUL", false);
        setEnabledForAction("AUTRE1", false);
        setEnabledForAction("AUTRE2", false);
        setEnabledForAction("GRAPHEGENERATIONNAV", false);
        setEnabledForAction("HISTORIQUETABLEAU", false);
        setEnabledForAction("GRAPHEDUREEPARCOURS", false);

        setEnabledForAction("ATTENTEGENERALESELEMENTTABLEAU", false);
        setEnabledForAction("ATTENTEGENERALESCATEGORIETABLEAU", false);
        setEnabledForAction("ATTENTESPECIALISEE", false);

        setEnabledForAction("TABLEAUOCCUPGLOBAL", false);
        setEnabledForAction("TABLEAUOCCUPQUAI", false);

        setEnabledForAction("CROISEMENTSCHENAUXTABLEAU", false);
        setEnabledForAction("CROISEMENTSCERCLESTABLEAU", false);

        //setEnabledForAction("RESULTATSSIMU", false);
        setEnabledForAction("GN", false);
        setEnabledForAction("HIST", false);
        setEnabledForAction("DURR", false);
        setEnabledForAction("OCC", false);
        setEnabledForAction("ATT", false);
        setEnabledForAction("CR", false);

        // activer els comparaisons de simulation
        setEnabledForAction("COMPARESIMU1", false);
        setEnabledForAction("COMPARESIMU2", false);
        setEnabledForAction("COMPARESIMU3", false);
        setEnabledForAction("COMPARESIMU4", false);
        setEnabledForAction("COMPARESIMU5", false);
        setEnabledForAction("COMPARESIMU6", false);
      

    
  }
  

  /**
   * methode de lancement de calculs de sipor
   */
  public void lancementCalcul() {

    enregistrer();
    
    //-- creation d'une fenetre d'affichage d e la progression --//
    SiporProgressFrame fenetreProgression=new SiporProgressFrame();
    this.addInternalFrame(fenetreProgression);
    
    final BuMainPanel mp = getMainPanel();
    mp.setProgression(20);

    
    
    // ETAPE 1: ecriture des diff�rents fichiers de donn�es pour le noyau de clacul:

    mp.setMessage("Envoie des param�tres au noyau de calcul");
    mp.setProgression(10);

    try {
      fenetreProgression.miseAjourBarreProgression(0, "Ecriture des param�tres...", "Ecriture des param�tres cat�gories de navire");
      
        //cat�gories
      DParametresSipor.ecritDonneesCategoriesNavires(this.donnees_.getParams_().navires,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(2, "Ecriture des param�tres donn�es g�n�rales");
      // donn�es generales
      DParametresSipor
      .ecritDonneesGenerales(this.donnees_.getParams_().donneesGenerales,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(4, "Ecriture des param�tres �cluses");
      // ecluses
      DParametresSipor.ecritDonneesEcluses(this.donnees_.getParams_().ecluses,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(6, "Ecriture des param�tres bassins");
      // bassins
      DParametresSipor.ecritDonneesBassins(this.donnees_.getParams_().bassins,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(8, "Ecriture des param�tres gares");
      // gares
      DParametresSipor.ecritDonneesGares(this.donnees_.getParams_().gares,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(10, "Ecriture des param�tres mar�e");
      // mar�es
      DParametresSipor.ecritDonneesMarees(this.donnees_.getParams_().maree,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(12, "Ecriture des param�tres quais");
      // quais
      DParametresSipor.ecritDonneesQuais(this.donnees_.getParams_().quais,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(14, "Ecriture des param�tres cercles d'�vitage");
      // cercles
      DParametresSipor.ecritDonneesCercles(this.donnees_.getParams_().cercles,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(16, "Ecriture des param�tres chenaux");
      // chenal
      DParametresSipor.ecritDonneesChenal(this.donnees_.getParams_().cheneaux,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(18, "Ecriture des dur�es de parcours chenaux");

      // durees de parcours des chenaux
      DParametresSipor.ecritDonneesDureeParcoursChenal(this.donnees_.getParams_(),this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(20, "Ecriture des dur�es de parcours cercles d'�vitage");

      // durees de parcours des cercles
      DParametresSipor.ecritDonneesDureeParcoursCercle(this.donnees_.getParams_(),this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(22, "Ecriture des croisements cercles");

      // dur�es de croisement cercle
      DParametresSipor.ecritDonneesCroisementsCercle(this.donnees_.getParams_().cercles,this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(24, "Ecriture des croisements chenaux");

      
      // d�lais remplissage SAS ecluse.
      DParametresSipor.ecritDonneesRemplissageSAS(this.donnees_.getParams_(), this.donnees_.getProjet_().getFichier());
      fenetreProgression.miseAjourBarreProgression(24, "Ecriture des d�lais de remplissage des SAS �cluses");
      
      // dur�es de croisement des chenaux:
      DParametresSipor
      .ecritDonneesCroisementsChenal(this.donnees_.getParams_().cheneaux,this.donnees_.getProjet_().getFichier());
      
    } catch (final Exception ex) {

    }

    //-- ETAPE 2: lancement calcul noyau --//
    mp.setMessage("Execution du calcul...");
    mp.setProgression(25);
    fenetreProgression.miseAjourBarreProgression(25, "Lancement du noyau de calcul...", "");
  
    File fic=new File(donnees_.getProjet_().getFichier()+".his");
    
    
    if(this.MOCK_ENABLE) {
    	SiporMock mock = new SiporMock();
    	mock.mockGeneral(donnees_, donnees_.getProjet_().getFichier());
    } else {
		    
		    if(fic.exists()){
		      int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
		          SiporImplementation.INFORMATION_SOFT, " Un fichier historique existe.\n Ce fichier contient les r�sultats d'une pr�c�dente ex�cution \n du noyau de calcul.\n Voulez-vous relancer le noyau de calcul?\n Si vous voulez directement lire le fichier historique,\n tapez non.").activate();
		
		      if (confirmation == 0) {
		
		        if(!ExecuterCommandeDepuisServeur(1,donnees_.getProjet_().getFichier()))
		          return ;//si la procedure retourne une valeure false alors il y a eu erreur, par cons�quent on quitte.
		      }
		    }else
		    {
		      //-- on execute obligatoirement le noyau de calcul car aucun fichier historique trouv�... --//
		      if(!ExecuterCommandeDepuisServeur(1,donnees_.getProjet_().getFichier()))
		      return ;//si la procedure retourne une valeure false alors il y a eu erreur, par cons�quent on quitte.
		    }
		      
    }
		    
		    //-- ETAPE 2.5: Verification que le fichier historique a bien �t� cr�� --//
		        fenetreProgression.miseAjourBarreProgression(60, "V�rification de la g�n�ration...", "");
		     
		        
		        File fichier=new File(donnees_.getProjet_().getFichier()+".his");
		    if(!fichier.exists()){
		      new BuDialogError(null,SiporImplementation.INFORMATION_SOFT,"Le fichier historique est introuvable.\n Veuillez relancer le calcul (onglet Simulation, 'Calculer')").activate();
		      return;
		    }
		    
		    
		  //-- ETAPE 3: Exploitation des resultats --//
		    mp.setMessage("Fin calcul du noyau, exploitation des r�sultats.");
		    mp.setProgression(65);
		    fenetreProgression.miseAjourBarreProgression(65, "Lecture des r�sultats...", "");
		  
		    // lecture du ficheir resultats:
		    this.donnees_.setListeResultatsSimu_( DResultatsSipor.litResultatsSipor(this.donnees_.getProjet_().getFichier() + ".his"));
    
    
    fenetreProgression.miseAjourBarreProgression(70, "Exploitation des r�sultats...", "");
  calculExploitationResultats(fenetreProgression);

    new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
        "La simulation est termin�e, vous pouvez visualiser les r�sultats.\n Un fichier historique \n\n"+donnees_.getProjet_().getFichier()+".his \n\n a �t� cr��. \n Il a servi � r�aliser les statistiques.\n Attention ce fichier occupe beaucoup de place m�moire. \n Vous pouvez le supprimer (onglet Simulation).").activate();


    fenetreProgression.terminaison();
    
    
  }

  /**
   * Methode qui realise les calculs de toutes les interfaces de SIPOR.
   */
  public void calculExploitationResultats(SiporProgressFrame fenetreProgression) {

    final BuMainPanel mp = getMainPanel();

    this.donnees_.getParams_().ResultatsCompletsSimulation = new SParametresResultatsCompletSimulation();

    fenetreProgression.miseAjourBarreProgression(70, "exploitation des g�n�rations de navires.");
  mp.setMessage("exploitation des g�n�rations de navires.");
    mp.setProgression(70);
    // Etape 1: calcul des generations de navires:
    SiporAlgorithmeGenerationBateaux.calcul(donnees_);

    fenetreProgression.miseAjourBarreProgression(75, "calcul des g�n�rations des quais globaux.");
    mp.setMessage("calcul des generation des quais globaux.");
    mp.setProgression(75);
    // etape 2: calcul des occupations globales des quais.
    SiporAlgorithmeOccupGlobale.calcul(this.donnees_);

    fenetreProgression.miseAjourBarreProgression(80, "calcul des quais d�taill�s.");
    mp.setMessage("calcul des quais d�taill�s");
    mp.setProgression(80);
    // etape3: calucl des occupation par quais
    SiporAlgorithmeOccupationsQuais.calcul(donnees_);

    fenetreProgression.miseAjourBarreProgression(85, "calcul des attentes g�n�rales par �l�ments et cat�gories.");
    mp.setMessage("calcul des attentes g�n�rales par elements et cat�gories");
    mp.setProgression(85);
    // etape 4: calcul des attentes g�n�rales par �l�ments et par cat�gories
    SiporAlgorithmeAttentesGenerales.CalculApresSImu(donnees_);

    fenetreProgression.miseAjourBarreProgression(90, "calcul des dur�es de parcours.");
    mp.setMessage("calcul des dur�es de parcours");
    mp.setProgression(90);
    // etape 5: Calcul de toutes les dur�es de parcours.
    SiporAlgorithmeTOUTESDureesParcours.calcul(this.donnees_);

    fenetreProgression.miseAjourBarreProgression(95, "calcul des attentes par trajet.");
    mp.setMessage("calcul des attentes par trajet");
    mp.setProgression(95);
    // etape 6: calcul de la totalit� des attentes trajet
    SiporAlgorithmeTOUTESAttentesTrajet.calcul(donnees_);

    // enregistrement des r�sultats de la simulation
    donnees_.enregistrer();

    fenetreProgression.miseAjourBarreProgression(100, "simulation et exploitation termin�es.","");
    mp.setMessage("simulation et exploitation termin�es.");
    mp.setProgression(100);

    // on place le niveau a 2 puisque les donn�es ont �t� calcul�es.
    this.donnees_.getParams_().niveau = 2;
    // apres execution du calcul passage au niveau 2 pour acc�der aux r�sultats et activation des r�sultats
    activerCommandesResultatsSimulation();

  }

  // Change le projet en cours.
  protected void changerProjet(final String nomProjet) {
    projet_ = (FudaaProjet) projets_.get(nomProjet);
  }

  protected void creer() {

    this.donnees_.creer();
  
  }

  protected void ouvrir(String fichier) {

    donnees_.ouvrir(fichier);


  }

  protected void enregistrer() {

    // enregistrement des donn�es via la methode datasimualtion
    if (donnees_.getProjet_() != null) {
      donnees_.enregistrer();
     
    }
  }// fin methode enregistrer

  // Enregistrement du rappel des donn�es sous un nom choisi par l'utilisateur
  protected void enregistrerRappelDonneesSous() {
    final String[] ext = { "html", "htm" };
    final String nomFichier = MethodesUtiles.choisirFichierEnregistrement(ext, (Component) getApp());
    if (nomFichier != null) {
      MethodesUtiles.enregistrer(nomFichier, fRappelDonnees_.getHtmlSource(), getApp());
      fRappelDonnees_.putClientProperty("NomFichier", nomFichier);
    }
  }

  protected void enregistrerSous() {

    this.donnees_.enregistrerSous();
    
  }// fin methode enregistresous

  /**
   * Methode de fermeture du projet 2 methodes � g�rer: cas 1: aucun projet charg� cas 2: un projet charg�
   */
  public void fermer() {

    // cas 1: projet inexistant:
    if (this.donnees_.getProjet_() == null) {
      return;
    }

    // cas 2: projet existant:
    final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), getInformationsSoftware(),
        "Voulez-vous enregistrer la simulation\n" + this.donnees_.getProjet_().getFichier());
    if (c.activate() == JOptionPane.YES_OPTION) {
      enregistrer();
    }

    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("TEXTE", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("PARAMETRE", false);
    activeActionsExploitation();
    setTitle("Sipor");
  }// fni methode fermer

  protected void quitter() {
    // si un projet pr�sent
    final Object[] liste = projets_.keySet().toArray();
    if (liste.length != 0) {
      // demande si enregistrer le projet
      final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), getInformationsSoftware(),
          "Voulez-vous enregistrer le projet actuel\n");
      if (c.activate() == JOptionPane.YES_OPTION) {
        liste_.enregistre();
      }
      // Ferme les simulations ouvertes.
      for (int i = 0; i < liste.length; i++) {
        changerProjet((String) liste[i]);
        fermer();
      }
    }
    exit();// permet de fermer la fenetre???
  }

  protected void importerProjet() {
  
  }

  protected void importer(final String _vagId) {
  }

  /** Cr�ation ou affichage de la fentre pour les pr�f�rences. */
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new SiporPreferencesPanel());
    _prefs.add(new FudaaStartupExitPreferencesPanel(true));
    _prefs.add(new BuLookPreferencesPanel(this));
  }

  /**
   * ************************************************************************* METHODE DE DECLENCHEMENT DES FENETRES DE
   * SAISIES DE DONNEES *************************************************************************
   */

  /**
   * Methode de creation d'une fenetre interne de saisie des donnees generales dans l application principale
   */

  void parametreDonneesGenerales() {
    if (gestionDonneesGenerales_ == null) {
      FuLog.debug("interface nulle");
      gestionDonneesGenerales_ = new SiporFrameSaisieDonneesGenerales(this.donnees_);

      gestionDonneesGenerales_.setVisible(true);
      addInternalFrame(gestionDonneesGenerales_);
    } else {
      FuLog.debug("interface r�duite");// il ne faut pas la recr��er
      if (gestionDonneesGenerales_.isClosed()) {
        addInternalFrame(gestionDonneesGenerales_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionDonneesGenerales_);
        addInternalFrame(gestionDonneesGenerales_);

      }

    }
    
    
  }

  /**
   * Methode de creation d'une fenetre interne de saisie des quais dans l application principale
   */

  void parametreQuais() {
    if (gestionQuais_ == null) {
      FuLog.debug("interface nulle");
      gestionQuais_ = new SiporVisualiserQuais(this.donnees_);
      // System.out.println("55555");
      gestionQuais_.setVisible(true);
      addInternalFrame(gestionQuais_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionQuais_.isClosed()) {
        gestionQuais_.dispose();
        gestionQuais_ = new SiporVisualiserQuais(this.donnees_);
        addInternalFrame(gestionQuais_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionQuais_);
        gestionQuais_.dispose();
        gestionQuais_ = new SiporVisualiserQuais(this.donnees_);
        addInternalFrame(gestionQuais_);

      }
    }
    
  }

  /**
   * Methode de cration d une fenetre interne de saisie d'Ecluses dans l application principale
   */

  public void parametreEcluse() {
    if (gestionEcluses_ == null) {
      FuLog.debug("interface nulle");
      gestionEcluses_ = new SiporVisualiserEcluses(this.donnees_);
      // System.out.println("55555");
      gestionEcluses_.setVisible(true);
      addInternalFrame(gestionEcluses_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionEcluses_.isClosed()) {
        gestionEcluses_.dispose();
        gestionEcluses_ = new SiporVisualiserEcluses(this.donnees_);
        addInternalFrame(gestionEcluses_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionEcluses_);
        gestionEcluses_.dispose();
        gestionEcluses_ = new SiporVisualiserEcluses(this.donnees_);
        addInternalFrame(gestionEcluses_);

      }

    }
    

    // addInternalFrame(new SiporVisualiserEcluses(donnees_));
  }

  /**
   * Methode de cration d une fenetre interne de saisie de bassins dans l application principale
   */

  void parametreBassin() {
    if (gestionBassins_ == null) {
      FuLog.debug("interface nulle");
      gestionBassins_ = new SiporVisualiserBassins(this.donnees_);
      // System.out.println("55555");
      gestionBassins_.setVisible(true);

      addInternalFrame(gestionBassins_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionBassins_.isClosed()) {
        addInternalFrame(gestionBassins_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionBassins_);
        addInternalFrame(gestionBassins_);

      }

    }

    
    // addInternalFrame(new SiporVisualiserBassins(donnees_));
  }

  /**
   * Methode de cration d une fenetre interne de saisie de gares dans l application principale
   */

  void parametreGare() {
    if (gestionGares_ == null) {
      FuLog.debug("interface nulle");
      gestionGares_ = new SiporVisualiserGares(this.donnees_);
      // System.out.println("55555");
      gestionGares_.setVisible(true);
      gestionGares_.affichagePanel_.maj(donnees_);
      addInternalFrame(gestionGares_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionGares_.isClosed()) {
        gestionGares_.affichagePanel_.maj(donnees_);
        addInternalFrame(gestionGares_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        gestionGares_.affichagePanel_.maj(donnees_);
        activateInternalFrame(gestionGares_);
        addInternalFrame(gestionGares_);

      }

    }
    // addInternalFrame(new SiporVisualiserGares(donnees_));
  }

  /**
   * Methode de cration d une fenetre interne de saisie de cheneaux dans l application principale
   */

  public void parametreChenal() {

    if (gestionChenaux_ == null) {
      FuLog.debug("interface nulle");
      gestionChenaux_ = new SiporVisualiserChenal(this.donnees_);
      // System.out.println("55555");
      gestionChenaux_.setVisible(true);
      addInternalFrame(gestionChenaux_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionChenaux_.isClosed()) {
        addInternalFrame(gestionChenaux_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionChenaux_);
        addInternalFrame(gestionChenaux_);

      }

    }
    
    // addInternalFrame(new SiporVisualiserChenal(donnees_));
  }

  /**
   * Methode de cration d une fenetre interne de saisie de cheneaux dans l application principale
   */

  void parametreMaree() {

    if (gestionMarees_ == null) {
      FuLog.debug("interface nulle");
      gestionMarees_ = new SiporFrameSaisieMaree(this.donnees_);
      // System.out.println("55555");
      gestionMarees_.setVisible(true);
      addInternalFrame(gestionMarees_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionMarees_.isClosed()) {
        addInternalFrame(gestionMarees_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionMarees_);
        addInternalFrame(gestionMarees_);

      }

    }

    // addInternalFrame(new SiporFrameSaisieMaree(donnees_));
  }

  /**
   * Methode de creation d une fenetre interne de saisie des cat�gories de navire dans l application principale
   */

  void parametreNavire() {
    if (gestionNavires_ == null) {
      FuLog.debug("interface nulle");
      gestionNavires_ = new SiporVisualiserNavires(this.donnees_);
      // System.out.println("55555");
      gestionNavires_.setVisible(true);

      addInternalFrame(gestionNavires_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionNavires_.isClosed()) {
        gestionNavires_.dispose();
        gestionNavires_ = new SiporVisualiserNavires(this.donnees_);
        addInternalFrame(gestionNavires_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionNavires_);
        gestionNavires_.dispose();
        gestionNavires_ = new SiporVisualiserNavires(this.donnees_);

        addInternalFrame(gestionNavires_);

      }

    }

    
    // addInternalFrame(new SiporVisualiserNavires(donnees_));
  }

  /**
   * Methode de cr�ation d une fenetre interne de saisie de cercles dans l application principale
   */

  void parametreCercle() {

    if (gestionCercles_ == null) {
      FuLog.debug("interface nulle");
      gestionCercles_ = new SiporVisualiserCercles(this.donnees_);
      // System.out.println("55555");
      gestionCercles_.setVisible(true);
      addInternalFrame(gestionCercles_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionCercles_.isClosed()) {
        addInternalFrame(gestionCercles_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionCercles_);
        addInternalFrame(gestionCercles_);

      }

    }
    
    // addInternalFrame(new SiporVisualiserCercles(donnees_));
  }

  void activationRappelDonnees() {

    if (rappelDonnees_ == null) {
      FuLog.debug("interface nulle");
      rappelDonnees_ = new SiporFrameGenerationRappelDonnees(this.donnees_);
      // System.out.println("55555");
      rappelDonnees_.setVisible(true);
      addInternalFrame(rappelDonnees_);
    } else {
      FuLog.debug("interface ferm�e");
      if (rappelDonnees_.isClosed()) {
        addInternalFrame(rappelDonnees_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(rappelDonnees_);
        addInternalFrame(rappelDonnees_);

      }

    }

    // addInternalFrame(new SiporFrameSaisieMaree(donnees_));
  }

  /*********************************************************************************************************************
   * ****************************************************************************************************
   * ************************** Activation des Interfaces de sorties *********************************
   * ****************************************************************************************************
   ********************************************************************************************************************/

  /**
   * methode qui active le lancement de l'interface de visualisation des parametres r�sultats de la g�n�ration de
   * navires
   */
  public void activerResultatsgenerationNavires() {

    if (this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatGenerationBateaux(donnees_));

  }

  public void activerResultatHistorique() {
    if (this.donnees_.getListeResultatsSimu_() == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatHistorique(donnees_));

  }

  public void activerResultatDureesParcours() {
    if (this.donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsDureesParcours(donnees_));
  }

  public void activerResultatCroisementsChenaux() {
    if (this.donnees_.getListeResultatsSimu_() == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsCroisementsChenal(donnees_));
  }

  public void activerResultatCroisementsCercles() {
    if (this.donnees_.getListeResultatsSimu_() == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsCroisementsCercle(donnees_));
  }

  public void activerResultatAttentesGeneralesElement() {
    if (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsAttenteGeneraleElement(donnees_));
  }

  public void activerResultatAttentesGeneralesCategorie() {
    if (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsAttenteGeneraleCategories(donnees_));
  }

  public void activerResultatAttentesSpecialisee() {
    if (this.donnees_.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Erreur!!\n Relancez la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsAttenteTrajet(donnees_));
  }

  public void activerResultatOccupGlobales() {
    if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "\n Veuillez relancer la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatOccupationGlobales(donnees_));

  }

  public void activerResultatOccupQuai() {
    if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Veuillez relancer la simulation dabord," + " \n les r�sultats de la simulation ne sont pas charg�s"
              + "\n (pour relancer la simulation, aller dans" + "\n l'onglet Simulation et cliquer sur \"Calculer\")")
          .activate();

      return;
    }
    addInternalFrame(new SiporResultatsOccupationQuai(donnees_));
  }

  public void activerComparaisonNombreNavires() {
    addInternalFrame(new SiporResultatComparaisonGenerationBateaux(donnees_));
  }

  public void activerComparaisonOccupationGlobale() {
    addInternalFrame(new SiporResultatComparaisonOccupationGlobale(donnees_));
  }

  public void activerComparaisonDureesParcours() {
    addInternalFrame(new SiporResultatComparaisonDureeParcours(donnees_));
  }

  public void activerComparaisonOccupationQuais() {
    addInternalFrame(new SiporResultatComparaisonOccupQuais(donnees_));
  }
  
  public void activerComparaisonAttenteGenerale(){
    addInternalFrame(new SiporResultatComparaisonAttenteElement(donnees_));
  }
  
  public void activerComparaisonAttenteTrajet(){
    addInternalFrame(new SiporResultatComparaisonAttentetrajet(donnees_));
  }

  /**
   * methode de fermeture de toutes les interfaces
   */

  void fermerToutesLesInterfaces() {

    if (gestionQuais_ != null) {
      gestionQuais_.dispose();
    }
    if (gestionNavires_ != null) {
      gestionNavires_.dispose();
    }
    if (gestionChenaux_ != null) {
      gestionChenaux_.dispose();
    }
    if (gestionCercles_ != null) {
      gestionCercles_.dispose();
    }
    if (gestionGares_ != null) {
      gestionGares_.dispose();
    }
    if (gestionBassins_ != null) {
      gestionBassins_.dispose();
    }
    if (gestionEcluses_ != null) {
      gestionEcluses_.dispose();
    }
    if (gestionMarees_ != null) {
      gestionMarees_.dispose();
    }
    if (gestionDonneesGenerales_ != null) {
      gestionDonneesGenerales_.dispose();
    }
    if (gestionTopoBassins_ != null) {
      gestionTopoBassins_.dispose();
    }
    if (gestionTopoEcluses_ != null) {
      gestionTopoEcluses_.dispose();
    }
    if (gestionTopoChenaux_ != null) {
      gestionTopoChenaux_.dispose();
    }
    if (gestionTopoCercles_ != null) {
      gestionTopoCercles_.dispose();
    }
    if (gestionModelisation_ != null) {
      gestionModelisation_.dispose();
    }
    if (gestionNavigChenal_ != null) {
      gestionNavigChenal_.dispose();
    }
    if (gestionNavigCercle_ != null) {
      gestionNavigCercle_.dispose();
    }
    if (gestionDureeParcoursChenal_ != null) {
        gestionDureeParcoursChenal_.dispose();
      }
    if (gestionRemplissageSAS_ != null) {
    	gestionRemplissageSAS_.dispose();
      }
      
    
    if (gestionDureeParcoursCercle_ != null) {
      gestionDureeParcoursCercle_.dispose();
    }
    if (gestionGenesCercle_ != null) {
      gestionGenesCercle_.dispose();
      }
    

  }

  public void initialiserToutesLesInterfaces() {

    // etape 1: on ferme toutes les interfaces
    fermerToutesLesInterfaces();

    // etape 2: on remet a null toutes les interfaces .
    gestionQuais_ = null;
    gestionNavires_ = null;
    gestionChenaux_ = null;
    gestionCercles_ = null;
    gestionGares_ = null;
    gestionBassins_ = null;
    gestionEcluses_ = null;
    gestionMarees_ = null;
    gestionDonneesGenerales_ = null;
    gestionTopoBassins_ = null;
    gestionTopoEcluses_ = null;
    gestionTopoChenaux_ = null;
    gestionTopoCercles_ = null;
    gestionModelisation_ = null;
    gestionNavigChenal_ = null;
    gestionNavigCercle_ = null;
    gestionDureeParcoursChenal_ = null;
    gestionRemplissageSAS_ = null;
    gestionDureeParcoursCercle_ = null;
    gestionGenesCercle_=null;

  }

  // Ajoute une fenetre.
  protected BuInternalFrame creerFenetreInterne() {
    final BuInternalFrame frame = new BuInternalFrame("Graphe", true, true, true, true);
    frame.setBackground(Color.white);
    frame.getContentPane().setLayout(new BorderLayout());
    frame.setBounds(200, 200, 200, 200);
    frame.setSize(400, 400);
    frame.setVisible(true);
    addInternalFrame(frame);
    return frame;
  }

  /*--- methode de controle des parametres ---*/

  public void activeActionsExploitation() {
    SiporOutilsDonnees simulation = null;
    // autorise l'exploitation de la simulation si projet_ contient des r�sultats
    simulation = new SiporOutilsDonnees(projet_);
    if (simulation.getResultats() != null) {
      setEnabledForAction("TABLEAU", true);
    } else {
      setEnabledForAction("TABLEAU", false);
    }
    // autorise l'exploitation du projet si au moins une simulation avec des r�sultats
    final Object[] simulations = projets_.keySet().toArray();
    for (int i = 0; i < simulations.length; i++) {
      simulation = new SiporOutilsDonnees((FudaaProjet) projets_.get(simulations[i]));
      if (simulation.getResultats() != null) {
        setEnabledForAction("GRAPHE", true);
        return;
      }
    }
    // sinon pas de simulation avec des r�sultats
    setEnabledForAction("GRAPHE", false);
  }

  public void exit() {
    fermer();
    super.exit();
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_SIPOR = null;
    SERVEUR_SIPOR = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_SIPOR, CONNEXION_SIPOR);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculSipor.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculSipor.class);
    CONNEXION_SIPOR = c.getConnexion();
    SERVEUR_SIPOR = ICalculSiporHelper.narrow(c.getTache());
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return SiporPreferences.SIPOR;
  }
  
  
  /**
   * Methode de suppression d'une simulation parmi les simulations ouvertes.
   *
   */
  public void supprimerSimulation(){
    
    //-- cas particulier: 1 seule simulation pr�sente: on propose de quitter le logiciel --//
    if(liste_.getModel().getSize()<=1){
      exit();
      return ;
    }
    
    //-- Recuperation du projet Fudaa selectionn� --//
    String nomProjet=(String)liste_.getSelectedValue();
    FudaaProjet deleteProject= (FudaaProjet)projets_.get(liste_.getSelectedValue());
    int indiceProjetSupprime=liste_.getSelectedIndex();
    
    //-- Test: si le projet supprim� est le projet en cours: on recharge un autre projet --//
    if(deleteProject==donnees_.getProjet_())
    {
      if(indiceProjetSupprime!=0)
      //position 0 -> premiere simulation
      liste_.setSelectedIndex(0);
      else
        liste_.setSelectedIndex(1);
      // changement de simulation du cot� m�tier:
      donnees_.changerProjet((FudaaProjet)projets_.get(liste_.getSelectedValue()));
    }
    
    //-- On supprime le projet selectionn� de la liste et de la hashTable --//
    System.out.println("\nl'indice a supprimer est "+indiceProjetSupprime);
     System.out.println("la taille du modele est "+liste_.getModel().getSize());
    System.out.println("\nl'indice a selectionne est "+liste_.getSelectedIndex());
    ((DefaultListModel)liste_.getModel()).remove(indiceProjetSupprime);
    projets_.remove(nomProjet);
    
    liste_.revalidate();
    
  }
  
  
  /**
   * Methode de suppression du fichier historique.
   * Methode importante car le fichier est souvent tr�s volumineux et les r�sultats sont sauvegard� donc historique inutile.
   *
   */
  public void supprimerHistoriqueSimulation(){
    
    //-- path du fichier --//
    String path= donnees_.getProjet_().getFichier()+".his";
    
    File historique=new File(path);
    
    BuDialogConfirmation mess=new BuDialogConfirmation(this,INFORMATION_SOFT,"Voulez-vous vraiment supprimer le fichier historique?");
    final int confirmation=mess.activate();
    if(confirmation==0)
    {
      boolean reussite=historique.delete();
      
      if(!reussite)
        new BuDialogError(this.getApp(),INFORMATION_SOFT,"Le fichier historique n'a pas pu �tre supprim�.").activate();
      else
        new BuDialogMessage(this.getApp(),INFORMATION_SOFT,"Le fichier historique a �t� correctement supprim�.").activate();
    }
    
  }
  

  
  /**
   * Methode qui renseigne sur le chenmin vers les serveurs sipor.
   * @return
   */
  protected final String cheminServeur() {
      String path = System.getProperty("FUDAA_SERVEUR");
      if ((path == null) || path.equals("")) {
        path = System.getProperty("user.dir") + File.separator + "serveurs"
            + File.separator + "Fudaa-Sipor";
      }
      if (!path.endsWith(File.separator)) {
        path += File.separator;
      }
      return path;
    }


 /**
  * Methode qui permet d'executer une comande via la classe CExec. 
  * @param progFortran programme executable a lancer 
  * 0= SiporGenarr
  * 1= noyau de calcul 
  * @param nomEtude nom de l'etude sipor
  */
 protected boolean ExecuterCommandeDepuisServeur(int progFortran,String nomEtude){
   
   final String os = System.getProperty("os.name");
      final String path = cheminServeur();
      System.out.println("**\nLe chemin des serveurs est:\n "+path+" \n**");
      try {
        String[] cmd;
        
        if (os.startsWith("Windows")) {
          cmd = new String[2];
          if(progFortran==0)
            cmd[0] = path + "bin" + "\\win\\siporGenarr_win.exe";
          else
            cmd[0] = path + "bin" + "\\win\\sipor_win.exe";
        //  cmd[1] = path + "bin" + "\\win\\";
          cmd[1] = nomEtude;
        } else {
         // System.out.println("* yo 2 *");
          cmd = new String[2];
          if(progFortran==0)
            cmd[0] = path + "bin" + "/linux/siporGenarr_linux.x";
          else
            cmd[0] = path + "bin" + "/linux/sipor_linux.x";
          //cmd[1] = path + "bin" + "/linux/";
          cmd[1] = nomEtude;
            }
        System.out.println("**\nLa commande ex�cut�e est: \n "+cmd[0] + " " + cmd[1] +"\n**");
      
        
        try {
          final CExec ex = new CExec();
          ex.setCommand(cmd);
          ex.setOutStream(System.out);
          ex.setErrStream(System.err);
          ex.exec();
        }
        catch (final Throwable _e1) {
          System.out.println("Erreur rencontr�e lors de l'execution du code de calcul");
          _e1.printStackTrace();
          new BuDialogError(this.getApp(),INFORMATION_SOFT,"Erreur rencontr�e lors de l'execution du code de calcul").activate();
          return false;
        }
       
      }
      catch (final Exception ex) {
        System.out.println("Erreur lors de l'execution du code de calcul");
        if(progFortran==0)
        new BuDialogMessage(this.getApp(),INFORMATION_SOFT,"Impossible d'executer le g�n�rateur de navire genarr").activate();
        else
          new BuDialogMessage(this.getApp(),INFORMATION_SOFT,"Impossible d'executer le noyau de calcul Sipor").activate();
        return false;
      }
   
   return true;
 }
  
  
  
  
  /**
   * Methode de lancement de l'executable GENARR.
   * Permet de g�n�rer un fichier contenant la liste des navires.
   *
   */
  public void lancementGenarr(){
    
    //-- ETAPE 1: v�rification du nombre minimal de cat�gories de navire --//
    if(donnees_.getCategoriesNavires_().getListeNavires_().size()==0){
      new BuDialogError(this.getApp(),INFORMATION_SOFT,"Il doit exister au moins une cat�gorie de navire.\n" +
        "vant de r�aliser la g�n�ration de navires.").activate();
      return;
    }
    
    
    try {
     //-- Etape 2: creation du fichier des cat�gories de navire: .categ et donn�es g�n�rales --// 
    DParametresSipor.ecritDonneesCategoriesNavires(this.donnees_.getParams_().navires, this.donnees_.getProjet_().getFichier());
    DParametresSipor.ecritDonneesGenerales(this.donnees_.getParams_().donneesGenerales, this.donnees_.getProjet_().getFichier());
      
    //-- Etape 3: lancement de l'executable Genarr --//
    if(this.MOCK_ENABLE) {
    	SiporMock mock = new SiporMock();
    	mock.mockGenerationBateaux( donnees_.getProjet_().getFichier());
    } else {
	    if(!ExecuterCommandeDepuisServeur(0,donnees_.getProjet_().getFichier()))
	          return ;//si la procedure retourne une valeure false alors il y a eu erreur, par cons�quent on quitte.
    }
    
    
    
    
    
    
    //-- Etape 4: lecture du fichier g�n�r� et remplissage de la structure GENARR--//
    boolean result=donnees_.getGenarr_().lectureFichierGenarr(donnees_.getProjet_().getFichier());
    if(!result)
      return;
    
    new BuDialogMessage(this.getApp(),INFORMATION_SOFT,"La g�n�ration de navires a �t� r�alis� avec succ�s.").activate();
    
    
    } catch (IOException e) {
      new BuDialogError(this.getApp(),INFORMATION_SOFT,"Erreur dans la cr�ation du fichier des cat�gories de navire.\n" +
          "v�rifier la saisie des cat�gories de navires.").activate();
    }
  }
  
  /**
   * Methode d'aafichage des navires g�n�r�s par GENARR.
   * L'affichage s'effectue sous forme de tableau r�capitulatif triable.
   *
   */
  public void affichageGenarr(){
    
    //-- ETAPE 1: verification de l'existence du fichier GENARR: --//
      File fichier=new File(donnees_.getProjet_().getFichier()+".arriv");
    if(!fichier.exists()){
      new BuDialogError(null,SiporImplementation.INFORMATION_SOFT,"Le fichier de g�n�ration est introuvable.\n Veuillez relancer la g�n�ration de navire (onglet G�n�ration)").activate();
      return;
    }
    
    //-- ETAPE 2: Lecture du fichier GENARR --//
    boolean result= donnees_.getGenarr_().lectureFichierGenarr(donnees_.getProjet_().getFichier());
    if(!result)
        return;
     
    //-- activation de l'interface d'affichage de Genarr --// 
     addInternalFrame(new GenarrFrameAffichage(donnees_));
  }

public static ICalculSipor getSERVEUR_SIPOR() {
  return SERVEUR_SIPOR;
}

public static void setSERVEUR_SIPOR(ICalculSipor sERVEUR_SIPOR) {
  SERVEUR_SIPOR = sERVEUR_SIPOR;
}

public static IConnexion getCONNEXION_SIPOR() {
  return CONNEXION_SIPOR;
}

public static void setCONNEXION_SIPOR(IConnexion cONNEXION_SIPOR) {
  CONNEXION_SIPOR = cONNEXION_SIPOR;
}

public IParametresSipor getSiporParams_() {
  return siporParams_;
}

public void setSiporParams_(IParametresSipor siporParams_) {
  this.siporParams_ = siporParams_;
}

public IResultatsSipor getSiporResults_() {
  return siporResults_;
}

public void setSiporResults_(IResultatsSipor siporResults_) {
  this.siporResults_ = siporResults_;
}

public Hashtable getProjets_() {
  return projets_;
}

public void setProjets_(Hashtable projets_) {
  this.projets_ = projets_;
}

public FudaaProjet getProjet_() {
  return projet_;
}

public void setProjet_(FudaaProjet projet_) {
  this.projet_ = projet_;
}

public SiporOutilsDonnees getOutils_() {
  return outils_;
}

public void setOutils_(SiporOutilsDonnees outils_) {
  this.outils_ = outils_;
}

public static Color getBleuSipor() {
  return bleuSipor;
}

public static void setBleuSipor(Color bleuSipor) {
  SiporImplementation.bleuSipor = bleuSipor;
}

public static Color getBleuClairSipor() {
  return bleuClairSipor;
}

public static void setBleuClairSipor(Color bleuClairSipor) {
  SiporImplementation.bleuClairSipor = bleuClairSipor;
}

public SiporDataSimulation getDonnees_() {
  return donnees_;
}

public void setDonnees_(SiporDataSimulation donnees_) {
  this.donnees_ = donnees_;
}

public SiporVisualiserQuais getGestionQuais_() {
  return gestionQuais_;
}

public void setGestionQuais_(SiporVisualiserQuais gestionQuais_) {
  this.gestionQuais_ = gestionQuais_;
}

public SiporVisualiserNavires getGestionNavires_() {
  return gestionNavires_;
}

public void setGestionNavires_(SiporVisualiserNavires gestionNavires_) {
  this.gestionNavires_ = gestionNavires_;
}

public SiporVisualiserChenal getGestionChenaux_() {
  return gestionChenaux_;
}

public void setGestionChenaux_(SiporVisualiserChenal gestionChenaux_) {
  this.gestionChenaux_ = gestionChenaux_;
}

public SiporVisualiserCercles getGestionCercles_() {
  return gestionCercles_;
}

public void setGestionCercles_(SiporVisualiserCercles gestionCercles_) {
  this.gestionCercles_ = gestionCercles_;
}

public SiporVisualiserGares getGestionGares_() {
  return gestionGares_;
}

public void setGestionGares_(SiporVisualiserGares gestionGares_) {
  this.gestionGares_ = gestionGares_;
}

public SiporVisualiserBassins getGestionBassins_() {
  return gestionBassins_;
}

public void setGestionBassins_(SiporVisualiserBassins gestionBassins_) {
  this.gestionBassins_ = gestionBassins_;
}

public SiporVisualiserEcluses getGestionEcluses_() {
  return gestionEcluses_;
}

public void setGestionEcluses_(SiporVisualiserEcluses gestionEcluses_) {
  this.gestionEcluses_ = gestionEcluses_;
}

public SiporFrameSaisieMaree getGestionMarees_() {
  return gestionMarees_;
}

public void setGestionMarees_(SiporFrameSaisieMaree gestionMarees_) {
  this.gestionMarees_ = gestionMarees_;
}

public SiporFrameSaisieDonneesGenerales getGestionDonneesGenerales_() {
  return gestionDonneesGenerales_;
}

public void setGestionDonneesGenerales_(
    SiporFrameSaisieDonneesGenerales gestionDonneesGenerales_) {
  this.gestionDonneesGenerales_ = gestionDonneesGenerales_;
}

public SiporPanelTopologieBassin getGestionTopoBassins_() {
  return gestionTopoBassins_;
}

public void setGestionTopoBassins_(SiporPanelTopologieBassin gestionTopoBassins_) {
  this.gestionTopoBassins_ = gestionTopoBassins_;
}

public SiporPanelTopologieEcluse getGestionTopoEcluses_() {
  return gestionTopoEcluses_;
}

public void setGestionTopoEcluses_(SiporPanelTopologieEcluse gestionTopoEcluses_) {
  this.gestionTopoEcluses_ = gestionTopoEcluses_;
}

public SiporPanelTopologieChenal getGestionTopoChenaux_() {
  return gestionTopoChenaux_;
}

public void setGestionTopoChenaux_(SiporPanelTopologieChenal gestionTopoChenaux_) {
  this.gestionTopoChenaux_ = gestionTopoChenaux_;
}

public SiporPanelTopologieCercle getGestionTopoCercles_() {
  return gestionTopoCercles_;
}

public void setGestionTopoCercles_(SiporPanelTopologieCercle gestionTopoCercles_) {
  this.gestionTopoCercles_ = gestionTopoCercles_;
}

public SiporDessinerPortFrame getGestionModelisation_() {
  return gestionModelisation_;
}

public void setGestionModelisation_(SiporDessinerPortFrame gestionModelisation_) {
  this.gestionModelisation_ = gestionModelisation_;
}

public SiporPanelReglesNavigationChenal getGestionNavigChenal_() {
  return gestionNavigChenal_;
}

public void setGestionNavigChenal_(
    SiporPanelReglesNavigationChenal gestionNavigChenal_) {
  this.gestionNavigChenal_ = gestionNavigChenal_;
}

public SiporPanelReglesNavigationCercle getGestionNavigCercle_() {
  return gestionNavigCercle_;
}

public void setGestionNavigCercle_(
    SiporPanelReglesNavigationCercle gestionNavigCercle_) {
  this.gestionNavigCercle_ = gestionNavigCercle_;
}

public SiporPanelReglesParcoursChenal getGestionDureeParcoursChenal_() {
  return gestionDureeParcoursChenal_;
}

public void setGestionDureeParcoursChenal_(
    SiporPanelReglesParcoursChenal gestionDureeParcoursChenal_) {
  this.gestionDureeParcoursChenal_ = gestionDureeParcoursChenal_;
}

public SiporPanelReglesParcoursCercle getGestionDureeParcoursCercle_() {
  return gestionDureeParcoursCercle_;
}

public void setGestionDureeParcoursCercle_(
    SiporPanelReglesParcoursCercle gestionDureeParcoursCercle_) {
  this.gestionDureeParcoursCercle_ = gestionDureeParcoursCercle_;
}

public SiporFrameGenerationRappelDonnees getRappelDonnees_() {
  return rappelDonnees_;
}

public void setRappelDonnees_(SiporFrameGenerationRappelDonnees rappelDonnees_) {
  this.rappelDonnees_ = rappelDonnees_;
}

public BuBrowserFrame getfRappelDonnees_() {
  return fRappelDonnees_;
}

public void setfRappelDonnees_(BuBrowserFrame fRappelDonnees_) {
  this.fRappelDonnees_ = fRappelDonnees_;
}

public BuBrowserFrame getfStatistiquesFinales_() {
  return fStatistiquesFinales_;
}

public void setfStatistiquesFinales_(BuBrowserFrame fStatistiquesFinales_) {
  this.fStatistiquesFinales_ = fStatistiquesFinales_;
}

public BuAssistant getAssistant_() {
  return assistant_;
}

public void setAssistant_(BuAssistant assistant_) {
  this.assistant_ = assistant_;
}

public BuHelpFrame getAide_() {
  return aide_;
}

public void setAide_(BuHelpFrame aide_) {
  this.aide_ = aide_;
}

public BuTaskView getTaches_() {
  return taches_;
}

public void setTaches_(BuTaskView taches_) {
  this.taches_ = taches_;
}

public SiporListeSimulations getListe_() {
  return liste_;
}

public void setListe_(SiporListeSimulations liste_) {
  this.liste_ = liste_;
}

public BuInternalFrame getfGraphiques_() {
  return fGraphiques_;
}

public void setfGraphiques_(BuInternalFrame fGraphiques_) {
  this.fGraphiques_ = fGraphiques_;
}

public BuButton getSupprimerSimu_() {
  return supprimerSimu_;
}

public void setSupprimerSimu_(BuButton supprimerSimu_) {
  this.supprimerSimu_ = supprimerSimu_;
}

public BuButton getAjouterSimu_() {
  return ajouterSimu_;
}

public void setAjouterSimu_(BuButton ajouterSimu_) {
  this.ajouterSimu_ = ajouterSimu_;
}

public static boolean isApplication() {
  return IS_APPLICATION;
}

public static BuInformationsSoftware getInformationSoft() {
  return INFORMATION_SOFT;
}

public static BuInformationsDocument getIdsipor() {
  return idSipor_;
}

public SiporPanelReglesGenesCercle getGestionGenesCercle_() {
  return gestionGenesCercle_;
}

public void setGestionGenesCercle_(
    SiporPanelReglesGenesCercle gestionGenesCercle_) {
  this.gestionGenesCercle_ = gestionGenesCercle_;
}

public SiporPanelRemplissageSAS getGestionRemplissageSAS_() {
	return gestionRemplissageSAS_;
}

public void setGestionRemplissageSAS_(
		SiporPanelRemplissageSAS gestionRemplissageSAS_) {
	this.gestionRemplissageSAS_ = gestionRemplissageSAS_;
}

public SiporNetwork getNetworkEditor() {
	return networkEditor;
}

public void setNetworkEditor(SiporNetwork networkEditor) {
	this.networkEditor = networkEditor;
}
  
  
}
