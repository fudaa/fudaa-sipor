package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserGares;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextField;

/**
 * Panel de saisie des diff�rents Bassins
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelSaisieGare extends JPanel {

  // attributs:
  static int nbouverture = 0;
  /**
   * Jtext du nom a saisir
   */
  SiporTextField nom_ = new SiporTextField(10);

  /**
   * Bouton de validation du gare
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * donn�es de la simulation
   */
  SiporDataSimulation donnees_;

  
  /**
   * Fenetre principale de gestion des bassins
   */

  public  SiporVisualiserGares MENUGARES_;

  /**
   * Booleen qui indique si le panel est en mode modification MODE_MODIFICATION_ON=true =>>> mode modif sinon mode
   * saisie classique Par defaut r�gl� sur mode classique de saisie(booleen=false)
   */
  public   boolean UPDATE = false;

  /**
   * Indice de la gare a modifier:
   */
  public int GARE_A_MODIFIER_;

  /**
   * Constructeur du panel de saisie des bassins
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelSaisieGare(final SiporDataSimulation d, final SiporVisualiserGares vb) {

    // recuperation de la fenetre de commande principale
    MENUGARES_ = vb;
    donnees_ = d;

    nbouverture++;
    this.setLayout(new BorderLayout());

    this.nom_.setText("Gare " + (this.donnees_.getListeGare_().getListeGares_().size() + 1));
    this.nom_.setToolTipText("Saisissez le nom de la gare ici");

    validation_.setToolTipText("Cliquez sur ce bouton pour valider la saisie");

    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.println("validation du nom de la gare:");
        if (nom_.getText().equals("")) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "La gare n'a pas de nom.").activate();
        } else if (UPDATE && donnees_.getListeGare_().existeDoublon(nom_.getText(), GARE_A_MODIFIER_)) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le nom de gare est d�j� pris.").activate();
        } else if (!UPDATE && donnees_.getListeGare_().existeDoublon(nom_.getText(), -1)) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le nom de gare est d�j� pris.").activate();
        } else {

          /**
           * MODE SAISIE/
           */
          if (!UPDATE) {
            // FUDAA

            // 1)ajout de la chaine de caractere du bassin
            donnees_.getListeGare_().ajout(nom_.getText());
            
            //-- add element in network --//
            DefaultNetworkUserObject userobject = new DefaultNetworkUserObject(nom_.getText(),true);
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_GARE,
            		userobject);
            
            // 2)sauvegarde des donnees
            donnees_.enregistrer();
          } else {
            /***********************************************************************************************************
             * Mode MODIFICATION:
             */
            donnees_.getListeGare_().modification(GARE_A_MODIFIER_, nom_.getText());

          }
          // 2)mise a jour dans le tableau
          MENUGARES_.affichagePanel_.maj(donnees_);

          // 3)remise a zero des composants

          nom_.setText("Gare " + (donnees_.getListeGare_().getListeGares_().size() + 1));

          // 4)mode modification remis a false par defaut
          UPDATE = false;
          MENUGARES_.mode.setText("Saisie: ");
          validation_.setText("Valider");
          MENUGARES_.suppression_.setEnabled(true);
        }

      }

    });

    // architecture des composants
    final JPanel panneau = new JPanel();

    panneau.add(new JLabel("Nom de la gare: "));
    panneau.add(this.nom_);
    panneau.setBorder(SiporBordures.bordnormal_);
    this.add(panneau, BorderLayout.CENTER);

    this.add(validation_, BorderLayout.SOUTH);
    this.setBorder(SiporBordures.bordnormal_);

    // affichage:
  }

  /**
   * Methode qui transforme le type de la fenetre de saisie en fenetre de modification:
   * 
   * @param numBassin indice du tableau de bassin a modifier
   */

  public void MODE_MODIFICATION(final int numGare) {

    UPDATE = true;
    this.nom_.setText(this.donnees_.getListeGare_().retournerGare(numGare));
    GARE_A_MODIFIER_ = numGare;
    this.MENUGARES_.mode.setText("Modification: ");
    this.validation_.setText("Modifier");
    this.MENUGARES_.suppression_.setEnabled(false);
  }

}
