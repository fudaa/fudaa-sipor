package org.fudaa.fudaa.sipor.factory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import org.fudaa.dodico.corba.sipor.SParametresSiporGene;

public class SiporXstreamFactory {
	XStream xstream =null;

	public XStream initParser(){
		if(xstream==null){
			xstream = new XStream(new DomDriver());
			xstream.alias("SiporData", SParametresSiporGene.class);
		}
		return xstream;
	}


	public boolean enregistrer(SParametresSiporGene param,File fichier) throws IOException{
		boolean res=true;

		ObjectOutputStream out = initParser().createObjectOutputStream(new FileWriter(fichier));
		out.writeObject(param);
		out.close();
		return res;
	}



	public SParametresSiporGene chargerProjet(File fichier) throws Exception{
		SParametresSiporGene res=null;

		ObjectInputStream in = initParser().createObjectInputStream(new FileReader(fichier));
		res=(SParametresSiporGene) in.readObject();
		return res;
	}


	
	
	/** 
	 * AHX vérification chargement fichier unitaire.
	 * @param args
	 */
	public static void main(String args[]){

		SiporXstreamFactory factory=new SiporXstreamFactory();
		URL fileUrl=SiporXstreamFactory.class.getResource("/etudeTest/etudeSIPOR.sipor.xml");
		System.out.println("fichier trouvé- "+fileUrl.getFile());
		//-- tentative ouverture du fichier --//
		try {
			SParametresSiporGene struct=factory.chargerProjet(new File(fileUrl.getFile()));
			System.out.println("ok - def \n  "+factory.initParser().toXML(struct));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

