/*
 * @file         SiporAssistant.java
 * @creation     1999-11-03
 * @modification $Date: 2007-03-07 17:25:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.ui.implementation;

import com.memoire.bu.BuAssistant;

/**
 * L'assistant du client Sipor. Rien de plus que BuAssistant.
 * 
 * @version $Revision: 1.5 $ $Date: 2007-03-07 17:25:03 $ by $Author: deniger $
 * @author Nicolas Chevalier
 */
public class SiporAssistant extends BuAssistant {}
