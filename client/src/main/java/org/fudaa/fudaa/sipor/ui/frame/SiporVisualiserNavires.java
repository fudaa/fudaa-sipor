package org.fudaa.fudaa.sipor.ui.frame;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporChenal;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporNavire;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageNavires;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieNavires;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * INterface Graphique principale pour la visualisation de Navires: Cette interface permet de : - visualiser les
 * diffrentes cat�gories de Navires saisis - ajouter une cat�gorie de Navire et ses donnes correspondantes - modifier
 * une cat�gorie de Navire pralablement cree - supprimer une cat�gorie de Navire qui ne convient pas
 * 
 * @author Adrien Hadoux
 */

public class SiporVisualiserNavires extends SiporInternalFrame {

  // attributs

  /**
   * Donnes de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Layout cardlayout pour affichage des donnes
   */
  public CardLayout pile_;

  /**
   * Le panel de base qui contient tous les differents panels contient un layout de type CardLayout
   */
  public JPanel principalPanel_;

  /**
   * Panel d'affichage des differents Navires saisis layout classique flow layout ou grid layout
   */
  public SiporPanelAffichageNavires affichagePanel_;

  /**
   * ascenseur pour le panel d'affichage
   */
  JScrollPane ascAff_;

  /**
   * panel qui contient l'ascenceur (OPTIONNEL!!!!!!!!!!)
   */
  public JPanel conteneurAffichage_;

  /**
   * Panel de saisie des donnes relative aux Navires
   */

  public SiporPanelSaisieNavires SaisieNavirePanel_;

  /**
   * Panel de commande: panel qui contient les differnets boutons responsable de: -ajout -suppression -modification des
   * Navires
   */
  public JPanel controlePanel_;

  /**
   * Boutton de selection de la saisie
   */
  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton boutonAffichage_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_voir"), "Voir");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modif");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
  private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Suppr");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  /**
   * Combo de visualisation des Navires deja saisis
   */
  JComboBox listeNaviresSaisis_ = new JComboBox();

  boolean alterneTitre_ = true;

  /**
   * Constructeur de la Jframe
   */

  public SiporVisualiserNavires(final SiporDataSimulation d) {
    super("", true, true, true, true);
    // recuperation des donnes
    donnees_ = d;

    setTitle("Visualisation des cat�gories");

    setSize(800, 770);
    setBorder(SiporBordures.compound_);

    /**
     * tooltiptext des boutons
     */
    this.boutonAffichage_.setToolTipText("Permet de visualiser la totalit� des donn�es sous forme d'un tableau");
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");
    this.duplication_
        .setToolTipText("Permet de dupliquer une donn�e: cliquez d'abord sur l'�l�ment � dupliquer dans le menu \"voir\"");
    this.impression_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra par la suite imprimer");

    // definition des differents Layout
    final Container contenu = getContentPane();

    this.principalPanel_ = new JPanel();

    pile_ = new CardLayout(30, 10);
    this.principalPanel_.setLayout(pile_);

    // definition du propre panel d'affichage des Navires
    this.affichagePanel_ = new SiporPanelAffichageNavires(donnees_);

    // definition de l ascenceur pour le panel d'affichage
    //this.ascAff_ = new JScrollPane(affichagePanel_);

    // utilisation de swingx
    //ScrollPaneSelector.installScrollPaneSelector(this.ascAff_);

    // definition du panel de saisie d'un bateau

    SaisieNavirePanel_ = new SiporPanelSaisieNavires(donnees_, this);

    this.controlePanel_ = new JPanel();
    this.controlePanel_.setLayout(new FlowLayout());

    // ajout des 2 panel d'affichage et de saisie d'un Navire
    this.principalPanel_.add(this.affichagePanel_, "affichage");
    final JScrollPane pasc = new JScrollPane(this.SaisieNavirePanel_);
    // utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector(pasc);

    this.principalPanel_.add(pasc, "saisie");

    // ajout des panel dans la frame
    contenu.add(principalPanel_);
    contenu.add(controlePanel_, "South");

    // definition des Bubuttons:

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserNavires.this.windowClosed();

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(quitter_);

    boutonSaisie_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SaisieNavirePanel_.initialiser();
        pile_.last(principalPanel_);
        setTitle("Saisie d'une nouvelle cat�gorie de navires");
        SaisieNavirePanel_.setBorder(SiporBordures.navire);
        validate();

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonSaisie_);

    boutonAffichage_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.first(principalPanel_);

        alterneTitre_ = true;
        setTitle("Visualisation des cat�gories");

        validate();

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonAffichage_);

    modification_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // 1) recuperation du numero de ligne du quai via la jtable
        final int numNavire = affichagePanel_.tableau_.getSelectedRow();
        if (numNavire == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur la cat�gorie de navire �\n modifier dans le tableau d'affichage.")
              .activate();

        } else {
          // 2.5 changmeent de fenetre
          pile_.last(principalPanel_);
          setTitle("Modification d'une cat�gorie de navire");
          validate();

          // 2)appel a la mehode de modification de PanelSaisieNavire(a ecrire): met boolean MODIF=true
          SaisieNavirePanel_.MODE_MODIFICATION(numNavire);
          SaisieNavirePanel_.setBorder(SiporBordures.navire2);

          // 3)lors de la sauvegarde , utilise le booleen pour remplacer(methode set des donn�es) au lieu de add
        }// fin du if

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(modification_);

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numNavire = affichagePanel_.tableau_.getSelectedRow();
        if (numNavire == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur la cat�gorie de navire �\n supprimer dans le tableau d'affichage.")
              .activate();

        }

        else {

          // on s occupe de la supresion des cat�gories de navires:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT, "Etes-vous certain de vouloir supprimer la cat�gorie de navire "
                  + donnees_.getCategoriesNavires_().retournerNavire(numNavire).getNom() + " ?").activate();

          if (confirmation == 0) {
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getCategoriesNavires_().suppression(numNavire);

            /**
             * Suppression de la ligne et colonne correspondante des regles de navigations pour chaque chenal et chaque
             * cercle
             */
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              final SiporChenal chenal = donnees_.getListeChenal_().retournerChenal(i);
              chenal.getReglesNavigation_().suppressionNavire(numNavire);
            }
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              final SiporCercle cercle = donnees_.getListeCercle_().retournerCercle(i);
              cercle.getReglesNavigation_().suppressionNavire(numNavire);
              // -- AHX - 2011 - Genes --//
              cercle.getRegleGenes().suppressionNavire(numNavire);
            }

            /**
             * Regles durees passage: suppression de la colonne pour la cat�gorie de navire
             */
            donnees_.getReglesDureesParcoursChenal_().suppressionNavire(numNavire);
            donnees_.getReglesDureesParcoursCercle_().suppressionNavire(numNavire);
            donnees_.getReglesRemplissageSAS().suppressionNavire(numNavire);
            
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);

            // --On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });
    controlePanel_.add(suppression_);
    // controlePanel.setBorder(this.raisedBevel);

    this.duplication_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        final int numNavire = affichagePanel_.tableau_.getSelectedRow();
        if (numNavire == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur la cat�gorie de navire �\n dupliquer dans le tableau d'affichage.")
              .activate();

        } else {

          // on s occupe de la duplication des quais:
          // 1)on demande confirmation:
          String confirmation = "";
          confirmation = JOptionPane.showInputDialog(null, "Nom de la cat�gorie de navire � dupliquer: ");

          if (donnees_.getCategoriesNavires_().existeDoublon(confirmation, -1)) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Nom d�j� pris.").activate();
            return;
          }

          if (!confirmation.equals("")) {
            // 2)ON RECOPIE TOUTES LES DONNEES DUPLIQUEES dans un nouveau quai quel on ajoute
            final SiporNavire nouveau = new SiporNavire();
            final SiporNavire navireAdupliquer = donnees_.getCategoriesNavires_().retournerNavire(numNavire);

            nouveau.setGareDepart_(navireAdupliquer.getGareDepart_());
            nouveau.setNomGareDepart_(navireAdupliquer.getNomGareDepart_());
            nouveau.setCadenceQuai(navireAdupliquer.getCadenceQuai());
            nouveau.setCadenceQuaiPref(navireAdupliquer.getCadenceQuaiPref());
            nouveau.setDureeAttenteShiftSuivant(navireAdupliquer.getDureeAttenteShiftSuivant());
            /*F9
            nouveau.setDureeQuaiPref1(navireAdupliquer.getDureeQuaiPref1());
            nouveau.setDureeQuaiPref2(navireAdupliquer.getDureeQuaiPref2());
            nouveau.setDureeQuaiPref3(navireAdupliquer.getDureeQuaiPref3());
            */
            nouveau.setDureeServiceMaxAutresQuais(navireAdupliquer.getDureeServiceMaxAutresQuais());
            nouveau.setDureeServiceMaxQuaiPref(navireAdupliquer.getDureeServiceMaxQuaiPref());
            nouveau.setDureeServiceMinAutresQuais(navireAdupliquer.getDureeServiceMinAutresQuais());
            nouveau.setDureeServiceMinQuaiPref(navireAdupliquer.getDureeServiceMinQuaiPref());
            nouveau.setHoraireTravailSuivi(navireAdupliquer.getHoraireTravailSuivi());
            nouveau.setLongueurMax(navireAdupliquer.getLongueurMax());
            nouveau.setLongueurMin(navireAdupliquer.getLongueurMin());
            nouveau.setLargeurMax(navireAdupliquer.getLargeurMax());
            nouveau.setLargeurMin(navireAdupliquer.getLargeurMin());
            nouveau.setDureeAttenteMaximaleAdmissible(navireAdupliquer.getDureeAttenteMaximaleAdmissible());
            nouveau.getCreneauxJournaliers_().recopie(navireAdupliquer.getCreneauxJournaliers_());
            nouveau.getCreneauxouverture_().recopie(navireAdupliquer.getCreneauxouverture_());

            nouveau.setTypeModeChargement(navireAdupliquer.getTypeModeChargement());
            nouveau.setModeChargement(navireAdupliquer.getModeChargement());
            nouveau.setNbQuaisPreferentiels(navireAdupliquer.getNbQuaisPreferentiels());
            nouveau.setOrdreLoiAutresQuais(navireAdupliquer.getOrdreLoiAutresQuais());
            nouveau.setOrdreLoiQuaiPref(navireAdupliquer.getOrdreLoiQuaiPref());
            nouveau.setOrdreLoiVariationLongueur_(navireAdupliquer.getOrdreLoiVariationLongueur_());
            nouveau.setOrdreLoiVariationLargeur_(navireAdupliquer.getOrdreLoiVariationLargeur_());

            nouveau.setOrdreLoiVariationTonnage(navireAdupliquer.getOrdreLoiVariationTonnage());
            nouveau.setPriorite(navireAdupliquer.getPriorite());
            nouveau.setQuaiPreferentiel1(navireAdupliquer.getQuaiPreferentiel1());
            nouveau.setNomQuaiPreferentiel1(navireAdupliquer.getNomQuaiPreferentiel1());
            nouveau.setNomQuaiPreferentiel2(navireAdupliquer.getNomQuaiPreferentiel2());
            nouveau.setNomQuaiPreferentiel3(navireAdupliquer.getNomQuaiPreferentiel3());
            nouveau.setQuaiPreferentiel2(navireAdupliquer.getQuaiPreferentiel2());
            nouveau.setQuaiPreferentiel3(navireAdupliquer.getQuaiPreferentiel3());
            /*F9
            nouveau.setDureeQuaiPref1(navireAdupliquer.getDureeQuaiPref1());
            nouveau.setDureeQuaiPref2(navireAdupliquer.getDureeQuaiPref2());
            nouveau.setDureeQuaiPref3(navireAdupliquer.getDureeQuaiPref3());
            */
            nouveau.setTirantEauEntree(navireAdupliquer.getTirantEauEntree());
            nouveau.setTirantEauSortie(navireAdupliquer.getTirantEauSortie());
            nouveau.setTonnageMax(navireAdupliquer.getTonnageMax());
            nouveau.setTonnageMin(navireAdupliquer.getTonnageMin());

            // copie des horaires
            nouveau.getHoraireTravailSuivi().recopie(navireAdupliquer.getHoraireTravailSuivi());

            // cas loi generation des navires
            if (navireAdupliquer.getTypeLoiGenerationNavires_() == 0) {
              nouveau.setTypeLoiGenerationNavires_(0);
              nouveau.setNbBateauxattendus(navireAdupliquer.getNbBateauxattendus());
              nouveau.setEcartMoyenEntre2arrivees(navireAdupliquer.getEcartMoyenEntre2arrivees());
              nouveau.setLoiErlangGenerationNavire(navireAdupliquer.getLoiErlangGenerationNavire());
            } else if (navireAdupliquer.getTypeLoiGenerationNavires_() == 1) {

              nouveau.setTypeLoiGenerationNavires_(1);
              for (int i = 0; i < navireAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) navireAdupliquer
                    .getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            } else if (navireAdupliquer.getTypeLoiGenerationNavires_() == 2) {
              // cas loi journaliere
              nouveau.setTypeLoiGenerationNavires_(2);
              for (int i = 0; i < navireAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) navireAdupliquer
                    .getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            }

            // on donne le nouveau nom du quai a dupliquer
            nouveau.setNom(confirmation);

            // on ajoute le nouveau quai dupliqu�
            donnees_.getCategoriesNavires_().ajout(nouveau);

            /**
             * Creation d'un vecteur ligne regles de navigations suppl�mentaire pour chaque chenal et pour chaque
             * cercle:
             */
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              final SiporChenal chenal = donnees_.getListeChenal_().retournerChenal(i);

              chenal.getReglesNavigation_().ajoutNavire();
            }
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              final SiporCercle cercle = donnees_.getListeCercle_().retournerCercle(i);

              cercle.getReglesNavigation_().ajoutNavire();
              // -- AHX - 2011 - Genes --//
              cercle.getRegleGenes().ajoutNavire();
            }
            /**
             * Ajout d'une colonne regles dur�es parcours
             */
            donnees_.getReglesDureesParcoursChenal_().ajoutNavire();
            donnees_.getReglesDureesParcoursCercle_().ajoutNavire();
            donnees_.getReglesRemplissageSAS().ajoutNavire();

            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
          }// fin cas ou l on confirme la suppression
        }

      }
    });
    this.controlePanel_.add(duplication_);

    impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporVisualiserNavires.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = affichagePanel_.titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.getCategoriesNavires_().getListeNavires_().size() + 2][affichagePanel_.titreColonnes_.length];

          for (int i = 0; i < affichagePanel_.titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = affichagePanel_.titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
            modele.data_[i + 2] = affichagePanel_.ndata_[i];
          }

          modele.setNbRow(donnees_.getCategoriesNavires_().getListeNavires_().size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon

      }// fin de la methode public actionPerformed

    });

    this.controlePanel_.add(impression_);

    // combo de choix des Navires:
    // etape 1: Initialisation des donnes deja saisies
    for (int j = 0; j < donnees_.getCategoriesNavires_().getListeNavires_().size(); j++) {
      this.listeNaviresSaisis_.addItem(((SiporNavire) donnees_.getCategoriesNavires_().getListeNavires_().get(j))
          .getNom());
    }

    // listener lors du choix dela combo Box
    this.listeNaviresSaisis_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.println("bateau selectionn!!");
        /**
         * SUITE
         */
      }
    });

    /**
     * *************************************************************************************** THE PITI MENU QUI SERVENT
     * A RIEN ***************************************************************************************
     */

    /**
     * choix agreable des cat�gories de navire via un jcombobox
     */

    // ********************************************** test rendu

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserNavires.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    setVisible(true);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
