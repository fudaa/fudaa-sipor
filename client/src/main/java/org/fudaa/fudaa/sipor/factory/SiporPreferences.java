/*
 * @file         SiporPreferences.java
 * @creation     1999-10-01
 * @modification $Date: 2007-08-03 13:52:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.factory;

import com.memoire.bu.BuPreferences;

/**
 * Préférences de sipor.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-08-03 13:52:40 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class SiporPreferences extends BuPreferences {
  public final static SiporPreferences SIPOR = new SiporPreferences();
  
  
  
  
  
  
}
