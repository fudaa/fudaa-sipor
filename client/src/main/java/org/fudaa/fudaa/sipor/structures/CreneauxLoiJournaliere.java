/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.structures;

/**
 * classe de gestion des donn�es des lois journalieres
 * 
 * @version $Version$
 * @author adrien hadoux
 */
public class CreneauxLoiJournaliere {

  public double creneau1deb = 0;
  public double creneau1fin = 0;
  public  double creneau2deb = 0;
  public double creneau2fin = 0;
  public  double creneau3deb = 0;
  public double creneau3fin = 0;
public double getCreneau1deb() {
	return creneau1deb;
}
public void setCreneau1deb(double creneau1deb) {
	this.creneau1deb = creneau1deb;
}
public double getCreneau1fin() {
	return creneau1fin;
}
public void setCreneau1fin(double creneau1fin) {
	this.creneau1fin = creneau1fin;
}
public double getCreneau2deb() {
	return creneau2deb;
}
public void setCreneau2deb(double creneau2deb) {
	this.creneau2deb = creneau2deb;
}
public double getCreneau2fin() {
	return creneau2fin;
}
public void setCreneau2fin(double creneau2fin) {
	this.creneau2fin = creneau2fin;
}
public double getCreneau3deb() {
	return creneau3deb;
}
public void setCreneau3deb(double creneau3deb) {
	this.creneau3deb = creneau3deb;
}
public double getCreneau3fin() {
	return creneau3fin;
}
public void setCreneau3fin(double creneau3fin) {
	this.creneau3fin = creneau3fin;
}

}