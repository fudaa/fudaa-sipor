/**
 * @file Sipor.java
 * @creation 1999-10-01
 * @modification $Date: 2007-08-03 13:52:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.sipor;

import java.awt.Frame;

import javax.swing.UIManager;

import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * Permet de lancer l'application cliente de SIPOR.
 * 
 * @version $Revision: 1.20 $ $Date: 2007-08-03 13:52:41 $ by $Author: hadouxad $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class Sipor {

  /**
   * @param args les arg de l'appli
   */
  public static void main(final String[] args) {

    
	  try {
	      //UIManager.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");
	      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	     
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
   

    final Fudaa f = new Fudaa();
    if(args != null && args.length>0 && args[0] != null &&  "MOCK=true".equals(args[0]) ) {
    	SiporImplementation.MOCK_ENABLE = true;
    }
    
    f.launch(args, SiporImplementation.informationsSoftware(), true);
    f.startApp(new SiporImplementation());

  }
}