package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * barre de progreesion qui permet de.
 * 
 * @version $Id: SiporJProgressBarreProgression.java,v 1.3 2007-05-04 14:01:02 deniger Exp $
 * @author hadoux
 */

public final class SiporJProgressBarreProgression {

  private static final long serialVersionUID = 1L;

  private static SiporJProgressBarreProgression instance;

  private static JProgressBar barreprogression_;

  private static int ligne;

  private static JLabel texte;

  private static JFrame cadre;

  /**
   * Le constructeur de la classe.
   * 
   * @param _preface String permettant de placer un cours texte de description de l'action en cours. par exemple:
   *          "Lecture du fichier: "
   * @param string Indique l'�l�ment li� � la JProgressBar.
   * @param _nbLigne Indique la valeur maximal de la JProgressBar.
   */
  private SiporJProgressBarreProgression(final String _preface, String _string, final int _nbLigne) {
    String string = _string;
    /*
     * Je recuper le nom du fichier dans le chemin fournit en param�tre. S'il est trop long, Je le raccourci et j'ajoute
     * "..."
     */
    string = string.substring(string.lastIndexOf("\\") + 1);
    if (string.length() > 15) {
      string = string.substring(0, 15) + "...";
    }
    ligne = _nbLigne;

    /* Creation de la fen�tre. */
    cadre = new JFrame("En cours de chargement");
    final JPanel panneau = new JPanel();
    texte = new JLabel(_preface + string);
    barreprogression_ = new JProgressBar(0, 100);
    panneau.add("Center", barreprogression_);
    panneau.add("Center", texte);
    cadre.getContentPane().add(BorderLayout.CENTER, panneau);
    cadre.setSize(275, 85);
    cadre.setResizable(false);

    /** Lecture de la taille de l'�cran */
    /* Je centre ma fen�tre au milieu. */
    final java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

    cadre.setLocation((screenSize.width - cadre.getWidth()) / 2, (screenSize.height - cadre.getHeight()) / 2);
    cadre.setVisible(true);
  }

  /**
   * Methode permettant de mettre � jour la barre de progression.
   * 
   * @param _compteur La valeur de la barre.
   */
  public void setValeur(final int _compteur) {
    final int val = _compteur * 100 / ligne;
    barreprogression_.setValue(val);
  }

  /**
   * cr�ation/recuperation de l'instance unique.
   * 
   * @param _preface String permettant de placer un cours texte de description de l'action en cours. par exemple:
   *          "Lecture du fichier: "
   * @param string Indique l'element li� � la JProgressBar.
   * @param _nbLigne Indique la valeur maximal de la JProgressBar.
   * @return l'instance de la JProgressBar.
   */
  public static SiporJProgressBarreProgression getInstance(final String _preface, String _string, final int _nbLigne) {
    String string = _string;
    if (instance == null) {
      instance = new SiporJProgressBarreProgression(_preface, string, _nbLigne);
    } else {
      /*
       * Mise a� jour des valeurs lors d'un nouvel appel. Par exemple: premier appel:" Chargement du fichier: test.txt.
       * second appel:" Chargement du fichier: test2.txt.
       */
      string = string.substring(string.lastIndexOf("\\") + 1);
      if (string.length() > 15) {
        string = string.substring(0, 15) + "...";
      }
      texte.setText(_preface + string);
      barreprogression_.setValue(0);
      ligne = _nbLigne;
      cadre.setVisible(true);
    }

    return instance;
  }

  /**
   * Mise � jour de la barre de progression afin de la remplir. Attente tr�s courte pour laisser le temps �
   * l'utilisateur de voir que le chargement a bien �t� effectu�. Suppression de la fen�tre.
   */
  public void dispose() {
    barreprogression_.setValue(100);
    final Object o = new Object();
    try {
      synchronized (o) {
        //FIXME FRED hein
        o.wait(200);
      }
    } catch (final InterruptedException ex) {}

    cadre.dispose();

  }
}
