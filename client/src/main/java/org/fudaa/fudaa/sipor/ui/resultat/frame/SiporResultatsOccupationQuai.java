/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeOccupationsQuais;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatsOccupationQuai extends SiporInternalFrame /*implements ActionListener */{

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Tableau contenant les r�sultats de la repartition tonnage
   */

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  /** Comboliste de selection de l element de d�part */
  String[] listeElt = { "Chenal", "Cercle", "Ecluse", "Quai" };
  JComboBox listeQuais_ = new JComboBox();
  /** Comboliste de selection de l element d'arrivee */
  /** horaire de d�part */

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  JCheckBox choixNbNavires_ = new JCheckBox("Nombre navires", true);

  JCheckBox choixTempsService_ = new JCheckBox("TS total", true);
  JCheckBox choixTempsServiceMoy_ = new JCheckBox("R�partition TS", true);
  JCheckBox choixTonnage_ = new JCheckBox("Tonnage", true);
  JCheckBox choixTonnage2_ = new JCheckBox("R�partition tonnage", true);

  JCheckBox[] tableauChoixNavires_;

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "NbNav.Quai", "TpsServiceTotal", "TS moyen", "Tonnage" };
  // 4

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();

  /**
   * Panel contenant le tableau et les boutons de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatsOccupationQuai(final SiporDataSimulation _donnees) {
    super("Occupation des quais d�taill�e", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le chenal 0 et le quai 0 dans l horaire entre 0 et 24

    setSize(820, 600);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);

    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipalAffichage_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    panelPrincipalAffichage_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.getCategoriesNavires_().getListeNavires_().size()];
    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom(), true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }
    for (int i = 0; i < this.donnees_.getlQuais_().getlQuais_().size(); i++) {
      listeQuais_.addItem(this.donnees_.getlQuais_().retournerQuais(i).getNom());
    }
    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(1, 1));

    selectionPanel1 = new BuPanel();
    selectionPanel1.add(new JLabel("S�lectionner le quai:"));
    selectionPanel1.add(this.listeQuais_);
    selectionPanel1.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel1);
    selectionPanel1.add(lancerRecherche_);

    this.selectionPanel_.setBorder(this.compound_);


    this.lancerRecherche_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    final JTabbedPane panoption = new JTabbedPane();
    this.optionPanel_.add(panoption);

    final Box bVert = Box.createVerticalBox();
    final JScrollPane pcnasc1 = new JScrollPane(bVert);
    panoption.addTab("Occup", pcnasc1);
    // bVert.add(new JLabel("Options Affichage:"));
    bVert.add(new JLabel(""));
    bVert.add(this.choixNbNavires_);
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Options Affichage");
    bVert.setBorder(bordure1);
    bVert.add(this.choixTempsService_);
    bVert.add(this.choixTempsServiceMoy_);
    bVert.add(this.choixTonnage_);
    bVert.add(this.choixTonnage2_);


    final Box bVert2 = Box.createVerticalBox();
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Affichage navires");
    bVert2.setBorder(bordure2);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    panoption.addTab("Navires", pcnasc);

    this.optionPanel_.setBorder(this.compound_);

    // listener des checkbox de choix des options d affichage

    this.choixTonnage_.addActionListener(this);
    this.choixTonnage2_.addActionListener(this);

    this.choixTempsService_.addActionListener(this);
    this.choixTempsServiceMoy_.addActionListener(this);
    this.choixNbNavires_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(new JLabel("S�lectionnez la cat�gorie � visualiser:   "));
    this.controlPanel_.add(this.ListeNavires_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatsOccupationQuai.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    final JScrollPane panneauHisto = new JScrollPane(this.histo_);
    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(/* this.histo_ */panneauHisto, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatsOccupationQuai.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatsOccupationQuai.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // affichage du tableau

    // recuperation de l'indice du quai que l on veut visualiser:
    final int ELEMENTCHOISI = this.listeQuais_.getSelectedIndex();

    // operation magique qui permet de determiner ce tableau
    int indiceTbaleau = 0;
    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.getCategoriesNavires_().getListeNavires_().size()][5];

    if (val < 0) {

      for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
        if (this.tableauChoixNavires_[i].isSelected()) {
          data[indiceTbaleau][0] = this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom();

          // ecriture des donn�es calcul�es pour les dur�es de parcours
          // si les cases correspondantes ont �t� coch�es:
          if (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[i].tempsServiceTotalAuQuai != 0) {
            int indiceColonne = 1;
            if (this.choixNbNavires_.isSelected()) {
              data[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[i].nbNaviresUtiliseQuai);
            }

            if (this.choixTempsService_.isSelected()) {
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[i].tempsServiceTotalAuQuai/FonctionsSimu.NOMBRE_SIMULATIONS);
            }

            if (this.choixTempsServiceMoy_.isSelected()) {
              data[indiceTbaleau][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[i].tempsServiceTotalAuQuai / (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[i].nbNaviresUtiliseQuai));
            }

            if (this.choixTonnage_.isSelected()) {
              data[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu((int) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[i].tonnage);
            }
          }
          indiceTbaleau++;
        }// fin du if si la cat�gorie a �t� selectionn�e pour etre affich�e
      }

    } else if (val < this.donnees_.getCategoriesNavires_().getListeNavires_().size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data = new Object[1][this.titreTableau_.length];
      data[0][0] = this.donnees_.getCategoriesNavires_().retournerNavire(val).getNom();
      // on complete les don�nes par le tableau de resultats:
      // data[0][0]..........
      int indiceColonne = 1;
      if (this.choixNbNavires_.isSelected()) {
        data[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[val].nbNaviresUtiliseQuai);
      }

      if (this.choixTempsService_.isSelected()) {
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float)this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[val].tempsServiceTotalAuQuai/FonctionsSimu.NOMBRE_SIMULATIONS);

      }
      if (this.choixTempsServiceMoy_.isSelected()) {
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[val].tempsServiceTotalAuQuai / (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[val].nbNaviresUtiliseQuai));

      }

      if (this.choixTonnage_.isSelected()) {
        data[0][indiceColonne++] = ""
            + (int) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[val].tonnage);

      }

    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    final int ELEMENTCHOISI = this.listeQuais_.getSelectedIndex();

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Occupations du quai " + (String) this.listeQuais_.getSelectedItem();

    g += " \"\n";
    if (this.choixNbNavires_.isSelected()) {
      g += "  sous-titre \" Nombre de navires au quai \"\n";
    } else if (this.choixTempsService_.isSelected()) {
      g += "  sous-titre \" Temps de service en fonction des cat�gories \"\n";
    } else if (this.choixTempsServiceMoy_.isSelected()) {
      g += "  sous-titre \" Temps de service moyen \"\n";
    } else if (this.choixTonnage_.isSelected()) {
      g += "  sous-titre \" Tonnage en fonction des cat�gories \"\n";
    } else if (this.choixTonnage2_.isSelected()) {
      g += "  sous-titre \" R�partition du tonnage en fonction des cat�gories \"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    if (this.choixNbNavires_.isSelected()) {
      g += "    titre \"nombre" + "\"\n";
      g += "    unite \"" + "navires" + "\"\n";
    } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && !this.choixTempsServiceMoy_.isSelected()
        && (this.choixTonnage_.isSelected() || this.choixTonnage2_.isSelected()))

    {
      g += "    titre \"tonnage" + "\"\n";
      g += "    unite \"" + "tonnes" + "\"\n";
    }

    else {
      g += "    titre \"duree" + "\"\n";
      g += "    unite \"" + "H.min" + "\"\n";
    }
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (this.choixNbNavires_.isSelected()) {
      g += (float)FonctionsSimu.diviserSimu( SiporAlgorithmeOccupationsQuais.determineNbNaviresMax(donnees_));
    } else if (!this.choixNbNavires_.isSelected() && this.choixTempsService_.isSelected()) {
      g += SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeOccupationsQuais
          .determineTempsServiceTotalMax(donnees_)/FonctionsSimu.NOMBRE_SIMULATIONS);
    } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && this.choixTempsServiceMoy_.isSelected()) {
      g += SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeOccupationsQuais
          .determineTempsServiceMax(donnees_));
    } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && !this.choixTempsServiceMoy_.isSelected() && this.choixTonnage_.isSelected()) {
      g += (float)FonctionsSimu.diviserSimu( SiporAlgorithmeOccupationsQuais.determineTonnageMax(donnees_));
    } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && !this.choixTempsServiceMoy_.isSelected() && !this.choixTonnage_.isSelected()
        && this.choixTonnage2_.isSelected()) {
      g += (float) SiporAlgorithmeOccupationsQuais.determineMaxRepartitionTonnage(donnees_);
    }
    // DETERMINE LE MAX
    g += "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    g += "  courbe\n  {\n";
    g += "    titre \"";
    if (this.choixNbNavires_.isSelected()) {
      g += "  nombres de navires";
    } else if (this.choixTempsService_.isSelected()) {
      g += "Temps de service total";
    } else if (this.choixTempsServiceMoy_.isSelected()) {
      g += "  temps de service maxi";
    } else if (this.choixTonnage_.isSelected() || this.choixTonnage2_.isSelected()) {
      g += "Tonnage";
    }

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur   BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BB0000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixNbNavires_.isSelected()) {
          g +=FonctionsSimu.diviserSimu( this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].nbNaviresUtiliseQuai);
        } else if (this.choixTempsService_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceTotalAuQuai/FonctionsSimu.NOMBRE_SIMULATIONS);
        } else if (this.choixTempsServiceMoy_.isSelected()) {
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceMaxiAuQuai);
        } else if (this.choixTonnage_.isSelected()) {
          g += (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnage);
        } else if (this.choixTonnage2_.isSelected()) {
          g += this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnageMax;
        }

        g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
            + "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo max************************************************

    if (this.choixTempsServiceMoy_.isSelected() && !this.choixNbNavires_.isSelected()
        && !this.choixTempsService_.isSelected())

    {
      // ******************************debut histo moy************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";

      g += "temps de service moyen";

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB8800 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceTotalAuQuai / (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].nbNaviresUtiliseQuai));

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************

      // ******************************debut histo min************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";
      g += "temps de service mini";

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BBC00 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceMiniAuQuai);

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************
    }// fin si cest temps de service moyen

    // //******************************fin histo max************************************************

    if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && !this.choixTempsServiceMoy_.isSelected() && !this.choixTonnage_.isSelected()
        && this.choixTonnage2_.isSelected())

    {
      // ******************************debut histo moy************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";

      g += "tonnage moyen";

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB8800 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnage
              / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].nbNaviresUtiliseQuai;

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************

      // ******************************debut histo min************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";
      g += "temps de service mini";

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BBC00 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnageMin;

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************
    }// fin si cest temps de service moyen

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /*********************************************************************************************************************
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   ********************************************************************************************************************/
  String affichageHistogramme() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    final int ELEMENTCHOISI = this.listeQuais_.getSelectedIndex();

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Occupations du quai " + (String) this.listeQuais_.getSelectedItem();

    g += " \"\n";
    if (this.choixNbNavires_.isSelected()) {
      g += "  sous-titre \" Nombre de navires au quai \"\n";
    } else if (this.choixTempsService_.isSelected()) {
      g += "  sous-titre \" Temps de service en fonction des cat�gories \"\n";
    } else if (this.choixTempsServiceMoy_.isSelected()) {
      g += "  sous-titre \" Temps de service moyen \"\n";
    } else if (this.choixTonnage_.isSelected()) {
      g += "  sous-titre \" Tonnage en fonction des cat�gories \"\n";
    } else if (this.choixTonnage2_.isSelected()) {
      g += "  sous-titre \" R�partition tonnage en fonction des cat�gories \"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    if (this.choixNbNavires_.isSelected()) {
      g += "    titre \"nombre" + "\"\n";
      g += "    unite \"" + "navires" + "\"\n";
    } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && !this.choixTempsServiceMoy_.isSelected()
        && (this.choixTonnage_.isSelected() || this.choixTonnage2_.isSelected()))

    {
      g += "    titre \"tonnage" + "\"\n";
      g += "    unite \"" + "tonnes" + "\"\n";
    } else {
      g += "    titre \"duree" + "\"\n";
      g += "    unite \"" + "H.min" + "\"\n";
    }

    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (this.choixNbNavires_.isSelected()) {
        g += (float)FonctionsSimu.diviserSimu( SiporAlgorithmeOccupationsQuais.determineNbNaviresMax(donnees_));
      } else if (!this.choixNbNavires_.isSelected() && this.choixTempsService_.isSelected()) {
        g += SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeOccupationsQuais
            .determineTempsServiceTotalMax(donnees_)/FonctionsSimu.NOMBRE_SIMULATIONS);
      } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
          && this.choixTempsServiceMoy_.isSelected()) {
        g += SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeOccupationsQuais
            .determineTempsServiceMax(donnees_));
      } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
          && !this.choixTempsServiceMoy_.isSelected() && this.choixTonnage_.isSelected()) {
        g += (float)FonctionsSimu.diviserSimu( SiporAlgorithmeOccupationsQuais.determineTonnageMax(donnees_));
      } else if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
          && !this.choixTempsServiceMoy_.isSelected() && !this.choixTonnage_.isSelected()
          && this.choixTonnage2_.isSelected()) {
        g += (float) SiporAlgorithmeOccupationsQuais.determineMaxRepartitionTonnage(donnees_);
      }

    // DETERMINE LE MAX
    g += "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    g += "  courbe\n  {\n";
    g += "    titre \"";
    if (this.choixNbNavires_.isSelected()) {
      g += "  nombres de navires";
    } else if (this.choixTempsService_.isSelected()) {
      g += "Temps de service total";
    } else if (this.choixTempsServiceMoy_.isSelected()) {
      g += "  temps de service maxi";
    } else if (this.choixTonnage_.isSelected()) {
      g += "Tonnage";
    } else if (this.choixTonnage2_.isSelected()) {
      g += "Tonnage max";
    }

    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur " + 1 + " \n";
    g += "surface.couleur   BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixNbNavires_.isSelected()) {
            g +=FonctionsSimu.diviserSimu( this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].nbNaviresUtiliseQuai);
          } else if (this.choixTempsService_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceTotalAuQuai/FonctionsSimu.NOMBRE_SIMULATIONS);
          } else if (this.choixTempsServiceMoy_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceMaxiAuQuai);
          } else if (this.choixTonnage_.isSelected()) {
            g += (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnage);
          } else if (this.choixTonnage2_.isSelected()) {
            g += this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnageMax;
          }

        g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
            + "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo max************************************************

    if (this.choixTempsServiceMoy_.isSelected() && !this.choixNbNavires_.isSelected()
        && !this.choixTempsService_.isSelected()) {
      // ******************************debut histo moy************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";

      g += "temps de service moyen";

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur " + 1 + " \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceTotalAuQuai / (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].nbNaviresUtiliseQuai));

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************

      // ******************************debut histo min************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";
      g += "temps de service mini";

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur " + 1 + " \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tempsServiceMiniAuQuai);

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************
    }// fin si cest temps de service moyen

    if (!this.choixNbNavires_.isSelected() && !this.choixTempsService_.isSelected()
        && !this.choixTempsServiceMoy_.isSelected() && !this.choixTonnage_.isSelected()
        && this.choixTonnage2_.isSelected())

    {
      // ******************************debut histo moy************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";

      g += "tonnage moyen";

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur " + 1 + " \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnage
              / this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].nbNaviresUtiliseQuai;

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************

      // ******************************debut histo min************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";
      g += "tonnage mini";

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur " + 1 + " \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          g += this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[ELEMENTCHOISI].tableauOccupationQuaiParNavires[n].tonnageMin;

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      // //******************************fin histo moy************************************************
    }// fin si cest temps de service moyen

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
    if (source == this.choixNbNavires_ || source == this.choixTonnage_ || source == this.choixTonnage2_
        || source == this.choixTempsService_ || source == this.choixTempsServiceMoy_) {
      // clic sur un checkBox:
      // construction de la colonne des titres

      if (!this.choixTempsService_.isSelected() && !this.choixTempsServiceMoy_.isSelected()
          && !this.choixTonnage_.isSelected() && !this.choixTonnage2_.isSelected()) {
        this.choixTempsServiceMoy_.setSelected(true);
      }

      int compteurColonnes = 0;
      if (this.choixTonnage_.isSelected()) {
        compteurColonnes += 1;
      }
      if (this.choixTempsService_.isSelected()) {
        compteurColonnes += 1;
      }
      if (this.choixTempsServiceMoy_.isSelected()) {
        compteurColonnes += 1;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
      }

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;

      this.titreTableau_[indiceColonne++] = "Cat�gorie";
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "NbNav.Quai";
      }

      if (this.choixTempsService_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "TpsServiceTotal";

      }
      if (this.choixTempsServiceMoy_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "TS moyen";

      }
      if (this.choixTonnage_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Tonnage";

      }

      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.lancerRecherche_) {
      // lancement des calculs pour les dur�es de parcours:

      // mise a jour des affichages:
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      // }
    } else if (source == this.valideSeuil_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
