package org.fudaa.fudaa.sipor.structures;

import java.io.Serializable;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;



/**
 * Classe de descrieption d un cercle
 * 
 * @author Adrien Hadoux
 */
public class SiporCercle implements NetworkUserObject, Serializable{

  String nom_;

  /**
   * regles de navigations du chenal
   */
  SiporStrutureReglesNavigation reglesNavigation_;

  SiporStrutureGenes regleGenes; 
  
  public   SiporCercle() {
	  this(0);
  }
  
  public   SiporCercle(final int numNavires) {
    // initialisation de la matrice du chenal
    this.reglesNavigation_ = new SiporStrutureReglesNavigation(numNavires);
    //-- AHX- GESTION genes 2011 - genes --//
    regleGenes=new SiporStrutureGenes(numNavires);
  }

  int gareAmont_ = 0;
  int gareAval_ = 1;

  public  String[] affichage() {
    final String[] t = new String[1];
    t[0] = " nom: " + nom_;
    return t;
  }

public String getNom_() {
	return nom_;
}

public void setNom_(String nom_) {
	this.nom_ = nom_;
}

public SiporStrutureReglesNavigation getReglesNavigation_() {
	return reglesNavigation_;
}

public void setReglesNavigation_(SiporStrutureReglesNavigation reglesNavigation_) {
	this.reglesNavigation_ = reglesNavigation_;
}

public SiporStrutureGenes getRegleGenes() {
	return regleGenes;
}

public void setRegleGenes(SiporStrutureGenes regleGenes) {
	this.regleGenes = regleGenes;
}

public int getGareAmont_() {
	return gareAmont_;
}

public void setGareAmont_(int gareAmont_) {
	this.gareAmont_ = gareAmont_;
}

public int getGareAval_() {
	return gareAval_;
}

public void setGareAval_(int gareAval_) {
	this.gareAval_ = gareAval_;
}

public String getName() {
	return getNom_();
}

public void setName(String value) {
	setNom_(value);
}

}