/**
 *@creation 9 oct. 06
 *@modification $Dates$
 *@license GNU General Public Licence 2
 *@copyright (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;

import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.structures.CreneauxLoiJournaliere;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Fenetre de saisie des crneaux de al loi journaliere de la fenetre de saisie des quais
 * 
 * @author Adrien Hadoux
 */
public class SiporFrameSaisieLoiJournaliere2 extends SiporInternalFrame {

  // attributs

  private static final String INTERVALLES_DE_TEMPS_DISTINCTS = "Erreur! Cet horaire est inclus dans le premier cr�neau. Les cr�neaux sont des intervalles de temps distincts.";

  /**
   * Panel global contenant toutes les donnees de la frame
   */
  JPanel global_;

  /**
   * JText des creneaux a saisir
   */
  JTextField creneau1debut_ = new JTextField(3);
  JTextField creneau1fin_ = new JTextField(3);
  JTextField creneau2debut_ = new JTextField(3);
  JTextField creneau2fin_ = new JTextField(3);
  JTextField creneau3debut_ = new JTextField(3);
  JTextField creneau3fin_ = new JTextField(3);

  /**
   * Bouton de validation des horaires lance les tests decontroles de cohrence des donnes
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal22_oui"), "Valider");

  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * Objet horaire qui sera rempli suite a la saisie de l utilisateur
   */
  CreneauxLoiJournaliere creneauLoiJournaliere_;

  /**
   * Constructeur de la fenetre de saisie des horaires
   * 
   * @param h horaire qui va etre rempli par l'utilisateur
   */

  SiporFrameSaisieLoiJournaliere2(final CreneauxLoiJournaliere _creneauLoiJournaliere) {

    super("", true, true, true, true);
    creneauLoiJournaliere_ = _creneauLoiJournaliere;
    setTitle("Saisie d'une loi journali�re");
    setSize(415, 180);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));

    /**
     * position relative a la position parent
     */

    /**
     * *************************************************** controles sur les composants
     * **************************************************
     */

    // listener du bouton de validation:
    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        creationHoraire();
      }
    });

    this.creneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.creneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.creneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.creneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.creneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.creneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);

    // valeur recuperee de la structure horaire
    this.creneau1debut_.setText("" + (float) creneauLoiJournaliere_.creneau1deb);
    this.creneau1fin_.setText("" + (float) creneauLoiJournaliere_.creneau1fin);
    this.creneau2debut_.setText("" + (float) creneauLoiJournaliere_.creneau2deb);
    this.creneau2fin_.setText("" + (float) creneauLoiJournaliere_.creneau2fin);
    this.creneau3debut_.setText("" + (float) creneauLoiJournaliere_.creneau3deb);
    this.creneau3fin_.setText("" + (float) creneauLoiJournaliere_.creneau3fin);

    this.creneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        if (!creneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1debut_.getText());
            if (i < 0) {
              
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              creneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24,
                  MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            creneau1debut_.setText("");
          }
        }
      }
    });

    this.creneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!creneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              creneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24,
                  MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau1fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(creneau1debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur! Cet horaire de fin est inf�rieur � l'horaire de d�part!", MessageConstants.WARN,
                    JOptionPane.ERROR_MESSAGE);
                creneau1fin_.setText("");

              }
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            creneau1fin_.setText("");
          }
        }
      }
    });

    this.creneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!creneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              creneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24,
                  MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau2debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(creneau1debut_.getText()) && i < Float.parseFloat(creneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      INTERVALLES_DE_TEMPS_DISTINCTS,
                      MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            creneau2debut_.setText("");
          }
        }
      }
    });

    this.creneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!creneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              creneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24,
                  MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau2fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(creneau2debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur! Cet horaire de fin est inf�rieur � l'horaire de d�part.", MessageConstants.WARN,
                    JOptionPane.ERROR_MESSAGE);
                creneau2fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(creneau1debut_.getText()) && i < Float.parseFloat(creneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        INTERVALLES_DE_TEMPS_DISTINCTS,
                        MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
                creneau2fin_.setText("");
              }

            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            creneau2fin_.setText("");
          }
        }
      }
    });

    this.creneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              creneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24,
                  MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(creneau1debut_.getText()) && i < Float.parseFloat(creneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      INTERVALLES_DE_TEMPS_DISTINCTS,
                      MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent avec l intervalle 2
            if (i > Float.parseFloat(creneau2debut_.getText()) && i < Float.parseFloat(creneau2fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur! Cet horaire est inclus dans le deuxi�me cr�neau. Les cr�neaux sont des intervalles de temps distincts.",
                      MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            creneau3debut_.setText("");
          }
        }
      }
    });

    this.creneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        if (!creneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              creneau3fin_.setText("");
            } else if (i > 24) {
              
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24,
                  MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
              creneau3fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(creneau3debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur! Cet horaire de fin est inf�rieur � l'horaire de d�part.", MessageConstants.WARN,
                    JOptionPane.ERROR_MESSAGE);
                creneau3fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(creneau1debut_.getText()) && i < Float.parseFloat(creneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        INTERVALLES_DE_TEMPS_DISTINCTS,
                        MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
                creneau3fin_.setText("");
              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(creneau2debut_.getText()) && i < Float.parseFloat(creneau2fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur! Cet horaire est inclus dans le deuxi�me cr�neau! Les cr�neaux sont des intervalles de temps distincts.",
                        MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
                creneau3fin_.setText("");
              }

            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            creneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    global_ = new JPanel();
    global_.setLayout(new GridLayout(4, 1));

    // panel de saisie du premier Cr�neau horaire
    final JPanel ch1 = new JPanel();

    ch1.add(new JLabel(" Cr�neau 1: Horaire: "));
    ch1.add(this.creneau1debut_);
    ch1.add(new JLabel("-"));
    ch1.add(this.creneau1fin_);
    ch1.add(new JLabel("HEURES.MINUTES"));
    ch1.setBorder(bordnormal_);
    global_.add(ch1);

    // panel de saisie du deuxieme horaire
    final JPanel ch2 = new JPanel();
    ch2.add(new JLabel(" Cr�neau 2: Horaire: "));
    ch2.add(this.creneau2debut_);
    ch2.add(new JLabel("-"));
    ch2.add(this.creneau2fin_);
    ch2.add(new JLabel("HEURES.MINUTES"));
    ch2.setBorder(bordnormal_);

    global_.add(ch2);

    // //panel de saisie du troisieme horaire
    final JPanel ch4 = new JPanel();
    ch4.add(new JLabel(" Cr�neau 3: Horaire: "));
    ch4.add(this.creneau3debut_);
    ch4.add(new JLabel("-"));
    ch4.add(this.creneau3fin_);
    ch4.add(new JLabel("HEURES.MINUTES"));
    ch4.setBorder(bordnormal_);

    global_.add(ch4);

    // panel qui contient le bouton de validation:
    final JPanel ch3 = new JPanel();
    ch3.add(new JLabel("Cliquez ici pour valider: "));
    ch3.add(this.validation_);
    ch3.setBorder(bordnormal_);
    global_.add(ch3);
    global_.setBorder(compound_);
    getContentPane().add(global_);

    // affichage de la fenetre
    setVisible(true);

  }

  /**
   * Methode booleene de validation des donnes saisies
   */
  boolean controle_creationHoraire() {

    if (this.creneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 1: heure de d�part manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 1: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.creneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 2: heure de d�part manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 2: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // tous les tests ont t ngatifs, les donnes sont donc correctes.
    return true;
  }

  /**
   * Methode de controle de creation des horaires
   */

  void creationHoraire() {

    if (controle_creationHoraire()) {
      System.out.println("yes: tous les controles ont t vrifi, les donnes sont cohrentes!!");

      // creation d'un nouvel objet horaire:
      creneauLoiJournaliere_.creneau1deb = Float.parseFloat(this.creneau1debut_.getText());
      creneauLoiJournaliere_.creneau1fin = Float.parseFloat(this.creneau1fin_.getText());
      creneauLoiJournaliere_.creneau2deb = Float.parseFloat(this.creneau2debut_.getText());
      creneauLoiJournaliere_.creneau2fin = Float.parseFloat(this.creneau2fin_.getText());
      creneauLoiJournaliere_.creneau3deb = Float.parseFloat(this.creneau3debut_.getText());
      creneauLoiJournaliere_.creneau3fin = Float.parseFloat(this.creneau3fin_.getText());

      // destruction de la frame:
      this.dispose();

    }

  }

}
