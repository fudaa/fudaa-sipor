/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license GNU General Public Licence 2
 *@copyright (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sipor.ui.frame;

/**
 * Fenetre de saisie des parametres des mar�es saisie, modification et affichage des donn�es....
 * 
 * @version $Version$
 * @author adrien hadoux
 */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import org.apache.commons.lang.StringUtils;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.sipor.SParametresMaree2;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.algorithmes.MareeDouziemes;
import org.fudaa.fudaa.sipor.factory.SiporResource;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporTableModelMaree;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporCellEditor;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldDuree;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldFloat;

public class SiporFrameSaisieMaree extends SiporInternalFrame implements MareeListener{

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

/**
   * bouton e validation des donn�es.
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal22_oui"), "Valider");

  // les principaux composants de la fenetre

  SiporTextFieldDuree periodeviveeau_ = new SiporTextFieldDuree(7);
  SiporTextFieldDuree periodemaree_ = new SiporTextFieldDuree(7);
  SiporTextFieldFloat hauteurMer_ = new SiporTextFieldFloat(7);
  SiporTextFieldFloat niveauHauteMerMorteEau_ = new SiporTextFieldFloat(7);
  SiporTextFieldFloat niveauHauteMerViveEau_ = new SiporTextFieldFloat(7);
  SiporTextFieldFloat niveauBasseMerViveEau_ = new SiporTextFieldFloat(7);
  SiporTextFieldFloat niveauBasseMerMorteEau_ = new SiporTextFieldFloat(7);
  
  BGraphe schema = new BGraphe();
  double tableauDouziemes_[];
  /**
   * Bordures de la fenetre des mar�es.
   */
  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * parametres de la simulation.
   */
  SiporDataSimulation donnees_;

  JTabbedPane classeur_ = new JTabbedPane();

  /**
   * Constructeur de la fenetre de saisie des parametres des mar�es.
   * 
   * @param donnees_ donn�es de al simulation
   */
  public SiporFrameSaisieMaree(final SiporDataSimulation _donnees) {
    super(CtuluLibString.EMPTY_STRING, true, true, true, true);

    // recuperation des donn�es
    donnees_ = _donnees;


    setSize(1100, 700);
    setTitle("Donn�es des mar�es");
    setBorder(SiporBordures.compound_);
    this.periodemaree_.setToolTipText("P�riode en heure.minutes de la mar�e. en France, initialis�e � 12h26 (ie: 12.26 )");
    this.periodeviveeau_
        .setToolTipText("P�riode des vives eaux en heures.minutes. Par d�faut, initialis�e � 354h (ie: 354)");
    this.hauteurMer_.setToolTipText("Hauteur moyenne de la mer en m�tres");
    this.niveauBasseMerMorteEau_.setToolTipText("Moyenne du niveau de la basse mer en p�riode de morte eaux en m�tres");
    this.niveauBasseMerViveEau_.setToolTipText("Moyenne du niveau de la basse mer en p�riode de vive eaux en m�tres");
    this.niveauHauteMerMorteEau_.setToolTipText("Moyenne du niveau de la haute mer en p�riode de morte eaux en m�tres");
    this.niveauHauteMerViveEau_.setToolTipText("Moyenne du niveau de la haute mer en p�riode de vive eaux en m�tres");

    /*******************************************************************************************************************
     * Controles des zones de saisies
     */
    initTableau();

    initFocusViveEeau();

    initFocusPeriodeMaree();

    initFocusHauteurMer();

    initFocusBasseMer();

    initFocusHauteMer();

    initFocusBasseMerViveEau();

    initFocusHauteMerViveEau();

    /**
     * listener de validation des donn�es
     */
    this.validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent _e) {
        creationMaree();

      }

    });

    // disposition des elements dans la frame
    final Container contenu = getContentPane();
    // creation d un card layout
    // gestionnaire de mise en forme card layout
    contenu.setLayout(new BorderLayout());

    final Box global = Box.createVerticalBox();
    global.setBorder(SiporBordures.compound_);

    final JPanel periode = new JPanel();

    periode.setLayout(new GridLayout(2, 1));

    final JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    p1.add(new JLabel("P�riode vive eaux: "));
    p1.add(this.periodeviveeau_);
    p1.add(new JLabel("Heures.Minutes"));
    p1.setBorder(SiporBordures.bordnormal_);
    periode.add(p1);

    final JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    p2.add(new JLabel("P�riode mar�e:"));
    p2.add(this.periodemaree_);
    p2.add(new JLabel("Heures.Minutes"));
    p2.setBorder(SiporBordures.bordnormal_);
    periode.add(p2);

    final TitledBorder bordure1 = BorderFactory.createTitledBorder(SiporBordures.compound_, "P�riode");
    periode.setBorder(bordure1);

    global.add(periode);

    final JPanel phauteur = new JPanel(new FlowLayout(FlowLayout.LEFT));
    phauteur.add(new JLabel("Hauteur moyenne de la mer: "));
    phauteur.add(this.hauteurMer_);
    phauteur.add(createUnitLabel());
    phauteur.setBorder(SiporBordures.bordnormal_);

    final TitledBorder bordure11 = BorderFactory.createTitledBorder(SiporBordures.compound_, "Hauteur");
    phauteur.setBorder(bordure11);
    global.add(phauteur);

    initBasseMerPanel(global);

    initHauteMerPanel(global);

    // -- Interfaces JTable de saisie des douzi�mes --//
    double[] taka = new double[12];
    for (int i = 0; i < 12; i++)
      taka[i] = 0.0;

    final  SiporTableModelMaree modeleMaree = new SiporTableModelMaree(tableauDouziemes_);
    modeleMaree.setListener(this);
    BuTable tableauMarees = new BuTable(modeleMaree);
    tableauMarees.setDefaultEditor(Object.class, new SiporCellEditor());
    tableauMarees.getColumnModel().getColumn(0).setPreferredWidth(50);

    BuPanel afficheSchema = new BuPanel(new GridLayout(1, 1));
    afficheSchema.add(schema);

    afficheSchema.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Sch�ma"));

    JPanel conteneurTableau = new JPanel(new BorderLayout());
    conteneurTableau.add(tableauMarees, BorderLayout.CENTER);
    conteneurTableau.add(tableauMarees.getTableHeader(), BorderLayout.PAGE_START);
    conteneurTableau.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Saisie des douzi�mes"));

    // panel des douziemes
    JSplitPane conteneurDouziemes = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, afficheSchema, conteneurTableau);
    conteneurDouziemes.setDividerLocation(900);
    conteneurDouziemes.setDividerSize(2);

    // -- Mise en place du classeur --//
    contenu.add(classeur_, BorderLayout.CENTER);
    classeur_.addTab("G�n�ral", global);
    classeur_.addTab("Saisie des douzi�mes", conteneurDouziemes);
    classeur_.addChangeListener(new ChangeListener() {
		
		public void stateChanged(ChangeEvent e) {
			renderMaree(modeleMaree.getTableau_());
			
		}
	});
    final JPanel controlPanel = new JPanel();
    controlPanel.add(new JLabel("Valider la saisie:"));
    controlPanel.add(validation_);
    controlPanel.setBorder(SiporBordures.compound_);
    contenu.add(controlPanel, BorderLayout.SOUTH);

    // initialisation des donn�es
    initialiser();

    // affichage de al fenetre
    this.setVisible(true);
  }

  private JLabel createUnitLabel() {
    return new JLabel("Metres");
  }

  private void initTableau() {

    this.tableauDouziemes_ = new double[donnees_.getParams_().maree.tableauDouzaines.length];
    for (int i = 0; i < donnees_.getParams_().maree.tableauDouzaines.length; i++)
      this.tableauDouziemes_[i] = donnees_.getParams_().maree.tableauDouzaines[i];
  }

  private void initHauteMerPanel(final Box _global) {
    final JPanel hautemer = new JPanel();
    hautemer.setLayout(new GridLayout(2, 1));

    final JPanel b3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    b3.add(createHauteMerLabel());
    b3.add(new JLabel("P�riode Morte Eau:"));
    b3.add(this.niveauHauteMerMorteEau_);
    b3.add(createUnitLabel());
    b3.add(new JLabel("   (coef 45)"));
    b3.setBorder(SiporBordures.bordnormal_);
    hautemer.add(b3);

    final JPanel b4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    b4.add(createHauteMerLabel());
    b4.add(new JLabel("P�riode Vive Eau:"));
    b4.add(this.niveauHauteMerViveEau_);
    b4.add(createUnitLabel());
    b4.add(new JLabel("   (coef 95)"));
    b4.setBorder(SiporBordures.bordnormal_);
    hautemer.add(b4);

    final TitledBorder bordure3 = BorderFactory.createTitledBorder(SiporBordures.compound_, "Haute mer");
    hautemer.setBorder(bordure3);

    _global.add(hautemer);
  }

  private JLabel createHauteMerLabel() {
    return new JLabel("Haute Mer:  ");
  }

  private void initBasseMerPanel(final Box _global) {
    final JPanel bassemer = new JPanel();
    bassemer.setLayout(new GridLayout(2, 1));

    final JPanel b1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    b1.add(createBasseMerLabel());
    b1.add(new JLabel("P�riode Morte Eau:"));
    b1.add(this.niveauBasseMerMorteEau_);
    b1.add(createUnitLabel());
    b1.add(new JLabel("   (coef 45)"));
    b1.setBorder(SiporBordures.bordnormal_);
    bassemer.add(b1);

    final JPanel b2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    b2.add(createBasseMerLabel());
    b2.add(new JLabel("P�riode Vive Eau:"));
    b2.add(this.niveauBasseMerViveEau_);
    b2.add(createUnitLabel());
    b2.add(new JLabel("   (coef 95)"));
    b2.setBorder(SiporBordures.bordnormal_);
    bassemer.add(b2);

    final TitledBorder bordure2 = BorderFactory.createTitledBorder(SiporBordures.compound_, "Basse mer");
    bassemer.setBorder(bordure2);

    _global.add(bassemer);
  }

  private JLabel createBasseMerLabel() {
    return new JLabel("Basse Mer:  ");
  }

  private void initFocusHauteMerViveEau() {
    this.niveauHauteMerViveEau_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent _e) {
        niveauHauteMerViveEau_.selectAll();
      }

      public void focusLost(final FocusEvent _e) {
        // test de validit
        if (!niveauHauteMerViveEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
          try {
            final float i = Float.parseFloat(niveauHauteMerViveEau_.getText());
            if (i < 0) {
              erreurNegatifNumber();
              niveauHauteMerViveEau_.setText(CtuluLibString.EMPTY_STRING);
            }
          } catch (final NumberFormatException nfe) {
            erreurWrongNumber();
            niveauHauteMerViveEau_.setText(CtuluLibString.EMPTY_STRING);
          }
        }
      }
    });
  }

  private void initFocusBasseMerViveEau() {
    this.niveauBasseMerViveEau_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent _e) {
        niveauBasseMerViveEau_.selectAll();
      }

      public void focusLost(final FocusEvent _e) {
        // test de validit
        if (!niveauBasseMerViveEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
          try {
            final float i = Float.parseFloat(niveauBasseMerViveEau_.getText());
            if (i < 0) {
              erreurNegatifNumber();
              niveauBasseMerViveEau_.setText(CtuluLibString.EMPTY_STRING);
            }
          } catch (final NumberFormatException nfe) {
            erreurWrongNumber();
            niveauBasseMerViveEau_.setText(CtuluLibString.EMPTY_STRING);
          }
        }
      }
    });
  }

  private void initFocusHauteMer() {
    this.niveauHauteMerMorteEau_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent _e) {
        niveauHauteMerMorteEau_.selectAll();
      }

      public void focusLost(final FocusEvent _e) {
        // test de validit
        if (!niveauHauteMerMorteEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
          try {
            final float i = Float.parseFloat(niveauHauteMerMorteEau_.getText());
            if (i < 0) {
              erreurNegatifNumber();
              niveauHauteMerMorteEau_.setText(CtuluLibString.EMPTY_STRING);
            }
          } catch (final NumberFormatException nfe) {
            erreurWrongNumber();
            niveauHauteMerMorteEau_.setText(CtuluLibString.EMPTY_STRING);
          }
        }
      }
    });
  }

  private void initFocusBasseMer() {
    this.niveauBasseMerMorteEau_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent _e) {
        niveauBasseMerMorteEau_.selectAll();
      }

      public void focusLost(final FocusEvent _e) {
        // test de validit
        if (!niveauBasseMerMorteEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
          try {
            final float i = Float.parseFloat(niveauBasseMerMorteEau_.getText());
            if (i < 0) {
              erreurNegatifNumber();
              niveauBasseMerMorteEau_.setText(CtuluLibString.EMPTY_STRING);
            }
          } catch (final NumberFormatException nfe) {
            erreurWrongNumber();
            niveauBasseMerMorteEau_.setText(CtuluLibString.EMPTY_STRING);
          }
        }
      }
    });
  }

  private void initFocusHauteurMer() {
    this.hauteurMer_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent _e) {
        hauteurMer_.selectAll();
      }

      public void focusLost(final FocusEvent _e) {
        // test de validit
        if (!hauteurMer_.getText().equals(CtuluLibString.EMPTY_STRING)) {
          try {
            final float i = Float.parseFloat(hauteurMer_.getText());
            if (i < 0) {
              erreurNegatifNumber();
              hauteurMer_.setText(CtuluLibString.EMPTY_STRING);
            }
          } catch (final NumberFormatException nfe) {
            erreurWrongNumber();
            hauteurMer_.setText(CtuluLibString.EMPTY_STRING);
          }
        }
      }
    });
  }

  private void initFocusPeriodeMaree() {
    this.periodemaree_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent _e) {
        periodemaree_.selectAll();
      }

      public void focusLost(final FocusEvent _e) {
        // test de validit
        if (!periodemaree_.getText().equals(CtuluLibString.EMPTY_STRING)) {
          try {
            final float i = Float.parseFloat(periodemaree_.getText());
            if (i < 0) {
              erreurNegatifNumber();
              periodemaree_.setText(CtuluLibString.EMPTY_STRING);
            }
          } catch (final NumberFormatException nfe) {
            erreurWrongNumber();
            periodemaree_.setText(CtuluLibString.EMPTY_STRING);
          }
        }
      }
    });
  }

  private void initFocusViveEeau() {

  }

  boolean verificationDonneesMarees() {

    if (this.periodeviveeau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "P�riode vive eau manquante.").activate();
      return false;
    } else if (this.periodemaree_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "P�riode mar�e manquante.").activate();
      return false;
    } else if (this.hauteurMer_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Hauteur moyenne de la mer manquante.").activate();
      return false;
    } else if (this.niveauBasseMerMorteEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Moyenne du niveau de la basse mer \nen p�riode de mortes eaux manquante.").activate();
      return false;
    } else if (this.niveauBasseMerViveEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Moyenne du niveau de la basse mer\n en p�riode de vives eaux manquante.").activate();
      return false;
    } else if (this.niveauHauteMerMorteEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Moyenne du niveau de la haute mer \nen p�riode de mortes eaux manquante.").activate();
      return false;
    } else if (this.niveauHauteMerViveEau_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Moyenne du niveau de la Haute mer \nen p�riode de vives eaux manquante.").activate();
      return false;
    }

    /**
     * verification des donn�es saisies pour les mortes eaux et vive eaux: hauteur
     */
    final float bmme = Float.parseFloat(this.niveauBasseMerMorteEau_.getText());
    final float bmve = Float.parseFloat(this.niveauBasseMerViveEau_.getText());
    final float hmme = Float.parseFloat(this.niveauHauteMerMorteEau_.getText());
    final float hmve = Float.parseFloat(this.niveauHauteMerViveEau_.getText());

    if (bmve > bmme) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Hauteur basse mer Vive eau est sup�rieure \n� la hauteur basse mer morte eau.").activate();
      return false;

    }
    if (hmve < hmme) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Hauteur haute mer Morte eau est sup�rieure \n� la hauteur haute mer vive eau.").activate();
      return false;

    }
    if (hmme < bmme) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Hauteur haute mer Morte eau est inf�rieure\n� la hauteur basse mer morte eau.").activate();
      return false;

    }

    // arriv� a ce stade, les don�nes ne pr�sentent plus d'erreurs possibles
    return true;
  }

  /**
   * Methode de creation des don�nes des marais verification de la coherence des saisies.
   */
  void creationMaree() {
    if (verificationDonneesMarees()) {
      /**
       * donn�es verifi�es; on peut valider els donn�es et les ajouter aux parametres:
       */
      // 1) allocation de memoire poru l element mar�e
      this.donnees_.getParams_().maree = new SParametresMaree2();
      this.donnees_.getParams_().maree.periodeViveEaux = Double.parseDouble(this.periodeviveeau_.getText());
      this.donnees_.getParams_().maree.periodeMaree = Double.parseDouble(this.periodemaree_.getText());
      this.donnees_.getParams_().maree.hauteurMoyenneMer = Double.parseDouble(this.hauteurMer_.getText());
      this.donnees_.getParams_().maree.niveauBasseMerMorteEau = Double.parseDouble(this.niveauBasseMerMorteEau_
          .getText());
      this.donnees_.getParams_().maree.niveauBasseMerViveEau = Double
          .parseDouble(this.niveauBasseMerViveEau_.getText());
      this.donnees_.getParams_().maree.niveauHauteMerMorteEau = Double.parseDouble(this.niveauHauteMerMorteEau_
          .getText());
      this.donnees_.getParams_().maree.niveauHauteMerViveEau = Double
          .parseDouble(this.niveauHauteMerViveEau_.getText());
      // -- recopie des douziemes--//
      this.donnees_.getParams_().maree.tableauDouzaines = tableauDouziemes_;

      new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
          "Les donn�es de la mar�e ont �t� ajout�es \navec succ�s!").activate();

      dispose();

    }

  }

  /**
   * methode permettant dinitialiser les parametres de saisie des mar�es.
   */
  void initialiser() {
    this.periodemaree_.setText(CtuluLibString.EMPTY_STRING + (float) this.donnees_.getParams_().maree.periodeMaree);
    this.periodeviveeau_
        .setText(CtuluLibString.EMPTY_STRING + (float) this.donnees_.getParams_().maree.periodeViveEaux);
    this.hauteurMer_.setText(CtuluLibString.EMPTY_STRING + (float) this.donnees_.getParams_().maree.hauteurMoyenneMer);
    this.niveauBasseMerMorteEau_.setText(CtuluLibString.EMPTY_STRING
        + this.donnees_.getParams_().maree.niveauBasseMerMorteEau);
    this.niveauBasseMerViveEau_.setText(CtuluLibString.EMPTY_STRING
        + this.donnees_.getParams_().maree.niveauBasseMerViveEau);
    this.niveauHauteMerMorteEau_.setText(CtuluLibString.EMPTY_STRING
        + this.donnees_.getParams_().maree.niveauHauteMerMorteEau);
    this.niveauHauteMerViveEau_.setText(CtuluLibString.EMPTY_STRING
        + this.donnees_.getParams_().maree.niveauHauteMerViveEau);

  }

  public void erreurNegatifNumber() {
    new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
        "Ce nombre est n�gatif.").activate();
  }

  public void erreurWrongNumber() {
    new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().getInformationsSoftware(),
        MessageConstants.NOMBRE_N_EXISTE_PAS).activate();
  }

public void renderMaree(double[] douziemes) {
	MareeDouziemes md = new MareeDouziemes();
	float hauteMer=0, basseMer=0, basseMerMorte=0, hauteMerMorte=0, periodeMer=12.25f ;
	
	if(niveauBasseMerViveEau_.getText() != null && niveauBasseMerViveEau_.getText().length()>0)
		basseMer = Float.parseFloat(niveauBasseMerViveEau_.getText());
	if(niveauHauteMerViveEau_.getText() != null && niveauHauteMerViveEau_.getText().length()>0)
		hauteMer = Float.parseFloat(niveauHauteMerViveEau_.getText());
	
	if(niveauBasseMerMorteEau_.getText() != null && niveauBasseMerMorteEau_.getText().length()>0)
		basseMerMorte = Float.parseFloat(niveauBasseMerMorteEau_.getText());
	if(niveauHauteMerMorteEau_.getText() != null && niveauHauteMerMorteEau_.getText().length()>0)
		hauteMerMorte = Float.parseFloat(niveauHauteMerMorteEau_.getText());
	
	if(this.periodemaree_.getText() != null && periodemaree_.getText().length()>0)
		periodeMer = Float.parseFloat(periodemaree_.getText());
	
	
	String resGraphe ="";
	try{		
			resGraphe = md.generateGrapheDouziemes(basseMer, hauteMer, periodeMer, douziemes,basseMerMorte,hauteMerMorte, douziemes);
	
	
    if (StringUtils.isNotBlank(resGraphe))
		schema.setFluxDonnees(new ByteArrayInputStream(resGraphe.getBytes()));
	}catch(Exception e) {
		FuLog.error(e.getMessage());
	}
}

}
