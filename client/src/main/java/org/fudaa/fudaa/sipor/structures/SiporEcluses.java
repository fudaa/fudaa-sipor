package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;

/**
 * Classe de gestion des cheneaux permet de stocker les donn�e relatives au bassin
 * 
 * @author Adrien Hadoux
 */

public class SiporEcluses {

  /**
   * Liste de string contenant les noms des bassins
   */
  ArrayList listeEcluses_ = new ArrayList();

  /**
   * Methode d'ajout d'une ecluse:
   * 
   * @param c �cluse � ajouter
   */

  public void ajout(final SiporEcluse c) {

    listeEcluses_.add(c);
    SiporDataSimulation.setProperty("ecluse");
  }

  /**
   * Methode qui retourne la i eme ecluse
   * 
   * @param i indice de l'ecluse du tableau d'��clusea retourner
   * @return un objet de typ �c�clusequi pourra etre modifie et renvoye
   */

  public SiporEcluse retournerEcluse(final int i) {
    if (i < this.listeEcluses_.size()) {
      return (SiporEcluse) this.listeEcluses_.get(i);
    } else {
      return null;
    }
  }

  /**
   * Methode de suppression du n ieme element de la liste de ecluses
   * 
   * @param n
   */
  public  void suppression(final int n) {

    this.listeEcluses_.remove(n);

  }

  /**
   * Methode de modification de la n ie �cluse par l'�cluse en parametre d entr�e
   * 
   * @param n indice de l'�clus�cluseifier
   * @para �cluse�cluseremplacer
   */
  public void modification(final int n, final SiporEcluse c) {
    this.listeEcluses_.set(n, c);

  }

  public  boolean existeDoublon(final String _nomComposant, final int k) {
    for (int i = 0; i < this.listeEcluses_.size(); i++) {
      if (i != k) {
        if (this.retournerEcluse(i).getNom_().equals(_nomComposant)) {
          return true;
        }
      }

    }

    // arriv�a ce stade; on a pas trouv�de doublons, le nom n'existe donc pas!
    return false;

  }

  public  int retourneIndice(final String nomEcluse) {
    for (int i = 0; i < this.listeEcluses_.size(); i++) {
      if (this.retournerEcluse(i).getNom_().equals(nomEcluse)) {
        return i;
      }
    }

    // aarriv� ce stade on a rien rettrouv�  
    return -1;
  }

public ArrayList getListeEcluses_() {
	return listeEcluses_;
}

public void setListeEcluses_(ArrayList listeEcluses_) {
	this.listeEcluses_ = listeEcluses_;
}

}
