package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuTable;

import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporEcluse;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des ecluses sous forme d un tableau: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelAffichageEcluse extends JPanel {

  public String[] titreColonnes = { "Nom", "Longueur", "Largeur", "Dur�e �clus�e", "Dur�e fausse bassin�e", "Dur�e passage �tale", "Cr�neau �tale",
      "Loi fr�quence", "Fr�quence indispo", "Dur�e indispo", "Loi Erlang", "Cr�neau 1", "Cr�neau 2", "Cr�neau 3" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des quais
   */

  public Object[][] data = {};

  public  Object[][] ndata;

  public BuTable tableau_ = new BuTable(data, titreColonnes);

  /**
   * Panel ascenceur
   */
  public  JScrollPane ascenceur;

  /**
   * Bordure du tableau
   */
  public   Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * constructeur du panel d'affichage des quais
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelAffichageEcluse(final SiporDataSimulation d) {

    setLayout(new BorderLayout());
    this.maj(d);

  }

  /**
   * Methode d'ajout d'une cellule
   */
  public void maj(final SiporDataSimulation d) {

    ndata = new Object[d.getListeEcluse_().getListeEcluses_().size()][14];

    for (int i = 0; i < d.getListeEcluse_().getListeEcluses_().size(); i++) {

      /**
       * MODIFICATION DES DONNEES: mettre les donn�es ecluses ici
       */
      final SiporEcluse q = d.getListeEcluse_().retournerEcluse(i);
      ndata[i][0] = q.getNom_();
      ndata[i][1] = "" + (float) q.getLongueur_();
      ndata[i][2] = "" + (float) q.getLargeur_();
      ndata[i][3] = "" + (float) q.getTempsEclusee_();
      ndata[i][4] = "" + (float) q.getTempsFausseBassinnee_();
      ndata[i][5] = "" + (float) q.getDureePassageEtale_();
      ndata[i][6] = " " + (float) q.getCreneauEtaleAvantPleineMerDeb_() + " � " + (float) q.getCreneauEtaleApresPleineMerFin_();;

      if (q.getTypeLoi_() == 0) {
        ndata[i][7] = "Loi d'Erlang: " + Integer.toString(q.getLoiFrequence_());
      } else if (q.getTypeLoi_() == 1) {
        ndata[i][7] = "Loi d�terministe: ";
      } else if (q.getTypeLoi_() == 2) {
        ndata[i][7] = "Loi journali�re: ";
      }

      if (q.getTypeLoi_() == 0) {
        ndata[i][8] = "" + (float) q.getFrequenceMoyenne_();
      } else {
        ndata[i][8] = "";
      }
      ndata[i][9] = "" + (float) q.getDureeIndispo_();
      ndata[i][10] = Integer.toString(q.getLoiIndispo_());

      ndata[i][11] = " " + SiporHoraire.format( q.getH_().semaineCreneau1HeureDep) + " � " + SiporHoraire.format( q.getH_().semaineCreneau1HeureArrivee);
      ndata[i][12] = " " + SiporHoraire.format( q.getH_().semaineCreneau2HeureDep) + " � " + SiporHoraire.format( q.getH_().semaineCreneau2HeureArrivee);
      ndata[i][13] = " " + SiporHoraire.format( q.getH_().semaineCreneau3HeureDep) + " � " + SiporHoraire.format( q.getH_().semaineCreneau3HeureArrivee);

    }

    this.tableau_ = new BuTable(ndata, this.titreColonnes) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    for(int i=0; i<titreColonnes.length;i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }
    tableau_.setBorder(this.borduretab);
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    this.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    
    this.revalidate();
    this.updateUI();
  }

}
