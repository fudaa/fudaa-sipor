package org.fudaa.fudaa.sipor.factory;

import org.fudaa.dodico.corba.sipor.SParametresCercle2;
import org.fudaa.dodico.corba.sipor.SParametresCercleGene;
import org.fudaa.dodico.corba.sipor.SParametresCerclesGenes;
import org.fudaa.dodico.corba.sipor.SParametresSipor2;
import org.fudaa.dodico.corba.sipor.SParametresSiporGene;

/**
 * Classe qui d�crit les m�thodes de divisions des donn�es en sorties du noyau de calcul pour retrouver des r�sultats sur une simulation 
 * et non plus sur 6.
 *@version $Version$
 * @author hadoux
 *
 */
public class FonctionsSimu {

	/**
	 * Nombre de simulations r�alis�es pour un lancement de calcul.
	 * Ce nombre correspond au nombre de fois que l'on multiplie le temps de simulation.
	 */
	public final static int NOMBRE_SIMULATIONS=6;
	
	/**
	 * Methode qui divise le nombre par le nombre de simulations effectives par le noyau de calcul.
	 * par d�faut le nombre de simulations est de 6
	 * @param val
	 * @return
	 */
public static int diviserSimu(double val){
	
	int resultat=0;
	
	if(val<NOMBRE_SIMULATIONS && val>0)
		resultat= 1;
	else
	resultat=(int)Math.round(val/NOMBRE_SIMULATIONS);
	
	
	return resultat;
}

public static int[] diviserSimu(int[] tab){
	
	int[] newT=new int[tab.length] ;
	
	for(int i=0;i<tab.length;i++)
		newT[i]=diviserSimu(tab[i]);
	
	return newT;
	
}



public static SParametresSiporGene copieOldProjetIntoNewProjet(SParametresSipor2 oldSImul){
	SParametresSiporGene newSimul=new SParametresSiporGene();
	newSimul.bassins=oldSImul.bassins;
	newSimul.cheneaux=oldSImul.cheneaux;
	newSimul.donneesGenerales=oldSImul.donneesGenerales;
	newSimul.ecluses=oldSImul.ecluses;
	newSimul.gares=oldSImul.gares;
	newSimul.grapheTopologie=oldSImul.grapheTopologie;
	newSimul.maree=oldSImul.maree;
	newSimul.matriceDureeParcoursCercles=oldSImul.matriceDureeParcoursCercles;
	newSimul.matriceDureeParcoursChenaux=oldSImul.matriceDureeParcoursChenaux;
	newSimul.navires=oldSImul.navires;
	newSimul.nbColonnesDureeParcoursCercles=oldSImul.nbColonnesDureeParcoursCercles;
	newSimul.nbColonnesDureeParcoursChenaux=oldSImul.nbColonnesDureeParcoursChenaux;
	newSimul.nbLignesDureeParcoursCercles=oldSImul.nbLignesDureeParcoursCercles;
	newSimul.nbLignesDureeParcoursChenaux=oldSImul.nbLignesDureeParcoursChenaux;
	newSimul.niveau=oldSImul.niveau;
	newSimul.quais=oldSImul.quais;
	newSimul.ResultatsCompletsSimulation=oldSImul.ResultatsCompletsSimulation;
	
	//-- partie sp�cifique:  cercles --//
	newSimul.cercles=new SParametresCerclesGenes();
	newSimul.cercles.nbCercles=oldSImul.cercles.nbCercles;
	newSimul.cercles.listeCercles=new SParametresCercleGene[newSimul.cercles.nbCercles];
	for(int i=0;i<newSimul.cercles.nbCercles;i++){
		SParametresCercle2 old=oldSImul.cercles.listeCercles[i];
		newSimul.cercles.listeCercles[i]=new SParametresCercleGene(old.nom, old.dimensionMatriceReglesNav,
				old.matriceReglesNavigation, new double[old.dimensionMatriceReglesNav][old.dimensionMatriceReglesNav], old.gareAmont, old.gareAval);
	}
return newSimul;		
}


}
