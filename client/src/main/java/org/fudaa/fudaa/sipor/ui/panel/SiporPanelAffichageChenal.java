package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuTable;

import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporChenal;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des cheneaux sous forme d un tableau: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelAffichageChenal extends JPanel {

  public String[] titreColonnes_ = { "Nom", "Profondeur", "Soumis � la mar�e","Loi fr�quence", "Fr�quence indispo", "Dur�e indispo", "Loi Erlang", "Cr�neau 1", "Cr�neau 2", "Cr�neau 3" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des quais
   */

  Object[][] data_ = {};

  public Object[][] ndata_;

  public BuTable tableau_ = new BuTable(data_, titreColonnes_);

  /**
   * Bordure du tableau
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * constructeur du panel d'affichage des quais
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelAffichageChenal(final SiporDataSimulation d) {

    setLayout(new BorderLayout());

    this.maj(d);

  }

  /**
   * Methode de mise a jour du tableau
   */
  public void maj(final SiporDataSimulation d) {

    ndata_ = new Object[d.getListeChenal_().getListeChenaux_().size()][10];

    for (int i = 0; i < d.getListeChenal_().getListeChenaux_().size(); i++) {
      final SiporChenal q = d.getListeChenal_().retournerChenal(i);
      q.affichage();
      ndata_[i][0] = q.getNom_();
      // ndata_[i][1]=Double.toString(q.longueur_);
      ndata_[i][1] = "" + (float) q.getProfondeur_();
      if (q.isSoumisMaree_()) {
        ndata_[i][2] = "oui";
      } else {
        ndata_[i][2] = "non";
      }
     
      //-- lois --//
      if (q.getTypeLoi_() == 0) {
          ndata_[i][3] = "Loi d'Erlang: " + Integer.toString(q.getLoiFrequence_());
        } else if (q.getTypeLoi_() == 1) {
          ndata_[i][3] = "Loi d�terministe: ";
        } else if (q.getTypeLoi_() == 2) {
          ndata_[i][3] = "Loi journali�re: ";
        }

        if (q.getTypeLoi_() == 0) {
          ndata_[i][4] = "" + (float) q.getFrequenceMoyenne_();
        } else {
          ndata_[i][4] = "";
        }
        ndata_[i][5] = "" + (float) q.getDureeIndispo_();
        ndata_[i][6] = Integer.toString(q.getLoiIndispo_());
      ndata_[i][7] = "" + SiporHoraire.format( q.getH_().semaineCreneau1HeureDep) + " � " + SiporHoraire.format( q.getH_().semaineCreneau1HeureArrivee);
      ndata_[i][8] = "" + SiporHoraire.format( q.getH_().semaineCreneau2HeureDep) + " � " + SiporHoraire.format( q.getH_().semaineCreneau2HeureArrivee);
      ndata_[i][9] = "" + SiporHoraire.format( q.getH_().semaineCreneau3HeureDep) + " � " + SiporHoraire.format( q.getH_().semaineCreneau3HeureArrivee);

    
      // data[i][3]=Float.toString();

    }

    this.tableau_ = new BuTable(ndata_, this.titreColonnes_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    for(int i=0; i<titreColonnes_.length;i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
               
        }
    tableau_.getTableHeader();

    tableau_.setBorder(this.borduretab_);
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    this.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    this.revalidate();
    this.updateUI();
  }

}
