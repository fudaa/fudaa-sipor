package org.fudaa.fudaa.sipor.algorithmes;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Genere un mod�le d egraphe contenant les valeur pour chaque douziemes de graphe en se basant sur hauteur min et max de la mar�e.
 * @author Adrien
 *
 */
public class MareeDouziemes {

	public static class CoupleDouzieme {
		public float heure;
		public float hauteur;
		//-- nombre de douzieme de variation --//
		public int  variation;
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "Couple douzieme: \n heure="+heure +"\nhauteur="+hauteur+"\nvariation="+variation+"\n\n";
		}

	}


	public String generateGrapheDouziemes(float mareeBasse, float mareeHaute , float periodeMer,  double[] tableauDouziemes,
			float mareeBasseMorte, float mareeHauteMorte ,  double[] tableauDouziemesMorte) {
		List<CoupleDouzieme> res = computeDouziemes(mareeBasse, mareeHaute, periodeMer, tableauDouziemes);
		System.out.println(res);

		List<CoupleDouzieme> resMorte = computeDouziemes(mareeBasseMorte, mareeHauteMorte, periodeMer, tableauDouziemesMorte);
		System.out.println(res);

		String graph = displayGraphe(res, mareeBasse, mareeHaute, periodeMer, false,
				resMorte,mareeBasseMorte,mareeHauteMorte );

		return graph;
	}

	public List<CoupleDouzieme> computeDouziemes(float mareeBasse, float mareeHaute , float periodeMaree,  double[] tableauDouziemes) {

		//-- le marnage --//
		float deltaHauteurMaree =  (mareeHaute - mareeBasse)/12;

		float deltaDureeMer =   periodeMaree / 12;

		List<CoupleDouzieme> res = new  ArrayList<CoupleDouzieme>();

		//-- init de l'indice mar�e --//
		float indiceHauteurMaree = mareeBasse;
		float indiceHeureMaree = 6;
		double  variationHauteur;
		CoupleDouzieme couple = new CoupleDouzieme();
		couple.heure = indiceHeureMaree;
		couple.hauteur = indiceHauteurMaree;

		res.add(couple);
		boolean marreMonte = true;

		for(int i=0;i< tableauDouziemes.length;i++) {

			//-- calcul de la variation --//

			variationHauteur = (deltaHauteurMaree * tableauDouziemes[i]);

			indiceHeureMaree = indiceHeureMaree + deltaDureeMer;

			if(marreMonte && ( indiceHauteurMaree + variationHauteur <= mareeHaute)) {
				//-- mar�e monte --//
				indiceHauteurMaree = (float) (indiceHauteurMaree + variationHauteur);
				
				//-- pour pr�munir les erreurs d'arrondi --//
				if(indiceHauteurMaree > mareeHaute)
					indiceHauteurMaree = mareeHaute;
				
			} else {
				marreMonte = false;
				//--mar�e redescend --//
				indiceHauteurMaree = (float) (indiceHauteurMaree - variationHauteur);
			}
			couple = new CoupleDouzieme();
			couple.heure = indiceHeureMaree;
			couple.hauteur = indiceHauteurMaree;
			couple.variation = (int) tableauDouziemes[i];
			res.add(couple);
		}


		return res;

	}


	public String displayGraphe(List<CoupleDouzieme> liste,  float hauteurMareeMin, float hauteurMareeMax,float periodeMaree, boolean histo ) {
		return displayGraphe(liste, hauteurMareeMin, hauteurMareeMax, periodeMaree, histo, null, 0,0);
	}

	public String displayGraphe(List<CoupleDouzieme> liste,  float hauteurMareeMin, float hauteurMareeMax,float periodeMaree, boolean histo , 
			List<CoupleDouzieme> listeMorte,  float hauteurMareeMinMorte, float hauteurMareeMaxMorte) {	  

		
		float hauteurMin = (hauteurMareeMin>hauteurMareeMinMorte)?hauteurMareeMinMorte:hauteurMareeMin;
		float hauteurMax = (hauteurMareeMax>hauteurMareeMaxMorte)?hauteurMareeMax:hauteurMareeMaxMorte;
		
		String g = "";

		g += "graphe\n{\n  titre \"Graphe des douzi�mes  \"\n" ;
		g += "  animation non\n";
		g += "  legende " + "oui" + "\n";
		g += " marges\n {\n";
		g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

		g += "  axe\n  {\n"; // abscisses
		g += "    titre \"Intervalles de Dur�e" + "\"\n";
		g += "    orientation " + "horizontal" + "\n";
		g += "    graduations non\n";
		g += "    minimum " + 0 + "\n";
		g += "    maximum " + 12
				+ "\n";
		g += "  }\n";

		g += "  axe\n  {\n"; // Ordonn�es
		g += "    titre \"Hauteur" + "\"\n";
		g += "    unite \"m�tres";
		g += "\"\n";
		g += "    orientation " + "vertical" + "\n";
		g += "    graduations oui\n";
		g += "    minimum " + hauteurMin + "\n";
		g += "    maximum " + hauteurMax+ "\n";
		g += "  }\n";



		g += "  courbe\n  {\n";
		g += "    titre \"";
		g += "Approximation des douzi�mes Vive eau";
		g += "\"\n";
		g += "    type ";
		if (histo) g += "histogramme"; else g += "courbe";
		g += "\n";
		g += "    aspect\n {\n";
		g += "contour.largeur 1 \n";
		g += "surface.couleur  	BB0000 \n";
		g += "texte.couleur 000000 \n";
		g += "contour.couleur ";
		if (histo) g += "000000"; else g += "BB0000";
		g += " \n";
		g += "    }\n";
		g += "    valeurs\n    {\n";

		int cpt = 1;
		for (CoupleDouzieme cd:liste) {
			g += (cpt++)
					+ " ";

			g +=  cd.hauteur;
			String etiquette = " variation de " + cd.variation  + "/12";
			if(cpt==0)
				etiquette = "";

			g += "\n etiquette  \n \" "+ etiquette +" \" \n"/* + */
					+ "\n";

		}
		g += "    }\n";
		g += "  }\n";


		//-- courbe 2??
		if(listeMorte != null) {
			g += "  courbe\n  {\n";
			g += "    titre \"";
			g += "Approximation des douzi�mes Morte eau";
			g += "\"\n";
			g += "    type ";
			if (histo) g += "histogramme"; else g += "courbe";
			g += "\n";
			g += "    aspect\n {\n";
			g += "contour.largeur 1 \n";
			g += "surface.couleur  	7065B7 \n";
			g += "texte.couleur 000000 \n";
			g += "contour.couleur ";
			if (histo) g += "000000"; else g += "7065B7";
			g += " \n";
			g += "    }\n";
			g += "    valeurs\n    {\n";

			cpt = 1;
			for (CoupleDouzieme cd:listeMorte) {
				g += (cpt++)
						+ " ";

				g +=  cd.hauteur;
				String etiquette = " variation de " + cd.variation  + "/12";
				if(cpt==0)
					etiquette = "";

				g += "\n etiquette  \n \" "+ etiquette +" \" \n"/* + */
						+ "\n";

			}
			g += "    }\n";
			g += "  }\n";

		}

		/**
		 * declaration d'un seuil min et max
		 */
		g += " contrainte\n";
		g += "{\n";
		g += "titre \"hauteur basse mer Vive eau \"\n";
		g += " type max\n";
		g += " valeur " + hauteurMareeMin + CtuluLibString.LINE_SEP_SIMPLE;
		g += " \n }\n";  

		g += " contrainte\n";
		g += "{\n";
		g += "titre \"hauteur haute mer Vive eau\"\n";
		g += " type max\n";
		g += " valeur " + hauteurMareeMax + CtuluLibString.LINE_SEP_SIMPLE;
		g += " \n }\n";    

		if(listeMorte != null) {
			g += " contrainte\n";
			g += "{\n";
			g += "titre \"hauteur basse mer Morte eau \"\n";
			g += " type max\n";
			g += " valeur " + hauteurMareeMinMorte + CtuluLibString.LINE_SEP_SIMPLE;
			g += " \n }\n";  

			g += " contrainte\n";
			g += "{\n";
			g += "titre \"hauteur haute mer Morte eau\"\n";
			g += " type max\n";
			g += " valeur " + hauteurMareeMaxMorte + CtuluLibString.LINE_SEP_SIMPLE;
			g += " \n }\n";    
		}
		//-- doit on le supprimer?
		g += "    }\n";
		g += "  }\n";

		System.out.println("graphe \n\n "+g);

		return g;
	}




}
