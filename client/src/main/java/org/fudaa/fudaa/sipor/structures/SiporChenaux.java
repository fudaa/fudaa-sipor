package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;


/**
 * Classe de gestion des cheneaux permet de stocker les donn�e relatives au bassin.
 * 
 * @author Adrien Hadoux
 */
public class SiporChenaux {

  /**
   * Liste de string contenant les noms des bassins.
   */
  ArrayList listeChenaux_ = new ArrayList();

  /**
   * Methode d'ajout d'un chenal.
   * 
   * @param _c chenal � ajouter
   */

  public void ajout(final SiporChenal _c) {

    listeChenaux_.add(_c);
    SiporDataSimulation.setProperty("chenal");
  }

  /**
   * Methode qui retourne le i eme chenal.
   * 
   * @param _i indice du cercle du tableau de cheneaux a retourner
   * @return un objet de type string qui pourra etre modifi et renvoy
   */

  public SiporChenal retournerChenal(final int _i) {
    if (_i < this.listeChenaux_.size()) {
      return (SiporChenal) this.listeChenaux_.get(_i);
    }
    return null;
  }

  /**
   * Methode de suppression du n ieme element de la liste de cheneaux.
   * 
   * @param _n
   */
  public  void suppression(final int _n) {

    listeChenaux_.remove(_n);
    //-- Notification aux vues pour se mettre � jour --//
    SiporDataSimulation.setProperty("chenal");
  }

  /**
   * Methode de modification du n ieme chenal par le chenal entr� en parametre d entr�e.
   * 
   * @param _n indice du chenal a modifier
   * @param _c chenal a remplacer
   */
  public  void modification(final int _n, final SiporChenal _c) {
    this.listeChenaux_.set(_n, _c);
    //-- Notification aux vues pour se mettre � jour --//
    SiporDataSimulation.setProperty("chenal");
  }

  /**
   * methode qui permet de determiner si le nom d'un bassin est unique ou s'il appartient deja a la liste des bassins.
   */
  public  boolean existeDoublon(final String _nomComposant, final int _k) {
    for (int i = 0; i < this.listeChenaux_.size(); i++) {
      if (i != _k) {
        if (this.retournerChenal(i).getNom_().equals(_nomComposant)) {
          return true;
        }
      }

    }

    // arriv� a ce stade; on a pas trouv�de doublons, le nom n'existe donc pas!
    return false;

  }

  public int retourneIndice(final String _nomChenal) {
    for (int i = 0; i < this.listeChenaux_.size(); i++) {
      if (this.retournerChenal(i).getNom_().equals(_nomChenal)) {
        return i;
      }
    }
    return -1;
  }

public ArrayList getListeChenaux_() {
	return listeChenaux_;
}

public void setListeChenaux_(ArrayList listeChenaux_) {
	this.listeChenaux_ = listeChenaux_;
}

}
