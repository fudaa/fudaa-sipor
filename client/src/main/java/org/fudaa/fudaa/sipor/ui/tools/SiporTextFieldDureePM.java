package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuDialogError;

import org.fudaa.fudaa.sipor.SiporImplementation;

public class SiporTextFieldDureePM extends SiporTextField {

  public SiporTextFieldDureePM() {
    super();
  }

  public SiporTextFieldDureePM(int columns) {
    super(columns);
  }

  public SiporTextFieldDureePM(String text, int columns) {
    super(text, columns);
  }

  public SiporTextFieldDureePM(String text) {
    super(text);
  }

  /**
   * Methode destinee a etre surchargee par les classes h�ritant de SiporTextField. Permet de g�rer les contr�les apr�s
   * saisie de l'utilisateur
   **/
  public void traitementApresFocus(FocusEvent e) {

    String contenu = this.getText();

    if (contenu.equals("")) return;
    //-- Traduction en float --//

    try {
      float valeur = Float.parseFloat(contenu);

      {

        if (contenu.lastIndexOf(".") != -1) {
          String unite = contenu.substring(contenu.lastIndexOf(".") + 1, contenu.length());
          if (unite.length() > 2) {
            new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                "Il doit y avoir 2 chiffres maximum apr�s la virgule").activate();
            setText("");
            this.requestFocus();
            return;
          }

          float valUnite = Float.parseFloat(unite);
          if (valUnite >= 60) {
            new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                "Les unit�s doivent �tre inf�rieures � 60 minutes.").activate();
            setText("");
            this.requestFocus();
            return;

          }

        }

        float format = valeur;//conversionFormat(valeur,contenu);
        this.setText("" + format);
      }

    } catch (NumberFormatException e1) {
      new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Ce nombre n'existe pas.").activate();
      setText("");
      this.requestFocus();
    }

    SiporTextFieldDureePM.this.setForeground(Color.black);
  }

  /**
   * methode qui formatte un reel en duree. 2 chiffres apr�s la virgule. modulo 60 minutes exemple: 35.652 se
   * transformera en 36.05
   **/
  protected float conversionFormat(float valeur, String contenu) {

    boolean isNegative = false;

    if (valeur < 0) {
      isNegative = true;
      valeur = valeur * -1;
      contenu = "" + valeur;
    }

    float resul = valeur;
    String min = "";
    int minutes = 00;

    if (contenu.lastIndexOf(".") == -1) minutes = 00;
    else {
      min = contenu.substring(contenu.lastIndexOf(".") + 1);
      minutes = Integer.parseInt(min);
      if (minutes / 10 == 0 && min.length() == 1) minutes = minutes * 10;
    }
    while (minutes / 100 != 0)
      minutes = minutes / 10;


    //creation des heures
    int heures = 0;
    //le cast va arrondir au chiffre le plus proche or on veut garder le nombre tel quel
    heures = (int) valeur;

    //modulo des minutes
    while (minutes >= 60) {
      heures++;
      minutes -= 60;
    }

    float resultat = heures + (float) (minutes / 100.0);

    if (isNegative) resultat = resultat * -1;

    return resultat;

  }

  protected int taille() {
    return 5;
  }

  protected void specifier_validator() {

  }

}
