package org.fudaa.fudaa.sipor.ui.modeles;

import java.util.Observable;
import java.util.Observer;

import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuDialogError;

import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * Modele pour les g�nes occasionn�es dans les cercles.
 * 
 * @author Adrien Hadoux
 */
public class SiporModeleGenesCercles extends AbstractTableModel implements Observer {

  private static final long serialVersionUID = 1L;
  SiporDataSimulation donnees_;
  SiporCercle cercleCourant_;

  public SiporModeleGenesCercles(SiporDataSimulation _d, SiporCercle cercleCourant) {

    donnees_ = _d;
    cercleCourant_ = cercleCourant;
    donnees_.addObservers(this);
  }

  public int getColumnCount() {
    return this.donnees_.getCategoriesNavires_().getListeNavires_().size() + 1;
  }

  public int getRowCount() {

    System.out.println("nb navires: " + donnees_.getCategoriesNavires_().getListeNavires_().size());
    return this.donnees_.getCategoriesNavires_().getListeNavires_().size();
  }

  /** retourne le double correspondant au parcours **/
  public Object getValueAt(int i, int j) {

    // -- si colonne 0: on affiche les noms des navires --//
    if (j == 0) return "  " + getNavireName(i);
    else return "" + this.cercleCourant_.getRegleGenes().retournerAij(i, j - 1);
  }

  /** Recuperation des donnees (saisie par utilisateur) par le tableau et analyse pour validation **/
  public void setValueAt(Object value, int rowIndex, int columnIndex) {

    // -- aucun traitement pour first line because it's a title one... --//
    if (columnIndex == 0) return;

    // recuperation des vrais indices du tableau: il faut translater a cause des titres des noms
    double valeur;
    try {
      valeur = Double.parseDouble((String) value);

      // -- Gestion des erreurs --//
      if (valeur < 0) {
        new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
            SiporConstantes.constanteErreurCoupleNavire + "(" + getNavireName(rowIndex) + " - "
                + getNavireName(columnIndex - 1)).activate();
        return;
      }

      // -- test si l'utilisateur entre des centi�mes --//
      if (((String) value).lastIndexOf(".") != -1) {
        String min = ((String) value).substring(((String) value).lastIndexOf(".") + 1);
        int minutes = Integer.parseInt(min);
        if (minutes / 100 != 0) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              SiporConstantes.constanteErreurDigitsCoupleNavire + "(" + getNavireName(rowIndex) + " - "
                  + getNavireName(columnIndex - 1)).activate();

          return;
        }

      }

    } catch (NumberFormatException e) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          SiporConstantes.constanteErreurCoupleNavire + "(" + getNavireName(rowIndex) + " - "
              + getNavireName(columnIndex - 1)).activate();

      return;
    }

    // -- mise a jour des genes navires --//
    this.cercleCourant_.getRegleGenes().modification(valeur, rowIndex, columnIndex - 1);

    // mise a jour du tableau (cot� graphique)
    // -- AHX il faut tout ettre � jour car les donn�es en diagonales sont �galement mis a jour: si A C modifi� alors C
    // A l'est aussi. --//
    fireTableDataChanged();

  }

  public boolean isCellEditable(int row, int col) {
    if (col == 0) {
      return false;
    } else {
      return true;
    }
  }

  public String getNavireName(int indice) {
    return donnees_.getCategoriesNavires_().getListeNavires_().get(indice).getNom();
  }

  /** Methode qui permet de recuperer les noms des colonnes du tableau **/
  public String getColumnName(int column) {
    if (column == 0) // -- AHX: on retourne le nom des navires --//
    return SiporConstantes.constanteCatNavireNom;
    else return getNavireName(column - 1);
  }

  public int getMaxCol() {
    return getColumnCount();
  }

  /**
   * retourne le nombre de ligne
   */
  public int getMaxRow() {
    return getRowCount();
  }

  public void update(Observable o, Object arg) {
    if (arg.equals("cercle") || arg.equals("navire")) this.fireTableDataChanged();
  }

  public SiporDataSimulation getDonnees_() {
    return donnees_;
  }

  public void setDonnees_(SiporDataSimulation donnees_) {
    this.donnees_ = donnees_;
  }

}
