package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;
import java.util.List;


/**
 * Classe qui contient la liste des quais du port a simuler.
 * 
 * @author Adrien Hadoux
 */

public class SiporListeQuais {

  // attributs:

  List lQuais_ = new ArrayList();

  public  SiporListeQuais() {

  }

  /**
   * Methode d'ajout d'un quai.
   * 
   * @param _q quai pass en parametre d 'entre
   */
  public  void ajout(final SiporQuais _q) {

    lQuais_.add(_q);
    //-- Notification aux vues pour se mettre � jour --//
    SiporDataSimulation.setProperty("quai");
  }

  /**
   * Methode de modification d'un Quai une position donne.
   * 
   * @param _n indice du Quai a modifier dans le tableau de Quais
   * @param _q quai modifi remplacer par un autre navire
   */
  public  void modification(final int _n, final SiporQuais _q) {
    this.lQuais_.set(_n, _q);
    //-- Notification aux vues pour se mettre � jour --//
    SiporDataSimulation.setProperty("quai");
  }

  /**
   * @return le nombre de catgories de Navires differents
   */
  public   int getNombreQuais() {
    return this.lQuais_.size();

  }

  /**
   * Methode qui retourne le i eme quai tres puissant car c'est cette methode qui sera a la base de la modification d'un
   * quai: il suffit de faire appel a la fonction et de stock le quai retourn dans une variable temp, de modifier cette
   * variable temp, ce qui aura alors pour effet de modifier le contenu du tableau de quais!
   * 
   * @param _i indice du quai du tableau de quais a retourner
   * @return un objet de type quai qui pourra etre modifi et renvoy
   */

  public SiporQuais retournerQuais(final int _i) {
    if (_i < this.lQuais_.size()) {
      return (SiporQuais) this.lQuais_.get(_i);
    }
    return null;
  }

  /**
   * Methode d'affichage de la liste de quais.
   */
  public  void affichage() {
    for (int i = 0; i < this.lQuais_.size(); i++) {
      ((SiporQuais) lQuais_.get(i)).affichage();
    }

  }

  /**
   * Methode de suppression d'un Quai.
   * 
   * @param _n entier correspondant l'indice du quai a detruire
   */
  public void suppression(final int _n) {
    lQuais_.remove(_n);
    //-- Notification aux vues pour se mettre � jour --//
    SiporDataSimulation.setProperty("quai");
  }

  /**
   * Methode permettant de recuperer le numero du quai en fonction de son nom.
   * 
   * @param _nomQuai chaine de caractere correspondant au nom du quai a rechercher dans la liste des quais
   * @return indice du quai correspondant � l chaine en param�tre d'entr�e
   */

  public  int retournerIndiceQuai(final String _nomQuai) {

//    final int indice = -1;

    for (int i = 0; i < this.lQuais_.size(); i++) {
      if (this.retournerQuais(i).getNom().equals(_nomQuai)) {
        return i;
      }
    }

    return -1;

  }

  public boolean existeDoublon(final String _nomComposant, final int _k) {
    for (int i = 0; i < this.lQuais_.size(); i++) {
      if (i != _k) {
        if (this.retournerQuais(i).getNom().equals(_nomComposant)) {
          return true;
        }
      }

    }

    // arriv�a ce stade; on a pas trouv�de doublons, le nom n'existe donc pas!
    return false;

  }

public List getlQuais_() {
	return lQuais_;
}

public void setlQuais_(List lQuais_) {
	this.lQuais_ = lQuais_;
}

}
