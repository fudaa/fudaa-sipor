/**
 *@creation 28 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sipor.SParametresResultatsMatricesCroisements;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * @version $Version$
 * @author hadoux
 */
public class SiporAlgorithmeCroisementsChenal {

  /**
   * methode statique qui d�termine si un creneau est inclus dans l 'autre ou inversement
   * 
   * @param a1
   * @param a2
   * @param b1
   * @param b2
   * @return
   */
  public static boolean est_inclus(final double a1, final double a2, final double b1, final double b2) {
    if (b1 <= a1 && a1 <= b2) {
      return true;
    }
    if (b1 <= a2 && a2 <= b2) {
      return true;
    }
    if (a1 <= b1 && b1 <= a2) {
      return true;
    }
    if (a1 <= b2 && b2 <= a2) {
      return true;
    }
    if (b1 <= a1 && a2 <= b2) {
      return true;
    }
    if (a1 <= b1 && b2 <= a2) {
      return true;
    }

    return false;
  }

  public static void calcul(final SiporDataSimulation donnees_) {

    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // etape 1: recuperation du tableau de matrices de croisements:allocation memoire du tableau en fonction du nombre
    // de chenaux
    resultats.tableauDeMatricesCroisementsChenal = new SParametresResultatsMatricesCroisements[donnees_.getListeChenal_().getListeChenaux_()
        .size()];
    //

    for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
      // allocation memoire de la i emem structure
      resultats.tableauDeMatricesCroisementsChenal[i] = new SParametresResultatsMatricesCroisements();

      // allocation de la memoire de la matrice dans ce chenal: matrice carr� de tailel du nombre de navires
      resultats.tableauDeMatricesCroisementsChenal[i].matriceCroisements = new int[donnees_.getCategoriesNavires_().getListeNavires_()
          .size()][donnees_.getCategoriesNavires_().getListeNavires_().size()];

      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        for (int l = 0; l < donnees_.getCategoriesNavires_().getListeNavires_().size(); l++) {
          resultats.tableauDeMatricesCroisementsChenal[i].matriceCroisements[k][l] = 0;
        }
      }

    }

    // calcul des croisements dans les chenaux
    for (int n = 0; n < donnees_.getListeResultatsSimu_().nombreNavires; n++) {
    	
    	//-- on recherche l'indice du quai du navire n --//
    	int quaiN=-1;
    	boolean trouve=false;
    	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours && !trouve;i++)
    		if(donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[i].typeElement==3){
    			quaiN=i;
    			trouve=true;}
    	if(quaiN==-1){System.out.println("Erreur: occupation quai: on n'a pas trouv� de quai! impossible...");}
    	
      for (int m = n + 1; m < donnees_.getListeResultatsSimu_().nombreNavires; m++) {
    	  
    	//-- on recherche l'indice du quai du navire M --//
      	int quaiM=-1;
      	trouve=false;
      	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[m].NbElemtnsParcours && !trouve;i++)
      		if(donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[i].typeElement==3){
      			quaiM=i;
      			trouve=true;}
      	if(quaiM==-1){System.out.println("Erreur: occupation quai: on a pas ttrouv�e quai! impossible...");}
    	  
        for (int t1 = 0; t1 < donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours; t1++) {
          for (int t2 = 0; t2 < donnees_.getListeResultatsSimu_().listeEvenements[m].NbElemtnsParcours; t2++) {
            if (donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].typeElement == 0
                && donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].typeElement == 0
                && donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].indiceElement == donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].indiceElement
                && SiporAlgorithmeCroisementsChenal.est_inclus(
                    donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].heureEntree,
                    donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].heureSortie,
                    donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].heureEntree,
                    donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].heureSortie)
                && ((t1 < quaiN && t2 > quaiM) || (t1 > quaiN && t2 < quaiM))

            ) {
              /*
               * si les elements parocurus sont tous des chenaux, qu ils sont identiques et que les creneaux de
               * franchissement de ces chenaux se chevauchent => donc nos 2 amigo les navires se rencontrent et que els
               * elements sont dans le sens different (et c'est le duel de la mort)
               */
              final int chenal = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].indiceElement;
              final int cat1 = donnees_.getListeResultatsSimu_().listeEvenements[n].categorie;
              final int cat2 = donnees_.getListeResultatsSimu_().listeEvenements[m].categorie;

              resultats.tableauDeMatricesCroisementsChenal[chenal].matriceCroisements[cat1][cat2]++;
              // puisque la matrice est symetrique car : si un navire x rencontre un navire y, la r�ciproque est
              // �galement valable
              if (cat1 != cat2) {
                resultats.tableauDeMatricesCroisementsChenal[chenal].matriceCroisements[cat2][cat1]++;
              }
            }

          }
        }
      }
    }

  }

}
