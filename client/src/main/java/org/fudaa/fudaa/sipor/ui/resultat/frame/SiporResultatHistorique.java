/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */

public class SiporResultatHistorique extends SiporInternalFrame {

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Num navire", "Cat�gorie", "Longueur", "Tonnage", "Position", "Heure entr�e",
      "Heure sortie", "Att.acces", "Att.marees", "Att.secu", "Att.occupation", "Att.indispo" };
  // 12

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatHistorique(final SiporDataSimulation _donnees) {
    super("Historique", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    setSize(820, 400);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipal_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal22_arbre"), panelGestionTableau_);

    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(new JLabel("S�lectionnez la cat�gorie � visualiser:   "));
    this.controlPanel_.add(this.ListeNavires_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatHistorique.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });
    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatHistorique.this.windowClosed();

      }
    };
    this.quitter_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatHistorique.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {

    // on comptabilise le nombre d evenements de al simulation:
    int compteur = 0;
    for (int i = 0; i < this.donnees_.getListeResultatsSimu_().nombreNavires; i++) {
      compteur += donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours;
    }

    data = new Object[compteur][12];

    if (val < 0)// afficher tout l historique
    {
      int h = 0;
      for (int i = 0; i < this.donnees_.getListeResultatsSimu_().nombreNavires; i++) {
        for (int k = 0; k < this.donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours; k++) {

          data[h][0] = "" + this.donnees_.getListeResultatsSimu_().listeEvenements[i].numero;
          data[h][1] = ""
              + this.donnees_.getCategoriesNavires_()
                  .retournerNavire(this.donnees_.getListeResultatsSimu_().listeEvenements[i].categorie).getNom();

          data[h][2] = "" + this.donnees_.getListeResultatsSimu_().listeEvenements[i].longueur;
          data[h][3] = "" + this.donnees_.getListeResultatsSimu_().listeEvenements[i].tonnage;
          if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 0) {
            data[h][4] = ""
                + this.donnees_.getListeChenal_()
                    .retournerChenal(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom_();
          }
          if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 1) {
            data[h][4] = ""
                + this.donnees_.getListeCercle_()
                    .retournerCercle(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom_();
          }
          if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 2) {
            data[h][4] = ""
                + this.donnees_.getListeEcluse_()
                    .retournerEcluse(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom_();
          }
          if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 3) {
            data[h][4] = ""
                + this.donnees_.getlQuais_()
                    .retournerQuais(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom();
          }

          data[h][5] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureEntree);
          data[h][6] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie);
          data[h][7] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].acces);
          data[h][8] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].marees);
          data[h][9] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].secu);
          data[h][10] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].occupation);
          data[h][11] = ""
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indispo);
          h++;

        }

      }

    } else if (val < this.donnees_.getCategoriesNavires_().getListeNavires_().size()) {
      compteur = 0;
      for (int i = 0; i < this.donnees_.getListeResultatsSimu_().nombreNavires; i++) {
        if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].categorie == val) {
          compteur += donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours;
        }
      }

      data = new Object[compteur][12];

      int h = 0;
      for (int i = 0; i < this.donnees_.getListeResultatsSimu_().nombreNavires; i++) {
        if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].categorie == val) {
          for (int k = 0; k < this.donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours; k++) {

            data[h][0] = "" + this.donnees_.getListeResultatsSimu_().listeEvenements[i].numero;
            data[h][1] = ""
                + this.donnees_.getCategoriesNavires_()
                    .retournerNavire(this.donnees_.getListeResultatsSimu_().listeEvenements[i].categorie).getNom();

            data[h][2] = "" + this.donnees_.getListeResultatsSimu_().listeEvenements[i].longueur;
            data[h][3] = "" + this.donnees_.getListeResultatsSimu_().listeEvenements[i].tonnage;
            if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 0) {
              data[h][4] = ""
                  + this.donnees_.getListeChenal_()
                      .retournerChenal(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom_();
            }
            if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 1) {
              data[h][4] = ""
                  + this.donnees_.getListeCercle_()
                      .retournerCercle(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom_();
            }
            if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 2) {
              data[h][4] = ""
                  + this.donnees_.getListeEcluse_()
                      .retournerEcluse(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom_();
            }
            if (this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == 3) {
              data[h][4] = ""
                  + this.donnees_.getlQuais_()
                      .retournerQuais(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement).getNom();
            }

            data[h][5] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureEntree);
            data[h][6] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie);
            data[h][7] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].acces);
            data[h][8] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].marees);
            data[h][9] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].secu);
            data[h][10] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].occupation);
            data[h][11] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2(this.donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indispo);
            h++;
          }

        }
      }

    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
