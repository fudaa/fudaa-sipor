/**
 *@creation 16 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * Cette classe permet de realiser les calculs destin� aux resultats sur la eneration des des navires cette classe
 * d�finie la structure de donn�e utilis� afin de r�aliser le traitement ainsi que les algorithmes utilis�s pour
 * calculer et r�cup�rer les informations importantes Remarque: les structures utilis�es sont optimum pour obtenir la
 * meilleur complexit�, aussi sont elles diff�rentes.
 * 
 * @version $Version$
 * @author hadoux
 */
public class SiporAlgorithmeGenerationBateaux {

  /**
   * Methode qui realise les calculs pour les g�n�rations de bateaux
   */
  public static void calcul(final SiporDataSimulation donnees_) {

    /**
     * Structure utilis�e: un tableau d entier dont chaque case correspon,d a une cat�gorie de navire chaque case
     * contient le nombre de navires g�n�r� pour la cat�gorie donn�e:
     */
    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // remplissage du tableau resultats.ResultatsGenerationNavires[]
    resultats.ResultatsGenerationNavires = new int[donnees_.getCategoriesNavires_().getListeNavires_().size()];

    // initialisation du tableau de navires
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      resultats.ResultatsGenerationNavires[i] = 0;
    }

    // on incr�mente la case n dans le tableau de cat�gorie a chaque fois que le bateau appartient a la cat�gorie n
    for (int i = 0; i < donnees_.getListeResultatsSimu_().nombreNavires; i++) {
      resultats.ResultatsGenerationNavires[donnees_.getListeResultatsSimu_().listeEvenements[i].categorie]++;
    }
  }

  public static float max(final SiporDataSimulation donnees_) {
    int max = 0;
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      if (max < donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[i]) {
        max = donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[i];
      }
    }
    return (float) (max + 0.25 * max);
  }

}
