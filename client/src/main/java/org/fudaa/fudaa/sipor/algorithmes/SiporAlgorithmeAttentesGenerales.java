package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsAttente;
import org.fudaa.dodico.corba.sipor.SParametresResultatsAttenteCategorie;
import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sipor.SiporResultatsDonneeTrajet;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

public class SiporAlgorithmeAttentesGenerales {

	/**
	 * metode de calcul de la totalite des temps d attente dans tous les sens de la simulation. effectue tous ces calculs
	 * des fin du lancement de la simulation
	 * 
	 * @param donnees_
	 * @param nomfichier
	 */
	public static void CalculApresSImu(final SiporDataSimulation donnees_) {
		/*
		 * rangements des differents type d'�l�ment dans le tableau: indiceType == 0 =>"chenal" indiceType == 1 =>"cercle"
		 * indiceType == 2 =>"ecluse" indiceType == 3 => "quai"
		 */

		/*
		 * sens: 0: sens amont vers aval: on ne cherche que les attentes dans les gares amont de lelement 1: sens aval vers
		 * amont: on ne cherche les temps que dans les gares aval de l element 2: les 2 sens : on cumule les attentes a la
		 * fois des gares amont et aval.
		 */

		final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;
		int compteurElementsAttente;
		boolean trouve;
		/*******************************************************************************************************************
		 * CALCUL DES ATTENTES DANS LE SENS ENTRANT
		 ******************************************************************************************************************/
		// allocation de m�moire pour autant d elements qu il y a eu de saisies
		resultats.AttentesTousElementsToutesCategoriesSens1 = new SParametresResultatsAttente[donnees_.getlQuais_().getlQuais_()
		                                                                                      .size()
		                                                                                      + donnees_.getListeEcluse_().getListeEcluses_().size()
		                                                                                      + donnees_.getListeChenal_().getListeChenaux_().size()
		                                                                                      + donnees_.getListeCercle_().getListeCercles_().size()];

		compteurElementsAttente = 0;
		trouve = false;

		// chenal
		for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].typeElement = 0;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re la cat�gorie  associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				
				// pour chaque element du trajet du navire k des qu on trouve el premeir element on se retire
				trouve = false;
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3)
					{
						//-- On a pas trouv� l'�l�ment en entrant car on est deja arriv� au quai: on quitte donc la boucle --//
						trouve=true;
					}
					else
						if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 0
								&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
							trouve = true;
							// on comptabilise les diff�rents temps d'attente

							
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
							// attentes d'acces
							// attente totale d'acces
							double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
							}

							// attentes de marees
							// attente totale de marees
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
							// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
							}

							// attentes d'Secu
							// attente totale d'Secu
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
							// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
							}

							// attentes d'Occup
							// attente totale d'Occup
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
							// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
							}

							// attentes d'Panne
							// attente totale d'Panne
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
							// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
							}

							// attentes d'totale
							// attente totale d'acces
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
							}

						}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		// cercles de vitage
		for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].typeElement = 1;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re l cat�gorie e associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3)
					{
						//-- On a pas trouv� l'�l�ment en entrant car on est deja arriv� au quai: on quitte donc la boucle --//
						trouve=true;
					}
					else
						if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 1
								&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
							trouve = true;
							// on comptabilise les diff�rents temps d'attente
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
							// attentes d'acces
							// attente totale d'acces
							double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
							}

							// attentes de marees
							// attente totale de marees
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
							// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
							}

							// attentes d'Secu
							// attente totale d'Secu
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
							// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
							}

							// attentes d'Occup
							// attente totale d'Occup
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
							// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
							}

							// attentes d'Panne
							// attente totale d'Panne
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
							// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
							}

							// attentes d'totale
							// attente totale d'acces
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
							}

						}
				}

			}
			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}

			// incremente l element
			compteurElementsAttente++;
		}

		// ecluse
		for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].typeElement = 2;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re  cat�gorie ie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3)
					{
						//-- On a pas trouv� l'�l�ment en entrant car on est deja arriv� au quai: on quitte donc la boucle --//
						trouve=true;
					}
					else
						if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 2
								&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
							trouve = true;
							// on comptabilise les diff�rents temps d'attente
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
							
							// attentes d'acces
							// attente totale d'acces
							double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
							}

							// attentes de marees
							// attente totale de marees
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
							// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
							}

							// attentes d'Secu
							// attente totale d'Secu
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
							// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
							}

							// attentes d'Occup
							// attente totale d'Occup
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
							// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
							}

							// attentes d'Panne
							// attente totale d'Panne
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
							// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
							}

							// attentes d'totale
							// attente totale d'acces
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
								resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
							}

						}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}
		// quai
		for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].typeElement = 3;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re cat�gorie rie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3
							&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
						trouve = true;
						// on comptabilise les diff�rents temps d'attente
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
						
						// attentes d'acces
						// attente totale d'acces
						double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
						}

						// attentes de marees
						// attente totale de marees
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
						// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
						}

						// attentes d'Secu
						// attente totale d'Secu
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
						// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
						}

						// attentes d'Occup
						// attente totale d'Occup
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
						// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
						}

						// attentes d'Panne
						// attente totale d'Panne
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
						// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
						}

						// attentes d'totale
						// attente totale d'acces
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
							resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
						}

					}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		/*******************************************************************************************************************
		 * CALCUL DES ATTENTES DANS LE SENS SORTANT
		 ******************************************************************************************************************/
		// allocation de m�moire pour autant d elements qu il y a eu de saisies
		resultats.AttentesTousElementsToutesCategoriesSens2 = new SParametresResultatsAttente[donnees_.getlQuais_().getlQuais_()
		                                                                                      .size()
		                                                                                      + donnees_.getListeEcluse_().getListeEcluses_().size()
		                                                                                      + donnees_.getListeChenal_().getListeChenaux_().size()
		                                                                                      + donnees_.getListeCercle_().getListeCercles_().size()];
		compteurElementsAttente = 0;
		trouve = false;

		// chenal
		for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].typeElement = 0;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re la  cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				// pour chaque element du trajet du navire k des qu on trouve el premeir element on se retire
				trouve = false;
				for (int e = donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours - 1; e >= 0 && !trouve; e--) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3)
					{
						//-- On a pas trouv� l'�l�ment en entrant car on est deja arriv� au quai: on quitte donc la boucle --//
						trouve=true;
					}
					else
						if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 0
								&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
							trouve = true;
							// on comptabilise les diff�rents temps d'attente
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
							
							// attentes d'acces
							// attente totale d'acces
							double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
							}

							// attentes de marees
							// attente totale de marees
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
							// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
							}

							// attentes d'Secu
							// attente totale d'Secu
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
							// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
							}

							// attentes d'Occup
							// attente totale d'Occup
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
							// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
							}

							// attentes d'Panne
							// attente totale d'Panne
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
							// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
							}

							// attentes d'totale
							// attente totale d'acces
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
							}

						}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		// cercles de vitage
		for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].typeElement = 1;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
			  // on r�cup�re la  cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours - 1; e >= 0 && !trouve; e--) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3)
					{
						//-- On a pas trouv� l'�l�ment en entrant car on est deja arriv� au quai: on quitte donc la boucle --//
						trouve=true;
					}
					else
						if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 1
								&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
							trouve = true;
							// on comptabilise les diff�rents temps d'attente
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
							
							// attentes d'acces
							// attente totale d'acces
							double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
							}

							// attentes de marees
							// attente totale de marees
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
							// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
							}

							// attentes d'Secu
							// attente totale d'Secu
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
							// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
							}

							// attentes d'Occup
							// attente totale d'Occup
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
							// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
							}

							// attentes d'Panne
							// attente totale d'Panne
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
							// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
							}

							// attentes d'totale
							// attente totale d'acces
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
							}

						}
				}

			}
			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}

			// incremente l element
			compteurElementsAttente++;
		}

		// ecluse
		for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].typeElement = 2;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re la cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours - 1; e >= 0 && !trouve; e--) {
					if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3)
					{
						//-- On a pas trouv� l'�l�ment en entrant car on est deja arriv� au quai: on quitte donc la boucle --//
						trouve=true;
					}
					else
						// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
						if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 2
								&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
							trouve = true;
							// on comptabilise les diff�rents temps d'attente
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
							
							// attentes d'acces
							// attente totale d'acces
							double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
							}

							// attentes de marees
							// attente totale de marees
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
							// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
							}

							// attentes d'Secu
							// attente totale d'Secu
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
							// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
							}

							// attentes d'Occup
							// attente totale d'Occup
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
							// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
							}

							// attentes d'Panne
							// attente totale d'Panne
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
							// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
							}

							// attentes d'totale
							// attente totale d'acces
							attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
							// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
							if (attente != 0) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
							}
							// on teste si l'attente est minimale ou maximale:
							if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
							}
							if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
								resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
							}

						}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}
		// quai
		for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].typeElement = 3;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                 .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
			  // on r�cup�re la  cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours - 1; e >= 0 && !trouve; e--) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3
							&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
						trouve = true;
						// on comptabilise les diff�rents temps d'attente
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
						
						// attentes d'acces
						// attente totale d'acces
						double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
						}

						// attentes de marees
						// attente totale de marees
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
						// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
						}

						// attentes d'Secu
						// attente totale d'Secu
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
						// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
						}

						// attentes d'Occup
						// attente totale d'Occup
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
						// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
						}

						// attentes d'Panne
						// attente totale d'Panne
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
						// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
						}

						// attentes d'totale
						// attente totale d'acces
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
							resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
						}

					}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		/*******************************************************************************************************************
		 * CALCUL DES ATTENTES DANS LES 2 SENS
		 ******************************************************************************************************************/
		// allocation de m�moire pour autant d elements qu il y a eu de saisies
		resultats.AttentesTousElementsToutesCategoriesLes2Sens = new SParametresResultatsAttente[donnees_.getlQuais_().getlQuais_()
		                                                                                         .size()
		                                                                                         + donnees_.getListeEcluse_().getListeEcluses_().size()
		                                                                                         + donnees_.getListeChenal_().getListeChenaux_().size()
		                                                                                         + donnees_.getListeCercle_().getListeCercles_().size()];

		compteurElementsAttente = 0;
		trouve = false;

		// chenal
		for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].typeElement = 0;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                    .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
			  // on r�cup�re la  cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				// pour chaque element du trajet du navire k des qu on trouve el premeir element on se retire
				trouve = false;
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 0
							&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
						trouve = true;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
						
						int e2=-1;
						boolean trouve2=false;
						for(int z=e+1;z<donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve2;z++)
							if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[z].typeElement == 0
									&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[z].indiceElement == i) {
								e2=z;
								trouve=true;
							}
						//on comptabilise les diff�rents temps d'attente
						// attentes d'acces
						// attente totale d'acces
						double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].acces;

						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
						}

						// attentes de marees
						// attente totale de marees
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].marees;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
						// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
						}

						// attentes d'Secu
						// attente totale d'Secu
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].secu;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
						// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
						}

						// attentes d'Occup
						// attente totale d'Occup
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].occupation;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
						// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
						}

						// attentes d'Panne
						// attente totale d'Panne
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].indispo;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
						// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
						}

						// attentes d'totale
						// attente totale d'acces
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						if(e2!=-1)
							attente+=  donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].secu;

						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
						}

					}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		// cercles de vitage
		for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].typeElement = 1;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                    .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
			  // on r�cup�re la  cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				// pour chaque element du trajet du navire k des qu on trouve el premeir element on se retire
				trouve = false;
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 1
							&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
						trouve = true;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
						
						int e2=-1;
						boolean trouve2=false;
						for(int z=e+1;z<donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve2;z++)
							if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[z].typeElement == 1
									&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[z].indiceElement == i) {
								e2=z;
								trouve=true;
							}

						// on comptabilise les diff�rents temps d'attente

						// attentes d'acces
						// attente totale d'acces
						double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].acces;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
						}

						// attentes de marees
						// attente totale de marees
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].marees;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
						// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
						}

						// attentes d'Secu
						// attente totale d'Secu
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].secu;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
						// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
						}

						// attentes d'Occup
						// attente totale d'Occup
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].occupation;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
						// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
						}

						// attentes d'Panne
						// attente totale d'Panne
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].indispo;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
						// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
						}

						// attentes d'totale
						// attente totale d'acces
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						if(e2!=-1)
							attente+= donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].secu;

						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
						}

					}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		// ecluse
		for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].typeElement = 2;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                    .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r� cat�gorie cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				// pour chaque element du trajet du navire k des qu on trouve el premeir element on se retire
				trouve = false;
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 2
							&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
						trouve = true;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
						

						int e2=-1;
						boolean trouve2=false;
						for(int z=e+1;z<donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve2;z++)
							if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[z].typeElement == 2
									&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[z].indiceElement == i) {
								e2=z;
								trouve=true;
							}
						// on comptabilise les diff�rents temps d'attente

						// attentes d'acces
						// attente totale d'acces
						double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
						if(e2!=-1)
							attente += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].acces;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
						}

						// attentes de marees
						// attente totale de marees
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
						if(e2!=-1)
							attente += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].marees;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
						// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
						}

						// attentes d'Secu
						// attente totale d'Secu
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						if(e2!=-1)
							attente += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].secu;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
						// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
						}

						// attentes d'Occup
						// attente totale d'Occup
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
						if(e2!=-1)
							attente += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].occupation;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
						// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
						}

						// attentes d'Panne
						// attente totale d'Panne
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
						if(e2!=-1)
							attente += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].indispo;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
						// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
						}

						// attentes d'totale
						// attente totale d'acces
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						if(e2!=-1)
							attente += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].acces
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].marees
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].indispo
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].occupation
							+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e2].secu;

						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
						}

					}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		// quai
		for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente] = new SParametresResultatsAttente();
			// type element: il s agit d un quai donc 3
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].typeElement = 3;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].indiceElement = i;
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
			                                                                                                                                                    .size()];
			// initialisation de tous les parametres d'attente
			for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nombreNaviresTotal = 0;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteAccesMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMareeMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteSecuMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteOccupMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attentePanneMini = 9999999;

				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteMegaTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMaxi = 0;
				resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[k].attenteTotaleMini = 9999999;

			}
			// boucle sur l historique et pour chaque ligne, on remplit les donn�e dans la bonne case grace a l indice de
			// cat�gorie

			for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
				// on r�cup�re cat�gorie  cat�gorie associee au navire:
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				trouve = false;
				// pour chaque element du trajet du navire k
				for (int e = 0; e < donnees_.getListeResultatsSimu_().listeEvenements[k].NbElemtnsParcours && !trouve; e++) {
					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].typeElement == 3
							&& donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indiceElement == i) {
						trouve = true;
						// on comptabilise les diff�rents temps d'attente
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nombreNaviresTotal++;
						
						// attentes d'acces
						// attente totale d'acces
						double attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = attente;
						}

						// attentes de marees
						// attente totale de marees
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeTotale += attente;
						// on incremente nb navires ayant attendu l'Maree si le temps d'Maree est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteMaree++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = attente;
						}

						// attentes d'Secu
						// attente totale d'Secu
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuTotale += attente;
						// on incremente nb navires ayant attendu l'Secu si le temps d'Secu est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = attente;
						}

						// attentes d'Occup
						// attente totale d'Occup
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupTotale += attente;
						// on incremente nb navires ayant attendu l'Occup si le temps d'Occup est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = attente;
						}

						// attentes d'Panne
						// attente totale d'Panne
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneTotale += attente;
						// on incremente nb navires ayant attendu l'Panne si le temps d'Panne est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttentePanne++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = attente;
						}

						// attentes d'totale
						// attente totale d'acces
						attente = donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].acces
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].marees
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].indispo
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].occupation
						+ donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[e].secu;
						resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMegaTotale += attente;
						// on incremente nb navires ayant attendu l'acces si le temps d'acces est non nul:
						if (attente != 0) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						}
						// on teste si l'attente est minimale ou maximale:
						if (attente > resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						}
						if (attente < resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini) {
							resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = attente;
						}

					}
				}

			}

			for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteAccesMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteMareeMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteSecuMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteOccupMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attentePanneMini = 0;
				}
				if (resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
					resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente].tableauAttenteCategories[n].attenteTotaleMini = 0;
				}
			}
			// incremente l element
			compteurElementsAttente++;
		}

		// on met par defaut les attentes en entr�es dasn l'�l�ment:
		donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens1;

	}

	/**
	 * Methode statique qui ne fais que charger la structure en fonction du sens donn�een effet tous les calculs ont �t�
	 * �ffectu�s apr�s simulation:
	 * 
	 * @param donnees_
	 * @param sensCirculation
	 * @param heureDep
	 * @param heureFin
	 */
	public static void calcul(final SiporDataSimulation donnees_, final int sensCirculation) {

		if (sensCirculation == 0) {
			// sens entrant: alors on charge la structure des r�sultats dans le sens entrant
			donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens1;
		} else if (sensCirculation == 1) {
			// sens sortant: alors on charge la structure des r�sultats dans le sens sortant
			donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens2;

		} else {
			// les 2 sens: alors on charge la structure des r�sultats dans les 2 sens
			donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesLes2Sens;

		}
	}// fin methode statique de calcul







	public static int[] existenceTrajet(final int typeElementDepart, final int typeElementArrivee,
			final int elementDepart, final int elementArrivee, final SiporResultatsDonneeTrajet[] trajet) {
		final int[] t = new int[4];
		// on initialise le tableau avec les indices non trouv�s
		t[0] = -1;
		t[1] = -1;
		t[2] = -1;
		t[3] = -1;
		int	init=-1;
		boolean trouve=false;

		//--Depart aller --//
		for (int i = 0; i < trajet.length && !trouve; i++) {

			//si l'on a d�pass� le quai c est trop tard
			if (trajet[i].typeElement==3 &&  typeElementDepart!=3){
				trouve=true;
			}
			else
				// des qu on a trouv� les elements de depart et d'arrivee, alors on peut quitter
				if (t[0] != -1 ) {
					trouve=true;
				} 
				else {
					// si l'�l�ment de depart est �quivalent � l'�l�ment du trajet alors on a trouv� notre depart
					if (typeElementDepart == trajet[i].typeElement && elementDepart == trajet[i].indiceElement) {
						t[0] = i;
					}
				}

		}


		//--Depart retour --//


		init=t[0]+1;
		trouve=false;
		for (int i = init; i < trajet.length && !trouve; i++) {
			// des qu on a trouv� les elements de depart et d'arrivee, alors on peut quitter
			if (t[2] != -1 ) {
				trouve=true;
			} 
			else {
				// si l'�l�ment de depart est �quivalent � l'�l�ment du trajet alors on a trouv� notre depart
				if (typeElementDepart == trajet[i].typeElement && elementDepart == trajet[i].indiceElement ) {
					t[2] = i;
				}
			}

		}

		//-- cas particulier du quai: on donne le meme indice a l aller qu au retour --//
		if(typeElementDepart==3 && t[0] != -1 )
			t[2]=t[0];

		trouve=false;

		//--arriv�e all� --//
		for (int i = 0; i < trajet.length && !trouve; i++) {

			//si l'on a d�pass� le quai c est trop tard
			if (trajet[i].typeElement==3 &&  typeElementArrivee!=3){
				trouve=true;
			}
			else
				// des qu on a trouv� les elements de depart et d'arrivee, alors on peut quitter
				if (t[1] != -1 ) {
					trouve=true;
				} 
				else {
					// si l'�l�ment de depart est �quivalent � l'�l�ment du trajet alors on a trouv� notre depart
					if (typeElementArrivee == trajet[i].typeElement && elementArrivee == trajet[i].indiceElement) {
						t[1] = i;
					}
				}

		}
		//--Arriv�e retour --//

		init=t[1]+1;
		trouve=false;
		for (int i = init; i < trajet.length && !trouve; i++) {
			// des qu on a trouv� les elements de depart et d'arrivee, alors on peut quitter
			if (t[3] != -1 ) {
				trouve=true;
			} 
			else {
				// si l'�l�ment de depart est �quivalent � l'�l�ment du trajet alors on a trouv� notre depart
				if (typeElementArrivee == trajet[i].typeElement && elementArrivee == trajet[i].indiceElement
				) {
					t[3] = i;
				}
			}
		}

		//-- cas particulier du quai: on donne le meme indice a l aller qu au retour --//
		if(typeElementArrivee==3 && t[1] != -1 )
			t[3]=t[1];




		// verification que l'utilisateur a bien entr� son element de depart =>ie qu il se trouve bien avant l'�l�ment
		// d'arriv�e
		// sinon l'algorithme le remet dans l'ordre:

		if (t[0] > t[1]) {
			final int temp = t[0];
			t[0] = t[1];
			t[1] = temp;
		}


		if (t[2] > t[3] && t[3]!=-1) {
			final int temp = t[2];
			t[2] = t[3];
			t[3] = temp;
		}


		return t;
	}

	/**
	 * methode qui calcule les attentes sur un trajet donn� par l'utilisateur:
	 * 
	 * @param typeDepart
	 * @param indiceElementDep
	 * @param typeArrivee
	 * @param indiceElementAr
	 * @param sens
	 */
	public static void calculTrajet(final SiporDataSimulation donnees_, final int typeElementDepart,
			final int elementDepart, final int typeElementArrivee, final int elementArrivee, final int sens) {


		/*
		 * sens: 0: sens amont vers aval: on ne cherche que les attentes dans les gares amont de lelement 1: sens aval vers
		 * amont: on ne cherche les temps que dans les gares aval de l element 2: les 2 sens : on cumule les attentes a la
		 * fois des gares amont et aval.
		 */

		final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

		// allocation de m�moire pour un element: cet element est un trajet
		resultats.tableauAttenteTrajet = new SParametresResultatsAttente();

		resultats.tableauAttenteTrajet.tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
		                                                                                                   .size()];
		// initialisation de tous les parametres d'attente
		for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini = 9999999;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini = 9999999;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini = 9999999;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini = 9999999;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini = 9999999;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi = 0;
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini = 9999999;
		}
		// boucle sur l historique et pour chaque ligne, on remplit les ddonn�edans la bonne case grace a l indice de
		// cat�gorie

		for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
			int[] t = new int[4];
			int Depart = 0;
			int Arrivee = 0;
			int Depart2=0;
			int Arrivee2=0;
			// on recherche dans le trajet du navire si le sous trajet existe
			t = SiporAlgorithmeAttentesGenerales.existenceTrajet(typeElementDepart, typeElementArrivee, elementDepart,
					elementArrivee, donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet);
			// la m�thode retourne les indices des positions depart et arrivee du le tableau de trajet dans t
			if (  (t[0] != -1 && t[1] != -1 && sens==0)  //si sens all� et on a trouv� les �l�ments � l aller
					||(t[2] != -1 && t[3] != -1 && sens==1)  // ou si sens retour et on a trouv� elements retour
					||sens==2  							//ou si sens = aller - retour	
			) {
				// si le depart et l arrivee appartiennent au trajet du navire
				if (sens == 0 || sens == 2)// si sens est entrant ou dans les 2 sens
				{
					Depart = t[0];
					Arrivee = t[1];
				} else {
					// cas ou le snsn du trajet est sortant, alors on doit inverser les depart et arrivee que l'on peut retrouver
					// le depart correpond a la deuxieme occurence de l element et vu que le trajet est impair
					// et commence par 0, ion peut d�terminer le depart et l'arrivee du chemin au retour

					Depart = t[2];

					Arrivee= t[3];
				}

				double ATTENTE_TOTALE_MAREE = 0;
				double ATTENTE_TOTALE_ACCES = 0;
				double ATTENTE_TOTALE_SECURITE = 0;
				double ATTENTE_TOTALE_OCCUPATION = 0;
				double ATTENTE_TOTALE_INDISPONIBILITE = 0;
				final int n = donnees_.getListeResultatsSimu_().listeEvenements[k].categorie;
				// on comptabilise les attentes entre les bornes depart et arrivee

				//-- cas aller si choix != DOUBLE SENS OU choix= DOUBLE SENS et elements existent--//
				if(   sens !=2     ||      (sens==2 && t[0]!=-1 && t[1]!=-1)    ){
					for (int i = Depart; i < Arrivee + 1; i++) // on a v�rifi� dans l'autre fonction que depart<arrivee
					{ 
						// on comptabilise les attentes dans le cas du sens entrant ou sortant
						// ou alors is c 'est dans les 2 sens mais il s'agit d'un quai donc pas de retour a comptabiliser
						ATTENTE_TOTALE_ACCES += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].acces;
						ATTENTE_TOTALE_MAREE += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].marees;
						ATTENTE_TOTALE_SECURITE += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].secu;
						ATTENTE_TOTALE_OCCUPATION += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].occupation;
						ATTENTE_TOTALE_INDISPONIBILITE += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].indispo;

					}
				}

				//-- cas retour si choix= DOUBLE SENS et elements existent--//
				if(sens==2 && t[2]!=-1 && t[3]!=-1){
					Depart2=t[2];
					Arrivee2=t[3];
					for (int i = Depart2; i < Arrivee2 + 1; i++){          
						if(donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].typeElement != 3){
							// on a choisi les 2 sens, on rajoute donc les sorties sauf pour le quai
							// qui n est apparu qu une fois
							ATTENTE_TOTALE_ACCES += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].acces;
							ATTENTE_TOTALE_MAREE += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].marees;
							ATTENTE_TOTALE_SECURITE += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].secu;
							ATTENTE_TOTALE_OCCUPATION += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].occupation;
							ATTENTE_TOTALE_INDISPONIBILITE += donnees_.getListeResultatsSimu_().listeEvenements[k].tableauTrajet[i].indispo;
						}

					}//fin du for

				}//fin du if cas double sens



				// on augente le nombre de navires de la cat�gorie n
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nombreNaviresTotal++;
				// on ajoute les attentes totales du trajet et on determine le max, min et le nombre de navires qui ont attendus
				// attente acces
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesTotale += ATTENTE_TOTALE_ACCES;
				if (ATTENTE_TOTALE_ACCES != 0) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces++;
				}
				if (ATTENTE_TOTALE_ACCES > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi = ATTENTE_TOTALE_ACCES;
				}
				if (ATTENTE_TOTALE_ACCES < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini = ATTENTE_TOTALE_ACCES;
				}
				// attente maree
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeTotale += ATTENTE_TOTALE_MAREE;
				if (ATTENTE_TOTALE_MAREE != 0) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteMaree++;
				}
				if (ATTENTE_TOTALE_MAREE > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMaxi) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMaxi = ATTENTE_TOTALE_MAREE;
				}
				if (ATTENTE_TOTALE_MAREE < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMini) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMini = ATTENTE_TOTALE_MAREE;
				}
				// attente secu
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuTotale += ATTENTE_TOTALE_SECURITE;
				if (ATTENTE_TOTALE_SECURITE != 0) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu++;
				}
				if (ATTENTE_TOTALE_SECURITE > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi = ATTENTE_TOTALE_SECURITE;
				}
				if (ATTENTE_TOTALE_SECURITE < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini = ATTENTE_TOTALE_SECURITE;
				}
				// attente occup
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupTotale += ATTENTE_TOTALE_OCCUPATION;
				if (ATTENTE_TOTALE_OCCUPATION != 0) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup++;
				}
				if (ATTENTE_TOTALE_OCCUPATION > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi = ATTENTE_TOTALE_OCCUPATION;
				}
				if (ATTENTE_TOTALE_OCCUPATION < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini = ATTENTE_TOTALE_OCCUPATION;
				}
				// attente indisponibilit�
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneTotale += ATTENTE_TOTALE_INDISPONIBILITE;
				if (ATTENTE_TOTALE_INDISPONIBILITE != 0) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne++;
				}
				if (ATTENTE_TOTALE_INDISPONIBILITE > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi = ATTENTE_TOTALE_INDISPONIBILITE;
				}
				if (ATTENTE_TOTALE_INDISPONIBILITE < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini = ATTENTE_TOTALE_INDISPONIBILITE;
				}

				// attente totale:
				final double dureeTotale = ATTENTE_TOTALE_OCCUPATION + ATTENTE_TOTALE_INDISPONIBILITE + ATTENTE_TOTALE_SECURITE
				+ ATTENTE_TOTALE_MAREE + ATTENTE_TOTALE_ACCES;
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMegaTotale += dureeTotale;
				if (dureeTotale != 0) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale++;

				}
				if (dureeTotale > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi = dureeTotale;
				}
				if (dureeTotale < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini) {
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini = dureeTotale;
				}

			}// fin du si le sous trajet appartient au trajet du navire

		}

		for (int n = 0; n < donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMareeMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini = 0;
			}
		}


	}

	public static void calculTrajetDepuisListeTrajet(final SiporDataSimulation donnees_, final int typeElementDepart,
			final int elementDepart, final int typeElementArrivee, final int elementArrivee, final int sens) {


		/*
		 * sens: 0: sens amont vers aval: on ne cherche que les attentes dans les gares amont de lelement 1: sens aval vers
		 * amont: on ne cherche les temps que dans les gares aval de l element 2: les 2 sens : on cumule les attentes a la
		 * fois des gares amont et aval.
		 */

		final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

		// allocation de m�moire pour un element: cet element est un trajet
		resultats.tableauAttenteTrajet = new SParametresResultatsAttente();

		resultats.tableauAttenteTrajet.tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.getCategoriesNavires_().getListeNavires_()
		                                                                                                   .size()];

		for (int i = 0; i < resultats.TOUTESAttenteTrajet.length; i++) {
			if (resultats.TOUTESAttenteTrajet[i].indiceElement == elementDepart
					&& resultats.TOUTESAttenteTrajet[i].typeElement == typeElementDepart
					&& resultats.TOUTESAttenteTrajet[i].indiceElement2 == elementArrivee
					&& resultats.TOUTESAttenteTrajet[i].typeElement2 == typeElementArrivee
					&& resultats.TOUTESAttenteTrajet[i].sens == sens) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories = resultats.TOUTESAttenteTrajet[i].tableauAttenteCategories;
				return;
			} else if (resultats.TOUTESAttenteTrajet[i].indiceElement == elementArrivee
					&& resultats.TOUTESAttenteTrajet[i].typeElement == typeElementArrivee
					&& resultats.TOUTESAttenteTrajet[i].indiceElement2 == elementDepart
					&& resultats.TOUTESAttenteTrajet[i].typeElement2 == typeElementDepart
					&& resultats.TOUTESAttenteTrajet[i].sens == sens) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories = resultats.TOUTESAttenteTrajet[i].tableauAttenteCategories;
				return;
			}

		}

	}

	/**
	 * Methode qui determine l attente maximum pour les attentes g�n�ralis�es
	 * 
	 * @param donnees_
	 * @return
	 */
	public static float determineAttenteMax(final SiporDataSimulation donnees_) {
		float max = 0;

		for (int i = 0; i < donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; i++) {
			for (int j = 0; j < donnees_.getCategoriesNavires_().getListeNavires_().size(); j++) {
				if (max < donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[j].attenteTotaleMaxi) {
					max = (float) donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[j].attenteTotaleMaxi;
				}

			}
		}
		return (float) (max + 25.0 / 100.0 * max);
	}

	/**
	 * Methode qui determine les attentes maximum pour les attentes de parcours
	 * 
	 * @param donnees_
	 * @return
	 */
	public static float determineAttenteMaxTrajet(final SiporDataSimulation donnees_) {
		float max = 0;

		for (int j = 0; j < donnees_.getCategoriesNavires_().getListeNavires_().size(); j++) {
			if (max < donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleMaxi) {
				max = (float) donnees_.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleMaxi;
			}

		}
		return (float) (max + 25.0 / 100.0 * max);
	}

}
