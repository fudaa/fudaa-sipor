/*
 * @file         SiporDialogueParametresAffichageGraphiques.java
 * @creation     1999-10-01
 * @modification $Date: 2007-03-09 08:39:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;

import cz.autel.dmi.HIGConstraints;
import cz.autel.dmi.HIGLayout;

/**
 * Boite de dialogue pour saisir les param�tres d'affichage des graphiques sur plusieurs simulations (dans la commande
 * exploitation du projet).
 * 
 * @version $Revision: 1.7 $ $Date: 2007-03-09 08:39:00 $ by $Author: deniger $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class SiporDialogueParametresAffichageGraphiques extends JDialog {
  // Donn�es.
  int minAbs, maxAbs, minOrd, maxOrd;
  int margeH = 50, margeB = 50, margeG = 50, margeD = 50;
  boolean minAbsAuto = true, maxAbsAuto = true, minOrdAuto = true, maxOrdAuto = true;
  boolean couleur = true, legende = true;
  int hauteurGraphe = 400, largeurGraphe = 400;
  // Zones pour saisir les min et max sur les axes.
  BuTextField tfMinAbs = BuTextField.createIntegerField();
  BuTextField tfMaxAbs = BuTextField.createIntegerField();
  BuTextField tfMinOrd = BuTextField.createIntegerField();
  BuTextField tfMaxOrd = BuTextField.createIntegerField();
  // Cases � cocher pour min et max automatiques sur les axes.
  JCheckBox boxMinAbsAuto = new JCheckBox("Auto", true);
  JCheckBox boxMaxAbsAuto = new JCheckBox("Auto", true);
  JCheckBox boxMinOrdAuto = new JCheckBox("Auto", true);
  JCheckBox boxMaxOrdAuto = new JCheckBox("Auto", true);
  // Zones pour saisir les marges.
  BuTextField tfMargeH = BuTextField.createIntegerField();
  BuTextField tfMargeB = BuTextField.createIntegerField();
  BuTextField tfMargeG = BuTextField.createIntegerField();
  BuTextField tfMargeD = BuTextField.createIntegerField();
  // Cases � cocher pour la l�gende et la couleur.
  JCheckBox boxLegende = new JCheckBox("l�gende", true);
  JCheckBox boxCouleur = new JCheckBox("couleur", true);
  // Zones pour saisir la taille de la fenetre du graphique.
  BuTextField tfLargeurGraphe = BuTextField.createIntegerField();
  BuTextField tfHauteurGraphe = BuTextField.createIntegerField();

  /**
   * Constructeur.
   * 
   * @param parent fenetre proprietaire de la boite de dialogue.
   */
  public SiporDialogueParametresAffichageGraphiques(final Frame parent) {
    super(parent, "Param�tres pour l'affichage du graphique", true);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    final int[] widths = { 10, 100, 100, 100, 20, 100, 100, 10 };
    final int[] heights = { 10, 25, 25, 25, 25, 25, 25, 25, 25, 25, 50, 10 };
    final HIGLayout lm = new HIGLayout(widths, heights);
    final HIGConstraints c = new HIGConstraints();
    final Container contenu = this.getContentPane();
    contenu.setLayout(lm);
    // Affiche les composants pour le min et la max sur abscisse et ordonn�e/
    contenu.add(new BuLabel("Minimum"), c.rcwh(2, 3, 2, 1, ""));
    contenu.add(new BuLabel("Maximum"), c.rcwh(2, 6, 2, 1, ""));
    contenu.add(new BuLabel("Abscisses : "), c.rc(3, 2, "l"));
    contenu.add(new BuLabel("Orddonn�es: "), c.rc(4, 2, "l"));
    contenu.add(boxMinAbsAuto, c.rc(3, 4, "l"));
    contenu.add(boxMaxAbsAuto, c.rc(3, 7, "l"));
    contenu.add(boxMinOrdAuto, c.rc(4, 4, "l"));
    contenu.add(boxMaxOrdAuto, c.rc(4, 7, "l"));
    contenu.add(tfMinAbs, c.rc(3, 3, "ll"));
    contenu.add(tfMaxAbs, c.rc(3, 6, "ll"));
    contenu.add(tfMinOrd, c.rc(4, 3, "ll"));
    contenu.add(tfMaxOrd, c.rc(4, 6, "ll"));
    // Ajoute les composants pour les marges.
    contenu.add(new BuLabel("Marges"), c.rc(6, 2, "l"));
    contenu.add(new BuLabel("Haut : "), c.rc(6, 3, "r"));
    contenu.add(tfMargeH, c.rc(6, 4, "ll"));
    contenu.add(new BuLabel("Bas : "), c.rc(6, 6, "r"));
    contenu.add(tfMargeB, c.rc(6, 7, "ll"));
    contenu.add(new BuLabel("Gauche : "), c.rc(7, 3, "r"));
    contenu.add(tfMargeG, c.rc(7, 4, "ll"));
    contenu.add(new BuLabel("Droite : "), c.rc(7, 6, "r"));
    contenu.add(tfMargeD, c.rc(7, 7, "ll"));
    // Case � cocher pour la l�gende et la couleur.
    contenu.add(new BuLabel("Afficher"), c.rc(9, 2, "l"));
    contenu.add(boxLegende, c.rcwh(9, 3, 2, 1, ""));
    contenu.add(boxCouleur, c.rcwh(9, 6, 2, 1, ""));
    // Taille de la fenetre graphique.
    contenu.add(new BuLabel("Graphique"), c.rc(10, 2, "l"));
    contenu.add(new BuLabel("Largeur : "), c.rc(10, 3, "r"));
    contenu.add(tfLargeurGraphe, c.rc(10, 4, "ll"));
    contenu.add(new BuLabel("Hauteur : "), c.rc(10, 6, "r"));
    contenu.add(tfHauteurGraphe, c.rc(10, 7, "ll"));
    // Bouton pour valider.
    final BuButton bValider = new BuButton(BuResource.BU.getIcon("valider"), "Valider");
    bValider.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent evt) {
        zonesVersDonnees();
        setVisible(false);
      }
    });
    contenu.add(bValider, c.rcwh(11, 2, 2, 1, "b"));
    // Bouton pour annuler.
    final BuButton bAnnuler = new BuButton(BuResource.BU.getIcon("annuler"), "Annuler");
    bAnnuler.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent evt) { // Met les valeurs des donn�es dans les zones.
        donneesVersZones();
        setVisible(false);
      }
    });
    contenu.add(bAnnuler, c.rcwh(11, 6, 2, 1, "b"));
    // Activer les TextFields selon "auto" pour les min et max.
    final ChangeListener cl = new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        JCheckBox source = (JCheckBox) evt.getSource();
        if (source == boxMinAbsAuto) {
          tfMinAbs.setEnabled(!source.isSelected());
        } else if (source == boxMaxAbsAuto) {
          tfMaxAbs.setEnabled(!source.isSelected());
        } else if (source == boxMinOrdAuto) {
          tfMinOrd.setEnabled(!source.isSelected());
        } else if (source == boxMaxOrdAuto) {
          tfMaxOrd.setEnabled(!source.isSelected());
        }
      }
    };
    boxMinAbsAuto.addChangeListener(cl);
    boxMaxAbsAuto.addChangeListener(cl);
    boxMinOrdAuto.addChangeListener(cl);
    boxMaxOrdAuto.addChangeListener(cl);
    donneesVersZones();
    this.setResizable(false);
    this.pack();
  }

  /**
   * Copie les donn�es les zones de saisie.
   */
  void donneesVersZones() {
    // Zones pour saisir les min et max sur les axes.
    tfMinAbs.setValue(new Integer(minAbs));
    tfMaxAbs.setValue(new Integer(maxAbs));
    tfMinOrd.setValue(new Integer(minOrd));
    tfMaxOrd.setValue(new Integer(maxOrd));
    // Cases � cocher pour min et max automatiques sur les axes.
    boxMinAbsAuto.setSelected(minAbsAuto);
    boxMaxAbsAuto.setSelected(maxAbsAuto);
    boxMinOrdAuto.setSelected(minOrdAuto);
    boxMaxOrdAuto.setSelected(maxOrdAuto);
    // Zones pour saisir les marges.
    tfMargeH.setValue(new Integer(margeH));
    tfMargeB.setValue(new Integer(margeB));
    tfMargeG.setValue(new Integer(margeG));
    tfMargeD.setValue(new Integer(margeD));
    // Cases � cocher pour la l�gende et la couleur.
    boxLegende.setSelected(legende);
    boxCouleur.setSelected(couleur);
    // Zones pour saisir la taille de la fenetre du graphique.
    tfLargeurGraphe.setValue(new Integer(largeurGraphe));
    tfHauteurGraphe.setValue(new Integer(hauteurGraphe));
  }

  /**
   * Lit les valeurs des zones de texte et les affecte aux donn�es */
  void zonesVersDonnees() {
    // Zones pour saisir les min et max sur les axes.
    minAbs = ((Integer) tfMinAbs.getValue()).intValue();
    maxAbs = ((Integer) tfMaxAbs.getValue()).intValue();
    minOrd = ((Integer) tfMinOrd.getValue()).intValue();
    maxOrd = ((Integer) tfMaxOrd.getValue()).intValue();
    // Cases � cocher pour min et max automatiques sur les axes.
    minAbsAuto = boxMinAbsAuto.isSelected();
    maxAbsAuto = boxMaxAbsAuto.isSelected();
    minOrdAuto = boxMinOrdAuto.isSelected();
    maxOrdAuto = boxMaxOrdAuto.isSelected();
    // Zones pour saisir les marges.
    margeH = ((Integer) tfMargeH.getValue()).intValue();
    margeB = ((Integer) tfMargeB.getValue()).intValue();
    margeG = ((Integer) tfMargeG.getValue()).intValue();
    margeD = ((Integer) tfMargeD.getValue()).intValue();
    // Cases � cocher pour la l�gende et la couleur.
    legende = boxLegende.isSelected();
    couleur = boxCouleur.isSelected();
    // Zones pour saisir la taille de la fenetre du graphique.
    largeurGraphe = ((Integer) tfLargeurGraphe.getValue()).intValue();
    hauteurGraphe = ((Integer) tfHauteurGraphe.getValue()).intValue();
  }
}
