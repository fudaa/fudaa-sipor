package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsDureParcoursCategories;
import org.fudaa.dodico.corba.sipor.SParametresResultatsDureesParcoursTrajet;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

public class SiporAlgorithmeTOUTESDureesParcours {

  /**
   * Methode qui calcule toutes les dur�es de parcours en evaluant toutes les combinaisons de trajet
   * 
   * @param _d: donnes
   */
  public static void calcul(final SiporDataSimulation _d) {
    // calcul de toutes les dur�es de parcours

    // 0: determiner le place totale a allouer:
    _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs = new SParametresResultatsDureesParcoursTrajet[_d.getListeChenal_().getListeChenaux_()
        .size()
        * (_d.getListeChenal_().getListeChenaux_().size() * 2 + _d.getListeCercle_().getListeCercles_().size() * 2
            + _d.getListeEcluse_().getListeEcluses_().size() * 2 + _d.getlQuais_().getlQuais_().size() * 2)
        + _d.getListeCercle_().getListeCercles_().size()
        * (_d.getListeCercle_().getListeCercles_().size() * 2 + _d.getListeEcluse_().getListeEcluses_().size() * 2 + _d.getlQuais_().getlQuais_()
            .size() * 2)
        + _d.getListeEcluse_().getListeEcluses_().size()
        * (_d.getListeEcluse_().getListeEcluses_().size() * 2 + _d.getlQuais_().getlQuais_().size() * 2)];

    int compteur = 0;

    // 1: entre les chenaux
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = i; j < _d.getListeChenal_().getListeChenaux_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 0, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 0, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }

    // 2: chenaux et cercles
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < _d.getListeCercle_().getListeCercles_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 1, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 1, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 3: chenaux et ecluses
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < _d.getListeEcluse_().getListeEcluses_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 2, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 2, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 4 chenaux et quais
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < _d.getlQuais_().getlQuais_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 3, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 0, i, 3, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 5 cercles et cercles
    for (int i = 0; i < _d.getListeCercle_().getListeCercles_().size(); i++) {
      for (int j = i; j < _d.getListeCercle_().getListeCercles_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 1, i, 1, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 1, i, 1, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 6 cercles et ecluses
    for (int i = 0; i < _d.getListeCercle_().getListeCercles_().size(); i++) {
      for (int j = 0; j < _d.getListeEcluse_().getListeEcluses_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 1, i, 2, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 1, i, 2, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 7 cercles et quais
    for (int i = 0; i < _d.getListeCercle_().getListeCercles_().size(); i++) {
      for (int j = 0; j < _d.getlQuais_().getlQuais_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 1, i, 3, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 1, i, 3, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 8 ecluses et ecluses
    for (int i = 0; i < _d.getListeEcluse_().getListeEcluses_().size(); i++) {
      for (int j = i; j < _d.getListeEcluse_().getListeEcluses_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 2, i, 2, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 2, i, 2, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;

      }
    }
    // 9 ecluses et quais
    for (int i = 0; i < _d.getListeEcluse_().getListeEcluses_().size(); i++) {
      for (int j = i; j < _d.getlQuais_().getlQuais_().size(); j++) {
        // calcul dans le sens entrant:
        SiporAlgorithmeDureesParcours.calcul(_d, 2, i, 3, j, 0);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }
        compteur++;
        // calcul dans le sens sortant:
        SiporAlgorithmeDureesParcours.calcul(_d, 2, i, 3, j, 1);
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
        }

        if (i != _d.getListeEcluse_().getListeEcluses_().size() - 1 || j != _d.getlQuais_().getlQuais_().size()) {
          compteur++;
        }

      }
    }

  }
}
