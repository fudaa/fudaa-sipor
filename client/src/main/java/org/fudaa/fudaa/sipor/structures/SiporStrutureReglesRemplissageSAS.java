package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;

public class SiporStrutureReglesRemplissageSAS {

  /**
   * definition de la matrice produit cart�sien entre SAS �cluse et cat�gories de navire.
   */
  ArrayList matriceDelais = new ArrayList();

  public SiporStrutureReglesRemplissageSAS() {

  }

  /**
   * methjode d'ajout d'un ligne
   */
  public void ajoutLigne(final int _numNavires) {

    final ArrayList vecteurEcluse = new ArrayList();

    for (int k = 0; k < _numNavires; k++) {
      vecteurEcluse.add(new Double(0));
    }

    this.matriceDelais.add(vecteurEcluse);

  }

  /**
   * Methode de suppression d une ligne
   * 
   * @param _numEcluse
   */
  public void SuprimerLigne(final int _numEcluse) {
    this.matriceDelais.remove(_numEcluse);
  }

  /**
   * Methode d'ajout d'une colonne ie ajout d un navire
   */

  public void ajoutNavire() {
    for (int k = 0; k < this.matriceDelais.size(); k++) {
      final ArrayList vecteurLigne = (ArrayList) this.matriceDelais.get(k);
      vecteurLigne.add(new Double(0));
    }

  }

  /**
   * Methode de suppression d un navire
   * 
   * @param _numNavire
   */
  public  void suppressionNavire(final int _numNavire) {
    for (int k = 0; k < this.matriceDelais.size(); k++) {
      final ArrayList vecteurLigne = (ArrayList) this.matriceDelais.get(k);
      vecteurLigne.remove(_numNavire);
    }

  }

  /**
   * Methode de recuperation de la valeur (i j) de la matrice
   * 
   * @param ligne indice de la ligne
   * @param col indice de la colonne
   * @return la valeur associ�e a la case i j de la matrice
   */
  public  double retournerValeur(final int ligne, final int col) {
    double val = 0;
    final ArrayList vecteurLigne = (ArrayList) this.matriceDelais.get(ligne);
    val = ((Double) vecteurLigne.get(col)).doubleValue();

    return val;
  }

  /**
   * Methode d insertion d un objet de type Double dans la matrice
   * 
   * @param val valeure a inserer
   * @param ligne indice de ligne
   * @param col indice de colonne
   */
  public  void modifierValeur(final double val, final int ligne, final int col) {
    final Double nouveau = new Double(val);

    final ArrayList vecteurLigne = (ArrayList) this.matriceDelais.get(ligne);
    vecteurLigne.set(col, nouveau);

  }
  
  
  public void dupliquerLigne(final int lineModel, final int lineTarget, final int numNavires) {
	  
	  for(int i=0;i< numNavires; i++){
		  modifierValeur( retournerValeur(lineModel, i),lineTarget, i);
	  }
	  
  }
  

public ArrayList getMatriceDuree() {
	return matriceDelais;
}

public void setMatriceDuree(ArrayList matriceDelais) {
	this.matriceDelais = matriceDelais;
}

}
