package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserCercles;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextField;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldFloat;

/**
 * Panel de saisie des cercles d'�vitage
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelSaisieCercle extends JPanel {

  // attributs:
  static int nbouverture = 0;
  /**
   * Jtext du nom a saisir
   */
  SiporTextField nom_ = new SiporTextField(10);

  /**
   * Jtext de saisie du diametre
   */

  SiporTextFieldFloat diametre_ = new SiporTextFieldFloat(3);

  /**
   * Bouton de validation du Cercle
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * Bordures du panel
   */

  
  /**
   * Mode du panel: si le mode modification est true alors cela signifie que l on est en mode modification et donc le
   * booleen est = a true par defaut le mode modification est off
   */
  boolean MODE_MODIFICATION_ON_ = false;

  /**
   * Indice du cercle a modifier dnas le tableau de cercles
   */
  int CERCLE_A_MODIFIER_;

  /**
   * FENETRE PRINCIPALE DE GESTION DES CERCLES: CETTE FENETRE A TOUS LES POUVOIRS
   */
  SiporVisualiserCercles MENUCERCLES_;

  /**
   * parametres de la simulation
   */
  SiporDataSimulation donnees_;

  public SiporPanelSaisieCercle(final SiporDataSimulation d, final SiporVisualiserCercles vc) {

    MENUCERCLES_ = vc;
    donnees_ = d;
    nbouverture++;
    this.setLayout(new BorderLayout());

    /**
     * Controles de la frame
     */
    this.diametre_
        .setToolTipText("Saisissez le diam�tre en m�tres du cercle d'�vitage ici. Par exmple, un diam�tre de 10 m�tres");

   

    this.nom_.setText("Cercle " + (donnees_.getListeCercle_().getListeCercles_().size() + 1));
    this.nom_.setToolTipText("Saisissez le nom du cercle d'�vitage ici");

    validation_.setToolTipText("Cliquez sur ce bouton pour valider la saisie");

    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.println("validation du nom du cercle d'�vitage:");
        if (nom_.getText().equals("")) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le cercle d'�vitage n'as pas de nom.").activate();
        } else if (MODE_MODIFICATION_ON_ && donnees_.getListeCercle_().existeDoublon(nom_.getText(), CERCLE_A_MODIFIER_)) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le nom est d�j� pris.").activate();
        } else if (!MODE_MODIFICATION_ON_ && donnees_.getListeCercle_().existeDoublon(nom_.getText(), -1)) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le nom est d�j� pris.").activate();
        }
      
        else {
          //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            donnees_.baisserNiveauSecurite();

          // r�cup�ration de la donn�e du cercle d evitage
          // FUDAA

          final SiporCercle nouveau = new SiporCercle(donnees_.getCategoriesNavires_().getListeNavires_().size());
          nouveau.setNom_(nom_.getText());
          // nouveau.diametre_=Double.parseDouble(diametre_.getText());

          if (!MODE_MODIFICATION_ON_) {
            /**
             * mode ajout
             */

            donnees_.getListeCercle_().ajout(nouveau);

            /**
             * Regles durees de parcours ajout d une ligne
             */
            donnees_.getReglesDureesParcoursCercle_().ajoutLigne(donnees_.getCategoriesNavires_().getListeNavires_().size());

            
            //-- add element in network --//
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_CERCLE,
            		nouveau);
            
            // 2)sauvegarde des donnees
            donnees_.enregistrer();
          } else {
            /**
             * MODE MODIFICATION
             */
            SiporCercle cercleTomodify= donnees_.getListeCercle_().retournerCercle(CERCLE_A_MODIFIER_);
            nouveau.setReglesNavigation_(cercleTomodify.getReglesNavigation_());
            //-- Genes 2011 --//
            nouveau.setRegleGenes(cercleTomodify.getRegleGenes());
            nouveau.setGareAmont_(cercleTomodify.getGareAmont_());
            nouveau.setGareAval_(cercleTomodify.getGareAval_());

            donnees_.getListeCercle_().modification(CERCLE_A_MODIFIER_, nouveau);

          }

          // 2)mise a jour dans le tableau
          MENUCERCLES_.affichagePanel_.maj(donnees_);

          // 3)remise a zero des composants

          nom_.setText("Cercle " + (donnees_.getListeCercle_().getListeCercles_().size() + 1));

          // 4)mode modification remis a false par defaut
          MODE_MODIFICATION_ON_ = false;
          MENUCERCLES_.mode_.setText("Saisie: ");
          validation_.setText("Valider");
          MENUCERCLES_.suppression_.setEnabled(true);

        }

      }

    });

    /**
     * Affichage des composants
     */

    // architecture des composants
    final JPanel panneau = new JPanel();

    panneau.add(new JLabel("Nom du cercle d'�vitage: "));
    panneau.add(this.nom_);

    panneau.setBorder(SiporBordures.bordnormal_);
    this.add(panneau, BorderLayout.CENTER);

    this.add(validation_, BorderLayout.SOUTH);

    // affichage:
    setVisible(true);
  }

  public void MODE_MODIFICATION(final int numCercle) {

    MODE_MODIFICATION_ON_ = true;
    this.nom_.setText((this.donnees_.getListeCercle_().retournerCercle(numCercle)).getNom_());
    CERCLE_A_MODIFIER_ = numCercle;
    this.MENUCERCLES_.mode_.setText("Modification: ");
    this.validation_.setText("modifier");
    this.MENUCERCLES_.suppression_.setEnabled(false);
  }

}
