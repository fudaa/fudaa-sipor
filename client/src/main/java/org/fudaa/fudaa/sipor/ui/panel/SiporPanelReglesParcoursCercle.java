/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleDureeParcoursCercles;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporCellEditorDureeParcours;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Panel qui g�re la saisie des dur�es de parcours de navires genes dans les cercles d'�vitage.
 * @author Adrien Hadoux
 */
public class SiporPanelReglesParcoursCercle extends SiporInternalFrame {

  /**
   * Tableau contenant les donn�es du tableau affich� en java (definition attribut car sert pour transformation excel)
   */
  Object[][] ndata;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes_;

  /**
   * Tableau de type JTable qui contiendra les donnees des navires
   */

  BuTable tableau_;

  /**
   * Modele qui contient la partie metier des donn�es.
   * Utilisee comme mod�le du tableau
   */
  SiporModeleDureeParcoursCercles modeleTableau_;
  
  

  /**
   * Bouton de validation des regles de navigations.
   */

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  
  /**
   * Panel qui contiendra le tableau.
   */
  JPanel global_ = new JPanel();

  /**
   * panel de controle
   */
  JPanel controlPanel = new JPanel();
  /**
   * Bordure du tableau.
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Indice du cercle choisi (par defaut 0 => le premier cercle.
   */
  int cercleChoisi_ = 0;

  /**
   * constructeur du panel d'affichage des bassins.
   * 
   * @param d donn�es de la simulation.
   */
  public SiporPanelReglesParcoursCercle(final SiporDataSimulation _donnees) {

    super("", true, true, true, true);

    donnees_ = _donnees;

    //-- Creation du modele du tableau --//
    modeleTableau_=new SiporModeleDureeParcoursCercles(donnees_);
    
    
    global_.setLayout(new BorderLayout());

    /**
     * Initialisation du nombre de cercles.
     */

    this.affichage();

    /**
     * Creation de la fenetre
     */

    setTitle("Dur�e de parcours au sein des cercles d'�vitage (en Heures.Minutes) ");
    setSize(800, 400);
    setBorder(SiporBordures.compound_);
    getContentPane().setLayout(new BorderLayout());
    
   // final JScrollPane ascenceur = new JScrollPane(global_);

    getContentPane().add(global_, BorderLayout.CENTER);

    controlPanel.setBorder(this.borduretab_);
    

    controlPanel.add(this.impression_);
    controlPanel.add(validation_);

    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    /**
     * Listener du comboChoix de cercle: lors dde la selection du cercle, on affiche (rafraichis) le tableau de choix
     * des regles de navigations pour ce cercle
     */
    this.impression_
        .setToolTipText("G�n�re un fichier Excel du tableau. Attention, ce bouton ne g�n�re que le tableau du cercle affich�.");

    

    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        tableau_.editCellAt(0, 0);
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(SiporPanelReglesParcoursCercle.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = modeleTableau_;

         

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);
            donnees_.getApplication();
            new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, 
        "Fichier Excel g�n�r� avec succ�s.").activate();
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });

    // listener du bouton de validation

    this.validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        dispose();

      }

    });

    // affichage de la frame
    setVisible(true);

  }

  

  /**
   * Methode d affichage des composants du BuTable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

  //--creation du tableau avec l'obejt modele qui g�re la liaison entre utilisateur et partie m�tier --//
  tableau_ = new BuTable(new SiporModeleDureeParcoursCercles(donnees_));
    
    //-- editeur par d�faut du tableau --// 
    tableau_.setDefaultEditor(Object.class,new SiporCellEditorDureeParcours());
  
    
    
    this.global_.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    this.global_.revalidate();
    this.global_.updateUI();

    this.revalidate();
    this.updateUI();

  }

  

}
