/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license GNU General Public Licence 2
 *@copyright (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeDureesParcours;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatsDureesParcours extends SiporInternalFrame /* implements ActionListener */{

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  /** Comboliste de selection de l element de d�part */
  String[] listeElt = { "Chenal", "Cercle", "Ecluse", "Quai" };
  JComboBox ListeTypesDepart_ = new JComboBox(listeElt);
  JComboBox ListeElementDepart_ = new JComboBox();
  /** Comboliste de selection de l element d'arrivee */
  JComboBox ListeTypesArrivee_ = new JComboBox(listeElt);
  JComboBox ListeElementArrivee_ = new JComboBox();
  /** horaire de d�part */
  JComboBox listeGaresDepart_ = new JComboBox();
  JComboBox listeGaresArrivee_ = new JComboBox();
  String[] listeSens = { "Entrant", "Sortant" };
  JComboBox Sens_ = new JComboBox(listeSens);
  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  JCheckBox choixNbNavires_ = new JCheckBox("Nombre navires", true);
  JCheckBox choixTotalDuree_ = new JCheckBox("Dur�e Totale", true);

  JCheckBox choixMin_ = new JCheckBox("Dur�e minimum", true);
  JCheckBox choixMoy_ = new JCheckBox("Dur�e Moyenne", true);
  JCheckBox choixMax_ = new JCheckBox("Dur�e Maximum", true);

  JCheckBox[] tableauChoixNavires_;

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Dur�e minimum", "Dur�e moyenne", "Dur�e maximum", "Nombre de navires" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  JLabel texteDuree_ = new JLabel("");
  JLabel texteDuree2_ = new JLabel("");

  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatsDureesParcours(final SiporDataSimulation _donnees) {
    super("Durees de parcours", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le chenal 0 et le quai 0 dans l horaire entre 0 et 24
    SiporAlgorithmeDureesParcours.calcul2(donnees_, 0, 0, 3, 0, 0);

    setSize(820, 600);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);

    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipalAffichage_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    panelPrincipalAffichage_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.getCategoriesNavires_().getListeNavires_().size()];
    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom(),
          true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }
    for (int i = 0; i < this.donnees_.getListeGare_().getListeGares_().size(); i++) {
      this.listeGaresDepart_.addItem((String) this.donnees_.getListeGare_().retournerGare(i));
      this.listeGaresArrivee_.addItem((String) this.donnees_.getListeGare_().retournerGare(i));

    }
    this.listeGaresDepart_.addActionListener(this);
    this.listeGaresArrivee_.addActionListener(this);

    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(2, 1));

    selectionPanel1 = new BuPanel();
    selectionPanel1.add(new JLabel("Dur�es de parcours"));
    texteDuree_.setText("entre l'entr�e de l'�l�ment");
    selectionPanel1.add(texteDuree_);
    selectionPanel1.add(this.ListeTypesDepart_);
    selectionPanel1.add(this.ListeElementDepart_);
    texteDuree2_.setText("et l'entr�e de l'�l�ment");
    selectionPanel1.add(texteDuree2_);
    selectionPanel1.add(this.ListeTypesArrivee_);
    selectionPanel1.add(this.ListeElementArrivee_);

    selectionPanel1.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel1);

    final BuPanel selectionPanel2 = new BuPanel();
    selectionPanel2.add(new JLabel("(les attentes sont comprises)"));
    selectionPanel2.add(new JLabel("Sens du parcours:"));
    selectionPanel2.add(this.Sens_);
    selectionPanel2.add(lancerRecherche_);

    selectionPanel2.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel2);

    this.selectionPanel_.setBorder(this.compound_);
    this.Sens_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if (Sens_.getSelectedIndex() == 1) {

          texteDuree_.setText("entre la sortie de l'�l�ment");
          texteDuree2_.setText("et la sortie de l'�l�ment");

        } else

        {
          texteDuree_.setText("entre l'entr�e de l'�l�ment");
          texteDuree2_.setText("et l'entr�e de l'�l�ment");

        }
      }
    });
    // listener des liste box
    final ActionListener RemplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        int selection = 0;
        if (e.getSource() == ListeTypesDepart_) {
          selection = ListeTypesDepart_.getSelectedIndex();
        } else {
          selection = ListeTypesArrivee_.getSelectedIndex();
        }
        // "quai","ecluse","chenal","cercle","bassin"
        switch (selection) {

        case 3:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 2:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 0:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 1:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        }

      }
    };
    this.ListeTypesDepart_.addActionListener(RemplissageElement);
    this.ListeTypesArrivee_.addActionListener(RemplissageElement);
    this.ListeElementDepart_.addActionListener(this);
    this.ListeElementArrivee_.addActionListener(this);

    this.ListeTypesDepart_.setSelectedIndex(0);
    this.ListeTypesArrivee_.setSelectedIndex(3);
    
    this.lancerRecherche_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/
    final JTabbedPane panoption = new JTabbedPane();
    this.optionPanel_.add(panoption);

    final Box bVert = Box.createVerticalBox();
    final JScrollPane pcnasc1 = new JScrollPane(bVert);
    panoption.addTab("Dur�es", pcnasc1);
    bVert.add(new JLabel(""));
    bVert.add(this.choixNbNavires_);
    bVert.add(this.choixMin_);
    bVert.add(this.choixMoy_);
    bVert.add(this.choixMax_);

    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Options Affichage");
    bVert.setBorder(bordure1);
    this.optionPanel_.setBorder(this.compound_);

    final Box bVert2 = Box.createVerticalBox();
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }

    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Affichage navires");
    bVert2.setBorder(bordure2);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    panoption.addTab("Navires", pcnasc);

    // listener des checkbox de choix des options d affichage

    this.choixMax_.addActionListener(this);
    this.choixMin_.addActionListener(this);
    this.choixMoy_.addActionListener(this);
    this.choixNbNavires_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(new JLabel("S�lectionnez la cat�gorie � visualiser:   "));
    this.controlPanel_.add(this.ListeNavires_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatsDureesParcours.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatsDureesParcours.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatsDureesParcours.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // affichage du tableau

    // operation magique qui permet de determiner ce tableau

    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.getCategoriesNavires_().getListeNavires_().size()][6];
    int indiceNavire = 0;
    if (val < 0) {

      for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
        if (this.tableauChoixNavires_[i].isSelected()) {
          data[indiceNavire][0] = this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
          if (this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale != 0) {
            // ecriture des donn�es calcul�es pour les dur�es de parcours
            // si les cases correspondantes ont �t� coch�es:
            int indiceColonne = 1;
            if (this.choixMin_.isSelected()) {
              data[indiceNavire][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMinimumParcours);
            }
            if (this.choixMoy_.isSelected()) {
              data[indiceNavire][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale / this.donnees_
                          .getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].nombreNavires));
            }
            if (this.choixMin_.isSelected()) {
              data[indiceNavire][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMaximumParcours);
            }
            if (this.choixNbNavires_.isSelected()) {
              data[indiceNavire][indiceColonne++] = ""
                  + FonctionsSimu
                      .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].nombreNavires);
            }
            if (this.choixTotalDuree_.isSelected()) {
              data[indiceNavire][indiceColonne++] = ""
                  + SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale);
            }
          }
          indiceNavire++;
        }// fin du if: si le navire est selectionn� par l utilisateur:
      }

    } else if (val < this.donnees_.getCategoriesNavires_().getListeNavires_().size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data = new Object[1][6];
      data[0][0] = this.donnees_.getCategoriesNavires_().retournerNavire(val).getNom();
      // on complete les don�nes par le tableau de resultats:
      // data[0][0]..........
      int indiceColonne = 1;
      if (this.choixMin_.isSelected()) {
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[val].dureeMinimumParcours);
      }
      if (this.choixMoy_.isSelected()) {
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[val].dureeParcoursTotale / this.donnees_
                    .getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[val].nombreNavires));
      }
      if (this.choixMin_.isSelected()) {
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[val].dureeMaximumParcours);
      }
      if (this.choixNbNavires_.isSelected()) {
        data[0][indiceColonne++] = ""
            + FonctionsSimu
                .diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[val].nombreNavires);
      }
      if (this.choixTotalDuree_.isSelected()) {
        data[0][indiceColonne++] = ""
            + SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes2((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[val].dureeParcoursTotale);
      }

    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);
    // this.panelTableau_.add(this.controlPanel_,BorderLayout.SOUTH);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    if (this.ListeElementDepart_.getSelectedIndex() == this.ListeElementArrivee_.getSelectedIndex()
        && this.ListeTypesDepart_.getSelectedIndex() == this.ListeTypesArrivee_.getSelectedIndex()) {
      g += "  titre \" temps de s�jour � partir de l'�l�ment " + (String) this.ListeElementDepart_.getSelectedItem()
          + " \"\n";
    } else {
      g += "  titre \" Dur�es de Parcours de " + (String) this.ListeElementDepart_.getSelectedItem() + " vers "
          + (String) this.ListeElementArrivee_.getSelectedItem() + " dans le sens "
          + (String) this.Sens_.getSelectedItem() + " \"\n";
    }
    g += "  sous-titre \" Dur�es de parcours \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
  

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" dur�es" + "\"\n";
    g += "    unite \"" + " H.Min" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeDureesParcours
            .determineDureeMaxi(donnees_))
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    if (this.choixMax_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es maxi"
      + "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur   BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB0000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie

              + " "
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes(this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMaximumParcours)// tabGen[n]
              + "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
              + "\n";
          indiceNavire++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
    if (this.choixMoy_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es moyennes"
      + "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB8800 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
    
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " "
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeParcoursTotale / this.donnees_
                      .getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].nombreNavires));
          if (!this.choixMax_.isSelected()) {
            g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n";
          }
          g += "\n";
          indiceNavire++;
        }

      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    }
    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
    if (this.choixMin_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es minimum"
      + "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BBCC00 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;

      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " "
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMinimumParcours);
          if (!this.choixMax_.isSelected() && !choixMoy_.isSelected()) {
            g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n";
          }
          g += "\n";
          indiceNavire++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    /**
     * declaration d'un seuil
     */
    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";

    if (this.ListeElementDepart_.getSelectedIndex() == this.ListeElementArrivee_.getSelectedIndex()
        && this.ListeTypesDepart_.getSelectedIndex() == this.ListeTypesArrivee_.getSelectedIndex()) {
      g += "  titre \" temps de s�jour � partir de l'�l�ment " + (String) this.ListeElementDepart_.getSelectedItem()
          + " \"\n";
    } else {
      g += "  titre \" Dur�es de Parcours de " + (String) this.ListeElementDepart_.getSelectedItem() + " vers "
          + (String) this.ListeElementArrivee_.getSelectedItem() + " dans le sens "
          + (String) this.Sens_.getSelectedItem() + " \"\n";
    }

    g += "  sous-titre \" Dur�es de parcours \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
  

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" dur�es" + "\"\n";
    g += "    unite \"" + " H.Min" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeDureesParcours
            .determineDureeMaxi(donnees_))
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    if (this.choixMax_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es maxi"
      + "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur   BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " "
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes(this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMaximumParcours)// tabGen[n]
              + "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
              + "\n";
          indiceNavire++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
    if (this.choixMoy_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es moyennes"
      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      + "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " "
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeParcoursTotale / this.donnees_
                      .getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].nombreNavires));
          if (!this.choixMax_.isSelected()) {
            g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n";
          }
          g += "\n";
          indiceNavire++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
    if (this.choixMin_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es minimum"
      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      + "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " "
              + SiporTraduitHoraires
                  .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMinimumParcours);
          if (!this.choixMax_.isSelected() && !choixMoy_.isSelected()) {
            g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n";
          }
          g += "\n";
          indiceNavire++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    if (source == this.choixTotalDuree_ || source == this.choixNbNavires_ || source == this.choixMax_
        || source == this.choixMin_ || source == this.choixMoy_)
    {
      // clic sur un checkBox:
      // construction de la colonne des titres
      int compteurColonnes = 0;
      if (this.choixMax_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixMin_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixMoy_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
        // if(this.choixTotalDuree_.isSelected())compteurColonnes++;
      }

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;
      this.titreTableau_[indiceColonne++] = "Cat�gorie";
      if (this.choixMin_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Dur�e minimum";
      }
      if (this.choixMoy_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Dur�e moyenne";
      }
      if (this.choixMax_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Dur�e maximum";
      }
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Nombre navires";
      }

      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.lancerRecherche_) {
     
      // lancement des calculs pour les dur�es de parcours:
      SiporAlgorithmeDureesParcours.calcul2(donnees_, this.ListeTypesDepart_.getSelectedIndex(),
          this.ListeElementDepart_.getSelectedIndex(), this.ListeTypesArrivee_.getSelectedIndex(),
          this.ListeElementArrivee_.getSelectedIndex(), this.Sens_.getSelectedIndex());
      // mise a jour des affichages:
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      // }
    } else if (source == this.valideSeuil_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    } else if (source == this.ListeElementDepart_ || source == this.ListeElementArrivee_) {
      if (this.ListeElementDepart_.getSelectedIndex() == this.ListeElementArrivee_.getSelectedIndex()
          && this.ListeTypesDepart_.getSelectedIndex() == this.ListeTypesArrivee_.getSelectedIndex()) {
        texteDuree_.setForeground(Color.RED);
        texteDuree_.setText("temps de sejour:");
        texteDuree2_.setText("");
        this.ListeElementArrivee_.setEnabled(false);
        this.ListeTypesArrivee_.setEnabled(false);

        this.Sens_.setEnabled(false);
      } else {
        texteDuree_.setForeground(Color.BLACK);

        this.Sens_.setEnabled(true);
        this.Sens_.setSelectedIndex(0);
        this.ListeElementArrivee_.setEnabled(true);
        this.ListeTypesArrivee_.setEnabled(true);
      }
    }

    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
