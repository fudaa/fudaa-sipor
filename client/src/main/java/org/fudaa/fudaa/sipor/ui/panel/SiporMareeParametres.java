/*
 * @file         SiporMareeParametres.java
 * @creation     1999-10-01
 * @modification $Date: 2007-07-23 12:46:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.sipor.SParametresMaree;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.ui.tools.DureeField;
import org.fudaa.fudaa.sipor.ui.tools.TextFieldsMeters;

/**
 * Classe impl�mentant l'onglet "mar�e" pour les param�tres de sipor.
 * 
 * @version $Revision: 1.8 $ $Date: 2007-07-23 12:46:32 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class SiporMareeParametres extends BuPanel {
  // Raccourci vers la mar�e locale de l'application
  SParametresMaree maree;
  // Zones de saisie contenues dans l'onglet
  DureeField dfPeriodeVivesEaux = new DureeField(false, true, true, false);
  DureeField dfPeriodeMaree = new DureeField(false, true, true, true);
  TextFieldsMeters tfmHauteurMoyenneMer = new TextFieldsMeters();
  TextFieldsMeters tfmMoyenneNiveauHauteMerMortesEaux = new TextFieldsMeters();
  TextFieldsMeters tfmMoyenneNiveauHauteMerVivesEaux = new TextFieldsMeters();
  TextFieldsMeters tfmMoyenneNiveauBasseMerMortesEaux = new TextFieldsMeters();
  TextFieldsMeters tfmMoyenneNiveauBasseMerVivesEaux = new TextFieldsMeters();

  /** Utilise un GridBagLayout pour placer un composant avec des contraintes sp�cifiques. */
  public void placeComposant(final GridBagLayout lm, final Component composant, final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }

  /**
   * Constructeur.
   * 
   * @param appli instance de SiporImplementation.
   */
  public SiporMareeParametres(final BuCommonInterface appli) {
    super();
    final SiporImplementation implementation = (SiporImplementation) appli.getImplementation();
    // cr�ation du layout manager et des bordures
    final GridBagLayout lm = new GridBagLayout();
    final GridBagConstraints c = new GridBagConstraints();
    this.setLayout(lm);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    // Contraintes communes � tous les composants
    c.gridy = GridBagConstraints.RELATIVE;
    c.anchor = GridBagConstraints.WEST;
    c.ipadx = 5;
    c.ipady = 5;
    // Etiquettes
    c.gridx = 0;
    placeComposant(lm, new JLabel("P�riode "), c);
    placeComposant(lm, new JLabel("    - des vives  eaux : "), c);
    placeComposant(lm, new JLabel("    - de la mar�e     : "), c);
    c.insets.top = 20;
    c.insets.bottom = 20;
    placeComposant(lm, new JLabel("Hauteur moyenne de la mer : "), c);
    c.insets.top = 0;
    c.insets.bottom = 0;
    placeComposant(lm, new JLabel("Moyenne du niveau de la haute mer  "), c);
    placeComposant(lm, new JLabel("    - en p�riode des mortes eaux : "), c);
    c.insets.bottom = 20;
    placeComposant(lm, new JLabel("    - en p�riode des vives  eaux : "), c);
    c.insets.bottom = 0;
    placeComposant(lm, new JLabel("Moyenne du niveau de la basse mer"), c);
    placeComposant(lm, new JLabel("    - en p�riode des mortes eaux : "), c);
    placeComposant(lm, new JLabel("    - en p�riode des vives  eaux : "), c);
    // Zones de saisie
    c.gridx = 1;
    c.gridy = 1;
    placeComposant(lm, dfPeriodeVivesEaux, c);
    c.gridy = 2;
    placeComposant(lm, dfPeriodeMaree, c);
    c.gridy = 3;
    placeComposant(lm, tfmHauteurMoyenneMer, c);
    c.gridy = 5;
    placeComposant(lm, tfmMoyenneNiveauHauteMerMortesEaux, c);
    c.gridy = 6;
    placeComposant(lm, tfmMoyenneNiveauHauteMerVivesEaux, c);
    c.gridy = 8;
    placeComposant(lm, tfmMoyenneNiveauBasseMerMortesEaux, c);
    c.gridy = 9;
    placeComposant(lm, tfmMoyenneNiveauBasseMerVivesEaux, c);
    setMaree(implementation.outils_.getMaree());
  }

  /** Mutateur de maree avec affichage des nouvelles donn�es dans l'onglet. */
  public void setMaree(final SParametresMaree maree_) {
    maree = maree_;
    dfPeriodeVivesEaux.setDureeField(maree.periodeVivesEaux);
    dfPeriodeMaree.setDureeField(maree.periodeMaree);
    tfmHauteurMoyenneMer.setValue(maree.hauteurMoyenneMer);
    tfmMoyenneNiveauHauteMerMortesEaux.setValue(maree.moyenneNiveauHauteMerMortesEaux);
    tfmMoyenneNiveauHauteMerVivesEaux.setValue(maree.moyenneNiveauHauteMerVivesEaux);
    tfmMoyenneNiveauBasseMerMortesEaux.setValue(maree.moyenneNiveauBasseMerMortesEaux);
    tfmMoyenneNiveauBasseMerVivesEaux.setValue(maree.moyenneNiveauBasseMerVivesEaux);
  }

  /** Accesseur de maree avec enregistrement pr�alable des donn�es de l'onglet. */
  public SParametresMaree getMaree() {
    maree.periodeVivesEaux = dfPeriodeVivesEaux.getDureeFieldLong();
    maree.periodeMaree = dfPeriodeMaree.getDureeFieldLong();
    maree.hauteurMoyenneMer = tfmHauteurMoyenneMer.toDouble();
    maree.moyenneNiveauHauteMerMortesEaux = tfmMoyenneNiveauHauteMerMortesEaux.toDouble();
    maree.moyenneNiveauHauteMerVivesEaux = tfmMoyenneNiveauHauteMerVivesEaux.toDouble();
    maree.moyenneNiveauBasseMerMortesEaux = tfmMoyenneNiveauBasseMerMortesEaux.toDouble();
    maree.moyenneNiveauBasseMerVivesEaux = tfmMoyenneNiveauBasseMerVivesEaux.toDouble();
    return maree;
  }

  /**
   * Controle la validit� de "maree".
   * 
   * @param maree maree � v�rifier.
   * @return message d�crivant les erreurs ; chaine vide si maree correcte.
   */
  public static String controleMaree(final SParametresMaree maree) {
    String erreur = "";
    if (maree.moyenneNiveauBasseMerMortesEaux > maree.hauteurMoyenneMer) {
      erreur += "moyenne du niveau de la basse mer en p�riode des mortes eaux > moyenne du niveau de la mer\n";
    }
    if (maree.moyenneNiveauBasseMerVivesEaux > maree.moyenneNiveauBasseMerMortesEaux) {
      erreur += "moyenne du niveau de la basse mer en p�riode des vives eaux > ";
      erreur += "moyenne du niveau de la basse mer en p�riode des mortes eaux\n";
    }
    if (maree.moyenneNiveauHauteMerMortesEaux < maree.hauteurMoyenneMer) {
      erreur += "moyenne du niveau de la haute mer en p�riode des mortes eaux < moyenne du niveau de la mer\n";
    }
    if (maree.moyenneNiveauHauteMerVivesEaux < maree.moyenneNiveauHauteMerMortesEaux) {
      erreur += "moyenne du niveau de la haute mer en p�riode des vives eaux < ";
      erreur += "moyenne du niveau de la haute mer en p�riode des mortes eaux\n";
    }
    return erreur;
  }
}
