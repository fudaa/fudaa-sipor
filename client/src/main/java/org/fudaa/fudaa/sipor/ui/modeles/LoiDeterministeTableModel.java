package org.fudaa.fudaa.sipor.ui.modeles;

import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuDialogError;

import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

public class LoiDeterministeTableModel extends AbstractTableModel{

  
  
   /**
     * Donn�es de la loi deterministe
     */
    ArrayList loiDeterministe_ = new ArrayList();

    
    CoupleLoiDeterministe nouveauCouple=null;

  private SiporDataSimulation donnees_;
    
    
  public LoiDeterministeTableModel(ArrayList _loiDeterministe, SiporDataSimulation _donnee){
    loiDeterministe_=_loiDeterministe;
    donnees_=_donnee;
  }
  
  
  public int getColumnCount() {
    return 2;
  }

  public int getRowCount() {
    return loiDeterministe_.size()+1;
  }

  
  public String formatHeure(double value) {
		String data  = "" + value;
		if(data.contains(","))
			data = data.replace(",", ":");
		if(data.contains("."))
			data = data.replace(".", ":");
		
		return data;
		
	}
	public double unformatHeure(String data) throws ParseException {
		if(data.contains(":"))
			data = data.replace(":", ".");
		if(data.contains(","))
			data = data.replace(",", ".");
		double res = Double.parseDouble(data);
		return res;
		
	}
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    
    if(rowIndex==getRowCount()-1)
      {
      if(nouveauCouple!=null)
      {
        if(columnIndex==0 && nouveauCouple.getJour_()!=-1)
          return ""+nouveauCouple.getJour_();
          else
            if(columnIndex==1 && nouveauCouple.getTemps_()!=-1)
              return formatHeure(nouveauCouple.getTemps_());
            
            
        
      }
        return "";
      }
    
    if(columnIndex==0 )
      return ""+((CoupleLoiDeterministe) this.loiDeterministe_.get(rowIndex)).getJour_();
    
    return formatHeure((double) ((CoupleLoiDeterministe) this.loiDeterministe_.get(rowIndex)).getTemps_());
    
    
  }

  
  public String getColumnName(int column) {
    if(column==0)
      return "Jour";
    return "Horaire";
  }

  
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    // TODO Auto-generated method stub
    return true;
  }

  
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    
    double temps=-1;
    int jour=-1;
    
    try {
      if(columnIndex==0)
        jour=Integer.parseInt((String)aValue);
      else
       // temps=Double.parseDouble( (String)aValue);
      	temps = unformatHeure((String)aValue);
    
    } catch (NumberFormatException e) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                    "La saisie n'est pas un nombre correcte").activate();
            return;
    } catch (ParseException e) {
    	 new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                 "La saisie n'est pas un nombre correcte").activate();
         return;
	}
    
    
    if(columnIndex==1){
            //-- TEST HORAIRE --//
        
      if(temps>24 || temps<0){
        new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
        "L'horaire doit �tre compris entre 0 et 24h").activate();
        return;
      }
      
      //-- TEST FORMATTAGE HORAIRE --//
      String contenu=(String)aValue;

      if(contenu.lastIndexOf(".")!=-1){
        String unite=contenu.substring(contenu.lastIndexOf(".")+1, contenu.length());
        if(unite.length()>2){
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "2 chiffres apr�s la virgule maximum").activate();

          return;
        }


        float valUnite=Float.parseFloat(unite);
        if(valUnite>=60){
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Les unit�s doivent �tre inf�rieures � 60 minutes.").activate();
          return;
        }
      }
    }// fin test formattage horaire
    else
    {
      
      //-- TEST JOUR --//
      if(jour>donnees_.getParams_().donneesGenerales.nombreJours)
      {
        new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
        "Le jour doit �tre inf�rieur au nombre \n de jours apr�s la simulation").activate();
        return;
      }
      else
        if(jour<0){
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Le jour doit �tre positif.").activate();
          return;
        }
      
    }
    
    
    
    
    //-- MODE AJOUT --//
    if(rowIndex==loiDeterministe_.size())
    {
      //-- ajout de l'�l�ment --//
      if(nouveauCouple==null){
        nouveauCouple=new CoupleLoiDeterministe(-1,-1);
      }
      
      
      //-- ajout de la valeur selon la colonne --//
      if(columnIndex==0)
        nouveauCouple.setJour_(jour);
      else
        nouveauCouple.setTemps_(temps);
      
      //-- test si le nouveau couple est complet, on l'ajoute � la liste des couples --//
      if(nouveauCouple.getJour_()!=-1 && nouveauCouple.getTemps_()!=-1){
        loiDeterministe_.add(new CoupleLoiDeterministe(nouveauCouple.getJour_(), nouveauCouple.getTemps_()));
        //reinitialisation du couple
        nouveauCouple=null;
        //la structure du butable doit changer: il faut le faire recalculer car il a une ligne de plus
        this.fireTableStructureChanged();
      }
      
    }
    else
    {
      //-- MODE MODIFICATION --//
      CoupleLoiDeterministe coupleAmodifier= (CoupleLoiDeterministe) this.loiDeterministe_.get(rowIndex);
          
      if(columnIndex==0)
        coupleAmodifier.setJour_(jour);
      else
        coupleAmodifier.setTemps_(temps);
        
      //mise a joru du contenu de la cellule
      this.fireTableCellUpdated(rowIndex, columnIndex);
      
    }
    
    
  }


  public ArrayList getLoiDeterministe_() {
    return loiDeterministe_;
  }


  public void setLoiDeterministe_(ArrayList loiDeterministe_) {
    this.loiDeterministe_ = loiDeterministe_;
  }


  public CoupleLoiDeterministe getNouveauCouple() {
    return nouveauCouple;
  }


  public void setNouveauCouple(CoupleLoiDeterministe nouveauCouple) {
    this.nouveauCouple = nouveauCouple;
  }


  public SiporDataSimulation getDonnees_() {
    return donnees_;
  }


  public void setDonnees_(SiporDataSimulation donnees_) {
    this.donnees_ = donnees_;
  }
  
  

}
