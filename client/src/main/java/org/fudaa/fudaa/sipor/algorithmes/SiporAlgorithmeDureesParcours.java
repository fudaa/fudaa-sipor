/**
 *@creation 16 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sipor.SParametresResultatsDureParcoursCategories;
import org.fudaa.dodico.corba.sipor.SParametresResultatsDureeParcoursNavires;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * @version $Version$
 * @author hadoux
 */
public class SiporAlgorithmeDureesParcours {

  /** Methode qui realise les calculs pour les g�n�rations de bateaux */
  public static void calcul(final SiporDataSimulation donnees_, final int typeElement1, final int indiceElement1,
      final int typeElement2, final int indiceElement2, final int sens/* ,float horaireDep, float horaireFin */) {

    /**
     * SENS 0 => ENTRANT 1 => SORTANT
     */
    /**
     * ATTENTION WARNING: CAS OU L ON A LA MEME element en entree qu en sortie: on comptabilise le temps d entree et LE
     * TEMPS DE SORTIE DE LA PROCHAINE OCCURENCE DE L ELEMENT DANS LA LISTE DU TRAJET ON MET UN ELSE LORS DE LA
     * SELECTION DU NAVIRE POUR LA SELECTRION SOIT DU TEMPS DE DEPART DE LA GARE OU LE TEMSP DE SORTIE DE LA GARE CAR
     * SINON IL VA PRENDRE LE DEPART ET LA SORTIE DU MEME EVNEMENT!!!!!! LE FAIT DE METTRE UN ELSE PERMET DE PRENDRE LE
     * TEMPS DE DEPART A LA GARE 1 ET PLUS LOIN DE PRENDRE L ARRIVEE A LA GARE 1; EN EFFET LES DONNES DE LA SIMU SONT
     * ORGANISE EN ORDRE EVENEMENTIEL CROISSANT.
     */

    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // etape 1: remplissage du tableau resultats.DureesParcoursNavires[]
    // qui donne les dur�es d attente de tous les navires ainsi que leur cat�gorie associ�e.
    resultats.DureeParcoursNavire = new SParametresResultatsDureeParcoursNavires[donnees_.getListeResultatsSimu_().nombreNavires];

    for (int i = 0; i < donnees_.getListeResultatsSimu_().nombreNavires; i++) {
      // allocation memoire des donn�es
      resultats.DureeParcoursNavire[i] = new SParametresResultatsDureeParcoursNavires();
      // initialisation des dur�es des heures entrees des elements dans les 2 sens
      resultats.DureeParcoursNavire[i].heure1Entree1Navire = -1;
      resultats.DureeParcoursNavire[i].heure2Entree1Navire = -1;
      resultats.DureeParcoursNavire[i].heure1Entree2Navire = -1;
      resultats.DureeParcoursNavire[i].heure2Entree2Navire = -1;
      resultats.DureeParcoursNavire[i].categorieAssociee = donnees_.getListeResultatsSimu_().listeEvenements[i].categorie;

      int QUAI=-1;
      boolean trouve=false;
      //-- calcul de l'indice du quai
      for (int k = 0; k < donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours && !trouve; k++)
        if(donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement==3){
          QUAI=k;
          trouve=true;
        }
      
      
      for (int k = 0; k < donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours; k++) {
        if (donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement1
            && donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == typeElement1) {
          // si l'�l�ment du trajet est equivalent a celui entr� en parametre 1
          if (k<=QUAI/*resultats.DureeParcoursNavire[i].heure1Entree1Navire == -1*/) {
            // on rencontre l'element 1 pour la premiere fois
            // donc a l'all�e du navire
            resultats.DureeParcoursNavire[i].heure1Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureEntree;

            // cas particulier: l'elelemnt est un quai: la deuxieme heure de apssage correspond alors � l'horaire de
            // sortie du quai:
            if (typeElement1 == 3) {
              resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;

            }
          } else {
            // on rencontre l'�l�ment pour la deuxieme fois
            // donc au retour
            // cas particulier; si l'utilisateur a choisi le meme element: on calcule le temps de sejour donc de l
            // entree vers la sortie du meme element
            if (indiceElement1 == indiceElement2 && typeElement1 == typeElement2) {
              resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;
            } else {
              resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;
            }

          }
        }// fin du if si l'�l�ment du trajet est egal a celui entree en parametre 1 de l utilisateur
        else if (donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement2
            && donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == typeElement2) {
          // si l'�l�ment du trajet est equivalent a celui entr� en parametre 2
          if (k<=QUAI/*resultats.DureeParcoursNavire[i].heure1Entree2Navire == -1*/) {
            // on rencontre l'element a l all�e
            // donc a l'all�e du navire
            resultats.DureeParcoursNavire[i].heure1Entree2Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureEntree;
            // cas particulier: l'elelemnt est un quai: la deuxieme heure de apssage correspond alors � l'horaire de
            // sortie du quai:
            if (typeElement2 == 3) {
              resultats.DureeParcoursNavire[i].heure2Entree2Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;

            }
          } else {
            // on rencontre l'�l�ment pour la deuxieme fois
            // donc au retour
            resultats.DureeParcoursNavire[i].heure2Entree2Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;

          }

        }// fin du if si l'�l�ment du trajet est egal a celui entree en parametre 2 de l utilisateur
      }
    }// fin de l algorithme 1

    // etape 2: remplissage du tableau de cat�gories de navires avec els donn�es suivantes:
    // dur�e de parcour total de tous les navire de la cat�gorie
    // nombre de navires de la cat�gorie
    // dur�e minimum parcour
    // dur�e maximum du parcour:
    // utilisation de la structure DureeParcoursCat�gorie:
    resultats.DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[donnees_.getCategoriesNavires_().getListeNavires_()
        .size()];
    float duree = 0;
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      // allocation memoire
      resultats.DureeParcoursCategorie[i] = new SParametresResultatsDureParcoursCategories();
      // initialisation des donn�es des dur�es de parcours pour les cat�gories
      resultats.DureeParcoursCategorie[i].dureeMaximumParcours = 0;
      resultats.DureeParcoursCategorie[i].dureeMinimumParcours = 9999999;
      resultats.DureeParcoursCategorie[i].dureeParcoursTotale = 0;
      resultats.DureeParcoursCategorie[i].nombreNavires = 0;

      for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
        if (resultats.DureeParcoursNavire[k].categorieAssociee == i
            ) {
          // si la cat�gorie du navire qui contient les dur�es de parcours est �quivalente a la cat�gorie s�lectionn�e
          duree = 0;
          // exploitation des horaires trouv�s en fonction du sens demand�

          // cas particulier : si on choisit le meme element: on calcule le temps de sejour donc de l'entree vers la
          // sortie
          if (indiceElement1 == indiceElement2 && typeElement1 == typeElement2) {
            resultats.DureeParcoursNavire[k].heure1Entree2Navire = 0;
            duree = Math
                .abs((float) (resultats.DureeParcoursNavire[k].heure2Entree1Navire - resultats.DureeParcoursNavire[k].heure1Entree1Navire));
          } else {
            // cas normal: type ou element differents
            if (sens == 0 && resultats.DureeParcoursNavire[k].heure1Entree2Navire!=-1 && resultats.DureeParcoursNavire[k].heure1Entree1Navire!=-1) {
              // sens entrant:
              // on prend els horaires entrees 1 des element 1 et 2
              duree = Math
                  .abs((float) (resultats.DureeParcoursNavire[k].heure1Entree2Navire - resultats.DureeParcoursNavire[k].heure1Entree1Navire));
            } else
             if(sens==1 && resultats.DureeParcoursNavire[k].heure2Entree1Navire!=-1 && resultats.DureeParcoursNavire[k].heure2Entree2Navire!=-1){
              // sens sortant
              // on prend les horaires entrees 2 des elements 1 et 2
              duree = Math
                  .abs((float) (resultats.DureeParcoursNavire[k].heure2Entree1Navire - resultats.DureeParcoursNavire[k].heure2Entree2Navire));

            }

          }

          // si le navire en passe pas par la, on en le prends pas en compte
          if ((resultats.DureeParcoursNavire[k].heure1Entree1Navire != -1 && resultats.DureeParcoursNavire[k].heure1Entree2Navire != -1)
              ||
              (resultats.DureeParcoursNavire[k].heure2Entree1Navire != -1 && resultats.DureeParcoursNavire[k].heure2Entree2Navire != -1)
              ) {
            // exploitation de la donnee duree recuperee pour la categorie
          if(sens==0 && resultats.DureeParcoursNavire[k].heure1Entree1Navire != -1
              && resultats.DureeParcoursNavire[k].heure1Entree2Navire != -1)  
            
            resultats.DureeParcoursCategorie[i].nombreNavires++;
          else
            if(sens==1 && resultats.DureeParcoursNavire[k].heure2Entree1Navire != -1
                        && resultats.DureeParcoursNavire[k].heure2Entree2Navire != -1)  
                      resultats.DureeParcoursCategorie[i].nombreNavires++;
            
          
          resultats.DureeParcoursCategorie[i].dureeParcoursTotale += duree;
           
            
            if (resultats.DureeParcoursCategorie[i].dureeMaximumParcours < duree) {
              resultats.DureeParcoursCategorie[i].dureeMaximumParcours = duree;
            }

            if (resultats.DureeParcoursCategorie[i].dureeMinimumParcours > duree) {
              resultats.DureeParcoursCategorie[i].dureeMinimumParcours = duree;
            }

          }
        
            
        }// fni du si la cat�gorie concorde

      }// fin du for
      if (resultats.DureeParcoursCategorie[i].dureeMinimumParcours == 9999999) {
        resultats.DureeParcoursCategorie[i].dureeMinimumParcours = 0;
      }
    }

  }

  
  
  
  
  public static void Nouveaucalcul(final SiporDataSimulation donnees_, final int typeElement1, final int indiceElement1,
        final int typeElement2, final int indiceElement2, final int sens/* ,float horaireDep, float horaireFin */) {

      /**
       * SENS 0 => ENTRANT 1 => SORTANT
       */
      /**
       * ATTENTION WARNING: CAS OU L ON A LA MEME element en entree qu en sortie: on comptabilise le temps d entree et LE
       * TEMPS DE SORTIE DE LA PROCHAINE OCCURENCE DE L ELEMENT DANS LA LISTE DU TRAJET ON MET UN ELSE LORS DE LA
       * SELECTION DU NAVIRE POUR LA SELECTRION SOIT DU TEMPS DE DEPART DE LA GARE OU LE TEMSP DE SORTIE DE LA GARE CAR
       * SINON IL VA PRENDRE LE DEPART ET LA SORTIE DU MEME EVNEMENT!!!!!! LE FAIT DE METTRE UN ELSE PERMET DE PRENDRE LE
       * TEMPS DE DEPART A LA GARE 1 ET PLUS LOIN DE PRENDRE L ARRIVEE A LA GARE 1; EN EFFET LES DONNES DE LA SIMU SONT
       * ORGANISE EN ORDRE EVENEMENTIEL CROISSANT.
       */

      final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

      // etape 1: remplissage du tableau resultats.DureesParcoursNavires[]
      // qui donne les dur�es d attente de tous les navires ainsi que leur cat�gorie associ�e.
      resultats.DureeParcoursNavire = new SParametresResultatsDureeParcoursNavires[donnees_.getListeResultatsSimu_().nombreNavires];

      for (int i = 0; i < donnees_.getListeResultatsSimu_().nombreNavires; i++) {
        
        for (int k = 0; k < donnees_.getListeResultatsSimu_().listeEvenements[i].NbElemtnsParcours; k++) {
          if (donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement1
              && donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == typeElement1) {
            // si l'�l�ment du trajet est equivalent a celui entr� en parametre 1
            if (resultats.DureeParcoursNavire[i].heure1Entree1Navire == -1) {
              // on rencontre l'element 1 pour la premiere fois
              // donc a l'all�e du navire
              resultats.DureeParcoursNavire[i].heure1Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureEntree;

              // cas particulier: l'elelemnt est un quai: la deuxieme heure de apssage correspond alors � l'horaire de
              // sortie du quai:
              if (typeElement1 == 3) {
                resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;

              }
            } else {
              // on rencontre l'�l�ment pour la deuxieme fois
              // donc au retour
              // cas particulier; si l'utilisateur a choisi le meme element: on calcule le temps de sejour donc de l
              // entree vers la sortie du meme element
              if (indiceElement1 == indiceElement2 && typeElement1 == typeElement2) {
                resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;
              } else {
                resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;
              }

            }
          }// fin du if si l'�l�ment du trajet est egal a celui entree en parametre 1 de l utilisateur
          else if (donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement2
              && donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].typeElement == typeElement2) {
            // si l'�l�ment du trajet est equivalent a celui entr� en parametre 2
            if (resultats.DureeParcoursNavire[i].heure1Entree2Navire == -1) {
              // on rencontre l'element 1 pour la premiere fois
              // donc a l'all�e du navire
              resultats.DureeParcoursNavire[i].heure1Entree2Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureEntree;
              // cas particulier: l'elelemnt est un quai: la deuxieme heure de apssage correspond alors � l'horaire de
              // sortie du quai:
              if (typeElement2 == 3) {
                resultats.DureeParcoursNavire[i].heure2Entree2Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;

              }
            } else {
              // on rencontre l'�l�ment pour la deuxieme fois
              // donc au retour
              resultats.DureeParcoursNavire[i].heure2Entree2Navire = donnees_.getListeResultatsSimu_().listeEvenements[i].tableauTrajet[k].heureSortie;

            }

          }// fin du if si l'�l�ment du trajet est egal a celui entree en parametre 2 de l utilisateur
        }
      }// fin de l algorithme 1

      resultats.DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[donnees_.getCategoriesNavires_().getListeNavires_()
          .size()];
      float duree = 0;
      for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
        // allocation memoire
        resultats.DureeParcoursCategorie[i] = new SParametresResultatsDureParcoursCategories();
        // initialisation des donn�es des dur�es de parcours pour les cat�gories
        resultats.DureeParcoursCategorie[i].dureeMaximumParcours = 0;
        resultats.DureeParcoursCategorie[i].dureeMinimumParcours = 9999999;
        resultats.DureeParcoursCategorie[i].dureeParcoursTotale = 0;
        resultats.DureeParcoursCategorie[i].nombreNavires = 0;

        for (int k = 0; k < donnees_.getListeResultatsSimu_().nombreNavires; k++) {
          if (resultats.DureeParcoursNavire[k].categorieAssociee == i) {
            // si la cat�gorie du navire qui contient les dur�es de parcours est �quivalente a la cat�gorie s�lectionn�e
            duree = 0;
            // exploitation des horaires trouv�s en fonction du sens demand�

            // cas particulier : si on choisit le meme element: on calcule le temps de sejour donc de l'entree vers la
            // sortie
            if (indiceElement1 == indiceElement2 && typeElement1 == typeElement2) {
              resultats.DureeParcoursNavire[k].heure1Entree2Navire = 0;
              duree = Math
                  .abs((float) (resultats.DureeParcoursNavire[k].heure2Entree1Navire - resultats.DureeParcoursNavire[k].heure1Entree1Navire));
            } else {
              // cas normal: type ou element differents
              if (sens == 0) {
                // sens entrant:
                // on prend els horaires entrees 1 des element 1 et 2
                duree = Math
                    .abs((float) (resultats.DureeParcoursNavire[k].heure1Entree2Navire - resultats.DureeParcoursNavire[k].heure1Entree1Navire));
              } else {
                // sens sortant
                // on prend les horaires entrees 2 des elements 1 et 2
                duree = Math
                    .abs((float) (resultats.DureeParcoursNavire[k].heure2Entree1Navire - resultats.DureeParcoursNavire[k].heure2Entree2Navire));

              }

            }

            // si le navire en passe pas par la, on en le prends pas en compte
            if ((resultats.DureeParcoursNavire[k].heure1Entree1Navire != -1 && resultats.DureeParcoursNavire[k].heure1Entree2Navire != -1)
                ||
                (resultats.DureeParcoursNavire[k].heure2Entree1Navire != -1 && resultats.DureeParcoursNavire[k].heure2Entree2Navire != -1)
                ) {
              // exploitation de la donnee duree recuperee pour la categorie
            if(sens==0 && resultats.DureeParcoursNavire[k].heure1Entree1Navire != -1
                && resultats.DureeParcoursNavire[k].heure1Entree2Navire != -1)  
              
              resultats.DureeParcoursCategorie[i].nombreNavires++;
            else
              if(sens==1 && resultats.DureeParcoursNavire[k].heure2Entree1Navire != -1
                          && resultats.DureeParcoursNavire[k].heure2Entree2Navire != -1)  
                        resultats.DureeParcoursCategorie[i].nombreNavires++;
              
            
            resultats.DureeParcoursCategorie[i].dureeParcoursTotale += duree;
             
              
              if (resultats.DureeParcoursCategorie[i].dureeMaximumParcours < duree) {
                resultats.DureeParcoursCategorie[i].dureeMaximumParcours = duree;
              }

              if (resultats.DureeParcoursCategorie[i].dureeMinimumParcours > duree) {
                resultats.DureeParcoursCategorie[i].dureeMinimumParcours = duree;
              }

            }
          
              
          }// fni du si la cat�gorie concorde

        }// fin du for
        if (resultats.DureeParcoursCategorie[i].dureeMinimumParcours == 9999999) {
          resultats.DureeParcoursCategorie[i].dureeMinimumParcours = 0;
        }
      }

    }

    
    
    
    
  
  /**
   * Methode qui lit la structure ayant deja calcul� l ensemble des dur�es de parcours. Cette methode permet d'eviter de
   * relire le fichier historique
   */

  public static void calcul2(final SiporDataSimulation donnees_, final int typeElement1, final int indiceElement1,
      final int typeElement2, final int indiceElement2, final int sens/* ,float horaireDep, float horaireFin */) {
    {
      for (int i = 0; i < donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs.length; i++) {
        if (donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i] == null) {
          return;
        }
        if (typeElement1 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement1
            && typeElement2 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement2
            && indiceElement1 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element1
            && indiceElement2 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element2
            && sens == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].sens) {
          // on recupere la structure voulue
          donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie = donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].DureeParcoursCategorie;
          return;
        } else if (typeElement1 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement2
            && typeElement2 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement1
            && indiceElement1 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element2
            && indiceElement2 == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element1
            && sens == donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].sens) {
          // on recupere la structure voulue
          donnees_.getParams_().ResultatsCompletsSimulation.DureeParcoursCategorie = donnees_.getParams_().ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].DureeParcoursCategorie;
          return;
        }
      }
    }

  }

  /**
   * methode qui permet de deduire la dur�e maxi afin de pouvior fixer la limite max du graphe ainsi que de l
   * histogramme
   * 
   * @return
   */

  public static float determineDureeMaxi(final SiporDataSimulation donnees_) {
    double max = 0;
    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // recherche du maxdans un tableau:
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      if (resultats.DureeParcoursCategorie[i].dureeMaximumParcours > max) {
        max = resultats.DureeParcoursCategorie[i].dureeMaximumParcours;
      }

    }

    return (float) (max + 0.25 * max);
  }

}
