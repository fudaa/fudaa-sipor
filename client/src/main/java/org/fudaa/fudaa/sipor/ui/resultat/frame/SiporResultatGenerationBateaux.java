/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeGenerationBateaux;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.fudaa.fudaa.sipor.ui.tools.SiporJFreeChartCamembert;
import org.jfree.chart.ChartPanel;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatGenerationBateaux extends SiporInternalFrame {

  /**
   * Parametre resultat pour cetet interface: il s'agit d'un tableau d'entier: ce tableau contient N cases avec N le
   * nombre de cat�gories de navires et la case i du tableau corresponda au nombre de navires g�n�r�s pour la cat�gorie
   * i
   */
  int[] tabGen;

  /**
   * tableau de checkBox destiner a l utilisateuir pour choisr les navire a visualiser sur les diff�rents supports.
   */
  JCheckBox[] tableauChoixNavires_;

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  
  /**
   * Panel qui affiche le camembert
   */
  ChartPanel camembert_;
  
  
  
  /**
   * panel qui affiche l'histogramme 3d
   */
  ChartPanel histo3d_;
  
  
  String titreTableau_[] = { "Cat�gorie", "Nombre de navires" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatGenerationBateaux(final SiporDataSimulation _donnees) {
    super("Generation Navires", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // lancement du calcul des donn�es

    setSize(700, 500);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    this.getContentPane().add(this.optionPanel_, BorderLayout.WEST);
    
    
    
    tableauChoixNavires_ = new JCheckBox[this.donnees_.getCategoriesNavires_().getListeNavires_().size()];
    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom(), true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }
    
    
    
    //-- creation des camemberts --//
    camembert_= creerCamembert();
    
    
    //-- creatiion de l'histo 3d --//
    histo3d_=creerHisto3d();
    // ajout du tableau dans le panel tabbed
    panelPrincipal_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    panelPrincipal_.addTab("Camembert", FudaaResource.FUDAA.getIcon("crystal_graphe"), camembert_);
    
    
    
    
    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipal_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    panelPrincipal_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);


    
    
    
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(new JLabel("S�lectionnez la cat�gorie � visualiser:   "));
    this.controlPanel_.add(this.ListeNavires_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer.");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatGenerationBateaux.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    final JTabbedPane panoption = new JTabbedPane();
    this.optionPanel_.add(panoption);

    final Box bVert2 = Box.createVerticalBox();
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "navires");
    bVert2.setBorder(bordure1);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    panoption.addTab("Navires", pcnasc);

    this.optionPanel_.setBorder(this.compound_);

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatGenerationBateaux.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatGenerationBateaux.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // affichage du tableau

    /*******************************************************************************************************************
     * HEP HEP!!! C EST ICI QUE L ON EFFECTUE LE CALCUL QUI RETOURNE LE TABLEAU * D'ENTIER DONT CHAQUE CASE I EST LE
     * NOMBRE DE NAVIRES GENERES POUR LA CATEGORIE I * CALCUL EFFECTUE UNIQUEMENT AVEC ELS DONNEEES EN SORTIE DE LA
     * SIMULATION * ******************************************************************************************
     */
    // allocation memoire du tableau de n cases
    this.tabGen = new int[this.donnees_.getCategoriesNavires_().getListeNavires_().size()];

    // operation magique qui permet de determiner ce tableau

    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.getCategoriesNavires_().getListeNavires_().size() + 1][2];

    if (val < 0) {
      int compteurNavires = 0;
      int indiceNav = 0;
      for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
        if (this.tableauChoixNavires_[i].isSelected()) {
          data[indiceNav][0] = this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom();

          data[indiceNav][1] = "" + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[i]);
          compteurNavires += FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[i]);
          indiceNav++;

        }
      }
      data[indiceNav][0] = "TOTAL";
      data[indiceNav][1] = "" + compteurNavires;

    } else if (val < this.donnees_.getCategoriesNavires_().getListeNavires_().size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data = new Object[1][2];
      data[0][0] = this.donnees_.getCategoriesNavires_().retournerNavire(val).getNom();
      data[0][1] = "" + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[val]);
    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    // determiner le nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" G�n�ration navires \"\n";
    g += "  sous-titre \" Nombres de navires par cat�gories \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
   

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Cat�gories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// this.donnees_.getCategoriesNavires_().getListeNavires_().size()
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" nb.navires" + "\"\n";
    g += "    unite \" Navires \"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + FonctionsSimu.diviserSimu((SiporAlgorithmeGenerationBateaux.max(donnees_)))
    // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    
    g += "  courbe\n  {\n";
    g += "    titre \"" + "nombre de navires g�n�r�s"
   
    + "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    trace lineaire\n";
    g += "    marqueurs oui\n";
    g += "    aspect\n {\n";
    g += "      contour.couleur " // choix entre "3366FF", "FFCC00", "FF0000", "009900"
        + "3366FF" + " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " " +FonctionsSimu.diviserSimu( this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[n]) 
            + "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
            + "\n";
        indiceNavire++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";
    // }//fin du for

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;
    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" G�n�ration navires \"\n";
    g += "  sous-titre \" Nombres de navires par cat�gories \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" nb.navires" + "\"\n";
    g += "    unite \"" + " Navires" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + FonctionsSimu.diviserSimu((SiporAlgorithmeGenerationBateaux.max(donnees_)))
    // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    /*
     * PLUSIEURS COURBES
     */
    g += "  courbe\n  {\n";
    g += "    titre \"" + "nombre de navires g�n�r�s"
    + "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " " + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[n]) 
            + "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
            + "\n";
        indiceNavire++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    /**
     * declaration d'un seuil
     */
    g += " contrainte\n";
    g += "{\n";
    // a mettre le seuil
    g += "titre \"seuil : max =" + 50 + "\"\n";
    // str+="orientation horizontal \n";
    g += " type max\n";
    g += " valeur " + 100 + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

    g += " \n }\n";
    // }//fin du for

    return g;
  }

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

       
        modifierCamemberts();
        
      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  
  /**
   * Methode d'affichage du camembert correspondant � la proportion de navires de la simulation.
   * @return
   */
  public ChartPanel creerCamembert(){
    
    String[] noms=new String[donnees_.getCategoriesNavires_().getListeNavires_().size()];
    for(int i=0;i<donnees_.getCategoriesNavires_().getListeNavires_().size();i++)
      noms[i]=donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
    
   
    
    return SiporJFreeChartCamembert.creerCamembert(FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires), noms, "G�n�rations de navires par cat�gories");
    
  }
  
 public void modifierCamemberts(){
   
   
    int nbNaviresAffiches=0;
    for(int i=0;i<donnees_.getCategoriesNavires_().getListeNavires_().size();i++)
      if (this.tableauChoixNavires_[i].isSelected())
        nbNaviresAffiches++;
    
    String[] noms=new String[nbNaviresAffiches];
    int cpt=0;
    for(int i=0;i<donnees_.getCategoriesNavires_().getListeNavires_().size();i++)
      if (this.tableauChoixNavires_[i].isSelected())
        noms[cpt++]=donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
    
    
    
    int[] tabNavires=new int[nbNaviresAffiches];
    cpt=0;
    for(int i=0;i<donnees_.getCategoriesNavires_().getListeNavires_().size();i++)
      if (this.tableauChoixNavires_[i].isSelected())
        tabNavires[cpt++]=this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires[i];
   
   
   SiporJFreeChartCamembert.modifierCamembert(camembert_, FonctionsSimu.diviserSimu(tabNavires), noms, "G�n�rations de navires par cat�gories");
   }
 
 
 public ChartPanel creerHisto3d(){
    
   String[] noms=new String[donnees_.getCategoriesNavires_().getListeNavires_().size()];
    int cpt=0;
    for(int i=0;i<donnees_.getCategoriesNavires_().getListeNavires_().size();i++)
        noms[cpt++]=donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
    
    return SiporJFreeChartCamembert.creerHisto3d(this.donnees_.getParams_().ResultatsCompletsSimulation.ResultatsGenerationNavires, "G�n�rations de navires par cat�gories",noms);
}
  
  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
