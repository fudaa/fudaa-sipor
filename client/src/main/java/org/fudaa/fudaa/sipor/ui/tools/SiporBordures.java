package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class SiporBordures {

  public static Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  public static Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  public static Border bordnormal_ = BorderFactory.createEtchedBorder();
  public static Border complet = BorderFactory.createCompoundBorder( raisedBevel_,loweredBevel_);
  public static Border compound_ = complet;//BorderFactory.createEtchedBorder();
  
  public static Font siporFonte=new Font("Tahoma",Font.BOLD,12);
  public static Font siporFontetitres=new Font("Tahoma",Font.BOLD,14);
  public static Border chenaux=BorderFactory.createTitledBorder(compound_,"Saisir un chenal", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border chenaux2=BorderFactory.createTitledBorder(compound_,"Modifier un chenal", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border ecluse=BorderFactory.createTitledBorder(compound_,"Saisir une �cluse", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border ecluse2=BorderFactory.createTitledBorder(compound_,"Modifier une �cluse", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border quai=BorderFactory.createTitledBorder(compound_,"Saisir un quai", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border quai2=BorderFactory.createTitledBorder(compound_,"Modifier un quai", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border navire=BorderFactory.createTitledBorder(compound_,"Saisir une cat�gorie de navire", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  public static Border navire2=BorderFactory.createTitledBorder(compound_,"Modifier une cat�gorie de navire", TitledBorder.CENTER, TitledBorder.TOP,siporFontetitres) ;
  
  
  SiporBordures(){
    
  }
  
  public static Border createTitledBorder(final String title){
    
    return BorderFactory.createTitledBorder(compound_,title);//, TitledBorder.LEFT, TitledBorder.TOP,siporFonte) ;
  }
  
  
}
