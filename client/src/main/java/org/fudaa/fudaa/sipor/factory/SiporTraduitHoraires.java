package org.fudaa.fudaa.sipor.factory;

public class SiporTraduitHoraires {

  public static float traduitMinutesEnHeuresMinutes(final double val) {
    final int valeur = (int) val;
    float horaire = 0;
    int heure = 0;
    int minute = 0;
    int reste = 0;

    heure = valeur / 60;
    reste = valeur % 60;

    minute = reste/* /60 */;

    // reste= reste%60;

    // if(reste>30)minute++;

    horaire = (float) (heure + minute / 100.0);

    return horaire;
  }

  public static String traduitMinutesEnHeuresMinutes2(final double val) {
    final int valeur = (int) val;
    int heure = 0;
    int minute = 0;
    int reste = 0;

    heure = valeur / 60;
    reste = valeur % 60;

    minute = reste/* /60 */;

    // reste= reste%60;

    // if(reste>30)minute++;
    final String resultat;
if(minute>=10)
    resultat = "" + heure + "H " + minute + "Min";
else
	resultat = "" + heure + "H 0" + minute + "Min";
    return resultat;
  }
  
  public static int traduitHeuresMinutesEnMinutes(final double val) {
	  final float valeur = (float) val;
	  int horaire = 0;
	  int heure = 0;
	  int minute = 0;

	  heure = (int) valeur;
	  minute = (int) ((valeur-heure)*100);

	  if(minute<60) horaire = heure*60 + minute;

	  return horaire;
  }

}
