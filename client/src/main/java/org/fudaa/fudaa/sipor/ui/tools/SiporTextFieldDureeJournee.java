package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuDialogError;

import org.fudaa.fudaa.sipor.SiporImplementation;

public class SiporTextFieldDureeJournee extends SiporTextFieldDuree {

  public SiporTextFieldDureeJournee() {
    super();
  }

  public SiporTextFieldDureeJournee(int columns) {
    super(columns);
  }

  public SiporTextFieldDureeJournee(String text, int columns) {
    super(text, columns);
  }

  public SiporTextFieldDureeJournee(String text) {
    super(text);
  }

  public void traitementApresFocus(FocusEvent e) {

    String contenu = this.getText();

    if (contenu.equals("")) return;
    //-- Traduction en float --//

    try {
      float valeur = Float.parseFloat(contenu);
      if (valeur < 0) {
        new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Le nombre doit �tre compris entre 0 et 24.")
            .activate();

        setText("");
        this.requestFocus();
      } else {

        if (contenu.lastIndexOf(".") != -1) {
          String unite = contenu.substring(contenu.lastIndexOf(".") + 1, contenu.length());
          if (unite.length() > 2) {
            new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                "Il doit y avoir 2 chiffres maximum apr�s la virgule").activate();
            setText("");
            this.requestFocus();
            return;
          }

          float valUnite = Float.parseFloat(unite);
          if (valUnite >= 60) {
            new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                "Les unit�s doivent �tre inf�rieures � 60 minutes.").activate();
            setText("");
            this.requestFocus();
            return;

          }

        }

        //transformation en nombre � 2 chiffres apr�s le virgule
        float format = valeur;// super.conversionFormat(valeur,contenu);

        if (format > 24) {
          new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Le nombre doit �tre compris entre 0 et 24.")
              .activate();

          setText("");
          this.requestFocus();
        } else this.setText("" + format);

      }

    } catch (NumberFormatException e1) {
      new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Ce nombre n'existe pas.").activate();
      setText("");
      this.requestFocus();
    }

    SiporTextFieldDureeJournee.this.setForeground(Color.black);
  }

  protected int taille() {
    return 5;
  }
}
