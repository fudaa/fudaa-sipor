package org.fudaa.fudaa.sipor.structures;

import java.io.Serializable;
import java.util.ArrayList;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;


/**
 * Classes regroupant les donn�es des ecluses.
 * 
 * @author Adrien Hadoux
 */
public class SiporEcluse implements NetworkUserObject, Serializable{

  String nom_;
  double longueur_;
  double largeur_;
  double tempsEclusee_;
  double tempsFausseBassinnee_;
  double dureePassageEtale_;
  double creneauEtaleAvantPleineMerDeb_;
  double creneauEtaleApresPleineMerFin_ = 24;
  SiporHoraire horaire = new SiporHoraire();

  double dureeIndispo_;
  double frequenceMoyenne_;

  // loi: indice de 1 a 10
  int loiIndispo_;
  int loiFrequence_;

  /**
   * Type de loi ENTIER QUI PREND LA VALEUR DE LA LOI CHOISIE.<br>: 0 => loi d erlang<br>
   * 1 => deterministe <br>
   * 2 => journaliere par defaut loi d'erlang
   */
  int typeLoi_;

  /**
   * Declaration du tableau de loi deterministe.
   */
  ArrayList loiDeterministe_ = new ArrayList();
  /**
   * Gare amont de l �cluse par defaut mis a zero.
   */
  int gareAmont_;

  /**
   * gare aval mis par defaut a la gare 1.
   */

  int gareAval_ = 1;

  /**
   * constructeur par d�faut de la classe ecluse.
   */
  public SiporEcluse() {

  }

  /**
   * Constructeur de la classe ��cluseour une saisie des attributs.
   * 
   * @param _nom
   * @param _longueur
   * @param _largeur
   * @param _tempsEclusee
   * @param _tempsFausseBassinnee
   * @param _creneauEtaleAvantPleineMerDeb
   * @param _creneauEtaleAvantPleineMerFin
   * @param _creneauEtaleApresPleineMerDeb
   * @param _creneauEtaleApresPleineMerFin
   * @param _h
   * @param _gareAmont
   * @param _gareAval
   */
  public SiporEcluse(final String _nom, final double _longueur, final double _largeur, final double _tempsEclusee,
      final double _tempsFausseBassinnee, final double _creneauEtaleAvantPleineMerDeb,
      final double _creneauEtaleAvantPleineMerFin, final double _creneauEtaleApresPleineMerDeb,
      final double _creneauEtaleApresPleineMerFin, final SiporHoraire _h, final int _gareAmont, final int _gareAval) {
    super();
    this.nom_ = _nom;
    this.longueur_ = _longueur;
    this.largeur_ = _largeur;
    this.tempsEclusee_ = _tempsEclusee;
    this.tempsFausseBassinnee_ = _tempsFausseBassinnee;
    this.creneauEtaleAvantPleineMerDeb_ = _creneauEtaleAvantPleineMerDeb;
    this.creneauEtaleApresPleineMerFin_ = _creneauEtaleApresPleineMerFin;
    this.horaire = _h;
    this.gareAmont_ = _gareAmont;
    this.gareAval_ = _gareAval;
  }

  public String[] affichage() {
    final String[] t = new String[8];
    t[0] = " nom: " + nom_;
    t[1] = "\n longueur: " + (float) longueur_;
    t[2] = "\n largeur: " + (float) largeur_;
    t[3] = "\n temps eclusee: " + (float) tempsEclusee_;
    t[4] = "\n fausse bassinnee: " + (float) tempsFausseBassinnee_;
    t[5] = "\n creneau etale:";
    t[6] = "\n        avant pleine mer: " + (float) this.creneauEtaleAvantPleineMerDeb_;
    t[7] = "\n        apr�s pleine mer: " + (float) this.creneauEtaleApresPleineMerFin_;

    return t;
  }

public String getNom_() {
	return nom_;
}

public void setNom_(String nom_) {
	this.nom_ = nom_;
}

public double getLongueur_() {
	return longueur_;
}

public void setLongueur_(double longueur_) {
	this.longueur_ = longueur_;
}

public double getLargeur_() {
	return largeur_;
}

public void setLargeur_(double largeur_) {
	this.largeur_ = largeur_;
}

public double getTempsEclusee_() {
	return tempsEclusee_;
}

public void setTempsEclusee_(double tempsEclusee_) {
	this.tempsEclusee_ = tempsEclusee_;
}

public double getTempsFausseBassinnee_() {
	return tempsFausseBassinnee_;
}

public void setTempsFausseBassinnee_(double tempsFausseBassinnee_) {
	this.tempsFausseBassinnee_ = tempsFausseBassinnee_;
}

public double getDureePassageEtale_() {
	return dureePassageEtale_;
}

public void setDureePassageEtale_(double dureePassageEtale_) {
	this.dureePassageEtale_ = dureePassageEtale_;
}

public double getCreneauEtaleAvantPleineMerDeb_() {
	return creneauEtaleAvantPleineMerDeb_;
}

public void setCreneauEtaleAvantPleineMerDeb_(
		double creneauEtaleAvantPleineMerDeb_) {
	this.creneauEtaleAvantPleineMerDeb_ = creneauEtaleAvantPleineMerDeb_;
}

public double getCreneauEtaleApresPleineMerFin_() {
	return creneauEtaleApresPleineMerFin_;
}

public void setCreneauEtaleApresPleineMerFin_(
		double creneauEtaleApresPleineMerFin_) {
	this.creneauEtaleApresPleineMerFin_ = creneauEtaleApresPleineMerFin_;
}

public SiporHoraire getH_() {
	return horaire;
}

public void setH_(SiporHoraire h_) {
	this.horaire = h_;
}

public double getDureeIndispo_() {
	return dureeIndispo_;
}

public void setDureeIndispo_(double dureeIndispo_) {
	this.dureeIndispo_ = dureeIndispo_;
}

public double getFrequenceMoyenne_() {
	return frequenceMoyenne_;
}

public void setFrequenceMoyenne_(double frequenceMoyenne_) {
	this.frequenceMoyenne_ = frequenceMoyenne_;
}

public int getLoiIndispo_() {
	return loiIndispo_;
}

public void setLoiIndispo_(int loiIndispo_) {
	this.loiIndispo_ = loiIndispo_;
}

public int getLoiFrequence_() {
	return loiFrequence_;
}

public void setLoiFrequence_(int loiFrequence_) {
	this.loiFrequence_ = loiFrequence_;
}

public int getTypeLoi_() {
	return typeLoi_;
}

public void setTypeLoi_(int typeLoi_) {
	this.typeLoi_ = typeLoi_;
}

public ArrayList getLoiDeterministe_() {
	return loiDeterministe_;
}

public void setLoiDeterministe_(ArrayList loiDeterministe_) {
	this.loiDeterministe_ = loiDeterministe_;
}

public int getGareAmont_() {
	return gareAmont_;
}

public void setGareAmont_(int gareAmont_) {
	this.gareAmont_ = gareAmont_;
}

public int getGareAval_() {
	return gareAval_;
}

public void setGareAval_(int gareAval_) {
	this.gareAval_ = gareAval_;
}

public String getName() {
	// TODO Auto-generated method stub
	return getNom_();
}

public SiporHoraire getHoraire() {
	return horaire;
}

public void setHoraire(SiporHoraire horaire) {
	this.horaire = horaire;
}

public void setName(String value) {
	setNom_(value);
}

}
