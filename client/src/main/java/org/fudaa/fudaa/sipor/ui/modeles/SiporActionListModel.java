package org.fudaa.fudaa.sipor.ui.modeles;

import java.util.ArrayList;

import javax.swing.AbstractListModel;

import org.fudaa.fudaa.sipor.ui.tools.SiporActionClicUSer;

public class SiporActionListModel extends AbstractListModel {

  ArrayList listeActions_;

  public SiporActionListModel(ArrayList listeActions) {
    listeActions_ = listeActions;
  }

  public Object getElementAt(int index) {
    if (index == 0) return "Initialisation";

    //		-- On recupere la derniere action realisee --//	
    SiporActionClicUSer action = (SiporActionClicUSer) listeActions_.get(index);

    if (action.compteur == action.getNbTotalGares_()) return " Sch�ma complet ";

    return "Action " + index;
  }

  public int getSize() {
    return listeActions_.size();
  }

  public void miseAjour() {
    fireContentsChanged(this, 0, getSize());
  }
}
