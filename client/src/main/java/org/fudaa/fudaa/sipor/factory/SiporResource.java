/*
 * @file         SiporResource.java
 * @creation     1999-10-01
 * @modification $Date: 2007-07-24 09:43:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.factory;

import com.memoire.bu.BuResource;

import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Les ressources de Sipor.
 * 
 * @version $Revision: 1.6 $ $Date: 2007-07-24 09:43:33 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class SiporResource extends FudaaResource {
  protected SiporResource(BuResource _parent) {
		super(_parent);
	}
/** structure de parametres (SParametresSipor) */
  public final static String parametres = "parametres";
  /** T�moin pour l'utilisation du bassin 1 (bool�en). */
  public final static String utilisationBassin1 = "utilisationBassin1";
  /** T�moin pour l'utilisation du bassin 2 (bool�en). */
  public final static String utilisationBassin2 = "utilisationBassin2";
  /** T�moin pour l'utilisation du bassin 3 (bool�en). */
  public final static String utilisationBassin3 = "utilisationBassin3";
  /** R�sultats des it�rations (SResultatsIterations). */
  public final static String resultats = "resultats";
  public final static SiporResource SIPOR = new SiporResource(FudaaResource.FUDAA);
}
