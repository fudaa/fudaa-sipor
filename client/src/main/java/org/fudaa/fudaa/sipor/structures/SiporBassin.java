package org.fudaa.fudaa.sipor.structures;

import java.io.Serializable;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.w3c.dom.Node;

/**
 * 
 */

/**
 * contient les parametres necessaire a la description des bassins classe de base des bassins
 * 
 * @author principal
 */
public class SiporBassin implements NetworkUserObject, Serializable{

  /**
   * definiton d uun bassin
   */
  String nom_;

  /**
   * Gare amont du bassin correspond a l indice de la gare dans la liste de gares par defaut la gare amont est la
   * premiere gare
   */
  int gareAmont = 0;

public String getNom_() {
	return nom_;
}

public void setNom_(String nom_) {
	this.nom_ = nom_;
}

public int getGareAmont() {
	return gareAmont;
}

public void setGareAmont(int gareAmont) {
	this.gareAmont = gareAmont;
}

public String getName() {
	// TODO Auto-generated method stub
	return nom_;
}

public void setName(String value) {
	setNom_(value);
}
}
