package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuDialogError;

import org.fudaa.fudaa.sipor.SiporImplementation;

public class SiporTextFieldDuree extends SiporTextField{

	public SiporTextFieldDuree() {
		super();
	}

	

	public SiporTextFieldDuree(int columns) {
		super(columns);
	}

	public SiporTextFieldDuree(String text, int columns) {
		super(text, columns);
	}

	public SiporTextFieldDuree(String text) {
		super(text);
	}

	
	@Override
	public String getText() {
		String res = super.getText();
		if(res != null) {
			res = res.replace(":",".").replace(",", ".");
		}
		return res;
	}

	@Override
	public void setText(String res) {
		if(res != null) {
			res = res.replace(".",":").replace(",", ":");
		}
		super.setText(res);
	}


	/**Methode destinee a etre surchargee par les classes h�ritant de SiporTextField.
	 * Permet de g�rer les contr�les apr�s saisie de l'utilisateur **/
	public void traitementApresFocus(FocusEvent e){

		String contenu=this.getText();

		if (contenu.equals("")) 
			return;
		//-- Traduction en float --//

		try {
			float valeur=Float.parseFloat(contenu);
			if(valeur<0)
			{
				new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
				"Ce nombre est n�gatif").activate();

				setText("");
				this.requestFocus();
			}
			else
			{
				if(contenu.lastIndexOf(".")!=-1){
					String unite=contenu.substring(contenu.lastIndexOf(".")+1, contenu.length());
					if(unite.length()>2){
						new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
						"Il doit y avoir 2 chiffres maximum apr�s la virgule").activate();
						setText("");
						this.requestFocus();
						return;
					}
					
					
						float valUnite=Float.parseFloat(unite);
						if(valUnite>=60){
							new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
							"Les unit�s doivent �tre inf�rieures � 60 minutes.").activate();
							setText("");
							this.requestFocus();
							return;
							
						}
					
				
				}
				//transformation en nombre � 2 chiffres apr�s le virgule
				float format= valeur;//conversionFormat(valeur,contenu);
				this.setText(""+format);

			}

		} catch (NumberFormatException e1) {
			new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
			"Ce nombre n'existe pas.").activate();
			setText("");
			this.requestFocus();
		}

		SiporTextFieldDuree.this.setForeground(Color.black);
	}


	/**methode qui formatte un reel en duree.
	 * 2 chiffres apr�s la virgule.
	 * modulo 60 minutes
	 * exemple: 35.652 se transformera en 36.05  **/
	protected float conversionFormat(float valeur,String contenu){

		float resul=valeur;
		String min="";
		int minutes=00;

		if(contenu.lastIndexOf(".")==-1)
			minutes=00;
		else
		{
			min=contenu.substring(contenu.lastIndexOf(".")+1);
			minutes=Integer.parseInt(min);
			if(minutes/10==0 && min.length()==1)
				minutes=minutes*10;
		}
		while(minutes/100!=0)
			minutes=minutes/10;

		//creation des heures
		int heures=0;
		//le cast va arrondir au chiffre le plus proche or on veut garder le nombre tel quel
		heures=(int)valeur;

		//modulo des minutes
		while(minutes>=60)
		{
			heures++;
			minutes-=60;
		}

		float resultat=heures+ (float)(minutes/100.0);
		
		
		
		
		return resultat;

	}

	protected int taille(){
		return 5;
	}
	
protected void specifier_validator(){
	
	}

}
