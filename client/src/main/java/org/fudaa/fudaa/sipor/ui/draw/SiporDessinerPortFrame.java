/**
 * 
 */
package org.fudaa.fudaa.sipor.ui.draw;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuToolBar;

import org.fudaa.dodico.corba.sipor.SParametresGrapheTopologie;
import org.fudaa.dodico.corba.sipor.SParametresGrapheTopologies;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.html.GenerateurPostScript;
import org.fudaa.fudaa.sipor.ui.modeles.SiporActionListModel;
import org.fudaa.fudaa.sipor.ui.tools.SiporActionClicUSer;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * Classe qui g�re le panel d'affichage du port affiche un message indiquant le nombre de gare a saisir et un bouton de
 * validation/impression/generation en format postscript.
 * 
 * @author Adrien Hadoux
 */
public class SiporDessinerPortFrame extends SiporInternalFrame {

  /**
   * Panel de dessin du port
   */
  public SiporDessinerPort panelDessin_;

  int compteur_ = 0;

  JLabel labelCompteur_;
  JLabel labelMessage1_ = new JLabel();
  JLabel labelMessage2_ = new JLabel();
  JLabel labelMessage3_ = new JLabel();
  JMenuItem menuaffichageNoms_;

  final BuButton rafraichir_ = new BuButton(FudaaResource.FUDAA.getIcon("rafraichir_18"), "Rafra�chir");
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter Ps");
  final BuButton validation2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton initialisation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_creer"), "Nouveau");

  final BuButton retour_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_defaire"), "Retour arri�re");

  /**
   * Arraylist contenant la liste successives des graphes apr�s chaque modification.
   */
  ArrayList listeActions_ = new ArrayList();

  /**
   * Liste d�crivant les diff�rentes actions r�alis�es par l'utilisateur.
   */
  JList bigDataList;
  SiporActionListModel bigData;

  /**
   * Listener
   */
  ActionListener listenerInit_;

  /**
   * Donn�es de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Definition d'un scroll pane
   */
  JScrollPane ascenceur_;

  boolean firstTime_ = true;

  /**
   * Constructeur de la fenetre d'affichage du port
   * 
   * @param _donnees donn�es de la simulation
   */
  public SiporDessinerPortFrame(final SiporDataSimulation _donnees, final FudaaCommonImplementation _application) {
    super("", true, true, true, true);
    donnees_ = _donnees;

    this.getContentPane().setLayout(new BorderLayout());
    this.setTitle("Sch�ma du port");
    this.setSize(800, 640);
    this.setBorder(SiporBordures.compound_);
    // insertion des composants dans la frame
    final JPanel controlPanel = new JPanel(new GridLayout(2, 1));
    controlPanel.setBorder(SiporBordures.compound_);
    // controlPanel.add(new JLabel("Nombre de gares � ins�rer: "));
    labelCompteur_ = new JLabel("");
    labelCompteur_.setBorder(SiporBordures.bordnormal_);
    JPanel panelLabel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    panelLabel.add(labelCompteur_);
    panelLabel.add(labelMessage1_);
    labelMessage1_.setBorder(SiporBordures.bordnormal_);
    labelMessage2_.setBorder(SiporBordures.bordnormal_);
    labelMessage3_.setBorder(SiporBordures.bordnormal_);
    controlPanel.add(panelLabel);
    JPanel panelLabel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    panelLabel2.add(labelMessage2_);
    controlPanel.add(panelLabel2);
    this.getContentPane().add(controlPanel, BorderLayout.SOUTH);

    // insertion du panel de dessin dans la frame
    this.panelDessin_ = new SiporDessinerPort(this.donnees_, this.compteur_, this, _application);
    this.ascenceur_ = new JScrollPane(/* panelDessin_ */);

    JViewport port = new JViewport();
    port.setView(panelDessin_);
    this.ascenceur_.setViewport(port);

    // utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector(ascenceur_);

    final JPanel bourrin = new JPanel();
    bourrin.setSize(1200, 800);
    bourrin.setLayout(new GridLayout(1, 1));
    bourrin.add(ascenceur_);
    this.getContentPane().add(bourrin, BorderLayout.CENTER);

    rafraichir_.setToolTipText("rafra�chit le dessin de la mod�lisation du port");
    rafraichir_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        panelDessin_.repaint();
      }
    });

    // -- Listener retour topologie du port --//
    retour_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        undo();

      }
    });

    listenerInit_ = new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        initialisationDessin();

      }
    };

    initialisation_.setToolTipText("Permet de r�initialiser le dessin du port");
    initialisation_.addActionListener(listenerInit_);

    validation_.setToolTipText("Permet de g�n�rer un fichier PostScript � partir de la mod�lisation du port");
    validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // panelDessin_.exportation();
        // 1) creation d'un g�n�rateur postscript
        final GenerateurPostScript generateur = new GenerateurPostScript(donnees_, SiporDessinerPortFrame.this);
        // 2) appel a la methode enregistrer
        generateur.ecrirePostScript();

      }
    });

    retour_.setToolTipText("Permet de revenir en arri�re");

    validation2_.setToolTipText("Permet de g�n�rer une image png � partir de la mod�lisation du port");

    validation2_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // execution de la methode de generation de JPeg
        panelDessin_.exportation();

      }
    });

    creationToolBarre();
    creationMenuBar();
    creationUndoList();
    enregistrerAction();

    // affichage de la fenetre
    this.setVisible(true);

  }

  /** Methode qui reinitialise le dessin **/
  public void initialisationDessin() {
    panelDessin_.compteur_ = -1;
    donnees_.getParams_().grapheTopologie = new SParametresGrapheTopologies();
    donnees_.getParams_().grapheTopologie.nbArcs = 0;
    donnees_.getParams_().grapheTopologie.graphe = new SParametresGrapheTopologie[2000];
    donnees_.getParams_().grapheTopologie.nbGaresDessinnees = 0;
    panelDessin_.x_ = 0;
    panelDessin_.y_ = 0;
    panelDessin_.oldX_ = 0;
    panelDessin_.oldY_ = 0;
    panelDessin_.tableauGare_ = new ArrayList();
    panelDessin_.nbGares_ = donnees_.getListeGare_().getListeGares_().size();;
    labelMessage1_.setText("Cliquez sur le dessin pour positionner les gares");
    labelMessage2_.setText("");
    panelDessin_.repaint();
  }

  // -- methode de creation de la barre toolkit --//
  private void creationToolBarre() {
    BuToolBar barre = new BuToolBar();
    // barre.setBorder(SiporBordures.compound_);
    validation_.setVerticalTextPosition(AbstractButton.BOTTOM);
    validation_.setHorizontalTextPosition(AbstractButton.CENTER);
    validation2_.setVerticalTextPosition(AbstractButton.BOTTOM);
    validation2_.setHorizontalTextPosition(AbstractButton.CENTER);
    initialisation_.setVerticalTextPosition(AbstractButton.BOTTOM);
    initialisation_.setHorizontalTextPosition(AbstractButton.CENTER);
    retour_.setVerticalTextPosition(AbstractButton.BOTTOM);
    retour_.setHorizontalTextPosition(AbstractButton.CENTER);

    retour_.setFont(new Font("Tahoma", Font.PLAIN, 10));
    validation2_.setFont(new Font("Tahoma", Font.PLAIN, 10));
    initialisation_.setFont(new Font("Tahoma", Font.PLAIN, 10));
    barre.add(initialisation_);
    // barre.add(validation_);
    barre.add(validation2_);
    // barre.add(retour_);
    barre.setFloatable(false);
    this.getContentPane().add(barre, BorderLayout.PAGE_START);

  }

  // -- methode de creation de la barre de menu --//
  private void creationMenuBar() {

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Affichage");
    final JMenuItem menuRafraichissement = new JMenuItem("Rafra�chir");
    final JMenuItem menuReinitialisation = new JMenuItem("R�initialiser");
    menuaffichageNoms_ = new JMenuItem("Masquer noms param�tres");

    final JMenu menuInfo = new JMenu("A propos de");
    final JMenu menuGeneration = new JMenu("G�n�ration");
    final JMenuItem menuPostScript = new JMenuItem("Fichier postscript");
    final JMenuItem menuJpeg = new JMenuItem("Fichier Jpeg");
    final JMenuItem menuAidePS = new JMenuItem("A propos de postscript");

    menuFile.add(menuFileExit);
    menuOption.add(menuRafraichissement);
    menuOption.add(menuReinitialisation);
    menuOption.add(menuaffichageNoms_);
    menuInfo.add(menuAidePS);
    menuGeneration.add(menuJpeg);
    menuGeneration.add(menuPostScript);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    menuBar.add(menuGeneration);
    setJMenuBar(menuBar);

    // -- listener des menus --//

    menuAidePS.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        new BuDialogMessage(
            null,
            null,
            "Le format PostScript est un format qui permet de visualiser des dessins et de les imprimer. Pour en savoir plus, http://www.adobe.com/products/postscript/")
            .activate();

      }
    });

    menuRafraichissement.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        panelDessin_.repaint();
      }
    });
    menuaffichageNoms_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        if (panelDessin_.afficheConnections_ == true) {
          panelDessin_.afficheConnections_ = false;
          menuaffichageNoms_.setText("Afficher noms param�tres");
        } else {
          panelDessin_.afficheConnections_ = true;
          menuaffichageNoms_.setText("Masquer noms param�tres");
        }
        panelDessin_.repaint();
      }
    });

    menuReinitialisation.addActionListener(this.listenerInit_);

    menuPostScript.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // 1) creation d'un g�n�rateur postscript
        final GenerateurPostScript generateur = new GenerateurPostScript(donnees_, SiporDessinerPortFrame.this);
        // 2) appel a la methode enregistrer
        generateur.ecrirePostScript();

      }
    });

    menuJpeg.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // execution de la methode de generation de JPeg
        panelDessin_.exportation();

      }
    });
  }

  public void enregistrerAction() {

    // -- Nouveau graphe duplicata --//
    SParametresGrapheTopologies nouveau = new SParametresGrapheTopologies();

    // -- Graphe source --//
    SParametresGrapheTopologies copie = donnees_.getParams_().grapheTopologie;

    nouveau.graphe = new SParametresGrapheTopologie[2000];
    nouveau.nbArcs = copie.nbArcs;
    nouveau.nbGaresDessinnees = copie.nbGaresDessinnees;
    for (int k = 0; k < nouveau.nbArcs; k++) {
      nouveau.graphe[k] = new SParametresGrapheTopologie();
      nouveau.graphe[k].nomConnection = copie.graphe[k].nomConnection;
      nouveau.graphe[k].numGare1 = copie.graphe[k].numGare1;
      nouveau.graphe[k].numGare2 = copie.graphe[k].numGare2;
      nouveau.graphe[k].typeConnection = copie.graphe[k].typeConnection;
      nouveau.graphe[k].xGare1 = copie.graphe[k].xGare1;
      nouveau.graphe[k].xGare2 = copie.graphe[k].xGare2;
      nouveau.graphe[k].yGare1 = copie.graphe[k].yGare1;
      nouveau.graphe[k].yGare2 = copie.graphe[k].yGare2;

    }

    ArrayList copieGare = new ArrayList();
    for (int i = 0; i < panelDessin_.tableauGare_.size(); i++) {
      int x = ((PointPort) panelDessin_.tableauGare_.get(i)).x_;
      int y = ((PointPort) panelDessin_.tableauGare_.get(i)).y_;
      PointPort p = new PointPort(x, y);

      copieGare.add(p);
    }

    // --Enregistrement du duplicata dans la listeAction --//
    SiporActionClicUSer action = new SiporActionClicUSer(nouveau, panelDessin_.compteur_, copieGare, donnees_
        .getListeGare_().getListeGares_().size());
    listeActions_.add(action);

    // -- Mise a jour de la liste des actions --//
    bigData.miseAjour();

  }

  /** Methode qui charge les parametres correspondant � une action precedemment sauvegard�e **/
  public void chargerAction(SiporActionClicUSer actionPrecedente) {

    panelDessin_.compteur_ = actionPrecedente.compteur;
    donnees_.getParams_().grapheTopologie = actionPrecedente.graphe;
    panelDessin_.tableauGare_ = actionPrecedente.tableauGare_;

    // -- Nouveau graphe source --//
    SParametresGrapheTopologies copie = actionPrecedente.graphe;

    // -- Graphe duplicata --//

    SParametresGrapheTopologies nouveau = new SParametresGrapheTopologies();

    nouveau.graphe = new SParametresGrapheTopologie[2000];
    nouveau.nbArcs = copie.nbArcs;
    nouveau.nbGaresDessinnees = copie.nbGaresDessinnees;
    for (int k = 0; k < nouveau.nbArcs; k++) {
      nouveau.graphe[k] = new SParametresGrapheTopologie();
      nouveau.graphe[k].nomConnection = copie.graphe[k].nomConnection;
      nouveau.graphe[k].numGare1 = copie.graphe[k].numGare1;
      nouveau.graphe[k].numGare2 = copie.graphe[k].numGare2;
      nouveau.graphe[k].typeConnection = copie.graphe[k].typeConnection;
      nouveau.graphe[k].xGare1 = copie.graphe[k].xGare1;
      nouveau.graphe[k].xGare2 = copie.graphe[k].xGare2;
      nouveau.graphe[k].yGare1 = copie.graphe[k].yGare1;
      nouveau.graphe[k].yGare2 = copie.graphe[k].yGare2;

    }

    ArrayList copieGare = new ArrayList();
    for (int i = 0; i < actionPrecedente.tableauGare_.size(); i++) {
      int x = ((PointPort) actionPrecedente.tableauGare_.get(i)).x_;
      int y = ((PointPort) actionPrecedente.tableauGare_.get(i)).y_;
      PointPort p = new PointPort(x, y);

      copieGare.add(p);
    }
    // on reinitialise les parametres a leur ancienne valeur
    panelDessin_.tableauGare_ = copieGare;
    panelDessin_.compteur_ = actionPrecedente.compteur;
    donnees_.getParams_().grapheTopologie = nouveau;

  }

  public void undo() {

    if (listeActions_.size() < 1) {
      initialisationDessin();
      return;
    }
    // -- On recupere la derniere action realisee --//
    SiporActionClicUSer actionPrecedente = (SiporActionClicUSer) listeActions_.get(listeActions_.size() - 1);

    chargerAction(actionPrecedente);

    // --On supprime la derniere action de la liste puisque l'on retourne � elle --//
    listeActions_.remove(listeActions_.size() - 1);
    // listeActions_.remove(listeActions_.size()-1);

    panelDessin_.repaint();
  }

  public void undoSpecial(int index) {

    if (index < 1) {
      initialisationDessin();
      return;
    }
    // -- On recupere la derniere action realisee --//
    SiporActionClicUSer actionPrecedente = (SiporActionClicUSer) listeActions_.get(index);

    // on reinitialise les parametres a leur ancienne valeur
    chargerAction(actionPrecedente);


    panelDessin_.repaint();
  }

  /** methode de creation de la list des actions pour revenir en arriere. **/
  public void creationUndoList() {

    bigData = new SiporActionListModel(listeActions_);

    bigDataList = new JList(bigData);

    bigDataList.setOpaque(true);
    JPanel panelUndo = new JPanel(new BorderLayout());
    panelUndo.add(new JScrollPane(bigDataList), BorderLayout.CENTER);
    panelUndo.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Actions"));

    BuButton nettoyer = new BuButton(FudaaResource.FUDAA.getIcon("crystal_corbeille"), "Nettoyer");
    nettoyer.setFont(new Font("Tahoma", Font.PLAIN, 10));
    nettoyer.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {

        if (listeActions_.size() > 1) {
          SiporActionClicUSer init = (SiporActionClicUSer) listeActions_.get(0);
          listeActions_.clear();
          listeActions_.add(init);
          // -- Mise a jour de la liste des actions --//
          bigData.miseAjour();
        }
      }

    });

    panelUndo.add(nettoyer, BorderLayout.SOUTH);
    this.getContentPane().add(panelUndo, BorderLayout.EAST);

    // -- Listener de la liste --//
    MouseListener mouseListener = new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // if (e.getClickCount() == 1) {
        int index = bigDataList.locationToIndex(e.getPoint());

        undoSpecial(index);

        // }
      }
    };

    // ajout du listener de la liste
    bigDataList.addMouseListener(mouseListener);

  }

}
