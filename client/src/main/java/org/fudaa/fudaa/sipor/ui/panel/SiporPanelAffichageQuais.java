package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuTable;

import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporQuais;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des quais sous forme d un tableau: avec la possibilit� de modifier ou de supprimer
 * une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT SI ON UTILISE
 * PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE TABLEHEADER EN DEBUT
 * DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelAffichageQuais extends JPanel implements Observer {

  public String[] titreColonnes_ = { "Nom", "Bassin", "Longueur", "D�halage autorise", "Loi fr�quence", "Fr�quence indispo",
      "Dur�e indispo", "Loi Erlang", "Cr�neau 1", "Cr�neau 2", "Cr�neau 3" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des quais
   */

  public Object[][] data_ = {};
  public Object[][] ndata_;

  public BuTable tableau_ = new BuTable(data_, titreColonnes_);
  public  DefaultTableModel modele_;

  /**
   * Panel ascenceur
   */
  public  JScrollPane ascenceur_;

  /**
   * Bordure du tableau
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  
  SiporDataSimulation donnees_;
  /**
   * constructeur du panel d'affichage des quais
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelAffichageQuais(final SiporDataSimulation d) {

    donnees_=d;
    //-- enregistrement de l'observer aupres de l'observable --//
    d.addObservers(this);
    
    setLayout(new BorderLayout());

    this.maj(d);

  }

  /**
   * Methode d'ajout d'une cellule
   */
  public  void maj(final SiporDataSimulation d) {

    ndata_ = new Object[d.getlQuais_().getlQuais_().size()][11];

    for (int i = 0; i < d.getlQuais_().getlQuais_().size(); i++) {
      final SiporQuais q = d.getlQuais_().retournerQuais(i);
      q.affichage();
      ndata_[i][0] = q.getNom();
      ndata_[i][1] = d.getListebassin_().retournerBassin(q.getBassin_()) ;
      ndata_[i][2] = "" + (float) q.getLongueur_();
      if (q.isDehalageAutorise_()) {
        ndata_[i][3] = "oui";
      } else {
        ndata_[i][3] = "non";
      }
      if (q.getTypeLoi_() == 0) {
        ndata_[i][4] = "Loi d'Erlang: " + Integer.toString(q.getLoiFrequence_());
      } else if (q.getTypeLoi_() == 1) {
        ndata_[i][4] = "Loi deterministe: ";
      } else if (q.getTypeLoi_() == 2) {
        ndata_[i][4] = "Loi journaliere: ";
      }

      if (q.getTypeLoi_() == 0) {
        ndata_[i][5] = "" + (float) q.getFrequenceMoyenne_();
      } else {
        ndata_[i][5] = "";
      }
      ndata_[i][6] = "" + (float) q.getDureeIndispo_();
      ndata_[i][7] = Integer.toString(q.getLoiIndispo_());

      ndata_[i][8] = "" + (float) q.getH_().semaineCreneau1HeureDep + " � " + (float) q.getH_().semaineCreneau1HeureArrivee;
      ndata_[i][9] = "" + (float) q.getH_().semaineCreneau2HeureDep + " � " + (float) q.getH_().semaineCreneau2HeureArrivee;;
      ndata_[i][10] = "" + (float) q.getH_().semaineCreneau3HeureDep + " � " + (float) q.getH_().semaineCreneau3HeureArrivee;;

    }

    this.tableau_ = new BuTable(ndata_, this.titreColonnes_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    
    for(int i=0; i<titreColonnes_.length;i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }
    
    tableau_.getTableHeader();
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tableau_.setBorder(this.borduretab_);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.add(tableau_, BorderLayout.CENTER);

    this.revalidate();
    this.updateUI();
  }

public void update(Observable o, Object arg) {
  if(arg.equals("bassin"))
    maj(donnees_);
}

}
