package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.SiporResource;
import org.fudaa.fudaa.sipor.structures.SiporBassin;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserBassins;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextField;

/**
 * Panel de saisie des dif�rents Bassins
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelSaisieBassin extends JPanel {

  // attributs:
  static int nbouverture = 0;
  /**
   * Jtext du nom a saisir
   */
  SiporTextField nom_ = new SiporTextField(10);

  /**
   * Bouton de validation du bassin
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * donn�es de la simulation
   */
  SiporDataSimulation donnees_;

  

  /**
   * Fenetre principale de gestion des bassins
   */

  SiporVisualiserBassins MENUBASSINS_;

  /**
   * Booleen qui indique si le panel est en mode modification MODE_MODIFICATION_ON=true =>>> mode modif sinon mode
   * saisie classique Par defaut r�gl� sur mode classique de saisie(booleen=false)
   */
  boolean MODE_MODIFICATION_ON_ = false;

  /**
   * Indice du bassin a modifier:
   */
  int BASSIN_A_MODIFIER_;

  /**
   * Constructeur du panel de saisie des bassins
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelSaisieBassin(final SiporDataSimulation d, final SiporVisualiserBassins vb) {

    // recuperation de la fenetre de commande principale
    MENUBASSINS_ = vb;
    donnees_ = d;

    nbouverture++;
    this.setLayout(new BorderLayout());

    this.getNom_().setText("Bassin " + (donnees_.getListebassin_().getListeBassins_().size() + 1));
    this.getNom_().setToolTipText("Saisissez le nom du bassin ici");

    validation_.setToolTipText("Cliquez sur ce bouton pour valider la saisie");

    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.println("validation du nom du bassin:");
        if (nom_.getText().equals("")) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le bassin n'as pas de nom.").activate();
        } else if (MODE_MODIFICATION_ON_ && donnees_.getListebassin_().existeDoublon(nom_.getText(), BASSIN_A_MODIFIER_)) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Le nom est d�j� utilis� par un autre bassin.").activate();
        } else if (!MODE_MODIFICATION_ON_ && donnees_.getListebassin_().existeDoublon(nom_.getText(), -1)) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur, Le nom est d�j� utilis� par un autre bassin.").activate();
        }

        else {
          //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            donnees_.baisserNiveauSecurite();
          /**
           * MODE SAISIE/
           */
          if (!MODE_MODIFICATION_ON_) {// r�cup�ration de la donn�e du bassin
            // FUDAA

            // 1)ajout de la chaine de caractere du bassin
            SiporBassin b = donnees_.getListebassin_().ajout(nom_.getText());
            
            
          //-- add element in network --//
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_BASSIN,
            		b);
            
            // 2)sauvegarde des donnees
            donnees_.enregistrer();

          } else {
            /***********************************************************************************************************
             * Mode MODIFICATION:
             */
            // recuperation des informatiosn des gares amonts et avals

            donnees_.getListebassin_().modification(BASSIN_A_MODIFIER_, nom_.getText());
            new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Bassin "
                + nom_.getText() + " correctement modifi�.").activate();
          }
          // 2)mise a jour dans le tableau
          MENUBASSINS_.affichagePanel.maj(donnees_);

          // 3)remise a zero des composants

          nom_.setText("Bassin " + (donnees_.getListebassin_().getListeBassins_().size() + 1));

          // 4)mode modification remis a false par defaut
          MODE_MODIFICATION_ON_ = false;
          MENUBASSINS_.mode_.setText("Saisie: ");
          validation_.setText("Valider");
          MENUBASSINS_.suppression_.setEnabled(true);
        }

      }

    });

    // architecture des composants
    final JPanel panneau = new JPanel();

    panneau.add(new JLabel(/*"Nom du bassin: "*/SiporResource.SIPOR.getString("Nom du bassin")));
    panneau.add(this.nom_);
    panneau.setBorder(SiporBordures.bordnormal_);
    this.add(panneau, BorderLayout.CENTER);

    this.add(validation_, BorderLayout.SOUTH);
    this.setBorder(SiporBordures.bordnormal_);

    // affichage:
  }

  /**
   * Methode qui transforme le type de la fenetre de saisie en fenetre de modification:
   * 
   * @param numBassin indice du tableau de bassin a modifier
   */

  public void MODE_MODIFICATION(final int numBassin) {

    MODE_MODIFICATION_ON_ = true;
    this.nom_.setText(this.donnees_.getListebassin_().retournerBassin(numBassin));
    BASSIN_A_MODIFIER_ = numBassin;
    this.MENUBASSINS_.mode_.setText("Modification: ");
    this.validation_.setText("modifier");
    this.MENUBASSINS_.suppression_.setEnabled(false);
  }

public SiporTextField getNom_() {
  return nom_;
}

public void setNom_(SiporTextField nom_) {
  this.nom_ = nom_;
}

}
