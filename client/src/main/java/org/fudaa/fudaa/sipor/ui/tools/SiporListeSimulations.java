/*
 * @file         SiporListeSimulations.java
 * @creation     1999-10-01
 * @modification $Date: 2007-07-31 16:16:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Component;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuEmptyList;
import com.memoire.fu.FuLib;

import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.MethodesUtiles;

/**
 * Liste g�rant l'ouverture de plusieurs simulations � la fois.
 * 
 * @version $Revision: 1.13 $ $Date: 2007-07-31 16:16:43 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class SiporListeSimulations extends BuEmptyList implements ListSelectionListener {
  private DefaultListModel model_;
  SiporImplementation sipor;

  public SiporListeSimulations(final BuCommonInterface appli) {
    super();
    sipor = (SiporImplementation) appli.getImplementation();
    setModel_(new DefaultListModel());
    setModel(getModel_());
    setEmptyText("Aucune simulation");
    setCellRenderer(new AfficheurNomsFichiers());
    addListSelectionListener(this);
  }

  /** Ajoute une simulation dans la liste. */
  public void ajouteSimulation(final String nomFichier, final FudaaProjet projet) {
    sipor.getProjets_().put(nomFichier, projet);
    if (!getModel_().contains(nomFichier)) {
      getModel_().addElement(nomFichier);
    }
    revalidate();
    repaint(200);
  }

  /** Retire une simulation de la liste. */
  public void enleveSimulation(final String nomFichier) {
    sipor.getProjets_().remove(nomFichier);
    getModel_().removeElement(nomFichier);
    revalidate();
    repaint(200);
  }

  /** Changement du projet en cours dans l'impl�mentation. */
  public void valueChanged(final ListSelectionEvent evt) {
    if (evt.getValueIsAdjusting()) {
      return;
    }
    
    final JList source = (JList) evt.getSource();
    // Effectue le changement de projet en cours.

    sipor.setProjet_((FudaaProjet) sipor.getProjets_().get(source.getSelectedValue()));
    sipor.outils_.setProjet(sipor.getProjet_());

    /**
     * modification du projet pour SiporDataSimulation
     */
    sipor.getDonnees_().changerProjet(sipor.getProjet_());

    sipor.setTitle(sipor.getProjet_().getFichier());
    sipor.activeActionsExploitation();
    // Mise � jour de fp_ et fermeture de fRappelDonnees.

    final BuDesktop dk = sipor.getMainPanel().getDesktop();
    if (sipor.getfRappelDonnees_() != null) {
      try {
        sipor.getfRappelDonnees_().setClosed(true);
        sipor.getfRappelDonnees_().setSelected(false);
      } catch (final PropertyVetoException ex) {}
    }
    if (sipor.getfStatistiquesFinales_() != null) {
      try {
        sipor.getfStatistiquesFinales_().setClosed(true);
        sipor.getfStatistiquesFinales_().setSelected(false);
      } catch (final PropertyVetoException ex) {}
      dk.removeInternalFrame(sipor.getfStatistiquesFinales_());
      sipor.setfStatistiquesFinales_(null);
    }

  }

  /** Pour enregistrer la liste des simulations ouvertes dans un fihcier .pro. */
  public void enregistre() {
    final String[] ext = { "pro" };
    String contenu = "";
    final Object[] tempo = getModel_().toArray();
    final String[] cles = new String[tempo.length];
    for (int i = 0; i < tempo.length; i++) {
      cles[i] = (String) tempo[i];
    }
    // v�rifie que toutes les simulations sont dans le meme repertoire
    String rpt = cles[0];
    // test si fichier du repertoire courant
    if (rpt.lastIndexOf(File.separator) == -1) {
      rpt = System.getProperty("user.dir") + File.separator + "exemples" + File.separator + "sipor" + File.separator;
    } else {
      rpt = rpt.substring(0, rpt.lastIndexOf(File.separator) + 1);
    }
    String cle = "";
    for (int i = 0; i < cles.length; i++) {
      cle = cles[i];
      // si chemin d'acces identique
      if ((cle.startsWith(rpt)) && ((cle.substring(rpt.length())).lastIndexOf(File.separator) == -1)) {
        contenu += cle.substring(rpt.length()) + "\n";
      } else if (rpt.equalsIgnoreCase(System.getProperty("user.dir") + File.separator + "exemples" + File.separator
          + "sipor" + File.separator)) {
        contenu += cle + "\n";
      } else {
        new BuDialogError(sipor.getApp(), SiporImplementation.INFORMATION_SOFT,
            "Les simulations doivent �tre dans le m�me r�pertoire").activate();
        return;
      }
    }
    // v�rifie que le fichier d'enregistrement de la liste se trouve dans le meme repertoire
    // que les simulations.
    String nomFichier = MethodesUtiles.choisirFichierEnregistrement(ext, (Component) sipor.getApp(), rpt);
    if (nomFichier == null) {
      return;
    }
    if (!nomFichier.substring(0, nomFichier.lastIndexOf(File.separator) + 1).equals(rpt)) {
      new BuDialogError(sipor.getApp(), SiporImplementation.INFORMATION_SOFT,
          "La liste doit �tre enregistr�e dans\nle m�me r�pertoire que les simulations").activate();
      return;
    }
    // ajoute l"extension si elle est manquante
    if (nomFichier.lastIndexOf(".pro") != (nomFichier.length() - 4)) {
      nomFichier += ".pro";
    }
    MethodesUtiles.enregistrer(nomFichier, contenu, sipor.getApp());
  }

  /** Pour ouvrir un ensemble de simulations (d�fini dans un fichier .pro). */
  public void ouvrir() {
    // Ferme les simulations ouvertes.
    final Object[] liste = getModel_().toArray();
    for (int i = 0; i < liste.length; i++) {
      setSelectedValue(liste[i], true);
      sipor.fermer();
    }
    final String[] ext = { "pro" };
    final String nomFichier = MethodesUtiles.choisirFichier(ext, (Component) sipor.getApp(), null, 1);
    if (nomFichier == null) {
      return;
    }
    final String rpt = nomFichier.substring(0, nomFichier.lastIndexOf(File.separator) + 1);
    final Vector simulations = new Vector();
    // fred si exception le flux ne sera pas frme
    BufferedReader lecteur = null;
    try {
      lecteur = new BufferedReader(new FileReader(nomFichier));
      String line = "";
      while ((line = lecteur.readLine()) != null) {
        simulations.add(rpt + line);
      }
      // dans le finally

    } catch (final IOException ex) {
      new BuDialogError(sipor.getApp(), SiporImplementation.INFORMATION_SOFT, "Lecture du fichier impossible");
    } finally {
      FuLib.safeClose(lecteur);
    }
    for (int i = 0; i < simulations.size(); i++) {
      final FudaaProjet projet = new FudaaProjet(sipor.getApp(), new FudaaFiltreFichier("sipor"));
      projet.setEnrResultats(true);
      projet.ouvrir((String) simulations.elementAt(i));
      if (projet.estConfigure()) {
        ajouteSimulation((String) simulations.elementAt(i), projet);
      }
    }
    if (sipor.getProjets_().size() > 0) {
      new BuDialogMessage(sipor.getApp(), SiporImplementation.INFORMATION_SOFT, "Fichiers charg�s").activate();
      setSelectedIndex(0);
      sipor.activerCommandesSimulation();
      sipor.activeActionsExploitation();
    }
  }

public DefaultListModel getModel_() {
	return model_;
}

public void setModel_(DefaultListModel model_) {
	this.model_ = model_;
}
}
