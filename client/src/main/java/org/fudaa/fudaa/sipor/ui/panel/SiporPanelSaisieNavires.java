package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuDialogWarning;
import com.memoire.fu.FuLog;

import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporChenal;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;
import org.fudaa.fudaa.sipor.structures.SiporNavire;
import org.fudaa.fudaa.sipor.structures.SiporQuais;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieHoraires;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieHorairesCompletSemaine;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieHorairesResume;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieLoiDeterministe;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieLoiJournaliere;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserNavires;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextField;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldDuree;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldFloat;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldInteger;

/**
 * Panel de saisie des Navire Utilise un systeme d'onglet pour pouvoir saisir la totalit des donnes relative aux navires
 * 
 * @author Adrien Hadoux
 */

public class SiporPanelSaisieNavires extends JPanel {


  /**
   * Donnees de la simulation
   */

  SiporDataSimulation donnees_;

  /**
   * Panel des Informations Gnrales
   */
  JPanel panelDonneesGenerales_ = new JPanel();

  static int nbouverture = 0;

  String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };

  String[] tabh_ = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
      "18", "19", "20", "21", "22", "23", "24" };
  String[] tabm_ = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
      "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36",
      "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55",
      "56", "57", "58", "59" };

  String[] listeHoraires;
  
  SiporFrameSaisieLoiJournaliere fenetreLoiJournaliere_ = null;
  SiporFrameSaisieLoiDeterministe fenetreLoideter_ = null;

  SiporTextField dgNom_ = new SiporTextField(10);

  /**
   * loi de generation des navires
   */
  String[] choixLoi_ = { "Erlang", "D�terministe", "Journali�re" };
  JComboBox choixLoiGenerationNav_ = new JComboBox(choixLoi_);

  // loi erlang
  SiporTextFieldInteger nbBateauxAttendus_ = new SiporTextFieldInteger(3);
  SiporTextFieldFloat ecartMoyenEntre2Arrivees_ = new SiporTextFieldFloat(3);
  JComboBox loiGenerationNavErlang_ = new JComboBox(tabloi_);
  /**
   * variable contenant le tableau des couples pour la loi deterministe
   */
  ArrayList loiDeterministe_ = new ArrayList();

  String[] choixPriorite_ = { "Normale", "Forte" };
  JComboBox dgPriorite_ = new JComboBox(choixPriorite_);

  JComboBox dgGareDepart = new JComboBox();;

  SiporTextFieldFloat dgLongMin_ = new SiporTextFieldFloat(3);
  SiporTextFieldFloat dgLongMoy_ = new SiporTextFieldFloat(3);
  SiporTextFieldFloat dgLargMin_ = new SiporTextFieldFloat(3);
  SiporTextFieldFloat dgLargMax_ = new SiporTextFieldFloat(3);

  SiporTextFieldFloat dgLongMax_ = new SiporTextFieldFloat(3);
  SiporTextFieldFloat dgTirantEntree_ = new SiporTextFieldFloat(3);
  SiporTextFieldFloat dgTirantSortie_ = new SiporTextFieldFloat(3);
  JButton dgSuivant_ = new JButton("Etape Suivante");

  // bordures
  final BuButton creneauG_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "");
  final BuButton creneauPM_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "");

  /*
   * ******************************************************************************************
   * Attributs Panel de la liste des quais
   * ****************************************************************************************** *
   */
  /**
   * Panel de la liste des quais accessibles par oredre preferentiel
   */
  JPanel panelListeQuais_ = new JPanel();

  // attributs quais pr�f�rentiels
  JComboBox lqQuaiPref1_;
  JComboBox lqQuaiPref2_;
  JComboBox lqQuaiPref3_;
 
 /* F9 - besoin avort� de dur�e pr�f�rentielle
  * SiporTextFieldDuree lqDuree1_ = new SiporTextFieldDuree(3);
  SiporTextFieldDuree lqDuree2_ = new SiporTextFieldDuree(3);
  SiporTextFieldDuree lqDuree3_ = new SiporTextFieldDuree(3);
  */
  /*
   * ****************************************************************************************** Attributs Panel des
   * operations de chargement dechargement
   * ****************************************************************************************** *
   */
  /**
   * Panel des operations de chargement, dchargement
   */

  JPanel panelOperationsCD_ = new JPanel();
  /**
   * Panels qui vont etre cre selon le choix de l'utilisateur
   */
  JInternalFrame modeLoi_, modeShift_;

  // attributs panel de chargement dechargement des Navires:
  JRadioButton cdShift = new JRadioButton("Mode Shift");
  JRadioButton cdLoi = new JRadioButton("Mode Loi de service");
  BuButton pshift_ = new BuButton("Param�tres...");
  BuButton ploi_ = new BuButton("Param�tres...");
  JLabel messNbaviresAttendus = new JLabel("Nb Navires attendus sur la dur�e de la simulation:");

  // attributs mode Shift
  JComboBox cdHoraires_ = new JComboBox();
  JCheckBox choixHorairesNavires_ = new JCheckBox("");
  JButton cdRevoirHoraires_ = new JButton("?");
  JComboBox cdDureeProchainShiftheure_ = new JComboBox(tabh_);
  JComboBox cdDureeProchainShiftminute_ = new JComboBox(tabm_);

  SiporTextFieldDuree cdDureeProchainShift = new SiporTextFieldDuree(3);
  SiporTextFieldFloat cdTonnageMin_ = new SiporTextFieldFloat(5);
  SiporTextFieldFloat cdTonnageMax_ = new SiporTextFieldFloat(5);
  // JComboBox cdLoiVarTonnage_ = new JComboBox(tabloi_);
  SiporTextFieldFloat cdCadenceQuaiPref_ = new SiporTextFieldFloat(4);
  SiporTextFieldFloat cdCadenceAutreQuai_ = new SiporTextFieldFloat(4);

  final BuButton cdvalidationShift_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  final BuButton cdvalidationLoi_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  final BuButton creneau_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "Saisir horaire");

  SiporTextFieldDuree cdDureeServiceMinQuaiPref_ = new SiporTextFieldDuree(4);
  SiporTextFieldDuree cdDureeServiceMaxQuaiPref_ = new SiporTextFieldDuree(4);
  SiporTextFieldDuree cdDureeServiceMinAutreQuai_ = new SiporTextFieldDuree(4);
  SiporTextFieldDuree cdDureeServiceMaxAutreQuai_ = new SiporTextFieldDuree(4);

  SiporTextFieldDuree dureeAttenteMaxAdmissible_ = new SiporTextFieldDuree(3);
  
  /**
   * Bouton de validation de la saisie de la cagorie de navire
   */

  final BuButton validerNavire_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  String chtemp_;

  /**
   * Panel principal de gestion des navires c est a partir de ce panel que l on peut effectuer toutes les operations de
   * mise a jour d affichage par exemple.
   */
  SiporVisualiserNavires MENUNAVIRES_;

  /**
   * BOOLEEN qui precise si le panel de saisie d'une cat�gorie de navire est en mode ajout ou en mode modification si
   * il est en mode modification: le booleen est a true sinon il est a false par defaut le mode modification est a false
   * donc en mode saisie classique
   */
  boolean UPDATE = false;

  /**
   * indice dans la liste des quais du quai a modifier dans le cas ou l on est en mode modificaiton
   */
  int NAVIRE_A_MODIFIER_;

  /**
   * Horaire qui sera saisie par l'interface de saisie des horaires
   */
  SiporHoraire horaire_ = new SiporHoraire();

  SiporHoraire horaireG_ = new SiporHoraire();
  SiporHoraire horairePM_ = new SiporHoraire();

  Color vert = new Color(241, 241, 241);
  Color rouge = new Color(157, 194, 143);

  private boolean premierAvertissment = true;

  /**
   * Constructeur du Panel de saisie:
   */
  public SiporPanelSaisieNavires(final SiporDataSimulation d, final SiporVisualiserNavires vn) {

    // recuperation des donnees de la simulation
    donnees_ = d;

    MENUNAVIRES_ = vn;

    nbouverture++;

    /*
     * ************************************* Creation des controles des donnes gnrales
     * **************************************
     */

    dgNom_.setToolTipText("Veuillez entrer le nom qui correspond � la catgorie de Navires");

    this.dgPriorite_.setToolTipText("Veuillez choisir le degr� de priorit� de cette cat�gorie de navire");


    this.dgLongMin_.setToolTipText("Nombre entier correspondant � la taille minimale de cette cat�gorie");

    this.dgLongMax_
        .setToolTipText("Nombre r�el correspondant � la taille maximale de cette cat�gorie.\nAttention: doit �tre sup�rieur � la taille minimale.");
    this.dgLargMin_.setToolTipText("Nombre entier correspondant � la largeur minimale de cette cat�gorie.");

    this.dgLargMax_
        .setToolTipText("Nombre r�el correspondant � la largeur maximale de cette cat�gorie.\nAttention: doit �tre sup�rieur � la taille minimale.");

    this.dgTirantEntree_.setToolTipText("Nombre r�el en m�tres correspondant au tirant d'eau en entr�e dans le port.");

    this.dgTirantSortie_.setToolTipText("Nombre r�el en m�tres correspondant au tirant d'eau en sortie du port.");


    // Panel de gestion des donnes de Quai preferentiel:
    this.lqQuaiPref1_ = new JComboBox();
    this.lqQuaiPref1_.setToolTipText("Choisissez le quai pr�f�rentiel ou Aucun si c'est le cas");
    this.lqQuaiPref2_ = new JComboBox();
    this.lqQuaiPref2_.setToolTipText("Choisissez le quai pr�f�rentiel ou Aucun si c'est le cas");
    this.lqQuaiPref3_ = new JComboBox();
    this.lqQuaiPref3_.setToolTipText("Choisissez le quai pr�f�rentiel ou Aucun si c'est le cas");
    // mise a jour de la saisie pour le remplissage des donnes
    this.majSaisie();
/*F9 
    this.lqDuree1_
        .setToolTipText("Nombre r�el en heures.minutes correspondant � la dur�e pendant laquelle ce quai reste pr�f�rentiel num�ro 1. Exemple: pour entrer 8h36, tapez 8.36");

    this.lqDuree2_
        .setToolTipText("Nombre r�el en heures.minutes correspondant � la dur�e pendant laquelle ce quai reste pr�f�rentiel num�ro 2. Exemple: pour entrer 8h36, tapez 8.36");

    this.lqDuree3_
        .setToolTipText("Nombre r�el en heures.minutes correspondant � la dur�e pendant laquelle ce quai reste pr�f�rentiel num�ro 3. Exemple: pour entrer 8h36, tapez 8.36");
*/

    // ajout des parametres des quais pr�f�rentiels:

    /*
     * ****************************************************************** Creation des controles des oprations de
     * chargement, dchargement *******************************************************************
     */

    // les contrles des differents boites de saisie

    this.cdDureeProchainShift.setToolTipText("temps residuel de traitement avant transfert au shift suivant");

    // le tonnage:
    this.cdTonnageMin_
        .setToolTipText("Nombre r�el en tonnes correspondant au tonnage minimum. Exemple: pour rentrer 8000 tonnes, tapez 8000");

    this.cdTonnageMax_
        .setToolTipText("Nombre r�el en tonnes correspondant au tonnage maximum. Exemple: pour rentrer 8000 tonnes, tapez 8000");

    this.cdCadenceQuaiPref_
        .setToolTipText("Nombre r�el correspondant � la cadence pr�f�rentielle en tonnes par heure T/H.");

    this.cdCadenceAutreQuai_
        .setToolTipText("Nombre r�el correspondant � la cadence pr�f�rentielle en tonnes par heure T/H.");

    this.cdHoraires_.setToolTipText("L'ensemble des horaires saisis pr�c�demment sont r�pertori�s ici");
    this.cdDureeProchainShiftheure_.setToolTipText("choisissez la dur�e d'attente avant le prochain Shift");
    this.cdDureeProchainShiftminute_.setToolTipText("choisissez la dur�e d'attente avant le prochain Shift");

    this.cdvalidationShift_.setToolTipText("Validez les valeurs saisies via ce bouton");

    this.cdDureeServiceMinQuaiPref_
        .setToolTipText("Nombre r�el correspondant � la dur�e de service minimum du quai pr�f�rentiel.");

    this.cdDureeServiceMaxQuaiPref_
        .setToolTipText("Nombre r�el correspondant � la dur�e de service maximum du quai pr�f�rentiel.");

    this.cdDureeServiceMaxAutreQuai_
        .setToolTipText("Nombre r�el correspondant � la dur�e de service maximum des autres quais.");

    this.cdDureeServiceMinAutreQuai_
        .setToolTipText("Nombre r�el correspondant � la dur�e de service minimum des autres quais.");

    // bouton de validation de la fenetre du mode Shift
    this.cdvalidationShift_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        modeShift_.dispose();
      }
    });

    // bouton de validation de la fenetre du mode Loi de proba
    this.cdvalidationLoi_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        modeLoi_.dispose();
      }
    });

    this.validerNavire_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // lance la mthode de contrle de creation de Navire
        creationNavire();

      }
    });
    creneau_.setToolTipText("saisissez les horaires en cliquant sur ce bouton");

    this.creneau_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        donnees_.getApplication().addInternalFrame(new SiporFrameSaisieHorairesCompletSemaine(horaire_));
      }
    });

    creneauG_.setToolTipText("Saisissez les cr�neaux journaliers en cliquant sur ce bouton");

    this.creneauG_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        donnees_.getApplication().addInternalFrame(new SiporFrameSaisieHorairesResume(horaireG_));
      }
    });

    creneauPM_
        .setToolTipText("Saisissez les cr�neaux d'ouverture par rapport � la pleine mer, en cliquant sur ce bouton");

    this.creneauPM_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        donnees_.getApplication().addInternalFrame(
            new SiporFrameSaisieHoraires(horairePM_, donnees_.getParams_().maree.periodeMaree));
      }
    });

    choixLoiGenerationNav_.setToolTipText("Veuillez choisir le type de loi de g�n�ration de navires.");
    this.choixLoiGenerationNav_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        final int choixLoi = choixLoiGenerationNav_.getSelectedIndex();

        if (choixLoi == 0) {
          // Cas 0: loi d erlang
          messNbaviresAttendus.setForeground(Color.RED);
          nbBateauxAttendus_.setEnabled(true);
          // ecartMoyenEntre2Arrivees_.setEnabled(true);
          loiGenerationNavErlang_.setEnabled(true);

        } else if (choixLoi == 1) {
          // cas 1: loi deterministe
          messNbaviresAttendus.setForeground(Color.BLACK);

          nbBateauxAttendus_.setEnabled(false);
          // ecartMoyenEntre2Arrivees_.setEnabled(false);
          loiGenerationNavErlang_.setEnabled(false);

          if (fenetreLoideter_ == null) {
            FuLog.debug("interface nulle");
            fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, null, 
            		"<html><body>format de saisie de l'horaire: <bold>HH:mm</bold> <br />  </body></html> ",
          		  new Dimension(500, 300));

            // System.out.println("55555");
            fenetreLoideter_.setVisible(true);
            donnees_.getApplication().addInternalFrame(fenetreLoideter_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoideter_.isClosed()) {
              donnees_.getApplication().addInternalFrame(fenetreLoideter_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");
              donnees_.getApplication().activateInternalFrame(fenetreLoideter_);
              donnees_.getApplication().addInternalFrame(fenetreLoideter_);

            }
          }

        } else if (choixLoi == 2) {

          // cas 2: loi journaliere
          messNbaviresAttendus.setForeground(Color.BLACK);
          nbBateauxAttendus_.setEnabled(false);
          // ecartMoyenEntre2Arrivees_.setEnabled(false);
          loiGenerationNavErlang_.setEnabled(false);

          if (fenetreLoiJournaliere_ == null) {
            FuLog.debug("interface nulle");
            fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, null, 
            		"<html><body>format de saisie de l'horaire: <bold>HH:mm</bold> </body></html> ",
          		  new Dimension(500, 300));

            // System.out.println("55555");
            fenetreLoiJournaliere_.setVisible(true);
            donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoiJournaliere_.isClosed()) {
              donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");
              donnees_.getApplication().activateInternalFrame(fenetreLoiJournaliere_);
              donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);

            }
          }

        }

      }

    });

    ButtonGroup group = new ButtonGroup();
    group.add(cdShift);
    group.add(cdLoi);

    cdShift.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        ploi_.setEnabled(false);
        pshift_.setEnabled(true);
        modeShift_.setVisible(true);
        donnees_.getApplication().addInternalFrame(modeShift_);

      }
    });
    cdLoi.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        ploi_.setEnabled(true);
        pshift_.setEnabled(false);
        modeLoi_.setVisible(true);
        donnees_.getApplication().addInternalFrame(modeLoi_);

      }
    });
    pshift_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        modeShift_.setVisible(true);
        donnees_.getApplication().addInternalFrame(modeShift_);

      }
    });
    ploi_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        modeLoi_.setVisible(true);
        donnees_.getApplication().addInternalFrame(modeLoi_);

      }
    });

    choixHorairesNavires_.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        if (choixHorairesNavires_.isSelected()) cdHoraires_.setEnabled(true);
        else cdHoraires_.setEnabled(false);
      }

    });
    cdHoraires_.setEnabled(false);

    /**
     * ******************************************************************************************** ARCHITECTURE DU
     * PANEL DE DONNEES GENERALES
     * ********************************************************************************************
     */
    final JPanel total = new JPanel();

    this.setBorder(SiporBordures.navire);
    total.setLayout(new BorderLayout());

    // panel g�n�ral
    this.panelDonneesGenerales_.setLayout(new GridLayout(2, 1));

    final JPanel pdg1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    pdg1.add(new JLabel("Nom de la cat�gorie: "));
    pdg1.add(this.dgNom_);
    pdg1.add(new JLabel("Priorit�: "));
    pdg1.add(this.dgPriorite_);
    pdg1.add(new JLabel("Gare d�part: "));
    pdg1.add(this.dgGareDepart);
    this.panelDonneesGenerales_.add(pdg1);
    
    JPanel pdg2 =  new JPanel(new FlowLayout(FlowLayout.CENTER));
    pdg2.add(new JLabel("dur�e d�attente maximale admissible (H.Min): "));
    pdg2.add(this.dureeAttenteMaxAdmissible_);
    this.panelDonneesGenerales_.add(pdg2);

    panelDonneesGenerales_.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "G�n�ral"));
    total.add(this.panelDonneesGenerales_, BorderLayout.NORTH);

    // panel du centre
    // JPanel centre=new JPanel(new GridLayout(5,1));
    Box centre = Box.createVerticalBox();

    // centre.setBorder(this.SiporBordures.compound_);
    total.add(centre, BorderLayout.CENTER);

    // panel dimensions
    JPanel panelDimension = new JPanel(new FlowLayout(FlowLayout.CENTER));
    panelDimension.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Dimensions"));
    centre.add(panelDimension);

    Box boxLongueurs = Box.createVerticalBox();
    boxLongueurs.setBorder(BorderFactory.createTitledBorder(SiporBordures.bordnormal_, "Longueurs", 0, 0, null,
        SiporImplementation.bleuClairSipor));
    panelDimension.add(boxLongueurs);

    final JPanel pdg21 = new JPanel();
    pdg21.add(new JLabel("Minimum: "));
    pdg21.add(this.dgLongMin_);
    pdg21.add(new JLabel("M�tres  "));
    boxLongueurs.add(pdg21);
    final JPanel pdg22 = new JPanel();
    pdg22.add(new JLabel("Maximum: "));
    pdg22.add(this.dgLongMax_);
    pdg22.add(new JLabel("M�tres  "));
    boxLongueurs.add(pdg22);

    Box boxLargeurs = Box.createVerticalBox();
    boxLargeurs.setBorder(BorderFactory.createTitledBorder(SiporBordures.bordnormal_, "Largeurs", 0, 0, null,
        SiporImplementation.bleuClairSipor));
    panelDimension.add(boxLargeurs);

    final JPanel pdg31 = new JPanel();
    pdg31.add(new JLabel("Minimum: "));
    pdg31.add(this.dgLargMin_);
    pdg31.add(new JLabel("M�tres  "));
    boxLargeurs.add(pdg31);
    final JPanel pdg32 = new JPanel();
    pdg32.add(new JLabel("Maximum: "));
    pdg32.add(this.dgLargMax_);
    pdg32.add(new JLabel("M�tres  "));
    boxLargeurs.add(pdg32);

    // Tirant eau + quai
    JPanel paneltirantQuai = new JPanel(new GridLayout(2, 1));
    centre.add(paneltirantQuai);

    JPanel tirantEau = new JPanel(new FlowLayout(FlowLayout.CENTER));
    tirantEau.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Tirant d'eau"));
    paneltirantQuai.add(tirantEau);

    final JPanel plqa = new JPanel();
    plqa.add(new JLabel("Entr�e: "));
    plqa.add(this.dgTirantEntree_);
    plqa.add(new JLabel("M�tres"));
    plqa.setBorder(SiporBordures.bordnormal_);
    tirantEau.add(plqa);

    final JPanel plqb = new JPanel();
    plqb.add(new JLabel("Sortie: "));
    plqb.add(this.dgTirantSortie_);
    plqb.add(new JLabel("M�tres"));
    plqb.setBorder(SiporBordures.bordnormal_);
    tirantEau.add(plqb);

    JPanel Quai = new JPanel(new FlowLayout(FlowLayout.CENTER));
    Quai.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Quais pr�f�rentiels"));
    paneltirantQuai.add(Quai);
    
    final JPanel plq11 = new JPanel(new GridLayout(1,2));
   
    plq11.add(new JLabel("Quai 1:"));
    plq11.add(lqQuaiPref1_);
   /*F9
    plq11.add(new JLabel("Dur�e (hh.mm):"));
    plq11.add(lqDuree1_);
     */
    plq11.setBorder(SiporBordures.bordnormal_);
    Quai.add(plq11);

    final JPanel plq21 = new JPanel(new GridLayout(1,2));
    
    plq21.add(new JLabel("Quai 2:"));
    plq21.add(lqQuaiPref2_);
    /*F9
    plq21.add(new JLabel("Dur�e (hh.mm):"));
    plq21.add(lqDuree2_);
    */
    plq21.setBorder(SiporBordures.bordnormal_);
    Quai.add(plq21);
	
    final JPanel plq31 = new JPanel(new GridLayout(1,2));
   
    plq31.add(new JLabel("Quai 3:"));
    plq31.add(lqQuaiPref3_);
   /*F9
    plq31.add(new JLabel("Dur�e (hh.mm):"));
    plq31.add(lqDuree3_);
    */
    plq31.setBorder(SiporBordures.bordnormal_);
    Quai.add(plq31);

    JPanel creneauCharge = new JPanel(new GridLayout(1, 2));
    centre.add(creneauCharge);

    // panel des creneaux horaires
    Box panelCreneaux = Box.createVerticalBox();
    panelCreneaux.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Cr�neaux"));
    creneauCharge.add(panelCreneaux);
    final JPanel plq771 = new JPanel();
    plq771.add(new JLabel("Cr�neaux horaire d'acc�s"));
    plq771.add(this.creneauG_);
    plq771.setBorder(SiporBordures.bordnormal_);
    panelCreneaux.add(plq771);
    final JPanel plq772 = new JPanel();
    plq772.add(new JLabel("Cr�neaux Pleine Mer"));
    plq772.add(this.creneauPM_);
    plq772.setBorder(SiporBordures.bordnormal_);
    panelCreneaux.add(plq772);

    // panel chargement dechargement
    JPanel panelCharge = new JPanel(new FlowLayout(FlowLayout.LEFT));
    panelCharge.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Mode chargement/d�chargement"));
    creneauCharge.add(panelCharge);

    Box boxcd = Box.createVerticalBox();
    panelCharge.add(boxcd);

    JPanel pshift = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pshift.add(cdShift);
    pshift.add(pshift_);
    boxcd.add(pshift);
    JPanel ploi = new JPanel(new FlowLayout(FlowLayout.LEFT));
    ploi.add(cdLoi);
    ploi.add(ploi_);
    boxcd.add(ploi);

    // Loi generation de navires
    JPanel panelLoigeneration = new JPanel(new GridLayout(2, 1));
    panelLoigeneration
        .setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Loi g�n�ration de navires"));
    centre.add(panelLoigeneration);

    final JPanel mscdtype3 = new JPanel();
    mscdtype3.add(new JLabel("Type de loi:"));
    mscdtype3.add(this.choixLoiGenerationNav_);
    mscdtype3.add(new JLabel("Ordre loi d'Erlang:"));
    mscdtype3.add(this.loiGenerationNavErlang_);
    panelLoigeneration.add(mscdtype3);

    final JPanel mscdtype4 = new JPanel();
    // mscdtype4.add(new JLabel("ecart moyen entre 2 arrivees:"));
    // mscdtype4.add(this.ecartMoyenEntre2Arrivees_);
    messNbaviresAttendus = new JLabel("Nb Navires attendus sur la dur�e de la simulation:");
    messNbaviresAttendus.setForeground(Color.RED);
    mscdtype4.add(messNbaviresAttendus);
    mscdtype4.add(this.nbBateauxAttendus_);
    panelLoigeneration.add(mscdtype4);

    JPanel panelValidation = new JPanel(new FlowLayout(FlowLayout.CENTER));
    centre.add(panelValidation);
    panelValidation.setBorder(SiporBordures.compound_);
    panelValidation.add(this.validerNavire_);

    /**
     * ************************************************************************ Fenetre g�n�r�e par le mode shift
     * ************************************************************************
     */

    // controles de la saisie des operations de chargement dechargement
    modeShift_ = new JInternalFrame("", true, true, true, true);
    modeShift_.setBorder(SiporBordures.compound_);
    // frame contenant les donnes du mode Shift
    modeShift_.setSize(500, 340);
    modeShift_.setTitle("Mode de chargement d�chargement Shift");
    modeShift_.getContentPane().setLayout(new BorderLayout());

    // donnees generales
    final JPanel msD = new JPanel();
    msD.add(new JLabel("Dur�e avant Shift suivant:"));
    msD.add(this.cdDureeProchainShift);
    msD.add(new JLabel("H.MIN"));
    msD.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "G�n�ral"));
    modeShift_.getContentPane().add(msD, BorderLayout.NORTH);

    // panel validation
    final JPanel msL = new JPanel();
    msL.add(cdvalidationShift_);
    msL.setBorder(SiporBordures.compound_);
    modeShift_.getContentPane().add(msL, BorderLayout.SOUTH);

    Box mscentre = Box.createVerticalBox();
    mscentre.setBorder(SiporBordures.compound_);
    modeShift_.getContentPane().add(mscentre, BorderLayout.CENTER);

    // panel des horaires
    JPanel cdsh = new JPanel(new GridLayout(2, 1));
    cdsh.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Horaire de travail"));
    mscentre.add(cdsh);

    final JPanel mscentre1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    mscentre1.add(new JLabel("Saisir:"));
    mscentre1.add(this.creneau_);
    cdsh.add(mscentre1);

    final JPanel mscentre2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    mscentre2.add(new JLabel("Ou choisir dans la liste:"));
    mscentre2.add(choixHorairesNavires_);
    mscentre2.add(this.cdHoraires_);
    cdsh.add(mscentre2);

    JPanel cadenceTonnage = new JPanel(new GridLayout(1, 2));
    mscentre.add(cadenceTonnage);

    final JPanel msT = new JPanel(new GridLayout(2, 1));
    msT.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Tonnage"));
    cadenceTonnage.add(msT);

    final JPanel msT11 = new JPanel();
    msT11.add(new JLabel("Min:"));
    msT11.add(this.cdTonnageMin_);
    msT11.add(new JLabel("Tonnes"));
    msT11.setBorder(SiporBordures.bordnormal_);
    msT.add(msT11);

    final JPanel msT12 = new JPanel();
    msT12.add(new JLabel("Max:"));
    msT12.add(this.cdTonnageMax_);
    msT12.add(new JLabel("Tonnes"));
    msT12.setBorder(SiporBordures.bordnormal_);
    msT.add(msT12);

    final JPanel msC = new JPanel(new GridLayout(2, 1));
    msC.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Cadence aux quais"));
    cadenceTonnage.add(msC);

    final JPanel msC11 = new JPanel();
    msC11.add(new JLabel("Quai pr�f�rentiel:"));
    msC11.add(this.cdCadenceQuaiPref_);
    msC11.add(new JLabel("T/H"));
    msC11.setBorder(SiporBordures.bordnormal_);
    msC.add(msC11);

    final JPanel msC12 = new JPanel();
    msC12.add(new JLabel("Autres quais:"));
    msC12.add(this.cdCadenceAutreQuai_);
    msC12.add(new JLabel("T/H"));
    msC12.setBorder(SiporBordures.bordnormal_);
    msC.add(msC12);

    /**
     * ************************************************************************ Fenetre generee par le mode loi
     * ************************************************************************
     */

    // panel contenant les donnes du mode loi
    modeLoi_ = new JInternalFrame("", true, true, true, true);
    modeLoi_.setTitle("Mode de chargement loi de probabilit�");
    modeLoi_.setSize(450, 260);

    // ajout des composants dans la JFrame
    final JPanel ml = new JPanel();
    ml.setLayout(new BorderLayout());
    ml.setBorder(SiporBordures.bordnormal_);

    final JPanel centreml = new JPanel();

    centreml.setLayout(new GridLayout(2, 1));
    centreml.setBorder(SiporBordures.bordnormal_);
    ml.add(centreml, BorderLayout.CENTER);

    Box box1 = Box.createVerticalBox();
    JPanel ml2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    ml2.add(new JLabel("Minimum "));
    ml2.add(this.cdDureeServiceMinQuaiPref_);
    ml2.add(new JLabel("Heures.minutes"));
    JPanel ml22 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    ml22.add(new JLabel("Maximum "));
    ml22.add(this.cdDureeServiceMaxQuaiPref_);
    ml22.add(new JLabel("Heures.minutes"));
    box1.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Dur�e de service au quai pr�f�rentiel"));

    box1.add(ml2);
    box1.add(ml22);
    centreml.add(box1);

    Box box2 = Box.createVerticalBox();
    final JPanel ml4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    ml4.add(new JLabel("Minimum"));
    ml4.add(this.cdDureeServiceMinAutreQuai_);
    ml4.add(new JLabel("Heures.minutes"));
    final JPanel ml44 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    ml44.add(new JLabel("Maximum"));
    ml44.add(this.cdDureeServiceMaxAutreQuai_);
    ml44.add(new JLabel("Heures.minutes"));
    box2.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Dur�e de service aux autres quais"));
    box2.add(ml4);
    box2.add(ml44);
    centreml.add(box2);

    final JPanel pval = new JPanel();
    pval.add(new JLabel("Validez la saisie: "));
    pval.add(cdvalidationLoi_);
    pval.setBorder(SiporBordures.compound_);

    ml.add(pval, BorderLayout.SOUTH);
    ml.setBorder(SiporBordures.compound_);
    modeLoi_.getContentPane().add(ml);
    modeLoi_.setBorder(SiporBordures.compound_);

    this.add(total);

    this.initialiser();

    this.dgNom_.requestFocus();
    this.dgNom_.selectAll();
  }

  /**
   * Methode tres importante de mise a jour des donnes importantes telle que la liste des quais METHODE TRES PUISSANTE
   * ET IMPORTANTE SI MODIFICATION DE AUTRE PARAMETRES ILS SERONT REMIS A JOUR POUR LA SAISIE DES NAVIRES VIA CETTE
   * INTERFACE!!
   */

  public void majSaisie() {
    // remplissage des gares dans le jcombobox

    this.dgGareDepart.removeAllItems();

    for (int i = 0; i < this.donnees_.getListeGare_().getListeGares_().size(); i++) {
      this.dgGareDepart.addItem(this.donnees_.getListeGare_().retournerGare(i));
    }

    /**
     * Construction de la liste des horaires deja saisies et repertori�s dans datasimulation
     */

    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      if (i >= this.cdHoraires_.getItemCount()
          && donnees_.getCategoriesNavires_().retournerNavire(i).getModeChargement() == 0) {
        this.cdHoraires_.addItem("Horaire " + (donnees_.getCategoriesNavires_().retournerNavire(i)).getNom());
      }
    }

    // this.cdHoraires_=new JComboBox(listeHoraires);
    this.cdHoraires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if (cdHoraires_.getItemCount() != 0) {
          // modification de l horaire actuel du panel par celui du vecteur
          final int var = cdHoraires_.getSelectedIndex();

          // si ce n est pas le champs vide selectionn�
          // ATTENZIOOOOOONNNNNNNNNE
          // ON RECOPIE LE HORAIRE POUR PAS FAIRE ERREUR DE REFERENZZZZZE
          // CAR ZINON TOUT VA EXPLOZER!!! HIIIIMMMMMMMMEL
          // hum en fait si on recoit la reference et qu on modifie le
          // nouvel horaire, �a aura pour consequence de modifier l'horaire du navire selectionn� ce qui poserais
          // eine grosse probleme...
          horaire_.recopie((SiporHoraire) donnees_.getCategoriesNavires_().retournerNavire(var)
              .getHoraireTravailSuivi());


        }

      }

    });

    // mise a jour de la liste des quais pr�f�rentiels dans les comboBox
    this.lqQuaiPref1_.removeAllItems();
    this.lqQuaiPref2_.removeAllItems();
    this.lqQuaiPref3_.removeAllItems();
    this.lqQuaiPref2_.addItem("aucun");
    this.lqQuaiPref3_.addItem("aucun");

    for (int i = 0; i < this.donnees_.getlQuais_().getlQuais_().size(); i++) {
      this.lqQuaiPref1_.addItem(((SiporQuais) this.donnees_.getlQuais_().getlQuais_().get(i)).getNom());
      this.lqQuaiPref2_.addItem(((SiporQuais) this.donnees_.getlQuais_().getlQuais_().get(i)).getNom());
      this.lqQuaiPref3_.addItem(((SiporQuais) this.donnees_.getlQuais_().getlQuais_().get(i)).getNom());
    }
    this.lqQuaiPref1_.validate();
    this.lqQuaiPref2_.validate();
    this.lqQuaiPref3_.validate();

  }

  /**
   * Methode qui controle toutes les donnes saisies pour dfinir une nouvelle catgorie de Navire
   */

  public boolean controle_creationNavire() {

    if (dgNom_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "Nom manquant.")
          .activate();
      return false;
    } else if (this.UPDATE
        && donnees_.getCategoriesNavires_().existeDoublon(dgNom_.getText(), this.NAVIRE_A_MODIFIER_)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "Nom d�j� pris.")
          .activate();
      return false;
    } else if (!this.UPDATE && donnees_.getCategoriesNavires_().existeDoublon(dgNom_.getText(), -1)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "Nom d�j� pris.")
          .activate();
      return false;
    }

    if (choixLoiGenerationNav_.getSelectedIndex() == 0) {
      if (this.nbBateauxAttendus_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Le nombre de bateaux attendus est manquant.").activate();
        return false;

      }
    }

    if (this.dgLongMin_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Longueur Minimum manquante.").activate();
      return false;

    } else if (this.dgLongMax_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Longueur maximum manquante.").activate();
      return false;
    } else {
      float a = Float.parseFloat(this.dgLongMin_.getText());
      float b = Float.parseFloat(this.dgLongMax_.getText());
      if (b < a) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Longueur maximum inf�rieure � la longueur minimum.").activate();
        return false;
      }
    }
    if (this.dgLargMax_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Largeur maximum manquante.").activate();
      return false;
    } else if (this.dgLargMin_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Largeur minimum manquante.").activate();
      return false;
    } else {

      float a = Float.parseFloat(this.dgLargMin_.getText());
      float b = Float.parseFloat(this.dgLargMax_.getText());
      if (b < a) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Largeur maximum est inf�rieure � la longueur minimum").activate();
        return false;
      }

    }

    if (this.dgTirantEntree_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Tirant d'eau en entr�e manquant").activate();
      return false;
    }
    if (this.dgTirantSortie_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Tirant d'eau en sortie manquant.").activate();
      return false;
    }
   
    // controle de la saisie des horaires:
    if (this.horaireG_.semaineCreneau1HeureArrivee == -1 || this.horaireG_.semaineCreneau1HeureDep == -1
        || this.horaireG_.semaineCreneau2HeureArrivee == -1 || this.horaireG_.semaineCreneau2HeureDep == -1) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Cr�neaux journaliers non saisis correctement").activate();

      return false;

    }

    if (this.horairePM_.semaineCreneau1HeureArrivee == -1 && this.horairePM_.semaineCreneau1HeureDep == -1
        && this.horairePM_.semaineCreneau2HeureArrivee == -1 && this.horairePM_.semaineCreneau2HeureDep == -1) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Cr�neaux d'ouverture par rapport � la pleine mer non saisis correctement").activate();

      return false;

    }

    // cas mode shift
    if (this.cdShift.isSelected()) {

      if (this.cdDureeProchainShift.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre Shift: dur�e avant shift suivant manquante").activate();
        donnees_.getApplication().addInternalFrame(modeShift_);
        return false;
      }

      if (this.cdTonnageMin_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre Shift: Tonnage minimum manquant.").activate();
        donnees_.getApplication().addInternalFrame(modeShift_);
        return false;
      } else

      if (this.cdTonnageMax_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre Shift: Tonnage maximum manquant.").activate();
        donnees_.getApplication().addInternalFrame(modeShift_);
        return false;
      } else {
        float a = Float.parseFloat(this.cdTonnageMin_.getText());
        float b = Float.parseFloat(this.cdTonnageMax_.getText());
        if (b < a) {
          new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
              "Fen�tre Shift: Tonnage maximum est inf�rieur au tonnage minimum.").activate();
          donnees_.getApplication().addInternalFrame(modeShift_);
          return false;
        }
      }

      if (this.cdCadenceQuaiPref_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre Shift: Cadence quai pr�f�rentiel manquante.").activate();
        donnees_.getApplication().addInternalFrame(modeShift_);
        return false;
      }
      if (this.cdCadenceAutreQuai_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre Shift: Cadence autres quais manquante.").activate();
        donnees_.getApplication().addInternalFrame(modeShift_);
        return false;
      }

      if (premierAvertissment) {
        float cad1 = Float.parseFloat(cdCadenceAutreQuai_.getText());
        float cad2 = Float.parseFloat(cdCadenceQuaiPref_.getText());
        if (cad2 < cad1) {
          new BuDialogWarning(
              donnees_.getApplication().getApp(),
              donnees_.getApplication().INFORMATION_SOFT,
              "Attention: \nLa cadence au quai pr�f�rentiel " +
              "est inf�rieure\n � la cadence aux autres quais.\n" +
              "Ceci n'est pas une erreur mais un avertissement.\n" +
              "Cliquez de nouveau sur le bouton de validation pour valider")
              .activate();
          premierAvertissment = false;
          donnees_.getApplication().addInternalFrame(modeShift_);
          return false;

        }
      }
      // controle de la saisie des horaires:
      if ((!choixHorairesNavires_.isSelected())
          && (this.horaire_.lundiCreneau1HeureArrivee == -1 || this.horaire_.lundiCreneau1HeureDep == -1
              || this.horaire_.lundiCreneau2HeureArrivee == -1 || this.horaire_.lundiCreneau2HeureDep == -1)) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Cr�neaux du mode shift non saisis correctement.").activate();
        donnees_.getApplication().addInternalFrame(modeShift_);

        return false;

      }

    } else// cas mode loi
    {

      if (this.cdDureeServiceMinQuaiPref_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre chargement / loi de probabilit�: \nDur�e de service minimum quai pr�f�rentiel manquant.")
            .activate();
        donnees_.getApplication().addInternalFrame(modeLoi_);
        return false;
      } else if (this.cdDureeServiceMaxQuaiPref_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre chargement / loi de probabilit�: \nDur�e de service maximum quai pr�f�rentiel manquant.")
            .activate();
        donnees_.getApplication().addInternalFrame(modeLoi_);
        return false;
      } else {
        float a = Float.parseFloat(cdDureeServiceMinQuaiPref_.getText());
        float b = Float.parseFloat(cdDureeServiceMaxQuaiPref_.getText());

        if (b < a) {
          new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
              "Fen�tre Loi / dur�e service Maximum inf�rieure � la dur�e de service minimum.").activate();
          donnees_.getApplication().addInternalFrame(modeLoi_);
          return false;
        }

      }

      if (this.cdDureeServiceMinAutreQuai_.getText().equals("") && this.lqQuaiPref2_.getSelectedIndex() != 0) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre chargement / loi de probabilit�:\n dur�e de service minimum autres quais manquant.").activate();
        donnees_.getApplication().addInternalFrame(modeLoi_);
        return false;
      } else if (this.cdDureeServiceMaxAutreQuai_.getText().equals("") && this.lqQuaiPref2_.getSelectedIndex() != 0) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Fen�tre chargement / loi de probabilit�: \ndur�e de service maximum aux autres quais manquant.").activate();
        donnees_.getApplication().addInternalFrame(modeLoi_);
        return false;
      } else {
        float a = Float.parseFloat(cdDureeServiceMinAutreQuai_.getText());
        float b = Float.parseFloat(cdDureeServiceMaxAutreQuai_.getText());

        if (b < a) {
          new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
              "Fen�tre Loi: la dur�e du service maximum est inf�rieure � celle du service minimum.").activate();
          donnees_.getApplication().addInternalFrame(modeLoi_);
          return false;
        }

      }

    }

    /**
     * controle de verification des quais pr�f�rentiels: le meme quai ne peut etre selctionn� � la fois comme quai
     * pr�f�rentiel 1 et quai pr�f�rentiel 2.
     */
    final String nomQuai = (String) this.lqQuaiPref1_.getSelectedItem();
    final String nomQuai2 = (String) this.lqQuaiPref2_.getSelectedItem();
    final String nomQuai3 = (String) this.lqQuaiPref3_.getSelectedItem();

    if (nomQuai.equals(nomQuai2)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Quais pr�f�rentiels:\n le quai pr�f�rentiel 1 doit �tre diff�rent du quai 2.").activate();
      return false;
    }
    if (nomQuai.equals(nomQuai3)) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Quais pr�f�rentiels:\n le quai pr�f�rentiel 1 doit �tre diff�rent du quai 3.").activate();
      return false;
    }
    if (nomQuai2.equals(nomQuai3) && !nomQuai2.equals("aucun")) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Quais pr�f�rentiels:\n le quai pr�f�rentiel 2 doit �tre diff�rent du quai 3.").activate();
      return false;
    }

    /**
     * Controle plus pouss� des quais: test si la longueur du quai est bien sup�rieur a la longueur du navire:
     */
    final int quaiPreferentiel1 = donnees_.getlQuais_().retournerIndiceQuai(
        (String) this.lqQuaiPref1_.getSelectedItem());
    final int quaiPreferentiel2 = donnees_.getlQuais_().retournerIndiceQuai(
        ((String) this.lqQuaiPref2_.getSelectedItem()));
    final int quaiPreferentiel3 = donnees_.getlQuais_().retournerIndiceQuai(
        ((String) this.lqQuaiPref3_.getSelectedItem()));

    final float longueurMax = Float.parseFloat(dgLongMax_.getText());

    if (quaiPreferentiel1 != -1) {
      final float longueurQuai = (float) this.donnees_.getlQuais_().retournerQuais(quaiPreferentiel1).getLongueur_();
      if (longueurQuai < longueurMax) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Le navire est trop grand pour le quai.\n" + "La longueur max du navire �tant: " + longueurMax + "\n"
                + "et la longueur max du quai pr�f�rentiel 1\n" + " "
                + donnees_.getlQuais_().retournerQuais(quaiPreferentiel1).getNom() + " �tant: " + longueurQuai + ".\n"
                + "Il est impossible que ce navire obtienne ce quai.").activate();

        return false;

      }

    }

    if (quaiPreferentiel2 != -1) {
      final float longueurQuai = (float) this.donnees_.getlQuais_().retournerQuais(quaiPreferentiel2).getLongueur_();
      if (longueurQuai < longueurMax) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, ""
            + "La longueur max du navire �tant: " + longueurMax + "\n" + "et la longueur max du quai pr�f�rentiel 2\n"
            + " " + donnees_.getlQuais_().retournerQuais(quaiPreferentiel2).getNom() + " �tant: " + longueurQuai
            + ".\n" + "Il est impossible que ce navire obtienne ce quai.").activate();

        return false;

      }

    }

    if (quaiPreferentiel3 != -1) {
      final float longueurQuai = (float) this.donnees_.getlQuais_().retournerQuais(quaiPreferentiel3).getLongueur_();
      if (longueurQuai < longueurMax) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, ""
            + "La longueur Max du navire �tant: " + longueurMax + "\n" + "et la longueur Max du quai pr�f�rentiel 1\n"
            + " " + donnees_.getlQuais_().retournerQuais(quaiPreferentiel3).getNom() + " �tant: " + longueurQuai
            + ".\n" + "Il est impossible que ce navire obtienne ce quai.").activate();

        return false;

      }

    }

    // tous les tests ont t epuis: les donnes saisies sont bel et bien correctes!!!
    return true;
  }

  public void creationNavire() {
    if (controle_creationNavire()) {
      // creation d'un objet Navire
      
      SiporNavire navire = null;
      if(!UPDATE)
    	  navire = new SiporNavire();
      else
    	  navire = this.donnees_.getCategoriesNavires_().retournerNavire(NAVIRE_A_MODIFIER_);
    	  
    	  
      // remplissage des donnes ici
      navire.setNom(this.dgNom_.getText());
      navire.setDureeAttenteMaximaleAdmissible(Double.parseDouble(this.dureeAttenteMaxAdmissible_.getText()));
      navire.setPriorite(this.dgPriorite_.getSelectedIndex());

      navire.setLongueurMax(Float.parseFloat(this.dgLongMax_.getText()));
      navire.setLongueurMin(Float.parseFloat(this.dgLongMin_.getText()));
      navire.setLargeurMax(Float.parseFloat(this.dgLargMax_.getText()));
      navire.setLargeurMin(Float.parseFloat(this.dgLargMin_.getText()));


      navire.setGareDepart_(this.dgGareDepart.getSelectedIndex());
      navire.setNomGareDepart_(this.donnees_.getListeGare_().retournerGare(navire.getGareDepart_()));

      navire.setQuaiPreferentiel1(donnees_.getlQuais_().retournerIndiceQuai(
          (String) this.lqQuaiPref1_.getSelectedItem()));
      navire.setQuaiPreferentiel2(donnees_.getlQuais_().retournerIndiceQuai(
          ((String) this.lqQuaiPref2_.getSelectedItem())));
      navire.setQuaiPreferentiel3(donnees_.getlQuais_().retournerIndiceQuai(
          ((String) this.lqQuaiPref3_.getSelectedItem())));

      navire.setNomQuaiPreferentiel1((String) this.lqQuaiPref1_.getSelectedItem());
      navire.setNomQuaiPreferentiel2((String) this.lqQuaiPref2_.getSelectedItem());
      navire.setNomQuaiPreferentiel3((String) this.lqQuaiPref3_.getSelectedItem());
      /*F9
      if(this.lqDuree1_.getText()!= null && this.lqDuree1_.getText().length()>0)
    	  navire.setDureeQuaiPref1(Double.parseDouble(this.lqDuree1_.getText()));
      if(this.lqDuree2_.getText()!= null && this.lqDuree2_.getText().length()>0)
    	  navire.setDureeQuaiPref2(Double.parseDouble(this.lqDuree2_.getText()));
      if(this.lqDuree3_.getText()!= null && this.lqDuree3_.getText().length()>0)
    	  navire.setDureeQuaiPref3(Double.parseDouble(this.lqDuree3_.getText()));
      */
      // recuperation des horires
      navire.getCreneauxJournaliers_().recopie(this.horaireG_);
      // horaires creneaux ouvertures
      navire.getCreneauxouverture_().recopie(this.horairePM_);

      if (cdShift.isSelected()) navire.setModeChargement(0);// this.cdType_.getSelectedIndex();
      else navire.setModeChargement(1);

      if (navire.getModeChargement() == 0) {
        navire.setTypeModeChargement("shift");
      } else {
        navire.setTypeModeChargement("loi");
      }

      navire.setTirantEauEntree(Double.parseDouble(this.dgTirantEntree_.getText()));
      navire.setTirantEauSortie(Double.parseDouble(this.dgTirantSortie_.getText()));

      // recopiage des horaires
      if (!choixHorairesNavires_.isSelected() || cdHoraires_.getSelectedIndex() < -1
          || cdHoraires_.getSelectedIndex() >= donnees_.getCategoriesNavires_().getListeNavires_().size()) navire
          .getHoraireTravailSuivi().recopie(this.horaire_);
      else {
        int indice = cdHoraires_.getSelectedIndex();
        navire.getHoraireTravailSuivi().recopie(
            donnees_.getCategoriesNavires_().retournerNavire(indice).getHoraireTravailSuivi());
      }
      /**
       * ajout mode chargemebnt dechargement
       */
      if (navire.getModeChargement() == 0) {
        // mode shift
        navire.setCadenceQuai(Double.parseDouble(this.cdCadenceAutreQuai_.getText()));
        navire.setCadenceQuaiPref(Double.parseDouble(this.cdCadenceQuaiPref_.getText()));
        // reste dur�e avant shift suivant
        navire.setDureeAttenteShiftSuivant(Double.parseDouble(this.cdDureeProchainShift.getText()));
        navire.setTonnageMax(Double.parseDouble(this.cdTonnageMax_.getText()));
        navire.setTonnageMin(Double.parseDouble(this.cdTonnageMin_.getText()));
      } else {
        // mode loi de service:

        navire.setDureeServiceMaxAutresQuais(Double.parseDouble(this.cdDureeServiceMaxAutreQuai_.getText()));
        navire.setDureeServiceMaxQuaiPref(Double.parseDouble(this.cdDureeServiceMaxQuaiPref_.getText()));
        navire.setDureeServiceMinAutresQuais(Double.parseDouble(this.cdDureeServiceMinAutreQuai_.getText()));
        navire.setDureeServiceMinQuaiPref(Double.parseDouble(this.cdDureeServiceMinQuaiPref_.getText()));

      }

      /**
       * Saisie des loi de generation de navires: JComboBox choixLoiGenerationNav_=new JComboBox(choixLoi_);
       */

      if (choixLoiGenerationNav_.getSelectedIndex() == 0) {
        navire.setTypeLoiGenerationNavires_(0);
        navire.setNbBateauxattendus(Integer.parseInt(this.nbBateauxAttendus_.getText()));

        navire.setLoiErlangGenerationNavire(Integer.parseInt((String) this.loiGenerationNavErlang_.getSelectedItem()));
      } else if (choixLoiGenerationNav_.getSelectedIndex() == 1) {

        navire.setTypeLoiGenerationNavires_(1);
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          navire.getLoiDeterministe_().add(c);

        }

      } else if (choixLoiGenerationNav_.getSelectedIndex() == 2) {
        // cas loi journaliere
        navire.setTypeLoiGenerationNavires_(2);
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          navire.getLoiDeterministe_().add(c);

        }

      }

      // ajout dans la dataSimulation:

      if (UPDATE == false) {
        this.donnees_.getCategoriesNavires_().ajout(navire);
        new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Cat�gorie de navire " + navire.getNom() + " correctement ajout�e.").activate();

        /**
         * Creation d'un vecteur ligne suppl�mentaire regles de navigations pour chaque chenal et cercles:
         */
        for (int i = 0; i < this.donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
          final SiporChenal chenal = this.donnees_.getListeChenal_().retournerChenal(i);

          chenal.getReglesNavigation_().ajoutNavire();

        }
        for (int i = 0; i < this.donnees_.getListeCercle_().getListeCercles_().size(); i++) {
          final SiporCercle cercle = this.donnees_.getListeCercle_().retournerCercle(i);

          cercle.getReglesNavigation_().ajoutNavire();

          // -- AHX - 2011 - Genes --//
          cercle.getRegleGenes().ajoutNavire();

        }

        /**
         * Creation d'un vecteur colonne supplementaire
         */
        this.donnees_.getReglesDureesParcoursChenal_().ajoutNavire();
        this.donnees_.getReglesDureesParcoursCercle_().ajoutNavire();
        this.donnees_.getReglesRemplissageSAS().ajoutNavire();

      } else {
        donnees_.getCategoriesNavires_().modification(NAVIRE_A_MODIFIER_, navire);
        new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Cat�gorie de navire " + navire.getNom() + " correctement modifi�e.").activate();
      }

      // --On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      donnees_.baisserNiveauSecurite();

      // 2)sauvegarde des donnees
      donnees_.enregistrer();

      // ajout d l horaire dans la liste des horaires des navires(horaire qui pourra par la suite etre
      // selectionn� dans une jcombo afin d'acc�l�rer la sasiie et la rendre plus ergonomique

      // initialisation des parametresml.add(ml6);
      this.initialiser();

      // affichage du tableau modifi� magique!!
      this.MENUNAVIRES_.pile_.first(this.MENUNAVIRES_.principalPanel_);

      // REMISE A ZERO DES COMPOSANTS...
      this.initialiser();

    }

  }

  /**
   * ******************************* METHODE QUI INDIQUE QUE LA FENETRE DE SAISIE D UNE CATEGORIE DE NAVIRE DEVIENT UNE
   * FENETRE DE MODIFICATION D UNE CATEGORIE
   * 
   * @param numNavire indice de la categorie de navire dans la liste des categories a modifier
   */

  public void MODE_MODIFICATION(final int numNavire) {

    majSaisie();
    // 1) passage en mode modification
    this.UPDATE = true;

    // 2) recuperation de l indice de la cat�gorie de navire
    this.NAVIRE_A_MODIFIER_ = numNavire;

    // 3) recuperation de la structure de navire

    final SiporNavire nav = this.donnees_.getCategoriesNavires_().retournerNavire(this.NAVIRE_A_MODIFIER_);

    // 4)remplissage des donnn�es relatives au quai:
    this.dgLongMax_.setText("" + (float) nav.getLongueurMax());
    this.dgLongMin_.setText("" + (float) nav.getLongueurMin());
    this.dgLargMax_.setText("" + (float) nav.getLargeurMax());
    this.dgLargMin_.setText("" + (float) nav.getLargeurMin());
    this.dgNom_.setText(nav.getNom());
    this.dureeAttenteMaxAdmissible_.setText("" + (float)nav.getDureeAttenteMaximaleAdmissible());
    this.dgTirantEntree_.setText("" + (float) nav.getTirantEauEntree());
    this.dgTirantSortie_.setText("" + (float) nav.getTirantEauSortie());
    // �modification du navire: changment du nom du bouton
    this.validerNavire_.setText("Modifier");
    // index pointant vers la gare de depart
    this.dgGareDepart.setSelectedIndex(nav.getGareDepart_());

    this.dgPriorite_.setSelectedIndex(nav.getPriorite());

    // index des loi de variations

    // quais pr�f�rentiels
    this.lqQuaiPref1_.setSelectedIndex(nav.getQuaiPreferentiel1());
    this.lqQuaiPref2_.setSelectedIndex(nav.getQuaiPreferentiel2() + 1);
    this.lqQuaiPref3_.setSelectedIndex(nav.getQuaiPreferentiel3() + 1);
    /*F9
    this.lqDuree1_.setText(""+ nav.getDureeQuaiPref1());
    this.lqDuree2_.setText(""+ nav.getDureeQuaiPref2());
    this.lqDuree3_.setText(""+ nav.getDureeQuaiPref3());
    */
    // cas mode Shift
    if (nav.getModeChargement() == 0) {
      this.cdShift.setSelected(true);
      this.cdCadenceAutreQuai_.setText("" + (float) nav.getCadenceQuai());
      this.cdCadenceQuaiPref_.setText("" + (float) nav.getCadenceQuaiPref());
      // reste dur�e avant shift suivant
      this.cdDureeProchainShift.setText("" + (float) nav.getDureeAttenteShiftSuivant());
      this.cdTonnageMax_.setText("" + (float) nav.getTonnageMax());
      this.cdTonnageMin_.setText("" + (float) nav.getTonnageMin());
    } else // cas mode loi de service
    {
      //
      this.cdLoi.setSelected(true);
      this.cdDureeServiceMaxAutreQuai_.setText("" + (float) nav.getDureeServiceMaxAutresQuais());
      this.cdDureeServiceMaxQuaiPref_.setText("" + (float) nav.getDureeServiceMaxQuaiPref());
      this.cdDureeServiceMinAutreQuai_.setText("" + (float) nav.getDureeServiceMinAutresQuais());
      this.cdDureeServiceMinQuaiPref_.setText("" + (float) nav.getDureeServiceMinQuaiPref());

    }

    // mise a jour des donn�es selon loi de generation de navires
    if (nav.getTypeLoiGenerationNavires_() == 0) {

      this.nbBateauxAttendus_.setText("" + nav.getNbBateauxattendus());
      // this.ecartMoyenEntre2Arrivees_.setText(""+(float)nav.ecartMoyenEntre2arrivees);
      this.loiGenerationNavErlang_.setSelectedIndex(nav.getLoiErlangGenerationNavire() - 1);
      this.choixLoiGenerationNav_.setSelectedIndex(0);

    } else if (nav.getTypeLoiGenerationNavires_() == 1) {

      for (int i = 0; i < nav.getLoiDeterministe_().size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) nav.getLoiDeterministe_()
            .get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiGenerationNav_.setSelectedIndex(1);
    } else if (nav.getTypeLoiGenerationNavires_() == 2) {
      // cas loi journaliere

      for (int i = 0; i < nav.getLoiDeterministe_().size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) nav.getLoiDeterministe_()
            .get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiGenerationNav_.setSelectedIndex(2);

    }

    this.horaireG_.recopie(nav.getCreneauxJournaliers_());
    this.horairePM_.recopie(nav.getCreneauxouverture_());
    this.horaire_.recopie(nav.getHoraireTravailSuivi());

    this.modeShift_.setVisible(false);

    this.modeLoi_.setVisible(false);
    if (this.fenetreLoideter_ != null) {
      fenetreLoideter_.setVisible(false);
    }

    if (this.fenetreLoiJournaliere_ != null) {
      fenetreLoiJournaliere_.setVisible(false);
    }

    this.dgNom_.requestFocus();
    this.dgNom_.selectAll();

  }

  /**
   * Methode d'initialisation du contenu des donn�es:
   */
  public void initialiser() {

    majSaisie();
    fenetreLoideter_ = null;
    this.premierAvertissment = true;
    this.horaire_ = new SiporHoraire();
    this.horaireG_ = new SiporHoraire();
    this.horairePM_ = new SiporHoraire();

    if (this.dgGareDepart.getItemCount() != 0) {
      this.dgGareDepart.setSelectedIndex(0);
    }

    this.dgNom_.setText("Cat�gorie " + (donnees_.getCategoriesNavires_().getListeNavires_().size() + 1));
    this.dureeAttenteMaxAdmissible_.setText("0");
    this.dgLongMax_.setText("");
    this.dgLongMin_.setText("");
    /*F9
    this.lqDuree1_.setText("");
    this.lqDuree2_.setText("");
    this.lqDuree3_.setText("");
    */
    this.dgTirantEntree_.setText("");
    this.dgTirantSortie_.setText("");
    this.validerNavire_.setText("Valider");
    this.dgLargMax_.setText("");
    this.dgLargMin_.setText("");

    this.dgPriorite_.setSelectedIndex(0);

    // REMISE A ZERO D UN JCOMBO
    this.lqQuaiPref1_.setSelectedIndex(0);
    this.lqQuaiPref2_.setSelectedIndex(0);
    this.lqQuaiPref3_.setSelectedIndex(0);
    /*F9
    this.lqDuree1_.setText("0");
    this.lqDuree2_.setText("0");
    this.lqDuree3_.setText("0");
    */
    // remise a zero de al fenetre shift
    this.cdCadenceAutreQuai_.setText("");
    this.cdCadenceQuaiPref_.setText("");
    this.cdDureeServiceMinAutreQuai_.setText("0");
    this.cdTonnageMax_.setText("");
    this.cdTonnageMin_.setText("");

    // remise a zero loi de serrvice
    this.cdDureeServiceMinQuaiPref_.setText("");
    this.cdDureeProchainShift.setText("");
    this.cdDureeServiceMaxAutreQuai_.setText("0");
    this.cdDureeServiceMaxQuaiPref_.setText("");

    choixLoiGenerationNav_.setSelectedIndex(0);
    this.loiGenerationNavErlang_.setSelectedIndex(0);
    nbBateauxAttendus_.setText("");
    nbBateauxAttendus_.setEnabled(true);
    this.loiDeterministe_ = new ArrayList();

    // rafraichissement du tableau
    this.MENUNAVIRES_.affichagePanel_.maj(donnees_);

    // r�initialisation du mode modification
    this.UPDATE = false;

    this.dgNom_.requestFocus();
    this.dgNom_.selectAll();

  }

}
