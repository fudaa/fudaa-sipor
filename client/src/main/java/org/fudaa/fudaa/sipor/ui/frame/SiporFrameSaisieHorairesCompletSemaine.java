/*
 * @creation 12 oct. 06
 * 
 * @modification $Dates$
 * 
 * @license GNU General Public Licence 2
 * 
 * @copyright (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sipor.ui.frame;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;

import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Fenetre de gestion des horaires de semaines d�taill�s: 3 cr�neaux � saisir pour: lundi mardi mercredi jeudi vendredi
 * samedi dimanche jours f�ri�s D�rive de la classe SiporFrameSaisieHorairesComplet qui d�rive de
 * SiporFrameSaisieHoraire.
 * 
 * @see SiporFrameSaisieHorairesComplet
 * @see SiporFrameSaisieHoraire
 * @version $Version$
 * @author hadoux
 */
public class SiporFrameSaisieHorairesCompletSemaine extends SiporInternalFrame {

	
	public static class HoraireField extends JTextField {

		public HoraireField(int i) {
			super(i);
		}

		@Override
		public String getText() {
			String res = super.getText();
			if(res != null) {
				res = res.replace(":",".").replace(",", ".");
			}
			return res;
		}

		@Override
		public void setText(String res) {
			if(res != null) {
				res = res.replace(".",":").replace(",", ":");
			}
			super.setText(res);
		}
		
		
	}
	
  /**
   * Objet horaire qui sera rempli suite a la saisie de l utilisateur.
   */
  SiporHoraire horaire_;

  /**
   * table des differents menus de saisie des horaires.
   */
  JTabbedPane typesHoraires_ = new JTabbedPane();

  // les paneaux en charge des saisies d horaires
  JPanel lundiglobal_;
  JPanel mardiglobal_;
  JPanel mercrediglobal_;
  JPanel jeudiglobal_;
  JPanel vendrediglobal_;
  JPanel sglobal_;
  JPanel dglobal_;
  JPanel fglobal_;

  public static BuButton createValidationBt() {
    return new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  }

  final BuButton validation_ = createValidationBt();
  final BuButton validation2_ = createValidationBt();
  final BuButton validation3_ = createValidationBt();
  final BuButton validation4_ = createValidationBt();
  final BuButton validation5_ = createValidationBt();
  final BuButton validation6_ = createValidationBt();
  final BuButton validation7_ = createValidationBt();
  final BuButton validation8_ = createValidationBt();
  final BuButton validation9_ = createValidationBt();

  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  // TODO FRED: repetition
  /**
   * JText du lundi.
   */
  JTextField lundicreneau1debut_ = new HoraireField(3);
  JTextField lundicreneau1fin_ = new HoraireField(3);
  JTextField lundicreneau2debut_ = new HoraireField(3);
  JTextField lundicreneau2fin_ = new HoraireField(3);
  JTextField lundicreneau3debut_ = new HoraireField(3);
  JTextField lundicreneau3fin_ = new HoraireField(3);

  /**
   * JText du lundi.
   */
  JTextField mardicreneau1debut_ = new HoraireField(3);
  JTextField mardicreneau1fin_ = new HoraireField(3);
  JTextField mardicreneau2debut_ = new HoraireField(3);
  JTextField mardicreneau2fin_ = new HoraireField(3);
  JTextField mardicreneau3debut_ = new HoraireField(3);
  JTextField mardicreneau3fin_ = new HoraireField(3);

  /**
   * JText du mercredi
   */
  JTextField mercredicreneau1debut_ = new HoraireField(3);
  JTextField mercredicreneau1fin_ = new HoraireField(3);
  JTextField mercredicreneau2debut_ = new HoraireField(3);
  JTextField mercredicreneau2fin_ = new HoraireField(3);
  JTextField mercredicreneau3debut_ = new HoraireField(3);
  JTextField mercredicreneau3fin_ = new HoraireField(3);

  /**
   * JText du jeudi
   */
  JTextField jeudicreneau1debut_ = new HoraireField(3);
  JTextField jeudicreneau1fin_ = new HoraireField(3);
  JTextField jeudicreneau2debut_ = new HoraireField(3);
  JTextField jeudicreneau2fin_ = new HoraireField(3);
  JTextField jeudicreneau3debut_ = new HoraireField(3);
  JTextField jeudicreneau3fin_ = new HoraireField(3);

  /**
   * JText du vendredi
   */
  JTextField vendredicreneau1debut_ = new HoraireField(3);
  JTextField vendredicreneau1fin_ = new HoraireField(3);
  JTextField vendredicreneau2debut_ = new HoraireField(3);
  JTextField vendredicreneau2fin_ = new HoraireField(3);
  JTextField vendredicreneau3debut_ = new HoraireField(3);
  JTextField vendredicreneau3fin_ = new HoraireField(3);

  /**
   * JText du samedi
   */
  JTextField screneau1debut_ = new HoraireField(3);
  JTextField screneau1fin_ = new HoraireField(3);
  JTextField screneau2debut_ = new HoraireField(3);
  JTextField screneau2fin_ = new HoraireField(3);
  JTextField screneau3debut_ = new HoraireField(3);
  JTextField screneau3fin_ = new HoraireField(3);

  /**
   * JText du dimanche
   */
  JTextField dcreneau1debut_ = new HoraireField(3);
  JTextField dcreneau1fin_ = new HoraireField(3);
  JTextField dcreneau2debut_ = new HoraireField(3);
  JTextField dcreneau2fin_ = new HoraireField(3);
  JTextField dcreneau3debut_ = new HoraireField(3);
  JTextField dcreneau3fin_ = new HoraireField(3);

  /**
   * JText des Jours f�ri�s
   */
  JTextField fcreneau1debut_ = new HoraireField(3);
  JTextField fcreneau1fin_ = new HoraireField(3);
  JTextField fcreneau2debut_ = new HoraireField(3);
  JTextField fcreneau2fin_ = new HoraireField(3);
  JTextField fcreneau3debut_ = new HoraireField(3);
  JTextField fcreneau3fin_ = new HoraireField(3);

  public SiporFrameSaisieHorairesCompletSemaine(final SiporHoraire _h) {
    super("", true, true, true, true);
    horaire_ = _h;
    setTitle("Saisie d'un horaire");
    setSize(415, 280);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));

    final ActionListener listenerBouton = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        creationHoraire();
      }

    };

    validation_.addActionListener(listenerBouton);
    validation2_.addActionListener(listenerBouton);
    validation3_.addActionListener(listenerBouton);
    validation4_.addActionListener(listenerBouton);
    validation5_.addActionListener(listenerBouton);
    validation6_.addActionListener(listenerBouton);
    validation7_.addActionListener(listenerBouton);
    validation8_.addActionListener(listenerBouton);

    /**
     * Menu lundi
     */
    this.lundicreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.lundicreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.lundicreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.lundicreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.lundicreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.lundicreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.lundiCreneau1HeureArrivee != -1 && horaire_.lundiCreneau2HeureArrivee != -1
        && horaire_.lundiCreneau3HeureArrivee != -1 && horaire_.lundiCreneau1HeureDep != -1
        && horaire_.lundiCreneau2HeureDep != -1 && horaire_.lundiCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.lundicreneau1debut_.setText("" + (float) horaire_.lundiCreneau1HeureDep);
      this.lundicreneau1fin_.setText("" + (float) horaire_.lundiCreneau1HeureArrivee);
      this.lundicreneau2debut_.setText("" + (float) horaire_.lundiCreneau2HeureDep);
      this.lundicreneau2fin_.setText("" + (float) horaire_.lundiCreneau2HeureArrivee);
      this.lundicreneau3debut_.setText("" + (float) horaire_.lundiCreneau3HeureDep);
      this.lundicreneau3fin_.setText("" + (float) horaire_.lundiCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.lundicreneau1debut_.setText("0.0");
      this.lundicreneau1fin_.setText("24.0");
      this.lundicreneau2debut_.setText("0.0");
      this.lundicreneau2fin_.setText("0.0");
      this.lundicreneau3debut_.setText("0.0");
      this.lundicreneau3fin_.setText("0.0");
    }

    this.lundicreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        lundicreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!lundicreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(lundicreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            lundicreneau1debut_.setText("");
          }
        }
      }
    });

    this.lundicreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        lundicreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!lundicreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(lundicreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau1fin_.setText("");
            } else if (i > 24) {

              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            lundicreneau1fin_.setText("");
          }
        }
      }
    });

    this.lundicreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        lundicreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!lundicreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(lundicreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau2debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            lundicreneau2debut_.setText("");
          }
        }
      }
    });

    this.lundicreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        lundicreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!lundicreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(lundicreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            lundicreneau2fin_.setText("");
          }
        }
      }
    });

    this.lundicreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        lundicreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!lundicreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(lundicreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            lundicreneau3debut_.setText("");
          }
        }
      }
    });

    this.lundicreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        lundicreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!lundicreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(lundicreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              lundicreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            lundicreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    lundiglobal_ = new JPanel();
    lundiglobal_.setLayout(new GridLayout(5, 1));

    final JPanel lundich11 = new JPanel();
    lundich11.add(new JLabel("Horaires du lundi"));
    lundiglobal_.setBorder(compound_);
    lundiglobal_.add(lundich11);
    // panel de saisie du premier Cr�neau horaire
    final JPanel lundich1 = new JPanel();

    lundich1.add(new JLabel(" Cr�neau 1, Horaire: "));
    lundich1.add(this.lundicreneau1debut_);
    lundich1.add(new JLabel("�"));
    lundich1.add(this.lundicreneau1fin_);
    lundich1.add(new JLabel("HEURES:MINUTES"));
    lundich1.setBorder(bordnormal_);
    lundiglobal_.add(lundich1);

    // panel de saisie du deuxieme horaire
    final JPanel lundich2 = new JPanel();
    lundich2.add(new JLabel(" Cr�neau 2, Horaire: "));
    lundich2.add(this.lundicreneau2debut_);
    lundich2.add(new JLabel("�"));
    lundich2.add(this.lundicreneau2fin_);
    lundich2.add(new JLabel("HEURES:MINUTES"));
    lundich2.setBorder(bordnormal_);
    lundiglobal_.add(lundich2);

    // panel de saisie du troisieme horaire
    final JPanel lundich4 = new JPanel();
    lundich4.add(new JLabel(" Cr�neau 3, Horaire: "));
    lundich4.add(this.lundicreneau3debut_);
    lundich4.add(new JLabel("�"));
    lundich4.add(this.lundicreneau3fin_);
    lundich4.add(new JLabel("HEURES:MINUTES"));
    lundich4.setBorder(bordnormal_);
    lundiglobal_.add(lundich4);

    // panel qui contient le bouton de validation:
    final JPanel lundich3 = new JPanel();
    lundich3.add(new JLabel("Cliquez ici pour valider: "));
    lundich3.add(this.validation_);
    lundich3.setBorder(bordnormal_);

    lundiglobal_.setBorder(compound_);
    lundiglobal_.add(lundich3);
    this.typesHoraires_.add("lundi", lundiglobal_);

    /**
     * Menu mardi
     */
    this.mardicreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.mardicreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.mardicreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.mardicreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.mardicreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.mardicreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.mardiCreneau1HeureArrivee != -1 && horaire_.mardiCreneau2HeureArrivee != -1
        && horaire_.mardiCreneau3HeureArrivee != -1 && horaire_.mardiCreneau1HeureDep != -1
        && horaire_.mardiCreneau2HeureDep != -1 && horaire_.mardiCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.mardicreneau1debut_.setText("" + (float) horaire_.mardiCreneau1HeureDep);
      this.mardicreneau1fin_.setText("" + (float) horaire_.mardiCreneau1HeureArrivee);
      this.mardicreneau2debut_.setText("" + (float) horaire_.mardiCreneau2HeureDep);
      this.mardicreneau2fin_.setText("" + (float) horaire_.mardiCreneau2HeureArrivee);
      this.mardicreneau3debut_.setText("" + (float) horaire_.mardiCreneau3HeureDep);
      this.mardicreneau3fin_.setText("" + (float) horaire_.mardiCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.mardicreneau1debut_.setText("0.0");
      this.mardicreneau1fin_.setText("24.0");
      this.mardicreneau2debut_.setText("0.0");
      this.mardicreneau2fin_.setText("0.0");
      this.mardicreneau3debut_.setText("0.0");
      this.mardicreneau3fin_.setText("0.0");
    }

    this.mardicreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mardicreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!mardicreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mardicreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mardicreneau1debut_.setText("");
          }
        }
      }
    });

    this.mardicreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mardicreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!mardicreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mardicreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mardicreneau1fin_.setText("");
          }
        }
      }
    });

    this.mardicreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mardicreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!mardicreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mardicreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mardicreneau2debut_.setText("");
          }
        }
      }
    });

    this.mardicreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mardicreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!mardicreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mardicreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mardicreneau2fin_.setText("");
          }
        }
      }
    });

    this.mardicreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mardicreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!mardicreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mardicreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mardicreneau3debut_.setText("");
          }
        }
      }
    });

    this.mardicreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mardicreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!mardicreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mardicreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mardicreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mardicreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    mardiglobal_ = new JPanel();
    mardiglobal_.setLayout(new GridLayout(5, 1));

    // panel qui contient le bouton de validation:
    final JPanel mardich3 = new JPanel();
    mardich3.add(new JLabel("Horaires du mardi"));
    mardiglobal_.setBorder(compound_);
    mardiglobal_.add(mardich3);

    // panel de saisie du premier Cr�neau horaire
    final JPanel mardich1 = new JPanel();

    mardich1.add(new JLabel(" Cr�neau 1, Horaire: "));
    mardich1.add(this.mardicreneau1debut_);
    mardich1.add(new JLabel("�"));
    mardich1.add(this.mardicreneau1fin_);
    mardich1.add(new JLabel("HEURES:MINUTES"));
    mardich1.setBorder(bordnormal_);
    mardiglobal_.add(mardich1);

    // panel de saisie du deuxieme horaire
    final JPanel mardich2 = new JPanel();
    mardich2.add(new JLabel(" Cr�neau 2, Horaire: "));
    mardich2.add(this.mardicreneau2debut_);
    mardich2.add(new JLabel("�"));
    mardich2.add(this.mardicreneau2fin_);
    mardich2.add(new JLabel("HEURES:MINUTES"));
    mardich2.setBorder(bordnormal_);
    mardiglobal_.add(mardich2);

    // panel de saisie du troisieme horaire
    final JPanel mardich4 = new JPanel();
    mardich4.add(new JLabel(" Cr�neau 3, Horaire: "));
    mardich4.add(this.mardicreneau3debut_);
    mardich4.add(new JLabel("�"));
    mardich4.add(this.mardicreneau3fin_);
    mardich4.add(new JLabel("HEURES:MINUTES"));
    mardich4.setBorder(bordnormal_);
    mardiglobal_.add(mardich4);

    // panel qui contient le bouton de validation:
    final JPanel mardich31 = new JPanel();
    mardich31.add(new JLabel("Cliquez ici pour valider: "));
    mardich31.add(this.validation2_);
    mardich31.setBorder(bordnormal_);

    mardiglobal_.setBorder(compound_);
    mardiglobal_.add(mardich31);

    this.typesHoraires_.add("mardi", mardiglobal_);

    /**
     * Menu mercredi
     */
    this.mercredicreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.mercredicreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.mercredicreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.mercredicreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.mercredicreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.mercredicreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.mercrediCreneau1HeureArrivee != -1 && horaire_.mercrediCreneau2HeureArrivee != -1
        && horaire_.mercrediCreneau3HeureArrivee != -1 && horaire_.mercrediCreneau1HeureDep != -1
        && horaire_.mercrediCreneau2HeureDep != -1 && horaire_.mercrediCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.mercredicreneau1debut_.setText("" + (float) horaire_.mercrediCreneau1HeureDep);
      this.mercredicreneau1fin_.setText("" + (float) horaire_.mercrediCreneau1HeureArrivee);
      this.mercredicreneau2debut_.setText("" + (float) horaire_.mercrediCreneau2HeureDep);
      this.mercredicreneau2fin_.setText("" + (float) horaire_.mercrediCreneau2HeureArrivee);
      this.mercredicreneau3debut_.setText("" + (float) horaire_.mercrediCreneau3HeureDep);
      this.mercredicreneau3fin_.setText("" + (float) horaire_.mercrediCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.mercredicreneau1debut_.setText("0.0");
      this.mercredicreneau1fin_.setText("24.0");
      this.mercredicreneau2debut_.setText("0.0");
      this.mercredicreneau2fin_.setText("0.0");
      this.mercredicreneau3debut_.setText("0.0");
      this.mercredicreneau3fin_.setText("0.0");
    }

    this.mercredicreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mercredicreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!mercredicreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mercredicreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mercredicreneau1debut_.setText("");
          }
        }
      }
    });

    this.mercredicreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mercredicreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!mercredicreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mercredicreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mercredicreneau1fin_.setText("");
          }
        }
      }
    });

    this.mercredicreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mercredicreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!mercredicreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mercredicreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau2debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mercredicreneau2debut_.setText("");
          }
        }
      }
    });

    this.mercredicreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mercredicreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!mercredicreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mercredicreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mercredicreneau2fin_.setText("");
          }
        }
      }
    });

    this.mercredicreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mercredicreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        if (!mercredicreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mercredicreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mercredicreneau3debut_.setText("");
          }
        }
      }
    });

    this.mercredicreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        mercredicreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!mercredicreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(mercredicreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              mercredicreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            mercredicreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    mercrediglobal_ = new JPanel();
    mercrediglobal_.setLayout(new GridLayout(5, 1));

    // panel qui contient le bouton de validation:
    final JPanel mercredich3 = new JPanel();
    mercredich3.add(new JLabel("Horaires du mercredi"));
    mercrediglobal_.setBorder(compound_);
    mercrediglobal_.add(mercredich3);
    // panel de saisie du premier Cr�neau horaire
    final JPanel mercredich1 = new JPanel();

    mercredich1.add(new JLabel(" Cr�neau 1, Horaire: "));
    mercredich1.add(this.mercredicreneau1debut_);
    mercredich1.add(new JLabel("�"));
    mercredich1.add(this.mercredicreneau1fin_);
    mercredich1.add(new JLabel("HEURES:MINUTES"));
    mercredich1.setBorder(bordnormal_);
    mercrediglobal_.add(mercredich1);

    // panel de saisie du deuxieme horaire
    final JPanel mercredich2 = new JPanel();
    mercredich2.add(new JLabel(" Cr�neau 2, Horaire: "));
    mercredich2.add(this.mercredicreneau2debut_);
    mercredich2.add(new JLabel("�"));
    mercredich2.add(this.mercredicreneau2fin_);
    mercredich2.add(new JLabel("HEURES:MINUTES"));
    mercredich2.setBorder(bordnormal_);
    mercrediglobal_.add(mercredich2);

    // panel de saisie du troisieme horaire
    final JPanel mercredich4 = new JPanel();
    mercredich4.add(new JLabel(" Cr�neau 3, Horaire: "));
    mercredich4.add(this.mercredicreneau3debut_);
    mercredich4.add(new JLabel("�"));
    mercredich4.add(this.mercredicreneau3fin_);
    mercredich4.add(new JLabel("HEURES:MINUTES"));
    mercredich4.setBorder(bordnormal_);
    mercrediglobal_.add(mercredich4);

    final JPanel mercredich31 = new JPanel();
    mercredich31.add(new JLabel("Cliquez ici pour valider: "));
    mercredich31.add(this.validation3_);
    mercredich31.setBorder(bordnormal_);

    mercrediglobal_.setBorder(compound_);
    mercrediglobal_.add(mercredich31);

    // panel qui contient le bouton de validation:

    this.typesHoraires_.add("mercredi", mercrediglobal_);

    /**
     * Menu jeudi
     */
    this.jeudicreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.jeudicreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.jeudicreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.jeudicreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.jeudicreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.jeudicreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.jeudiCreneau1HeureArrivee != -1 && horaire_.jeudiCreneau2HeureArrivee != -1
        && horaire_.jeudiCreneau3HeureArrivee != -1 && horaire_.jeudiCreneau1HeureDep != -1
        && horaire_.jeudiCreneau2HeureDep != -1 && horaire_.jeudiCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.jeudicreneau1debut_.setText("" + (float) horaire_.jeudiCreneau1HeureDep);
      this.jeudicreneau1fin_.setText("" + (float) horaire_.jeudiCreneau1HeureArrivee);
      this.jeudicreneau2debut_.setText("" + (float) horaire_.jeudiCreneau2HeureDep);
      this.jeudicreneau2fin_.setText("" + (float) horaire_.jeudiCreneau2HeureArrivee);
      this.jeudicreneau3debut_.setText("" + (float) horaire_.jeudiCreneau3HeureDep);
      this.jeudicreneau3fin_.setText("" + (float) horaire_.jeudiCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.jeudicreneau1debut_.setText("0.0");
      this.jeudicreneau1fin_.setText("24.0");
      this.jeudicreneau2debut_.setText("0.0");
      this.jeudicreneau2fin_.setText("0.0");
      this.jeudicreneau3debut_.setText("0.0");
      this.jeudicreneau3fin_.setText("0.0");
    }

    this.jeudicreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        jeudicreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!jeudicreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(jeudicreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            jeudicreneau1debut_.setText("");
          }
        }
      }
    });

    this.jeudicreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        jeudicreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!jeudicreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(jeudicreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            jeudicreneau1fin_.setText("");
          }
        }
      }
    });

    this.jeudicreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        jeudicreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!jeudicreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(jeudicreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            jeudicreneau2debut_.setText("");
          }
        }
      }
    });

    this.jeudicreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        jeudicreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!jeudicreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(jeudicreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            jeudicreneau2fin_.setText("");
          }
        }
      }
    });

    this.jeudicreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        jeudicreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!jeudicreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(jeudicreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            jeudicreneau3debut_.setText("");
          }
        }
      }
    });

    this.jeudicreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        jeudicreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!jeudicreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(jeudicreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              jeudicreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            jeudicreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    jeudiglobal_ = new JPanel();
    jeudiglobal_.setLayout(new GridLayout(5, 1));

    // panel qui contient le bouton de validation:
    final JPanel jeudich3 = new JPanel();
    jeudich3.add(new JLabel("Horaires du jeudi"));
    jeudiglobal_.setBorder(compound_);
    jeudiglobal_.add(jeudich3);

    // panel de saisie du premier Cr�neau horaire
    final JPanel jeudich1 = new JPanel();

    jeudich1.add(new JLabel(" Cr�neau 1, Horaire: "));
    jeudich1.add(this.jeudicreneau1debut_);
    jeudich1.add(new JLabel("�"));
    jeudich1.add(this.jeudicreneau1fin_);
    jeudich1.add(new JLabel("HEURES:MINUTES"));
    jeudich1.setBorder(bordnormal_);
    jeudiglobal_.add(jeudich1);

    // panel de saisie du deuxieme horaire
    final JPanel jeudich2 = new JPanel();
    jeudich2.add(new JLabel(" Cr�neau 2, Horaire: "));
    jeudich2.add(this.jeudicreneau2debut_);
    jeudich2.add(new JLabel("�"));
    jeudich2.add(this.jeudicreneau2fin_);
    jeudich2.add(new JLabel("HEURES:MINUTES"));
    jeudich2.setBorder(bordnormal_);
    jeudiglobal_.add(jeudich2);

    // panel de saisie du troisieme horaire
    final JPanel jeudich4 = new JPanel();
    jeudich4.add(new JLabel(" Cr�neau 3, Horaire: "));
    jeudich4.add(this.jeudicreneau3debut_);
    jeudich4.add(new JLabel("�"));
    jeudich4.add(this.jeudicreneau3fin_);
    jeudich4.add(new JLabel("HEURES:MINUTES"));
    jeudich4.setBorder(bordnormal_);
    jeudiglobal_.add(jeudich4);

    final JPanel jeudich31 = new JPanel();
    jeudich31.add(new JLabel("Cliquez ici pour valider: "));
    jeudich31.add(this.validation4_);
    jeudich31.setBorder(bordnormal_);

    jeudiglobal_.setBorder(compound_);
    jeudiglobal_.add(jeudich31);

    // panel qui contient le bouton de validation:
    this.typesHoraires_.add("jeudi", jeudiglobal_);

    /**
     * Menu vendredi
     */
    this.vendredicreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.vendredicreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.vendredicreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.vendredicreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.vendredicreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.vendredicreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.vendrediCreneau1HeureArrivee != -1 && horaire_.vendrediCreneau2HeureArrivee != -1
        && horaire_.vendrediCreneau3HeureArrivee != -1 && horaire_.vendrediCreneau1HeureDep != -1
        && horaire_.vendrediCreneau2HeureDep != -1 && horaire_.vendrediCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.vendredicreneau1debut_.setText("" + (float) horaire_.vendrediCreneau1HeureDep);
      this.vendredicreneau1fin_.setText("" + (float) horaire_.vendrediCreneau1HeureArrivee);
      this.vendredicreneau2debut_.setText("" + (float) horaire_.vendrediCreneau2HeureDep);
      this.vendredicreneau2fin_.setText("" + (float) horaire_.vendrediCreneau2HeureArrivee);
      this.vendredicreneau3debut_.setText("" + (float) horaire_.vendrediCreneau3HeureDep);
      this.vendredicreneau3fin_.setText("" + (float) horaire_.vendrediCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.vendredicreneau1debut_.setText("0.0");
      this.vendredicreneau1fin_.setText("24.0");
      this.vendredicreneau2debut_.setText("0.0");
      this.vendredicreneau2fin_.setText("0.0");
      this.vendredicreneau3debut_.setText("0.0");
      this.vendredicreneau3fin_.setText("0.0");
    }

    this.vendredicreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        vendredicreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!vendredicreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(vendredicreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            vendredicreneau1debut_.setText("");
          }
        }
      }
    });

    this.vendredicreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        vendredicreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!vendredicreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(vendredicreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            vendredicreneau1fin_.setText("");
          }
        }
      }
    });

    this.vendredicreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        vendredicreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!vendredicreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(vendredicreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau2debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            vendredicreneau2debut_.setText("");
          }
        }
      }
    });

    this.vendredicreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        vendredicreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!vendredicreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(vendredicreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            vendredicreneau2fin_.setText("");
          }
        }
      }
    });

    this.vendredicreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        vendredicreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!vendredicreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(vendredicreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            vendredicreneau3debut_.setText("");
          }
        }
      }
    });

    this.vendredicreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        vendredicreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!vendredicreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(vendredicreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              vendredicreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            vendredicreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    vendrediglobal_ = new JPanel();
    vendrediglobal_.setLayout(new GridLayout(5, 1));

    // panel qui contient le bouton de validation:
    final JPanel vendredich3 = new JPanel();
    vendredich3.add(new JLabel("Horaires du vendredi"));
    vendrediglobal_.setBorder(compound_);
    vendrediglobal_.add(vendredich3);

    // panel de saisie du premier Cr�neau horaire
    final JPanel vendredich1 = new JPanel();

    vendredich1.add(new JLabel(" Cr�neau 1, Horaire: "));
    vendredich1.add(this.vendredicreneau1debut_);
    vendredich1.add(new JLabel("�"));
    vendredich1.add(this.vendredicreneau1fin_);
    vendredich1.add(new JLabel("HEURES:MINUTES"));
    vendredich1.setBorder(bordnormal_);
    vendrediglobal_.add(vendredich1);

    // panel de saisie du deuxieme horaire
    final JPanel vendredich2 = new JPanel();
    vendredich2.add(new JLabel(" Cr�neau 2, Horaire: "));
    vendredich2.add(this.vendredicreneau2debut_);
    vendredich2.add(new JLabel("�"));
    vendredich2.add(this.vendredicreneau2fin_);
    vendredich2.add(new JLabel("HEURES:MINUTES"));
    vendredich2.setBorder(bordnormal_);
    vendrediglobal_.add(vendredich2);

    // panel de saisie du troisieme horaire
    final JPanel vendredich4 = new JPanel();
    vendredich4.add(new JLabel(" Cr�neau 3, Horaire: "));
    vendredich4.add(this.vendredicreneau3debut_);
    vendredich4.add(new JLabel("�"));
    vendredich4.add(this.vendredicreneau3fin_);
    vendredich4.add(new JLabel("HEURES:MINUTES"));
    vendredich4.setBorder(bordnormal_);
    vendrediglobal_.add(vendredich4);

    final JPanel vendredich31 = new JPanel();
    vendredich31.add(new JLabel("Cliquez ici pour valider: "));
    vendredich31.add(this.validation5_);
    vendredich31.setBorder(bordnormal_);

    vendrediglobal_.setBorder(compound_);
    vendrediglobal_.add(vendredich31);

    this.typesHoraires_.add("vendredi", vendrediglobal_);

    /**
     * Menu samedi
     */
    this.screneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.screneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.screneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.screneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.screneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.screneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.samediCreneau1HeureArrivee != -1 && horaire_.samediCreneau2HeureArrivee != -1
        && horaire_.samediCreneau3HeureArrivee != -1 && horaire_.samediCreneau1HeureDep != -1
        && horaire_.samediCreneau2HeureDep != -1 && horaire_.samediCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.screneau1debut_.setText("" + (float) horaire_.samediCreneau1HeureDep);
      this.screneau1fin_.setText("" + (float) horaire_.samediCreneau1HeureArrivee);
      this.screneau2debut_.setText("" + (float) horaire_.samediCreneau2HeureDep);
      this.screneau2fin_.setText("" + (float) horaire_.samediCreneau2HeureArrivee);
      this.screneau3debut_.setText("" + (float) horaire_.samediCreneau3HeureDep);
      this.screneau3fin_.setText("" + (float) horaire_.samediCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.screneau1debut_.setText("0.0");
      this.screneau1fin_.setText("24.0");
      this.screneau2debut_.setText("0.0");
      this.screneau2fin_.setText("0.0");
      this.screneau3debut_.setText("0.0");
      this.screneau3fin_.setText("0.0");
    }

    this.screneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        screneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!screneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            screneau1debut_.setText("");
          }
        }
      }
    });

    this.screneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        screneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!screneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            screneau1fin_.setText("");
          }
        }
      }
    });

    this.screneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        screneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau2debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            screneau2debut_.setText("");
          }
        }
      }
    });

    this.screneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        screneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            screneau2fin_.setText("");
          }
        }
      }
    });

    this.screneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        screneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            screneau3debut_.setText("");
          }
        }
      }
    });

    this.screneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        screneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              screneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            screneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    sglobal_ = new JPanel();
    sglobal_.setLayout(new GridLayout(5, 1));

    // panel qui contient le bouton de validation:
    final JPanel sch3 = new JPanel();
    sch3.add(new JLabel("Horaires du samedi"));
    sglobal_.setBorder(compound_);
    sglobal_.add(sch3);

    // panel de saisie du premier Cr�neau horaire
    final JPanel sch1 = new JPanel();

    sch1.add(new JLabel(" Cr�neau 1, Horaire: "));
    sch1.add(this.screneau1debut_);
    sch1.add(new JLabel("�"));
    sch1.add(this.screneau1fin_);
    sch1.add(new JLabel("HEURES:MINUTES"));
    sch1.setBorder(bordnormal_);
    sglobal_.add(sch1);

    // panel de saisie du deuxieme horaire
    final JPanel sch2 = new JPanel();
    sch2.add(new JLabel(" Cr�neau 2, Horaire: "));
    sch2.add(this.screneau2debut_);
    sch2.add(new JLabel("�"));
    sch2.add(this.screneau2fin_);
    sch2.add(new JLabel("HEURES:MINUTES"));
    sch2.setBorder(bordnormal_);
    sglobal_.add(sch2);

    // panel de saisie du troisieme horaire
    final JPanel sch4 = new JPanel();
    sch4.add(new JLabel(" Cr�neau 3, Horaire: "));
    sch4.add(this.screneau3debut_);
    sch4.add(new JLabel("�"));
    sch4.add(this.screneau3fin_);
    sch4.add(new JLabel("HEURES:MINUTES"));
    sch4.setBorder(bordnormal_);
    sglobal_.add(sch4);

    final JPanel sch31 = new JPanel();
    sch31.add(new JLabel("Cliquez ici pour valider: "));
    sch31.add(this.validation6_);
    sch31.setBorder(bordnormal_);

    sglobal_.setBorder(compound_);
    sglobal_.add(sch31);

    // panel qui contient le bouton de validation:

    this.typesHoraires_.add("samedi", sglobal_);

    /**
     * Menu dimanche
     */
    this.dcreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.dcreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.dcreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.dcreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.dcreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.dcreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.dimancheCreneau1HeureArrivee != -1 && horaire_.dimancheCreneau2HeureArrivee != -1
        && horaire_.dimancheCreneau3HeureArrivee != -1 && horaire_.dimancheCreneau1HeureDep != -1
        && horaire_.dimancheCreneau2HeureDep != -1 && horaire_.dimancheCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.dcreneau1debut_.setText("" + (float) horaire_.dimancheCreneau1HeureDep);
      this.dcreneau1fin_.setText("" + (float) horaire_.dimancheCreneau1HeureArrivee);
      this.dcreneau2debut_.setText("" + (float) horaire_.dimancheCreneau2HeureDep);
      this.dcreneau2fin_.setText("" + (float) horaire_.dimancheCreneau2HeureArrivee);
      this.dcreneau3debut_.setText("" + (float) horaire_.dimancheCreneau3HeureDep);
      this.dcreneau3fin_.setText("" + (float) horaire_.dimancheCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.dcreneau1debut_.setText("0.0");
      this.dcreneau1fin_.setText("24.0");
      this.dcreneau2debut_.setText("0.0");
      this.dcreneau2fin_.setText("0.0");
      this.dcreneau3debut_.setText("0.0");
      this.dcreneau3fin_.setText("0.0");
    }

    this.dcreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        dcreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!dcreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF);
              dcreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            dcreneau1debut_.setText("");
          }
        }
      }
    });

    this.dcreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        dcreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!dcreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            dcreneau1fin_.setText("");
          }
        }
      }
    });

    this.dcreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        dcreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!dcreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau2debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            dcreneau2debut_.setText("");
          }
        }
      }
    });

    this.dcreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        dcreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!dcreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            dcreneau2fin_.setText("");
          }
        }
      }
    });

    this.dcreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        dcreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!dcreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS);
            dcreneau3debut_.setText("");
          }
        }
      }
    });

    this.dcreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        dcreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!dcreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              dcreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            dcreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    dglobal_ = new JPanel();
    dglobal_.setLayout(new GridLayout(5, 1));

    final JPanel dch4 = new JPanel();
    dch4.add(new JLabel("Horaires du dimanche"));
    dglobal_.setBorder(compound_);
    dglobal_.add(dch4);

    // panel de saisie du premier Cr�neau horaire
    final JPanel dch1 = new JPanel();

    dch1.add(new JLabel(" Cr�neau 1, Horaire: "));
    dch1.add(this.dcreneau1debut_);
    dch1.add(new JLabel("�"));
    dch1.add(this.dcreneau1fin_);
    dch1.add(new JLabel("HEURES:MINUTES"));
    dch1.setBorder(bordnormal_);
    dglobal_.add(dch1);

    // panel de saisie du deuxieme horaire
    final JPanel dch2 = new JPanel();
    dch2.add(new JLabel(" Cr�neau 2, Horaire: "));
    dch2.add(this.dcreneau2debut_);
    dch2.add(new JLabel("�"));
    dch2.add(this.dcreneau2fin_);
    dch2.add(new JLabel("HEURES:MINUTES"));
    dch2.setBorder(bordnormal_);
    dglobal_.add(dch2);

    // panel qui contient le troisieme horaire:
    final JPanel dch3 = new JPanel();
    dch3.add(new JLabel(" Cr�neau 3, Horaire: "));
    dch3.add(this.dcreneau3debut_);
    dch3.add(new JLabel("�"));
    dch3.add(this.dcreneau3fin_);
    dch3.add(new JLabel("HEURES:MINUTES"));
    dch3.setBorder(bordnormal_);
    dglobal_.add(dch3);
    // sch3.add(this.validation);
    dglobal_.add(dch3);

    final JPanel dch31 = new JPanel();
    dch31.add(new JLabel("Cliquez ici pour valider: "));
    dch31.add(this.validation7_);
    dch31.setBorder(bordnormal_);

    dglobal_.setBorder(compound_);
    dglobal_.add(dch31);

    dglobal_.setBorder(compound_);
    this.typesHoraires_.add("dimanche", dglobal_);

    /**
     * Menu ferie
     */
    this.fcreneau1debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.fcreneau2debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.fcreneau1fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.fcreneau2fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);
    this.fcreneau3debut_.setToolTipText(MessageConstants.TOOLTIP_OUVERTURE);
    this.fcreneau3fin_.setToolTipText(MessageConstants.TOOLTIP_FERMETURE);

    if (horaire_.ferieCreneau1HeureArrivee != -1 && horaire_.ferieCreneau2HeureArrivee != -1
        && horaire_.ferieCreneau3HeureArrivee != -1 && horaire_.ferieCreneau1HeureDep != -1
        && horaire_.ferieCreneau2HeureDep != -1 && horaire_.ferieCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.fcreneau1debut_.setText("" + (float) horaire_.ferieCreneau1HeureDep);
      this.fcreneau1fin_.setText("" + (float) horaire_.ferieCreneau1HeureArrivee);
      this.fcreneau2debut_.setText("" + (float) horaire_.ferieCreneau2HeureDep);
      this.fcreneau2fin_.setText("" + (float) horaire_.ferieCreneau2HeureArrivee);
      this.fcreneau3debut_.setText("" + (float) horaire_.ferieCreneau3HeureDep);
      this.fcreneau3fin_.setText("" + (float) horaire_.ferieCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.fcreneau1debut_.setText("0.0");
      this.fcreneau1fin_.setText("24.0");
      this.fcreneau2debut_.setText("0.0");
      this.fcreneau2fin_.setText("0.0");
      this.fcreneau3debut_.setText("0.0");
      this.fcreneau3fin_.setText("0.0");

    }

    this.fcreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        fcreneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!fcreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            fcreneau1debut_.setText("");
          }
        }
      }
    });

    this.fcreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        fcreneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!fcreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau1fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            fcreneau1fin_.setText("");
          }
        }
      }
    });

    this.fcreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        fcreneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!fcreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            fcreneau2debut_.setText("");
          }
        }
      }
    });

    this.fcreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        fcreneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!fcreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau2fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            fcreneau2fin_.setText("");
          }
        }
      }
    });

    this.fcreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        fcreneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!fcreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            fcreneau3debut_.setText("");
          }
        }
      }
    });

    this.fcreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        fcreneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!fcreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, MessageConstants.HORAIRE_EST_NEGATIF, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, MessageConstants.COMPRIS_ENTRE_0_ET_24, MessageConstants.WARN,
                  JOptionPane.ERROR_MESSAGE);
              fcreneau3fin_.setText("");
            }

          } catch (final NumberFormatException nfe) {

            JOptionPane.showMessageDialog(null, MessageConstants.NOMBRE_N_EXISTE_PAS, MessageConstants.WARN,
                JOptionPane.ERROR_MESSAGE);
            fcreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    fglobal_ = new JPanel();
    fglobal_.setLayout(new GridLayout(5, 1));

    final JPanel fch4 = new JPanel();
    fch4.add(new JLabel("Horaires des jours f�ri�s"));
    fglobal_.setBorder(compound_);
    fglobal_.add(fch4);

    // panel de saisie du premier Cr�neau horaire
    final JPanel fch1 = new JPanel();

    fch1.add(new JLabel(" Cr�neau 1, Horaire: "));
    fch1.add(this.fcreneau1debut_);
    fch1.add(new JLabel("�"));
    fch1.add(this.fcreneau1fin_);
    fch1.add(new JLabel("HEURES:MINUTES"));
    fch1.setBorder(bordnormal_);
    fglobal_.add(fch1);

    // panel de saisie du deuxieme horaire
    final JPanel fch2 = new JPanel();
    fch2.add(new JLabel(" Cr�neau 2, Horaire: "));
    fch2.add(this.fcreneau2debut_);
    fch2.add(new JLabel("�"));
    fch2.add(this.fcreneau2fin_);
    fch2.add(new JLabel("HEURES:MINUTES"));
    fch2.setBorder(bordnormal_);
    fglobal_.add(fch2);

    // panel qui contient le bouton de validation:
    final JPanel fch3 = new JPanel();
    fch3.add(new JLabel(" Cr�neau 3, Horaire: "));
    fch3.add(this.fcreneau3debut_);
    fch3.add(new JLabel("�"));
    fch3.add(this.fcreneau3fin_);
    fch3.add(new JLabel("HEURES:MINUTES"));
    fch3.setBorder(bordnormal_);
    fglobal_.add(fch3);

    final JPanel fch31 = new JPanel();
    fch31.add(new JLabel("Cliquez ici pour valider: "));
    fch31.add(this.validation8_);
    fch31.setBorder(bordnormal_);

    fglobal_.setBorder(compound_);
    fglobal_.add(fch31);

    fglobal_.setBorder(compound_);
    this.typesHoraires_.add("jours f�ries", fglobal_);

    // **************************************************************************

    this.getContentPane().add(this.typesHoraires_);

    this.validate();

  }

  /**
   * Methode booleene de validation des donnes saisies
   */
  boolean controle_creationHoraire() {

    if (this.lundicreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Lundi Cr�neau 1, heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.lundicreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Lundi Cr�neau 1, heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.lundicreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Lundi Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.lundicreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! lundi Cr�neau 2: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.lundicreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Lundi Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.lundicreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Lundi Cr�neau 3: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.mardicreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mardi Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mardicreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!  mardi Cr�neau 1: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.mardicreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mardi Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mardicreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mardi Cr�neau 2: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mardicreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mardi Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mardicreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mardi Cr�neau 3: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.mercredicreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mercredi Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mercredicreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!  mercredi Cr�neau 1: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.mercredicreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mercredi Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mercredicreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mercredi Cr�neau 2: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mercredicreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mercredi Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.mercredicreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Mercredi Cr�neau 3: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.jeudicreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jeudi Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.jeudicreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!  jeudi Cr�neau 1: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.jeudicreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jeudi Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.jeudicreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jeudi Cr�neau 2: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.jeudicreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jeudi Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.jeudicreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jeudi Cr�neau 3: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.vendredicreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Vendredi Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.vendredicreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!  vendredi Cr�neau 1: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.vendredicreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Vendredi Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.vendredicreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Vendredi Cr�neau 2: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.vendredicreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Vendredi Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.vendredicreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Vendredi Cr�neau 3: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

 

    if (this.screneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Samedi Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Samedi Cr�neau 1: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.screneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Samedi Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Samedi Cr�neau 2: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Samedi Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Samedi Cr�neau 3: heure de fin manquant!", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.dcreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Dimanche Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Dimanche Cr�neau 1: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.dcreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Dimanche Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Dimanche Cr�neau 2: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Dimanche Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Dimanche Cr�neau 3: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.fcreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jours f�ri�s Cr�neau 1: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.fcreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jours f�ri�s Cr�neau 1: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.fcreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jours f�ri�s Cr�neau 2: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.fcreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jours f�ri�s Cr�neau 2: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.fcreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jours f�ri�s Cr�neau 3: heure de d�part manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.fcreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Jours f�ri�s Cr�neau 3: heure de fin manquant!",
          MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // TODO: APPROCHE OBJET ....
    return verifCoherenceCreneaux("Lundi", Float.parseFloat(this.lundicreneau1debut_.getText()), Float
        .parseFloat(this.lundicreneau1fin_.getText()), Float.parseFloat(this.lundicreneau2debut_.getText()), Float
        .parseFloat(this.lundicreneau2fin_.getText()), Float.parseFloat(this.lundicreneau3debut_.getText()), Float
        .parseFloat(this.lundicreneau3fin_.getText()))
        && verifCoherenceCreneaux("Mardi", Float.parseFloat(this.mardicreneau1debut_.getText()), Float
            .parseFloat(this.mardicreneau1fin_.getText()), Float.parseFloat(this.mardicreneau2debut_.getText()), Float
            .parseFloat(this.mardicreneau2fin_.getText()), Float.parseFloat(this.mardicreneau3debut_.getText()), Float
            .parseFloat(this.mardicreneau3fin_.getText()))
        && verifCoherenceCreneaux("Mercredi", Float.parseFloat(this.mercredicreneau1debut_.getText()), Float
            .parseFloat(this.mercredicreneau1fin_.getText()), Float.parseFloat(this.mercredicreneau2debut_.getText()),
            Float.parseFloat(this.mercredicreneau2fin_.getText()), Float.parseFloat(this.mercredicreneau3debut_
                .getText()), Float.parseFloat(this.mercredicreneau3fin_.getText()))
        && verifCoherenceCreneaux("Jeudi", Float.parseFloat(this.jeudicreneau1debut_.getText()), Float
            .parseFloat(this.jeudicreneau1fin_.getText()), Float.parseFloat(this.jeudicreneau2debut_.getText()), Float
            .parseFloat(this.jeudicreneau2fin_.getText()), Float.parseFloat(this.jeudicreneau3debut_.getText()), Float
            .parseFloat(this.jeudicreneau3fin_.getText()))
        && verifCoherenceCreneaux("Vendredi", Float.parseFloat(this.vendredicreneau1debut_.getText()), Float
            .parseFloat(this.vendredicreneau1fin_.getText()), Float.parseFloat(this.vendredicreneau2debut_.getText()),
            Float.parseFloat(this.vendredicreneau2fin_.getText()), Float.parseFloat(this.vendredicreneau3debut_
                .getText()), Float.parseFloat(this.vendredicreneau3fin_.getText()))
        && verifCoherenceCreneaux("Samedi", Float.parseFloat(this.screneau1debut_.getText()), Float
            .parseFloat(this.screneau1fin_.getText()), Float.parseFloat(this.screneau2debut_.getText()), Float
            .parseFloat(this.screneau2fin_.getText()), Float.parseFloat(this.screneau3debut_.getText()), Float
            .parseFloat(this.screneau3fin_.getText()))
        && verifCoherenceCreneaux("Dimanche", Float.parseFloat(this.dcreneau1debut_.getText()), Float
            .parseFloat(this.dcreneau1fin_.getText()), Float.parseFloat(this.dcreneau2debut_.getText()), Float
            .parseFloat(this.dcreneau2fin_.getText()), Float.parseFloat(this.dcreneau3debut_.getText()), Float
            .parseFloat(this.dcreneau3fin_.getText()))
        && verifCoherenceCreneaux("F�ri�", Float.parseFloat(this.fcreneau1debut_.getText()), Float
            .parseFloat(this.fcreneau1fin_.getText()), Float.parseFloat(this.fcreneau2debut_.getText()), Float
            .parseFloat(this.fcreneau2fin_.getText()), Float.parseFloat(this.fcreneau3debut_.getText()), Float
            .parseFloat(this.fcreneau3fin_.getText()))

    ;

    // tous les tests ont t ngatifs, les donnes sont donc correctes.
    // return true;
  }

  /**
   * Methode de controle de creation des horaires.
   */
  void creationHoraire() {

    if (controle_creationHoraire()) {
      // creation d'un nouvel objet horaire:

      horaire_.lundiCreneau1HeureDep = Float.parseFloat(this.lundicreneau1debut_.getText());
      horaire_.lundiCreneau1HeureArrivee = Float.parseFloat(this.lundicreneau1fin_.getText());
      horaire_.lundiCreneau2HeureDep = Float.parseFloat(this.lundicreneau2debut_.getText());
      horaire_.lundiCreneau2HeureArrivee = Float.parseFloat(this.lundicreneau2fin_.getText());
      horaire_.lundiCreneau3HeureDep = Float.parseFloat(this.lundicreneau3debut_.getText());
      horaire_.lundiCreneau3HeureArrivee = Float.parseFloat(this.lundicreneau3fin_.getText());

      horaire_.mardiCreneau1HeureDep = Float.parseFloat(this.mardicreneau1debut_.getText());
      horaire_.mardiCreneau1HeureArrivee = Float.parseFloat(this.mardicreneau1fin_.getText());
      horaire_.mardiCreneau2HeureDep = Float.parseFloat(this.mardicreneau2debut_.getText());
      horaire_.mardiCreneau2HeureArrivee = Float.parseFloat(this.mardicreneau2fin_.getText());
      horaire_.mardiCreneau3HeureDep = Float.parseFloat(this.mardicreneau3debut_.getText());
      horaire_.mardiCreneau3HeureArrivee = Float.parseFloat(this.mardicreneau3fin_.getText());

      horaire_.mercrediCreneau1HeureDep = Float.parseFloat(this.mercredicreneau1debut_.getText());
      horaire_.mercrediCreneau1HeureArrivee = Float.parseFloat(this.mercredicreneau1fin_.getText());
      horaire_.mercrediCreneau2HeureDep = Float.parseFloat(this.mercredicreneau2debut_.getText());
      horaire_.mercrediCreneau2HeureArrivee = Float.parseFloat(this.mercredicreneau2fin_.getText());
      horaire_.mercrediCreneau3HeureDep = Float.parseFloat(this.mercredicreneau3debut_.getText());
      horaire_.mercrediCreneau3HeureArrivee = Float.parseFloat(this.mercredicreneau3fin_.getText());

      horaire_.jeudiCreneau1HeureDep = Float.parseFloat(this.jeudicreneau1debut_.getText());
      horaire_.jeudiCreneau1HeureArrivee = Float.parseFloat(this.jeudicreneau1fin_.getText());
      horaire_.jeudiCreneau2HeureDep = Float.parseFloat(this.jeudicreneau2debut_.getText());
      horaire_.jeudiCreneau2HeureArrivee = Float.parseFloat(this.jeudicreneau2fin_.getText());
      horaire_.jeudiCreneau3HeureDep = Float.parseFloat(this.jeudicreneau3debut_.getText());
      horaire_.jeudiCreneau3HeureArrivee = Float.parseFloat(this.jeudicreneau3fin_.getText());

      horaire_.vendrediCreneau1HeureDep = Float.parseFloat(this.vendredicreneau1debut_.getText());
      horaire_.vendrediCreneau1HeureArrivee = Float.parseFloat(this.vendredicreneau1fin_.getText());
      horaire_.vendrediCreneau2HeureDep = Float.parseFloat(this.vendredicreneau2debut_.getText());
      horaire_.vendrediCreneau2HeureArrivee = Float.parseFloat(this.vendredicreneau2fin_.getText());
      horaire_.vendrediCreneau3HeureDep = Float.parseFloat(this.vendredicreneau3debut_.getText());
      horaire_.vendrediCreneau3HeureArrivee = Float.parseFloat(this.vendredicreneau3fin_.getText());

      horaire_.samediCreneau1HeureDep = Float.parseFloat(this.screneau1debut_.getText());
      horaire_.samediCreneau1HeureArrivee = Float.parseFloat(this.screneau1fin_.getText());
      horaire_.samediCreneau2HeureDep = Float.parseFloat(this.screneau2debut_.getText());
      horaire_.samediCreneau2HeureArrivee = Float.parseFloat(this.screneau2fin_.getText());
      horaire_.samediCreneau3HeureDep = Float.parseFloat(this.screneau3debut_.getText());
      horaire_.samediCreneau3HeureArrivee = Float.parseFloat(this.screneau3fin_.getText());

      horaire_.dimancheCreneau1HeureDep = Float.parseFloat(this.dcreneau1debut_.getText());
      horaire_.dimancheCreneau1HeureArrivee = Float.parseFloat(this.dcreneau1fin_.getText());
      horaire_.dimancheCreneau2HeureDep = Float.parseFloat(this.dcreneau2debut_.getText());
      horaire_.dimancheCreneau2HeureArrivee = Float.parseFloat(this.dcreneau2fin_.getText());
      horaire_.dimancheCreneau3HeureDep = Float.parseFloat(this.dcreneau3debut_.getText());
      horaire_.dimancheCreneau3HeureArrivee = Float.parseFloat(this.dcreneau3fin_.getText());

      horaire_.ferieCreneau1HeureDep = Float.parseFloat(this.fcreneau1debut_.getText());
      horaire_.ferieCreneau1HeureArrivee = Float.parseFloat(this.fcreneau1fin_.getText());
      horaire_.ferieCreneau2HeureDep = Float.parseFloat(this.fcreneau2debut_.getText());
      horaire_.ferieCreneau2HeureArrivee = Float.parseFloat(this.fcreneau2fin_.getText());
      horaire_.ferieCreneau3HeureDep = Float.parseFloat(this.fcreneau3debut_.getText());
      horaire_.ferieCreneau3HeureArrivee = Float.parseFloat(this.fcreneau3fin_.getText());

      // verification des donnes saisies:
      horaire_.affichage();

      // destruction de la frame:
      this.dispose();

    }

  }

  /**
   * methode de verifiation de la saisie des horaires.
   * 
   * @param _texte texte qui specifie de quel horairez il s agit
   * @param _a1 Cr�neau 1 debut
   * @param _a2 Cr�neau 1 fin
   * @param _b1 Cr�neau 2 debut
   * @param _b2 Cr�neau 2 fin
   * @param _c1 Cr�neau 3 debut
   * @param _c2 Cr�neau 3 fin
   * @return
   */
  boolean verifCoherenceCreneaux(final String _texte, final float _a1, final float _a2, final float _b1,
      final float _b2, final float _c1, final float _c2) {
    if (_a1 > _a2) {
      JOptionPane.showMessageDialog(null, "Erreur horaires " + _texte
          + "!\n Cr�neau 1: horaire d�part sup�rieur �celui d'arriv�e", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (_b1 > _b2) {
      JOptionPane.showMessageDialog(null, "Erreur horaires " + _texte
          + "!\n Cr�neau 2: horaire d�part sup�rieur � celui d'arriv�e", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (_c1 > _c2) {
      JOptionPane.showMessageDialog(null, "Erreur horaires " + _texte
          + "!\n Cr�neau 3: horaire d�part sup�rieur �celui d'arriv�e", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // inclusions des horaires:
    if ((_b1 < _a2 && _b1 > _a1) || (_b2 < _a2 && _b2 > _a1) || (_a1 > _b1 && _a1 < _b2) || (_a2 > _b1 && _a2 < _b2)) {
      JOptionPane.showMessageDialog(null,
          "Erreur horaires " + _texte + "!\n le Cr�neau 2 est inclus dans le Cr�neau 1", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if ((_b1 < _c2 && _b1 > _c1) || (_b2 < _c2 && _b2 > _c1) || (_c1 > _b1 && _c1 < _b2) || (_c2 > _b1 && _c2 < _b2)) {
      JOptionPane.showMessageDialog(null,
          "Erreur horaires " + _texte + "!\n le Cr�neau 2 est inclus dans le Cr�neau 3", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if ((_c1 < _a2 && _c1 > _a1) || (_c2 < _a2 && _c2 > _a1) || (_a1 > _c1 && _a1 < _c2) || (_a2 > _c1 && _a2 < _c2)) {
      JOptionPane.showMessageDialog(null,
          "Erreur horaires " + _texte + "!\n le Cr�neau 3 est inclus dans le Cr�neau 1", MessageConstants.WARN,
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

}
