package org.fudaa.fudaa.sipor.ui.html;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.memoire.bu.BuDialogError;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameGenerationRappelDonnees;

/**
 * Classe qui permet de g�n�rer un fichier HTML
 * par exemple un rappel des donn�es
 *@version $Version$
 * @author hadoux
 *
 */

public class SiporGenereNoteHtml {

    
  
public static void rappelDonnees(File _fichier, SiporFrameGenerationRappelDonnees _f,SiporDataSimulation _d)  
{
  //chaine contenant le rapport 
  String rapport="";
  
  int indicePartie=1;
  int chapitre=1;
  
  
  
  //creation d'un titre
  rapport+=creertitre(_f.titre_.getText(),_f.auteur_.getText(),_f.ZoneText_.getText());
  
  
  //creation du sommaire
  rapport+=creerSommaire(_f);
  
  
  
  
  
  rapport+="<br><br><h2>1.les param�tres </h2><br><br>";
  //insertion des donn�"es generales
  if(_f.dg_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les donn�es g�n�rales");
    rapport+=creerDonneesgenerales(_d, _f);
  }
//  insertion des gares
  if(_f.gare_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les gares");
    rapport+=creerTableauGares(_d,_f);
  }
//  insertion des bassins 
  if(_f.bassin_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les bassins");
    rapport+=creerTableauBassins(_d,_f);
  }
  
  //insertion des chenaux 
  if(_f.chenal_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les chenaux");
    rapport+=creerTableauChenaux(_d,_f);
  }
  
  
  if(_f.cercle_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les cercles d'�vitage");
    rapport+=creerTableauCercles(_d,_f);
  }
  
  if(_f.ecluse_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les �cluses");
    rapport+=creerTableauEcluses(_d,_f);
  }
  
  if(_f.quai_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les quais");
    rapport+=creerTableauQuais(_d,_f);
  }
  
  if(_f.maree_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les donn�es de mar�e");
    rapport+=creerDonneesMarees(_d,_f);
  }
  
  if(_f.nav_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les cat�gories de navire");
    rapport+=creerTableauCategorie(_d,_f);
  }
  
  // leeeeeeeeeessssssssssss topologies
  if((_f.topo1_.isSelected() ||_f.topo2_.isSelected() ||_f.topo3_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected()))
   rapport+="<br><br><h2>2.la topologie </h2><br><br>";
  
  
  chapitre=1;
  if(_f.topo1_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des bassins");
    rapport+=creerTableauTopologieBassins(_d,_f);
  
  }
  if(_f.topo2_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des chenaux");
    rapport+=creerTableauTopologieChenaux(_d,_f);
  
  }
  if(_f.topo3_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des cercles d'�vitage");
    rapport+=creerTableauTopologieCercles(_d,_f);
  
  }
  if(_f.topo4_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des �cluses");
    rapport+=creerTableauTopologieEcluses(_d,_f);
  
  }
  if(_f.topo5_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"mod�le du port");
    /*
    new BuDialogMessage(_d.getApplication_().getApp(),_d.getApplication_().isSipor_,
           "Veuillez specifier l'image du port � exporter:")
.activate();
*/
      rapport+=creerModele(_d,_f,_fichier.getAbsolutePath());
  }
  
  // les regles de navigations
  
  
  int partie=3;
  if(_f.topo1_.isSelected() ||_f.topo2_.isSelected() ||_f.topo3_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected())
  partie=3;
  else partie=2;
  
  if((_f.regle1_.isSelected() ||_f.regle2_.isSelected() ||_f.regle3_.isSelected()||_f.regle5_.isSelected() ))
    rapport+="<br><br><h2>"+partie+".les r�gles de navigation </h2><br><br>";
    
  chapitre=1;
  
  if(_f.regle1_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"r�gles de croisement des chenaux");
    rapport+=creerTableauCroisementChenaux(_d,_f);
  
  }
  if(_f.regle2_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"r�gles de croisement des cercles d'�vitage");
    rapport+=creerTableauCroisementCercles(_d,_f);
  
  }
  
  if(_f.regle3_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"dur�es de parcours des chenaux (h.min)");
    rapport+=creerTableauDureeChenaux(_d,_f);
    
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"d�lais de remplissage des SAS (h.min)");
    rapport+=creerTableauRemplissageSAS(_d,_f);
  
  }
  
  if(_f.regle4_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"dur�es de parcours des cercles d'�vitage (h.min)");
    rapport+=creerTableauDureeCercles(_d,_f);
  
  }
  if(_f.regle5_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,SiporConstantes.ConstanteTitreRapportHtm);
    rapport+=creerTableauGenesCercles(_d,_f);
  
  }
  
  //ecriture du contenu du rapport g�n�r�
   
     try {
       File file =CtuluLibFile.appendExtensionIfNeeded(_fichier, "html");
       final FileWriter fw = new FileWriter(file);
       fw.write(rapport, 0, rapport.length());
       fw.flush();
       fw.close();
     } catch (final IOException _e1) {
       new BuDialogError(_d.getApplication().getApp(),_d.getApplication().INFORMATION_SOFT,
           "Impossible d'�crire le fichier sur le disque.")
      .activate();
      
       return ;
     }
     
     //afficher el rapport dans l editeur
     _f.editeur.setDocumentText(rapport);
     
}


public static void apercuRapport(SiporFrameGenerationRappelDonnees _f,SiporDataSimulation _d) 
{
  //chaine contenant le rapport 
  String rapport="";
  
  //indice qui correponda � l'indice de la partie
  int indicePartie=1;
  int chapitre=1;
  
  
  
  //creation d'un titre
  rapport+=creertitre(_f.titre_.getText(),_f.auteur_.getText(),_f.ZoneText_.getText());
  
  
  //creation du sommaire
  rapport+=creerSommaire(_f);
  
  
  
  
  
  rapport+="<br><br><h2>1.les param�tres </h2><br><br>";
  //insertion des donn�es generales
  if(_f.dg_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les donn�es g�n�rales");
    rapport+=creerDonneesgenerales(_d, _f);
  }
//  insertion des gares
  if(_f.gare_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les gares");
    rapport+=creerTableauGares(_d,_f);
  }
//  insertion des bassins 
  if(_f.bassin_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les bassins");
    rapport+=creerTableauBassins(_d,_f);
  }
  
  //insertion des chenaux 
  if(_f.chenal_.isSelected())
  {
    //ecriture du tableau des chenaux:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les chenaux");
    rapport+=creerTableauChenaux(_d,_f);
  }
  
  
  if(_f.cercle_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les cercles d'�vitage");
    rapport+=creerTableauCercles(_d,_f);
  }
  
  if(_f.ecluse_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les �cluses");
    rapport+=creerTableauEcluses(_d,_f);
  }
  
  if(_f.quai_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les quais");
    rapport+=creerTableauQuais(_d,_f);
  }
  
  if(_f.maree_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les donn�es de mar�e");
    rapport+=creerDonneesMarees(_d,_f);
  }
  
  if(_f.nav_.isSelected())
  {
    //ecriture du tableau des cercles d evitage:
    rapport+=creerPartie(1,chapitre++,indicePartie++,"les cat�gories de navire");
    rapport+=creerTableauCategorie(_d,_f);
  }
  
  // leeeeeeeeeessssssssssss topologies
  if((_f.topo1_.isSelected() ||_f.topo2_.isSelected() ||_f.topo3_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected()))
   rapport+="<br><br><h2>2.la topologie </h2><br><br>";
  
  
  chapitre=1;
  if(_f.topo1_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des bassins");
    rapport+=creerTableauTopologieBassins(_d,_f);
  
  }
  if(_f.topo2_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des chenaux");
    rapport+=creerTableauTopologieChenaux(_d,_f);
  
  }
  if(_f.topo3_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des cercles d'�vitage");
    rapport+=creerTableauTopologieCercles(_d,_f);
  
  }
  if(_f.topo4_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"topologie des �cluses");
    rapport+=creerTableauTopologieEcluses(_d,_f);
  
  }
  if(_f.topo5_.isSelected())
  {
    rapport+=creerPartie(2,chapitre++,indicePartie++,"mod�le du port");
    _f.fichierImagePort_=_d.getProjet_().getFichier()+".jpg";
    
    rapport+=creerModele(_d,_f,_f.fichierImagePort_);
      //rapport+="<br /><br /> Aper�u non disponible en mode aper�u, il faut exporter le rapport pour cr�er le fichier image du sch�ma du port";
  }
  
  // les regles de navigations
  
  
  int partie=3;
  if(_f.topo1_.isSelected() ||_f.topo2_.isSelected() ||_f.topo3_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected())
  partie=3;
  else partie=2;
  
  if((_f.regle1_.isSelected() ||_f.regle2_.isSelected() ||_f.regle3_.isSelected() ||_f.regle5_.isSelected()))
    rapport+="<br><br><h2>"+partie+".les r�gles de navigation </h2><br><br>";
    
  chapitre=1;
  
  if(_f.regle1_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"r�gles de croisement des chenaux");
    rapport+=creerTableauCroisementChenaux(_d,_f);
  
  }
  if(_f.regle2_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"r�gles de croisement des cercles d'�vitage");
    rapport+=creerTableauCroisementCercles(_d,_f);
  
  }
  
  if(_f.regle3_.isSelected())
  {
	  rapport+=creerPartie(partie,chapitre++,indicePartie++,"dur�es de parcours des chenaux (h.min)");
	  rapport+=creerTableauDureeChenaux(_d,_f);
	   
	 rapport+=creerPartie(partie,chapitre++,indicePartie++,"d�lais de remplissage des SAS (h.min)");
	 rapport+=creerTableauRemplissageSAS(_d,_f);
	  
    
    
  }
  
  if(_f.regle4_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,"dur�es de parcours des cercles d'�vitage (h.min)");
    rapport+=creerTableauDureeCercles(_d,_f);
  
  }
  if(_f.regle5_.isSelected())
  {
    rapport+=creerPartie(partie,chapitre++,indicePartie++,SiporConstantes.ConstanteTitreRapportHtm);
    rapport+=creerTableauGenesCercles(_d,_f);
  
  }
  
     
     //afficher el rapport dans l editeur
     _f.editeur.setDocumentText(rapport);
     
}



public static String creerSommaire(SiporFrameGenerationRappelDonnees _f)
{
  String sommaire="";
  int num=1;
  
  sommaire+="<table width=\"50%\" border=0 ><th rowspan=\"2\" ><H2><font color=\"#000000\"> SOMMAIRE </font></H1></th><tr></tr>";

  
  sommaire+="<tr><td ><font color=\"#000000\"><h3>1.les param�tres </h3></font></td></tr>";
  
  
  if(_f.dg_.isSelected())
  sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les donn�es g�n�rales</font></a></font></td></tr>";
  
  if(_f.gare_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les gares </font></a></font></td></tr>";
    
  if(_f.bassin_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les bassins</font></a></font></td></tr>";
      
  if(_f.chenal_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les chenaux</font></a></font></td></tr>";
    
  if(_f.cercle_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les cercles d'�vitage</font></a></font></td></tr>";
    
  if(_f.ecluse_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les �cluses</font></a></font></td></tr>";
        
  if(_f.quai_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les quais</font></a></font></td></tr>";
  if(_f.maree_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les mar�es</font></a></font></td></tr>";
  if(_f.nav_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">les cat�gories de navire</font></a></font></td></tr>";
  
  int partie=3;
  if((_f.topo1_.isSelected() ||_f.topo2_.isSelected() ||_f.topo3_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected()))
  sommaire+="<tr><td ><font color=\"#000000\"><h3>2.la topologie </h3></font></td></tr>";
  else
    partie=2;
  
  int num2=1;
  if(_f.topo1_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">topologie des bassins</font></a></font></td></tr>";
  if(_f.topo2_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">topologie des chenaux</font></a></font></td></tr>";
  if(_f.topo3_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">topologie des cercles d'�vitage</font></a></font></td></tr>";
  if(_f.topo4_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">topologie des �cluses</font></a></font></td></tr>";
  if(_f.topo5_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">mod�le du port</font></a></font></td></tr>";
  
  
  if((_f.regle1_.isSelected() ||_f.regle2_.isSelected() ||_f.regle3_.isSelected() ||_f.regle5_.isSelected()))
    sommaire+="<tr><td ><font color=\"#000000\"><h3>"+partie+".les r�gles de navigation </h3></font></td></tr>";
  
  
  num2=1;
  if(_f.regle1_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">croisement des chenaux</font></a></font></td></tr>";
  if(_f.regle2_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">croisement des cercles d'�vitage</font></a></font></td></tr>";
  if(_f.regle3_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">dur�es de parcours des chenaux</font></a></font></td></tr>";
  if(_f.regle4_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">dur�es de parcours des cercles d'�vitage</font></a></font></td></tr>";
  if(_f.regle5_.isSelected())
    sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">"+SiporConstantes.ConstanteTitreRapportHtm+"</font></a></font></td></tr>";

  
  sommaire+="</table><br><br><br><br>";
  
  return sommaire;
  
}

public static String creertitre(String titre,String auteur,String commentaire)
{
  return
  "<table width=\"100%\" border=0 bgcolor=\"#EEEEEEE\" bordercolor=\"000000\"><tr><td><H1><center><font color=\"#000000\">Rappel de donn�es</font></center></H1></td></tr></table><br><br>" +
  "<table width=\"100%\" border=0 bgcolor=\"#EEEEEEE\" bordercolor=\"000000\"><tr><td><H1><center><font color=\"#000000\">"+titre+"</font></center></H1></td></tr></table><br><br>" +
      "<H3>"+auteur+" </H3><br><br>" +
          "<br><br> " +commentaire+
          "<br><br><br><br>";
}



/**
 * Methode qui permet d'�crire un titre sous un format specifique.
 * @param num numero de al partie.
 * @param titre titre de la partie.
 * @return la chaine coreespondante
 */
public static String creerPartie(int partie,int chapitre,int num,String titre)
{
  return
  "<table width=\"90%\" border=1 bgcolor=\"   #EEEEEEE\"><tr><td><font color=\"#3300BB\"><b id=\"index"+num+"\">"+partie+"."+chapitre+". "+titre+"</b></font></td></tr></table>";
}


public static String creerModele(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f,String fichier)
{
  String chaine="";


  //creation du fichier du modele


  //creation du dessin si il existe en lancant la fenetre cach�e
  _d.getApplication().activerModelisation();
  _d.getApplication().gestionModelisation_.setVisible(false);

  File file=new File((fichier));
  CtuluImageExport.exportImageFor(_d.getApplication(),file, _d.getApplication().gestionModelisation_.panelDessin_);


  //fermeture de la fenetre de dessin
  _d.getApplication().gestionModelisation_.setVisible(true);
  _d.getApplication().gestionModelisation_.dispose();


  System.out.println("fichier: "+file.getAbsolutePath());


  //insertion du modele
  chaine+="<br><br><img src=\""+file.getAbsolutePath()+"\"></img><br><br>";

  
  return chaine;
}

public static String creerDonneesgenerales(final SiporDataSimulation _d,final SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\"> graine d'initialisation</th> <td bgcolor=\"    #EEEEEEE \">"+_d.getParams_().donneesGenerales.graine+" </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> nombre de jours de la simulation</th> <td bgcolor=\"   #EEEEEEE \">"+_d.getParams_().donneesGenerales.nombreJours+" </td></tr>";
    String jour="";
    switch(_d.getParams_().donneesGenerales.jourDepart)
    {
    case 1: jour="Lundi";break;
    case 2: jour="Mardi";break;
    case 3: jour="Mercredi";break;
    case 4: jour="Jeudi";break;
    case 5: jour="Vendredi";break;
    case 6: jour="Samedi";break;
    case 7: jour="Dimanche";break;
    }
    chaine+="<tr><th bgcolor=\"AACCEE\"> jour de d�part de la simulation</th> <td bgcolor=\"    #EEEEEEE \">"+jour+" </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> pourcentage du pied de pilote</th> <td bgcolor=\"    #EEEEEEE \">"+_d.getParams_().donneesGenerales.piedDePilote+"% </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> nombre de jours f�ri�s</th> <td bgcolor=\"   #EEEEEEE \">"+_d.getParams_().donneesGenerales.nombreJoursFeries+" </td></tr>";
    
      
    
    
    
  
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}


public static String creerDonneesMarees(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\"> p�riode vive eau</th> <td bgcolor=\"   #EEEEEEE \">"+(float)_d.getParams_().maree.periodeViveEaux+" </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> p�riode mar�e</th> <td bgcolor=\"    #EEEEEEE \">"+(float)_d.getParams_().maree.periodeMaree+" </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> hauteur moyenne de la mer</th> <td bgcolor=\"    #EEEEEEE \">"+(float)_d.getParams_().maree.hauteurMoyenneMer+"m </td></tr>";
    chaine+="</table></p><br>";
    
    chaine+="<p><center>basse mer</center><p ><table border=1 ><tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> p�riode vive eau</th> <td bgcolor=\"   #EEEEEEE \">"+(float)_d.getParams_().maree.niveauBasseMerViveEau+"m </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> p�riode morte eau</th> <td bgcolor=\"    #EEEEEEE \">"+(float)_d.getParams_().maree.niveauBasseMerMorteEau+"m </td></tr>";
        chaine+="</table></p><br>";
    
    
       chaine+="<p><center>haute mer</center><p ><table border=1 ><tr>";
    
       chaine+="<tr><th bgcolor=\"AACCEE\"> p�riode vive eau</th> <td bgcolor=\"    #EEEEEEE \">"+(float)_d.getParams_().maree.niveauHauteMerViveEau+"m </td></tr>";
    chaine+="<tr><th bgcolor=\"AACCEE\"> p�riode morte eau</th> <td bgcolor=\"    #EEEEEEE \">"+(float)_d.getParams_().maree.niveauHauteMerMorteEau+"m </td></tr>";
        
    
      
    
    
    
  
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}


public static String creerTableauGares(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeGare_().getListeGares_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeGare_().retournerGare(i)+"</th>";
    
      
    
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}

public static String creerTableauBassins(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListebassin_().getListeBassins_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListebassin_().retournerBassin(i)+"</th>";
    
      
    
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}


/**
 * Methode d'ecriture des cercles.
 * @param _d
 * @param _f
 * @return
 */
public static String creerTableauCercles(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeCercle_().getListeCercles_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeCercle_().retournerCercle(i).getNom_()+"</th>";
    
      
    
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}



/**
 * methode d'ecriture des chenaux
 * @param _d
 * @param _f
 * @return
 */
public static String creerTableauChenaux(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  
  
  if(_f.chenalCr_.isSelected())
  {
    chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  if(_f.chenalPro_.isSelected()) chaine+="<th rowspan=\"2\"  bgcolor=\"   #EEEEEEE\">profondeur (m)</th>";
  if(_f.chenalMar_.isSelected()) chaine+="<th rowspan=\"2\"  bgcolor=\"   #EEEEEEE\">soumis � la mar�e</th>";
  }
  else
  {
    chaine+="<th  align=\"center\" bgcolor=\"   #EEEEEEE\">Nom</th>";
    if(_f.chenalPro_.isSelected()) chaine+="<th   bgcolor=\"    #EEEEEEE\">profondeur (m)</th>";
    if(_f.chenalMar_.isSelected()) chaine+="<th   bgcolor=\"    #EEEEEEE\">soumis � la mar�e</th>";
    }
  
  if(_f.chenalCr_.isSelected())
    {
    chaine+="<th colspan=\"3\"  bgcolor=\"    #EEEEEEE\">Cr�naux (h.min)</th>";
    chaine+="</tr><tr bgcolor=\"    #EEEEEEE\"><th>Creneau 1</th><th>Cr�neau 2</th><th>Cr�neau 3</th>";
    }
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeChenal_().getListeChenaux_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeChenal_().retournerChenal(i).getNom_()+"</th>";
    
    //caracteristiques
    if(_f.chenalPro_.isSelected()) chaine+="<td>"+(float)_d.getListeChenal_().retournerChenal(i).getProfondeur_()+"</td> ";
    if(_f.chenalMar_.isSelected()) 
         if(_d.getListeChenal_().retournerChenal(i).isSoumisMaree_())
             chaine+="<td>oui</td> ";
         else
           chaine+="<td>non</td> ";
    if(_f.chenalCr_.isSelected())
    {
      chaine+="<td>"+(float)_d.getListeChenal_().retournerChenal(i).getH_().semaineCreneau1HeureDep+"-"+(float)_d.getListeChenal_().retournerChenal(i).getH_().semaineCreneau1HeureArrivee+"</td> ";
      chaine+="<td>"+(float)_d.getListeChenal_().retournerChenal(i).getH_().semaineCreneau2HeureDep+"-"+(float)_d.getListeChenal_().retournerChenal(i).getH_().semaineCreneau2HeureArrivee+"</td> ";
      chaine+="<td>"+(float)_d.getListeChenal_().retournerChenal(i).getH_().semaineCreneau3HeureDep+"-"+(float)_d.getListeChenal_().retournerChenal(i).getH_().semaineCreneau3HeureArrivee+"</td> ";
      
      
    }
    
    
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}
  
public static String creerTableauEcluses(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  if(_f.ecluseCr_.isSelected())
  {
  chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  
  if(_f.ecltaille_.isSelected())
    {
      chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">longueur (m)</th>";
      chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">largeur (m)</th>";
    }
  
  if(_f.eclDur_.isSelected()) 
    {
      chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">dur�e �cluse (h.min)</th>";
      chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">dur�e fausse bassin�e (h.min)</th>";
      chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">dur�e passage � l'�tale (h.min)</th>";
    }
  
  if(_f.ecluseIndispo_.isSelected())
  {
    chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">loi indisponibilit�</th>";
    chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">dur�e moyenne (h.min)</th>";
      
  }
  
  }
  else
  {
    chaine+="<th   bgcolor=\"   #EEEEEEE\">Nom</th>";
    
    if(_f.ecltaille_.isSelected())
      {
        chaine+="<th   bgcolor=\"   #EEEEEEE\">longueur (m)</th>";
        chaine+="<th   bgcolor=\"   #EEEEEEE\">largeur (m)</th>";
      }
    
    if(_f.eclDur_.isSelected()) 
      {
        chaine+="<th   bgcolor=\"   #EEEEEEE\">dur�e �cluse (h.min)</th>";
        chaine+="<th   bgcolor=\"   #EEEEEEE\">dur�e fausse bassin�e (h.min)</th>";
        chaine+="<th   bgcolor=\"   #EEEEEEE\">dur�e passage � l'�tale (h.min)</th>";
      }
    
    if(_f.ecluseIndispo_.isSelected())
    {
      chaine+="<th   bgcolor=\"   #EEEEEEE\">loi indisponibilit�</th>";
      chaine+="<th   bgcolor=\"   #EEEEEEE\">dur�e moyenne (h.min)</th>";
        
    }
    
    }
  
  if(_f.ecluseCr_.isSelected())
    {
    chaine+="<th colspan=\"3\"  bgcolor=\"    #EEEEEEE\">Cr�naux (h.min)</th>";
    chaine+="</tr><tr bgcolor=\"    #EEEEEEE\"><th>Cr�neau 1</th><th>Cr�neau 2</th><th>Cr�neau 3</th>";
    }
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeEcluse_().getListeEcluses_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeEcluse_().retournerEcluse(i).getNom_()+"</th>";
    
    //caracteristiques
    if(_f.ecltaille_.isSelected())
    {
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getLongueur_()+"</td>";
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getLargeur_()+"</td>";
    }
    
    if(_f.eclDur_.isSelected())
    {
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getTempsEclusee_()+"</td>";
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getTempsFausseBassinnee_()+"</td>";
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getDureePassageEtale_()+"</td>";
    }
    
    if(_f.ecluseIndispo_.isSelected())
    {
      if(_d.getListeEcluse_().retournerEcluse(i).getLoiIndispo_()==0)
      chaine+="<td> Erlang</td>";
      else
        if(_d.getListeEcluse_().retournerEcluse(i).getLoiIndispo_()==1)
          chaine+="<td> D�terministe</td>";
        else
          chaine+="<td> Journali�re</td>";
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getDureeIndispo_()+"</td>";
      
    }
    
    if(_f.ecluseCr_.isSelected())
    {
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getH_().semaineCreneau1HeureDep+"-"+(float)_d.getListeEcluse_().retournerEcluse(i).getH_().semaineCreneau1HeureArrivee+"</td> ";
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getH_().semaineCreneau2HeureDep+"-"+(float)_d.getListeEcluse_().retournerEcluse(i).getH_().semaineCreneau2HeureArrivee+"</td> ";
      chaine+="<td>"+(float)_d.getListeEcluse_().retournerEcluse(i).getH_().semaineCreneau3HeureDep+"-"+(float)_d.getListeEcluse_().retournerEcluse(i).getH_().semaineCreneau3HeureArrivee+"</td> ";
      
      
    }
    
    
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}



public static String creerTableauQuais(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  if(_f.quaiCr_.isSelected()){
  
  chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  
  if(_f.quaitaille_.isSelected())
    {
     chaine+="<th rowspan=\"2\"  bgcolor=\"   #EEEEEEE\">bassin d'appartenance</th>";  
     chaine+="<th rowspan=\"2\"  bgcolor=\"   #EEEEEEE\">longueur (m)</th>";
     chaine+="<th rowspan=\"2\"  bgcolor=\"   #EEEEEEE\">d�halage autoris�</th>";
      
    }
  
  
  
  if(_f.quaiIndispo_.isSelected())
  {
    chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">loi indisponibilit�</th>";
    chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">dur�e moyenne (h.min)</th>";
      
  }
  
  }
  else{
    
    chaine+="<th   bgcolor=\"   #EEEEEEE\">Nom</th>";
    
    if(_f.quaitaille_.isSelected())
      {
       chaine+="<th   bgcolor=\"    #EEEEEEE\">bassin d'appartenance</th>";  
       chaine+="<th   bgcolor=\"    #EEEEEEE\">longueur (m)</th>";
       chaine+="<th   bgcolor=\"    #EEEEEEE\">d�halage autoris�</th>";
        
      }
    
    
    
    if(_f.quaiIndispo_.isSelected())
    {
      chaine+="<th   bgcolor=\"   #EEEEEEE\">loi indisponibilit�</th>";
      chaine+="<th   bgcolor=\"   #EEEEEEE\">dur�e moyenne (h.min)</th>";
        
    }
    
    }
    
  
  
  if(_f.quaiCr_.isSelected())
    {
    chaine+="<th colspan=\"3\"  bgcolor=\"    #EEEEEEE\">Cr�naux (h.min)</th>";
    chaine+="</tr><tr bgcolor=\"    #EEEEEEE\"><th>Cr�neau 1</th><th>Cr�neau 2</th><th>Cr�neau 3</th>";
    }
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getlQuais_().getlQuais_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getlQuais_().retournerQuais(i).getNom()+"</th>";
    
    //caracteristiques
    if(_f.quaitaille_.isSelected())
    {
      chaine+="<td>"+_d.getListebassin_().retournerBassin(_d.getlQuais_().retournerQuais(i).getBassin_())+"</td>";
      chaine+="<td>"+(float)_d.getlQuais_().retournerQuais(i).getLongueur_()+"</td>";
      if(_d.getlQuais_().retournerQuais(i).isDehalageAutorise_())
      chaine+="<td>oui</td>";
      else
        chaine+="<td>non</td>";
    }
    
    
    if(_f.quaiIndispo_.isSelected())
    {
      if(_d.getlQuais_().retournerQuais(i).getLoiIndispo_()==0)
      chaine+="<td> Erlang</td>";
      else
        if(_d.getlQuais_().retournerQuais(i).getLoiIndispo_()==1)
          chaine+="<td> D�terministe</td>";
        else
          chaine+="<td> Journali�re</td>";
      chaine+="<td>"+(float)_d.getlQuais_().retournerQuais(i).getDureeIndispo_()+"</td>";
      
    }
    
    if(_f.quaiCr_.isSelected())
    {
      chaine+="<td>"+(float)_d.getlQuais_().retournerQuais(i).getH_().semaineCreneau1HeureDep+"-"+(float)_d.getlQuais_().retournerQuais(i).getH_().semaineCreneau1HeureArrivee+"</td> ";
      chaine+="<td>"+(float)_d.getlQuais_().retournerQuais(i).getH_().semaineCreneau2HeureDep+"-"+(float)_d.getlQuais_().retournerQuais(i).getH_().semaineCreneau2HeureArrivee+"</td> ";
      chaine+="<td>"+(float)_d.getlQuais_().retournerQuais(i).getH_().semaineCreneau3HeureDep+"-"+(float)_d.getlQuais_().retournerQuais(i).getH_().semaineCreneau3HeureArrivee+"</td> ";
      
      
    }
    
    
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}


public static String creerTableauCategorie(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  if(_f.navQ1_.isSelected() ||_f.navQ2_.isSelected() ||_f.navQ3_.isSelected() || _f.navCr_.isSelected() ||_f.navCrPM_.isSelected() )
  {
  chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  if(_f.navPrio_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">priorit�</th>";
  if(_f.navGare_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">gare d�part</th>";  
  if(_f.navLong_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">longueur (m)</th>";  
  if(_f.navLarg_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">largeur (m)</th>"; 
  if(_f.navTE_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">tirant eau E/S (m)</th>";    
  if(_f.navMode_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"    #EEEEEEE\">mode C/D</th>";
  if(_f.navLoi_.isSelected())chaine+="<th rowspan=\"2\"  bgcolor=\"   #EEEEEEE\">loi g�n�ration</th>";
  }
  else
  {
    chaine+="<th   bgcolor=\"   #EEEEEEE\">Nom</th>";
    if(_f.navPrio_.isSelected())chaine+="<th   bgcolor=\"   #EEEEEEE\">priorit�</th>";
    if(_f.navGare_.isSelected())chaine+="<th   bgcolor=\"   #EEEEEEE\">gare d�part</th>";  
    if(_f.navLong_.isSelected())chaine+="<th   bgcolor=\"   #EEEEEEE\">longueur (m)</th>";  
    if(_f.navLarg_.isSelected())chaine+="<th   bgcolor=\"   #EEEEEEE\">largeur (m)</th>"; 
    if(_f.navTE_.isSelected())chaine+="<th   bgcolor=\"   #EEEEEEE\">tirant eau E/S</th>";    
    if(_f.navMode_.isSelected())chaine+="<th   bgcolor=\"   #EEEEEEE\">mode C/D</th>";
    if(_f.navLoi_.isSelected())chaine+="<th   bgcolor=\"    #EEEEEEE\">loi g�n�ration</th>";
    }
  
  if(_f.navQ1_.isSelected() ||_f.navQ2_.isSelected() ||_f.navQ3_.isSelected() )
    chaine+="<th rowspan=\"1\" colspan=\"3\"  bgcolor=\"    #EEEEEEE\">quais pr�f�rentiels</th>";
  
  if(_f.navCr_.isSelected())
    chaine+="<th rowspan=\"1\" colspan=\"3\"  bgcolor=\"    #EEEEEEE\">Cr�neaux (h.min)</th>";    
    
  if(_f.navCrPM_.isSelected())
    chaine+="<th rowspan=\"1\" colspan=\"3\"  bgcolor=\"    #EEEEEEE\">Cr�neaux PM</th>";   
  
  chaine+="</tr>";
  
  
  
  //sous tableaux de titres
  if(_f.navQ1_.isSelected() ||_f.navQ2_.isSelected() ||_f.navQ3_.isSelected() || _f.navCr_.isSelected() || _f.navCrPM_.isSelected())
    chaine+="<tr>";
  
  if(_f.navQ1_.isSelected() ||_f.navQ2_.isSelected() ||_f.navQ3_.isSelected() )
    {
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">quai 1</th>";
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">quai 2</th>";
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">quai 3</th>";
    }
  
  if(_f.navCr_.isSelected())
  {
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">cr�neau 1</th>";
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">cr�neau 2</th>";
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">cr�neau 3</th>";
    }
  if(_f.navCrPM_.isSelected())
  {
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">cr�neau PM 1</th>";
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">cr�neau PM 2</th>";
      chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">cr�neau PM 3</th>";
    }
  
  
  
  if(_f.navQ1_.isSelected() ||_f.navQ2_.isSelected() ||_f.navQ3_.isSelected() || _f.navCr_.isSelected() || _f.navCrPM_.isSelected())
  chaine+="</tr>";
  
  
  
  
  
  //2 constitution du contenu
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
  {
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>"; 
    
    
    
    
    if(_f.navPrio_.isSelected())
         if(_d.getCategoriesNavires_().retournerNavire(i).getPriorite()==0)chaine+="<td>forte</td>";
         else chaine+="<td>normale</td>";
      
    if(_f.navGare_.isSelected())
      chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getCategoriesNavires_().retournerNavire(i).getGareDepart_())+"</td>";
    
    if(_f.navLong_.isSelected())
      chaine+="<td>"+(float)_d.getCategoriesNavires_().retournerNavire(i).getLongueurMin()+" -"+(float)_d.getCategoriesNavires_().retournerNavire(i).getLongueurMax()+"</td>";
    if(_f.navLarg_.isSelected())
      chaine+="<td>"+(float)_d.getCategoriesNavires_().retournerNavire(i).getLargeurMin()+" -"+(float)_d.getCategoriesNavires_().retournerNavire(i).getLargeurMax()+"</td>";
    
    if(_f.navTE_.isSelected())
      chaine+="<td>E:"+(float)_d.getCategoriesNavires_().retournerNavire(i).getTirantEauEntree()+" - S:"+(float)_d.getCategoriesNavires_().retournerNavire(i).getTirantEauSortie()+"</td>";
    if(_f.navMode_.isSelected())
      if(_d.getCategoriesNavires_().retournerNavire(i).getModeChargement()==0)
      chaine+="<td>shift</td>";
      else
        chaine+="<td>loi service</td>";
    
    if(_f.navLoi_.isSelected())
        if(_d.getCategoriesNavires_().retournerNavire(i).getTypeLoiGenerationNavires_()==0)
          chaine+="<td>Erlang "+_d.getCategoriesNavires_().retournerNavire(i).getLoiErlangGenerationNavire()+"</td>";
        else
          if(_d.getCategoriesNavires_().retournerNavire(i).getTypeLoiGenerationNavires_()==1)
        chaine+="<td>D�terministe</td>";
          else
            chaine+="<td>Journali�re</td>";
  
  
    if(_f.navQ1_.isSelected() ||_f.navQ2_.isSelected() ||_f.navQ3_.isSelected() )
    {
      chaine+="<td>"+_d.getlQuais_().retournerQuais(_d.getCategoriesNavires_().retournerNavire(i).getQuaiPreferentiel1()).getNom()+":"/*+(float)_d.getCategoriesNavires_().retournerNavire(i).getDureeQuaiPref1()*/+"H.M</td>";
      if(_d.getCategoriesNavires_().retournerNavire(i).getQuaiPreferentiel2()!=-1)
      chaine+="<td>"+_d.getlQuais_().retournerQuais(_d.getCategoriesNavires_().retournerNavire(i).getQuaiPreferentiel2()).getNom()+":"/*+(float)_d.getCategoriesNavires_().retournerNavire(i).getDureeQuaiPref2()*/+"H.M</td>";
      else chaine+="<td>pas de quai</td>";
      if(_d.getCategoriesNavires_().retournerNavire(i).getQuaiPreferentiel3()!=-1)
      chaine+="<td>"+_d.getlQuais_().retournerQuais(_d.getCategoriesNavires_().retournerNavire(i).getQuaiPreferentiel3()).getNom()+":"/*+(float)_d.getCategoriesNavires_().retournerNavire(i).getDureeQuaiPref3()*/+"H.M</td>";
      else chaine+="<td>pas de quai</td>";
    }
    
    if(_f.navCr_.isSelected())
    {
      chaine+="<td>"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxJournaliers_().semaineCreneau1HeureDep+"-"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxJournaliers_().semaineCreneau1HeureArrivee+"</td>";
      chaine+="<td>"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxJournaliers_().semaineCreneau2HeureDep+"-"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxJournaliers_().semaineCreneau2HeureArrivee+"</td>";
      chaine+="<td>"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxJournaliers_().semaineCreneau3HeureDep+"-"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxJournaliers_().semaineCreneau3HeureArrivee+"</td>";
    }
    
    
    if(_f.navCrPM_.isSelected())
    {
      chaine+="<td>"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxouverture_().semaineCreneau1HeureDep+"-"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxouverture_().semaineCreneau1HeureArrivee+"</td>";
      chaine+="<td>"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxouverture_().semaineCreneau2HeureDep+"-"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxouverture_().semaineCreneau2HeureArrivee+"</td>";
      chaine+="<td>"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxouverture_().semaineCreneau3HeureDep+"-"+_d.getCategoriesNavires_().retournerNavire(i).getCreneauxouverture_().semaineCreneau3HeureArrivee+"</td>";
    }
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}




public static String creerTableauTopologieBassins(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Gare de rattachement</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListebassin_().getListeBassins_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListebassin_().retournerBassin(i)+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListebassin_().retournerBassin2(i).getGareAmont())+"</td>";
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}

public static String creerTableauTopologieChenaux(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Gare de rattachement avant</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Gare de rattachement apr�s</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeChenal_().getListeChenaux_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeChenal_().retournerChenal(i).getNom_()+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListeChenal_().retournerChenal(i).getGareAmont_())+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListeChenal_().retournerChenal(i).getGareAval_())+"</th>";
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}



public static String creerTableauTopologieCercles(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Gare de rattachement avant</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Gare de rattachement apr�s</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeCercle_().getListeCercles_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeCercle_().retournerCercle(i).getNom_()+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListeCercle_().retournerCercle(i).getGareAmont_())+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListeCercle_().retournerCercle(i).getGareAval_())+"</th>";
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}

public static String creerTableauTopologieEcluses(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p><p ><table border=1 ><tr>";
  
  //1 constitution des titres
  
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Nom</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEEE\">Gare de rattachement avant</th>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Gare de rattachement apr�s</th>";
  
  
  chaine+="</tr>";
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeEcluse_().getListeEcluses_().size();i++)
  {
    //nom
    chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.getListeEcluse_().retournerEcluse(i).getNom_()+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListeEcluse_().retournerEcluse(i).getGareAmont_())+"</th>";
    chaine+="<td>"+_d.getListeGare_().retournerGare(_d.getListeEcluse_().retournerEcluse(i).getGareAval_())+"</th>";
    //fin de la ligne
    chaine+="</tr>";
  }
  
  
  chaine+="</table></p><br><br>";
  return chaine;
}



public static String creerTableauCroisementChenaux(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p>";
  
  
  
  for(int c=0;c<_d.getListeChenal_().getListeChenaux_().size();c++)
  {
    chaine+="<p > Croisement dans le chenal "+_d.getListeChenal_().retournerChenal(c).getNom_()+"<br><br>";
  //1 constitution des titres
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
  
  
  chaine+="</tr>";
  
  
  //2 constitution du contenu
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
  {
    chaine+="<tr>";
//    nom
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
    
    for(int j=0;j<_d.getCategoriesNavires_().getListeNavires_().size();j++)
    {
      if(_d.getListeChenal_().retournerChenal(c).getReglesNavigation_().retournerAij(i, j).booleanValue()==true)
      chaine+="<td>oui </td>";
      else
        chaine+="<td>non </td>";
    
    
    
     }
    chaine+="</tr>";
  
  }
  
  chaine+="</table></p><br><br>";
  
 }
  
  
  return chaine;
}




/**
 * methode d ecriture des croisements dans les cercles
 * @param _d
 * @param _f
 * @return
 */
public static String creerTableauCroisementCercles(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f){
  String chaine="<p>";
    
  for(int c=0;c<_d.getListeCercle_().getListeCercles_().size();c++) {
    chaine+="<p > Croisement dans le cercle "+_d.getListeCercle_().retournerCercle(c).getNom_()+"<br><br>";
  //1 constitution des titres
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
  chaine+="</tr>";
  //2 constitution du contenu
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++){
    chaine+="<tr>";
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
    for(int j=0;j<_d.getCategoriesNavires_().getListeNavires_().size();j++){
      if(_d.getListeCercle_().retournerCercle(c).getReglesNavigation_().retournerAij(i, j).booleanValue()==true)
      chaine+="<td>oui </td>";
      else
        chaine+="<td>non </td>";
     }
    chaine+="</tr>";
  }
  chaine+="</table></p><br><br>";
 }
  return chaine;
}

/**
 * generation htm des genes.
 *  AHX- GESTION genes 2011 - genes 
 * @param _d
 * @param _f
 * @return
 */
public static String creerTableauGenesCercles(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f){
  String chaine="<p>";
    
  for(int c=0;c<_d.getListeCercle_().getListeCercles_().size();c++) {
    chaine+="<p > "+SiporConstantes.ConstanteSectionRapportHtm+" "+_d.getListeCercle_().retournerCercle(c).getNom_()+"<br><br>";
  //1 constitution des titres
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
  chaine+="</tr>";
  //2 constitution du contenu
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++){
    chaine+="<tr>";
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
    for(int j=0;j<_d.getCategoriesNavires_().getListeNavires_().size();j++){
      chaine+="<td>"+_d.getListeCercle_().retournerCercle(c).getRegleGenes().retournerAij(i, j).doubleValue()+"</td>";
     }
    chaine+="</tr>";
  }
  chaine+="</table></p><br><br>";
 }
  return chaine;
}

public static String creerTableauDureeChenaux(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p>";
  
  
  
    
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
  
  
  chaine+="</tr>";
  
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeChenal_().getListeChenaux_().size();i++)
  {
    chaine+="<tr>";
//    nom
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getListeChenal_().retournerChenal(i).getNom_()+"</th>";
    
    for(int j=0;j<_d.getCategoriesNavires_().getListeNavires_().size();j++)
    {
      chaine+="<td>"+(float)_d.getReglesDureesParcoursChenal_().retournerValeur(i,j) +"</td>";
      
     }
    chaine+="</tr>";
  
  }
  
  chaine+="</table></p><br><br>";
  
 
  
  
  return chaine;
}


public static String creerTableauRemplissageSAS(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p>";
  
  
  
    
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
  
  
  chaine+="</tr>";
  
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeEcluse_().getListeEcluses_().size();i++)
  {
    chaine+="<tr>";
//    nom
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getListeEcluse_().retournerEcluse(i).getNom_()+"</th>";
    
    for(int j=0;j<_d.getCategoriesNavires_().getListeNavires_().size();j++)
    {
      chaine+="<td>"+(float)_d.getReglesRemplissageSAS().retournerValeur(i,j) +"</td>";
      
     }
    chaine+="</tr>";
  
  }
  
  chaine+="</table></p><br><br>";
  
 
  
  
  return chaine;
}


public static String creerTableauDureeCercles(SiporDataSimulation _d,SiporFrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p>";
  
  
  
    
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  
  for(int i=0;i<_d.getCategoriesNavires_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getCategoriesNavires_().retournerNavire(i).getNom()+"</th>";
  
  
  chaine+="</tr>";
  
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeCercle_().getListeCercles_().size();i++)
  {
    chaine+="<tr>";
//    nom
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getListeCercle_().retournerCercle(i).getNom_()+"</th>";
    
    for(int j=0;j<_d.getCategoriesNavires_().getListeNavires_().size();j++)
    {
      chaine+="<td>"+(float)_d.getReglesDureesParcoursCercle_().retournerValeur(i,j) +"</td>";
      
     }
    chaine+="</tr>";
  
  }
  
  chaine+="</table></p><br><br>";
  
 
  
  
  return chaine;
}



}



