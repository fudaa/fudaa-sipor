package org.fudaa.fudaa.sipor.siporNetwork;

import java.awt.Color;
import java.util.HashMap;

import javax.swing.UIManager;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.NetworkGraphModel;
import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.ebli.network.simulationNetwork.shape.ShapeChenal;
import org.fudaa.ebli.network.simulationNetwork.ui.EditorMenuBar;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporBassin;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporChenal;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporEcluse;

import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxConstants;

public class SiporNetwork extends SimulationNetworkEditor{




public SiporNetwork(NetworkGraphModel modelSipor) {
		super(modelSipor);
		
		
		
	
	}

/**
 * usefull method if synchro is lost, this action allow to re-syncrho data to model entities.
 */
public void synchronizeFromSiporData(SiporDataSimulation d) {
	//-- first clean all --//
	this.cleanAllNetwork();
	startingX = 600;
	startingY= 230;
	startingNodeX = 100;
	startingNodeY= 230;
	HashMap<Integer, DefaultNetworkUserObject> mapGares =new HashMap<Integer, DefaultNetworkUserObject>();
	//-- add each element --//
	for(int i=0;i< d.getListeGare_().getListeGares_().size() ; i++) {
		String value = d.getListeGare_().retournerGare(i);
		DefaultNetworkUserObject gare =  new DefaultNetworkUserObject(value, true);
		this.addNewNetworkElement(DEFAULT_VALUE_GARE,gare);
		mapGares.put(i,gare);
		
	}
	for(int i=0;i< d.getListebassin_().getListeBassins_().size() ; i++) {
		NetworkUserObject data = d.getListebassin_().retournerBassin2(i);
		this.addNewNetworkElement(DEFAULT_VALUE_BASSIN,data);
		connectElement(mapGares.get(d.getListebassin_().retournerBassin2(i).getGareAmont()), null, data);
	}
		
	for(int i=0;i< d.getListeChenal_().getListeChenaux_().size() ; i++) {
		NetworkUserObject data = d.getListeChenal_().retournerChenal(i);
		 this.addNewNetworkElement(DEFAULT_VALUE_CHENAL,data);
		connectElement(mapGares.get(d.getListeChenal_().retournerChenal(i).getGareAmont_()),
				mapGares.get(d.getListeChenal_().retournerChenal(i).getGareAval_()), data);
	}
	for(int i=0;i< d.getListeEcluse_().getListeEcluses_().size() ; i++) {
		NetworkUserObject data = d.getListeEcluse_().retournerEcluse(i);
		 this.addNewNetworkElement(DEFAULT_VALUE_ECLUSE,data);
		 connectElement(mapGares.get(d.getListeEcluse_().retournerEcluse(i).getGareAmont_()),
					mapGares.get(d.getListeEcluse_().retournerEcluse(i).getGareAval_()), data);
	}
	for(int i=0;i< d.getListeCercle_().getListeCercles_().size() ; i++) {
		NetworkUserObject data = d.getListeCercle_().retournerCercle(i);
		this.addNewNetworkElement(DEFAULT_VALUE_CERCLE,data);
		connectElement(mapGares.get(d.getListeCercle_().retournerCercle(i).getGareAmont_()),
				mapGares.get(d.getListeCercle_().retournerCercle(i).getGareAval_()), data);
	}
}




/**
 * 
 * @param args
 */
public static void main(String[] args)
{
	try
	{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}
	catch (Exception e1)
	{
		e1.printStackTrace();
	}

	mxSwingConstants.SHADOW_COLOR = Color.LIGHT_GRAY;
	mxConstants.W3C_SHADOWCOLOR = "#D3D3D3";
	SiporDataSimulation donnees = new SiporDataSimulation(new SiporImplementation());
	
	donnees.getListebassin_().ajout("Bassin 0");
	
	SiporChenal chenal = new SiporChenal();
	chenal.setNom_("Chenal 0");
	donnees.getListeChenal_().ajout(chenal);
	
	SiporEcluse ecluse = new SiporEcluse();
	ecluse.setNom_("Ecluse 0");
	donnees.getListeEcluse_().ajout(ecluse);
	
	SiporCercle cercle = new SiporCercle();
	cercle.setNom_("Cercle 0");
	donnees.getListeCercle_().ajout(cercle);
	
	
	donnees.getListeGare_().ajout("Gare 0");
	
	
	
	SiporNetwork networkEditor = new SiporNetwork(new NetworkGraphModel( new SiporNetworkDaoFactory(donnees)));
	networkEditor.createFrame(new EditorMenuBar(networkEditor)).setVisible(true);
	
	SiporChenal c = new SiporChenal();
	c.setNom_("TOToR 5");
	
	SiporBassin b = new SiporBassin();
	b.setNom_("BASINOR");
	networkEditor.addNewNetworkElement("Chenal",c);
	networkEditor.addNewNetworkElement("Bassin",b);
	SiporBassin b2 = new SiporBassin();
	b2.setNom_("BASINOR 2");
	networkEditor.addNewNetworkElement("Bassin",b2);
	
	networkEditor.deleteNetworkElement("Bassin", b);
	DefaultNetworkUserObject g = new DefaultNetworkUserObject("GAREOR 2",true);
	DefaultNetworkUserObject g2 = new DefaultNetworkUserObject("GAREOR 3",true);
	networkEditor.addNewNetworkElement("Gare",new DefaultNetworkUserObject("GAREOR",true));
	networkEditor.addNewNetworkElement("Gare",g);
	networkEditor.addNewNetworkElement("Gare",g2);
	networkEditor.addNewNetworkElement("Gare",new DefaultNetworkUserObject("GAREOR 4",true));
	
	
	networkEditor.connectElement(g,g2, c);
	
	
	
}

}
