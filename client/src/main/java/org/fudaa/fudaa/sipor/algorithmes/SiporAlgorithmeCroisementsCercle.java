/**
 *@creation 28 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sipor.SParametresResultatsMatricesCroisements;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * @version $Version$
 * @author hadoux
 */
public class SiporAlgorithmeCroisementsCercle {

  public static void calcul(final SiporDataSimulation donnees_) {

    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // etape 1: recuperation du tableau de matrices de croisements:allocation memoire du tableau en fonction du nombre
    // de cercles
    resultats.tableauDeMatricesCroisementsCercles = new SParametresResultatsMatricesCroisements[donnees_.getListeCercle_().getListeCercles_()
        .size()];
    //

    for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
      // allocation memoire de la i emem structure
      resultats.tableauDeMatricesCroisementsCercles[i] = new SParametresResultatsMatricesCroisements();

      // allocation de la memoire de la matrice dans ce Cercle: matrice carr� de tailel du nombre de navires
      resultats.tableauDeMatricesCroisementsCercles[i].matriceCroisements = new int[donnees_.getCategoriesNavires_().getListeNavires_()
          .size()][donnees_.getCategoriesNavires_().getListeNavires_().size()];

      // pour chaque cercles calculer les croisements dans ce Cercle

      // au d�but valeur bidons:
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        for (int l = 0; l < donnees_.getCategoriesNavires_().getListeNavires_().size(); l++) {
          resultats.tableauDeMatricesCroisementsCercles[i].matriceCroisements[k][l] = 0;
        }
      }

    }

    // calcul des croisements dans les chenaux
    for (int n = 0; n < donnees_.getListeResultatsSimu_().nombreNavires; n++) {
    	
    	//-- on recherche l'indice du quai du navire n --//
    	int quaiN=-1;
    	boolean trouve=false;
    	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours && !trouve;i++)
    		if(donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[i].typeElement==3){
    			quaiN=i;
    			trouve=true;}
    	if(quaiN==-1){System.out.println("Erreur: occupation quai: on a pas trouv�de quai! impossible...");}
    	
      for (int m = n + 1; m < donnees_.getListeResultatsSimu_().nombreNavires; m++) {
    	  
    	  	//-- on recherche l'indice du quai du navire M --//
        	int quaiM=-1;
        	trouve=false;
        	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[m].NbElemtnsParcours && !trouve;i++)
        		if(donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[i].typeElement==3){
        			quaiM=i;
        			trouve=true;}
        	if(quaiM==-1){System.out.println("Erreur: occupation quai: on n'a pas trouv� quai! impossible...");}
    	  
        for (int t1 = 0; t1 < donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours; t1++) {
          for (int t2 = 0; t2 < donnees_.getListeResultatsSimu_().listeEvenements[m].NbElemtnsParcours; t2++) {
            if (donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].typeElement == 1
                && donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].typeElement == 1
                && donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].indiceElement == donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].indiceElement
                && SiporAlgorithmeCroisementsChenal.est_inclus(
                    donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].heureEntree,
                    donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].heureSortie,
                    donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].heureEntree,
                    donnees_.getListeResultatsSimu_().listeEvenements[m].tableauTrajet[t2].heureSortie)
                && ((t1 < quaiN && t2 > quaiM) || (t1 > quaiN && t2 < quaiM))) {
              /*
               * si les elements parocurus sont tous des chenaux, qu ils sont identiques et que les creneaux de
               * franchissement de ces chenaux se chevauchent => donc nos 2 amigo les navires se rencontrent (et c'est
               * le duel de la mort)
               */
              final int cercle = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[t1].indiceElement;
              final int cat1 = donnees_.getListeResultatsSimu_().listeEvenements[n].categorie;
              final int cat2 = donnees_.getListeResultatsSimu_().listeEvenements[m].categorie;

              resultats.tableauDeMatricesCroisementsCercles[cercle].matriceCroisements[cat1][cat2]++;
              // puisque la matrice est symetrique car : si un navire x rencontre un navire y, la r�ciproque est
              // �galement valable
              if (cat1 != cat2) {
                resultats.tableauDeMatricesCroisementsCercles[cercle].matriceCroisements[cat2][cat1]++;
              }
            }

          }
        }
      }
    }

  }

}
