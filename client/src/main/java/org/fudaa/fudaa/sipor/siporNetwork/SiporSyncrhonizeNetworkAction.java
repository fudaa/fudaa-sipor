package org.fudaa.fudaa.sipor.siporNetwork;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
/**
 * Action in case something went wrong between modelisation of port and data typing in the overall software.
 * @author Adrien
 *
 */
public class SiporSyncrhonizeNetworkAction extends AbstractAction {
	SiporNetwork network;
	SiporDataSimulation datas;
	public SiporSyncrhonizeNetworkAction(SiporNetwork network, SiporDataSimulation datas) {
		super("re-synchro donn�es simu");
		this.network = network;
		this.datas = datas;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		network.synchronizeFromSiporData(datas);
	}
	
	

}
