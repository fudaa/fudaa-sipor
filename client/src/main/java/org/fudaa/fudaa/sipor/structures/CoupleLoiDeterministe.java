/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.structures;

/**
 * classe de gestion des donn�e pour les lois deterministe.
 * 
 * @version $Version$
 * @author adrien hadoux
 */
public class CoupleLoiDeterministe {
  int jour_;
  double temps_;

  public CoupleLoiDeterministe(final int _jour, final double _temps) {

    this.jour_ = _jour;
    this.temps_ = _temps;
  }

  public CoupleLoiDeterministe(final CoupleLoiDeterministe _c) {

    this.jour_ = _c.jour_;
    this.temps_ = _c.temps_;
  }

public int getJour_() {
	return jour_;
}

public void setJour_(int jour_) {
	this.jour_ = jour_;
}

public double getTemps_() {
	return temps_;
}

public void setTemps_(double temps_) {
	this.temps_ = temps_;
}

}
