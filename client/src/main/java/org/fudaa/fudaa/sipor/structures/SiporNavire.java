package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Classe de base de dfinition des Navires avec leur nom et differentes caractristiques
 * 
 * @author Adrien Hadoux
 */

public class SiporNavire {

  // definition des attributs de Navire

  /**
   * Nom de la catgorie de Navire.
   */
  String nom = CtuluLibString.EMPTY_STRING;

  
  double dureeAttenteMaximaleAdmissible=0;
  
  /**
   * Priorite de la categorie: 2 valeur 0 ou 1 Par defaut 1.
   */
  int priorite = 1;

  /**
   * Type de loi Loi de génération des navires 0 => loi d erlang 1 => deterministe 2 => journaliere par defaut loi
   * d'erlang.
   */
  int typeLoiGenerationNavires_ ;

  /**
   * loi d erlang typeLoi=0
   */
  int loiErlangGenerationNavire = 1;
  int nbBateauxattendus = 0;
  double ecartMoyenEntre2arrivees = 0;

  /**
   * Declaration du tableau de loi deterministe et journaliere:
   */
  ArrayList loiDeterministe_ = new ArrayList();

  /**
   * Longueur du bateaux : entier
   */
  double longueurMin = -1;
  double longueurMax = -1;

  double largeurMin = -1;
  double largeurMax = -1;
  /**
   * Loi de variation de la longueur:
   */
  int OrdreLoiVariationLongueur_ = 1;
  int OrdreLoiVariationLargeur_ = 1;
  /**
   * gare de depart correspond a l'indice de la gare dans le vecteur de gares de datasimulation apr defaut correspond a
   * la gare de rade: la gare 0
   */
  int gareDepart_ = 0;
  String NomGareDepart_;

  /**
   * Reel qui correspond au Tirant d'eau en entre
   */
  double tirantEauEntree = -1;

  /**
   * Tirant d eau sortie
   */
  double tirantEauSortie = -1;

  /**
   * Nombre de quias preferentiels
   */

  int nbQuaisPreferentiels = 1;

  /**
   * Quais preferentiels entier qui correspond au numero du tableau de Quai
   */

  int quaiPreferentiel1 = 0;
  String nomQuaiPreferentiel1;
  /*F9
  double dureeQuaiPref1 = 0;
  */
  int quaiPreferentiel2 = 0;
  String nomQuaiPreferentiel2;
  /*F9
  double dureeQuaiPref2 = 0;
	*/
  int quaiPreferentiel3 = 0;
  String nomQuaiPreferentiel3;
  /*F9
  double dureeQuaiPref3 = 0;
  s*/
  SiporHoraire creneauxJournaliers_ = new SiporHoraire();
  SiporHoraire creneauxouverture_ = new SiporHoraire();

  /**
   * Mode de chargement dechargement Shift: shift =>0 loi aleatoire: loi de service =>1 par defaut loi shift
   */
  int ModeChargement = 0;
  String typeModeChargement = "shift";
  // *************************************************
  // mode de chargement shift
  /**
   * Cas mode de chargement shift
   */
  /**
   * correspond au numero de l horaire precedemment saisi
   */
  SiporHoraire horaireTravailSuivi = new SiporHoraire();
  double dureeAttenteShiftSuivant = -1;

  /**
   * Tonnage: reel 3 typede tonnage: minimum, moyen et maximum a ceci est associ une loi de variation du tonnage
   */

  double tonnageMin = -1;
  double tonnageMax = -1;
  int OrdreLoiVariationTonnage = -1;

  /**
   * cadence des quais
   */
  double cadenceQuaiPref = -1;
  double cadenceQuai = -1;

  // *************************************************
  // mode de chargement Loi

  /**
   * cas mode de chargement Loi
   */
  double dureeServiceMinQuaiPref = -1;
  double dureeServiceMinAutresQuais = -1;
  double dureeServiceMaxQuaiPref = -1;
  double dureeServiceMaxAutresQuais = -1;

  int ordreLoiQuaiPref = -1;
  int ordreLoiAutresQuais = -1;

  /**
   * constructeur par defaut
   */
  public SiporNavire() {

  }

  /**
   * Constructeur avec parametre d'entree: le nom de la catégoriede Navire
   */
  public  SiporNavire(final String nom) {
    this.nom = nom;
  }

  /**
   * Methode de verification des donnes saisies Verifie que toutes les donnes ont t saisies:
   * 
   * @return oui si le navire est saisi completement
   */

  public boolean navireCompletementSaisi() {

    // test de la non vacuit du nom du navire
    if (nom == "") {
      return false;
    }

    return true;
  }

  /**
   * Methode qui verifie si des incohrences ont lieu dans toutes les donnes relative aux catgories de navire
   */

  public boolean verifCoherenceDonnees() {
    if (this.longueurMin <= 0) {
      return false;
    }
    if (OrdreLoiVariationLongueur_ == -1) {
      return false;
    }
    if (this.tirantEauEntree == -1) {
      return false;
    }
    if (this.tirantEauSortie == -1) {
      return false;
    }

    // plusieurs quais preferentiels identiques
    if (this.quaiPreferentiel1 != 0 && this.quaiPreferentiel1 == this.quaiPreferentiel2) {
      return false;
    }
    if (this.quaiPreferentiel1 != 0 && this.quaiPreferentiel1 == this.quaiPreferentiel3) {
      return false;
    }
    if (this.quaiPreferentiel2 != 0 && this.quaiPreferentiel2 == this.quaiPreferentiel3) {
      return false;
    }
    
    return true;
  }

  /**
   * Methode d affichage d un Navire
   */

  public  void affichage() {
    System.out.print("nom du navire: " + nom + "\n priorite: " + priorite + "\n longeurs du bateau min: " + longueurMin
        + "\n max: " + longueurMax + "\n Loi de variation de la longueur: " + OrdreLoiVariationLongueur_
        + "\n Tirant d'eau en entree: " + this.tirantEauEntree + "\n Tirant d eau en sortie: " + this.tirantEauSortie
        + "\n Nb de quais preferentiels: " + this.nbQuaisPreferentiels + "\n Quai preferentiel 1: "
        + this.quaiPreferentiel1 + "\n                   2: " + this.quaiPreferentiel2 + "\n Quai preferentiel 3"
        + this.quaiPreferentiel3 + "\n "
        + "\n Mode de chargement: " + ModeChargement);

    if (ModeChargement == 0) {
      System.out.println("Mode chargement shift:" + "\n horaire de travail suivi: " + this.horaireTravailSuivi
          + "\n duree d'attente avant prochain shift: " + this.dureeAttenteShiftSuivant + "\n Tonnage Min: "
          + this.tonnageMin + "\n Tonnage Max: " + this.tonnageMax + "\nOrdre de la loi de variation du tonnage: "
          + this.OrdreLoiVariationTonnage + "\n cadence Quai preferentiels: " + this.cadenceQuaiPref
          + "\n cadence autres quais: " + this.cadenceQuai + "\n" + "");

    } else if (this.ModeChargement == 1) {
      System.out.println("Mode chargement selon une loi:" + "\n duree service minimum au quai preferentiel: "
          + this.dureeServiceMinQuaiPref + "\n               maximum                       "
          + this.dureeServiceMaxQuaiPref + "\n duree service minimum autres quais:         "
          + this.dureeServiceMinAutresQuais + "\n               maximum                       "
          + this.dureeServiceMaxAutresQuais + "\n" + "");
    } else {
      System.out.println("Erreur!! aucun chargement , dechargement ne correspond  cette lettre!!!!!");
    }

  }

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public int getPriorite() {
	return priorite;
}

public void setPriorite(int priorite) {
	this.priorite = priorite;
}

public int getTypeLoiGenerationNavires_() {
	return typeLoiGenerationNavires_;
}

public void setTypeLoiGenerationNavires_(int typeLoiGenerationNavires_) {
	this.typeLoiGenerationNavires_ = typeLoiGenerationNavires_;
}

public int getLoiErlangGenerationNavire() {
	return loiErlangGenerationNavire;
}

public void setLoiErlangGenerationNavire(int loiErlangGenerationNavire) {
	this.loiErlangGenerationNavire = loiErlangGenerationNavire;
}

public int getNbBateauxattendus() {
	return nbBateauxattendus;
}

public void setNbBateauxattendus(int nbBateauxattendus) {
	this.nbBateauxattendus = nbBateauxattendus;
}

public double getEcartMoyenEntre2arrivees() {
	return ecartMoyenEntre2arrivees;
}

public void setEcartMoyenEntre2arrivees(double ecartMoyenEntre2arrivees) {
	this.ecartMoyenEntre2arrivees = ecartMoyenEntre2arrivees;
}

public ArrayList getLoiDeterministe_() {
	return loiDeterministe_;
}

public void setLoiDeterministe_(ArrayList loiDeterministe_) {
	this.loiDeterministe_ = loiDeterministe_;
}

public double getLongueurMin() {
	return longueurMin;
}

public void setLongueurMin(double longueurMin) {
	this.longueurMin = longueurMin;
}

public double getLongueurMax() {
	return longueurMax;
}

public void setLongueurMax(double longueurMax) {
	this.longueurMax = longueurMax;
}

public double getLargeurMin() {
	return largeurMin;
}

public void setLargeurMin(double largeurMin) {
	this.largeurMin = largeurMin;
}

public double getLargeurMax() {
	return largeurMax;
}

public void setLargeurMax(double largeurMax) {
	this.largeurMax = largeurMax;
}

public int getOrdreLoiVariationLongueur_() {
	return OrdreLoiVariationLongueur_;
}

public void setOrdreLoiVariationLongueur_(int ordreLoiVariationLongueur_) {
	OrdreLoiVariationLongueur_ = ordreLoiVariationLongueur_;
}

public int getOrdreLoiVariationLargeur_() {
	return OrdreLoiVariationLargeur_;
}

public void setOrdreLoiVariationLargeur_(int ordreLoiVariationLargeur_) {
	OrdreLoiVariationLargeur_ = ordreLoiVariationLargeur_;
}

public int getGareDepart_() {
	return gareDepart_;
}

public void setGareDepart_(int gareDepart_) {
	this.gareDepart_ = gareDepart_;
}

public String getNomGareDepart_() {
	return NomGareDepart_;
}

public void setNomGareDepart_(String nomGareDepart_) {
	NomGareDepart_ = nomGareDepart_;
}

public double getTirantEauEntree() {
	return tirantEauEntree;
}

public void setTirantEauEntree(double tirantEauEntree) {
	this.tirantEauEntree = tirantEauEntree;
}

public double getTirantEauSortie() {
	return tirantEauSortie;
}

public void setTirantEauSortie(double tirantEauSortie) {
	this.tirantEauSortie = tirantEauSortie;
}

public int getNbQuaisPreferentiels() {
	return nbQuaisPreferentiels;
}

public void setNbQuaisPreferentiels(int nbQuaisPreferentiels) {
	this.nbQuaisPreferentiels = nbQuaisPreferentiels;
}

public int getQuaiPreferentiel1() {
	return quaiPreferentiel1;
}

public void setQuaiPreferentiel1(int quaiPreferentiel1) {
	this.quaiPreferentiel1 = quaiPreferentiel1;
}

public String getNomQuaiPreferentiel1() {
	return nomQuaiPreferentiel1;
}

public void setNomQuaiPreferentiel1(String nomQuaiPreferentiel1) {
	this.nomQuaiPreferentiel1 = nomQuaiPreferentiel1;
}



public int getQuaiPreferentiel2() {
	return quaiPreferentiel2;
}

public void setQuaiPreferentiel2(int quaiPreferentiel2) {
	this.quaiPreferentiel2 = quaiPreferentiel2;
}

public String getNomQuaiPreferentiel2() {
	return nomQuaiPreferentiel2;
}

public void setNomQuaiPreferentiel2(String nomQuaiPreferentiel2) {
	this.nomQuaiPreferentiel2 = nomQuaiPreferentiel2;
}



public int getQuaiPreferentiel3() {
	return quaiPreferentiel3;
}

public void setQuaiPreferentiel3(int quaiPreferentiel3) {
	this.quaiPreferentiel3 = quaiPreferentiel3;
}

public String getNomQuaiPreferentiel3() {
	return nomQuaiPreferentiel3;
}

public void setNomQuaiPreferentiel3(String nomQuaiPreferentiel3) {
	this.nomQuaiPreferentiel3 = nomQuaiPreferentiel3;
}



public SiporHoraire getCreneauxJournaliers_() {
	return creneauxJournaliers_;
}

public void setCreneauxJournaliers_(SiporHoraire creneauxJournaliers_) {
	this.creneauxJournaliers_ = creneauxJournaliers_;
}

public SiporHoraire getCreneauxouverture_() {
	return creneauxouverture_;
}

public void setCreneauxouverture_(SiporHoraire creneauxouverture_) {
	this.creneauxouverture_ = creneauxouverture_;
}

public int getModeChargement() {
	return ModeChargement;
}

public void setModeChargement(int modeChargement) {
	ModeChargement = modeChargement;
}

public String getTypeModeChargement() {
	return typeModeChargement;
}

public void setTypeModeChargement(String typeModeChargement) {
	this.typeModeChargement = typeModeChargement;
}

public SiporHoraire getHoraireTravailSuivi() {
	return horaireTravailSuivi;
}

public void setHoraireTravailSuivi(SiporHoraire horaireTravailSuivi) {
	this.horaireTravailSuivi = horaireTravailSuivi;
}

public double getDureeAttenteShiftSuivant() {
	return dureeAttenteShiftSuivant;
}

public void setDureeAttenteShiftSuivant(double dureeAttenteShiftSuivant) {
	this.dureeAttenteShiftSuivant = dureeAttenteShiftSuivant;
}

public double getTonnageMin() {
	return tonnageMin;
}

public void setTonnageMin(double tonnageMin) {
	this.tonnageMin = tonnageMin;
}

public double getTonnageMax() {
	return tonnageMax;
}

public void setTonnageMax(double tonnageMax) {
	this.tonnageMax = tonnageMax;
}

public int getOrdreLoiVariationTonnage() {
	return OrdreLoiVariationTonnage;
}

public void setOrdreLoiVariationTonnage(int ordreLoiVariationTonnage) {
	OrdreLoiVariationTonnage = ordreLoiVariationTonnage;
}

public double getCadenceQuaiPref() {
	return cadenceQuaiPref;
}

public void setCadenceQuaiPref(double cadenceQuaiPref) {
	this.cadenceQuaiPref = cadenceQuaiPref;
}

public double getCadenceQuai() {
	return cadenceQuai;
}

public void setCadenceQuai(double cadenceQuai) {
	this.cadenceQuai = cadenceQuai;
}

public double getDureeServiceMinQuaiPref() {
	return dureeServiceMinQuaiPref;
}

public void setDureeServiceMinQuaiPref(double dureeServiceMinQuaiPref) {
	this.dureeServiceMinQuaiPref = dureeServiceMinQuaiPref;
}

public double getDureeServiceMinAutresQuais() {
	return dureeServiceMinAutresQuais;
}

public void setDureeServiceMinAutresQuais(double dureeServiceMinAutresQuais) {
	this.dureeServiceMinAutresQuais = dureeServiceMinAutresQuais;
}

public double getDureeServiceMaxQuaiPref() {
	return dureeServiceMaxQuaiPref;
}

public void setDureeServiceMaxQuaiPref(double dureeServiceMaxQuaiPref) {
	this.dureeServiceMaxQuaiPref = dureeServiceMaxQuaiPref;
}

public double getDureeServiceMaxAutresQuais() {
	return dureeServiceMaxAutresQuais;
}

public void setDureeServiceMaxAutresQuais(double dureeServiceMaxAutresQuais) {
	this.dureeServiceMaxAutresQuais = dureeServiceMaxAutresQuais;
}

public int getOrdreLoiQuaiPref() {
	return ordreLoiQuaiPref;
}

public void setOrdreLoiQuaiPref(int ordreLoiQuaiPref) {
	this.ordreLoiQuaiPref = ordreLoiQuaiPref;
}

public int getOrdreLoiAutresQuais() {
	return ordreLoiAutresQuais;
}

public void setOrdreLoiAutresQuais(int ordreLoiAutresQuais) {
	this.ordreLoiAutresQuais = ordreLoiAutresQuais;
}

public double getDureeAttenteMaximaleAdmissible() {
	return dureeAttenteMaximaleAdmissible;
}

public void setDureeAttenteMaximaleAdmissible(
		double dureeAttenteMaximaleAdmissible) {
	this.dureeAttenteMaximaleAdmissible = dureeAttenteMaximaleAdmissible;
}

}
