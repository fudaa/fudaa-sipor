package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.fu.FuLog;

import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.CreneauxLoiJournaliere;
import org.fudaa.fudaa.sipor.structures.SiporBassins;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;
import org.fudaa.fudaa.sipor.structures.SiporQuais;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieHorairesResume;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieLoiDeterministe;
import org.fudaa.fudaa.sipor.ui.frame.SiporFrameSaisieLoiJournaliere;
import org.fudaa.fudaa.sipor.ui.frame.SiporVisualiserQuais;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextField;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldDuree;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldFloat;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldInteger;

/**
 * Panel de saisie des donnn�es relatives aux quais Ce paenl sert aussi de de panel de modification d'un quai ce mode se
 * distingue � la fin lors de la sauvegarde des donn�es via un booleen: MODE_MODIFICATION_ON=false: on ajoute la donn�e
 * saisie MODE_MODIFICATION_ON=false : on modifie le quai numero QUAI_A_MODIFIER
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelSaisieQuais extends JPanel {

  // attributs
  public static int nbouvertures = 0;

  SiporFrameSaisieLoiJournaliere fenetreLoiJournaliere_ = null;
  SiporFrameSaisieLoiDeterministe fenetreLoideter_ = null;

  // panels d affichage
  JPanel global_ = new JPanel();
  JPanel p0_ = new JPanel();
  
  JPanel p2_ = new JPanel();
  JPanel p3_ = new JPanel();

  
  SiporTextField nom_ = new SiporTextField(10);
  SiporTextFieldFloat longueur_ = new SiporTextFieldFloat(3);
  SiporTextFieldDuree dureeIndispo_ = new SiporTextFieldDuree(3);
  SiporTextFieldInteger frequenceMoyenne_ = new SiporTextFieldInteger(3);
  SiporTextFieldInteger frequenceMoyenne2_ = new SiporTextFieldInteger(3);
  
  String[] choix_ = { "non", "oui" };

  JComboBox dehalageAutorise_ = new JComboBox(choix_);
  JComboBox choixBassin_ = new JComboBox();

  String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  JComboBox loiProbaDuree_ = new JComboBox(tabloi_);
  JComboBox loiProbaFrequence_ = new JComboBox(tabloi_);

  String[] choixLoi_ = { "Erlang", "Deterministe" };
  JComboBox choixLoiFrequence = new JComboBox(choixLoi_);

  final BuButton creneau_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "Cr�neau");

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * Horaire complet a valider absolument!!
   */

  SiporHoraire horaire_ = new SiporHoraire();

  /**
   * liste des bassins:
   */
  public SiporBassins bassins_ = new SiporBassins();

  /**
   * Donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * FENETRE PRINCIPALE DE VISUALISAION DES QUAIS CETTE FENETRE RELIE TOUTES LES OPTIONS LIEES A LA MANIPULATION DES
   * QUAIS IL FAUT DONC PASSER PAR CETTE FENETRE PORU EFFECTUER TOUTES LES MODIFICATIONS IMAGINABLES
   */

  public  SiporVisualiserQuais MENUQUAIS_;

  /**
   * BOOLEEN qui precise si le panel de saisie d'un quai est en mode ajout ou en mode modification
   */
  public boolean UPDATE = false;

  /**
   * indice dans la liste des quais du quai a modifier
   */
  public  int QUAI_A_MODIFIER_;

  /**
   * variable contenant les creneaux de al loi journaliere
   */
  CreneauxLoiJournaliere creneauLoiJournaliere_ = new CreneauxLoiJournaliere();

  /**
   * variable contenant le tableau des couples pour la loi deterministe
   */
  ArrayList loiDeterministe_ = new ArrayList();

  /**
   * constructeur de la frame
   */
  public SiporPanelSaisieQuais(final SiporDataSimulation db, final SiporVisualiserQuais m) {
    MENUQUAIS_ = m;
    donnees_ = db;
    SiporPanelSaisieQuais.nbouvertures++;

    bassins_ = db.getListebassin_();

    this.majSaisie();

    setSize(535, 350);

    this.nom_.setText("quai " + (donnees_.getlQuais_().getlQuais_().size() + 1));
    
    /**
     * controles a realiser pour la feneetre de saisie des donn�es du quai
     */

    loiProbaDuree_
        .setToolTipText("Loi de probabilit� d'Erlang, choisissez un chiffre entre 1 et 10 avec 10 �quivalent � la loi r�guli�re");
    loiProbaFrequence_
        .setToolTipText("Loi de probabilit� d'Erlang, choisissez un chiffre entre 1 et 10 avec 10 �quivalent � la loi r�guli�re");

    
    

    this.creneau_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        donnees_.getApplication().addInternalFrame(new SiporFrameSaisieHorairesResume(horaire_));
      }
    });

    this.validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        creation_Quai();
      }
    });

    this.choixLoiFrequence.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        /**
         * ETAPE 1: determiner le type de loi selectionn�
         */
        final int choixLoi = choixLoiFrequence.getSelectedIndex();

        if (choixLoi == 0) {
          // Cas 0: loi d erlang
          frequenceMoyenne_.setEnabled(true);
          frequenceMoyenne2_.setEnabled(true);
          loiProbaFrequence_.setEnabled(true);

        } else if (choixLoi == 1) {
          // cas 1: loi deterministe
          frequenceMoyenne_.setEnabled(false);
          frequenceMoyenne2_.setEnabled(false);
          loiProbaFrequence_.setEnabled(false);
          // donnees_.getApplication_().addInternalFrame(new
          // SiporFrameSaisieLoiDeterministe(donnees_,loiDeterministe_,dureeIndispo_));

          if (fenetreLoideter_ == null) {
            FuLog.debug("interface nulle");

            fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

            // System.out.println("55555");
            fenetreLoideter_.setVisible(true);

            donnees_.getApplication().addInternalFrame(fenetreLoideter_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoideter_.isClosed()) {

              fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().addInternalFrame(fenetreLoideter_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");

              fenetreLoideter_ = new SiporFrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().activateInternalFrame(fenetreLoideter_);
              donnees_.getApplication().addInternalFrame(fenetreLoideter_);

            }
          }

        } else if (choixLoi == 2) {
          // cas 2: loi journaliere
          frequenceMoyenne_.setEnabled(false);
          frequenceMoyenne2_.setEnabled(false);
          loiProbaFrequence_.setEnabled(false);

          // lancement de la frame de saisie des creneaux de la loi journaliere
          // donnees_.getApplication_().addInternalFrame(new

          if (fenetreLoiJournaliere_ == null) {
            FuLog.debug("interface nulle");

            fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

            // System.out.println("55555");
            fenetreLoiJournaliere_.setVisible(true);
            donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoiJournaliere_.isClosed()) {

              fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");

              fenetreLoiJournaliere_ = new SiporFrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.getApplication().activateInternalFrame(fenetreLoiJournaliere_);
              donnees_.getApplication().addInternalFrame(fenetreLoiJournaliere_);

            }
          }

        }

      }

    });

    /**
     * Mise en page de la frame
     */

    // mise en page de la frame
    this.global_.setLayout(new BorderLayout());
    
    //general
    p0_.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "G�n�ral"));
    this.p0_.add(new JLabel("Nom du quai: "));
    p0_.add(this.nom_);
    p0_.add(new JLabel("   Bassin d'appartenance: "));
    p0_.add(this.choixBassin_);
    global_.add(p0_, BorderLayout.NORTH);
   
    
    //centre
    Box global1 = Box.createVerticalBox();
    global_.add(global1, BorderLayout.CENTER);
    //global1.setBorder(SiporBordures.compound_);
   
    //dimensions
    JPanel dimensions=new JPanel(new GridLayout(1,2));
    dimensions.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Dimensions"));
    global1.add(dimensions);
    
    final JPanel p11 = new JPanel();
    p11.add(new JLabel("Longueur du quai: "));
    p11.add(this.longueur_);
    p11.add(new JLabel("M"));
    p11.setBorder(SiporBordures.bordnormal_);
    dimensions.add(p11);

    final JPanel p12 = new JPanel();
    p12.add(new JLabel("D�halage autoris�: "));
    p12.add(this.dehalageAutorise_);

    p12.setBorder(SiporBordures.bordnormal_);
    dimensions.add(p12);

    

    
    //durees indisponibilites + loi indispo + creneau
    JPanel indispoCreneau=new JPanel(new GridLayout(1,2));
    global1.add(indispoCreneau);
    
    
    
   

   //loi indisponibilit�s
    Box loiindispo=Box.createVerticalBox();
    loiindispo.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Loi indisponibilit�"));
    indispoCreneau.add(loiindispo);
    
    final JPanel p31 = new JPanel();
    p31.add(new JLabel("Type de loi: "));
    p31.add(choixLoiFrequence);
    p31.setBorder(SiporBordures.bordnormal_);
    loiindispo.add(p31);

    final JPanel p32 = new JPanel();
    p32.add(new JLabel("Ecart moyen: "));
    p32.add(this.frequenceMoyenne_);
    p32.add(new JLabel("Jours"));
    p32.add(this.frequenceMoyenne2_);
    p32.add(new JLabel("Heures"));
    p32.setBorder(SiporBordures.bordnormal_);
    loiindispo.add(p32);

    final JPanel p33 = new JPanel();
    p33.add(new JLabel("Ordre loi d'Erlang fr�quence: "));
    p33.add(this.loiProbaFrequence_);
    p33.setBorder(SiporBordures.bordnormal_);
    loiindispo.add(p33);

    //duree indispo + creneaux
    Box dureeCreneaux=Box.createVerticalBox();
    indispoCreneau.add(dureeCreneaux);

    
    //duree indispo
    Box duree=Box.createVerticalBox();
    duree.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Dur�e indisponibilit�"));
    dureeCreneaux.add(duree);
    
    
    final JPanel p21 = new JPanel();
    p21.add(new JLabel("Dur�e moyenne: "));
    p21.add(this.dureeIndispo_);
    p21.add(new JLabel("Heures.Minutes"));
    p21.setBorder(SiporBordures.bordnormal_);
    duree.add(p21);

    final JPanel p23 = new JPanel();
    p23.add(new JLabel("Ordre loi d'Erlang de la dur�e: "));
    p23.add(this.loiProbaDuree_);
    p23.setBorder(SiporBordures.bordnormal_);
    duree.add(p23);

    
    //creneaux
    final JPanel p13 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    p13.add(this.creneau_);
    p13.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Cr�neaux horaires d'acc�s"));
    dureeCreneaux.add(p13);
    
    
    // ajout du panel sur la frame
    this.setBorder(SiporBordures.quai);

    this.add("Saisie d'un quai", global_);

    final JPanel pv = new JPanel();
    validation_.setSize(300, 30);
    pv.add(validation_);
    pv.setBorder(SiporBordures.compound_);
    global_.add(pv, BorderLayout.SOUTH);

    // affichage:
    setVisible(true);
  }

  /**
   * Methode de mise a jour des donn�es en l occurence les bassins
   */

  void majSaisie() {
    this.choixBassin_.removeAllItems();
    for (int i = 0; i < bassins_.getListeBassins_().size(); i++) {
      this.choixBassin_.addItem(bassins_.retournerBassin(i));
    }

  }

  /**
   * Methode de verification des parametres saisi
   * 
   * @return true si les donn�es sont coh�rentes et robustes
   */
  boolean controle_creation_Quai() {

    if (this.getNom_().getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Nom manquant")
          .activate();
      return false;
    } else if (this.UPDATE == true
        && this.donnees_.getlQuais_().existeDoublon(this.getNom_().getText(), this.QUAI_A_MODIFIER_)) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Nom d�j� pris.")
          .activate();
      return false;
    } else if (!this.UPDATE && this.donnees_.getlQuais_().existeDoublon(this.getNom_().getText(), -1)) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Nom d�j� pris.")
          .activate();
      return false;
    }

    if (this.longueur_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Longueur manquante.")
          .activate();
      return false;

    }
    if (this.dureeIndispo_.getText().equals("")) {
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Dur�e d'indisponibilit� manquante.").activate();
      return false;

    }

    /**
     * Cas loi de frequence= loi d erlang: il faut mettre une valeur moyenne
     */
    if (choixLoiFrequence.getSelectedIndex() == 0) {
      if (this.frequenceMoyenne_.getText().equals("") && this.frequenceMoyenne2_.getText().equals("")) {
        new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
            "Fr�quence moyenne manquante.").activate();
        return false;

      }
    }

   
    /**
     * cas loi detereministe: aucun controles: il peut y avoir 0 couples...
     */

    // controle de la saisie des horaires:
    if (this.horaire_.semaineCreneau1HeureArrivee == -1 || this.horaire_.semaineCreneau1HeureDep == -1
        || this.horaire_.semaineCreneau2HeureArrivee == -1 || this.horaire_.semaineCreneau2HeureDep == -1) {
      donnees_.getApplication();
      new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Cr�neaux non saisis correctement.").activate();
      return false;

    }

    // ariv� a ce stade, tous les test ont �t� n�gatifs, ce qui prouve que les donn�es sont bien robustes et coh�rentes
    return true;
  }

  /**
   * Methode de creation de la donn�e � partir des
   */

  void creation_Quai() {

    if (this.controle_creation_Quai()) {



      SiporQuais quai = null;
      
      if(!UPDATE)
    	  quai = new SiporQuais();
      else
    	  quai = this.donnees_.getlQuais_().retournerQuais(QUAI_A_MODIFIER_);
      
      quai.setNom_(this.getNom_().getText());
      quai.setLongueur_(Float.parseFloat(this.longueur_.getText()));
      if (((String) this.dehalageAutorise_.getSelectedItem()).equals("oui")) {
        quai.setDehalageAutorise_(true);
      } else {
        quai.setDehalageAutorise_(false);
      }
      quai.setDureeIndispo_(Float.parseFloat(this.dureeIndispo_.getText()));
      if (choixLoiFrequence.getSelectedIndex() == 0) {
        quai.setTypeLoi_(0);
        float moy=0;
        if(!this.frequenceMoyenne_.getText().equals(""))
          moy+= Float.parseFloat(this.frequenceMoyenne_.getText())*24;
        if(!this.frequenceMoyenne2_.getText().equals(""))
          moy+= Float.parseFloat(this.frequenceMoyenne2_.getText());
        quai.setFrequenceMoyenne_(moy) ;
        quai.setLoiFrequence_(Integer.parseInt((String) this.loiProbaFrequence_.getSelectedItem()));
      } else if (choixLoiFrequence.getSelectedIndex() == 1) {

        quai.setTypeLoi_(1);
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          quai.getLoiDeterministe_().add(c);

        }

      } else if (choixLoiFrequence.getSelectedIndex() == 2) {
        // cas loi journaliere
        quai.setTypeLoi_(2);
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          quai.getLoiDeterministe_().add(c);

        }

      }
      quai.setLoiIndispo_(Integer.parseInt((String) this.loiProbaDuree_.getSelectedItem()));
      quai.getH_().recopie(horaire_);

      // recuperation du bassin d appartenance
      quai.setNomBassin_((String) this.choixBassin_.getSelectedItem());
      // recuperation du numero de bassin
      quai.setBassin_(donnees_.getListebassin_().retrouveBassin(quai.getNomBassin_()));
      // FUDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDAAAAAAAAAAAAAAAA VAAAAAAARRRRRRRRRRRIAAAAAAABLES

      /**
       * ************************************** Ajout ou modification selon le mode
       */

      if (UPDATE == false) {
        donnees_.getlQuais_().ajout(quai);
        donnees_.getApplication();
        new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Le quai "
            + this.getNom_().getText() + " a �t� ajout� avec succ�s.").activate();
      } else {
        donnees_.getlQuais_().modification(QUAI_A_MODIFIER_, quai);
        donnees_.getApplication();
        new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Le quai "
            + this.getNom_().getText() + " a �t� modifi� avec succ�s.").activate();
      }
      /**
       * Fin ajout ****************************
       */
      
    //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      donnees_.baisserNiveauSecurite();

      
      // 2)sauvegarde des donnees
      donnees_.enregistrer();

      // remise a zero des donn�es du panel
      this.initialiser();

      // affichage du tableau modifi� magique!!
      this.MENUQUAIS_.pile_.first(this.MENUQUAIS_.principalPanel_);

    }

  }

  /**
   * ******************************* METHODE QUI INDIQUE QUE LA FENETRE DE SAISIE D UN QUAI DEVIENT UNE FENETRE DE
   * MODIFICATION D UN QUAI
   * 
   * @param numQuai indice du quai dans la liste des quais a modifier
   */

  public void MODE_MODIFICATION(final int numQuai) {

    majSaisie();
    // 1) passage en mode modification
    this.UPDATE = true;

    // 2) recuperation de l indice du quai
    this.QUAI_A_MODIFIER_ = numQuai;

    // 3) recuperation de la structure de quai

    final SiporQuais q = this.donnees_.getlQuais_().retournerQuais(this.QUAI_A_MODIFIER_);

    // 4)remplissage des donnn�es relatives au quai:
    this.getNom_().setText(q.getNom());
    this.longueur_.setText("" + (float) q.getLongueur_());
    this.dureeIndispo_.setText("" + (float) q.getDureeIndispo_());
    this.loiProbaDuree_.setSelectedIndex(q.getLoiIndispo_() - 1);
    horaire_.recopie(q.getH_());
    // recuperation du bassin d appartenance
    this.choixBassin_.setSelectedIndex(q.getBassin_());
    if (q.isDehalageAutorise_()) {
      this.dehalageAutorise_.setSelectedIndex(1);
    } else {
      this.dehalageAutorise_.setSelectedIndex(0);
    }

    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.loiProbaFrequence_.setSelectedIndex(0);

    if (q.getTypeLoi_() == 0) {

      this.frequenceMoyenne_.setText("" + ((int) q.getFrequenceMoyenne_())/24);
      this.frequenceMoyenne2_.setText("" + ((int) q.getFrequenceMoyenne_())%24);
      this.loiProbaFrequence_.setSelectedIndex(q.getLoiFrequence_() - 1);
      this.choixLoiFrequence.setSelectedIndex(0);

    } else if (q.getTypeLoi_() == 1) {

      for (int i = 0; i < q.getLoiDeterministe_().size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.getLoiDeterministe_().get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(1);
    } else if (q.getTypeLoi_() == 2) {
      // cas loi journaliere

      for (int i = 0; i < q.getLoiDeterministe_().size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.getLoiDeterministe_().get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(2);

    }

    if (this.fenetreLoideter_ != null) {
      fenetreLoideter_.setVisible(false);
    }

    if (this.fenetreLoiJournaliere_ != null) {
      fenetreLoiJournaliere_.setVisible(false);
    }

    // changement du nom du bouton de validation desdonn�es: modification
    this.validation_.setText("Modifier");

    this.getNom_().requestFocus();
    this.getNom_().selectAll();

  }

  /**
   * methode d'initialisation des champs de la frame
   */
   public  void initialiser() {
    fenetreLoideter_ = null;
    majSaisie();
    // REMISE A ZERO DES COMPOSANTS...
    this.horaire_ = new SiporHoraire();
    // remise a zero du contenu des sous fenetres
    this.loiDeterministe_ = new ArrayList();
    this.creneauLoiJournaliere_ = new CreneauxLoiJournaliere();

    this.getNom_().setText("Quai " + (donnees_.getlQuais_().getlQuais_().size() + 1));
    this.longueur_.setText("");
    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.dureeIndispo_.setText("");
    this.validation_.setText("Valider");
    this.loiProbaDuree_.setSelectedIndex(0);
    this.loiProbaFrequence_.setSelectedIndex(0);
    this.choixLoiFrequence.setSelectedIndex(0);
    this.choixBassin_.setSelectedIndex(0);

    // rafraichissement du tableau
    this.MENUQUAIS_.affichagePanel_.maj(donnees_);

    // reinitialisation du mode modification
    this.UPDATE = false;
  }

public SiporTextField getNom_() {
  return nom_;
}

public void setNom_(SiporTextField nom_) {
  this.nom_ = nom_;
}

}
