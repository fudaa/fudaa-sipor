package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class SiporCellEditor extends AbstractCellEditor
implements TableCellEditor{
	
	
	Object contenu_;
	SiporTextField saisie=new SiporTextField(10);
	
	public SiporCellEditor() {
		
	}

	/**Methode qui retourne le composant graphique utilis� pour r�aliser les saisies. **/
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		//saisie.setText(""+(String)value);
		//contenu_=value;

		//par d�faut on met dans le champs text le contenu pr�c�dent
		if((value instanceof Integer) || (value instanceof Float) || (value instanceof Double))
		saisie.setText(""+value);
		else 
			saisie.setText((String)value);

		return saisie;
	}

	public Object getCellEditorValue() {
		
		//-- transformation de la virgule en point
		String contenu=saisie.getText();
		if(!contenu.equals("")){
			int indice=contenu.lastIndexOf(",");
			if(indice!=-1){
				//-- remplacement de la virgule par un point --//
				String section1=contenu.substring(0, contenu.lastIndexOf(","));
				String section2=contenu.substring(contenu.lastIndexOf(",")+1,contenu.length());
				saisie.setText(section1+"."+section2);
				
			}
		
		
		}
		
		
		
		
		return saisie.getText();
	}

}
