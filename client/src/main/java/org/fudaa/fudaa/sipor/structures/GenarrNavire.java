package org.fudaa.fudaa.sipor.structures;


/**
 * Classe qui d�crit lun navire g�n�r� par l'executable fortran g�narr.
 *@version $Version$
 * @author hadoux
 *
 */

public class GenarrNavire {

	int navire;
	int categorie;
	int jour;
	int heure;
	int minute;
	
	public GenarrNavire(int navire, int categorie, int jour, int heure, int minute) {
		super();
		this.navire = navire;
		this.categorie = categorie;
		this.jour = jour;
		this.heure = heure;
		this.minute = minute;
	}
	
	public GenarrNavire(GenarrNavire clone){
		navire=clone.navire;
		categorie=clone.categorie;
		jour=clone.jour;
		heure=clone.heure;
		minute=clone.minute;
	}

	public int getNavire() {
		return navire;
	}

	public void setNavire(int navire) {
		this.navire = navire;
	}

	public int getCategorie() {
		return categorie;
	}

	public void setCategorie(int categorie) {
		this.categorie = categorie;
	}

	public int getJour() {
		return jour;
	}

	public void setJour(int jour) {
		this.jour = jour;
	}

	public int getHeure() {
		return heure;
	}

	public void setHeure(int heure) {
		this.heure = heure;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}
	
	
}
