package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporQuais;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageQuais;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieQuais;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * @author Adrien Hadoux
 */

public class SiporVisualiserQuais extends SiporInternalFrame {

  /**
   * Layout cardlayout pour affichage des donnes
   */
  public CardLayout pile_;

  /**
   * Le panel de base qui contient tous les differents panels contient un layout de type CardLayout
   */
  public JPanel principalPanel_;

  /**
   * Panel d'affichage des differents Navires saisis layout classique flow layout ou grid layout
   */
  public SiporPanelAffichageQuais affichagePanel_;

  /**
   * ascenseur pour le panel d'affichage
   */
  public JScrollPane ascAff_;

  /**
   * panel qui contient l'ascenceur (OPTIONNEL!!!!!!!!!!)
   */
  public JPanel conteneurAffichage_;

  /**
   * Panel de saisie des donnes relative aux Navires
   */

  public SiporPanelSaisieQuais SaisieQuaiPanel_;

  /**
   * Panel de commande: panel qui contient les differnets boutons responsable de: -ajout -suppression -modification des
   * Navires
   */
  public JPanel controlePanel_;

  /**
   * Boutton de selection de la saisie
   */
  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton boutonAffichage_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_voir"), "Voir");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modif");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
  private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Suppr");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  /**
   * Donnees
   */
  SiporDataSimulation donnees_;

  boolean alterneTitre_ = true;

  /**
   * Constructeur de la Jframe
   */

  public SiporVisualiserQuais(final SiporDataSimulation d) {

    super("", true, true, true, true);
    // recuperation des donnes
    donnees_ = d;

    setTitle("Visualisation des quais");
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));
    // location de la JFrame:
    // this.setLocationRelativeTo(this.getParent());
    setSize(695, 495);

    /**
     * tooltiptext des boutons
     */
    this.boutonAffichage_.setToolTipText("Permet de visualiser la totalit� des donn�es sous forme d'un tableau");
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");
    this.duplication_
        .setToolTipText("Permet de dupliquer une donn�e: cliquez d'abord sur l'�l�ment � dupliquer dans le menu \"voir\"");
    this.impression_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra par la suite imprimer");

    // definition des differents Layout
    final Container contenu = getContentPane();

    this.principalPanel_ = new JPanel();

    pile_ = new CardLayout(30, 10);
    this.principalPanel_.setLayout(pile_);

    // definition du propre panel d'affichage des Navires
    this.affichagePanel_ = new SiporPanelAffichageQuais(donnees_);

    // definition de l ascenceur pour le panel d'affichage

    this.ascAff_ = new JScrollPane(affichagePanel_);
    // utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector(this.ascAff_);
    // definition du panel de saisie d'un bateau
    // FrameSaisieQuais fq=new FrameSaisieQuais(donnees.listebassin);
    // fq.setVisible(false);
    SaisieQuaiPanel_ = new SiporPanelSaisieQuais(donnees_, this);

    this.controlePanel_ = new JPanel();
    this.controlePanel_.setLayout(new FlowLayout());

    // ajout des 2 panel d'affichage et de saisie d'un Navire
    this.principalPanel_.add(this.ascAff_, "Affichage");
    this.principalPanel_.add(this.SaisieQuaiPanel_, "Saisie");

    // ajout des panel dans la frame
    contenu.add(principalPanel_);
    contenu.add(controlePanel_, "South");

    /**
     * ******************************************************************************** THE CONTROL PANEL YEAH
     * *******************************************************************************
     */

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserQuais.this.windowClosed();

      }
    });
    controlePanel_.add(quitter_);

    boutonSaisie_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.last(principalPanel_);

        // 1) changmement de titre
        setTitle("Saisie d'un nouveau quai");

        validate();

        SaisieQuaiPanel_.initialiser();
        SaisieQuaiPanel_.setBorder(SiporBordures.quai);
        // indique a la fenetre de saisie qu il ne s agira en aucun ca d une modification
        SaisieQuaiPanel_.UPDATE = false;
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonSaisie_);

    boutonAffichage_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.first(principalPanel_);

        alterneTitre_ = true;
        setTitle("Visualisation des Quais");

        affichagePanel_.maj(donnees_);
        affichagePanel_.revalidate();
        validate();
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonAffichage_);

    modification_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // 1) recuperation du numero de ligne du quai via la jtable
        final int numQuai = affichagePanel_.tableau_.getSelectedRow();
        if (numQuai == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur le quai �\n modifier dans le tableau d'affichage.").activate();

        } else {
          // 2.5 changmeent de fenetre
          pile_.last(principalPanel_);
          setTitle("Modification d'un quai");
          SaisieQuaiPanel_.setBorder(SiporBordures.quai2);
          validate();

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          SaisieQuaiPanel_.MODE_MODIFICATION(numQuai);

          // 3)lors de la sauvegarde , utilise le booleen pour remplacer(methode set des donn�es) au lieu de add
        }// fin du if

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(modification_);

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numQuai = affichagePanel_.tableau_.getSelectedRow();
        if (numQuai == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur le quai �\n supprimer dans le tableau d'affichage.").activate();
        } else if (donnees_.quaiPreferentielNavire(numQuai) != -1) {
          final int indiceNavire = donnees_.quaiPreferentielNavire(numQuai);
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "\n Erreur! Le navire "
              + donnees_.getCategoriesNavires_().retournerNavire(indiceNavire).getNom() + "est affect� au quai "
              + donnees_.getlQuais_().retournerQuais(numQuai).getNom()

              + "\n Vous devez modifier le quai pr�f�rentiel de ce navire \navant de pouvoir supprimer ce quai.")
              .activate();
        } else {

          // on s occupe de la supresion des quais:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT, "Etes-vous certain de vouloir supprimer le quai "
                  + donnees_.getlQuais_().retournerQuais(numQuai).getNom() + " ?").activate();

          if (confirmation == 0) {
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getlQuais_().suppression(numQuai);
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);

            // --On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });
    controlePanel_.add(suppression_);
    // controlePanel.setBorder(this.raisedBevel);

    this.duplication_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        System.out.println("test duplication: ");
        final int numQuai = affichagePanel_.tableau_.getSelectedRow();
        if (numQuai == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur le quai �\n dupliquer dans le tableau d'affichage.").activate();

        } else {

          // on s occupe de la duplication des quais:
          // 1)on demande confirmation:
          String confirmation = "";
          confirmation = JOptionPane.showInputDialog(null, "Nom du quai dupliqu�: ");
          if (donnees_.getlQuais_().existeDoublon(confirmation, -1)) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Nom d�j� pris.").activate();
            return;
          }

          if (!confirmation.equals("")) {
            // 2)ON RECOPIE TOUTES LES DONNEES DUPLIQUEES dans un nouveau quai quel on ajoute
            final SiporQuais nouveau = new SiporQuais();
            final SiporQuais quaiAdupliquer = donnees_.getlQuais_().retournerQuais(numQuai);

            nouveau.setBassin_(quaiAdupliquer.getBassin_());
            nouveau.setNomBassin_(quaiAdupliquer.getNomBassin_());
            nouveau.setDehalageAutorise_(quaiAdupliquer.isDehalageAutorise_());
            nouveau.setDureeIndispo_(quaiAdupliquer.getDureeIndispo_());
            nouveau.getH_().recopie(quaiAdupliquer.getH_());

            nouveau.setLoiIndispo_(quaiAdupliquer.getLoiIndispo_());
            nouveau.setLongueur_(quaiAdupliquer.getLongueur_());

            // on donne le nouveau nom du quai a dupliquer
            nouveau.setNom_(confirmation);

            // cas de la loi sur la frequence
            if (quaiAdupliquer.getTypeLoi_() == 0) {
              nouveau.setTypeLoi_(0);
              nouveau.setFrequenceMoyenne_(quaiAdupliquer.getFrequenceMoyenne_());
              nouveau.setLoiFrequence_(quaiAdupliquer.getLoiFrequence_());
            } else if (quaiAdupliquer.getTypeLoi_() == 1) {

              nouveau.setTypeLoi_(1);
              for (int i = 0; i < quaiAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) quaiAdupliquer
                    .getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            } else if (quaiAdupliquer.getTypeLoi_() == 2) {
              // cas loi journaliere
              nouveau.setTypeLoi_(2);
              for (int i = 0; i < quaiAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) quaiAdupliquer
                    .getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            }

            // on ajoute le nouveau quai dupliqu�
            donnees_.getlQuais_().ajout(nouveau);

            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
          }// fin cas ou l on confirme la suppression
        }

      }
    });
    this.controlePanel_.add(duplication_);

    impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporVisualiserQuais.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = affichagePanel_.titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.getlQuais_().getlQuais_().size() + 2][affichagePanel_.titreColonnes_.length];

          for (int i = 0; i < affichagePanel_.titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = affichagePanel_.titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
            modele.data_[i + 2] = affichagePanel_.ndata_[i];
          }

          modele.setNbRow(donnees_.getlQuais_().getlQuais_().size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon

      }// fin de la methode public actionPerformed

    });

    this.controlePanel_.add(impression_);

    /**
     * *************************************************************************************** THE PITI MENU QUI SERVENT
     * A RIEN ***************************************************************************************
     */

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserQuais.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    setVisible(true);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
