package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporBassin;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageBassins;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieBassin;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Fenetre principale de saisie et d affichage des bassins
 * 
 * @author Adrien Hadoux
 */

public class SiporVisualiserBassins extends SiporInternalFrame {

  // attributs

  /*
   * Les panels
   */

  JPanel controlePanel = new JPanel();

  // panel d affichage global:
  JPanel global = new JPanel();

  /**
   * Panel de saisie des diff�rents bassins
   */
  SiporPanelSaisieBassin saisieBassinPanel;

  /**
   * Panel d affichage des duff�rents bassins sous forme de Jtable necessaire pour recup�rer l indice du bateau
   * selectionn�
   */

  public SiporPanelAffichageBassins affichagePanel;

  /**
   * Ascenceur pour le jtable
   */
  JScrollPane ascenceur;
  // composants:

  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modifier");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
  public final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Supprimer");

  public JLabel mode_ = new JLabel("Saisie");

  /**
   * Parametres de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Constructeur de la fenetre prend en parametre les donn�es de la simulation
   * 
   * @param d donn�es de la simulation
   */
  public SiporVisualiserBassins(final SiporDataSimulation d) {

    super("", true, true, true, true);
    donnees_ = d;
    setTitle("Gestion des bassins");
    setSize(500, 340);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));
    this.getContentPane().setLayout(new BorderLayout());

    /**
     * tooltiptext des boutons
     */
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut d'abord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");

    /**
     * ***************************************************************** Gestion des controles de la fenetre
     */

    this.modification_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numBassin = affichagePanel.tableau_.getSelectedRow();
        if (numBassin == -1) {
          JOptionPane.showMessageDialog(null,
              "Vous devez cliquer sur le bassin � modifier dans le tableau d'affichage.",
              MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
        } else {

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          saisieBassinPanel.MODE_MODIFICATION(numBassin);

        }// fin du if

      }
    });
    // this.suppression=new JButton("Supprimer un bassin");
    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel.tableau_.getSelectedRow());
        final int numBassin = affichagePanel.tableau_.getSelectedRow();

        if (numBassin == -1) {
          JOptionPane.showMessageDialog(null,
              "Erreur! Vous devez cliquer sur le bassin � supprimer dans le tableau d'affichage.",
              MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
        } else if (donnees_.quaiAppartientBassin(numBassin) != -1) {
          final int indiceQuai = donnees_.quaiAppartientBassin(numBassin);
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "le bassin "
              + donnees_.getListebassin_().retournerBassin(numBassin) + "contient au moins un quai"
              + "\n dont le quai: " + donnees_.getlQuais_().retournerQuais(indiceQuai).getNom()
              + ".\nModifiez le bassin d'appartenance de ce quai avant\n de supprimer ce bassin.").activate();
        } else {

          // on s occupe de la supresion des quais:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT, "Etes-vous certain de vouloir supprimer le bassin "
                  + donnees_.getListebassin_().retournerBassin(numBassin) + " ?").activate();

          if (confirmation == 0) {
        	  SiporBassin selected =  donnees_.getListebassin_().retournerBassin2(numBassin);
        	  
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getListebassin_().suppression(numBassin);
            // 3)mise a jour de l affichage:
            affichagePanel.maj(donnees_);

            // --On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();
            
          //-- delete element in network --//
            donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_BASSIN,
            		selected);
            

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserBassins.this.windowClosed();

      }
    });
    /**
     * ****************************************************************** Affichage des composants:
     */

    // affichage des composants:
    global.setLayout(new GridLayout(2, 1));

    // panel d affichage des donn�es du bassin
    affichagePanel = new SiporPanelAffichageBassins(donnees_);
    this.ascenceur = new JScrollPane(this.affichagePanel);
    global.add(this.ascenceur);

    // panel de saisie des donn�es du bassin
    this.saisieBassinPanel = new SiporPanelSaisieBassin(donnees_, this);

    final JPanel inter = new JPanel();
    inter.setLayout(new GridLayout(1, 3));
    final JPanel affichMode = new JPanel();
    affichMode.add(this.mode_);
    inter.add(affichMode);

    inter.add(this.saisieBassinPanel);
    inter.add(new JLabel(""));
    inter.setBorder(SiporBordures.compound_);
    global.add(inter);

    this.getContentPane().add(global, BorderLayout.CENTER);

    // le panel de controle:

    this.controlePanel.add(quitter_);
    // this.controlePanel.add(ajout);
    this.controlePanel.add(modification_);
    this.controlePanel.add(suppression_);

    this.controlePanel.setBorder(SiporBordures.compound_);
    this.getContentPane().add(controlePanel, BorderLayout.SOUTH);

    setVisible(true);

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserBassins.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
