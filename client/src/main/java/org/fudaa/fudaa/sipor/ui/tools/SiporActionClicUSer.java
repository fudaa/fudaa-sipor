package org.fudaa.fudaa.sipor.ui.tools;

import java.util.ArrayList;

import org.fudaa.dodico.corba.sipor.SParametresGrapheTopologies;



/**
 * Classe qui contient l'ensemble des parametres pour une iteration du panel de dessin
 *@version $Version$
 * @author hadoux
 *
 */
public class SiporActionClicUSer {

	public SParametresGrapheTopologies graphe;
	
	public int compteur;
	
	public ArrayList tableauGare_ ;

	
	private int nbTotalGares_=0;
	/**
	 * constructeur de la classe.
	 * @param graphe
	 * @param compteur
	 * @param tableauGare_
	 */
	public SiporActionClicUSer(SParametresGrapheTopologies graphe, int compteur, ArrayList tableauGare_,int nbTotalGares) {
		
		this.graphe = graphe;
		this.compteur = compteur;
		this.tableauGare_ = tableauGare_;
		this.setNbTotalGares_(nbTotalGares);
	}
	public int getNbTotalGares_() {
		return nbTotalGares_;
	}
	public void setNbTotalGares_(int nbTotalGares_) {
		this.nbTotalGares_ = nbTotalGares_;
	}
	
	 
	
}
