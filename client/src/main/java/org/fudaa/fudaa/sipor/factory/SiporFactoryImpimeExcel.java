package org.fudaa.fudaa.sipor.factory;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleGenesCercles;

public class SiporFactoryImpimeExcel {

	
	public void imprime(SiporModeleGenesCercles modeleSipor){
		
		// generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ =new String[modeleSipor.getColumnCount()];
          for(int i=0;i<modeleSipor.getColumnCount();i++)
        	  modele.nomColonnes_ [i]=modeleSipor.getColumnName(i);

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr��
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[modeleSipor.getRowCount()+1][modeleSipor.getColumnCount()];

          for (int i = 0; i < modeleSipor.getColumnCount(); i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = modeleSipor.getColumnName(i);

          }

          for (int i = 0; i < modeleSipor.getRowCount(); i++) 
          for (int j = 0; j < modeleSipor.getColumnCount(); j++) 

            modele.data_[i + 1][j] = modeleSipor.getValueAt(i, j);
          

        modele.setNbRow(modeleSipor.getRowCount()+1);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }
		
	
	
}
