/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogChoice;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;

import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.SiporFactoryImpimeExcel;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleGenesCercles;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Panel qui g�re la saisie des dur�es de genes entre navire  dans les cercles d'�vitage.
 * @author Adrien Hadoux
 */
public class SiporPanelReglesGenesCercle extends SiporInternalFrame  implements Observer{

    

    /**
     * ComboBox des cheneaux
     */
    JComboBox ComboChoixCercle_ = new JComboBox();

    SiporModeleGenesCercles modeleTable_=null;
   
    /**
     * Tableau de type JTable qui contiendra les donnees des navires
     */

    public  BuTable tableau_;

   

    /**
     * Bouton de validation des regles de navigations.
     */

    final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
    private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
    private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
     private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "");

    /**
     * Panel qui contiendra le tableau.
     */
    public   JPanel global_ = new JPanel();

    /**
     * panel de controle
     */
    public JPanel controlPanel = new JPanel();
    /**
     * Bordure du tableau.
     */

    public  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

    /**
     * donn�es de la simulation
     */
    SiporDataSimulation donnees_;

    /**
     * Indice du chenal choisi (par defaut 0 => le premier chenal.
     */
    public int cercleChoisi_ = 0;

    /**
     * constructeur du panel d'affichage des bassins.
     * 
     * @param d donn�es de la simulation.
     */
    public SiporPanelReglesGenesCercle(final SiporDataSimulation _donnees) {

      super("", true, true, true, true);

      donnees_ = _donnees;
      donnees_.addObservers(this);
      global_.setLayout(new BorderLayout());

     
      this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);

      /**
       * Initialisation du nombre de cheneaux.
       */

      this.remplissage();
      //-- afichage des elements dans le tableau du premier cercle par defaut --//
      this.affichage(donnees_.getListeCercle_().retournerCercle(0));

      /**
       * Creation de la fenetre
       */

      setTitle(SiporConstantes.ConstanteTitreIHMgenesSwing
          + donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getNom_());
      setSize(800, 400);
      setBorder(SiporBordures.compound_);
      getContentPane().setLayout(new BorderLayout());

      //final JScrollPane ascenceur = new JScrollPane(global_);

      getContentPane().add(global_, BorderLayout.CENTER);
      
      controlPanel.setBorder(this.borduretab_);
      controlPanel.add(quitter_);
      
      controlPanel.add(new JLabel(SiporConstantes.titreComboCercles));
      controlPanel.add(this.ComboChoixCercle_);

      controlPanel.add(this.duplication_);
      controlPanel.add(this.impression_);

      controlPanel.add(validation_);

      getContentPane().add(controlPanel, BorderLayout.SOUTH);

      /**
       * Listener du comboChoix de chenal: lors dde la selection du chenal, on affiche (rafraichis) le tableau de choix
       * des regles de navigations pour ce chenal
       */
      this.duplication_.setToolTipText(SiporConstantes.toolTipDupliquer);
      this.impression_
          .setToolTipText(SiporConstantes.toolTipImprimer);

      quitter_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent e) {
          dispose();
        }
      });

      this.impression_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent e) {
          if(modeleTable_!=null)
            (new SiporFactoryImpimeExcel()).imprime( modeleTable_);
          
            
        }

      });

      this.duplication_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent e) {
          final String[] values = new String[donnees_.getListeCercle_().getListeCercles_().size()];

          for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
            values[i] = donnees_.getListeCercle_().retournerCercle(i).getNom_();
          }

          final BuDialogChoice choix = new BuDialogChoice(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "choix du cercle mod�le", "Le cercle"
                  + donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getNom_()+ " r�cup�re les r�gles du cercle choisi ci-dessous", values);

          final int reponse = choix.activate();
          if (reponse != -1)

          {
            final int positionCercle = donnees_.getListeCercle_().retourneIndice(choix.getValue());
            if (positionCercle != -1) {
              donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getRegleGenes().duplication(donnees_.getListeCercle_()
                  .retournerCercle(positionCercle).getRegleGenes().getVecteurDeVecteur_());

              // rafraichissement de la duplication
              affichage(donnees_.getListeCercle_()
                    .retournerCercle(positionCercle));
            } else {
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "La duplication est impossible. Le cercle choisi n'existe pas.").activate();
            }
          }

        }
      });


      this.ComboChoixCercle_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent ev) {
          // 1) enregistrement des modification poru le chanl en cours:
         
          // 2) recuperation de l indice du cercle choisi
          // et affectation dans la variable du chenal choisi
          cercleChoisi_ = ComboChoixCercle_.getSelectedIndex();
          setTitle("R�gles Navigation de croisements du cercle "
              + getCercleNom(cercleChoisi_));

          // 3) affichage(rafraichissement des donn�es)
          affichage(donnees_.getListeCercle_().retournerCercle(cercleChoisi_));

        }

      });

      // listener du bouton de validation

      this.validation_.addActionListener(new ActionListener() {

        public void actionPerformed(final ActionEvent e) {

          new BuDialogMessage(
              donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT,
              SiporConstantes.constanteValidationGeneCercle)
              .activate();
          dispose();

        }

      });

      // affichage de la frame
      setVisible(true);

    }
    
    public String getCercleNom(int position){
      return this.donnees_.getListeCercle_().retournerCercle(position).getNom_();
    }

    /**
     * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
     */
    public  void remplissage() {
    // 1) remplissage des cheneaux dansla jcomobobox correspondante
      this.ComboChoixCercle_.removeAllItems();

      for (int i = 0; i < this.donnees_.getListeCercle_().getListeCercles_().size(); i++) {
        this.ComboChoixCercle_.addItem(getCercleNom(i));
      }
    }

    /**
     * Methode d affichage des composants du BuTable et du tableau de combo Cette methode est a impl�menter dans les
     * classes d�riv�es pour chaque composants
     */
    public  void affichage(SiporCercle cercleChoisi) {
    
     donnees_.getCategoriesNavires_().affichage(); 
     
      modeleTable_=new SiporModeleGenesCercles(donnees_,cercleChoisi);
      this.tableau_ = new BuTable(modeleTable_) ;
      
      //-- mis a jour du content du tableau --//
      modeleTable_.fireTableDataChanged();
      
      tableau_.setRowSelectionAllowed(true);
      tableau_.setColumnSelectionAllowed(true);
      tableau_.setCellSelectionEnabled(true);

      tableau_.setBorder(this.borduretab_);
      ColumnAutoSizer.sizeColumnsToFit(tableau_);
      tableau_.revalidate();

      this.global_.removeAll();

      this.global_.add(new JScrollPane(tableau_), BorderLayout.CENTER);
      this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
      tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
      this.global_.revalidate();
      this.global_.updateUI();


      this.revalidate();
      this.updateUI();

    }

   

       public void update(Observable o, Object arg) {
      if(arg.equals("cercle")||arg.equals("navire")){
      }
    } 
    
  }
