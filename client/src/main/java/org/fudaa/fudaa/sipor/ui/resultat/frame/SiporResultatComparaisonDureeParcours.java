
/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sipor.SParametresSipor2;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeDureesParcours;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmesComparaisonSimulation;
import org.fudaa.fudaa.sipor.factory.SiporResource;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des comparaisons des resultats de la generation des bateaux
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatComparaisonDureeParcours extends SiporInternalFrame /*implements ActionListener */{

  /**
   * liste des projets pour la comparaison de simulation: limit� a 10 simulations
   */
  FudaaProjet[] listeProjet_ = new FudaaProjet[10];

  /**
   * liste des r�sultats de simulation des simulations
   */
  SParametresSipor2[] listeParamsSimu_ = new SParametresSipor2[10];

  /**
   * nombre de simulations comparables sur les 10
   */
  int nombreSimulationsComparees_ = 0;

  /**
   * tableau sopecifiant selon els criteres de comparaison si chacune des simulatiopns sont comparables et indique
   * l'indice du navire a comparer
   */
  int[] comparePossible_;
  int[] comparePossible2_;
  /**
   * tableau de checkBox destiner a l utilisateuir pour choisr les navire a visualiser sur les diff�rents supports.
   */
  JCheckBox[] tableauChoixSimulations_;

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Nombre de navires" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();
  String[] listeElt = { "Chenal", "Cercle", "Ecluse", "Quai" };
  JComboBox ListetypeElem1_ = new JComboBox(listeElt);
  JComboBox ListetypeElem2_ = new JComboBox(listeElt);
  JComboBox ListeElem1_ = new JComboBox();
  JComboBox ListeElem2_ = new JComboBox();

  String[] listeSens = { "Entrant", "Sortant" };
  JComboBox sens_ = new JComboBox(listeSens);

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  boolean seuil2_ = false;
  JTextField valSeuil2_ = new JTextField(6);
  JCheckBox valideSeuil2_ = new JCheckBox("Seuil", false);
  float valeurSeuil2 = 0;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatComparaisonDureeParcours(final SiporDataSimulation _donnees) {
    super("Comparaison dur�es de parcours", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    /**
     * ajout des simulation valides a comparer et chargement de leur donn�es:
     */
    tableauChoixSimulations_ = new JCheckBox[10];
    int nbProjets = 0;
    for (int i = 0; i < this.donnees_.getApplication().getListe_().getModel_().size(); i++) {
      // A) creation du projet
      this.listeProjet_[nbProjets] = new FudaaProjet(this.donnees_.getApplication().getApp(), new FudaaFiltreFichier(
          "sipor"));
      // B) ouverture du projet
      this.listeProjet_[nbProjets].ouvrir(this.donnees_.getApplication().getListe_().getModel_().get(i).toString());

      // C) verification des donn�es

      if (((SParametresSipor2) this.listeProjet_[nbProjets].getParam(SiporResource.parametres)).ResultatsCompletsSimulation == null)// on
                                                                                                                                    // ne
                                                                                                                                    // peut
                                                                                                                                    // pas
                                                                                                                                    // comparer
                                                                                                                                    // cette
                                                                                                                                    // simulation
      {

        new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "La simulation "
            + this.donnees_.getApplication().getListe_().getModel_().get(i).toString() + " n'est pas valide"
            + "\n car elle n'a pas encore �t� lanc�e." +
            "\nLancer les calculs de cette simulation avant " + "\n de pouvoir la comparer").activate();
      } else if (((SParametresSipor2) this.listeProjet_[nbProjets].getParam(SiporResource.parametres)).ResultatsCompletsSimulation.TOUTEDureesParoucrs == null)
      {
        new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "La simulation "
            + this.donnees_.getApplication().getListe_().getModel_().get(i).toString() + " n'est pas valide"
            + "\n car elle n'a pas encore �t� lanc�e." +
            "\nLancer les calculs de cette simulation avant " + "\n de pouvoir la comparer").activate();
      } else {
        this.listeParamsSimu_[nbProjets] = (SParametresSipor2) this.listeProjet_[nbProjets]
            .getParam(SiporResource.parametres);
        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[nbProjets].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[nbProjets].getFichier().lastIndexOf("/") + 1;
        }

        this.tableauChoixSimulations_[nbProjets] = new JCheckBox(this.listeProjet_[nbProjets].getFichier().substring(
            debut, this.listeProjet_[nbProjets].getFichier().lastIndexOf(".sipor")), true);
        this.tableauChoixSimulations_[nbProjets].addActionListener(this);
        nombreSimulationsComparees_++;
        nbProjets++;
      }

    }

    // liste des comparaisons possibles

    setSize(700, 500);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    this.getContentPane().add(this.optionPanel_, BorderLayout.WEST);
    // ajout du tableau dans le panel tabbed

    // ajout des courbes dans le panel de la sous fenetre

    panelPrincipal_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);
    panelPrincipal_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    /*******************************************************************************************************************
     * gestion du panel du haut
     ******************************************************************************************************************/

    // panel qui contient les differents boutons
    this.controlPanel_.setLayout(new GridLayout(2, 1));

    final BuPanel panneau1 = new BuPanel();
    final BuPanel panneau2 = new BuPanel();
    panneau2.add(new JLabel("sens du trajet:"));
    panneau2.add(this.sens_);
    panneau2.add(new JLabel("Cat�gorie de navire � comparer:"));
    panneau2.add(this.ListeNavires_);
    panneau1.add(new JLabel("El�ment de d�part:"));
    panneau1.add(this.ListetypeElem1_);
    panneau1.add(this.ListeElem1_);

    panneau1.add(new JLabel("El�ment d'arriv�e:"));
    panneau1.add(this.ListetypeElem2_);
    panneau1.add(this.ListeElem2_);
    panneau2.add(this.validation_);
    // this.controlPanel_.add(this.ListeActions_);
    this.controlPanel_.add(panneau1);
    this.controlPanel_.add(panneau2);
    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Panneau de contr�le");
    this.controlPanel_.setBorder(bordurea);

    this.getContentPane().add(this.controlPanel_, BorderLayout.NORTH);

    this.ListeNavires_.addActionListener(this);
    this.ListeNavires_.setSelectedIndex(0);
    // this.ListeActions_.addActionListener(this);

    final ActionListener RemplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        int selection = 0;
        if (e.getSource() == ListetypeElem1_) {
          selection = ListetypeElem1_.getSelectedIndex();
        } else {
          selection = ListetypeElem2_.getSelectedIndex();
        }
        switch (selection) {

        case 3:
          if (e.getSource() == ListetypeElem1_) {
            ListeElem1_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElem1_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElem1_.validate();
          } else if (e.getSource() == ListetypeElem2_) {
            ListeElem2_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElem2_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElem2_.validate();
          }
          break;
        case 2:
          if (e.getSource() == ListetypeElem1_) {
            ListeElem1_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElem1_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElem1_.validate();
          } else if (e.getSource() == ListetypeElem2_) {
            ListeElem2_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElem2_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElem2_.validate();
          }
          break;
        case 0:
          if (e.getSource() == ListetypeElem1_) {
            ListeElem1_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElem1_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElem1_.validate();
          } else if (e.getSource() == ListetypeElem2_) {
            ListeElem2_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElem2_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElem2_.validate();
          }
          break;
        case 1:
          if (e.getSource() == ListetypeElem1_) {
            ListeElem1_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElem1_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElem1_.validate();
          } else if (e.getSource() == ListetypeElem2_) {
            ListeElem2_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElem2_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElem2_.validate();
          }
          break;
        }

        /**
         * detection d'un temps de sejour
         */
        if (e.getSource() == ListeElem1_ || e.getSource() == ListeElem2_) {
          if (ListetypeElem1_.getSelectedIndex() == ListetypeElem2_.getSelectedIndex()
              && ListeElem1_.getItemCount() != 0 && ListeElem2_.getItemCount() != 0
              && ((String) ListeElem1_.getSelectedItem()).equals(((String) ListeElem2_.getSelectedItem()))) {
            ListetypeElem2_.setEnabled(false);
            ListeElem2_.setEnabled(false);
            sens_.setEnabled(false);
          } else {
            ListetypeElem2_.setEnabled(true);
            ListeElem2_.setEnabled(true);
            sens_.setEnabled(true);
          }
        }
      }
    };
    this.ListetypeElem1_.addActionListener(RemplissageElement);
    this.ListetypeElem2_.addActionListener(RemplissageElement);
    this.validation_.addActionListener(this);

    this.ListetypeElem1_.setSelectedIndex(0);
    this.ListetypeElem2_.setSelectedIndex(3);

    this.ListeElem1_.addActionListener(RemplissageElement);
    this.ListeElem2_.addActionListener(RemplissageElement);

    // calcul des dur�es de parcours compar�es par d�faut
    comparePossible_ = VerificationComparaisonExistenceTrajet();
    comparePossible2_ = VerificationComparaisonExistenceNavires();

    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/


    final Box bVert2 = Box.createVerticalBox();
    for (int i = 0; i < nombreSimulationsComparees_; i++) {
      bVert2.add(this.tableauChoixSimulations_[i]);
    }
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "simu");
    bVert2.setBorder(bordure1);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    this.optionPanel_.add(pcnasc);
    this.optionPanel_.setBorder(this.compound_);

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    this.valSeuil2_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil2_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil2_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil2_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil2_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil2_.setText("");
          }
        }
      }
    });
    valideSeuil2_.addActionListener(this);

    this.controlPanelCourbes_.add(new JLabel(" Seuil:"));
    this.controlPanelCourbes_.add(valSeuil2_);
    this.controlPanelCourbes_.add(valideSeuil2_);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme.");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatComparaisonDureeParcours.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatComparaisonDureeParcours.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    String g = "";

    // determiner el nombre de cat�gories de navires selectionn�s

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceSimu = 0;

    g += "graphe\n{\n";
    g += "  titre \" Comparaison de simulation \"\n";
    g += "  sous-titre \"Dur�es de parcours du " + (String) this.ListeElem1_.getSelectedItem() + " � "
        + (String) this.ListeElem2_.getSelectedItem() + ". Sens: " + (String) this.sens_.getSelectedItem()
        + ". Navire: " + (String) this.ListeNavires_.getSelectedItem() + " \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" simu \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (this.nombreSimulationsComparees_ + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
        + "\n";
    System.out.print("nb comparaisons: " + this.nombreSimulationsComparees_);

    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" " + "\"\n";
    g += "    unite \"" + " H.MIN" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmesComparaisonSimulation
            .determinerMaxDureeParcours(this.listeParamsSimu_, this.nombreSimulationsComparees_)) + "\n";
    System.out.println("\n MAX= "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeDureesParcours
            .determineDureeMaxi(this.donnees_)) + " \n");

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += " dur�e de parcours maximum";

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 0 \n";
    g += "surface.couleur BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BB0000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceSimu = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceSimu + 1)// numero de la cat�gorie
            + " ";

        g += SiporTraduitHoraires
            .traduitMinutesEnHeuresMinutes(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeMaximumParcours);

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("/") + 1;
        }

        g += "\n etiquette  \n \""
            + this.listeProjet_[n].getFichier().substring(debut,
                this.listeProjet_[n].getFichier().lastIndexOf(".sipor")) + "\" \n" + "\n";
        indiceSimu++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += " dur�e de parcours moyenne";

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 0 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BB8800 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceSimu = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceSimu + 1)// numero de la cat�gorie
            + " ";
        g += SiporTraduitHoraires
            .traduitMinutesEnHeuresMinutes((1.0 * this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeParcoursTotale / this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].nombreNavires))

            + "\n";
        indiceSimu++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += " dur�e de parcours minimum";

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 0 \n";
    g += "surface.couleur BBCC00 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BBCC00 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceSimu = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceSimu + 1)// numero de la cat�gorie
            + " ";
        System.out
            .println("valeur simu "
                + n
                + ": "
                + this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeMinimumParcours);
        g += SiporTraduitHoraires
            .traduitMinutesEnHeuresMinutes(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeMinimumParcours);

        g += "\n";
        indiceSimu++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    if (seuil2_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil2 + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    String g = "";

    // determiner el nombre de cat�gories de navires selectionn�s

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceSimu = 0;

    g += "graphe\n{\n";
    g += "  titre \" Comparaison de simulation \"\n";
    g += "  sous-titre \"Dur�es de parcours du " + (String) this.ListeElem1_.getSelectedItem() + " � "
        + (String) this.ListeElem2_.getSelectedItem() + ". Sens: " + (String) this.sens_.getSelectedItem()
        + ". Navire: " + (String) this.ListeNavires_.getSelectedItem() + " \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" simu \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (this.nombreSimulationsComparees_ + 3)
        + "\n";
    System.out.print("nb comparaisons: " + this.nombreSimulationsComparees_);

    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" " + "\"\n";
    g += "    unite \"" + " H.MIN" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmesComparaisonSimulation
            .determinerMaxDureeParcours(this.listeParamsSimu_, this.nombreSimulationsComparees_)) + "\n";
    System.out.println("\n MAX= "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeDureesParcours
            .determineDureeMaxi(this.donnees_)) + " \n");

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += " dur�e de parcours maximum";

    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceSimu = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceSimu + 1)// numero de la cat�gorie
            + " ";

        g += SiporTraduitHoraires
            .traduitMinutesEnHeuresMinutes(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeMaximumParcours);

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("/") + 1;
        }

        g += "\n etiquette  \n \""
            + this.listeProjet_[n].getFichier().substring(debut,
                this.listeProjet_[n].getFichier().lastIndexOf(".sipor")) + "\" \n" + "\n";
        indiceSimu++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += " dur�e de parcours moyenne";

    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceSimu = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceSimu + 1)// numero de la cat�gorie
            + " ";
        g += SiporTraduitHoraires
            .traduitMinutesEnHeuresMinutes((1.0 * this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeParcoursTotale / this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].nombreNavires))

            + "\n";
        indiceSimu++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += " dur�e de parcours minimum";

    g += "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BBCC00 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceSimu = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceSimu + 1)// numero de la cat�gorie
            + " ";
        System.out
            .println("valeur simu "
                + n
                + ": "
                + this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeMinimumParcours);
        g += SiporTraduitHoraires
            .traduitMinutesEnHeuresMinutes(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTEDureesParoucrs[this.comparePossible_[n]].DureeParcoursCategorie[this.comparePossible2_[n]].dureeMinimumParcours);

        g += "\n";
        indiceSimu++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    // si la source provient d un navire du tableau de checkBox
    if (source == this.validation_) {
      if (this.ListetypeElem1_.getSelectedIndex() == 3 && ListetypeElem2_.getSelectedIndex() == 3) {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
            "Erreur!\n Aucune dur�e de parcours d'un quai vers un autre").activate();
        return;
      }
      // on v�rifie que la comparaison est possible
      comparePossible_ = VerificationComparaisonExistenceTrajet();
      comparePossible2_ = VerificationComparaisonExistenceNavires();
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    }
    for (int i = 0; i < this.tableauChoixSimulations_.length; i++) {
      if (source == this.tableauChoixSimulations_[i]) {
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    if (source == this.valideSeuil_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    if (source == this.valideSeuil2_) {
      if (this.valideSeuil2_.isSelected() && !this.valSeuil2_.getText().equals("")) {
        // booleen passe a true
        this.seuil2_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil2 = Float.parseFloat(this.valSeuil2_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil2_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui permet de v�rifier si les simulations sont comparables:
   * 
   * @return un tableau d'indice correspondant � l'indice dasn le tableau des dur�e s de parcours de la dur�e de
   *         parcours choisie par l'utilisateur
   */
  public int[] VerificationComparaisonExistenceTrajet() {
    final int[] comparePossible = new int[10];

    final int type1 = this.ListetypeElem1_.getSelectedIndex();
    final int type2 = this.ListetypeElem2_.getSelectedIndex();
    final int elem1 = this.ListeElem1_.getSelectedIndex();
    final int elem2 = this.ListeElem2_.getSelectedIndex();

    boolean trouve = false;
    for (int i = 0; i < this.nombreSimulationsComparees_; i++) {
      trouve = false;
      for (int k = 0; !trouve && k < this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs.length; k++) {
        if (this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].typeElement1 == type1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].typeElement2 == type2
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].element1 == elem1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].element2 == elem2
            && this.sens_.getSelectedIndex() == this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].sens) {
          // le trajet voulu existe bien donc on le precise via le booleen et on stocke l'indice dans le tableau de
          // comparaison
          trouve = true;
          comparePossible[i] = k;
        } else if (this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].typeElement2 == type1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].typeElement1 == type2
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].element2 == elem1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].element1 == elem2
            && this.sens_.getSelectedIndex() == this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].sens) {
          // le trajet voulu existe bien donc on le precise via le booleen et on stocke l'indice dans le tableau de
          // comparaison
          trouve = true;
          comparePossible[i] = k;
        } else if (this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].typeElement2 == type1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].typeElement1 == type2
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].element2 == elem1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].element1 == elem2
            //FIXME Fred 
            && type2 == type1 && elem1 == elem2) {
          // le trajet voulu existe bien donc on le precise via le booleen et on stocke l'indice dans le tableau de
          // comparaison
          trouve = true;
          comparePossible[i] = k;
        }

      }
      if (trouve == false) {
        comparePossible[i] = -1;
      }

    }

    return comparePossible;
  }

  public int[] VerificationComparaisonExistenceNavires() {
    final int[] comparePossible2 = new int[10];
    // on v�rifie que l'�l�ment navire a comparer existe bien dans l'ensemble des simulations
//    final int indiceNavire = this.ListeNavires_.getSelectedIndex();
    final String nomNavire = (String) this.ListeNavires_.getSelectedItem();
    /*
     * //etape 1: on verifie qu'il y a bien moins d'�l�ments 
     */
    // etape 2: on v�rifie que le nom du navire existe bien dans chaque simu
    boolean trouve = false;
    for (int i = 0; i < this.nombreSimulationsComparees_; i++) {
      trouve = false;
      for (int k = 0; !trouve && k < this.listeParamsSimu_[i].navires.listeNavires.length; k++) {
        if ((this.listeParamsSimu_[i].navires.listeNavires[k].nom).equals(nomNavire)) {
          trouve = true;
          comparePossible2[i] = k;
        }
      }
      if (trouve) {} else {
        comparePossible2[i] = -1;
      }
    }

    return comparePossible2;
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
