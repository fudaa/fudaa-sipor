package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;


/**
 * Declaration d'une classe servant de structure pour un tableau de stockage de couple dans le cas de la loi
 * deterministe
 */

public class SiporQuais {


  /**
   * Nom du quai
   */

  String Nom_;

  /**
   * Dimension du port
   */
  double longueur_;

  /**
   * Booleen qui precise sir le dehalage est autorise auquel cas c 'est true
   */
  boolean dehalageAutorise_ = true;

  /**
   * Indice qui correspond au numro du bassin qui contient le quai
   */
  int bassin_;

  /**
   * chaine de caractere correpondante au nom du bassin choisi
   */
  String nomBassin_;

  double dureeIndispo_;
  double frequenceMoyenne_;

  // loi: indice de 1 a 10
  int loiIndispo_;
  int loiFrequence_;

  /**
   * Type de loi ENTIER QUI PREND LA VALEUR DE LA LOI CHOISIE: 0 => loi d erlang 1 => deterministe 2 => journaliere par
   * defaut loi d'erlang
   */
  int typeLoi_ = 0;

  /**
   * Declaration du tableau de loi deterministe:
   */
  ArrayList loiDeterministe_ = new ArrayList();

  /**
   * Declaration des creneaux dans el cas de la loi journaliere
   */
  // CreneauxLoiJournaliere creneauLoiJournaliere= new CreneauxLoiJournaliere();

  // horaires:
  SiporHoraire h_ = new SiporHoraire();

  public  SiporQuais() {

  }

  /**
   * Methode d affichage d un quai
   */

  public void affichage() {
    System.out.println("\nNom du Quai: " + this.getNom() + "\nLongueur: " + this.longueur_);
    if (this.dehalageAutorise_) {
      System.out.println("D�halage autoris�");
    } else {
      System.out.println("D�halage interdit");
    }

  }

public String getNom() {
	return Nom_;
}

public void setNom_(String nom_) {
	Nom_ = nom_;
}

public double getLongueur_() {
	return longueur_;
}

public void setLongueur_(double longueur_) {
	this.longueur_ = longueur_;
}

public boolean isDehalageAutorise_() {
	return dehalageAutorise_;
}

public void setDehalageAutorise_(boolean dehalageAutorise_) {
	this.dehalageAutorise_ = dehalageAutorise_;
}

public int getBassin_() {
	return bassin_;
}

public void setBassin_(int bassin_) {
	this.bassin_ = bassin_;
}

public String getNomBassin_() {
	return nomBassin_;
}

public void setNomBassin_(String nomBassin_) {
	this.nomBassin_ = nomBassin_;
}

public double getDureeIndispo_() {
	return dureeIndispo_;
}

public void setDureeIndispo_(double dureeIndispo_) {
	this.dureeIndispo_ = dureeIndispo_;
}

public double getFrequenceMoyenne_() {
	return frequenceMoyenne_;
}

public void setFrequenceMoyenne_(double frequenceMoyenne_) {
	this.frequenceMoyenne_ = frequenceMoyenne_;
}

public int getLoiIndispo_() {
	return loiIndispo_;
}

public void setLoiIndispo_(int loiIndispo_) {
	this.loiIndispo_ = loiIndispo_;
}

public int getLoiFrequence_() {
	return loiFrequence_;
}

public void setLoiFrequence_(int loiFrequence_) {
	this.loiFrequence_ = loiFrequence_;
}

public int getTypeLoi_() {
	return typeLoi_;
}

public void setTypeLoi_(int typeLoi_) {
	this.typeLoi_ = typeLoi_;
}

public ArrayList getLoiDeterministe_() {
	return loiDeterministe_;
}

public void setLoiDeterministe_(ArrayList loiDeterministe_) {
	this.loiDeterministe_ = loiDeterministe_;
}

public SiporHoraire getH_() {
	return h_;
}

public void setH_(SiporHoraire h_) {
	this.h_ = h_;
}

}
