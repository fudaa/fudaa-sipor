package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresSipor2;

/**
 * Classe qui contient les algorithmes utiles � la comparaison de simulations entre elles.
 * 
 * @version $Version$
 * @author hadoux
 */
public class SiporAlgorithmesComparaisonSimulation {

  /**
   * Methode qui determine parmi la liste des simulation le nombre maximum de navires g�n�r�s m�thode utile pour les
   * limites de de l'axe pour les graphes ainsi que les histogrammes
   * 
   * @param _d
   * @return
   */
  public static int determinerMaxGenerationNavires(final SParametresSipor2[] _liste, final int _taille) {
    int max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.ResultatsGenerationNavires.length; k++) {
        if (max < _liste[i].ResultatsCompletsSimulation.ResultatsGenerationNavires[k]) {
          max = _liste[i].ResultatsCompletsSimulation.ResultatsGenerationNavires[k];
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  /**
   * Methode qui permet de determiner le nb de navires max qui occupent le quai
   * 
   * @param _liste
   * @param _taille
   * @return
   */
  public static int determinerMaxNaviresOccupationGlobale(final SParametresSipor2[] _liste, final int _taille) {
    int max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.tableauOccupationGlobal.length; k++) {
        if (_liste[i].ResultatsCompletsSimulation.tableauOccupationGlobal[k].nombreNaviresTotal > max) {
          max = _liste[i].ResultatsCompletsSimulation.tableauOccupationGlobal[k].nombreNaviresTotal;
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  public static int determinerMaxNaviresOccupationDetaillee(final SParametresSipor2[] _liste, final int _taille) {
    int max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais.length; k++) {
        for (int l = 0; l < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires.length; l++) {
          if (_liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].nbNaviresUtiliseQuai > max) {
            max = _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].nbNaviresUtiliseQuai;
          }
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  public static double determinerMaxTSTotalOccupationDetaillee(final SParametresSipor2[] _liste, final int _taille) {
    double max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais.length; k++) {
        for (int l = 0; l < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires.length; l++) {
          if (_liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tempsServiceTotalAuQuai > max) {
            max = _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tempsServiceTotalAuQuai;
          }
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  public static double determinerMaxTSMoyenneOccupationDetaillee(final SParametresSipor2[] _liste, final int _taille) {
    double max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais.length; k++) {
        for (int l = 0; l < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires.length; l++) {
          if (_liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tempsServiceMaxiAuQuai > max) {
            max = _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tempsServiceMaxiAuQuai;
          }
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  public static double determinerMaxTonnageOccupationDetaillee(final SParametresSipor2[] _liste, final int _taille) {
    double max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais.length; k++) {
        for (int l = 0; l < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires.length; l++) {
          if (_liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tonnage > max) {
            max = _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tonnage;
          }
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  public static double determinerMaxTonnageMoyenOccupationDetaillee(final SParametresSipor2[] _liste, final int _taille) {
    double max = 0;

    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais.length; k++) {
        for (int l = 0; l < _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires.length; l++) {
          if (_liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tonnageMax > max) {
            max = _liste[i].ResultatsCompletsSimulation.tableauOccupationTousQuais[k].tableauOccupationQuaiParNavires[l].tonnageMax;
          }
        }
      }
    }

    return (int) (max + 0.2 * max);
  }

  public static float determinerMaxDureeParcours(final SParametresSipor2[] _liste, final int _taille) {
    double max = 0;
    for (int i = 0; i < _taille; i++) {
      for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs.length; k++) {
        if (_liste[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k] != null) {
          for (int l = 0; l < _liste[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].DureeParcoursCategorie.length; l++) {
            if (_liste[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].DureeParcoursCategorie[l].dureeMaximumParcours > max) {
              max = _liste[i].ResultatsCompletsSimulation.TOUTEDureesParoucrs[k].DureeParcoursCategorie[l].dureeMaximumParcours;
            }
          }
        }
      }
    }

    return (float) (max + 0.2 * max);
  }

  
  
  
 public static float determinerMaxAttente(final SParametresSipor2[] _liste, final int _taille) {
	 double max = 0;
	 
	 for (int i = 0; i < _taille; i++) {
		 for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length;k++)
		 {
			 
			 if(_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k]!=null){
				 for (int l = 0; l <_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories.length;l++){
					 if(_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteAccesMaxi>max)
						 max=_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteAccesMaxi;
					 if(_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteMareeMaxi>max)
						 max=_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteMareeMaxi;
					 if(_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteOccupMaxi>max)
						 max=_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteOccupMaxi;
					 if(_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attentePanneMaxi>max)
						 max=_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attentePanneMaxi;
					 if(_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteSecuMaxi>max)
						 max=_liste[i].ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].tableauAttenteCategories[l].attenteSecuMaxi;
				 }
					 
			 }
				 
			 
			 
		 }
	 }
	 
	 
	 return (float) (max + 0.2 * max);
 }
  
 
 public static float determinerMaxAttenteTrajet(final SParametresSipor2[] _liste, final int _taille) {
	 double max = 0;

	 for (int i = 0; i < _taille; i++) {
		 for (int k = 0; k < _liste[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet.length;k++){

			 if(_liste[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k]!=null){
				 for (int s=0;s<_liste[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].tableauAttenteCategories.length;s++)
				 {
					 org.fudaa.dodico.corba.sipor.SParametresResultatsAttenteCategorie position=_liste[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].tableauAttenteCategories[s];

					 if(position.attenteAccesMaxi>max)
						 max=position.attenteAccesMaxi;
					 if(position.attenteMareeMaxi>max)
						 max=position.attenteMareeMaxi;
					 if(position.attenteOccupMaxi>max)
						 max=position.attenteOccupMaxi;
					 if(position.attentePanneMaxi>max)
						 max=position.attentePanneMaxi;
					 if(position.attenteSecuMaxi>max)
						 max=position.attenteSecuMaxi;
					 if(position.attenteTotaleMaxi>max)
						 max=position.attenteTotaleMaxi;
				 }

			 }
		 }
	 }
	 return (float) (max + 0.2 * max);
 }

}
