package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporChenal;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageChenal;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieChenal;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * Fenetre principale de gestion des cheneaux
 * 
 * @author Adrien Hadoux
 */

public class SiporVisualiserChenal extends SiporInternalFrame {

  /**
   * Layout cardlayout pour affichage des donnes
   */
  private CardLayout pile_;

  /**
   * Le panel de base qui contient tous les differents panels contient un layout de type CardLayout
   */
  public JPanel principalPanel_;

  /**
   * Panel d'affichage des differents Navires saisis layout classique flow layout ou grid layout
   */
  public SiporPanelAffichageChenal affichagePanel_;

  /**
   * ascenseur pour le panel d'affichage
   */
  JScrollPane ascAff_;

  /**
   * panel qui contient l'ascenceur (OPTIONNEL!!!!!!!!!!)
   */
  public  JPanel conteneurAffichage_;

  /**
   * Panel de saisie des donnes relative aux Navires
   */

  public SiporPanelSaisieChenal SaisieChenalPanel_;

  /**
   * Panel de commande: panel qui contient les differnets boutons responsable de: -ajout -suppression -modification des
   * Navires
   */
  public JPanel controlePanel_;

  /**
   * Boutton de selection de la saisie
   */
  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton boutonAffichage_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_voir"), "Voir");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modif");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_valider"), "Quitter");
  private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Suppr");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  
  /**
   * Donnees
   */
  SiporDataSimulation donnees_;

  boolean alterneTitre_ = true;

  /**
   * Constructeur de la Jframe
   */

  public SiporVisualiserChenal(final SiporDataSimulation d) {
    super("", true, true, true, true);
    // recuperation des donnes
    donnees_ = d;

    setTitle("Gestion des chenaux");
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    // location de la JFrame:
    // this.setLocationRelativeTo(this.getParent());
    setSize(700, 490);

    /**
     * tooltiptext des boutons
     */
    this.boutonAffichage_.setToolTipText("Permet de visualiser la totalit� des donn�es sous forme d'un tableau");
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");
    this.duplication_
        .setToolTipText("Permet de dupliquer une donn�e: cliquez d'abord sur l'�l�ment � dupliquer dans le menu \"voir\"");
    this.impression_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra par la suite imprimer");

    // definition des differents Layout
    final Container contenu = getContentPane();

    this.principalPanel_ = new JPanel();

    setPile_(new CardLayout(30, 10));
    this.principalPanel_.setLayout(getPile_());

    // definition du propre panel d'affichage des Navires
    this.affichagePanel_ = new SiporPanelAffichageChenal(donnees_);

    // definition de l ascenceur pour le panel d'affichage

    //this.ascAff_ = new JScrollPane(affichagePanel_);
    //  utilisation de swingx
   // ScrollPaneSelector.installScrollPaneSelector(this.ascAff_);
    // definition du panel de saisie d'un bateau
    SaisieChenalPanel_ = new SiporPanelSaisieChenal(donnees_, this);

    this.controlePanel_ = new JPanel();
    this.controlePanel_.setLayout(new FlowLayout());

    // ajout des 2 panel d'affichage et de saisie d'un Navire
    this.principalPanel_.add(this.affichagePanel_, "Affichage");
    this.principalPanel_.add(this.SaisieChenalPanel_, "Saisie");

    // ajout des panel dans la frame
    contenu.add(principalPanel_);
    contenu.add(controlePanel_, "South");

    /**
     * ******************************************************************************** THE CONTROL PANEL YEAH
     * *******************************************************************************
     */

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserChenal.this.windowClosed();

      }
    });
    controlePanel_.add(quitter_);

    boutonSaisie_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        getPile_().last(principalPanel_);

        // 1) changmement de titre
        setTitle("Saisie d'un nouveau chenal");
        validate();

        SaisieChenalPanel_.initialiser();
        SaisieChenalPanel_.setBorder(SiporBordures.chenaux);
        // indique a la fenetre de saisie qu il ne s agira en aucun ca d une modification
        SaisieChenalPanel_.UPDATE = false;
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonSaisie_);

    boutonAffichage_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        getPile_().first(principalPanel_);

        alterneTitre_ = true;
        setTitle("Visualisation des chenaux");

        affichagePanel_.maj(donnees_);
        affichagePanel_.revalidate();
        validate();
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonAffichage_);

    modification_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // 1) recuperation du numero de ligne du chenal via la jtable
        final int numChenal = affichagePanel_.tableau_.getSelectedRow();
        if (numChenal == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur le chenal �\n modifier dans le tableau d'affichage.").activate();
        } else {
          // 2.5 changmeent de fenetre
          getPile_().last(principalPanel_);
          setTitle("Modification d'un chenal");
          validate();

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          SaisieChenalPanel_.MODE_MODIFICATION(numChenal);
          SaisieChenalPanel_.setBorder(SiporBordures.chenaux2);
          // 3)lors de la sauvegarde , utilise le booleen pour remplacer(methode set des donn�es) au lieu de add
        }// fin du if

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(modification_);

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numChenal = affichagePanel_.tableau_.getSelectedRow();
        if (numChenal == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur le chenal �\n supprimer dans le tableau d'affichage.").activate();
        } else {

          // on s occupe de la supresion des chenaux:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT, "�tes vous sur de supprimer le chenal "
                  + donnees_.getListeChenal_().retournerChenal(numChenal).getNom_() + " ?").activate();

          if (confirmation == 0) {
        	  
        	SiporChenal selected =    donnees_.getListeChenal_().retournerChenal(numChenal);
        	  
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getListeChenal_().suppression(numChenal);

            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);

            /**
             * suppression d une ligne de la matrice des dur�es de parcours
             */
            donnees_.getReglesDureesParcoursChenal_().SuprimerLigne(numChenal);
            
            
          //-- delete element in network --//
            donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_CHENAL,
            		selected);
            

            //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });
    controlePanel_.add(suppression_);
  

    this.duplication_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        System.out.println("test duplication: ");
        final int numChenal = affichagePanel_.tableau_.getSelectedRow();
        if (numChenal == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur le chenal �\n dupliquer dans le tableau d'affichage.").activate();
        } else {

          // on s occupe de la duplication des quais:
          // 1)on demande confirmation:
          String confirmation = "";
          confirmation = JOptionPane.showInputDialog(null, "Nom du chenal dupliqu�: ");

          if (donnees_.getListeChenal_().existeDoublon(confirmation, -1)) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Nom d�j� utilis�.").activate();
            return;
          }

          if (!confirmation.equals("")) {
            // 2)ON RECOPIE TOUTES LES DONNEES DUPLIQUEES dans un nouveau quai quel on ajoute
            final SiporChenal nouveau = new SiporChenal(donnees_.getCategoriesNavires_().getListeNavires_().size());
            final SiporChenal chenalAdupliquer = donnees_.getListeChenal_().retournerChenal(numChenal);

            nouveau.setLongueur_(chenalAdupliquer.getLongueur_());
            nouveau.setProfondeur_(chenalAdupliquer.getProfondeur_());
            nouveau.setSoumisMaree_(chenalAdupliquer.isSoumisMaree_());
            nouveau.setVitesse_(chenalAdupliquer.getVitesse_());
            nouveau.getH_().recopie(chenalAdupliquer.getH_());
            
            
            //loi d indispo
            nouveau.setDureeIndispo_(chenalAdupliquer.getDureeIndispo_());
            nouveau.setLoiIndispo_(chenalAdupliquer.getLoiIndispo_());
            // cas de la loi sur la frequence
            if (chenalAdupliquer.getTypeLoi_() == 0) {
              nouveau.setTypeLoi_(0);
              nouveau.setFrequenceMoyenne_(chenalAdupliquer.getFrequenceMoyenne_());
              nouveau.setLoiFrequence_(chenalAdupliquer.getLoiFrequence_());
            } else if (chenalAdupliquer.getTypeLoi_() == 1) {

              nouveau.setTypeLoi_(1);
              for (int i = 0; i < chenalAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe(
                    (CoupleLoiDeterministe) chenalAdupliquer.getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            } else if (chenalAdupliquer.getTypeLoi_() == 2) {
              // cas loi journaliere
              nouveau.setTypeLoi_(2);
              for (int i = 0; i < chenalAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe(
                    (CoupleLoiDeterministe) chenalAdupliquer.getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            }
            
            
            // on donne le nouveau nom du quai a dupliquer
            nouveau.setNom_( confirmation);

            // on ajoute le nouveau quai dupliqu�
            donnees_.getListeChenal_().ajout(nouveau);

            /**
             * Regles durees de parcours ajout d une ligne
             */
            donnees_.getReglesDureesParcoursChenal_().ajoutLigne(donnees_.getCategoriesNavires_().getListeNavires_().size());

            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
            
            //-- add element in network --//
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_CHENAL,
            		nouveau);
            
            
          }// fin cas ou l on confirme la suppression
        }

      }
    });
    this.controlePanel_.add(duplication_);

    impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporVisualiserChenal.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = affichagePanel_.titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.getListeChenal_().getListeChenaux_().size() + 2][affichagePanel_.titreColonnes_.length];

          for (int i = 0; i < affichagePanel_.titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = affichagePanel_.titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
            modele.data_[i + 2] = affichagePanel_.ndata_[i];
          }

          modele.setNbRow(donnees_.getListeChenal_().getListeChenaux_().size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon

      }// fin de la methode public actionPerformed

    });

    this.controlePanel_.add(impression_);

    /**
     * *************************************************************************************** THE PITI MENU QUI SERVENT
     * A RIEN ***************************************************************************************
     */

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserChenal.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

public CardLayout getPile_() {
  return pile_;
}

public void setPile_(CardLayout pile_) {
  this.pile_ = pile_;
}

public SiporPanelAffichageChenal getAffichagePanel_() {
	return affichagePanel_;
}

public void setAffichagePanel_(SiporPanelAffichageChenal affichagePanel_) {
	this.affichagePanel_ = affichagePanel_;
}

}
