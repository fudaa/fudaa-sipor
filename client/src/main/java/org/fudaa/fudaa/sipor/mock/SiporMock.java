package org.fudaa.fudaa.sipor.mock;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporNavire;

/**
 * Simulate action done by the fortran brain. 
 * @author Adrien
 *
 */
public class SiporMock {

	
	public void mockGenerationBateaux(String path) {
		try {
			feedBateaux(  path,300);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void mockGeneral(final SiporDataSimulation data, final String pathSimu) {
		
		try {
			feedFileHis( data, pathSimu,300, data.getCategoriesNavires_().getListeNavires_());
			
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	public void feedBateaux(String pathSimu, int nbbateaux) throws IOException {
		PrintWriter writer = new PrintWriter(new File(pathSimu + ".arriv"));
		FortranWriter fortranwriter = new FortranWriter(writer);
		for(int i=0;i<nbbateaux;i++) {			
			fortranwriter.writeln(" 1 2 5 1");
		}
		fortranwriter.close();
	}
	
	
	
	
	
	private void feedFileHis(SiporDataSimulation data, String pathSimu , int nbBateaux, List<SiporNavire> listeBateau) throws IOException {
			PrintWriter writer = new PrintWriter(new File(pathSimu +".his"));
			FortranWriter fortranwriter = new FortranWriter(writer);
			float heureEntree = 1;
			float heureSortie = 1.2f;
			for(int i=0;i<nbBateaux;i++) {
				
				int random1or2= Math.random()>0.6f?1:2; 
				if(random1or2 > listeBateau.size())
					random1or2 = 1;
				float tempsSortie = 50.20f;
				fortranwriter.writeln((i+1)+" "+ random1or2 +" "+(200*random1or2*i) +" " + (300*random1or2 * i) + " 7");
				//-- write trajets entr�e puis sortie du port classique --//
				fortranwriter.writeln( "T 1 " + heureEntree + "  " + heureSortie + " 2.0 21.0 6.0 3.0 9");
				heureEntree += 0.2f;
				heureSortie += tempsSortie;
				fortranwriter.writeln( "C 1 " + heureEntree + "  " + heureSortie + " 11.0 5.0 1.5 3.0 2");
				heureEntree += 0.2f;
				heureSortie += tempsSortie;
				fortranwriter.writeln( "E 1 " + heureEntree + "  " + heureSortie + " 20.0 15.0 1.0 3.0 6");
				heureEntree += 0.2f;
				heureSortie += tempsSortie;
				fortranwriter.writeln( "Q 1 " + heureEntree + "  " + heureSortie + " 21.0 2.0 2.0 8.0 9");
				heureEntree += 0.2f;
				heureSortie += tempsSortie;
				fortranwriter.writeln( "E 1 " + heureEntree + "  " + heureSortie + " 10.0 5.0 2.0 8.0 22");
				heureEntree += 0.2f;
				heureSortie += tempsSortie;
				fortranwriter.writeln( "C 1 " + heureEntree + "  " + heureSortie + " 10.0 4.0 1.0 7.0 2");
				heureEntree += 0.2f;
				heureSortie += tempsSortie;	 
				fortranwriter.writeln( "T 1 " + heureEntree + "  " + heureSortie + " 12.0 3.0 11.0 13.0 8");
				 
			}
			fortranwriter.close();
	}
	
	
}
