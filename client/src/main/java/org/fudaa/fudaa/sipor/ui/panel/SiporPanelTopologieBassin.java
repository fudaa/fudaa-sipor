package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Panel d'affichage des donn�es topologiques sous forme d un tableau: cette classe permet de saisir les gares amont
 * et aval de chacun des chenaux cr��s.
 * AAAAAAAAAAAAAAAAAAATTTTTTTTTTTTTTTTTTTTTTTTTTEEEEEEEEEEEEEEEEEEEENNNNNNNNNNNNNNNNNNNNTTTTTTTTTTTIIIIIIIIIOOOOOONN ce
 * panel ne peut s utiliser qu a condition qu'il y ait au moins 2 gares pour joindre les 2 bouts!!!
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelTopologieBassin extends SiporInternalFrame implements Observer {

  /**
   * JCombo qui permettra de choisir pour chaque chenal les gares amont et gares avales
   */
	public JComboBox ComboGare;

  /**
   * Descriptif des elements des colonnes
   */
  public  String[] titreColonnes = { "Nom du bassin", "Gare Avant" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des bassins
   */

  public JTable tableau;

 

  /**
   * Bouton de validation des donn�es topolgiques saisies pour le bassin
   */
  public final BuButton validation = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * Panel qui contiendra le tableau
   */
  public JPanel global = new JPanel();

  /**
   * Bordure du tableau
   */

  public   Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  SiporDataSimulation d;

  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelTopologieBassin(final SiporDataSimulation de) {
    super("", true, true, true, true);
    // recuperation des donn�es de la simulation

    d = de;
    
    //-- enregistrement de l'observer aupres de l'observable --//
	d.addObservers(this);
    
	
	global.setLayout(new BorderLayout());

    /**
     * TEST SI AU MOINS 2 GARES EXISTENT
     */
    if (d.getListeGare_().getListeGares_().size() < 1) {
      new BuDialogError(d.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Erreur! Il n'existe pas de gare.").activate();
    }
    
    // remplissage des comboBox en onction des donn�es
    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();
    

    // listener du bouton de validation

    this.validation.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        miseAjourTopologie();

      }

    });

    /**
     * Creation de la fenetre
     */

    setTitle("Topologie des Bassins");
    setSize(400, 300);
    setBorder(SiporBordures.compound_);
    getContentPane().setLayout(new BorderLayout());

    final JScrollPane ascenceur = new JScrollPane(global);

    getContentPane().add(ascenceur, BorderLayout.CENTER);

    final JPanel controlPanel = new JPanel();
    controlPanel.add(validation);
    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  public void remplissage() {

    // 1)cr�ation du combo i des gares amont
    this.ComboGare = new JComboBox();

    // 3)remplissage du tableau de gare avec toutes les donn�es des gares
    for (int j = 0; j < d.getListeGare_().getListeGares_().size(); j++) {
      // ajout successif des differents noms de gare
      this.ComboGare.addItem(d.getListeGare_().retournerGare(j));

    }

  }

  /**
   * Methode d affichage des composants du JTable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  public void affichage() {

    final Object[][] ndata = new Object[ d.getListebassin_().getListeBassins_().size()][2];

    for (int i = 0; i <  d.getListebassin_().getListeBassins_().size(); i++) {

      ndata[i][0] = d.getListebassin_().retournerBassin(i);

      // dernieres valeur rentr�es pour les gares
      ndata[i][1] = d.getListeGare_().retournerGare(d.getListebassin_().retournerBassin2(i).getGareAmont());// comboBox i des gares
                                                                                              // amont

    }

    this.tableau = new JTable(ndata, this.titreColonnes) {
      public boolean isCellEditable(final int row, final int col) {
        if (col == 0) {
          return false;
        } else {
          return true;
        }
      }
    };

    tableau.setBorder(this.borduretab);

    // tableau.revalidate();
    // this.removeAll();
    this.global.add(/* ascenceur */tableau.getTableHeader(), BorderLayout.PAGE_START);
    this.global.add(tableau, BorderLayout.CENTER);

    final JPanel controlePanel = new JPanel();
    controlePanel.add(validation);

    /**
     * Creation pour les colonnes 2 et 3 d'objets de type jcombobox
     */
    final TableColumn gareAmontColumn = tableau.getColumnModel().getColumn(1);
    gareAmontColumn.setCellEditor(new DefaultCellEditor(this.ComboGare));

    global.add(controlePanel, BorderLayout.SOUTH);
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de verifier la pertinence des donn�es saisies: IE: v�rifie que les gares choisies en amont et
   * avales sont bien diff�rents pour une m�me �cluse   * 
   * @return true si les donn�es sont coh�rentes sinon retourne false et surtout indique a quel endroit se situe
   *         l'erreur de logique de la saisie
   */
  public boolean verificationCoherence() {
    return true;
  }

  /**
   * Methode qui permet de mettre a jour les gares saiies en amont et vaales pour chacun des cheneaux: v�rifie dans un
   * premier temps l coh�rence des donn�es:
   */

  public void miseAjourTopologie() {

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */
    if (this.verificationCoherence()) {

      /**
       * MODIFICAITON DES GARES AMONT ET AVALS POUR CHACUN DES CHENEAUX:
       */
      for (int i = 0; i <  d.getListebassin_().getListeBassins_().size(); i++) {
        // affichage de chacune des donn�es

        // recuperer une donn�e
        final String nomgareAmont = (String) this.tableau.getModel().getValueAt(i, 1);

        // ajout des gares amont et aval pour l'ecluse i

        /**
         * utilisation de la METHODE POUR RECUPERER LE NUMERO DE LA GARE A PARTIR DU NOM
         */
        this.d.getListebassin_().retournerBassin2(i).setGareAmont(d.getListeGare_().retrouverGare(nomgareAmont));
        
        
        //-- update network edge --//
        DefaultNetworkUserObject gareAmont = new DefaultNetworkUserObject(nomgareAmont,true);
        NetworkUserObject edge =   this.d.getListebassin_().retournerBassin2(i);
        this.d.getApplication().getNetworkEditor().connectElement(gareAmont, null, edge);

      }
      new BuDialogMessage(d.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
          "Les gares des bassins sont correctement saisies").activate();

      //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      d.baisserNiveauSecurite2();

      
      dispose();
    }// fin du if ajout des donn�es

  }

  public void update(Observable o, Object arg) {
	  if(arg.equals("gare") || arg.equals("bassin"))
	  {
		  remplissage();
		  affichage();
	  }
  }

}
