package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.fudaa.fudaa.sipor.ui.tools.SiporTextFieldDureePM;

/**
 * Fenetre de saisie des crneaux d'horaires standart
 * 
 * @author Adrien Hadoux
 */
public class SiporFrameSaisieHoraires extends SiporInternalFrame {

  // attributs

  /**
   * Panel global contenant toutes les donnes de la frame
   */
  JPanel global_;

  /**
   * JText du premier horaire a saisir
   */
  SiporTextFieldDureePM creneau1debut_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDureePM creneau1fin_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDureePM creneau2debut_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDureePM creneau2fin_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDureePM creneau3debut_ = new SiporTextFieldDureePM(3);
  SiporTextFieldDureePM creneau3fin_ = new SiporTextFieldDureePM(3);

  /**
   * Bouton de validation des horaires lance les tests decontroles de cohrence des donnes
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");

  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * Objet horaire qui sera rempli suite a la saisie de l utilisateur
   */
  SiporHoraire horaire_;

  /**
   * Constructeur de la fenetre de saisie des horaires
   * 
   * @param h horaire qui va etre rempli par l'utilisateur
   */

  public SiporFrameSaisieHoraires(final SiporHoraire h, final double periodeMaree) {

    super("", true, true, true, true);
    horaire_ = h;
    final double demiPeriodeMareeMinutes = .5 * SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(periodeMaree);
    setTitle("Saisie d'un horaire par rapport � la pleine mer");
    setSize(415, 180);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));

    /**
     * position relative a la position parent
     */
    // setLocationRelativeTo(this.getParent());

    /**
     * *************************************************** controles sur les composants
     * **************************************************
     */

    // listener du bouton de validation:
    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // lancement de la fonction de validation des dones saisies:
        creationHoraire();
      }
    });

    this.creneau1debut_.setToolTipText("Horaire d'ouverture au format \"h.mm\" (exemple : \"8.46\" signifie 8h46)");
    this.creneau2debut_.setToolTipText("Horaire d'ouverture au format \"h.mm\" (exemple : \"8.46\" signifie 8h46)");
    this.creneau3debut_.setToolTipText("Horaire d'ouverture au format \"h.mm\" (exemple : \"8.46\" signifie 8h46)");
    this.creneau1fin_.setToolTipText("Horaire de fermeture au format \"h.mm\" (exemple : \"8.46\" signifie 8h46)");
    this.creneau2fin_.setToolTipText("Horaire de fermeture au format \"h.mm\" (exemple : \"8.46\" signifie 8h46)");
    this.creneau3fin_.setToolTipText("Horaire de fermeture au format \"h.mm\" (exemple : \"8.46\" signifie 8h46)");

    if (horaire_.semaineCreneau1HeureArrivee != -1 && horaire_.semaineCreneau2HeureArrivee != -1
        && horaire_.semaineCreneau1HeureDep != -1 && horaire_.semaineCreneau2HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.creneau1debut_.setText("" + (float) horaire_.semaineCreneau1HeureDep);
      this.creneau1fin_.setText("" + (float) horaire_.semaineCreneau1HeureArrivee);
      this.creneau2debut_.setText("" + (float) horaire_.semaineCreneau2HeureDep);
      this.creneau2fin_.setText("" + (float) horaire_.semaineCreneau2HeureArrivee);
      this.creneau3debut_.setText("" + (float) horaire_.semaineCreneau3HeureDep);
      this.creneau3fin_.setText("" + (float) horaire_.semaineCreneau3HeureArrivee);
    } else {

      // valeur par defaut:
      this.creneau1debut_.setText(CtuluLibString.ZERO);
      this.creneau1fin_.setText(CtuluLibString.ZERO);
      this.creneau2debut_.setText(CtuluLibString.ZERO);
      this.creneau2fin_.setText(CtuluLibString.ZERO);
      this.creneau3debut_.setText(CtuluLibString.ZERO);
      this.creneau3fin_.setText(CtuluLibString.ZERO);
    }

    this.creneau1debut_.addFocusListener(new FocusAdapter()

    {
      public void focusGained(final FocusEvent e) {
        creneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!creneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1debut_.getText());
            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes
                || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes) {
              new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -"
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes) + " et "
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes)
                      + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau1debut_.setText("");
              creneau1debut_.requestFocus();
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur! Ce nombre n'existe pas!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau1debut_.setText("");
          }
        }
      }
    });

    this.creneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1fin_.getText());
            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes
                || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes) {
              new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -"
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes) + " et "
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes)
                      + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau1fin_.setText("");
              creneau1fin_.requestFocus();
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur! Ce nombre n'existe pas!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau1fin_.setText("");
          }
        }
      }
    });

    this.creneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2debut_.getText());
            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes
                || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes) {
              new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -"
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes) + " et "
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes)
                      + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau2debut_.setText("");
              creneau2debut_.requestFocus();
            }


          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur! Ce nombre n'existe pas!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau2debut_.setText("");
          }
        }
      }
    });

    this.creneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2fin_.getText());
            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes
                || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes) {
              new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -"
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes) + " et "
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes)
                      + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau2fin_.setText("");
              creneau2fin_.requestFocus();
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur! Ce nombre n'existe pas!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau2fin_.setText("");
          }
        }
      }
    });

    this.creneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau3debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau3debut_.getText());
            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes
                || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes) {
              new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -"
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes) + " et "
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes)
                      + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau3debut_.setText("");
              creneau3debut_.requestFocus();
            }

           

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur! Ce nombre n'existe pas!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
            creneau3debut_.setText("");
          }
        }
      }
    });

    this.creneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau3fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau3fin_.getText());
            if (SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) < -demiPeriodeMareeMinutes
                || SiporTraduitHoraires.traduitHeuresMinutesEnMinutes(i) > demiPeriodeMareeMinutes) {
              new BuDialogError(null, SiporImplementation.INFORMATION_SOFT,
                  "Le nombre doit �tre compris entre -"
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes) + " et "
                      + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(demiPeriodeMareeMinutes)
                      + "\n(+/- demi-p�riode de mar�e)").activate();
              creneau3fin_.setText("");
              creneau3fin_.requestFocus();
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur! Ce nombre n'existe pas!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
            creneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    global_ = new JPanel();
    global_.setLayout(new GridLayout(4, 1));

    // panel de saisie du premier creneau horaire
    final JPanel ch1 = new JPanel();

    ch1.add(new JLabel(" Cr�neau 1, Horaire: "));
    ch1.add(this.creneau1debut_);
    ch1.add(new JLabel("�"));
    ch1.add(this.creneau1fin_);
    ch1.add(new JLabel("HEURES.MINUTES"));
    ch1.setBorder(bordnormal_);
    global_.add(ch1);

    // panel de saisie du deuxieme horaire
    final JPanel ch2 = new JPanel();
    ch2.add(new JLabel(" Cr�neau 2, Horaire: "));
    ch2.add(this.creneau2debut_);
    ch2.add(new JLabel("�"));
    ch2.add(this.creneau2fin_);
    ch2.add(new JLabel("HEURES.MINUTES"));
    ch2.setBorder(bordnormal_);

    global_.add(ch2);

    // panel de saisie du troisi�me horaire
    final JPanel ch3 = new JPanel();
    ch3.add(new JLabel(" Cr�neau 3, Horaire: "));
    ch3.add(this.creneau3debut_);
    ch3.add(new JLabel("�"));
    ch3.add(this.creneau3fin_);
    ch3.add(new JLabel("HEURES.MINUTES"));
    ch3.setBorder(bordnormal_);

    global_.add(ch3);

    // panel qui contient le bouton de validation:
    final JPanel chV = new JPanel();
    chV.add(new JLabel("Cliquez ici pour valider: "));
    chV.add(this.validation_);
    chV.setBorder(bordnormal_);
    global_.add(chV);
    global_.setBorder(compound_);
    getContentPane().add(global_);

    // affichage de la fenetre
    setVisible(true);

  }

  /**
   * Methode booleene de validation des donnes saisies
   */
  boolean controle_creationHoraire() {

    if (this.creneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 1: heure de depart manquant!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 1: heure de fin manquant!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.creneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 2: heure de depart manquant!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 2: heure de fin manquant!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.creneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 3: heure de depart manquant!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur! Cr�neau 3: heure de fin manquant!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    final float a1 = Float.parseFloat(creneau1debut_.getText());
    final float a2 = Float.parseFloat(creneau1fin_.getText());
    final float b1 = Float.parseFloat(creneau2debut_.getText());
    final float b2 = Float.parseFloat(creneau2fin_.getText());
    final float c1 = Float.parseFloat(creneau3debut_.getText());
    final float c2 = Float.parseFloat(creneau3fin_.getText());

    // Contr�les phase 1 : s'assurer que les valeurs x1, x2 sont bien comprises dans entre -"demi-periode-maree

    // Contr�les phase 2 : s'assurer que : a1 <= a2 <= b1 <= b2 <= c1 <= c2, sauf dans le cas o� x1=x2=0
    if (a1 > a2) {
      JOptionPane.showMessageDialog(null,
          "L'horaire de fin du cr�neau 1 est inf�rieur � l'horaire de d�but de ce cr�neau", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    /*
     * else if(a1==a2 && b1==b2) { JOptionPane.showMessageDialog(null,
     * "Toutes les valeurs sont identiques. Ce ne sont pas des cr�neaux.", "Avertissement", JOptionPane.ERROR_MESSAGE);
     * return false; }
     */
    else if (b1 > b2) {
      JOptionPane.showMessageDialog(null,
          "L'horaire de fin du cr�neau 2 est ant�rieur � l'horaire de d�but de ce cr�neau", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (c1 > c2) {
      JOptionPane.showMessageDialog(null,
          "L'horaire de fin du cr�neau 3 est ant�rieur � l'horaire de d�but de ce cr�neau", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    else if (a1 > b2 && b2 != 0) {
      JOptionPane.showMessageDialog(null, "Le cr�neau 2 est ant�rieur au cr�neau 1", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (b1 > c2 && c2 != 0) {
      JOptionPane.showMessageDialog(null, "Le cr�neau 3 est ant�rieur au cr�neau 2", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    else if (b1 < a2 && (b1 != 0 || b2 != 0)) {
      JOptionPane.showMessageDialog(null,
          "Le cr�neau 2 est inclus dans le cr�neau 1. \nLes cr�neaux sont des intervalles de temps distincts.",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (c1 < b2 && (c1 != 0 || c2 != 0)) {
      JOptionPane.showMessageDialog(null,
          "Le cr�neau 3 est inclus dans le cr�neau 2. \nLes cr�neaux sont des intervalles de temps distincts.",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // tous les tests ont t ngatifs, les donnes sont donc correctes.
    return true;
  }

  /**
   * Methode de controle de creation des horaires
   */

  void creationHoraire() {

    if (controle_creationHoraire()) {

      // creation d'un nouvel objet horaire:
      horaire_.semaineCreneau1HeureDep = Float.parseFloat(this.creneau1debut_.getText());
      horaire_.semaineCreneau1HeureArrivee = Float.parseFloat(this.creneau1fin_.getText());
      horaire_.semaineCreneau2HeureDep = Float.parseFloat(this.creneau2debut_.getText());
      horaire_.semaineCreneau2HeureArrivee = Float.parseFloat(this.creneau2fin_.getText());
      horaire_.semaineCreneau3HeureDep = Float.parseFloat(this.creneau3debut_.getText());
      horaire_.semaineCreneau3HeureArrivee = Float.parseFloat(this.creneau3fin_.getText());

      // verification des donnes saisies:
      horaire_.affichage();

      // destruction de la frame:
      this.dispose();

    }

  }

}
