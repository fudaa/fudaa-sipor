package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;

import com.memoire.bu.BuTable;

import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * Panel d'affichage des donn�es des gares sous forme d un tableau BuTable: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelAffichageGares extends JPanel {

	 public String[] titreColonnes = { "Nom" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des bassins
   */

  public BuTable tableau_;

  /**
   * Bordure du tableau
   */

  public  Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   */
  public  SiporPanelAffichageGares(final SiporDataSimulation d) {

    setLayout(new BorderLayout());
    this.maj(d);

  }

  /**
   * Methode d'ajout d'une cellule
   */
  public  void maj(final SiporDataSimulation d) {

    final Object[][] ndata = new Object[d.getListeGare_().getListeGares_().size()][1];

    for (int i = 0; i < d.getListeGare_().getListeGares_().size(); i++) {
      final String nomGare = d.getListeGare_().retournerGare(i);

      ndata[i][0] = nomGare;

    }

    this.tableau_ = new BuTable(ndata, this.titreColonnes) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    tableau_.setBorder(this.borduretab);
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.add(tableau_, BorderLayout.CENTER);

    this.revalidate();
    this.updateUI();
  }

}
