package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuValueValidator;

import org.fudaa.fudaa.sipor.SiporImplementation;

public class SiporTextFieldInteger extends SiporTextField {

  public SiporTextFieldInteger() {
    super();
  }

  public SiporTextFieldInteger(int columns) {
    super(columns);
  }

  public SiporTextFieldInteger(String text, int columns) {
    super(text, columns);
  }

  public SiporTextFieldInteger(String text) {
    super(text);
  }

  /**
   * Methode destinee a etre surchargee par les classes h�ritant de SiporTextField. Permet de g�rer les contr�les apr�s
   * saisie de l'utilisateur
   **/
  protected void traitementApresFocus(FocusEvent e) {

    String contenu = this.getText();

    if (contenu.equals("")) return;

    //-- Traduction en integer --//

    try {
      int valeur = Integer.parseInt(contenu);
      if (valeur < 0) {
        new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Ce nombre est n�gatif").activate();
        setText("");
        this.requestFocus();
      }

    } catch (NumberFormatException e1) {
      new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Ce nombre n'est pas un entier.").activate();
      setText("");
      this.requestFocus();
    }

    SiporTextFieldInteger.this.setForeground(Color.black);
  }

  protected int taille() {
    return 4;
  }

  protected void specifier_validator() {
    this.setCharValidator(BuCharValidator.INTEGER);
    this.setStringValidator(BuStringValidator.INTEGER);
    this.setValueValidator(BuValueValidator.INTEGER);

  }
}
