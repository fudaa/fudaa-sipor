package org.fudaa.fudaa.sipor.ui.tools;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInternalFrame;

import org.fudaa.fudaa.sipor.factory.SiporResource;

public class SiporInternalFrame extends BuInternalFrame {

  public SiporInternalFrame() {
    super();
    siporStyle();
  }

  public SiporInternalFrame(String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
    super(title, resizable, closable, maximizable, iconifiable);
    siporStyle();
  }

  protected void siporStyle() {
    BuIcon icon = SiporResource.SIPOR.getIcon("iconesipor");

    this.setFrameIcon(icon);
    this.repaint();

  }

}
