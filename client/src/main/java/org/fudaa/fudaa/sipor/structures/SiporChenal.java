package org.fudaa.fudaa.sipor.structures;

import java.io.Serializable;
import java.util.ArrayList;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;


/**
 * 
 */

/**
 * Classe d�crivant le chenal:
 * 
 * @author Adrien Hadoux
 */
public class SiporChenal implements NetworkUserObject, Serializable{

  String nom_ = "";
  double longueur_ = -1;
  double profondeur_ = -1;
  boolean soumisMaree_ = false;
  double vitesse_ = -1;
  SiporHoraire horaires = new SiporHoraire();

  /**
   * donn�es topologiques
   */
  int gareAmont_ = 0;
  /**
   * donn�es topologiques
   */

  int gareAval_ = 1;

  /**
   * regles de navigations du chenal
   */
  SiporStrutureReglesNavigation reglesNavigation_;

  
  double dureeIndispo_;
  double frequenceMoyenne_;

  // loi: indice de 1 a 10
  int loiIndispo_;
  int loiFrequence_;

  /**
   * Type de loi ENTIER QUI PREND LA VALEUR DE LA LOI CHOISIE.<br>: 0 => loi d erlang<br>
   * 1 => deterministe <br>
   * 2 => journaliere par defaut loi d'erlang
   */
  int typeLoi_;
  
  /**
   * Declaration du tableau de loi deterministe.
   */
  ArrayList loiDeterministe_ = new ArrayList();
  
  /**
   * constructeur: initialisation des regles de navigations cette methode doit etre appel�e des que l'on cr�� un nouevau
   * chenal que l'on va stocker dans le vecteur de donn�es
   * 
   * @params numNavires. correspond au nombre de navires qui correspond a la dimension de la matrice:
   */
  public  SiporChenal(final int numNavires) {
    // initialisation de la matrice du chenal
    this.reglesNavigation_ = new SiporStrutureReglesNavigation(numNavires);
  }

  public  SiporChenal() {
	  this(0);
  }
  
  /**
   * methode d affichage des donn�es du chenal
   */
  public  String[] affichage() {
    final String[] t = new String[3];
    t[0] = "nom: " + nom_;
    t[1] = "\n Profondeur: " + (float) profondeur_;
    t[2] = "\n Soumis � la mar�e: " + soumisMaree_;

    return t;

  }

public String getNom_() {
	return nom_;
}

public void setNom_(String nom_) {
	this.nom_ = nom_;
}

public double getLongueur_() {
	return longueur_;
}

public void setLongueur_(double longueur_) {
	this.longueur_ = longueur_;
}

public double getProfondeur_() {
	return profondeur_;
}

public void setProfondeur_(double profondeur_) {
	this.profondeur_ = profondeur_;
}

public boolean isSoumisMaree_() {
	return soumisMaree_;
}

public void setSoumisMaree_(boolean soumisMaree_) {
	this.soumisMaree_ = soumisMaree_;
}

public double getVitesse_() {
	return vitesse_;
}

public void setVitesse_(double vitesse_) {
	this.vitesse_ = vitesse_;
}

public SiporHoraire getH_() {
	return horaires;
}

public void setH_(SiporHoraire h_) {
	this.horaires = h_;
}

public int getGareAmont_() {
	return gareAmont_;
}

public void setGareAmont_(int gareAmont_) {
	this.gareAmont_ = gareAmont_;
}

public int getGareAval_() {
	return gareAval_;
}

public void setGareAval_(int gareAval_) {
	this.gareAval_ = gareAval_;
}

public SiporStrutureReglesNavigation getReglesNavigation_() {
	return reglesNavigation_;
}

public void setReglesNavigation_(SiporStrutureReglesNavigation reglesNavigation_) {
	this.reglesNavigation_ = reglesNavigation_;
}

public double getDureeIndispo_() {
	return dureeIndispo_;
}

public void setDureeIndispo_(double dureeIndispo_) {
	this.dureeIndispo_ = dureeIndispo_;
}

public double getFrequenceMoyenne_() {
	return frequenceMoyenne_;
}

public void setFrequenceMoyenne_(double frequenceMoyenne_) {
	this.frequenceMoyenne_ = frequenceMoyenne_;
}

public int getLoiIndispo_() {
	return loiIndispo_;
}

public void setLoiIndispo_(int loiIndispo_) {
	this.loiIndispo_ = loiIndispo_;
}

public int getLoiFrequence_() {
	return loiFrequence_;
}

public void setLoiFrequence_(int loiFrequence_) {
	this.loiFrequence_ = loiFrequence_;
}

public int getTypeLoi_() {
	return typeLoi_;
}

public void setTypeLoi_(int typeLoi_) {
	this.typeLoi_ = typeLoi_;
}

public ArrayList getLoiDeterministe_() {
	return loiDeterministe_;
}

public void setLoiDeterministe_(ArrayList loiDeterministe_) {
	this.loiDeterministe_ = loiDeterministe_;
}

public String getName() {
	// TODO Auto-generated method stub
	return nom_;
}

public SiporHoraire getHoraires() {
	return horaires;
}

public void setHoraires(SiporHoraire horaires) {
	this.horaires = horaires;
}

public void setName(String value) {
	setNom_(value);
}

}
