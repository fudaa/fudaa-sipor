package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporEcluse;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageEcluse;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieEcluse;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * @author Adrien Hadoux
 */

public class SiporVisualiserEcluses extends SiporInternalFrame {

  /**
   * Layout cardlayout pour affichage des donnes
   */
  public CardLayout pile_;

  /**
   * Le panel de base qui contient tous les differents panels contient un layout de type CardLayout
   */
  public JPanel principalPanel_;

  /**
   * Panel d'affichage des differents Navires saisis layout classique flow layout ou grid layout
   */
  public SiporPanelAffichageEcluse affichagePanel_;

  /**
   * ascenseur pour le panel d'affichage
   */
  JScrollPane ascAff_;

  /**
   * panel qui contient l'ascenceur (OPTIONNEL!!!!!!!!!!)
   */
  JPanel conteneurAffichage_;

  /**
   * Panel de saisie des donnes relative aux Navires
   */

  public SiporPanelSaisieEcluse SaisieEclusePanel_;

  /**
   * Panel de commande: panel qui contient les differnets boutons responsable de: -ajout -suppression -modification des
   * Navires
   */
  JPanel controlePanel_;

  /**
   * Boutton de selection de la saisie
   */
  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton boutonAffichage_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_voir"), "Voir");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modif");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
  private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Suppr");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  /**
   * Donnees
   */
  SiporDataSimulation donnees_;

  boolean alterneTitre_ = true;

  /**
   * Constructeur de la Jframe
   */

  public SiporVisualiserEcluses(final SiporDataSimulation d) {

    super("", true, true, true, true);
    // recuperation des donnes
    donnees_ = d;

    setTitle("Visualisation des �cluses");

    // location de la JFrame:
    // this.setLocationRelativeTo(this.getParent());
    setSize(790, 625);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));
    /**
     * tooltiptext des boutons
     */
    this.boutonAffichage_.setToolTipText("Permet de visualiser la totalit� des donn�es sous forme d'un tableau");
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");
    this.duplication_
        .setToolTipText("Permet de dupliquer une donn�e: cliquez d'abord sur l'�l�ment � dupliquer dans le menu \"voir\"");
    this.impression_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra par la suite imprimer");

    // definition des differents Layout
    final Container contenu = getContentPane();

    this.principalPanel_ = new JPanel();

    pile_ = new CardLayout(30, 10);
    this.principalPanel_.setLayout(pile_);

    // definition du propre panel d'affichage des Navires
    this.affichagePanel_ = new SiporPanelAffichageEcluse(donnees_);

    // definition de l ascenceur pour le panel d'affichage

    //this.ascAff_ = new JScrollPane(affichagePanel_);
    // utilisation de swingx
   // ScrollPaneSelector.installScrollPaneSelector(this.ascAff_);
    // definition du panel de saisie d'un bateau
    // FrameSaisieQuais fq=new FrameSaisieQuais(donnees.listebassin);
    // fq.setVisible(false);
    SaisieEclusePanel_ = new SiporPanelSaisieEcluse(donnees_, this);

    this.controlePanel_ = new JPanel();
    this.controlePanel_.setLayout(new FlowLayout());

    // ajout des 2 panel d'affichage et de saisie d'un Navire
    this.principalPanel_.add(this.affichagePanel_, "Affichage");
    this.principalPanel_.add(this.SaisieEclusePanel_, "Saisie");

    // ajout des panel dans la frame
    contenu.add(principalPanel_);
    contenu.add(controlePanel_, "South");

    /**
     * ******************************************************************************** THE CONTROL PANEL YEAH
     * *******************************************************************************
     */

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserEcluses.this.windowClosed();

      }
    });
    controlePanel_.add(quitter_);

    boutonSaisie_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.last(principalPanel_);

        // 1) changmement de titre
        setTitle("Saisie d'une nouvelle �cluse");
        validate();

        SaisieEclusePanel_.initialiser();
        SaisieEclusePanel_.setBorder(SiporBordures.ecluse);
        // indique a la fenetre de saisie qu il ne s agira en aucun ca d une modification
        SaisieEclusePanel_.UPDATE = false;
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonSaisie_);

    boutonAffichage_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.first(principalPanel_);

        alterneTitre_ = true;
        setTitle("Visualisation des Ecluses");

        affichagePanel_.maj(donnees_);
        affichagePanel_.revalidate();
        validate();
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonAffichage_);

    modification_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // 1) recuperation du numero de ligne du quai via la jtable
        final int numEcluse = affichagePanel_.tableau_.getSelectedRow();
        if (numEcluse == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur l'�cluse �\n modifier dans le tableau d'affichage.").activate();
        } else {
          // 2.5 changmeent de fenetre
          pile_.last(principalPanel_);
          setTitle("Modification d'une �cluse");
          SaisieEclusePanel_.setBorder(SiporBordures.ecluse2);
          validate();

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          SaisieEclusePanel_.MODE_MODIFICATION(numEcluse);

          // 3)lors de la sauvegarde , utilise le booleen pour remplacer(methode set des donn�es) au lieu de add
        }// fin du if

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(modification_);

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numEcluse = affichagePanel_.tableau_.getSelectedRow();
        if (numEcluse == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur l'ecluse �\n supprimer dans le tableau d'affichage.").activate();
        } else {

          // on s occupe de la supresion des quais:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT, "Etes-vous sur de vouloir supprimer l'�cluse "
                  + donnees_.getListeEcluse_().retournerEcluse(numEcluse).getNom_()).activate();

          if (confirmation == 0) {
        	  SiporEcluse selected = donnees_.getListeEcluse_().retournerEcluse(numEcluse);
        	  
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getListeEcluse_().suppression(numEcluse);
            
            donnees_.getReglesRemplissageSAS().SuprimerLigne(numEcluse);
            
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);

            //-- delete element in network --//
            donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE,
            		selected);
            
            // --On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });
    controlePanel_.add(suppression_);
    // controlePanel.setBorder(this.raisedBevel);

    this.duplication_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        final int numEcluse = affichagePanel_.tableau_.getSelectedRow();
        if (numEcluse == -1) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "Erreur! Vous devez cliquer sur l'�cluse �\n dupliquer dans le tableau d'affichage.").activate();

        } else {

          // on s occupe de la duplication des ecluses:
          // 1)on demande confirmation:
          String confirmation = "";
          confirmation = JOptionPane.showInputDialog(null, "Nom de l'�cluse dupliqu�e: ");
          if (donnees_.getListeEcluse_().existeDoublon(confirmation, -1)) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Nom d�j� pris.").activate();
            return;
          }

          if (!confirmation.equals("")) {
            // 2)ON RECOPIE TOUTES LES DONNEES DUPLIQUEES dans une nouvelle ecluse quel on ajoute
            final SiporEcluse nouveau = new SiporEcluse();
            final SiporEcluse ecluseAdupliquer = donnees_.getListeEcluse_().retournerEcluse(numEcluse);

            nouveau.setLargeur_(ecluseAdupliquer.getLargeur_());
            nouveau.setLongueur_(ecluseAdupliquer.getLongueur_());
            nouveau.setTempsEclusee_(ecluseAdupliquer.getTempsEclusee_());
            nouveau.setCreneauEtaleApresPleineMerFin_(ecluseAdupliquer.getCreneauEtaleApresPleineMerFin_());
            nouveau.setCreneauEtaleAvantPleineMerDeb_(ecluseAdupliquer.getCreneauEtaleAvantPleineMerDeb_());
            nouveau.setH_(ecluseAdupliquer.getH_());
            nouveau.setTempsFausseBassinnee_(ecluseAdupliquer.getTempsFausseBassinnee_());
            nouveau.setDureePassageEtale_(ecluseAdupliquer.getDureePassageEtale_());
            nouveau.getH_().recopie(ecluseAdupliquer.getH_());

            // loi d indispo
            nouveau.setDureeIndispo_(ecluseAdupliquer.getDureeIndispo_());
            nouveau.setLoiIndispo_(ecluseAdupliquer.getLoiIndispo_());
            // cas de la loi sur la frequence
            if (ecluseAdupliquer.getTypeLoi_() == 0) {
              nouveau.setTypeLoi_(0);
              nouveau.setFrequenceMoyenne_(ecluseAdupliquer.getFrequenceMoyenne_());
              nouveau.setLoiFrequence_(ecluseAdupliquer.getLoiFrequence_());
            } else if (ecluseAdupliquer.getTypeLoi_() == 1) {

              nouveau.setTypeLoi_(1);
              for (int i = 0; i < ecluseAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) ecluseAdupliquer
                    .getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            } else if (ecluseAdupliquer.getTypeLoi_() == 2) {
              // cas loi journaliere
              nouveau.setTypeLoi_(2);
              for (int i = 0; i < ecluseAdupliquer.getLoiDeterministe_().size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) ecluseAdupliquer
                    .getLoiDeterministe_().get(i));
                nouveau.getLoiDeterministe_().add(c);

              }

            }

            // on donne le nouveau nom du quai a dupliquer
            nouveau.setNom_(confirmation);

            // on ajoute le nouveau quai dupliqu�
            donnees_.getListeEcluse_().ajout(nouveau);

            
            donnees_.getReglesRemplissageSAS().ajoutLigne(donnees_.getCategoriesNavires_().getListeNavires_().size());

            
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
            
            //-- add element in network --//
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE,
            		nouveau);
            
          }// fin cas ou l on confirme la suppression
        }

      }
    });
    this.controlePanel_.add(duplication_);

    /**
     * Bouton impression format excel
     */
    impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporVisualiserEcluses.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = affichagePanel_.titreColonnes;

          /**
           * transformation du model du tableau d�j� rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.getListeEcluse_().getListeEcluses_().size() + 2][affichagePanel_.titreColonnes.length];

          for (int i = 0; i < affichagePanel_.titreColonnes.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = affichagePanel_.titreColonnes[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
            modele.data_[i + 2] = affichagePanel_.ndata[i];
          }

          modele.setNbRow(donnees_.getListeEcluse_().getListeEcluses_().size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon

      }// fin de la methode public actionPerformed

    });

    this.controlePanel_.add(impression_);

    /**
     * *************************************************************************************** THE PITI MENU QUI SERVENT
     * A RIEN ***************************************************************************************
     */

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserEcluses.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
