package org.fudaa.fudaa.sipor.structures;

import java.text.DecimalFormat;

/**
 * Classe qui contient les informations des horaires
 * 
 * @author Adrien Hadoux
 */

public class SiporHoraire {
public  double semaineCreneau1HeureDep = -1, semaineCreneau1HeureArrivee = -1;
public   double semaineCreneau2HeureDep = -1, semaineCreneau2HeureArrivee = -1;
public   double semaineCreneau3HeureDep = -1, semaineCreneau3HeureArrivee = -1;

public   double lundiCreneau1HeureDep = -1, lundiCreneau1HeureArrivee = -1;
public   double lundiCreneau2HeureDep = -1, lundiCreneau2HeureArrivee = -1;
public   double lundiCreneau3HeureDep = -1, lundiCreneau3HeureArrivee = -1;

public double mardiCreneau1HeureDep = -1, mardiCreneau1HeureArrivee = -1;
public double mardiCreneau2HeureDep = -1, mardiCreneau2HeureArrivee = -1;
public double mardiCreneau3HeureDep = -1, mardiCreneau3HeureArrivee = -1;

public  double mercrediCreneau1HeureDep = -1, mercrediCreneau1HeureArrivee = -1;
public double mercrediCreneau2HeureDep = -1, mercrediCreneau2HeureArrivee = -1;
public double mercrediCreneau3HeureDep = -1, mercrediCreneau3HeureArrivee = -1;

public double jeudiCreneau1HeureDep = -1, jeudiCreneau1HeureArrivee = -1;
public double jeudiCreneau2HeureDep = -1, jeudiCreneau2HeureArrivee = -1;
public double jeudiCreneau3HeureDep = -1, jeudiCreneau3HeureArrivee = -1;

public double vendrediCreneau1HeureDep = -1, vendrediCreneau1HeureArrivee = -1;
public  double vendrediCreneau2HeureDep = -1, vendrediCreneau2HeureArrivee = -1;
public double vendrediCreneau3HeureDep = -1, vendrediCreneau3HeureArrivee = -1;

public  double samediCreneau1HeureDep = -1, samediCreneau1HeureArrivee = -1;
public double samediCreneau2HeureDep = -1, samediCreneau2HeureArrivee = -1;
public double samediCreneau3HeureDep = -1, samediCreneau3HeureArrivee = -1;

public double dimancheCreneau1HeureDep = -1, dimancheCreneau1HeureArrivee = -1;
public double dimancheCreneau2HeureDep = -1, dimancheCreneau2HeureArrivee = -1;
public double dimancheCreneau3HeureDep = -1, dimancheCreneau3HeureArrivee = -1;

public  double ferieCreneau1HeureDep = -1, ferieCreneau1HeureArrivee = -1;
public  double ferieCreneau2HeureDep = -1, ferieCreneau2HeureArrivee = -1;
public  double ferieCreneau3HeureDep = -1, ferieCreneau3HeureArrivee = -1;

public void affichage() {
    System.out.println("Creneau 1: heure depart: " + semaineCreneau1HeureDep + "\n 	heure d'arrive: "
        + semaineCreneau1HeureArrivee + "\n" + "Creneau 2: heure depart: " + semaineCreneau2HeureDep
        + "\n 	heure d'arrive: " + semaineCreneau2HeureArrivee + "\n Creneau 3: heure de depart: "
        + this.semaineCreneau3HeureDep + "\n             heure d arrivee:  " + this.semaineCreneau3HeureArrivee

    );

  }

  /**
   * Methode de recopie d'un horaire dans l horaire this Cette m�thode permet d'�viter une affectation d'adresse et
   * donc lors de la saisie d'un horaire pour une donn�e (quai, navire...) on peut utiliser le meme objet horaire ;
   * son adresse ne sera pas affect� aux nouveaux objets cr�� par la saisie et donc a chaque modification de l
   * objet horaire les horaire des objets cr��s ne seront pas modifi�
   * 
   * @param h horaire a recopier
   */

public void recopie(final SiporHoraire h) {
    this.dimancheCreneau1HeureArrivee = h.dimancheCreneau1HeureArrivee;
    this.dimancheCreneau1HeureDep = h.dimancheCreneau1HeureDep;
    this.dimancheCreneau2HeureArrivee = h.dimancheCreneau2HeureArrivee;
    this.dimancheCreneau2HeureDep = h.dimancheCreneau2HeureDep;
    this.dimancheCreneau3HeureArrivee = h.dimancheCreneau3HeureArrivee;
    this.dimancheCreneau3HeureDep = h.dimancheCreneau3HeureDep;
    this.ferieCreneau1HeureArrivee = h.ferieCreneau1HeureArrivee;
    this.ferieCreneau1HeureDep = h.ferieCreneau1HeureDep;
    this.ferieCreneau2HeureArrivee = h.ferieCreneau2HeureArrivee;
    this.ferieCreneau2HeureDep = h.ferieCreneau2HeureDep;
    this.ferieCreneau3HeureArrivee = h.ferieCreneau3HeureArrivee;
    this.ferieCreneau3HeureDep = h.ferieCreneau3HeureDep;

    this.lundiCreneau1HeureArrivee = h.lundiCreneau1HeureArrivee;
    this.lundiCreneau1HeureDep = h.lundiCreneau1HeureDep;
    this.lundiCreneau2HeureArrivee = h.lundiCreneau2HeureArrivee;
    this.lundiCreneau2HeureDep = h.lundiCreneau2HeureDep;
    this.lundiCreneau3HeureArrivee = h.lundiCreneau3HeureArrivee;
    this.lundiCreneau3HeureDep = h.lundiCreneau3HeureDep;

    this.mardiCreneau1HeureArrivee = h.mardiCreneau1HeureArrivee;
    this.mardiCreneau1HeureDep = h.mardiCreneau1HeureDep;
    this.mardiCreneau2HeureArrivee = h.mardiCreneau2HeureArrivee;
    this.mardiCreneau2HeureDep = h.mardiCreneau2HeureDep;
    this.mardiCreneau3HeureArrivee = h.mardiCreneau3HeureArrivee;
    this.mardiCreneau3HeureDep = h.mardiCreneau3HeureDep;

    this.mercrediCreneau1HeureArrivee = h.mercrediCreneau1HeureArrivee;
    this.mercrediCreneau1HeureDep = h.mercrediCreneau1HeureDep;
    this.mercrediCreneau2HeureArrivee = h.mercrediCreneau2HeureArrivee;
    this.mercrediCreneau2HeureDep = h.mercrediCreneau2HeureDep;
    this.mercrediCreneau3HeureArrivee = h.mercrediCreneau3HeureArrivee;
    this.mercrediCreneau3HeureDep = h.mercrediCreneau3HeureDep;

    this.jeudiCreneau1HeureArrivee = h.jeudiCreneau1HeureArrivee;
    this.jeudiCreneau1HeureDep = h.jeudiCreneau1HeureDep;
    this.jeudiCreneau2HeureArrivee = h.jeudiCreneau2HeureArrivee;
    this.jeudiCreneau2HeureDep = h.jeudiCreneau2HeureDep;
    this.jeudiCreneau3HeureArrivee = h.jeudiCreneau3HeureArrivee;
    this.jeudiCreneau3HeureDep = h.jeudiCreneau3HeureDep;

    this.vendrediCreneau1HeureArrivee = h.vendrediCreneau1HeureArrivee;
    this.vendrediCreneau1HeureDep = h.vendrediCreneau1HeureDep;
    this.vendrediCreneau2HeureArrivee = h.vendrediCreneau2HeureArrivee;
    this.vendrediCreneau2HeureDep = h.vendrediCreneau2HeureDep;
    this.vendrediCreneau3HeureArrivee = h.vendrediCreneau3HeureArrivee;
    this.vendrediCreneau3HeureDep = h.vendrediCreneau3HeureDep;

    this.samediCreneau1HeureArrivee = h.samediCreneau1HeureArrivee;
    this.samediCreneau1HeureDep = h.samediCreneau1HeureDep;
    this.samediCreneau2HeureArrivee = h.samediCreneau2HeureArrivee;
    this.samediCreneau2HeureDep = h.samediCreneau2HeureDep;
    this.samediCreneau3HeureArrivee = h.samediCreneau3HeureArrivee;
    this.samediCreneau3HeureDep = h.samediCreneau3HeureDep;

    this.semaineCreneau1HeureArrivee = h.semaineCreneau1HeureArrivee;
    this.semaineCreneau1HeureDep = h.semaineCreneau1HeureDep;
    this.semaineCreneau2HeureArrivee = h.semaineCreneau2HeureArrivee;
    this.semaineCreneau2HeureDep = h.semaineCreneau2HeureDep;
    this.semaineCreneau3HeureArrivee = h.semaineCreneau3HeureArrivee;
    this.semaineCreneau3HeureDep = h.semaineCreneau3HeureDep;

  }

public double getSemaineCreneau1HeureDep() {
	return semaineCreneau1HeureDep;
}

public void setSemaineCreneau1HeureDep(double semaineCreneau1HeureDep) {
	this.semaineCreneau1HeureDep = semaineCreneau1HeureDep;
}

public double getSemaineCreneau1HeureArrivee() {
	return semaineCreneau1HeureArrivee;
}

public void setSemaineCreneau1HeureArrivee(double semaineCreneau1HeureArrivee) {
	this.semaineCreneau1HeureArrivee = semaineCreneau1HeureArrivee;
}

public double getSemaineCreneau2HeureDep() {
	return semaineCreneau2HeureDep;
}

public void setSemaineCreneau2HeureDep(double semaineCreneau2HeureDep) {
	this.semaineCreneau2HeureDep = semaineCreneau2HeureDep;
}

public double getSemaineCreneau2HeureArrivee() {
	return semaineCreneau2HeureArrivee;
}

public void setSemaineCreneau2HeureArrivee(double semaineCreneau2HeureArrivee) {
	this.semaineCreneau2HeureArrivee = semaineCreneau2HeureArrivee;
}

public double getSemaineCreneau3HeureDep() {
	return semaineCreneau3HeureDep;
}

public void setSemaineCreneau3HeureDep(double semaineCreneau3HeureDep) {
	this.semaineCreneau3HeureDep = semaineCreneau3HeureDep;
}

public double getSemaineCreneau3HeureArrivee() {
	return semaineCreneau3HeureArrivee;
}

public void setSemaineCreneau3HeureArrivee(double semaineCreneau3HeureArrivee) {
	this.semaineCreneau3HeureArrivee = semaineCreneau3HeureArrivee;
}

public double getLundiCreneau1HeureDep() {
	return lundiCreneau1HeureDep;
}

public void setLundiCreneau1HeureDep(double lundiCreneau1HeureDep) {
	this.lundiCreneau1HeureDep = lundiCreneau1HeureDep;
}

public double getLundiCreneau1HeureArrivee() {
	return lundiCreneau1HeureArrivee;
}

public void setLundiCreneau1HeureArrivee(double lundiCreneau1HeureArrivee) {
	this.lundiCreneau1HeureArrivee = lundiCreneau1HeureArrivee;
}

public double getLundiCreneau2HeureDep() {
	return lundiCreneau2HeureDep;
}

public void setLundiCreneau2HeureDep(double lundiCreneau2HeureDep) {
	this.lundiCreneau2HeureDep = lundiCreneau2HeureDep;
}

public double getLundiCreneau2HeureArrivee() {
	return lundiCreneau2HeureArrivee;
}

public void setLundiCreneau2HeureArrivee(double lundiCreneau2HeureArrivee) {
	this.lundiCreneau2HeureArrivee = lundiCreneau2HeureArrivee;
}

public double getLundiCreneau3HeureDep() {
	return lundiCreneau3HeureDep;
}

public void setLundiCreneau3HeureDep(double lundiCreneau3HeureDep) {
	this.lundiCreneau3HeureDep = lundiCreneau3HeureDep;
}

public double getLundiCreneau3HeureArrivee() {
	return lundiCreneau3HeureArrivee;
}

public void setLundiCreneau3HeureArrivee(double lundiCreneau3HeureArrivee) {
	this.lundiCreneau3HeureArrivee = lundiCreneau3HeureArrivee;
}

public double getMardiCreneau1HeureDep() {
	return mardiCreneau1HeureDep;
}

public void setMardiCreneau1HeureDep(double mardiCreneau1HeureDep) {
	this.mardiCreneau1HeureDep = mardiCreneau1HeureDep;
}

public double getMardiCreneau1HeureArrivee() {
	return mardiCreneau1HeureArrivee;
}

public void setMardiCreneau1HeureArrivee(double mardiCreneau1HeureArrivee) {
	this.mardiCreneau1HeureArrivee = mardiCreneau1HeureArrivee;
}

public double getMardiCreneau2HeureDep() {
	return mardiCreneau2HeureDep;
}

public void setMardiCreneau2HeureDep(double mardiCreneau2HeureDep) {
	this.mardiCreneau2HeureDep = mardiCreneau2HeureDep;
}

public double getMardiCreneau2HeureArrivee() {
	return mardiCreneau2HeureArrivee;
}

public void setMardiCreneau2HeureArrivee(double mardiCreneau2HeureArrivee) {
	this.mardiCreneau2HeureArrivee = mardiCreneau2HeureArrivee;
}

public double getMardiCreneau3HeureDep() {
	return mardiCreneau3HeureDep;
}

public void setMardiCreneau3HeureDep(double mardiCreneau3HeureDep) {
	this.mardiCreneau3HeureDep = mardiCreneau3HeureDep;
}

public double getMardiCreneau3HeureArrivee() {
	return mardiCreneau3HeureArrivee;
}

public void setMardiCreneau3HeureArrivee(double mardiCreneau3HeureArrivee) {
	this.mardiCreneau3HeureArrivee = mardiCreneau3HeureArrivee;
}

public double getMercrediCreneau1HeureDep() {
	return mercrediCreneau1HeureDep;
}

public void setMercrediCreneau1HeureDep(double mercrediCreneau1HeureDep) {
	this.mercrediCreneau1HeureDep = mercrediCreneau1HeureDep;
}

public double getMercrediCreneau1HeureArrivee() {
	return mercrediCreneau1HeureArrivee;
}

public void setMercrediCreneau1HeureArrivee(double mercrediCreneau1HeureArrivee) {
	this.mercrediCreneau1HeureArrivee = mercrediCreneau1HeureArrivee;
}

public double getMercrediCreneau2HeureDep() {
	return mercrediCreneau2HeureDep;
}

public void setMercrediCreneau2HeureDep(double mercrediCreneau2HeureDep) {
	this.mercrediCreneau2HeureDep = mercrediCreneau2HeureDep;
}

public double getMercrediCreneau2HeureArrivee() {
	return mercrediCreneau2HeureArrivee;
}

public void setMercrediCreneau2HeureArrivee(double mercrediCreneau2HeureArrivee) {
	this.mercrediCreneau2HeureArrivee = mercrediCreneau2HeureArrivee;
}

public double getMercrediCreneau3HeureDep() {
	return mercrediCreneau3HeureDep;
}

public void setMercrediCreneau3HeureDep(double mercrediCreneau3HeureDep) {
	this.mercrediCreneau3HeureDep = mercrediCreneau3HeureDep;
}

public double getMercrediCreneau3HeureArrivee() {
	return mercrediCreneau3HeureArrivee;
}

public void setMercrediCreneau3HeureArrivee(double mercrediCreneau3HeureArrivee) {
	this.mercrediCreneau3HeureArrivee = mercrediCreneau3HeureArrivee;
}

public double getJeudiCreneau1HeureDep() {
	return jeudiCreneau1HeureDep;
}

public void setJeudiCreneau1HeureDep(double jeudiCreneau1HeureDep) {
	this.jeudiCreneau1HeureDep = jeudiCreneau1HeureDep;
}

public double getJeudiCreneau1HeureArrivee() {
	return jeudiCreneau1HeureArrivee;
}

public void setJeudiCreneau1HeureArrivee(double jeudiCreneau1HeureArrivee) {
	this.jeudiCreneau1HeureArrivee = jeudiCreneau1HeureArrivee;
}

public double getJeudiCreneau2HeureDep() {
	return jeudiCreneau2HeureDep;
}

public void setJeudiCreneau2HeureDep(double jeudiCreneau2HeureDep) {
	this.jeudiCreneau2HeureDep = jeudiCreneau2HeureDep;
}

public double getJeudiCreneau2HeureArrivee() {
	return jeudiCreneau2HeureArrivee;
}

public void setJeudiCreneau2HeureArrivee(double jeudiCreneau2HeureArrivee) {
	this.jeudiCreneau2HeureArrivee = jeudiCreneau2HeureArrivee;
}

public double getJeudiCreneau3HeureDep() {
	return jeudiCreneau3HeureDep;
}

public void setJeudiCreneau3HeureDep(double jeudiCreneau3HeureDep) {
	this.jeudiCreneau3HeureDep = jeudiCreneau3HeureDep;
}

public double getJeudiCreneau3HeureArrivee() {
	return jeudiCreneau3HeureArrivee;
}

public void setJeudiCreneau3HeureArrivee(double jeudiCreneau3HeureArrivee) {
	this.jeudiCreneau3HeureArrivee = jeudiCreneau3HeureArrivee;
}

public double getVendrediCreneau1HeureDep() {
	return vendrediCreneau1HeureDep;
}

public void setVendrediCreneau1HeureDep(double vendrediCreneau1HeureDep) {
	this.vendrediCreneau1HeureDep = vendrediCreneau1HeureDep;
}

public double getVendrediCreneau1HeureArrivee() {
	return vendrediCreneau1HeureArrivee;
}

public void setVendrediCreneau1HeureArrivee(double vendrediCreneau1HeureArrivee) {
	this.vendrediCreneau1HeureArrivee = vendrediCreneau1HeureArrivee;
}

public double getVendrediCreneau2HeureDep() {
	return vendrediCreneau2HeureDep;
}

public void setVendrediCreneau2HeureDep(double vendrediCreneau2HeureDep) {
	this.vendrediCreneau2HeureDep = vendrediCreneau2HeureDep;
}

public double getVendrediCreneau2HeureArrivee() {
	return vendrediCreneau2HeureArrivee;
}

public void setVendrediCreneau2HeureArrivee(double vendrediCreneau2HeureArrivee) {
	this.vendrediCreneau2HeureArrivee = vendrediCreneau2HeureArrivee;
}

public double getVendrediCreneau3HeureDep() {
	return vendrediCreneau3HeureDep;
}

public void setVendrediCreneau3HeureDep(double vendrediCreneau3HeureDep) {
	this.vendrediCreneau3HeureDep = vendrediCreneau3HeureDep;
}

public double getVendrediCreneau3HeureArrivee() {
	return vendrediCreneau3HeureArrivee;
}

public void setVendrediCreneau3HeureArrivee(double vendrediCreneau3HeureArrivee) {
	this.vendrediCreneau3HeureArrivee = vendrediCreneau3HeureArrivee;
}

public double getSamediCreneau1HeureDep() {
	return samediCreneau1HeureDep;
}

public void setSamediCreneau1HeureDep(double samediCreneau1HeureDep) {
	this.samediCreneau1HeureDep = samediCreneau1HeureDep;
}

public double getSamediCreneau1HeureArrivee() {
	return samediCreneau1HeureArrivee;
}

public void setSamediCreneau1HeureArrivee(double samediCreneau1HeureArrivee) {
	this.samediCreneau1HeureArrivee = samediCreneau1HeureArrivee;
}

public double getSamediCreneau2HeureDep() {
	return samediCreneau2HeureDep;
}

public void setSamediCreneau2HeureDep(double samediCreneau2HeureDep) {
	this.samediCreneau2HeureDep = samediCreneau2HeureDep;
}

public double getSamediCreneau2HeureArrivee() {
	return samediCreneau2HeureArrivee;
}

public void setSamediCreneau2HeureArrivee(double samediCreneau2HeureArrivee) {
	this.samediCreneau2HeureArrivee = samediCreneau2HeureArrivee;
}

public double getSamediCreneau3HeureDep() {
	return samediCreneau3HeureDep;
}

public void setSamediCreneau3HeureDep(double samediCreneau3HeureDep) {
	this.samediCreneau3HeureDep = samediCreneau3HeureDep;
}

public double getSamediCreneau3HeureArrivee() {
	return samediCreneau3HeureArrivee;
}

public void setSamediCreneau3HeureArrivee(double samediCreneau3HeureArrivee) {
	this.samediCreneau3HeureArrivee = samediCreneau3HeureArrivee;
}

public double getDimancheCreneau1HeureDep() {
	return dimancheCreneau1HeureDep;
}

public void setDimancheCreneau1HeureDep(double dimancheCreneau1HeureDep) {
	this.dimancheCreneau1HeureDep = dimancheCreneau1HeureDep;
}

public double getDimancheCreneau1HeureArrivee() {
	return dimancheCreneau1HeureArrivee;
}

public void setDimancheCreneau1HeureArrivee(double dimancheCreneau1HeureArrivee) {
	this.dimancheCreneau1HeureArrivee = dimancheCreneau1HeureArrivee;
}

public double getDimancheCreneau2HeureDep() {
	return dimancheCreneau2HeureDep;
}

public void setDimancheCreneau2HeureDep(double dimancheCreneau2HeureDep) {
	this.dimancheCreneau2HeureDep = dimancheCreneau2HeureDep;
}

public double getDimancheCreneau2HeureArrivee() {
	return dimancheCreneau2HeureArrivee;
}

public void setDimancheCreneau2HeureArrivee(double dimancheCreneau2HeureArrivee) {
	this.dimancheCreneau2HeureArrivee = dimancheCreneau2HeureArrivee;
}

public double getDimancheCreneau3HeureDep() {
	return dimancheCreneau3HeureDep;
}

public void setDimancheCreneau3HeureDep(double dimancheCreneau3HeureDep) {
	this.dimancheCreneau3HeureDep = dimancheCreneau3HeureDep;
}

public double getDimancheCreneau3HeureArrivee() {
	return dimancheCreneau3HeureArrivee;
}

public void setDimancheCreneau3HeureArrivee(double dimancheCreneau3HeureArrivee) {
	this.dimancheCreneau3HeureArrivee = dimancheCreneau3HeureArrivee;
}

public double getFerieCreneau1HeureDep() {
	return ferieCreneau1HeureDep;
}

public void setFerieCreneau1HeureDep(double ferieCreneau1HeureDep) {
	this.ferieCreneau1HeureDep = ferieCreneau1HeureDep;
}

public double getFerieCreneau1HeureArrivee() {
	return ferieCreneau1HeureArrivee;
}

public void setFerieCreneau1HeureArrivee(double ferieCreneau1HeureArrivee) {
	this.ferieCreneau1HeureArrivee = ferieCreneau1HeureArrivee;
}

public double getFerieCreneau2HeureDep() {
	return ferieCreneau2HeureDep;
}

public void setFerieCreneau2HeureDep(double ferieCreneau2HeureDep) {
	this.ferieCreneau2HeureDep = ferieCreneau2HeureDep;
}

public double getFerieCreneau2HeureArrivee() {
	return ferieCreneau2HeureArrivee;
}

public void setFerieCreneau2HeureArrivee(double ferieCreneau2HeureArrivee) {
	this.ferieCreneau2HeureArrivee = ferieCreneau2HeureArrivee;
}

public double getFerieCreneau3HeureDep() {
	return ferieCreneau3HeureDep;
}

public void setFerieCreneau3HeureDep(double ferieCreneau3HeureDep) {
	this.ferieCreneau3HeureDep = ferieCreneau3HeureDep;
}

public double getFerieCreneau3HeureArrivee() {
	return ferieCreneau3HeureArrivee;
}

public void setFerieCreneau3HeureArrivee(double ferieCreneau3HeureArrivee) {
	this.ferieCreneau3HeureArrivee = ferieCreneau3HeureArrivee;
}

public static String format(double val) {
	  DecimalFormat df = new DecimalFormat("00.00"); 
	  String data = df.format(val);
	  data = data.replace(".",":").replace(",", ":");
	  return data;
}

}
