package org.fudaa.fudaa.sipor.structures;

/**
 * Constantes relatives � SIPOR.
 * 
 * @author Adrien Hadoux
 *
 */
public class SiporConstantes {

  
  

  public static String titreComboCercles="cercle:";
  
  //-- param des genes: 2011 --//
  public static String ConstanteTitreRapportHtm="G�nes dans les cercles d'�vitage";
  public static String ConstanteSectionRapportHtm="G�nes dans le cercle";
  public static String ConstanteTitreRapportSwing="ins�rer les durees de g�nes cercle";
  public static String ConstanteTitreIHMgenesSwing="R�gles g�nes entre navires du cercle d'�vitage ";
  public static String constanteValidationGeneCercle="Regles de g�nes des cercles correctement saisies.\n Attention! un ajout de cat�gorie de navire implique qu'\n une ligne et une colonne suppl�mentaire � saisir \napparaitrons ici.\n(par d�faut les valeurs de cette ligne et colonne\n suppl�mentaire sont � oui)";
  public static String constanteCatNavireNom="Cat�gories de navires";
  public static String constanteEVENTgenes="REGLESGENESCERCLE";
  public static final String ConstanteMenuGene = "REGLESGENESGENERALES";
  
  //-- gestion messages erreurs --//
  public static String constanteErreurCoupleNavire="Erreur - la valeur du couple navire suivant doit �tre positive: ";
  public static String constanteErreurDigitsCoupleNavire="Erreur - un maximum de 2 chiffres apr�s la virgule sont attendus ";
  
  //-- tooltips --//
  public static String toolTipQuitter="cliquez sur ce bouton pour fermer la sous-fen�tre";
  public static String toolTipDupliquer="permet de dupliquer les informations";
  public static String toolTipImprimer="G�n�re un fichier excel du tableau. Attention, ce bouton ne g�n�re que le tableau du cercle affich�!";
  
  //-- I O errors --//
  public static String chargementErrorNouvelleStructure="Le fichier n'a pu �tre charg� correctement, \nil s'agit peut �tre d'un ancien fichier (sans informations de g�nes).\n Tentative r�cup�ration ancien projet...";
  
}
