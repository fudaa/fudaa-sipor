package org.fudaa.fudaa.sipor.structures;

import java.util.ArrayList;

public class SiporStrutureReglesDureesParcours {

  /**
   * definition du vecteur de vecteur ce vecteur est assimil� � une matrice dont les lignes sont les diff�rent
   * cheneaux(respectivement cercles) saisis et les colonnes sont assimil�es � des navires
   */
  ArrayList matriceDuree = new ArrayList();

  public SiporStrutureReglesDureesParcours() {

  }

  /**
   * methjode d'ajout d'un ligne
   */
  public void ajoutLigne(final int _numNavires) {

    final ArrayList vecteurChenal = new ArrayList();

    for (int k = 0; k < _numNavires; k++) {
      vecteurChenal.add(new Double(0));
    }

    this.matriceDuree.add(vecteurChenal);

  }

  /**
   * Methode de suppression d une ligne
   * 
   * @param _numChenal
   */
  public void SuprimerLigne(final int _numChenal) {
    this.matriceDuree.remove(_numChenal);
  }

  /**
   * Methode d'ajout d'une colonne ie ajout d un navire
   */

  public void ajoutNavire() {
    for (int k = 0; k < this.matriceDuree.size(); k++) {
      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
      vecteurLigne.add(new Double(0));
    }

  }

  /**
   * Methode de suppression d un navire
   * 
   * @param _numNavire
   */
  public  void suppressionNavire(final int _numNavire) {
    for (int k = 0; k < this.matriceDuree.size(); k++) {
      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
      vecteurLigne.remove(_numNavire);
    }

  }

  /**
   * Methode de recuperation de al valeur (i j) de la matrice
   * 
   * @param ligne indice de la ligne
   * @param col indice de la colonne
   * @return la valeur associ�e a la case i j de la matrice
   */
  public  double retournerValeur(final int ligne, final int col) {
    double val = 0;
    final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(ligne);
    val = ((Double) vecteurLigne.get(col)).doubleValue();

    return val;
  }

  /**
   * Methode d insertion d un objet de type Double dans la matrice
   * 
   * @param val valeure a inserer
   * @param ligne indice de ligne
   * @param col indice de colonne
   */
  public  void modifierValeur(final double val, final int ligne, final int col) {
    final Double nouveau = new Double(val);

    final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(ligne);
    vecteurLigne.set(col, nouveau);

  }

public ArrayList getMatriceDuree() {
	return matriceDuree;
}

public void setMatriceDuree(ArrayList matriceDuree) {
	this.matriceDuree = matriceDuree;
}

}
