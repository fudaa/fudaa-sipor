
/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sipor.SParametresSipor2;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmesComparaisonSimulation;
import org.fudaa.fudaa.sipor.factory.SiporResource;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des comparaisons des resultats de la generation des bateaux
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatComparaisonAttentetrajet extends SiporInternalFrame  {

  
  private static final long serialVersionUID = 1L;

/**
   * liste des projets pour la comparaison de simulation: limit� a 10 simulations
   */
  FudaaProjet[] listeProjet_ = new FudaaProjet[10];

  /**
   * liste des r�sultats de simulation des simulations
   */
  SParametresSipor2[] listeParamsSimu_ = new SParametresSipor2[10];

  /**
   * nombre de simulations comparables sur les 10
   */
  int nombreSimulationsComparees_ = 0;

  /**
   * tableau sopecifiant selon els criteres de comparaison si chacune des simulatiopns sont comparables et indique
   * l'indice du navire a comparer
   */
  int[] comparePossible_;

  int[] comparePossible2_;
  /**
   * tableau de checkBox destiner a l utilisateuir pour choisr les navire a visualiser sur les diff�rents supports.
   */
  JCheckBox[] tableauChoixSimulations_;

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Nombre de navires" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  String[] listeElt = { "Chenal", "Cercle", "Ecluse", "Quai" };
  JComboBox ListetypeElem1_ = new JComboBox(listeElt);
  JComboBox ListetypeElem2_ = new JComboBox(listeElt);
  
  JComboBox ListeElement_ = new JComboBox();
  JComboBox ListeElement2_ = new JComboBox();
  String[] listeSens = { "Entrant", "Sortant","Les 2 sens" };
  JComboBox sens_ = new JComboBox(listeSens);
  
  JComboBox ListeNavires_ = new JComboBox();

  String[] listeaction = { "Attente mar�e", "Attente s�curit�", "Attente acc�s", "Attente occupation","Attente indisponibilit�","Attente totale" };
  /**
   * liste des actions a realiser
   */
  JComboBox ListeActions_ = new JComboBox(listeaction);

  /**
   * bouton de generation des resultats
   */

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatComparaisonAttentetrajet(final SiporDataSimulation _donnees) {
    super("Comparaison attentes trajet", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      this.ListeElement_.addItem("" + donnees_.getlQuais_().retournerQuais(i).getNom());
    }

    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    /**
     * ajout des simulation valides a comparer et chargement de leur donn�es:
     */
    tableauChoixSimulations_ = new JCheckBox[10];
    int nbProjets = 0;
    for (int i = 0; i < this.donnees_.getApplication().getListe_().getModel_().size(); i++) {
      // A) creation du projet
      this.listeProjet_[nbProjets] = new FudaaProjet(this.donnees_.getApplication().getApp(), new FudaaFiltreFichier(
          "sipor"));
      // B) ouverture du projet
      this.listeProjet_[nbProjets].ouvrir(this.donnees_.getApplication().getListe_().getModel_().get(i).toString());

      // C) verification des donn�es

      if (((SParametresSipor2) this.listeProjet_[nbProjets].getParam(SiporResource.parametres)).ResultatsCompletsSimulation == null)// on
                                                                                                                                    // ne
                                                                                                                                    // peut
                                                                                                                                    // pas
                                                                                                                                    // comparer
                                                                                                                                    // cette
                                                                                                                                    // simulation
      {

        new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "La simulation "
            + this.donnees_.getApplication().getListe_().getModel_().get(i).toString() + " n'est pas valide"
            + "\n car elle n'a pas encore �t� lanc�e." +
            "\nLancer les calculs de cette simulation avant " + "\n de pouvoir la comparer").activate();
      } else if (((SParametresSipor2) this.listeProjet_[nbProjets].getParam(SiporResource.parametres)).ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens1
 == null && ((SParametresSipor2) this.listeProjet_[nbProjets].getParam(SiporResource.parametres)).ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens2
 == null && ((SParametresSipor2) this.listeProjet_[nbProjets].getParam(SiporResource.parametres)).ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesLes2Sens
 == null)// on
                                                                                                                                                                    // ne
                                                                                                                                                                    // peut
                                                                                                                                                                    // pas
                                                                                                                                                                    // comparer
                                                                                                                                                                    // cette
                                                                                                                                                                    // simulation
      {
        new BuDialogMessage(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "La simulation "
            + this.donnees_.getApplication().getListe_().getModel_().get(i).toString() + " n'est pas valide"
            + "\n car elle n'a pas encore �t� lanc�e." +
            "\nLancer les calculs de cette simulation avant " + "\n de pouvoir la comparer").activate();
      } else {
        this.listeParamsSimu_[nbProjets] = (SParametresSipor2) this.listeProjet_[nbProjets]
            .getParam(SiporResource.parametres);

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[nbProjets].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[nbProjets].getFichier().lastIndexOf("/") + 1;
        }

        this.tableauChoixSimulations_[nbProjets] = new JCheckBox(this.listeProjet_[nbProjets].getFichier().substring(
            debut, this.listeProjet_[nbProjets].getFichier().lastIndexOf(".sipor")), true);
        this.tableauChoixSimulations_[nbProjets].addActionListener(this);
        nombreSimulationsComparees_++;
        nbProjets++;
      }

    }

    // liste des comparaisons possibles
    comparePossible_ = VerificationComparaisonSimulationsPossibleEntreElements();

    comparePossible2_ = VerificationComparaisonSimulationsPossibleEntreNavires();

    setSize(800, 500);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    this.getContentPane().add(this.optionPanel_, BorderLayout.WEST);
    // ajout du tableau dans le panel tabbed

    // ajout des courbes dans le panel de la sous fenetre

    panelPrincipal_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);
    panelPrincipal_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    /*******************************************************************************************************************
     * gestion du panel du haut
     ******************************************************************************************************************/

    // panel qui contient les differents boutons
    this.controlPanel_.setLayout(new GridLayout(2,1));
    BuPanel panel1=new BuPanel(new FlowLayout(FlowLayout.LEFT));
    BuPanel panel2=new BuPanel(new FlowLayout(FlowLayout.LEFT));
    panel1.add(new JLabel("El�ment d�part:"));
    panel1.add(this.ListetypeElem1_);
    panel1.add(this.ListeElement_);
    panel1.add(new JLabel("El�ment arriv�e:"));
    panel1.add(this.ListetypeElem2_);
    panel1.add(this.ListeElement2_);
    panel1.add(new JLabel("Sens:"));
    panel1.add(this.sens_);
    panel2.add(new JLabel("Cat�gorie:"));
    panel2.add(this.ListeNavires_);

    panel2.add(new JLabel("Comparaison sur:"));
    panel2.add(this.ListeActions_);
    panel2.add(validation_);
    this.controlPanel_.add(panel1);
    this.controlPanel_.add(panel2);
    
    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Panneau de contr�le");
    this.controlPanel_.setBorder(bordurea);

    this.getContentPane().add(this.controlPanel_, BorderLayout.NORTH);

    this.ListeElement_.addActionListener(this);
    this.ListeNavires_.addActionListener(this);

    final ActionListener RemplissageElement = new ActionListener() {
        public void actionPerformed(ActionEvent e) {

          int selection = 0;
          if (e.getSource() == ListetypeElem1_) {
            selection = ListetypeElem1_.getSelectedIndex();
          } else {
            selection = ListetypeElem2_.getSelectedIndex();
          }
          // "quai","ecluse","chenal","cercle","bassin"
          switch (selection) {

          case 3:
            if (e.getSource() == ListetypeElem1_) {
              ListeElement_.removeAllItems();
              for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
                ListeElement_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
              }
              ListeElement_.validate();
            } else if (e.getSource() == ListetypeElem2_) {
              ListeElement2_.removeAllItems();
              for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
                ListeElement2_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
              }
              ListeElement2_.validate();
            }
            break;
          case 2:
            if (e.getSource() == ListetypeElem1_) {
              ListeElement_.removeAllItems();
              for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
                ListeElement_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
              }
              ListeElement_.validate();
            } else if (e.getSource() == ListetypeElem2_) {
              ListeElement2_.removeAllItems();
              for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
                ListeElement2_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
              }
              ListeElement2_.validate();
            }
            break;
          case 0:
            if (e.getSource() == ListetypeElem1_) {
              ListeElement_.removeAllItems();
              for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
                ListeElement_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
              }
              ListeElement_.validate();
            } else if (e.getSource() == ListetypeElem2_) {
              ListeElement2_.removeAllItems();
              for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
                ListeElement2_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
              }
              ListeElement2_.validate();
            }
            break;
          case 1:
            if (e.getSource() == ListetypeElem1_) {
              ListeElement_.removeAllItems();
              for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
                ListeElement_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
              }
              ListeElement_.validate();
            } else if (e.getSource() == ListetypeElem2_) {
              ListeElement2_.removeAllItems();
              for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
                ListeElement2_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
              }
              ListeElement2_.validate();
            }
            break;
          }

        
        }
      };
         
      this.ListetypeElem1_.addActionListener(RemplissageElement);
      this.ListetypeElem1_.setSelectedIndex(0);
    
      this.ListetypeElem2_.addActionListener(RemplissageElement);
      this.ListetypeElem2_.setSelectedIndex(3);
      
      this.ListeElement_.addActionListener(RemplissageElement);
      this.ListeElement2_.addActionListener(RemplissageElement);
      this.ListeActions_.addActionListener(this);
      validation_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/


    final Box bVert2 = Box.createVerticalBox();
    for (int i = 0; i < nombreSimulationsComparees_; i++) {
      bVert2.add(this.tableauChoixSimulations_[i]);
    }
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "simu");
    bVert2.setBorder(bordure1);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    this.optionPanel_.add(pcnasc);
    this.optionPanel_.setBorder(this.compound_);

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
   // System.out.println("Graphe: \n"+descriptionGraphe);
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatComparaisonAttentetrajet.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatComparaisonAttentetrajet.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    String g = "";

    // determiner el nombre de cat�gories de navires selectionn�s

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceElement = 0;

    g += "graphe\n{\n";
    g += "  titre \" Comparaison de simulation \"\n";
    g += "  sous-titre \"" + (String) this.ListeActions_.getSelectedItem() + "Trajet entre: Element: "
        + (String) this.ListetypeElem1_.getSelectedItem()+" "+ (String) this.ListeElement_.getSelectedItem() +" et "+ (String) this.ListetypeElem2_.getSelectedItem()+" "+ (String) this.ListeElement2_.getSelectedItem() + ", cat�gorie: " + (String) this.ListeNavires_.getSelectedItem()
        + " \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" simu \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (this.nombreSimulationsComparees_ + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
        + "\n";
    System.out.print("nb comparaisons: " + this.nombreSimulationsComparees_);

    g += "  }\n";

  
      g += "  axe\n  {\n"; // Ordonn�es
      g += "    titre \" " + "\"\n";
      g += "    unite \"" + " H.MIN" + "\"\n";
      g += "    orientation " + "vertical" + "\n";
      g += "    graduations oui\n";
      g += "    minimum " + 0 + "\n";
      g += "    maximum "
          + SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes(SiporAlgorithmesComparaisonSimulation.determinerMaxAttenteTrajet(this.listeParamsSimu_,
              this.nombreSimulationsComparees_)) + "\n";
    //}
      g += "  }\n";

    
    
    
    //----------------
    //-  Courbe attentes maxi
    //---------------  
    g += "  courbe\n  {\n";
    g += "    titre \"";
    
      g += "" + (String) this.ListeActions_.getSelectedItem()+" maxi";
   

    g += "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 0 \n";
    g += "surface.couleur BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BB0000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceElement = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
          && this.comparePossible2_[n] != -1) {
        g += (indiceElement + 1)// numero de la cat�gorie
            + " ";
        if (this.ListeActions_.getSelectedIndex() == 0) {
          g += SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMareeMaxi);
        } else  if (this.ListeActions_.getSelectedIndex() == 1) {
          g += SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteSecuMaxi);
        } else  if (this.ListeActions_.getSelectedIndex() == 2) {
          g += SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteAccesMaxi);
        } else  if (this.ListeActions_.getSelectedIndex() == 3) {
          g += SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteOccupMaxi);
        } else  if (this.ListeActions_.getSelectedIndex() == 4) {
          g += SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attentePanneMaxi);
        } else  if (this.ListeActions_.getSelectedIndex() == 5) {
          g += SiporTraduitHoraires
          .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteTotaleMaxi);
        }

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("/") + 1;
        }

        g += "\n etiquette  \n \""
            + this.listeProjet_[n].getFichier().substring(debut,
                this.listeProjet_[n].getFichier().lastIndexOf(".sipor")) + "\" \n" + "\n";
        indiceElement++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

  
    
    //----------------
    //-  Courbe attentes moyennes
    //---------------  
   
    g += "  courbe\n  {\n";
    g += "    titre \"";

    g += "" + (String) this.ListeActions_.getSelectedItem()+" moyen";

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 0 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB8800 \n";

      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceElement = 0;
      for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
        if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
            && this.comparePossible2_[n] != -1) {
          
          g += (indiceElement + 1)// numero de la cat�gorie
              + " ";

           if (this.ListeActions_.getSelectedIndex() == 0) {
                 g += SiporTraduitHoraires
                 .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMareeTotale
                 /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteMaree));
               } else  if (this.ListeActions_.getSelectedIndex() == 1) {
                   g += SiporTraduitHoraires
                   .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteSecuTotale
                           /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteSecu));
                }else
                  if (this.ListeActions_.getSelectedIndex() == 2) {
                    g += SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteAccesTotale
                            /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteAcces));
                 }else
                   if (this.ListeActions_.getSelectedIndex() == 3) {
                         g += SiporTraduitHoraires
                         .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteOccupTotale
                                 /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAtenteOccup));
                      }else
                        if (this.ListeActions_.getSelectedIndex() == 4) {
                              g += SiporTraduitHoraires
                              .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attentePanneTotale
                                      /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttentePanne));
                           }else
                             if (this.ListeActions_.getSelectedIndex() == 5) {
                                   g += SiporTraduitHoraires
                                   .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMegaTotale
                                           /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteTotale));
                                }
                  
          

          g += "\n";
          indiceElement++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

      
      
      
      //----------------
      //-  Courbe attentes mini
      //---------------  

      g += "  courbe\n  {\n";
      g += "    titre \"";

      g += "" + (String) this.ListeActions_.getSelectedItem()+" mini";

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 0 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BBCC00 \n";

      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceElement = 0;
      for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
        if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
            && this.comparePossible2_[n] != -1) {
          g += (indiceElement + 1)// numero de la cat�gorie
              + " ";

          if (this.ListeActions_.getSelectedIndex() == 0) {
              g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMareeMini);
            } else  if (this.ListeActions_.getSelectedIndex() == 1) {
              g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteSecuMini);
            } else  if (this.ListeActions_.getSelectedIndex() == 2) {
              g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteAccesMini);
            } else  if (this.ListeActions_.getSelectedIndex() == 3) {
              g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteOccupMini);
            } else  if (this.ListeActions_.getSelectedIndex() == 4) {
              g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attentePanneMini);
            } else  if (this.ListeActions_.getSelectedIndex() == 5) {
              g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteTotaleMini);
            }

          g += "\n";
          indiceElement++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";

      g += "  }\n";

    

    
    
    
    
    
    
    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    String g = "";

      // determiner el nombre de cat�gories de navires selectionn�s

      // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
      int indiceElement = 0;

      g += "graphe\n{\n";
      g += "  titre \" Comparaison de simulation \"\n";
      g += "  sous-titre \"" + (String) this.ListeActions_.getSelectedItem() + "Trajet entre: Element: "
        + (String) this.ListetypeElem1_.getSelectedItem()+" "+ (String) this.ListeElement_.getSelectedItem() +" et "+ (String) this.ListetypeElem2_.getSelectedItem()+" "+ (String) this.ListeElement2_.getSelectedItem() + ", cat�gorie: " + (String) this.ListeNavires_.getSelectedItem()
        + " \"\n";
      g += "  animation non\n";
      g += "  legende " + "oui" + "\n";

      g += "  axe\n  {\n"; // abscisses
      g += "    titre \" " + "\"\n";
      g += "    unite \" simu \"\n";
      g += "    orientation " + "horizontal" + "\n";
      g += "    graduations oui\n";
      g += "    minimum " + 0 + "\n";
      g += "    maximum " + (this.nombreSimulationsComparees_ + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
          + "\n";
      System.out.print("nb comparaisons: " + this.nombreSimulationsComparees_);

      g += "  }\n";

    
        g += "  axe\n  {\n"; // Ordonn�es
        g += "    titre \" " + "\"\n";
        g += "    unite \"" + " H.MIN" + "\"\n";
        g += "    orientation " + "vertical" + "\n";
        g += "    graduations oui\n";
        g += "    minimum " + 0 + "\n";
        g += "    maximum "
            + SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)SiporAlgorithmesComparaisonSimulation.determinerMaxAttenteTrajet(this.listeParamsSimu_,
                this.nombreSimulationsComparees_)) + "\n";
      //}
        g += "  }\n";

      
      
      
      //----------------
      //-  Courbe attentes maxi
      //---------------  
      g += "  courbe\n  {\n";
      g += "    titre \"";
      
        g += "" + (String) this.ListeActions_.getSelectedItem()+" maxi";
     

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";

      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceElement = 0;
      for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
        if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
            && this.comparePossible2_[n] != -1) {
          g += (indiceElement + 1)// numero de la cat�gorie
              + " ";
          if (this.ListeActions_.getSelectedIndex() == 0) {
            g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMareeMaxi);
          } else  if (this.ListeActions_.getSelectedIndex() == 1) {
            g +=SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float) this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteSecuMaxi);
          } else  if (this.ListeActions_.getSelectedIndex() == 2) {
            g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteAccesMaxi);
          } else  if (this.ListeActions_.getSelectedIndex() == 3) {
            g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteOccupMaxi);
          } else  if (this.ListeActions_.getSelectedIndex() == 4) {
            g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attentePanneMaxi);
          } else  if (this.ListeActions_.getSelectedIndex() == 5) {
            g += SiporTraduitHoraires
              .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteTotaleMaxi);
          }

          final String os = System.getProperty("os.name");
          int debut = 0;
          if (os.startsWith("Windows")) {
            debut = this.listeProjet_[n].getFichier().lastIndexOf("\\") + 1;
          } else {
            debut = this.listeProjet_[n].getFichier().lastIndexOf("/") + 1;
          }

          g += "\n etiquette  \n \""
              + this.listeProjet_[n].getFichier().substring(debut,
                  this.listeProjet_[n].getFichier().lastIndexOf(".sipor")) + "\" \n" + "\n";
          indiceElement++;
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";

      g += "  }\n";

    
      
      //----------------
      //-  Courbe attentes moyennes
      //---------------  
     
      g += "  courbe\n  {\n";
      g += "    titre \"";

      g += "" + (String) this.ListeActions_.getSelectedItem()+" moyen";

        g += "\"\n";
        g += "    type " + "histogramme" + "\n";
        g += "    aspect\n {\n";
        g += "contour.largeur 1 \n";
        g += "surface.couleur BB8800 \n";
        g += "texte.couleur 000000 \n";
        g += "contour.couleur 000000 \n";

        g += "    }\n";
        g += "    valeurs\n    {\n";

        indiceElement = 0;
        for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
          if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
              && this.comparePossible2_[n] != -1) {
            
            g += (indiceElement + 1)// numero de la cat�gorie
                + " ";

             if (this.ListeActions_.getSelectedIndex() == 0) {
                   g += SiporTraduitHoraires
                   .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMareeTotale
                   /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteMaree));
                 } else  if (this.ListeActions_.getSelectedIndex() == 1) {
                     g += SiporTraduitHoraires
                     .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteSecuTotale
                             /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteSecu));
                  }else
                    if (this.ListeActions_.getSelectedIndex() == 2) {
                      g +=SiporTraduitHoraires
                      .traduitMinutesEnHeuresMinutes((float) (this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteAccesTotale
                              /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteAcces));
                   }else
                     if (this.ListeActions_.getSelectedIndex() == 3) {
                           g += SiporTraduitHoraires
                           .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteOccupTotale
                                   /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAtenteOccup));
                        }else
                          if (this.ListeActions_.getSelectedIndex() == 4) {
                                g +=SiporTraduitHoraires
                                .traduitMinutesEnHeuresMinutes((float) (this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attentePanneTotale
                                        /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttentePanne));
                             }else
                               if (this.ListeActions_.getSelectedIndex() == 5) {
                                     g += SiporTraduitHoraires
                                     .traduitMinutesEnHeuresMinutes((float)(this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMegaTotale
                                             /this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].nbNaviresAttenteTotale));
                                  }
                    
            

            g += "\n";
            indiceElement++;
          }
        }// din du pour remplissage des coordonn�es
        g += "    }\n";
        g += "  }\n";

        
        
        
        //----------------
        //-  Courbe attentes mini
        //---------------  

        g += "  courbe\n  {\n";
        g += "    titre \"";

        g += "" + (String) this.ListeActions_.getSelectedItem()+" mini";

        g += "\"\n";
        g += "    type " + "histogramme" + "\n";
        g += "    aspect\n {\n";
        g += "contour.largeur 1 \n";
        g += "surface.couleur BBCC00 \n";
        g += "texte.couleur 000000 \n";
        g += "contour.couleur 000000 \n";

        g += "    }\n";
        g += "    valeurs\n    {\n";

        indiceElement = 0;
        for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
          if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1
              && this.comparePossible2_[n] != -1) {
            g += (indiceElement + 1)// numero de la cat�gorie
                + " ";

            if (this.ListeActions_.getSelectedIndex() == 0) {
                g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteMareeMini);
              } else  if (this.ListeActions_.getSelectedIndex() == 1) {
                g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteSecuMini);
              } else  if (this.ListeActions_.getSelectedIndex() == 2) {
                g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteAccesMini);
              } else  if (this.ListeActions_.getSelectedIndex() == 3) {
                g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteOccupMini);
              } else  if (this.ListeActions_.getSelectedIndex() == 4) {
                g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attentePanneMini);
              } else  if (this.ListeActions_.getSelectedIndex() == 5) {
                g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float)this.listeParamsSimu_[n].ResultatsCompletsSimulation.TOUTESAttenteTrajet[this.comparePossible_[n]].tableauAttenteCategories[this.comparePossible2_[n]].attenteTotaleMini);
              }

            g += "\n";
            indiceElement++;
          }
        }// din du pour remplissage des coordonn�es
        g += "    }\n";

        g += "  }\n";

      
    if (seuil_) {
        /**
         * declaration d'un seuil
         */
        g += " contrainte\n";
        g += "{\n";
        // a mettre le seuil
        g += "titre \"seuil \"\n";
        // str+="orientation horizontal \n";
        g += " type max\n";
        g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

        g += " \n }\n";
        // }//fin du for
      }

      return g;
  }

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    // si la source provient d un navire du tableau de checkBox
    if (source == validation_)//this.ListeElement_ || source == this.ListeActions_) 
    {
      if(ListeElement_.getSelectedIndex()==ListeElement2_.getSelectedIndex() && ListetypeElem1_.getSelectedIndex()==ListetypeElem2_.getSelectedIndex())
      {
        new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT, "Vous avez selectionn� 2 fois le m�me �l�ment pour le trajet, ce qui n'est pas correct.").activate();
        return;
      }
      // on v�rifie que la comparaison est possible
      this.comparePossible_ = VerificationComparaisonSimulationsPossibleEntreElements();
      this.comparePossible2_ = VerificationComparaisonSimulationsPossibleEntreNavires();

      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    }
    for (int i = 0; i < this.tableauChoixSimulations_.length; i++) {
      if (source == this.tableauChoixSimulations_[i]) {
        // on v�rifie que la comparaison est possible
        this.comparePossible_ = VerificationComparaisonSimulationsPossibleEntreElements();
        this.comparePossible2_ = VerificationComparaisonSimulationsPossibleEntreNavires();
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    if (source == this.valideSeuil_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui permet de v�rifier si les simulations sont comparables:
   * 
   * @return un tableau de booleen qui dit pour chaque simulation si elle est comparable ou non
   */
  public int[] VerificationComparaisonSimulationsPossibleEntreNavires() {
    final int[] comparePossible = new int[10];
    // on v�rifie que l'�l�ment navire a comparer existe bien dans l'ensemble des simulations
    final String nomNavire = (String) this.ListeNavires_.getSelectedItem();
    // etape 2: on v�rifie que le nom du navire existe bien dans chaque simu
    boolean trouve = false;
    for (int i = 0; i < this.nombreSimulationsComparees_; i++) {
      trouve = false;
      for (int k = 0; !trouve && k < this.listeParamsSimu_[i].navires.listeNavires.length; k++) {
        if ((this.listeParamsSimu_[i].navires.listeNavires[k].nom).equals(nomNavire)) {
          trouve = true;
          comparePossible[i] = k;
          
        }
      }
      if (trouve) {} else {
        comparePossible[i] = -1;
      }
    }

    return comparePossible;
  }

  public int[] VerificationComparaisonSimulationsPossibleEntreElements() {
    final int[] comparePossible = new int[10];
   
    
    final int type1 = this.ListetypeElem1_.getSelectedIndex();
    final int type2 = this.ListetypeElem2_.getSelectedIndex();
    final int elem1 = this.ListeElement_.getSelectedIndex();
    final int elem2 = this.ListeElement2_.getSelectedIndex();
    final int sensCirculation=this.sens_.getSelectedIndex();
    
    
    
    

    boolean trouve = false;
    for (int i = 0; i < this.nombreSimulationsComparees_; i++) {
      trouve = false;
      for (int k = 0; !trouve && k < this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet.length; k++) {
        if(this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k]!=null){
        if (this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].typeElement == type1
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].indiceElement == elem1
            &&this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].typeElement2 == type2
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].indiceElement2 == elem2
            && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].sens==sensCirculation
            ) {
          // le trajet voulu existe bien donc on le precise via le booleen et on stocke l'indice dans le tableau de comparaison
          trouve = true;
          comparePossible[i] = k;
        }
        else
          //�l�ments bon mais pas dans le meme ordre
          if(this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].typeElement == type2
        && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].indiceElement == elem2
        &&
        this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].typeElement2 == type1
        && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].indiceElement2 == elem1
        && this.listeParamsSimu_[i].ResultatsCompletsSimulation.TOUTESAttenteTrajet[k].sens==sensCirculation
        ) {
      // le trajet voulu existe bien donc on le precise via le booleen et on stocke l'indice dans le tableau de comparaison
      trouve = true;
      comparePossible[i] = k;
    }
      }
      }
      if (trouve == false) {
        comparePossible[i] = -1;
      }

    }
    


    return comparePossible;
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
