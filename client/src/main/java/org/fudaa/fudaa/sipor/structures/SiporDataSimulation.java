package org.fudaa.fudaa.sipor.structures;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.fu.FuLog;

import org.fudaa.dodico.corba.sipor.SParametresBassin2;
import org.fudaa.dodico.corba.sipor.SParametresBassins2;
import org.fudaa.dodico.corba.sipor.SParametresCercleGene;
import org.fudaa.dodico.corba.sipor.SParametresCerclesGenes;
import org.fudaa.dodico.corba.sipor.SParametresChenal2;
import org.fudaa.dodico.corba.sipor.SParametresCheneaux2;
import org.fudaa.dodico.corba.sipor.SParametresCoupleLoiDeterministe2;
import org.fudaa.dodico.corba.sipor.SParametresCoupleLoiJournaliere2;
import org.fudaa.dodico.corba.sipor.SParametresDonneesGenerales2;
import org.fudaa.dodico.corba.sipor.SParametresEcluse2;
import org.fudaa.dodico.corba.sipor.SParametresEcluses2;
import org.fudaa.dodico.corba.sipor.SParametresGare;
import org.fudaa.dodico.corba.sipor.SParametresGares;
import org.fudaa.dodico.corba.sipor.SParametresGrapheTopologie;
import org.fudaa.dodico.corba.sipor.SParametresGrapheTopologies;
import org.fudaa.dodico.corba.sipor.SParametresHoraires2;
import org.fudaa.dodico.corba.sipor.SParametresHorairesNavires2;
import org.fudaa.dodico.corba.sipor.SParametresMaree2;
import org.fudaa.dodico.corba.sipor.SParametresNavire2;
import org.fudaa.dodico.corba.sipor.SParametresNavires2;
import org.fudaa.dodico.corba.sipor.SParametresQuai2;
import org.fudaa.dodico.corba.sipor.SParametresQuais2;
import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sipor.SParametresSipor2;
import org.fudaa.dodico.corba.sipor.SParametresSiporGene;
import org.fudaa.dodico.corba.sipor.SiporResultatListeevenementsSimu;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.factory.SiporPreferences;
import org.fudaa.fudaa.sipor.factory.SiporResource;
import org.fudaa.fudaa.sipor.ui.modeles.SiporObservableSupport;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Classe fondamentale de l'application Sipor. Donnes globales de la simulation cette classe regroupe les donn�es
 * suivante: - reference de l'implementation Sipor - les classes des diff�rentes
 * donn�es(quai,ecluses,cheneaux,cercles,cat�gories de navires...) - la d�finition du projet Sipor - les structure
 * SParametres destin�es � la jonction Fortran/java - les m�thodes de sauvegardes / chargement des donn�es du projet -
 * les m�thodes de transfert classes donn�es => structures (et r�ciproquement)
 * 
 * @author Adrien Hadoux
 */

public class SiporDataSimulation {

  // definitions des attributs

  /**
   * Projet SIPOR.
   */
  protected FudaaProjet projet_;

  /**
   * Structure regroupant toutes les donn�es du projet Fudaa Sipor:IDL.
   */
  private SParametresSiporGene params_ = new SParametresSiporGene();

  /**
   * Structure des resultats de la simulation IDL.
   */
  SiporResultatListeevenementsSimu listeResultatsSimu_;

  /**
   * donn�es generales du port.
   */
  // fred : jamais utilise
//  SiporDonneesGenerales donneesGenerales_ = new SiporDonneesGenerales();

  /**
   * Tableau de catgories de Navires.
   */
  SiporNavires categoriesNavires_ = new SiporNavires();

  /**
   * Tableau des diffrents quais.
   */
  SiporListeQuais lQuais_ = new SiporListeQuais();


  /**
   * les donn�es des bassins.
   */
   SiporBassins listebassin_ = new SiporBassins();

  /**
   * les donn�es des gares.
   */
  SiporGares listeGare_ = new SiporGares();

  /**
   * Les donn�es des cercles.
   */
  SiporCercles listeCercle_ = new SiporCercles();

  /**
   * Les donn�es des cheneaux.
   */
  SiporChenaux listeChenal_ = new SiporChenaux();

  /**
   * Les donn�es des ecluses.
   */
   SiporEcluses listeEcluse_ = new SiporEcluses();

  /**
   * Structure de gestion des durees de passage.
   */
  SiporStrutureReglesDureesParcours reglesDureesParcoursChenal_ = new SiporStrutureReglesDureesParcours();
  SiporStrutureReglesDureesParcours reglesDureesParcoursCercle_ = new SiporStrutureReglesDureesParcours();
  SiporStrutureReglesRemplissageSAS reglesRemplissageSAS_ = new SiporStrutureReglesRemplissageSAS(); 
  /**
   * Liste d'horaires pour les categories de navires ces horaires ne sont pas enregistr� dans les sauvegardes, ils
   * servent juste a rendre la saisie plus rapide et ergonomique.
   */
  // FRED: JAMAIS UTILISE
//  ArrayList listeHorairesNavires_ = new ArrayList();

  /**
   * Interface parent SIPOR.
   */
  final SiporImplementation application_;

  /**
   * sous fenetre de verification des donn�es saisies.
   */
  SiporInternalFrame frameVerif_;

  
  
  /**
   * Structure GENARR pour lire les navires g�n�r�es
   * par l'executable GENARR. 
   */
  GenarrListeNavires genarr_=new GenarrListeNavires();
  
  
    /** DESIGN PATTERN Observer-Observable.
     * Observable du modele**/
  private static SiporObservableSupport observable=null;
  
  /** methode d acces a l observable * */
  public static SiporObservableSupport getObservable() {
    if (observable == null)
      observable = new SiporObservableSupport();
    return observable;
  }

  /** 
   * Fonction permettant d'enregistrer un Observer
   * qui sera pr�venu lors de la modification d'une
   * donn�es du mod�le.
   * 
   * @param ob Observer � enregistrer
   */
  public void addObservers(Observer ob) {
    getObservable().addObserver(ob);
  }
  
  /** 
   * Fonction permettant de sp�cifier aux Observer qu'une
   * partie des donn�es du mod�le ont �t� modifi�es
   * 
   * @param modelPart Partie du mod�le ayant �t� modifi�e.
   */
  public static void setProperty(Object modelPart) {
    // appel aux methodes de mise a jour de chaque panel mode expert
    getObservable().notifyObservers(modelPart);
  }
  
  
  
  public SiporDataSimulation(final SiporImplementation _application) {

    application_ = _application;

    // initialisation du projet a null
    this.projet_ = null;

    // initialisation du graphe de topologie
    this.getParams_().grapheTopologie = new SParametresGrapheTopologies();
    this.getParams_().grapheTopologie.nbArcs = 0;
    this.getParams_().grapheTopologie.graphe = new SParametresGrapheTopologie[2000];

    // initialisation des donn�es g�n�rales
    this.getParams_().donneesGenerales = new SParametresDonneesGenerales2();
    final java.util.Random random = new java.util.Random();
    int valal = random.nextInt();
    if (valal < 0) {
      valal = valal * -1;
    }
    this.getParams_().donneesGenerales.graine = valal;
    this.getParams_().donneesGenerales.jourDepart = 1;
    this.getParams_().donneesGenerales.nombreJours = 365;
    this.getParams_().donneesGenerales.nombreJoursFeries = 0;
    this.getParams_().donneesGenerales.piedDePilote = 10;

    //-- initialisation des donn�es des mar�es --//
    this.getParams_().maree = new SParametresMaree2();
    this.getParams_().maree.periodeMaree = 12.26;
    this.getParams_().maree.periodeViveEaux = 354;
    this.getParams_().maree.tableauDouzaines=new double[]{1,2,3,3,2,1,1,2,3,3,2,1};
    
    
    // initialisation des structures de r�sultats:
    this.getParams_().ResultatsCompletsSimulation = new SParametresResultatsCompletSimulation();

  }

  private final String projectName_ = "sipor";

  public void creer() {

    // Cr�ation d'un nouveau projet et tentative d'ouverture

    /**
     * creation d un nouveau projet pour datasimulation
     */
    this.projet_ = new FudaaProjet(this.getApplication().getApp(), new FudaaFiltreFichier(projectName_));

    /**
     * Initialisation du projet
     */
    this.projet_.creer();

    final FudaaProjet projet = this.projet_;

    if (!projet.estConfigure()) {
      projet.fermer(); // cr�ation de projet annul�e
    } else { // nouveau projet cr��
      String nomFichierVoulu = projet.getFichier();
      // ajoute l'extension .sipor si elle manque
      if (nomFichierVoulu.lastIndexOf(".sipor") != (nomFichierVoulu.length() - 6)) {
        projet.setFichier(projet.getFichier() + ".sipor");
        nomFichierVoulu = projet.getFichier();
      }
      if (this.getApplication().getProjets_().containsKey(nomFichierVoulu)) {
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
            "Fichier de meme nom d�j� ouvert").activate();
        projet.fermer();
        return;
      }

      // A: initialisation des donn�es
//      donneesGenerales_ = new SiporDonneesGenerales();
      categoriesNavires_ = new SiporNavires();
      lQuais_ = new SiporListeQuais();
//      listeHoraire_ = new SiporListeHoraires();
//      nbHoraires_ = 0;
      listebassin_ = new SiporBassins();
      listeGare_ = new SiporGares();
      listeCercle_ = new SiporCercles();
      listeChenal_ = new SiporChenaux();
      listeEcluse_ = new SiporEcluses();

      // Ajoute le projet qui vient d'etre ouvert et en fait le projet courant.
      this.getApplication().getListe_().ajouteSimulation(nomFichierVoulu, projet);
      this.getApplication().getListe_().setSelectedValue(nomFichierVoulu, true);

      this.getApplication().getOutils_().creationDonnees();

      this.getApplication().activerCommandesSimulation();
      this.getApplication().activeActionsExploitation();
      
      //-- on ouvre par d�faut le r�seau --//
      getApplication().getNetworkEditor().cleanAllNetwork();
      getApplication().activerNetworkEditor();

    }

  }

  public void enregistrer() {
    /**
     * 1) recopiage des donn�es de datasimulation dans le projet via les SParametres
     */
    setParametresProjet();

    /**
     * 2)ajout des parametre recopi� pr�c�demment
     */
    this.projet_.addParam(SiporResource.parametres, this.getParams_());

    /**
     * 4)enregistrement du projet
     */
    projet_.setEnrResultats(SiporPreferences.SIPOR.getBooleanProperty("enregistrerResultats"));
    // enregistrement des donn�es
    this.projet_.enregistre();
    
    //-- enregistrement du graphe --//
    if(getApplication().getNetworkEditor().hasChanged()) {
    	try {
			getApplication().getNetworkEditor().saveGraph(getProjet_().getFichier() + ".network");
		} catch (IOException e) {
			FuLog.error(e);
		}
    }
    

  }

  public  void enregistrerSous() {

    /**
     * 1) recopiage des donn�es de datasimulation dans le projet via les SParametres
     */
    setParametresProjet();

    /**
     * 2)ajout des parametre recopi� pr�c�demment
     */
    this.projet_.addParam(SiporResource.parametres, this.getParams_());

    /** 3) appel de la m�thode creer() */
    this.projet_.creer();

    /**
     * 4)enregistrement du projet
     */
    projet_.setEnrResultats(SiporPreferences.SIPOR.getBooleanProperty("enregistrerResultats"));
    // enregistrement des donn�es
    this.projet_.enregistre();

  //-- enregistrement du graphe --//
    if(getApplication().getNetworkEditor().hasChanged()) {
    	try {
			getApplication().getNetworkEditor().saveGraph(getProjet_().getFichier() + ".network");
		} catch (IOException e) {
			FuLog.error(e);
		}
    }
    
  }

  
  /**
   * AHX - chargement des structures SIPOR que l'on soit ancien syst�me ou nouveau syst�me.
   */
  public void chargerStructureSipor(){
    try{
        //-- new structure sipor --//
      this.setParams_( (SParametresSiporGene) this.projet_.getParam(SiporResource.parametres));
      
      }catch(Exception e){
        //-- old param sipor2--//
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
            SiporConstantes.chargementErrorNouvelleStructure).activate();
        System.out.println("Exception lors de l'ouverture du projet, peut-�tre est-ce un ancien format? tentative de r�cup�ration ancien format... ");
        SParametresSipor2 oldStructure=( (SParametresSipor2) this.projet_.getParam(SiporResource.parametres));
        
        if(oldStructure!=null){
           this.setParams_(FonctionsSimu.copieOldProjetIntoNewProjet(oldStructure));
        }
      }
  }
  
  
  /**
   * Methode d'ouverture d'un projet de type Fudaa Projet.
   */

  public  void ouvrir(String fichier) {

    // mise en m�moire de l'ancien projet avant ouverture d'un nouveau:
    // TRES UTILE DANS LA MESURE OU L OUVERTURE DONNE LIEU A UN ECHEC
    // ON PEUT AINSI RETABLIR L ANCIEN PROJET QUI LUI EST STABLE !!!
    String nomAncienProjet;
    if (projet_ != null) {
      nomAncienProjet = projet_.getFichier();
    } else {
      nomAncienProjet = null;
    }

    String repertoire=null;
    
    
    // Creation d'un nouveau projet et tentative d'ouverture
    repertoire = System.getProperty("user.dir") + File.separator + "exemples" + File.separator
        + projectName_;
    
    /**
     * 1)ouverture du projet Fudaa
     */
    if (repertoire.equals("null")
        || repertoire.equals("null.sipor")) {
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Interruption du chargement du fichier").activate();

      return;
    }

    // A) creation du projet
    try {
      projet_ = new FudaaProjet(this.getApplication().getApp(), new FudaaFiltreFichier(projectName_));
      // B) ouverture du projet
      
      if(fichier==null)
        //-- option ouvrir --//
        this.projet_.ouvrir();
      else
       //- option reouvrir --//
        this.projet_.ouvrir(fichier);
      
    } catch (final java.lang.ClassCastException c) {
      c.printStackTrace();
      // fred il y a erreur
      projet_ = null;
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Ce fichier ne contient pas de donn�es Sipor valides!").activate();

    }
   
    
    // fred donc si projet_ ==null on arrete
    if (projet_ == null) return;

    /**
     * 2) recuperation des donn�es dans le projet
     */

    
    
   chargerStructureSipor();
   
   
    if (this.getParams_() == null) {
      projet_ = null;
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Vous n'avez pas selectionn� de fichier valide!!\n\n Ouverture impossible").activate();
      // on reinitialise el projet a null pour eviter tout type de conflits
      // this.projet_=null;
      initParams();
      // Retour sur un precedent projet dans le cas ou la liste de projet est non vide
      if (!this.getApplication().getProjets_().isEmpty() && nomAncienProjet != null) {
        // on fais appel a la methode changerProjet qui
        // permet de relancer un projet existant non foireux
        // on relance l'ancien projet:
        changerProjet((FudaaProjet) this.getApplication().getProjets_().get(nomAncienProjet));

      }

      return;
    }

    /**
     * 3)recopiage des don�nes du projet dans DataSimulation
     */
    getParametresProjet();


    /**
     * 4) activation des commandes des fenetres de l application sipor
     */
    this.activerCommandesNiveau();

    /** methode qui permet l exploiattion des resultats */
    // this.getApplication_().activeActionsExploitation();
    /**
     * 5) verification de la configuration du projet
     */
    if (!projet_.estConfigure()) {
      projet_.fermer();
    } else {
      // 1)ouverture du projet r�ussie
      // 2)on v�rifie que le projet n est pas d�ja ouvert:
      final String nomFichierVoulu = this.projet_.getFichier();
      if (this.getApplication().getProjets_().containsKey(nomFichierVoulu)) {
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
            "Fichier d�j� ouvert").activate();
        this.projet_.fermer();
        return;
      }

      /**
       * 6) ajout dans la liste des projets
       */
      // Ajoute le projet qui vient d'etre ouvert et en fait le projet courant.
      this.getApplication().getListe_().ajouteSimulation(nomFichierVoulu, this.projet_);
      this.getApplication().getListe_().setSelectedValue(nomFichierVoulu, true);

      
      /** 7) ajout dans la liste des fichiers a reouvrir du ficheir ouvert **/
      final String r = projet_.getFichier();
      this.getApplication().getMainMenuBar().addRecentFile(r,  "");
      SiporPreferences.SIPOR.writeIniFile();
    
      //-- affiche le graphe au chargement --//
      getApplication().activerNetworkEditor();
      
    }

  }

  protected void initParams() {
    this.params_ = new SParametresSiporGene();
    // initialisation du graphe de topologie
    this.getParams_().grapheTopologie = new SParametresGrapheTopologies();
    this.getParams_().grapheTopologie.nbArcs = 0;
    this.getParams_().grapheTopologie.graphe = new SParametresGrapheTopologie[2000];
    // initialisation desdon�nes g�n�rales
    this.getParams_().donneesGenerales = new SParametresDonneesGenerales2();
    final java.util.Random random = new java.util.Random();
    int valal = random.nextInt();
    if (valal < 0) {
      valal = valal * -1;
    }
    this.getParams_().donneesGenerales.graine = valal;
    this.getParams_().donneesGenerales.jourDepart = 1;
    this.getParams_().donneesGenerales.nombreJours = 365;
    this.getParams_().donneesGenerales.nombreJoursFeries = 0;
    this.getParams_().donneesGenerales.piedDePilote = 10;
    // initialisation des donn�es des mar�es
    this.getParams_().maree = new SParametresMaree2();
    this.getParams_().maree.periodeMaree = 12.26;
    this.getParams_().maree.periodeViveEaux = 354;
    this.getParams_().maree.tableauDouzaines=new double[]{1,2,3,3,2,1,1,2,3,3,2,1};

    // initialisation des structures de r�sultats:
    this.getParams_().ResultatsCompletsSimulation = new SParametresResultatsCompletSimulation();
  }

  /**
   * methode appel�e lors du clic sur la liste des simulations de l'application.
   * 
   * @Param: nouveau projet projet correspondant a la ligne selectionnee dans la liste de selection des simulations
   */
  public  void changerProjet(final FudaaProjet _nouveauProjet) {

    // on commence par reinitialiser la totalit� des sous interfaces:
    this.getApplication().initialiserToutesLesInterfaces();

    try {
      projet_ = _nouveauProjet;
      // B) ouverture du projet
      // this.projet_.ouvrir();

    } catch (final java.lang.ClassCastException c) {

      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Ce fichier ne contient pas de donn�es Sipor valides!").activate();

    }

    /**
     * 2) recuperation des donn�es dans le projet
     */

    // B: recuperation des donnees du fichier de chargement:
    chargerStructureSipor();

    if (this.getParams_() == null) {
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Ce fichier ne contient pas de param�tres").activate();
      initParams();
      return;
    }

    /**
     * 3)recopie des don�nes du projet dans DataSimulation
     */
    getParametresProjet();

    new BuDialogMessage(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(), "Simulation: "
        + projet_.getFichier() + "\n\nParam�tres charg�s avec succ�s").activate();

  }

  /**
   * Methode permettant de dupliquer la simulation.
   */
  public  void dupliquerSimulation() {

    /**
     * creation d un nouveau projet pour datasimulation
     */
    this.projet_ = new FudaaProjet(this.getApplication().getApp(), new FudaaFiltreFichier(projectName_));

    /**
     * Initialisation du projet
     */
    this.projet_.creer();
    /**
     * Reinitialisation des parametres puisque mal charg�s
     */
    final SParametresSiporGene copieParams = new SParametresSiporGene();
    // initialisation du graphe de topologie
    copieParams.grapheTopologie = new SParametresGrapheTopologies();
    copieParams.grapheTopologie.nbArcs = 0;
    copieParams.grapheTopologie.graphe = new SParametresGrapheTopologie[2000];
    // initialisation desdon�nes g�n�rales
    copieParams.donneesGenerales = new SParametresDonneesGenerales2();
    copieParams.donneesGenerales.jourDepart = 1;
    copieParams.donneesGenerales.nombreJours = 365;
    copieParams.donneesGenerales.nombreJoursFeries = 0;
    copieParams.donneesGenerales.piedDePilote = 10;
    // initialisation des donn�es des mar�es
    copieParams.maree = new SParametresMaree2();
    copieParams.maree.periodeMaree = 12.26;
    copieParams.maree.periodeViveEaux = 354;
    this.getParams_().maree.tableauDouzaines=new double[]{1,2,3,3,2,1,1,2,3,3,2,1};

    /**
     * recopiage des donn�es des mar��s recopiage du graphe recopiage des donn�es g�n�rales
     */
    copieParams.donneesGenerales.jourDepart = this.getParams_().donneesGenerales.jourDepart;
    copieParams.donneesGenerales.nombreJours = params_.donneesGenerales.nombreJours;
    copieParams.donneesGenerales.nombreJoursFeries = params_.donneesGenerales.nombreJoursFeries;
    copieParams.donneesGenerales.piedDePilote = params_.donneesGenerales.piedDePilote;
    copieParams.donneesGenerales.tableauJoursFeries = new int[copieParams.donneesGenerales.nombreJoursFeries];
    for (int k = 0; k < copieParams.donneesGenerales.nombreJoursFeries; k++) {
      copieParams.donneesGenerales.tableauJoursFeries[k] = params_.donneesGenerales.tableauJoursFeries[k];
    }

    copieParams.maree.hauteurMoyenneMer = params_.maree.hauteurMoyenneMer;
    copieParams.maree.niveauBasseMerMorteEau = params_.maree.niveauBasseMerMorteEau;
    copieParams.maree.niveauBasseMerViveEau = params_.maree.niveauBasseMerViveEau;
    copieParams.maree.niveauHauteMerMorteEau = params_.maree.niveauHauteMerMorteEau;
    copieParams.maree.niveauHauteMerViveEau = params_.maree.niveauHauteMerViveEau;
    copieParams.maree.periodeMaree = params_.maree.periodeMaree;
    copieParams.maree.periodeViveEaux = params_.maree.periodeViveEaux;
    copieParams.maree.tableauDouzaines=new double[params_.maree.tableauDouzaines.length];
    for(int i=0;i<params_.maree.tableauDouzaines.length;i++)
      copieParams.maree.tableauDouzaines[i]=params_.maree.tableauDouzaines[i];
    
    
    copieParams.grapheTopologie.nbArcs = params_.grapheTopologie.nbArcs;
    copieParams.grapheTopologie.nbGaresDessinnees = params_.grapheTopologie.nbGaresDessinnees;
    for (int k = 0; k < copieParams.grapheTopologie.nbArcs; k++) {
      copieParams.grapheTopologie.graphe[k] = new SParametresGrapheTopologie();
      copieParams.grapheTopologie.graphe[k].nomConnection = params_.grapheTopologie.graphe[k].nomConnection;
      copieParams.grapheTopologie.graphe[k].numGare1 = params_.grapheTopologie.graphe[k].numGare1;
      copieParams.grapheTopologie.graphe[k].numGare2 = params_.grapheTopologie.graphe[k].numGare2;
      copieParams.grapheTopologie.graphe[k].typeConnection = params_.grapheTopologie.graphe[k].typeConnection;
      copieParams.grapheTopologie.graphe[k].xGare1 = params_.grapheTopologie.graphe[k].xGare1;
      copieParams.grapheTopologie.graphe[k].xGare2 = params_.grapheTopologie.graphe[k].xGare2;
      copieParams.grapheTopologie.graphe[k].yGare1 = params_.grapheTopologie.graphe[k].yGare1;
      copieParams.grapheTopologie.graphe[k].yGare2 = params_.grapheTopologie.graphe[k].yGare2;

    }

    // les parametres recopies deviennent les parametres de base qui vont recevoir el contenu des vecteurs
    // dynamiques dans la methode enregistrer()
    this.params_ = copieParams;
    // 2) sauvegarde du projet dupliqu�
    this.enregistrer();

    final String nomSimu = this.projet_.getFichier();
    // 3) ajout dans la liste des imulation du projet dupliqu�:
    this.getApplication().getListe_().ajouteSimulation(nomSimu, this.projet_);
    this.getApplication().getListe_().setSelectedValue(nomSimu, true);

  }

  /**
   * methode de recopiage des donn�es du projet charg� dans la structure dataSimuation.
   */
  public  void getParametresProjet() {

    // A: initialisation des donn�es
//    donneesGenerales_ = new SiporDonneesGenerales();
    categoriesNavires_ = new SiporNavires();
    lQuais_ = new SiporListeQuais();
//    listeHoraire_ = new SiporListeHoraires();
//    nbHoraires_ = 0;
    listebassin_ = new SiporBassins();
    listeGare_ = new SiporGares();
    listeCercle_ = new SiporCercles();
    listeChenal_ = new SiporChenaux();
    listeEcluse_ = new SiporEcluses();
    reglesDureesParcoursChenal_ = new SiporStrutureReglesDureesParcours();
    reglesDureesParcoursCercle_ = new SiporStrutureReglesDureesParcours();
    reglesRemplissageSAS_ = new SiporStrutureReglesRemplissageSAS();
//    listeHorairesNavires_ = new ArrayList();

    /** recopiage des donn�es des gares */

    /** 0)recopiage des donn�es associ�es a la matrice de parcours */
    for (int i = 0; i < this.params_.nbLignesDureeParcoursChenaux; i++) {
      this.reglesDureesParcoursChenal_.ajoutLigne(this.params_.nbColonnesDureeParcoursChenaux);
    }
     
    for (int i = 0; i < this.params_.nbLignesDureeParcoursChenaux; i++) {
      for (int j = 0; j < this.params_.nbColonnesDureeParcoursChenaux; j++) {
        this.reglesDureesParcoursChenal_.modifierValeur(this.params_.matriceDureeParcoursChenaux[i][j], i, j);
      }
    }
    
    /** 0)recopiage des donn�es associ�es a la matrice de remplissage SAS */
    for (int i = 0; i < this.params_.nbLignesRemplissageSAS; i++) {
        this.reglesRemplissageSAS_.ajoutLigne(this.params_.nbColonnesRemplissageSAS);
      }
       
      for (int i = 0; i < this.params_.nbLignesRemplissageSAS; i++) {
        for (int j = 0; j < this.params_.nbColonnesRemplissageSAS; j++) {
          this.reglesRemplissageSAS_.modifierValeur(this.params_.matriceRemplissageSAS[i][j], i, j);
        }
      }
    

    /** 0)recopiage des donn�es associ�es a la matrice de parcours */
    for (int i = 0; i < this.params_.nbLignesDureeParcoursCercles; i++) {
      this.reglesDureesParcoursCercle_.ajoutLigne(this.getParams_().nbColonnesDureeParcoursCercles);
    }

    

    for (int i = 0; i < this.getParams_().nbLignesDureeParcoursCercles; i++) {
      for (int j = 0; j < this.getParams_().nbColonnesDureeParcoursCercles; j++) {
        this.reglesDureesParcoursCercle_.modifierValeur(this.getParams_().matriceDureeParcoursCercles[i][j], i, j);
      }
    }

    /** 1) les donn�es associ�es aux gares: */

    // a) recopiage des donn�es de SParametresGares dans la structure de SiporDataSimulation:
    for (int i = 0; i < this.getParams_().gares.nbGares; i++) {
      this.listeGare_.ajout(this.getParams_().gares.listeGares[i].nom);
    }

    /** 2) les donn�es associ�es aux bassins: */

    // a) recopiage des donn�es de SParametresBassins2 dans la structure de SiporDataSimulation:
    for (int i = 0; i < this.getParams_().bassins.nbBassins; i++) {
      // allocation memoire nouvel element a inserer
      final SiporBassin nouveau = new SiporBassin();
      nouveau.nom_ = this.getParams_().bassins.listeBassins[i].nom;
      nouveau.gareAmont = this.getParams_().bassins.listeBassins[i].gareAmont;
      this.listebassin_.ajout(nouveau);
    }

    /** 3) les donn�es associ�es aux ecluses: */
    initEcluse();

    /** 4) les donn�es associ�es aux cheneaux: */

    initChenaux();

    /** 5) les donn�es associ�es aux cercles d evitages */
    initCercles();

    /** 6) les donn�es associ�es aux quais: */

    initQuais();

    /** 7) les donn�es associ�es aux categories de navire: */

    initNavires();

  }

  private void initNavires() {
    for (int i = 0; i < this.getParams_().navires.nombreNavires; i++) {
      final SiporNavire nouveau = new SiporNavire();
      nouveau.nom = this.getParams_().navires.listeNavires[i].nom;
      nouveau.priorite = this.getParams_().navires.listeNavires[i].priorite;
      nouveau.largeurMax = this.getParams_().navires.listeNavires[i].largeurMax;
      nouveau.largeurMin = this.getParams_().navires.listeNavires[i].largeurMin;
      nouveau.longueurMax = this.getParams_().navires.listeNavires[i].longueurMax;
      nouveau.longueurMin = this.getParams_().navires.listeNavires[i].longueurMin;
      nouveau.OrdreLoiVariationLargeur_ = this.getParams_().navires.listeNavires[i].ordreLoiVariationLargeur;
      nouveau.OrdreLoiVariationLongueur_ = this.getParams_().navires.listeNavires[i].ordreLoiVariationLongueur;
      // retrouver l indice de la gare
      nouveau.gareDepart_ = this.getParams_().navires.listeNavires[i].GareDepart;
      nouveau.NomGareDepart_ = this.listeGare_.retournerGare(nouveau.gareDepart_);
      
      nouveau.dureeAttenteMaximaleAdmissible = this.getParams_().navires.listeNavires[i].dureeAttenteMaxAdmissible;
      
      nouveau.tirantEauEntree = this.getParams_().navires.listeNavires[i].tirantEauEntree;
      nouveau.tirantEauSortie = this.getParams_().navires.listeNavires[i].tirantEauSortie;
      nouveau.quaiPreferentiel1 = this.getParams_().navires.listeNavires[i].QuaiPreferentiel1;
      nouveau.quaiPreferentiel2 = this.getParams_().navires.listeNavires[i].QuaiPreferentiel2;
      nouveau.quaiPreferentiel3 = this.getParams_().navires.listeNavires[i].QuaiPreferentiel3;
      /*F9
      nouveau.dureeQuaiPref1 = this.getParams_().navires.listeNavires[i].dureeQuaiPref1;
      nouveau.dureeQuaiPref2 = this.getParams_().navires.listeNavires[i].dureeQuaiPref2;
      nouveau.dureeQuaiPref3 = this.getParams_().navires.listeNavires[i].dureeQuaiPref3;
      */
      // horaires journaliers et creneaux d ouverture
      nouveau.creneauxJournaliers_.semaineCreneau1HeureArrivee = this.getParams_().navires.listeNavires[i].h.semaineCreneau1HeureArrivee;
      nouveau.creneauxJournaliers_.semaineCreneau1HeureDep = this.getParams_().navires.listeNavires[i].h.semaineCreneau1HeureDep;
      nouveau.creneauxJournaliers_.semaineCreneau2HeureArrivee = this.getParams_().navires.listeNavires[i].h.semaineCreneau2HeureArrivee;
      nouveau.creneauxJournaliers_.semaineCreneau2HeureDep = this.getParams_().navires.listeNavires[i].h.semaineCreneau2HeureDep;
      nouveau.creneauxJournaliers_.semaineCreneau3HeureArrivee = this.getParams_().navires.listeNavires[i].h.semaineCreneau3HeureArrivee;
      nouveau.creneauxJournaliers_.semaineCreneau3HeureDep = this.getParams_().navires.listeNavires[i].h.semaineCreneau3HeureDep;

      nouveau.creneauxouverture_.semaineCreneau1HeureArrivee = this.getParams_().navires.listeNavires[i].creneau1PleineMerFin;
      nouveau.creneauxouverture_.semaineCreneau1HeureDep = this.getParams_().navires.listeNavires[i].creneau1PleineMerDeb;
      nouveau.creneauxouverture_.semaineCreneau2HeureDep = this.getParams_().navires.listeNavires[i].creneau2PleineMerDeb;
      nouveau.creneauxouverture_.semaineCreneau2HeureArrivee = this.getParams_().navires.listeNavires[i].creneau2PleineMerFin;
      nouveau.creneauxouverture_.semaineCreneau3HeureDep = this.getParams_().navires.listeNavires[i].creneau3PleineMerDeb;
      nouveau.creneauxouverture_.semaineCreneau3HeureArrivee = this.getParams_().navires.listeNavires[i].creneau3PleineMerFin;

      // mode chargement, dechargement
      nouveau.typeModeChargement = this.getParams_().navires.listeNavires[i].typeModeChargement;
      System.out.println("je charge la chaine navire: " + nouveau.typeModeChargement);
      if (nouveau.typeModeChargement.equals("shift")) {
        nouveau.ModeChargement = 0;
      } else {
        nouveau.ModeChargement = 1;
      }

      // mode shift
      nouveau.dureeAttenteShiftSuivant = this.getParams_().navires.listeNavires[i].dureeAttenteShiftSuivant;
      nouveau.tonnageMax = this.getParams_().navires.listeNavires[i].tonnageMax;
      nouveau.tonnageMin = this.getParams_().navires.listeNavires[i].tonnageMin;
      nouveau.OrdreLoiVariationTonnage = this.getParams_().navires.listeNavires[i].ordreLoiVariationTonnage;
      nouveau.cadenceQuai = this.getParams_().navires.listeNavires[i].cadenceAutresQuais;
      nouveau.cadenceQuaiPref = this.getParams_().navires.listeNavires[i].cadenceQuaiPreferentiel;

      // recopiage des Horaires de travail suivi mode shift

      initNaviresHoraires(i, nouveau);

      // mode loi:
      nouveau.dureeServiceMinQuaiPref = this.getParams_().navires.listeNavires[i].dureeServiceMinQuaiPref;
      nouveau.dureeServiceMaxQuaiPref = this.getParams_().navires.listeNavires[i].dureeServiceMaxQuaiPref;
      nouveau.dureeServiceMinAutresQuais = this.getParams_().navires.listeNavires[i].dureeServiceMinAutresQuais;
      nouveau.dureeServiceMaxAutresQuais = this.getParams_().navires.listeNavires[i].dureeServiceMaxAutresQuais;
      nouveau.ordreLoiQuaiPref = this.getParams_().navires.listeNavires[i].ordreLoiDureeServiceQuaisPref;
      nouveau.ordreLoiAutresQuais = this.getParams_().navires.listeNavires[i].ordreLoiDureeServiceAutresQuais;

      nouveau.typeLoiGenerationNavires_ = this.getParams_().navires.listeNavires[i].typeLoiGenerationNavires;

      // cas 0: loi d erlang
      nouveau.nbBateauxattendus = this.getParams_().navires.listeNavires[i].nbNaviresAttendus;
      // nouveau.ecartMoyenEntre2arrivees=this.getParams_().navires.listeNavires[i].ecartEntre2arrivees;
      nouveau.loiErlangGenerationNavire = this.getParams_().navires.listeNavires[i].loiErlangGeneration;

      // cas 1: loi JOURNALIERE

      if (nouveau.typeLoiGenerationNavires_ == 2) {
        for (int k = 0; k < this.getParams_().navires.listeNavires[i].nombreHorairesJournaliers; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(1,
              this.getParams_().navires.listeNavires[i].TableauloiJournaliere[k].horaire));

        }
      }

      // cas 2: loi deterministe

      // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

      // recopiage des donn�es une par une
      if (nouveau.typeLoiGenerationNavires_ == 1) {
        for (int k = 0; k < this.getParams_().navires.listeNavires[i].nombreCouplesDeterministes; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(
              this.getParams_().navires.listeNavires[i].TableauloiDeterministe[k].jour,
              this.getParams_().navires.listeNavires[i].TableauloiDeterministe[k].horaire));

        }
      }

      this.categoriesNavires_.ajout(nouveau);
    }
  }

  private void initNaviresHoraires(int _i, final SiporNavire _navire) {
    _navire.horaireTravailSuivi.lundiCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.lundiCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau1HeureDep;
    _navire.horaireTravailSuivi.lundiCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.lundiCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau2HeureDep;
    _navire.horaireTravailSuivi.lundiCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.lundiCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau3HeureDep;

    _navire.horaireTravailSuivi.mardiCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.mardiCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau1HeureDep;
    _navire.horaireTravailSuivi.mardiCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.mardiCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau2HeureDep;
    _navire.horaireTravailSuivi.mardiCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.mardiCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau3HeureDep;

    _navire.horaireTravailSuivi.mercrediCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.mercrediCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau1HeureDep;
    _navire.horaireTravailSuivi.mercrediCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.mercrediCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau2HeureDep;
    _navire.horaireTravailSuivi.mercrediCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.mercrediCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau3HeureDep;

    _navire.horaireTravailSuivi.jeudiCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.jeudiCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau1HeureDep;
    _navire.horaireTravailSuivi.jeudiCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.jeudiCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau2HeureDep;
    _navire.horaireTravailSuivi.jeudiCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.jeudiCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau3HeureDep;

    _navire.horaireTravailSuivi.vendrediCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.vendrediCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau1HeureDep;
    _navire.horaireTravailSuivi.vendrediCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.vendrediCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau2HeureDep;
    _navire.horaireTravailSuivi.vendrediCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.vendrediCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau3HeureDep;

    _navire.horaireTravailSuivi.samediCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.samediCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau1HeureDep;
    _navire.horaireTravailSuivi.samediCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.samediCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau2HeureDep;
    _navire.horaireTravailSuivi.samediCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.samediCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau3HeureDep;

    _navire.horaireTravailSuivi.dimancheCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.dimancheCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau1HeureDep;
    _navire.horaireTravailSuivi.dimancheCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.dimancheCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau2HeureDep;
    _navire.horaireTravailSuivi.dimancheCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.dimancheCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau3HeureDep;

    _navire.horaireTravailSuivi.ferieCreneau1HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau1HeureArrivee;
    _navire.horaireTravailSuivi.ferieCreneau1HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau1HeureDep;
    _navire.horaireTravailSuivi.ferieCreneau2HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau2HeureArrivee;
    _navire.horaireTravailSuivi.ferieCreneau2HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau2HeureDep;
    _navire.horaireTravailSuivi.ferieCreneau3HeureArrivee = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau3HeureArrivee;
    _navire.horaireTravailSuivi.ferieCreneau3HeureDep = this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau3HeureDep;
  }

  private void initQuais() {
    for (int i = 0; i < this.getParams_().quais.nbQuais; i++) {
      final SiporQuais nouveau = new SiporQuais();
      nouveau.Nom_ = this.getParams_().quais.listeQuais[i].nom;
      nouveau.longueur_ = this.getParams_().quais.listeQuais[i].longueur;
      nouveau.dehalageAutorise_ = this.getParams_().quais.listeQuais[i].dehalageAutorise;
      // nouveau.bassin_=this.getParams_().quais.listeQuais[i].bassin;
      nouveau.bassin_ = this.getParams_().quais.listeQuais[i].Bassin;

      // recuperation de l indice du bassin
      nouveau.nomBassin_ = this.listebassin_.retournerBassin(nouveau.bassin_);

      nouveau.dureeIndispo_ = this.getParams_().quais.listeQuais[i].dureeIndispo;
      nouveau.loiIndispo_ = this.getParams_().quais.listeQuais[i].loiIndispo;

      // Les donn�es des lois de frequence a recopier

      // le type de loi
      nouveau.typeLoi_ = this.getParams_().quais.listeQuais[i].typeLoi;

      // cas 0: loi d erlang
      nouveau.frequenceMoyenne_ = this.getParams_().quais.listeQuais[i].frequenceMoyenne;
      nouveau.loiFrequence_ = this.getParams_().quais.listeQuais[i].loiFrequence;

      // cas 1: loi JOURNALIERE
      
      if (nouveau.typeLoi_ == 2) {
        for (int k = 0; k < this.getParams_().quais.listeQuais[i].nombreHorairesJournaliers; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(1,
              this.getParams_().quais.listeQuais[i].TableauloiJournaliere[k].horaire));

        }
      }

      // cas 2: loi deterministe

      // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

      // recopiage des donn�es une par une
      if (nouveau.typeLoi_ == 1) {
        for (int k = 0; k < this.getParams_().quais.listeQuais[i].nombreCouplesDeterministes; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(
              this.getParams_().quais.listeQuais[i].TableauloiDeterministe[k].jour,
              this.getParams_().quais.listeQuais[i].TableauloiDeterministe[k].horaire));

        }
      }

      // recopiage des horaire
      nouveau.h_.semaineCreneau1HeureArrivee = this.getParams_().quais.listeQuais[i].h.semaineCreneau1HeureArrivee;
      nouveau.h_.semaineCreneau1HeureDep = this.getParams_().quais.listeQuais[i].h.semaineCreneau1HeureDep;
      nouveau.h_.semaineCreneau2HeureArrivee = this.getParams_().quais.listeQuais[i].h.semaineCreneau2HeureArrivee;
      nouveau.h_.semaineCreneau2HeureDep = this.getParams_().quais.listeQuais[i].h.semaineCreneau2HeureDep;
      nouveau.h_.semaineCreneau3HeureArrivee = this.getParams_().quais.listeQuais[i].h.semaineCreneau3HeureArrivee;
      nouveau.h_.semaineCreneau3HeureDep = this.getParams_().quais.listeQuais[i].h.semaineCreneau3HeureDep;

      

      this.lQuais_.ajout(nouveau);
    }
  }

  private void initCercles() {
    for (int i = 0; i < this.getParams_().cercles.nbCercles; i++) {
      final SiporCercle nouveau = new SiporCercle(this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav);
      nouveau.nom_ = this.getParams_().cercles.listeCercles[i].nom;
      // nouveau.diametre_=this.getParams_().cercles.listeCercles[i].diametre;
      nouveau.gareAmont_ = this.getParams_().cercles.listeCercles[i].gareAmont;
      nouveau.gareAval_ = this.getParams_().cercles.listeCercles[i].gareAval;

      //-- recopiage des regles de navigations --//
      for (int k = 0; k < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; k++) {
        for (int l = 0; l < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; l++) {
          nouveau.reglesNavigation_.modification(this.getParams_().cercles.listeCercles[i].matriceReglesNavigation[k][l], k,
              l);
        }
      }
      //-- AHX- GESTION genes 2011 - genes --//
      //-- recopiage des genes --//
      for (int k = 0; k < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; k++) {
          for (int l = 0; l < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; l++) {
            nouveau.regleGenes.modification(this.getParams_().cercles.listeCercles[i].matriceGenesNavigation[k][l], k,
                l);
          }
        }
      this.listeCercle_.ajout(nouveau);
    }
  }

  private void initChenaux() {
    for (int i = 0; i < this.getParams_().cheneaux.nbCheneaux; i++) {
      final SiporChenal nouveau = new SiporChenal(this.getParams_().cheneaux.listeCheneaux[i].dimensionMatriceReglesNav);
      nouveau.nom_ = this.getParams_().cheneaux.listeCheneaux[i].nom;
      // nouveau.longueur_=this.getParams_().cheneaux.listeCheneaux[i].longueur;
      nouveau.profondeur_ = this.getParams_().cheneaux.listeCheneaux[i].profondeur;
      nouveau.soumisMaree_ = this.getParams_().cheneaux.listeCheneaux[i].soumisMaree;

      // nouveau.vitesse_=this.getParams_().cheneaux.listeCheneaux[i].vitesse;
      nouveau.gareAmont_ = this.getParams_().cheneaux.listeCheneaux[i].gareAmont;
      nouveau.gareAval_ = this.getParams_().cheneaux.listeCheneaux[i].gareAval;

      
      // loi indispo
      nouveau.dureeIndispo_ = this.getParams_().cheneaux.listeCheneaux[i].dureeIndispo;
      nouveau.loiIndispo_ = this.getParams_().cheneaux.listeCheneaux[i].loiIndispo;
      nouveau.typeLoi_ = this.getParams_().cheneaux.listeCheneaux[i].typeLoi;
      // cas 0: loi d erlang
      nouveau.frequenceMoyenne_ = this.getParams_().cheneaux.listeCheneaux[i].frequenceMoyenne;
      nouveau.loiFrequence_ = this.getParams_().cheneaux.listeCheneaux[i].loiFrequence;
      if (nouveau.typeLoi_ == 2) {
        for (int k = 0; k < this.getParams_().cheneaux.listeCheneaux[i].nombreHorairesJournaliers; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(1,
              this.getParams_().cheneaux.listeCheneaux[i].TableauloiJournaliere[k].horaire));

        }
      }

      // cas 2: loi deterministe

      // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

      // recopiage des donn�es une par une
      if (nouveau.typeLoi_ == 1) {
        for (int k = 0; k < this.getParams_().cheneaux.listeCheneaux[i].nombreCouplesDeterministes; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(
              this.getParams_().cheneaux.listeCheneaux[i].TableauloiDeterministe[k].jour,
              this.getParams_().cheneaux.listeCheneaux[i].TableauloiDeterministe[k].horaire));

        }
      }
      
      
      
      
      
      
      
      // recopiage des horaire
      nouveau.horaires.semaineCreneau1HeureArrivee = this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau1HeureArrivee;
      nouveau.horaires.semaineCreneau1HeureDep = this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau1HeureDep;
      nouveau.horaires.semaineCreneau2HeureArrivee = this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau2HeureArrivee;
      nouveau.horaires.semaineCreneau2HeureDep = this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau2HeureDep;
      nouveau.horaires.semaineCreneau3HeureArrivee = this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau3HeureArrivee;
      nouveau.horaires.semaineCreneau3HeureDep = this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau3HeureDep;

      // recopiage des regels de navigations:
      for (int k = 0; k < this.getParams_().cheneaux.listeCheneaux[i].dimensionMatriceReglesNav; k++) {
        for (int l = 0; l < this.getParams_().cheneaux.listeCheneaux[i].dimensionMatriceReglesNav; l++) {
          nouveau.reglesNavigation_.modification(this.getParams_().cheneaux.listeCheneaux[i].matriceReglesNavigation[k][l],
              k, l);
        }
      }

      this.listeChenal_.ajout(nouveau);
    }
  }

  private void initEcluse() {
    for (int i = 0; i < this.getParams_().ecluses.nbEcluses; i++) {
      final SiporEcluse nouveau = new SiporEcluse();
      nouveau.nom_ = this.getParams_().ecluses.listeEcluses[i].nom;
      nouveau.largeur_ = this.getParams_().ecluses.listeEcluses[i].largeur;
      nouveau.longueur_ = this.getParams_().ecluses.listeEcluses[i].longueur;
      nouveau.creneauEtaleAvantPleineMerDeb_ = this.getParams_().ecluses.listeEcluses[i].creneauEtaleAvantPleineMerDeb;
      nouveau.creneauEtaleApresPleineMerFin_ = this.getParams_().ecluses.listeEcluses[i].creneauEtaleApresPleineMerFin;
      nouveau.dureePassageEtale_ = this.getParams_().ecluses.listeEcluses[i].dureePassageEtale;
      nouveau.tempsEclusee_ = this.getParams_().ecluses.listeEcluses[i].tempsEclusee;
      nouveau.tempsFausseBassinnee_ = this.getParams_().ecluses.listeEcluses[i].tempsFausseBassinnee;

      nouveau.gareAmont_ = this.getParams_().ecluses.listeEcluses[i].gareAmont;
      nouveau.gareAval_ = this.getParams_().ecluses.listeEcluses[i].gareAval;

      // loi indispo
      nouveau.dureeIndispo_ = this.getParams_().ecluses.listeEcluses[i].dureeIndispo;
      nouveau.loiIndispo_ = this.getParams_().ecluses.listeEcluses[i].loiIndispo;
      nouveau.typeLoi_ = this.getParams_().ecluses.listeEcluses[i].typeLoi;
      // cas 0: loi d erlang
      nouveau.frequenceMoyenne_ = this.getParams_().ecluses.listeEcluses[i].frequenceMoyenne;
      nouveau.loiFrequence_ = this.getParams_().ecluses.listeEcluses[i].loiFrequence;
      if (nouveau.typeLoi_ == 2) {
        for (int k = 0; k < this.getParams_().ecluses.listeEcluses[i].nombreHorairesJournaliers; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(1,
              this.getParams_().ecluses.listeEcluses[i].TableauloiJournaliere[k].horaire));

        }
      }

      // cas 2: loi deterministe

      // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

      // recopiage des donn�es une par une
      if (nouveau.typeLoi_ == 1) {
        for (int k = 0; k < this.getParams_().ecluses.listeEcluses[i].nombreCouplesDeterministes; k++) {

          nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(
              this.getParams_().ecluses.listeEcluses[i].TableauloiDeterministe[k].jour,
              this.getParams_().ecluses.listeEcluses[i].TableauloiDeterministe[k].horaire));

        }
      }

      
      nouveau.horaire.semaineCreneau1HeureArrivee = this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau1HeureArrivee;
      nouveau.horaire.semaineCreneau1HeureDep = this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau1HeureDep;
      nouveau.horaire.semaineCreneau2HeureArrivee = this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau2HeureArrivee;
      nouveau.horaire.semaineCreneau2HeureDep = this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau2HeureDep;
      nouveau.horaire.semaineCreneau3HeureArrivee = this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau3HeureArrivee;
      nouveau.horaire.semaineCreneau3HeureDep = this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau3HeureDep;

      this.listeEcluse_.ajout(nouveau);
    }
  }

  /**
   * methode de recopiage des donn�es de la structure dataSimuation dans le projet qui sera ensuite enregistr�.
   */
  public  void setParametresProjet() {

    /** recopiage des donn�es des gares */

    /** 0)recopiage des donn�es associ�es a la matrice de parcours */
    this.getParams_().nbColonnesDureeParcoursChenaux = this.categoriesNavires_.listeNavires_.size();
    this.getParams_().nbLignesDureeParcoursChenaux = this.reglesDureesParcoursChenal_.matriceDuree.size();

    // allocation de la memoire pour la matrice
    this.getParams_().matriceDureeParcoursChenaux = new double[this.getParams_().nbLignesDureeParcoursChenaux][this.getParams_().nbColonnesDureeParcoursChenaux];

    for (int i = 0; i < this.getParams_().nbLignesDureeParcoursChenaux; i++) {
      for (int j = 0; j < this.getParams_().nbColonnesDureeParcoursChenaux; j++) {
        this.getParams_().matriceDureeParcoursChenaux[i][j] = this.reglesDureesParcoursChenal_.retournerValeur(i, j);
      }
    }
    
    /** 0)recopiage des donn�es associ�es a la matrice de SAS */
    this.getParams_().nbColonnesRemplissageSAS = this.categoriesNavires_.listeNavires_.size();
    this.getParams_().nbLignesRemplissageSAS = this.reglesRemplissageSAS_.matriceDelais.size();

    // allocation de la memoire pour la matrice
    this.getParams_().matriceRemplissageSAS = new double[this.getParams_().nbLignesRemplissageSAS][this.getParams_().nbColonnesRemplissageSAS];

    for (int i = 0; i < this.getParams_().nbLignesRemplissageSAS; i++) {
      for (int j = 0; j < this.getParams_().nbColonnesRemplissageSAS; j++) {
        this.getParams_().matriceRemplissageSAS[i][j] = this.reglesRemplissageSAS_.retournerValeur(i, j);
      }
    }
    

    /** 0)recopiage des donn�es associ�es a la matrice de parcours */
    this.getParams_().nbColonnesDureeParcoursCercles = this.categoriesNavires_.listeNavires_.size();
    this.getParams_().nbLignesDureeParcoursCercles = this.reglesDureesParcoursCercle_.matriceDuree.size();

    // allocation de la memoire pour la matrice
    this.getParams_().matriceDureeParcoursCercles = new double[this.getParams_().nbLignesDureeParcoursCercles][this.getParams_().nbColonnesDureeParcoursCercles];

    for (int i = 0; i < this.getParams_().nbLignesDureeParcoursCercles; i++) {
      for (int j = 0; j < this.getParams_().nbColonnesDureeParcoursCercles; j++) {
        this.getParams_().matriceDureeParcoursCercles[i][j] = this.reglesDureesParcoursCercle_.retournerValeur(i, j);
      }
    }

    /** 1) les donn�es associ�es aux gares: */
    // a) recopiage du nombre de gares
    setParametresGares();

    /** 2) les don�nes associ�es aux bassins: */
    // a) recopiage du nombre de bassins
    setParametresBassins();

    /** 3) les donn�es associ�es aux ecluses: */
    // a)recopiage du nombre d'ecluses et allocation de memoire
    setParametresEcluses();

    /** 4) les donn�es associ�es aux cheneaux: */
    setParametresChenaux();

    /** 5) les donn�es associ�es aux cercles: */
    setParametresCercle();

    /** 6) les donn�es associ�es aux quais */

    setParametresQuais();

    /** 7) les donn�es associ�es aux categories de navire: */

    setParametresNavires();

  }

  private void setParametresNavires() {
    // a)recopiage du nombre de cheneaux et allocation de memoire
    this.getParams_().navires = new SParametresNavires2();
    this.getParams_().navires.nombreNavires = this.categoriesNavires_.listeNavires_.size();
    this.getParams_().navires.listeNavires = new SParametresNavire2[this.categoriesNavires_.listeNavires_.size()];
    // b)recopiage

    for (int i = 0; i < this.getParams_().navires.nombreNavires; i++) {
      final SiporNavire copie = this.categoriesNavires_.retournerNavire(i);

      // allocation d'un navire:
      this.getParams_().navires.listeNavires[i] = new SParametresNavire2();

      this.getParams_().navires.listeNavires[i].nom = copie.nom;
      this.getParams_().navires.listeNavires[i].dureeAttenteMaxAdmissible = copie.dureeAttenteMaximaleAdmissible;
      
      this.getParams_().navires.listeNavires[i].priorite = copie.priorite;
      this.getParams_().navires.listeNavires[i].largeurMax = copie.largeurMax;
      this.getParams_().navires.listeNavires[i].largeurMin = copie.largeurMin;
      this.getParams_().navires.listeNavires[i].longueurMax = copie.longueurMax;
      this.getParams_().navires.listeNavires[i].longueurMin = copie.longueurMin;
      this.getParams_().navires.listeNavires[i].ordreLoiVariationLargeur = copie.OrdreLoiVariationLargeur_;
      this.getParams_().navires.listeNavires[i].ordreLoiVariationLongueur = copie.OrdreLoiVariationLongueur_;
      this.getParams_().navires.listeNavires[i].GareDepart = copie.gareDepart_;
      this.getParams_().navires.listeNavires[i].tirantEauEntree = copie.tirantEauEntree;
      this.getParams_().navires.listeNavires[i].tirantEauSortie = copie.tirantEauSortie;
      this.getParams_().navires.listeNavires[i].QuaiPreferentiel1 = copie.quaiPreferentiel1;
      this.getParams_().navires.listeNavires[i].QuaiPreferentiel2 = copie.quaiPreferentiel2;
      this.getParams_().navires.listeNavires[i].QuaiPreferentiel3 = copie.quaiPreferentiel3;
      /*F9
      this.getParams_().navires.listeNavires[i].dureeQuaiPref1 = copie.dureeQuaiPref1;
      this.getParams_().navires.listeNavires[i].dureeQuaiPref2 = copie.dureeQuaiPref2;
      this.getParams_().navires.listeNavires[i].dureeQuaiPref3 = copie.dureeQuaiPref3;
*/
      this.getParams_().navires.listeNavires[i].h = new SParametresHoraires2();
      // recopiage des Horaires journaliers
      this.getParams_().navires.listeNavires[i].h.semaineCreneau1HeureArrivee = copie.creneauxJournaliers_.semaineCreneau1HeureArrivee;
      this.getParams_().navires.listeNavires[i].h.semaineCreneau1HeureDep = copie.creneauxJournaliers_.semaineCreneau1HeureDep;
      this.getParams_().navires.listeNavires[i].h.semaineCreneau2HeureArrivee = copie.creneauxJournaliers_.semaineCreneau2HeureArrivee;
      this.getParams_().navires.listeNavires[i].h.semaineCreneau2HeureDep = copie.creneauxJournaliers_.semaineCreneau2HeureDep;
      this.getParams_().navires.listeNavires[i].h.semaineCreneau3HeureArrivee = copie.creneauxJournaliers_.semaineCreneau3HeureArrivee;
      this.getParams_().navires.listeNavires[i].h.semaineCreneau3HeureDep = copie.creneauxJournaliers_.semaineCreneau3HeureDep;

      // recopiage des creneaux d ouvertures
      this.getParams_().navires.listeNavires[i].creneau1PleineMerDeb = copie.creneauxouverture_.semaineCreneau1HeureDep;
      this.getParams_().navires.listeNavires[i].creneau1PleineMerFin = copie.creneauxouverture_.semaineCreneau1HeureArrivee;
      this.getParams_().navires.listeNavires[i].creneau2PleineMerDeb = copie.creneauxouverture_.semaineCreneau2HeureDep;
      this.getParams_().navires.listeNavires[i].creneau2PleineMerFin = copie.creneauxouverture_.semaineCreneau2HeureArrivee;
      this.getParams_().navires.listeNavires[i].creneau3PleineMerDeb = copie.creneauxouverture_.semaineCreneau3HeureDep;
      this.getParams_().navires.listeNavires[i].creneau3PleineMerFin = copie.creneauxouverture_.semaineCreneau3HeureArrivee;

      // mode chargement, dechargement
      System.out.println("je sauvegarde navire: " + copie.typeModeChargement);
      this.getParams_().navires.listeNavires[i].typeModeChargement = copie.typeModeChargement;

      // mode shift
      this.getParams_().navires.listeNavires[i].dureeAttenteShiftSuivant = copie.dureeAttenteShiftSuivant;
      this.getParams_().navires.listeNavires[i].tonnageMax = copie.tonnageMax;
      this.getParams_().navires.listeNavires[i].tonnageMin = copie.tonnageMin;
      this.getParams_().navires.listeNavires[i].ordreLoiVariationTonnage = copie.OrdreLoiVariationTonnage;
      this.getParams_().navires.listeNavires[i].cadenceAutresQuais = copie.cadenceQuai;
      this.getParams_().navires.listeNavires[i].cadenceQuaiPreferentiel = copie.cadenceQuaiPref;

      // recopiage des Horaires de travail suivi mode shift
      setParametresNavires(i, copie);

      // mode loi:
      this.getParams_().navires.listeNavires[i].dureeServiceMinQuaiPref = copie.dureeServiceMinQuaiPref;
      this.getParams_().navires.listeNavires[i].dureeServiceMaxQuaiPref = copie.dureeServiceMaxQuaiPref;
      this.getParams_().navires.listeNavires[i].dureeServiceMinAutresQuais = copie.dureeServiceMinAutresQuais;
      this.getParams_().navires.listeNavires[i].dureeServiceMaxAutresQuais = copie.dureeServiceMaxAutresQuais;
      this.getParams_().navires.listeNavires[i].ordreLoiDureeServiceQuaisPref = copie.ordreLoiQuaiPref;
      this.getParams_().navires.listeNavires[i].ordreLoiDureeServiceAutresQuais = copie.ordreLoiAutresQuais;

      // cas 0: loi d erlang
      this.getParams_().navires.listeNavires[i].typeLoiGenerationNavires = copie.typeLoiGenerationNavires_;
      this.getParams_().navires.listeNavires[i].nbNaviresAttendus = copie.nbBateauxattendus;
      // this.getParams_().navires.listeNavires[i].ecartEntre2arrivees=copie.ecartMoyenEntre2arrivees;
      this.getParams_().navires.listeNavires[i].loiErlangGeneration = copie.loiErlangGenerationNavire;

      if (copie.typeLoiGenerationNavires_ == 2) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().navires.listeNavires[i].TableauloiJournaliere = new SParametresCoupleLoiJournaliere2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de la loi deterministe
        this.getParams_().navires.listeNavires[i].nombreHorairesJournaliers = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une

        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().navires.listeNavires[i].TableauloiJournaliere[k] = new SParametresCoupleLoiJournaliere2();

          this.getParams_().navires.listeNavires[i].TableauloiJournaliere[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;

        }

      }
      // cas 2: loi deterministe

      if (copie.typeLoiGenerationNavires_ == 1) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().navires.listeNavires[i].TableauloiDeterministe = new SParametresCoupleLoiDeterministe2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de al loi deterministe
        this.getParams_().navires.listeNavires[i].nombreCouplesDeterministes = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une
        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().navires.listeNavires[i].TableauloiDeterministe[k] = new SParametresCoupleLoiDeterministe2();

          this.getParams_().navires.listeNavires[i].TableauloiDeterministe[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;
          this.getParams_().navires.listeNavires[i].TableauloiDeterministe[k].jour = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).jour_;

        }
      }

    }
  }

  private void setParametresNavires(int _i, final SiporNavire _navire) {
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi = new SParametresHorairesNavires2();
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau1HeureArrivee = _navire.horaireTravailSuivi.lundiCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau1HeureDep = _navire.horaireTravailSuivi.lundiCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau2HeureArrivee = _navire.horaireTravailSuivi.lundiCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau2HeureDep = _navire.horaireTravailSuivi.lundiCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau3HeureArrivee = _navire.horaireTravailSuivi.lundiCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.lundiCreneau3HeureDep = _navire.horaireTravailSuivi.lundiCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau1HeureArrivee = _navire.horaireTravailSuivi.mardiCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau1HeureDep = _navire.horaireTravailSuivi.mardiCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau2HeureArrivee = _navire.horaireTravailSuivi.mardiCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau2HeureDep = _navire.horaireTravailSuivi.mardiCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau3HeureArrivee = _navire.horaireTravailSuivi.mardiCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mardiCreneau3HeureDep = _navire.horaireTravailSuivi.mardiCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau1HeureArrivee = _navire.horaireTravailSuivi.mercrediCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau1HeureDep = _navire.horaireTravailSuivi.mercrediCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau2HeureArrivee = _navire.horaireTravailSuivi.mercrediCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau2HeureDep = _navire.horaireTravailSuivi.mercrediCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau3HeureArrivee = _navire.horaireTravailSuivi.mercrediCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.mercrediCreneau3HeureDep = _navire.horaireTravailSuivi.mercrediCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau1HeureArrivee = _navire.horaireTravailSuivi.jeudiCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau1HeureDep = _navire.horaireTravailSuivi.jeudiCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau2HeureArrivee = _navire.horaireTravailSuivi.jeudiCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau2HeureDep = _navire.horaireTravailSuivi.jeudiCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau3HeureArrivee = _navire.horaireTravailSuivi.jeudiCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.jeudiCreneau3HeureDep = _navire.horaireTravailSuivi.jeudiCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau1HeureArrivee = _navire.horaireTravailSuivi.vendrediCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau1HeureDep = _navire.horaireTravailSuivi.vendrediCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau2HeureArrivee = _navire.horaireTravailSuivi.vendrediCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau2HeureDep = _navire.horaireTravailSuivi.vendrediCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau3HeureArrivee = _navire.horaireTravailSuivi.vendrediCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.vendrediCreneau3HeureDep = _navire.horaireTravailSuivi.vendrediCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau1HeureArrivee = _navire.horaireTravailSuivi.samediCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau1HeureDep = _navire.horaireTravailSuivi.samediCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau2HeureArrivee = _navire.horaireTravailSuivi.samediCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau2HeureDep = _navire.horaireTravailSuivi.samediCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau3HeureArrivee = _navire.horaireTravailSuivi.samediCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.samediCreneau3HeureDep = _navire.horaireTravailSuivi.samediCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau1HeureArrivee = _navire.horaireTravailSuivi.dimancheCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau1HeureDep = _navire.horaireTravailSuivi.dimancheCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau2HeureArrivee = _navire.horaireTravailSuivi.dimancheCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau2HeureDep = _navire.horaireTravailSuivi.dimancheCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau3HeureArrivee = _navire.horaireTravailSuivi.dimancheCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.dimancheCreneau3HeureDep = _navire.horaireTravailSuivi.dimancheCreneau3HeureDep;

    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau1HeureArrivee = _navire.horaireTravailSuivi.ferieCreneau1HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau1HeureDep = _navire.horaireTravailSuivi.ferieCreneau1HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau2HeureArrivee = _navire.horaireTravailSuivi.ferieCreneau2HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau2HeureDep = _navire.horaireTravailSuivi.ferieCreneau2HeureDep;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau3HeureArrivee = _navire.horaireTravailSuivi.ferieCreneau3HeureArrivee;
    this.getParams_().navires.listeNavires[_i].horairesTravailSuivi.ferieCreneau3HeureDep = _navire.horaireTravailSuivi.ferieCreneau3HeureDep;
  }

  private void setParametresQuais() {
    // a)recopiage du nombre de cheneaux et allocation de memoire
    this.getParams_().quais = new SParametresQuais2();
    this.getParams_().quais.nbQuais = this.getlQuais_().getlQuais_().size();
    this.getParams_().quais.listeQuais = new SParametresQuai2[this.getlQuais_().getlQuais_().size()];
    // b)recopiage
    for (int i = 0; i < this.getlQuais_().getlQuais_().size(); i++) {
      final SiporQuais copie = this.lQuais_.retournerQuais(i);
      this.getParams_().quais.listeQuais[i] = new SParametresQuai2();
      this.getParams_().quais.listeQuais[i].nom = copie.getNom();
      this.getParams_().quais.listeQuais[i].longueur = copie.longueur_;
      this.getParams_().quais.listeQuais[i].dehalageAutorise = copie.dehalageAutorise_;
      // this.getParams_().quais.listeQuais[i].bassin=copie.bassin_;
      this.getParams_().quais.listeQuais[i].Bassin = copie.bassin_;
      this.getParams_().quais.listeQuais[i].dureeIndispo = copie.dureeIndispo_;
      this.getParams_().quais.listeQuais[i].loiIndispo = copie.loiIndispo_;

      // Les donn�es des lois de frequence a recopier

      // le type de loi
      this.getParams_().quais.listeQuais[i].typeLoi = copie.typeLoi_;

      // cas 0: loi d erlang
      this.getParams_().quais.listeQuais[i].loiFrequence = copie.loiFrequence_;
      this.getParams_().quais.listeQuais[i].frequenceMoyenne = copie.frequenceMoyenne_;

      // cas 1: loi journaliere
      
      if (copie.typeLoi_ == 2) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().quais.listeQuais[i].TableauloiJournaliere = new SParametresCoupleLoiJournaliere2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de la loi deterministe
        this.getParams_().quais.listeQuais[i].nombreHorairesJournaliers = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une

        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().quais.listeQuais[i].TableauloiJournaliere[k] = new SParametresCoupleLoiJournaliere2();

          this.getParams_().quais.listeQuais[i].TableauloiJournaliere[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;

        }

      }
      // cas 2: loi deterministe

      if (copie.typeLoi_ == 1) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().quais.listeQuais[i].TableauloiDeterministe = new SParametresCoupleLoiDeterministe2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de al loi deterministe
        this.getParams_().quais.listeQuais[i].nombreCouplesDeterministes = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une
        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().quais.listeQuais[i].TableauloiDeterministe[k] = new SParametresCoupleLoiDeterministe2();

          this.getParams_().quais.listeQuais[i].TableauloiDeterministe[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;
          this.getParams_().quais.listeQuais[i].TableauloiDeterministe[k].jour = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).jour_;

        }
      }
      this.getParams_().quais.listeQuais[i].h = new SParametresHoraires2();
      this.getParams_().quais.listeQuais[i].h.semaineCreneau1HeureArrivee = copie.h_.semaineCreneau1HeureArrivee;
      this.getParams_().quais.listeQuais[i].h.semaineCreneau1HeureDep = copie.h_.semaineCreneau1HeureDep;
      this.getParams_().quais.listeQuais[i].h.semaineCreneau2HeureArrivee = copie.h_.semaineCreneau2HeureArrivee;
      this.getParams_().quais.listeQuais[i].h.semaineCreneau2HeureDep = copie.h_.semaineCreneau2HeureDep;
      this.getParams_().quais.listeQuais[i].h.semaineCreneau3HeureArrivee = copie.h_.semaineCreneau3HeureArrivee;
      this.getParams_().quais.listeQuais[i].h.semaineCreneau3HeureDep = copie.h_.semaineCreneau3HeureDep;
    }
  }

  private void setParametresCercle() {
    // a)recopiage du nombre de cheneaux et allocation de memoire
    this.getParams_().cercles = new SParametresCerclesGenes();
    this.getParams_().cercles.nbCercles = this.getListeCercle_().getListeCercles_().size();
    this.getParams_().cercles.listeCercles = new SParametresCercleGene[this.getListeCercle_().getListeCercles_().size()];
    // b)recopiage
    for (int i = 0; i < this.getListeCercle_().getListeCercles_().size(); i++) {
      // allocation memoire nouveau chenal
      this.getParams_().cercles.listeCercles[i] = new SParametresCercleGene();
      // recuperation de l'�l�ment � copier
      final SiporCercle copie = this.listeCercle_.retournerCercle(i);
      this.getParams_().cercles.listeCercles[i].nom = copie.getNom_();
      // this.getParams_().cercles.listeCercles[i].diametre=copie.diametre_;
      this.getParams_().cercles.listeCercles[i].gareAmont = copie.gareAmont_;
      this.getParams_().cercles.listeCercles[i].gareAval = copie.gareAval_;

      //-- les regles de navigations --//
      //-- recopiage des regles de navigations --//
      // 1)dimension de la matrice
      this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav = copie.reglesNavigation_.vecteurDeVecteur_.size();
      // 2)allocation de memoire
      this.getParams_().cercles.listeCercles[i].matriceReglesNavigation = new boolean[copie.reglesNavigation_.vecteurDeVecteur_
          .size()][copie.reglesNavigation_.vecteurDeVecteur_.size()];
      // 3)recopiage de la matrice
      for (int k = 0; k < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; k++) {
        for (int l = 0; l < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; l++) {
          this.getParams_().cercles.listeCercles[i].matriceReglesNavigation[k][l] = copie.reglesNavigation_.retournerAij(k,
              l).booleanValue();
        }
      }
      //-- AHX- GESTION genes 2011 - genes --//
      this.getParams_().cercles.listeCercles[i].matriceGenesNavigation = new double[copie.regleGenes.vecteurDeVecteur_
                                                                                 .size()][copie.regleGenes.vecteurDeVecteur_.size()];
      for (int k = 0; k < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; k++) {
          for (int l = 0; l < this.getParams_().cercles.listeCercles[i].dimensionMatriceReglesNav; l++) {
            this.getParams_().cercles.listeCercles[i].matriceGenesNavigation[k][l] = copie.regleGenes.retournerAij(k,
                l).doubleValue();
          }
        }
    }
  }

  private void setParametresChenaux() {
    // a)recopiage du nombre de cheneaux et allocation de memoire
    this.getParams_().cheneaux = new SParametresCheneaux2();
    this.getParams_().cheneaux.nbCheneaux = this.getListeChenal_().getListeChenaux_().size();
    this.getParams_().cheneaux.listeCheneaux = new SParametresChenal2[this.getListeChenal_().getListeChenaux_().size()];
    // b)recopiage
    for (int i = 0; i < this.getListeChenal_().getListeChenaux_().size(); i++) {
      // allocation memoire nouveau chenal
      this.getParams_().cheneaux.listeCheneaux[i] = new SParametresChenal2();
      // recuperation de l'�l�ment � copier
      final SiporChenal copie = this.listeChenal_.retournerChenal(i);

      this.getParams_().cheneaux.listeCheneaux[i].nom = copie.getNom_();
      // this.getParams_().cheneaux.listeCheneaux[i].longueur=copie.longueur_;
      this.getParams_().cheneaux.listeCheneaux[i].profondeur = copie.profondeur_;
      this.getParams_().cheneaux.listeCheneaux[i].soumisMaree = copie.soumisMaree_;
      // this.getParams_().cheneaux.listeCheneaux[i].vitesse=copie.vitesse_;
      this.getParams_().cheneaux.listeCheneaux[i].gareAmont = copie.gareAmont_;
      this.getParams_().cheneaux.listeCheneaux[i].gareAval = copie.gareAval_;
     
      
      this.getParams_().cheneaux.listeCheneaux[i].dureeIndispo = copie.dureeIndispo_;
      this.getParams_().cheneaux.listeCheneaux[i].loiIndispo = copie.loiIndispo_;
      // Les donn�es des lois de frequence a recopier
      // le type de loi
      this.getParams_().cheneaux.listeCheneaux[i].typeLoi = copie.typeLoi_;

      // cas 0: loi d erlang
      this.getParams_().cheneaux.listeCheneaux[i].loiFrequence = copie.loiFrequence_;
      this.getParams_().cheneaux.listeCheneaux[i].frequenceMoyenne = copie.frequenceMoyenne_;

      if (copie.typeLoi_ == 2) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().cheneaux.listeCheneaux[i].TableauloiJournaliere = new SParametresCoupleLoiJournaliere2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de la loi deterministe
        this.getParams_().cheneaux.listeCheneaux[i].nombreHorairesJournaliers = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une

        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().cheneaux.listeCheneaux[i].TableauloiJournaliere[k] = new SParametresCoupleLoiJournaliere2();

          this.getParams_().cheneaux.listeCheneaux[i].TableauloiJournaliere[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;

        }

      }
      // cas 2: loi deterministe

      if (copie.typeLoi_ == 1) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().cheneaux.listeCheneaux[i].TableauloiDeterministe = new SParametresCoupleLoiDeterministe2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de al loi deterministe
        this.getParams_().cheneaux.listeCheneaux[i].nombreCouplesDeterministes = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une
        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().cheneaux.listeCheneaux[i].TableauloiDeterministe[k] = new SParametresCoupleLoiDeterministe2();

          this.getParams_().cheneaux.listeCheneaux[i].TableauloiDeterministe[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;
          this.getParams_().cheneaux.listeCheneaux[i].TableauloiDeterministe[k].jour = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).jour_;

        }
      }
      
      
      
      
      
      // recopiage des Horaires
      this.getParams_().cheneaux.listeCheneaux[i].h = new SParametresHoraires2();

      this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau1HeureArrivee = copie.horaires.semaineCreneau1HeureArrivee;
      this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau1HeureDep = copie.horaires.semaineCreneau1HeureDep;
      this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau2HeureArrivee = copie.horaires.semaineCreneau2HeureArrivee;
      this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau2HeureDep = copie.horaires.semaineCreneau2HeureDep;
      this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau3HeureArrivee = copie.horaires.semaineCreneau3HeureArrivee;
      this.getParams_().cheneaux.listeCheneaux[i].h.semaineCreneau3HeureDep = copie.horaires.semaineCreneau3HeureDep;

      // les regles de navigations
      // recopiage des regels de navigations:
      // 1)dimension de la matrice
      this.getParams_().cheneaux.listeCheneaux[i].dimensionMatriceReglesNav = copie.reglesNavigation_.vecteurDeVecteur_
          .size();
      // 2)allocation de memoire
      this.getParams_().cheneaux.listeCheneaux[i].matriceReglesNavigation = new boolean[copie.reglesNavigation_.vecteurDeVecteur_
          .size()][copie.reglesNavigation_.vecteurDeVecteur_.size()];
      // 3)recopiage de la matrice
      for (int k = 0; k < this.getParams_().cheneaux.listeCheneaux[i].dimensionMatriceReglesNav; k++) {
        for (int l = 0; l < this.getParams_().cheneaux.listeCheneaux[i].dimensionMatriceReglesNav; l++) {
          this.getParams_().cheneaux.listeCheneaux[i].matriceReglesNavigation[k][l] = copie.reglesNavigation_.retournerAij(
              k, l).booleanValue();
        }
      }

    }
  }

  private void setParametresEcluses() {
    this.getParams_().ecluses = new SParametresEcluses2();
    this.getParams_().ecluses.nbEcluses = this.getListeEcluse_().getListeEcluses_().size();
    this.getParams_().ecluses.listeEcluses = new SParametresEcluse2[this.getListeEcluse_().getListeEcluses_().size()];
    // b)recopiage
    for (int i = 0; i < this.getListeEcluse_().getListeEcluses_().size(); i++) {
      // allocation memoire nouvelel ecluse
      this.getParams_().ecluses.listeEcluses[i] = new SParametresEcluse2();
      // recuperation de l'�l�ment � copier
      final SiporEcluse copie = this.listeEcluse_.retournerEcluse(i);

      this.getParams_().ecluses.listeEcluses[i].nom = copie.getNom_();
      this.getParams_().ecluses.listeEcluses[i].largeur = copie.largeur_;
      this.getParams_().ecluses.listeEcluses[i].longueur = copie.longueur_;
      this.getParams_().ecluses.listeEcluses[i].creneauEtaleAvantPleineMerDeb = copie.creneauEtaleAvantPleineMerDeb_;
      this.getParams_().ecluses.listeEcluses[i].creneauEtaleApresPleineMerFin = copie.creneauEtaleApresPleineMerFin_;
      this.getParams_().ecluses.listeEcluses[i].gareAmont = copie.gareAmont_;
      this.getParams_().ecluses.listeEcluses[i].gareAval = copie.gareAval_;
      this.getParams_().ecluses.listeEcluses[i].dureePassageEtale = copie.dureePassageEtale_;
      this.getParams_().ecluses.listeEcluses[i].tempsEclusee = copie.tempsEclusee_;
      this.getParams_().ecluses.listeEcluses[i].tempsFausseBassinnee = copie.tempsFausseBassinnee_;

      this.getParams_().ecluses.listeEcluses[i].dureeIndispo = copie.dureeIndispo_;
      this.getParams_().ecluses.listeEcluses[i].loiIndispo = copie.loiIndispo_;

      // Les donn�es des lois de frequence a recopier

      // le type de loi
      this.getParams_().ecluses.listeEcluses[i].typeLoi = copie.typeLoi_;

      // cas 0: loi d erlang
      this.getParams_().ecluses.listeEcluses[i].loiFrequence = copie.loiFrequence_;
      this.getParams_().ecluses.listeEcluses[i].frequenceMoyenne = copie.frequenceMoyenne_;

      if (copie.typeLoi_ == 2) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().ecluses.listeEcluses[i].TableauloiJournaliere = new SParametresCoupleLoiJournaliere2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de la loi deterministe
        this.getParams_().ecluses.listeEcluses[i].nombreHorairesJournaliers = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une

        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().ecluses.listeEcluses[i].TableauloiJournaliere[k] = new SParametresCoupleLoiJournaliere2();

          this.getParams_().ecluses.listeEcluses[i].TableauloiJournaliere[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;

        }

      }
      // cas 2: loi deterministe

      if (copie.typeLoi_ == 1) {
        // allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
        this.getParams_().ecluses.listeEcluses[i].TableauloiDeterministe = new SParametresCoupleLoiDeterministe2[copie.loiDeterministe_
            .size()];
        // recopiage du nombre de donn�es de al loi deterministe
        this.getParams_().ecluses.listeEcluses[i].nombreCouplesDeterministes = copie.loiDeterministe_.size();

        // recopiage des donn�es une par une
        for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
          this.getParams_().ecluses.listeEcluses[i].TableauloiDeterministe[k] = new SParametresCoupleLoiDeterministe2();

          this.getParams_().ecluses.listeEcluses[i].TableauloiDeterministe[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).temps_;
          this.getParams_().ecluses.listeEcluses[i].TableauloiDeterministe[k].jour = ((CoupleLoiDeterministe) copie.loiDeterministe_
              .get(k)).jour_;

        }
      }

      this.getParams_().ecluses.listeEcluses[i].h = new SParametresHoraires2();
    
      this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau1HeureArrivee = copie.horaire.semaineCreneau1HeureArrivee;
      this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau1HeureDep = copie.horaire.semaineCreneau1HeureDep;
      this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau2HeureArrivee = copie.horaire.semaineCreneau2HeureArrivee;
      this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau2HeureDep = copie.horaire.semaineCreneau2HeureDep;
      this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau3HeureArrivee = copie.horaire.semaineCreneau3HeureArrivee;
      this.getParams_().ecluses.listeEcluses[i].h.semaineCreneau3HeureDep = copie.horaire.semaineCreneau3HeureDep;

    }
  }

  private void setParametresBassins() {
    this.getParams_().bassins = new SParametresBassins2();
    this.getParams_().bassins.nbBassins = this.listebassin_.listeBassins_.size();
    this.getParams_().bassins.listeBassins = new SParametresBassin2[this.listebassin_.listeBassins_.size()];
    // b) recopiage
    for (int i = 0; i < this.listebassin_.listeBassins_.size(); i++) {
      // allocation memoire nouveau bassin
      this.getParams_().bassins.listeBassins[i] = new SParametresBassin2();
      // recuperation de l element a copier
      final SiporBassin copie = this.listebassin_.retournerBassin2(i);
      this.getParams_().bassins.listeBassins[i].nom = copie.getNom_();
      this.getParams_().bassins.listeBassins[i].gareAmont = copie.gareAmont;
    }
  }

  private void setParametresGares() {
    this.getParams_().gares = new SParametresGares();
    this.getParams_().gares.nbGares = this.listeGare_.listeGares_.size();
    this.getParams_().gares.listeGares = new SParametresGare[this.listeGare_.listeGares_.size()];
    // b) recopiage
    for (int i = 0; i < this.listeGare_.listeGares_.size(); i++) {
      this.getParams_().gares.listeGares[i] = new SParametresGare(this.listeGare_.retournerGare(i));
    }
  }

  public  void activerCommandesNiveau() {

    // par defaut toujours activer la saisies des don�nes generales
    // cette saisie donne lieu au niveau suivant
    this.getApplication().setEnabledForAction("DONNEESGENERALES", true);

    /**
     * Activation de panel saisies topologie regles de navigation
     */
    this.getApplication().activerCommandesSimulation();

  }

  /**
   * Methode qui verifie toutes les donn�es avant lancement de la simulation.
   * 
   * @return true les �l�ments sont coh�rents, saisie completement et aucune exception relevee
   */
  public boolean verificationDonnees() {
    if (frameVerif_ != null) {
      if (!frameVerif_.isClosed()) {
        return false;
      }
    }

    /**
     * creation de la frame d'afficahge des verifications
     */
    final JLabel[] tableauVerif = new JLabel[800];
    final JPanel panelMessage = new JPanel();
    final JScrollPane asc = new JScrollPane(panelMessage);
    final JPanel controlPanel = new JPanel();
    final BuButton validation = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Validation");
    final BuButton quitter = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

    /* final SiporInternalFrame */frameVerif_ = new SiporInternalFrame("", true, true, true, true);

    quitter.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        frameVerif_.dispose();
        frameVerif_ = null;

      }
    });
    validation.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        frameVerif_.dispose();
        frameVerif_ = null;
      }

    });

    panelMessage.setLayout(new GridLayout(800, 1));
    for (int i = 0; i < 800; i++) {
      tableauVerif[i] = new JLabel("");
      tableauVerif[i].setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

      panelMessage.add(tableauVerif[i]);

    }
    controlPanel.add(quitter);
    controlPanel.add(validation);

    frameVerif_.getContentPane().setLayout(new BorderLayout());
    frameVerif_.getContentPane().add(asc, BorderLayout.CENTER);
    frameVerif_.getContentPane().add(controlPanel, BorderLayout.SOUTH);
    // bouton ne devient accessible que si la totalit� des test ont �t� r�alis�� avec succ�s
    validation.setEnabled(false);
    frameVerif_.setTitle("V�rifications avant lancement simulation");
    frameVerif_.setSize(650, 500);

    frameVerif_.setVisible(true);
    this.getApplication().addInternalFrame(frameVerif_);
    /** fin mise en page de la frame* */

    // test 1: on verifie qu il existe un nombre minimum de donnees avant lancement de la simulation:
    tableauVerif[0].setForeground(Color.BLUE);
    String font = getDefaultFontName();
    if (this.listeGare_.listeGares_.size() >= 2) {
      tableauVerif[0].setText("test 1 r�ussi: Nombre de gares sup�rieur a 2");
    } else {
      tableauVerif[0].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[0].setForeground(Color.RED);
      tableauVerif[0].setText("test 1 �choue: Nombre de gares sup�rieur a 2");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Erreur! Il faut au moins 2 gares.").activate();
      return false;
    }
    tableauVerif[1].setForeground(Color.BLUE);
    if (this.listebassin_.listeBassins_.size() >= 1) {
      tableauVerif[1].setText("test 2 r�ussi: au moins un bassin existant");
    } else {
      tableauVerif[1].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[1].setForeground(Color.RED);
      tableauVerif[1].setText("test 2 �choue: Nombre insuffisant de bassins");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Erreur! Il faut au moins un bassin").activate();
      return false;
    }
    tableauVerif[2].setForeground(Color.BLUE);
    if (this.getlQuais_().getlQuais_().size() >= 1) {
      tableauVerif[2].setText("test 3 r�ussi: au moins un quai existant");
    } else {
      tableauVerif[2].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[2].setForeground(Color.RED);
      tableauVerif[2].setText("test 3 �choue: Nombre insuffisant de quais");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Erreur! Il faut au moins un quai").activate();
      return false;
    }
    tableauVerif[3].setForeground(Color.BLUE);
    if (this.getListeChenal_().getListeChenaux_().size() >= 1) {
      tableauVerif[3].setText("test 4 r�ussi: au moins un chenal existant");
    } else {
      tableauVerif[3].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[3].setForeground(Color.RED);
      tableauVerif[3].setText("test 4 �choue: Nombre insuffisant de cheneaux");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Erreur! Il faut au moins un chenal").activate();
      return false;
    }
    tableauVerif[4].setForeground(Color.BLUE);
    if (this.getListeEcluse_().getListeEcluses_().size() >= 1) {
      tableauVerif[4].setText("test 5: Au moins une �cluse existante");
    } else {
      tableauVerif[4].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[4].setForeground(Color.RED);
      tableauVerif[4].setText("test 5: Aucune �cluse");
     

    }
    tableauVerif[5].setForeground(Color.BLUE);
    if (this.getListeCercle_().getListeCercles_().size() >= 1) {
      tableauVerif[5].setText("test 6 r�ussi: au moins un cercle d'�vitage existant");
    } else {
      tableauVerif[5].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[5].setForeground(Color.RED);
      tableauVerif[5].setText("test 6 WARNING: aucun cercle d'�vitage");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "WARNING! aucun cercle d'�vitage!!").activate();

    }
    tableauVerif[6].setForeground(Color.BLUE);
    if (this.categoriesNavires_.listeNavires_.size() >= 1) {
      tableauVerif[6].setText("test 7 r�ussi: au moins une cat�gorie de navire existant");
    } else {
      tableauVerif[6].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[6].setForeground(Color.RED);
      tableauVerif[6].setText("test 7 �choue: Nombre insuffisant de cat�gories de navires");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "Erreur! Il faut au moins une cat�gorie de navire").activate();
      return false;
    }

    int compteur = 7;
    // test 2: verification des gares orphelines
    tableauVerif[compteur].setForeground(Color.BLACK);
    tableauVerif[compteur++].setText("test 8: Signalement des gares orphelines");
    tableauVerif[compteur].setForeground(Color.BLUE);
    compteur = suppressionGaresOrphelines(tableauVerif, compteur);

    // test 3: verification de la saisie dees regles de parcours: chanaux
    tableauVerif[compteur].setForeground(Color.BLUE);
    if (!verifDureesParcoursChenal()) {
      tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[compteur].setForeground(Color.RED);
      tableauVerif[compteur++].setText("test 9 WARNING: Les dur�es de parcours chenaux nulles");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "WARNING:\n Les dur�es de parcours chenaux " + "\n Onglet r�gles/ r�gles dur�es de parcours chenaux  "
              + "\n - Il y a des valeurs non saisies (par exemple 0) ").activate();

    } else {
      tableauVerif[compteur++].setText("test 9 r�ussi: Les durees de parcours chenaux valides");
    }

    // test 4: verification de la saisie dees r�gles de parcours: chercles
    tableauVerif[compteur].setForeground(Color.BLUE);
    if (!verifDureesParcoursCercle()) {
      tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[compteur].setForeground(Color.RED);
      tableauVerif[compteur++].setText("test 10 WARNING: Les dur�es de parcours cercles nulles");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "test 10 WARNING:\n Les dur�es de parcours cercles "
              + "\n Onglet r�gles/ r�gles dur�es de parcours cercles  "
              + "\n - Il y a des valeurs non saisies (par exemple 0)").activate();
      

    } else {
      tableauVerif[compteur++].setText("test 10 r�ussi: Les dur�es de parcours cercles valides");
    }

    // test 5: passage navires ecluses

    tableauVerif[compteur].setForeground(Color.BLACK);
    tableauVerif[compteur++].setText("test 11:Signalement des largeurs cat�gories et �cluses");
    tableauVerif[compteur].setForeground(Color.BLUE);
    int anciencpt = compteur;
    compteur = verifPassageEcluse(tableauVerif, compteur);
    if (anciencpt != compteur) {

    } else {
      tableauVerif[compteur++].setText("test 11 r�ussi: toutes les cat�gories passent les �cluses.");
    }

    tableauVerif[compteur].setForeground(Color.BLACK);
    tableauVerif[compteur++].setText("test 12:Signalement des longueurs cat�gories et �cluses.");
    tableauVerif[compteur].setForeground(Color.BLUE);
    anciencpt = compteur;
    compteur = verifPassageEcluseLongueur(tableauVerif, compteur);
    if (anciencpt != compteur) {

    } else {
      tableauVerif[compteur++].setText("test 12 r�ussi: toutes les cat�gories sont moins longues que les �cluses.");
    }

    tableauVerif[compteur].setForeground(Color.BLACK);
    tableauVerif[compteur++]
        .setText("test 13: Signalement tirant d'eau entr�e des cat�gories par rapport � la profondeur des chenaux sans mar�es");
    tableauVerif[compteur].setForeground(Color.BLUE);
    int previousCpt = compteur;
    compteur = verifPassageChenaux(tableauVerif, compteur);

    if (compteur != previousCpt) {

    } else {
      tableauVerif[compteur++]
          .setText("test 13 r�ussi:tirant d'eau entr�e: toutes les cat�gories passent les chenaux!");
    }

    
    tableauVerif[compteur].setForeground(Color.BLUE);
    if (!verifGenesCercles()) {
      tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[compteur].setForeground(Color.RED);
      tableauVerif[compteur++].setText("test 14 WARNING: Les g�nes du cercle sont nulles");
      new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
          "WARNING:\n Les g�nes cercle " + "\n Onglet r�gles/ r�gles g�nes cercle  "
              + "\n - Il y a des valeurs non saisies (par exemple 0) ").activate();

    } else {
      tableauVerif[compteur++].setText("test 14 r�ussi: Les g�nes de cercle valides");
    }
    
    tableauVerif[compteur].setForeground(Color.BLACK);
    tableauVerif[compteur++]
        .setText("test 15: Signalement tirant d'eau sortie des cat�gories par rapport � la profondeur des chenaux sans mar�es");
    tableauVerif[compteur].setForeground(Color.BLUE);

    previousCpt = compteur;
    compteur = verifPassageChenauxSortie(tableauVerif, compteur);
    if (compteur != previousCpt) {

    } else {
      tableauVerif[compteur++]
          .setText("test 15 r�ussi:tirant d'eau sortie: toutes les cat�gories passent les chenaux.");
    }
    
    
    File fichier=new File(this.projet_.getFichier()+".arriv");
  if(!fichier.exists()){
    new BuDialogError(null,SiporImplementation.INFORMATION_SOFT,"Le fichier de g�n�ration est introuvable.\n Veuillez relancer la g�n�ration de navire (onglet G�n�ration)").activate();
    tableauVerif[compteur].setForeground(Color.RED);
    tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
      tableauVerif[compteur++].setText("test 16 �choue: Le fichier de g�n�ration des navires est introuvable.");
    
    return false;
  }
  tableauVerif[compteur].setForeground(Color.BLUE);
  tableauVerif[compteur++].setText("test 16 r�ussi: Le fichier de g�n�ration des navires existe.");
    
  
    
     
    
    

    new BuDialogMessage(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
        "La simulation peut �tre lanc�e, les donn�es sont v�rifi�es.").activate();
    // tous les test ont ete effectuees, le lancement de la simulation peut avoir lieu
    // bouton de validation devient accessible: ce bouton declenche l'onglet simulation
    validation.setEnabled(true);
    return true;
  }

  private String getDefaultFontName() {
    return "Helvetica";
  }

  public int verifPassageChenaux(final JLabel[] _t, int _cpt1) {
    int cpt = _cpt1;
    boolean probleme = false;
    // boolean validiteMethode = true;
    for (int k = 0; k < this.getListeChenal_().getListeChenaux_().size(); k++) {
      probleme = false;
      String descriptifProb = " WARNING: chenal " + this.listeChenal_.retournerChenal(k).getNom_() + " profondeur de "
          + (float) this.listeChenal_.retournerChenal(k).profondeur_;
      for (int l = 0; l < this.categoriesNavires_.listeNavires_.size(); l++) {
        if (this.categoriesNavires_.retournerNavire(l).tirantEauEntree > this.listeChenal_.retournerChenal(k).profondeur_) {
          // validiteMethode = false;
          probleme = true;
          descriptifProb = descriptifProb + getDescCategorie() + this.categoriesNavires_.retournerNavire(l).nom
              + " ne passe pas (tirant eau entr�e=" + (float) this.categoriesNavires_.retournerNavire(l).tirantEauEntree
              + " M)";
          _t[cpt].setForeground(Color.RED);
          _t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));
          _t[cpt++]
              .setText(getStringCategorie() + this.categoriesNavires_.retournerNavire(l).nom
                  + " a un tirant d'eau en entr�e sup�rieur � celui du chenal "
                  + this.listeChenal_.retournerChenal(k).getNom_());

        }

      }
      if (probleme) {
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(), descriptifProb)
            .activate();
      }

    }

    return cpt;
  }

  private String getStringCategorie() {
    return "la cat�gorie ";
  }

  private String getDescCategorie() {
    return "\n- la cat�gorie ";
  }

  public int verifPassageChenauxSortie(final JLabel[] _t, int _cpt) {
    int cpt = _cpt;
    boolean probleme = false;
    // boolean validiteMethode = true;
    for (int k = 0; k < this.getListeChenal_().getListeChenaux_().size(); k++) {
      probleme = false;
      String descriptifProb = " WARNING: chenal " + this.listeChenal_.retournerChenal(k).getNom_() + " profondeur de "
          + (float) this.listeChenal_.retournerChenal(k).profondeur_;
      for (int l = 0; l < this.categoriesNavires_.listeNavires_.size(); l++) {
        if (this.categoriesNavires_.retournerNavire(l).tirantEauSortie > this.listeChenal_.retournerChenal(k).profondeur_) {
          // validiteMethode = false;
          probleme = true;
          descriptifProb = descriptifProb + getDescCategorie() + this.categoriesNavires_.retournerNavire(l).nom
              + " ne passe pas (tirant eau sortie=" + (float) this.categoriesNavires_.retournerNavire(l).tirantEauSortie
              + " m)";
          _t[cpt].setForeground(Color.RED);
          _t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));
          _t[cpt++]
              .setText(getStringCategorie() + this.categoriesNavires_.retournerNavire(l).nom
                  + " a un tirant d'eau en sortie sup�rieur � celui du chenal "
                  + this.listeChenal_.retournerChenal(k).getNom_());

        }

      }
      if (probleme) {
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(), descriptifProb)
            .activate();
      }

    }

    return cpt;
  }

  public  int verifPassageEcluse(final JLabel[] _t, int _cpt1) {
    int cpt = _cpt1;
    boolean probleme = false;
    // boolean validiteMethode = true;
    for (int k = 0; k < this.getListeEcluse_().getListeEcluses_().size(); k++) {
      probleme = false;
      String descriptifProb = " WARNING: �cluse " + this.listeEcluse_.retournerEcluse(k).getNom_() + " largeur de "
          + (float) this.listeEcluse_.retournerEcluse(k).largeur_;
      for (int l = 0; l < this.categoriesNavires_.listeNavires_.size(); l++) {
        if (this.categoriesNavires_.retournerNavire(l).largeurMax > this.listeEcluse_.retournerEcluse(k).largeur_) {
          // validiteMethode = false;
          probleme = true;
          descriptifProb = descriptifProb + getDescCategorie() + this.categoriesNavires_.retournerNavire(l).nom
              + " ne passe pas (largeur=" + (float) this.categoriesNavires_.retournerNavire(l).largeurMax + " m)";
          _t[cpt].setForeground(Color.RED);
          _t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));

          _t[cpt++].setText(getStringCategorie() + this.categoriesNavires_.retournerNavire(l).nom
              + " ne passe pas l'�cluse " + this.listeEcluse_.retournerEcluse(k).getNom_());

        }

      }
      if (probleme) {
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(), descriptifProb)
            .activate();

      }
    }

    return cpt;
  }

  public  int verifPassageEcluseLongueur(final JLabel[] _t, int _cpt1) {
    int cpt = _cpt1;
    boolean probleme = false;
    for (int k = 0; k < this.getListeEcluse_().getListeEcluses_().size(); k++) {
      probleme = false;
      String descriptifProb = " WARNING: �cluse " + this.listeEcluse_.retournerEcluse(k).getNom_() + " longueur de "
          + (float) this.listeEcluse_.retournerEcluse(k).longueur_;
      for (int l = 0; l < this.categoriesNavires_.listeNavires_.size(); l++) {
        if (this.categoriesNavires_.retournerNavire(l).longueurMax > this.listeEcluse_.retournerEcluse(k).longueur_) {
          // validiteMethode = false;
          probleme = true;
          descriptifProb = descriptifProb + getDescCategorie() + this.categoriesNavires_.retournerNavire(l).nom
              + " ne passe pas (longueur=" + (float) this.categoriesNavires_.retournerNavire(l).longueurMax + " m)";
          _t[cpt].setForeground(Color.RED);
          _t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));

          _t[cpt++].setText(getStringCategorie() + this.categoriesNavires_.retournerNavire(l).nom + " de l'�cluse "
              + this.listeEcluse_.retournerEcluse(k).getNom_());

        }

      }
      if (probleme) {
        new BuDialogError(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(), descriptifProb)
            .activate();

      }
    }

    return cpt;
  }

  /**
   * Methode de verification des gares orphelines: cette m�thode v�rifie les gares qui n'ont pas de navires ayant poru
   * gare de d�part et qui ne sont impliqu�e dans aucune topologies...
   */
  public  int suppressionGaresOrphelines(final JLabel[] _t, int _cpt) {
    int cpt = _cpt;
    for (int i = 0; i < this.listeGare_.listeGares_.size(); i++) {
      // etape 1: on part du principe que la gare est orpheline
      boolean orpheline = true;

      // etape 2: verification d'existance dans les categories de navires
      for (int k = 0; k < this.categoriesNavires_.listeNavires_.size() && orpheline; k++) {
        if (this.categoriesNavires_.retournerNavire(k).gareDepart_ == i) {
          orpheline = false;
        }
      }

      // etape 3: verification d'existence dans les cheneaux
      for (int k = 0; k < this.getListeChenal_().getListeChenaux_().size() && orpheline; k++) {
        if (this.listeChenal_.retournerChenal(k).gareAmont_ == i) {
          orpheline = false;
        } else if (this.listeChenal_.retournerChenal(k).gareAval_ == i) {
          orpheline = false;
        }
      }

      // etape 4: verification d'existence des ecluses
      for (int k = 0; k < this.getListeEcluse_().getListeEcluses_().size() && orpheline; k++) {
        if (this.listeEcluse_.retournerEcluse(k).gareAmont_ == i) {
          orpheline = false;
        } else if (this.listeEcluse_.retournerEcluse(k).gareAval_ == i) {
          orpheline = false;
        }
      }

      // etape 5: verification d'existence des cercles d evitages
      for (int k = 0; k < this.getListeCercle_().getListeCercles_().size() && orpheline; k++) {
        if (this.listeCercle_.retournerCercle(k).gareAmont_ == i) {
          orpheline = false;
        } else if (this.listeCercle_.retournerCercle(k).gareAval_ == i) {
          orpheline = false;
        }
      }

      // etape 6: verification des bassins
      for (int k = 0; k < this.listebassin_.listeBassins_.size(); k++) {
        if (this.listebassin_.retournerBassin2(k).gareAmont == i) {
          orpheline = false;
        }
      }

      // testons si il la gare est orpheline: demande a l utilisateur de la supprimer:
      if (orpheline) {
        _t[cpt].setForeground(Color.RED);
        _t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));
        _t[cpt++].setText("la gare " + this.listeGare_.retournerGare(i) + " est une gare orpheline");
        final int reponse = new BuDialogConfirmation(this.getApplication().getApp(), this.getApplication()
            .getInformationsSoftware(), "Voulez-vous supprimer la gare orpheline " + this.listeGare_.retournerGare(i)
            + "?").activate();
        if (reponse == 0) {
          this.listeGare_.suppression(i);
          new BuDialogMessage(this.getApplication().getApp(), this.getApplication().getInformationsSoftware(),
              "la gare orpheline " + this.listeGare_.retournerGare(i) + "a �t� supprim�e avec succ�s.").activate();
          i--;
        }

      }

    }// fin du pour

    return cpt;

  }

  
  
  public   boolean verifGenesCercles() {
      for (int i = 0; i < this.getListeCercle_().getListeCercles_().size(); i++) 
        for (int j = 0; j < this.categoriesNavires_.listeNavires_.size(); j++) 
          for (int k = 0; k < this.categoriesNavires_.listeNavires_.size(); k++) 
          if ((double) this.getListeCercle_().retournerCercle(i).getRegleGenes().retournerAij(j,k) == (double) 0.0) 
            return false;
      return true;
    }

  /**
   * Methode de verification de la coh�rence des don�es saisies poru les dur�es de parcours.
   */
  public   boolean verifDureesParcoursChenal() {
    for (int i = 0; i < this.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < this.categoriesNavires_.listeNavires_.size(); j++) {
        // double val=((Double)vecteurLigne.get(j)).doubleValue();
        if ((float) this.reglesDureesParcoursChenal_.retournerValeur(i, j) == (float) 0.0) {
          return false;
        }
      }

    }

    // arriv� a ce stade il n y a plsu de doute possible; les donn�es sont bioen correctes et nnon vides
    return true;

  }

  /**
   * Methode de verification de la coh�rence des don�es saisies poru les dur�es de parcours.
   */
  public  boolean verifDureesParcoursCercle() {
    for (int i = 0; i < this.getListeCercle_().getListeCercles_().size(); i++) {
      for (int j = 0; j < this.categoriesNavires_.listeNavires_.size(); j++) {
        if (this.reglesDureesParcoursCercle_.retournerValeur(i, j) == 0) {
          return false;
        }
      }

    }

    // arriv� a ce stade il n y a plus de doute possible; les donn�es sont bioen correctes et nnon vides
    return true;

  }

  /**
   * Methode qui teste si le bassin numero k est le bassin d appartenance d un quai.
   * 
   * @param _numBassin numero du bassin
   * @return entier indice du quai qui appartient a ce bassin ou -1 sinon
   */
  public  int quaiAppartientBassin(final int _numBassin) {
    for (int i = 0; i < this.getlQuais_().getlQuais_().size(); i++) {
      if (this.lQuais_.retournerQuais(i).bassin_ == _numBassin) {
        return i;
      }
    }

    return -1;
  }

  /**
   * Metode qui teste si un quai est preferentiel a un navire auquel cas il retourne l indice du navire correpondant.
   * 
   * @param _numQuai indice du quai a tester
   * @return indice du navire possedant ce quai comme preferentiel
   */
  public  int quaiPreferentielNavire(final int _numQuai) {
    for (int i = 0; i < this.categoriesNavires_.listeNavires_.size(); i++) {
      if (this.categoriesNavires_.retournerNavire(i).quaiPreferentiel1 == _numQuai) {
        return i;
      } else if (this.categoriesNavires_.retournerNavire(i).quaiPreferentiel2 == _numQuai) {
        return i;
      } else if (this.categoriesNavires_.retournerNavire(i).quaiPreferentiel3 == _numQuai) {
        return i;
      }
    }

    return -1;
  }

  /**
   * Methode qui place le niveau de s�curite de l'application au plus bas.
   * Consequence: on doit redemarrer le test de v�rification des coh�rences des donn�es.
   * Utiliser: on utilise cette m�thode dans le cas ou l'on modifie des donn�es.
   *
   */
  public void baisserNiveauSecurite(){
    
    //-- lors de la modification de donn�es, on remet le niveau de s�curit� de l'appli au minimum --//
    this.getApplication().baisserNiveauSecurite();
    
  }
  
  /**
   * Idem que precedemment avec nuance.
   * Methode appliqu�e dans le cas de suppression d'�l�ment ou modification de la topologie.
   * Cons�quence suppl�mentaire: reinitialisation du dessin du port
   *
   */
  public void baisserNiveauSecurite2(){
    
    //-- lors de la modification de donn�es, on remet le niveau de s�curit� de l'appli au minimum --//
    this.getApplication().baisserNiveauSecurite();
    
    //-- On reinitialise le dessin du port --//
    this.getApplication().activerModelisation();
    this.getApplication().gestionModelisation_.setVisible(false);
    this.getApplication().gestionModelisation_.initialisationDessin();
  } 
  
  
  
  
  /**
   * Methode de test de g�n�ration d un fichier html.
   */
  public  void testHtml() {

    final String codeHTML = "<body bgcolor=\"#CCCCCC\">"
        + "<p align=\"center\"><font color=\"#000099\" size=\"6\"> Rapport d'erreurs"
        + "<table bgcolor=\"#CCCCCC\" border=\"5\" bordercolor=\"#333399\">" + " <TR><td>l1 </td>"
        + "<td> l2 </img> </td> <td>l3 </td><td> l4 </td></tr><tr><td>l5 </td><td> l6 </td>"
        + "<td>l7 </td> <td> l8 </td> </tr> <tr> <td>l9 </td><td> l10</td><td>l11 </td><td> l12 </td>"
        + "</tr></TABLE> </center></body>";

    BuBrowserFrame fHtml = new BuBrowserFrame(this.getApplication());

    fHtml.setHtmlSource(codeHTML, "rapport erreurs");
    this.getApplication().addInternalFrame(fHtml);

  }

public FudaaProjet getProjet_() {
  return projet_;
}

public void setProjet_(FudaaProjet projet_) {
  this.projet_ = projet_;
}

public SParametresSiporGene getParams_() {
  return params_;
}

public void setParams_(SParametresSiporGene params_) {
  this.params_ = params_;
}

public SiporResultatListeevenementsSimu getListeResultatsSimu_() {
  return listeResultatsSimu_;
}

public void setListeResultatsSimu_(
    SiporResultatListeevenementsSimu listeResultatsSimu_) {
  this.listeResultatsSimu_ = listeResultatsSimu_;
}

public SiporNavires getCategoriesNavires_() {
  return categoriesNavires_;
}

public void setCategoriesNavires_(SiporNavires categoriesNavires_) {
  this.categoriesNavires_ = categoriesNavires_;
}

public SiporListeQuais getlQuais_() {
  return lQuais_;
}

public void setlQuais_(SiporListeQuais lQuais_) {
  this.lQuais_ = lQuais_;
}

public SiporBassins getListebassin_() {
  return listebassin_;
}

public void setListebassin_(SiporBassins listebassin_) {
  this.listebassin_ = listebassin_;
}

public SiporGares getListeGare_() {
  return listeGare_;
}

public void setListeGare_(SiporGares listeGare_) {
  this.listeGare_ = listeGare_;
}

public SiporCercles getListeCercle_() {
  return listeCercle_;
}

public void setListeCercle_(SiporCercles listeCercle_) {
  this.listeCercle_ = listeCercle_;
}

public SiporChenaux getListeChenal_() {
  return listeChenal_;
}

public void setListeChenal_(SiporChenaux listeChenal_) {
  this.listeChenal_ = listeChenal_;
}

public SiporEcluses getListeEcluse_() {
  return listeEcluse_;
}

public void setListeEcluse_(SiporEcluses listeEcluse_) {
  this.listeEcluse_ = listeEcluse_;
}

public SiporStrutureReglesDureesParcours getReglesDureesParcoursChenal_() {
  return reglesDureesParcoursChenal_;
}

public void setReglesDureesParcoursChenal_(
    SiporStrutureReglesDureesParcours reglesDureesParcoursChenal_) {
  this.reglesDureesParcoursChenal_ = reglesDureesParcoursChenal_;
}

public SiporStrutureReglesDureesParcours getReglesDureesParcoursCercle_() {
  return reglesDureesParcoursCercle_;
}

public void setReglesDureesParcoursCercle_(
    SiporStrutureReglesDureesParcours reglesDureesParcoursCercle_) {
  this.reglesDureesParcoursCercle_ = reglesDureesParcoursCercle_;
}

public SiporInternalFrame getFrameVerif_() {
  return frameVerif_;
}

public void setFrameVerif_(SiporInternalFrame frameVerif_) {
  this.frameVerif_ = frameVerif_;
}

public GenarrListeNavires getGenarr_() {
  return genarr_;
}

public void setGenarr_(GenarrListeNavires genarr_) {
  this.genarr_ = genarr_;
}

public SiporImplementation getApplication() {
  return application_;
}

public String getProjectName_() {
  return projectName_;
}

public static void setObservable(SiporObservableSupport observable) {
  SiporDataSimulation.observable = observable;
}

public SiporStrutureReglesRemplissageSAS getReglesRemplissageSAS() {
	return reglesRemplissageSAS_;
}

public void setReglesRemplissageSAS_(
		SiporStrutureReglesRemplissageSAS reglesRemplissageSAS_) {
	this.reglesRemplissageSAS_ = reglesRemplissageSAS_;
}

}
