package org.fudaa.fudaa.sipor.siporNetwork;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.NetworkDaoFactory;
import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.sipor.structures.SiporBassin;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporChenal;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporEcluse;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;

public class SiporNetworkDaoFactory implements NetworkDaoFactory{

	private final SiporDataSimulation donnees ;
	
	private boolean hasChanged = false;
	
	public SiporNetworkDaoFactory(SiporDataSimulation donnees) {
		this.donnees = donnees;
	}
	
	public NetworkUserObject createElement(mxCell element) {
		hasChanged = true;
		NetworkUserObject createdElement = null;
		if(  SimulationNetworkEditor.DEFAULT_VALUE_BASSIN.equals(element.getValue())) {
			String nomBassin = "Bassin " + (donnees.getListebassin_().getListeBassins_().size()+1);
			SiporBassin bassin = new SiporBassin() ;
			bassin.setNom_(nomBassin);
			donnees.getListebassin_().ajout(bassin);
			createdElement = bassin;
		} else if(  SimulationNetworkEditor.DEFAULT_VALUE_GARE.equals(element.getValue())) {
			String nomGare = "Gare " + (donnees.getListeGare_().getListeGares_().size()+1);
			donnees.getListeGare_().ajout(nomGare);
			createdElement = new DefaultNetworkUserObject(nomGare, true);
		} else if(  SimulationNetworkEditor.DEFAULT_VALUE_CHENAL.equals(element.getValue())) {
			SiporChenal chenal =new SiporChenal(donnees.getCategoriesNavires_().getListeNavires_().size());
			chenal.setNom_("Chenal " + (donnees.getListeChenal_().getListeChenaux_().size()+1));
			donnees.getListeChenal_().ajout(chenal);
			donnees.getReglesDureesParcoursChenal_().ajoutLigne(donnees.getCategoriesNavires_().getListeNavires_().size());
			createdElement = chenal;
		}else if(  SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE.equals(element.getValue())) {
			SiporEcluse ecluse =new SiporEcluse();
			ecluse.setNom_("Ecluse " + (donnees.getListeEcluse_().getListeEcluses_().size()+1));
			donnees.getListeEcluse_().ajout(ecluse);
			donnees.getReglesRemplissageSAS().ajoutLigne(donnees.getCategoriesNavires_().getListeNavires_().size());
		    createdElement = ecluse;
		}else if(  SimulationNetworkEditor.DEFAULT_VALUE_CERCLE.equals(element.getValue())) {
			SiporCercle cercle =new SiporCercle(donnees.getCategoriesNavires_().getListeNavires_().size());
			cercle.setNom_("Cercle " + (donnees.getListeCercle_().getListeCercles_().size()+1));
			donnees.getListeCercle_().ajout(cercle);
			donnees.getReglesDureesParcoursCercle_().ajoutLigne(donnees.getCategoriesNavires_().getListeNavires_().size());

			createdElement = cercle;
		}
		donnees.getApplication().updateNetworkData();
		return createdElement;
	}

	public NetworkUserObject deleteElement(mxCell element) {
		hasChanged = true;
		NetworkUserObject deletedElement = null;
		
		if(!(element.getValue() instanceof NetworkUserObject)) {
			return null;
		}
		NetworkUserObject object = (NetworkUserObject) element.getValue();
		if(object instanceof DefaultNetworkUserObject) {
			DefaultNetworkUserObject data = (DefaultNetworkUserObject) object;
			if(data.isNode() && donnees.getListeGare_().getListeGares_().contains(data.getName())) {
				donnees.getListeGare_().getListeGares_().remove(data.getName());
			} else {
				//do nothing...
			}
			deletedElement = data;
		} else 
			if(object instanceof SiporBassin) {
				SiporBassin bassin = (SiporBassin) object;
				int numData = donnees.getListebassin_().getListeBassins_().indexOf(bassin);
				if(numData != -1) {
					donnees.getListebassin_().suppression(numData);	      
				}
				deletedElement = bassin;
			} else 
			if(object instanceof SiporChenal) {
				SiporChenal chenal = (SiporChenal) object;
				int numData = donnees.getListeChenal_().getListeChenaux_().indexOf(chenal);
				if(numData != -1) {
					donnees.getListeChenal_().suppression(numData);	           
					donnees.getReglesDureesParcoursChenal_().SuprimerLigne(numData);
				}
				deletedElement = chenal;
				
			}
			 else 
					if(object instanceof SiporEcluse) {
						SiporEcluse ecluse = (SiporEcluse) object;
						int numData = donnees.getListeEcluse_().getListeEcluses_().indexOf(ecluse);
						if(numData != -1) {
							donnees.getListeEcluse_().suppression(numData);	           
							donnees.getReglesRemplissageSAS().SuprimerLigne(numData);
						}
						deletedElement = ecluse;
					}
					else 
						if(object instanceof SiporCercle) {
							SiporCercle cercle = (SiporCercle) object;
							int numData = donnees.getListeCercle_().getListeCercles_().indexOf(cercle);
							if(numData != -1) {
								donnees.getListeCercle_().suppression(numData);	         
								donnees.getReglesDureesParcoursCercle_().SuprimerLigne(numData);
							}
							deletedElement = cercle;
						}
		donnees.getApplication().updateNetworkData();
		return deletedElement;
	}

	public Object connectElement(mxCell edge, mxCell terminal, boolean unconnect, boolean amont) {
		hasChanged = true;
		//-- terminal must be a node (= gare) or else we have a problem --//
		if(!(terminal.getValue() instanceof DefaultNetworkUserObject) || ! ((DefaultNetworkUserObject)terminal.getValue()).isNode()) {
			return null;
		}
		DefaultNetworkUserObject gare = (DefaultNetworkUserObject)terminal.getValue();
		int indiceGare = donnees.getListeGare_().getListeGares_().indexOf(gare.getName());		
		if(! (edge.getValue() instanceof NetworkUserObject)) {
			return null;
		
		}
			NetworkUserObject object = (NetworkUserObject) edge.getValue();
			if(object instanceof SiporBassin) {
				SiporBassin bassin = (SiporBassin) object;				
			    bassin.setGareAmont(indiceGare);
			} 
			if(object instanceof SiporChenal) {
				SiporChenal chenal = (SiporChenal) object;
				if(amont) {
					chenal.setGareAmont_(unconnect?0:indiceGare);
				} else
					chenal.setGareAval_(unconnect?0:indiceGare);
			} else
				if(object instanceof SiporEcluse) {
					SiporEcluse ecluse = (SiporEcluse) object;
					if(amont) {
						ecluse.setGareAmont_(unconnect?0:indiceGare);
					} else
						ecluse.setGareAval_(unconnect?0:indiceGare);
				} else
					if(object instanceof SiporCercle) {
						SiporCercle cercle = (SiporCercle) object;
						if(amont) {
							cercle.setGareAmont_(unconnect?0:indiceGare);
						} else
							cercle.setGareAval_(unconnect?0:indiceGare);
					}
		return "";
	}


	public Object loadNetworkMapping(mxGraph graph,HashMap<mxCell,NetworkUserObject> mappingUserObject ,HashMap<mxCell, mxCell[]> mappingNetworkConnection ) {
		hasChanged = true;
		mappingUserObject.clear();
		mappingNetworkConnection.clear();
		
		Object[] allCells = graph.getChildCells(graph.getDefaultParent(), true, true);
		
		List<mxCell> listeGare  = new ArrayList<mxCell>();
		
		for(Object data:allCells){
			if(data instanceof mxCell) {
				mxCell cell = (mxCell) data;
				
				if(cell.getValue() instanceof NetworkUserObject) {
					NetworkUserObject userObject = (NetworkUserObject)cell.getValue() ;
					//-- gare --//
					if(userObject instanceof DefaultNetworkUserObject) {
						donnees.getListeGare_().retrouverGare(userObject.getName());
						DefaultNetworkUserObject gare = new DefaultNetworkUserObject(userObject.getName(), true);
						mappingUserObject.put(cell, gare);
						cell.setValue(gare);
						listeGare.add(cell);
					} else if(userObject instanceof SiporChenal) {
						int indiceChenal = donnees.getListeChenal_().retourneIndice(userObject.getName());
						if(indiceChenal>=0){
							mappingUserObject.put(cell, donnees.getListeChenal_().retournerChenal(indiceChenal));
							cell.setValue(donnees.getListeChenal_().retournerChenal(indiceChenal));
						}
					} else if(userObject instanceof SiporEcluse) {
						int indiceEcluse = donnees.getListeEcluse_().retourneIndice(userObject.getName());
						if(indiceEcluse>=0) {
							mappingUserObject.put(cell, donnees.getListeEcluse_().retournerEcluse(indiceEcluse));
							cell.setValue(donnees.getListeEcluse_().retournerEcluse(indiceEcluse));
						}
					} else if(userObject instanceof SiporBassin) {
						int indiceBassin = donnees.getListebassin_().retrouveBassin(userObject.getName());
						if(indiceBassin>=0)	{
							mappingUserObject.put(cell, donnees.getListebassin_().retournerBassin2(indiceBassin));
							cell.setValue(donnees.getListebassin_().retournerBassin2(indiceBassin));
						}
					} else if(userObject instanceof SiporCercle) {
						int indiceCercle = donnees.getListeCercle_().retourneIndice(userObject.getName());
						if(indiceCercle>=0)	 {
							mappingUserObject.put(cell, donnees.getListeCercle_().retournerCercle(indiceCercle));
							cell.setValue(donnees.getListeCercle_().retournerCercle(indiceCercle));
						}
					} 
				}
			}
		}
		
		//-- deduire la topologie depuis les gares --//
		for(mxCell gare:listeGare) {			
			for(int i=0; i< gare.getEdgeCount();i++) {
				if( gare.getEdgeAt(i) instanceof mxCell) {
					mxCell edge = (mxCell) gare.getEdgeAt(i);					
					if( ! mappingNetworkConnection.containsKey(edge)) {
						mxCell[] connections = new mxCell[2];
						mappingNetworkConnection.put(edge, connections);
					}
					mxCell[] connections = mappingNetworkConnection.get(edge);
					//TODO gestion amont=0 et aval=1 � am�liorer
					int indiceGare = donnees.getListeGare_().retrouverGare(((NetworkUserObject)gare.getValue()).getName());
					NetworkUserObject userObjectEdge = (NetworkUserObject)edge.getValue() ;
					int indiceGareAmontUserObject=0;
					if(userObjectEdge instanceof SiporChenal) {
						indiceGareAmontUserObject = ((SiporChenal)userObjectEdge).getGareAmont_();
					} else if(userObjectEdge instanceof SiporEcluse) {
						indiceGareAmontUserObject = ((SiporEcluse)userObjectEdge).getGareAmont_();
					} else if(userObjectEdge instanceof SiporBassin) {
						indiceGareAmontUserObject = ((SiporBassin)userObjectEdge).getGareAmont();
					} else if(userObjectEdge instanceof SiporCercle) {
						indiceGareAmontUserObject = ((SiporCercle)userObjectEdge).getGareAmont_();
					}
					//-- pour savoir si on met en gare amont ou aval, on fait un double check avec les donn�es business -> 0: amont et 1 = aval --//	
					if(indiceGareAmontUserObject == indiceGare)
						connections[0] = gare;
					else 
						connections[1] = gare;
			}
		}
		}		
		return null;
	}

	public boolean hasChanged() {
		
		return hasChanged;
	}

	

}
