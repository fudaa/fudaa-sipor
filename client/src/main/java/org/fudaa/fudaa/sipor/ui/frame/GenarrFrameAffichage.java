package org.fudaa.fudaa.sipor.ui.frame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.GenarrModeleTable;
import org.fudaa.fudaa.sipor.ui.tools.SiporCellEditor;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

public class GenarrFrameAffichage extends SiporInternalFrame{


  SiporDataSimulation donnees_;

  BuTable tableau;


  GenarrModeleTable modeleGenarr_;

  JScrollPane panneauDeroulant_=null;
  boolean typeTrie=true;

  BuPanel commandePanel_=new BuPanel(new FlowLayout(FlowLayout.CENTER));
  private final BuButton sauver_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_enregistrer"), "Enregistrer");
  private final BuButton trier_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Trier");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton valider_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");


  public GenarrFrameAffichage(SiporDataSimulation _d){
    super("", true, true, true, true);
    donnees_=_d;


    //-- creation du modele du jtable --//
    modeleGenarr_=new GenarrModeleTable(donnees_);

    setTitle("Liste des navires g�n�r�s");
    setSize(400, 500);

    //-- mise a jour du tableau --//
    maj();
    //-- Panel des commandes --//
    commandePanel_.add(valider_);
    commandePanel_.add(trier_);
    commandePanel_.add(impression_);
    commandePanel_.add(sauver_);
    
    
    trier_.addActionListener(new ActionListener(){

      public void actionPerformed(ActionEvent e) {

        //-- trier les donn�es du tableau --//
        if(typeTrie){
          donnees_.getGenarr_().trier();
          typeTrie=false;
        }
        else
        {
          typeTrie=true;
          donnees_.getGenarr_().trierNavires();
        }
        //-- mise a jour du contenu du tableau --//
        //maj();
        modeleGenarr_.miseAjour();
      }

    });
    
    sauver_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
      
    donnees_.getGenarr_().ecritureFichierGenarr(donnees_.getProjet_().getFichier());
      }
    });
    valider_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });
    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        tableau.editCellAt(0, 0);
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(GenarrFrameAffichage.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */
          final SiporModeleExcel modele = modeleGenarr_;

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);
            donnees_.getApplication();
            new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, 
                "Fichier Excel g�n�r� avec succ�s.").activate();

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });
    trier_.setToolTipText("Permet de trier les donn�es du tableau selon la cat�gorie ou les navires.");
    sauver_.setToolTipText("permet d'enregistrer les modifications apport�es au fichier.");
    this.impression_.setToolTipText("G�n�re un fichier excel du tableau. Attention, ce bouton ne g�n�re que le tableau du chenal affich�!");
    this.valider_.setToolTipText(SiporConstantes.toolTipQuitter);

    JTableHeader header = tableau.getTableHeader();
    header.setReorderingAllowed(true);


  }

  public void maj(){

    if(panneauDeroulant_!=null)
      this.remove(panneauDeroulant_);




    tableau=new BuTable(modeleGenarr_);//data,titreColonnes);
    //-- editeur par d�faut du tableau --// 
    tableau.setDefaultEditor(Object.class,new SiporCellEditor());
    tableau.getTableHeader().setReorderingAllowed(false);
    
    
    
    JComboBox listeNavires=new JComboBox();
    for(int i=0;i<donnees_.getCategoriesNavires_().getListeNavires_().size();i++)
      listeNavires.addItem(donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    /**
       * Creation pour les colonnes 2 et 3 d'objets de type jcombobox
       */
      final TableColumn typeCol = tableau.getColumnModel().getColumn(1);
      typeCol.setCellEditor(new DefaultCellEditor(listeNavires));
      
    
    
    
    
    
    
    panneauDeroulant_=new JScrollPane(tableau);

    this.setLayout(new BorderLayout());

    this.add(panneauDeroulant_,BorderLayout.CENTER);
    this.add(commandePanel_,BorderLayout.SOUTH);

    //-- remise a jour des composants en cascade --//
    this.validate();
    this.repaint();
  }

}
