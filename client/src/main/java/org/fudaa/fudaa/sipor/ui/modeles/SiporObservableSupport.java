package org.fudaa.fudaa.sipor.ui.modeles;

import java.util.Observable;


/**
 * Classe contenant l'observable, objet qui sera ajout� au modele des donn�es SiporDataSImulation
 * et dans lequelle on enregistrera toutes les vues concern�es (JPanel qui extendent de observer.
 * 
 * Le principe est le suivant: les observers (vues, panels,...) s'enregistrent une fois � l'observable et 
 * lorsque les donn�es sont mises a jour (SiporDataSimulation, partie m�tier) l'observable notifie 
 * l'ensemble des observer en leur envoyant un signal. Cela a pour but de faire appl � la m�thode update() des
 * observer dans laquelle sera �crit le code permettant de mettre � jour les donn�es.
 *@version $Version$
 * @author hadoux
 *
 */
public class SiporObservableSupport extends Observable{

  
  public void notifyObservers() {
    setChanged();
    super.notifyObservers();
  }

  
  public void notifyObservers(Object arg) {
    setChanged();
    super.notifyObservers(arg);
  }

  
}
