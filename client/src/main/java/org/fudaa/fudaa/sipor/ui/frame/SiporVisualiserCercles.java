package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporCercle;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageCercles;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieCercle;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Fenetre principale de saisie et d affichage des bassins
 * 
 * @author Adrien Hadoux
 */

public class SiporVisualiserCercles extends SiporInternalFrame {

  // attributs

  /*
   * Les panels
   */

  JPanel controlePanel_ = new JPanel();

  // panel d affichage global:
  JPanel global_ = new JPanel();

  /**
   * Panel de saisie des diff�rents cercles
   */
  SiporPanelSaisieCercle saisieCerclePanel_;

  /**
   * Panel d affichage des duff�rents cercles sous forme de Jtable necessaire pour recup�rer l indice du cercle
   * selectionn�
   */

  public SiporPanelAffichageCercles affichagePanel_;

  /**
   * Ascenceur pour le jtable
   */
  JScrollPane ascenceur_;
  // composants:

  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modifier");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
  public final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Supprimer");

  public JLabel mode_ = new JLabel("Saisie");

  /**
   * Parametres de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Constructeur de la fenetre prend en parametre les donn�es de la simulation
   * 
   * @param d donn�es de la simulation
   */
  public SiporVisualiserCercles(final SiporDataSimulation d) {
    super("", true, true, true, true);
    donnees_ = d;
    setTitle("Gestion des cercles d'�vitage");
    setSize(530, 340);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
        .createLoweredBevelBorder()));
    this.getContentPane().setLayout(new BorderLayout());

    /**
     * tooltiptext des boutons
     */
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut d'abord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");

    /**
     * ***************************************************************** Gestion des controles de la fenetre
     */

    this.modification_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numCercle = affichagePanel_.tableau_.getSelectedRow();
        if (numCercle == -1) {
          JOptionPane.showMessageDialog(null,
              "Erreur! Vous devez cliquer sur le cercle � modifier dans le tableau d'affichage.",
              MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
        } else {

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          saisieCerclePanel_.MODE_MODIFICATION(numCercle);

        }// fin du if

      }
    });

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        final int numCercle = affichagePanel_.tableau_.getSelectedRow();
        if (numCercle == -1) {
          JOptionPane.showMessageDialog(null,
              "Erreur! Vous devez cliquer sur le cercle � supprimer dans le tableau d'affichage.",
              MessageConstants.WARN, JOptionPane.ERROR_MESSAGE);
        } else {

          // on s occupe de la supresion des cercles:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.getApplication().getApp(),
              SiporImplementation.INFORMATION_SOFT, "Etes-vous certain de vouloir supprimer le cercle "
                  + donnees_.getListeCercle_().retournerCercle(numCercle) + " ?").activate();

          if (confirmation == 0) {
        	  
        	  SiporCercle selected =   donnees_.getListeCercle_().retournerCercle(numCercle);
        	  
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getListeCercle_().suppression(numCercle);
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);

            /**
             * suppression d une ligne de la matrice des dur�es de parcours
             */
            donnees_.getReglesDureesParcoursCercle_().SuprimerLigne(numCercle);

            
            //-- delete element in network --//
            donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_CERCLE,
            		selected);
            
            // --On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserCercles.this.windowClosed();

      }
    });
    /**
     * ****************************************************************** Affichage des composants:
     */

    // affichage des composants:
    global_.setLayout(new GridLayout(2, 1));

    // panel d affichage des donn�es du bassin
    affichagePanel_ = new SiporPanelAffichageCercles(donnees_);
    this.ascenceur_ = new JScrollPane(this.affichagePanel_);
    global_.add(this.ascenceur_);

    // panel de saisie des donn�es du bassin
    this.saisieCerclePanel_ = new SiporPanelSaisieCercle(donnees_, this);

    final JPanel inter = new JPanel();
    inter.setLayout(new GridLayout(1, 3));
    final JPanel affichMode = new JPanel();
    affichMode.add(this.mode_);
    inter.add(affichMode);

    inter.add(this.saisieCerclePanel_);
    inter.add(new JLabel(""));
    inter.setBorder(SiporBordures.compound_);
    global_.add(inter);

    this.getContentPane().add(global_, BorderLayout.CENTER);

    // le panel de controle:

    this.controlePanel_.add(quitter_);
    // this.controlePanel.add(ajout);
    this.controlePanel_.add(modification_);
    this.controlePanel_.add(suppression_);

    this.controlePanel_.setBorder(SiporBordures.compound_);
    this.getContentPane().add(controlePanel_, BorderLayout.SOUTH);

    setVisible(true);

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserCercles.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }

}
