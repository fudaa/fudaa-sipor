/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeOccupGlobale;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */

public class SiporResultatOccupationGlobales extends SiporInternalFrame{

  
	private static final long serialVersionUID = 1L;

/**
   * Tableau contenant la liste des temps de service totaux
   */
  float[] ts_;

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * tableau de checkBox destiner a l utilisateuir pour choisr les navire a visualiser sur les diff�rents supports.
   */
  JCheckBox[] tableauChoixQuais_;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Quai", "Nb Navires Total", "Occup.Dur�e", "Occup.Lin�raire", "TS total" };

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();
  BGraphe histo2_ = new BGraphe();
  BGraphe histo3_ = new BGraphe();
  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes nbnavires
   */
  BuPanel controlPanelHisto2_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes temps service total
   */
  BuPanel controlPanelHisto3_ = new BuPanel();

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * panel de gestion des histogrammes du nb de navires
   */
  BuPanel panelHisto2_ = new BuPanel();
  BuPanel panelHisto3_ = new BuPanel();
  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeQuais_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  final BuButton exportationHisto2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  final BuButton exportationHisto3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter4_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter5_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  boolean seuil2_ = false;
  JTextField valSeuil2_ = new JTextField(6);
  JCheckBox valideSeuil2_ = new JCheckBox("Seuil", false);
  float valeurSeuil2 = 0;

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatOccupationGlobales(final SiporDataSimulation _donnees) {
    super("Taux d'occupation globale des quais", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // calcul des occupations globales

    // tableau contenant les temps de service
    ts_ = SiporAlgorithmeOccupGlobale.determineTStotal(this.donnees_);

    setSize(820, 400);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    this.getContentPane().add(this.optionPanel_, BorderLayout.WEST);
    // ajout du tableau dans le panel tabbed
    panelPrincipal_.addTab("tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipal_.addTab("Graphe occupation", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    panelPrincipal_.addTab("Histo occupation", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    panelPrincipal_.addTab("Histo nb navires", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto2_);

    panelPrincipal_.addTab("Histo TS", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto3_);

    tableauChoixQuais_ = new JCheckBox[this.donnees_.getlQuais_().getlQuais_().size()];
    for (int i = 0; i < this.donnees_.getlQuais_().getlQuais_().size(); i++) {
      this.tableauChoixQuais_[i] = new JCheckBox(this.donnees_.getlQuais_().retournerQuais(i).getNom(), true);
      this.tableauChoixQuais_[i].addActionListener(this);

    }
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(new JLabel("S�lectionnez le quai � visualiser:   "));
    this.controlPanel_.add(this.ListeQuais_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeQuais_.addItem("Tous");
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      this.ListeQuais_.addItem("" + donnees_.getlQuais_().retournerQuais(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeQuais_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeQuais_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatOccupationGlobales.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    final JTabbedPane panoption = new JTabbedPane();
    this.optionPanel_.add(panoption);

    final Box bVert2 = Box.createVerticalBox();
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Affiche quai");
    bVert2.setBorder(bordure1);
    for (int i = 0; i < this.tableauChoixQuais_.length; i++) {
      bVert2.add(this.tableauChoixQuais_[i]);
    }

    final JScrollPane pcnasc = new JScrollPane(bVert2);
    panoption.addTab("Quais:", pcnasc);

    this.optionPanel_.setBorder(this.compound_);

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    this.valSeuil2_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil2_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil2_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil2_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil2_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil2_.setText("");
          }
        }
      }
    });
    valideSeuil2_.addActionListener(this);

    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelCourbes_.add(valSeuil2_);
    this.controlPanelCourbes_.add(valideSeuil2_);

    /*******************************************************************************************************************
     * gestion du panel histogramme des occupations
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);

    /*******************************************************************************************************************
     * gestion du panel histogramme nb navires
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto2_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto2 = this.affichageHistogramme2();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo2_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto2.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto2_.add(this.histo2_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto2_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto2_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo2_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto2_.add(quitter4_);
    this.controlPanelHisto2_.add(exportationHisto2_);
    this.panelHisto2_.add(this.controlPanelHisto2_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme Temps service
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto3_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto3 = this.affichageHistogramme3();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo3_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto3.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto3_.add(this.histo3_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto3_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto3_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo2_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto3_.add(quitter5_);
    this.controlPanelHisto3_.add(exportationHisto3_);
    this.panelHisto3_.add(this.controlPanelHisto3_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatOccupationGlobales.this.windowClosed();

      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);
    this.quitter4_.addActionListener(actionQuitter);
    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatOccupationGlobales.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.getlQuais_().getlQuais_().size()][5];

    if (val < 0)// afficher tout l historique
    {
      int indiceQuai = 0;
      for (int i = 0; i < this.donnees_.getlQuais_().getlQuais_().size(); i++) {
        if (this.tableauChoixQuais_[i].isSelected()) {
          data[indiceQuai][0] = "" + this.donnees_.getlQuais_().retournerQuais(i).getNom();
          data[indiceQuai][1] = ""
              + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].nombreNaviresTotal);
          data[indiceQuai][2] = ""
              + (int) ((this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].occupationDuree/FonctionsSimu.NOMBRE_SIMULATIONS) * 100)
              + "."
              + (((int) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].occupationDuree/FonctionsSimu.NOMBRE_SIMULATIONS * 10000)) % 100)
              + "%";
          data[indiceQuai][3] = ""
              + (int) ((this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].occupationLineaire/FonctionsSimu.NOMBRE_SIMULATIONS) * 100)
              + "."
              + (((int) (this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[i].occupationLineaire/FonctionsSimu.NOMBRE_SIMULATIONS * 10000)) % 100)
              + "%";
          data[indiceQuai][4] = "" + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(ts_[i]/FonctionsSimu.NOMBRE_SIMULATIONS);
          indiceQuai++;
        }
      }

    } else if (val < this.donnees_.getlQuais_().getlQuais_().size()) {
      data = new Object[1][5];
      data[0][0] = "" + this.donnees_.getlQuais_().retournerQuais(val).getNom();
      data[0][1] = ""
          + FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[val].nombreNaviresTotal);
      data[0][2] = ""
          + (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[val].occupationDuree/FonctionsSimu.NOMBRE_SIMULATIONS;
      data[0][3] = ""
          + (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[val].occupationLineaire/FonctionsSimu.NOMBRE_SIMULATIONS;
      data[0][4] = "" + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes2(ts_[val]/FonctionsSimu.NOMBRE_SIMULATIONS);
    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  String affichageGraphe() {

    int indiceQuai = 0;

    final int nbQuais = this.donnees_.getlQuais_().getlQuais_().size();
    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Occupation globale \"\n";
    g += "  sous-titre \" Occupation � la dur�e et lin�aire \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" quais \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbQuais + 3)// this.donnees_.getCategoriesNavires_().getListeNavires_().size()
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" " + "\"\n";
    g += "    unite \" % \"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + 1
    // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
    // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    /*
     * PLUSIEURS COURBES
     */
    g += "  courbe\n  {\n";
    g += "    titre \"" + "occupation lin�aire"
    /*
     */
    + "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    trace lineaire\n";
    g += "    marqueurs oui\n";
    g += "    aspect\n {\n";
    g += "      contour.couleur " // choix entre "3366FF", "FFCC00", "FF0000", "009900"
        + "3366FF" + " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceQuai = 0;
    for (int n = 0; n < this.donnees_.getlQuais_().getlQuais_().size(); n++) {
      if (this.tableauChoixQuais_[n].isSelected()) {

        g += (indiceQuai + 1)// numero du quai
            + " "
            + (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[n].occupationLineaire/FonctionsSimu.NOMBRE_SIMULATIONS 
            + "\n";
        indiceQuai++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // }//fin du for
    g += "  courbe\n  {\n";
    g += "    titre \"" + "occupation � la dur�e"
    + "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    trace lineaire\n";
    g += "    marqueurs oui\n";
    g += "    aspect\n {\n";
    g += "      contour.couleur " // choix entre "3366FF", "FFCC00", "FF0000", "009900"
        + "99CC22" + " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceQuai = 0;
    for (int n = 0; n < this.donnees_.getlQuais_().getlQuais_().size(); n++) {
      if (this.tableauChoixQuais_[n].isSelected()) {

        g += (indiceQuai + 1)// numero du quai
            + " "
            + (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[n].occupationDuree/FonctionsSimu.NOMBRE_SIMULATIONS 
            + "\n";
        indiceQuai++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    if (seuil2_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      g += " type max\n";
      g += " valeur " + valeurSeuil2 + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    // int indiceQuai=0;

    int indiceQuai = 0;

    final int nbQuais = this.donnees_.getlQuais_().getlQuais_().size();
    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Occupation globale \"\n";
    g += "  sous-titre \" Occupation � la dur�e et lin�aire \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" quai \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbQuais + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" " + "\"\n";
    g += "    unite \"" + " %" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + 1
    // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
    // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    /*
     * PLUSIEURS COURBES
     */
    g += "  courbe\n  {\n";
    g += "    titre \"" + "occupation lin�aire"
    + "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceQuai = 0;
    for (int n = 0; n < this.donnees_.getlQuais_().getlQuais_().size(); n++) {
      if (this.tableauChoixQuais_[n].isSelected()) {

        g += (indiceQuai + 1)// numero du quai
            + " "
            + (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[n].occupationLineaire/FonctionsSimu.NOMBRE_SIMULATIONS 
            + "\n etiquette  \n \"" + this.donnees_.getlQuais_().retournerQuais(n).getNom() + "\" \n"/* + */
            + "\n";

        indiceQuai++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"" + "occupation � la dur�e"
    /*
     * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
     */
    + "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur 99CC22 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceQuai = 0;
    for (int n = 0; n < this.donnees_.getlQuais_().getlQuais_().size(); n++) {
      if (this.tableauChoixQuais_[n].isSelected()) {

        g += (indiceQuai + 1)// numero du quai
            + " "
            + (float) this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[n].occupationDuree/FonctionsSimu.NOMBRE_SIMULATIONS
            + "\n";
        indiceQuai++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  String affichageHistogramme2() {

    // int indiceQuai=0;

    int indiceQuai = 0;

    final int nbQuais = this.donnees_.getlQuais_().getlQuais_().size();
    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Nombre de navires passant au quai \"\n";
    g += "  sous-titre \" \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" quai \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbQuais + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" " + "\"\n";
    g += "    unite \"" + " navires" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + FonctionsSimu.diviserSimu(SiporAlgorithmeOccupGlobale.determineMaxNbNavires(donnees_))
    // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    /*
     * PLUSIEURS COURBES
     */
    g += "  courbe\n  {\n";
    g += "    titre \"" + "Nombre de navires"
    + "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceQuai = 0;
    for (int n = 0; n < this.donnees_.getlQuais_().getlQuais_().size(); n++) {
      if (this.tableauChoixQuais_[n].isSelected()) {

        g += (indiceQuai + 1)// numero du quai
            + " "
            + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationGlobal[n].nombreNaviresTotal )// tableau
                                                                                                                      // qui
                                                                                                                      // contient
                                                                                                                      // le
                                                                                                                      // nombre
                                                                                                                      // de
                                                                                                                      // navire
                                                                                                                      // pour
                                                                                                                      // chaque
                                                                                                                      // cat�gorie
            + "\n etiquette  \n \"" + this.donnees_.getlQuais_().retournerQuais(n).getNom() + "\" \n"/* + */
            + "\n";

        indiceQuai++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";
    return g;
  }

  String affichageHistogramme3() {

    // int indiceQuai=0;

    int indiceQuai = 0;

    final int nbQuais = this.donnees_.getlQuais_().getlQuais_().size();
    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Temps de Service total � chaque quai \"\n";
    g += "  sous-titre \" \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" " + "\"\n";
    g += "    unite \" quai \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbQuais + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+1)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" " + "\"\n";
    g += "    unite \"" + " Heures.Minutes" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(SiporAlgorithmeOccupGlobale.determineMaxTStotal(ts_)/FonctionsSimu.NOMBRE_SIMULATIONS)
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    g += "  courbe\n  {\n";
    g += "    titre \"" + "Nombre de navires"
    + "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";
    indiceQuai = 0;
    for (int n = 0; n < this.donnees_.getlQuais_().getlQuais_().size(); n++) {
      if (this.tableauChoixQuais_[n].isSelected()) {

        g += (indiceQuai + 1)// numero du quai
            + " " + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes(ts_[n]/FonctionsSimu.NOMBRE_SIMULATIONS) // tableau qui contient le nombre de
                                                                                // navire pour chaque cat�gorie
            + "\n etiquette  \n \"" + this.donnees_.getlQuais_().retournerQuais(n).getNom() + "\" \n"/* + */
            + "\n";

        indiceQuai++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";
    return g;
  }

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixQuais_.length && !trouve; k++) {
      if (source == this.tableauChoixQuais_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        final String descriptionHisto2 = this.affichageHistogramme2();
        this.histo2_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto2.getBytes()));
        final String descriptionHisto3 = this.affichageHistogramme3();
        this.histo3_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto3.getBytes()));

        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    if (source == this.valideSeuil_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    if (source == this.valideSeuil2_) {
      if (this.valideSeuil2_.isSelected() && !this.valSeuil2_.getText().equals("")) {
        // booleen passe a true
        this.seuil2_ = true;
        // on recupere al valeure du Seuil2 choisie par l utilisateur
        valeurSeuil2 = Float.parseFloat(this.valSeuil2_.getText());
        // on redesssinne l histogramme en tenant compte du Seuil2 de l utilisateur
        final String descriptionHisto = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil2_ = false;
        // on redesssinne l histogramme en tenant compte du Seuil2 de l utilisateur
        final String descriptionHisto = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
