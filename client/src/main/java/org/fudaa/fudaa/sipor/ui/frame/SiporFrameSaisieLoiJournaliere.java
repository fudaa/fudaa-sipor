/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */

package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;

import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.CoupleLoiDeterministe;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.LoiJournaliereTableModel;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Panel de saisie des couples pour la loi deterministe presentation souss forme de tableau: ajout , mise a jour,
 * affichage en temps reel une fois de plus....
 * 
 * @author Adrien Hadoux
 */
public class SiporFrameSaisieLoiJournaliere extends SiporInternalFrame {

  /**
   * JCombo qui permettra de choisir pour chaque chenal les gares amont et gares avales
   */
  JComboBox ComboGare;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes = { "Horaire" };

  /**
   * Tableau de type JTable qui contiendra les donn�es des bassins
   */

  JTable tableau;

  /**
   * Bouton de validation des donn�es topolgiques saisies pour le chenal
   */
  JButton validation = new JButton("Valider");

  /**
   * Fenetre qui contiendra le panel
   */
  JPanel global = new JPanel();

  /**
   * Bordure du tableau
   */

  Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la loi deterministe
   */
  ArrayList loiJournaliere_ = new ArrayList();

  SiporDataSimulation donnees_;

  /**
   * composant destin� a recevoir el focus desla destruction de la fenetre
   */

  Component composant_ = null;

private LoiJournaliereTableModel model;

  
  public SiporFrameSaisieLoiJournaliere(final SiporDataSimulation _donnees, final ArrayList _loiDeterministe,
	      final Component c) {
	  	this(_donnees,_loiDeterministe, c, null, null);
  }
  
  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   */
  public SiporFrameSaisieLoiJournaliere(final SiporDataSimulation _donnees, final ArrayList _loiJournaliere, final Component c,
		  final String comments, final Dimension d) {
    super("", true, true, true, true);
    // recuperation des donn�es de la simulation
    this.loiJournaliere_ = _loiJournaliere;
    composant_ = c;
    donnees_ = _donnees;
    global.setLayout(new BorderLayout());

    model = new LoiJournaliereTableModel(_loiJournaliere, _donnees);
    
    // remplissage des comboBox en onction des donn�es
    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();

    // listener du bouton de validation

    this.validation.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
/*
        tableau.editCellAt(299, 1);
        tableau.editingCanceled(null);
        miseAjourSaisieDeterministe();
*/
    	  new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "les donn�es ont �t� correctement saisies: " + loiJournaliere_.size() + " couples saisis! ").activate();

          dispose();
      }

    });

    /**
     * Creation de la fenetre
     */

    setTitle("saisie de la loi journali�re");
    if( d != null) {
    	setSize(d);
    }else
    setSize(250, 300);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    
    getContentPane().setLayout(new BorderLayout());

    final JScrollPane ascenceur = new JScrollPane(global);

    getContentPane().add(ascenceur, BorderLayout.CENTER);

    final JPanel controlPanel = new JPanel();
    controlPanel.add(validation);
    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    if(comments != null) {
    	JLabel label = new JLabel(comments);
    	label.setForeground(Color.blue);
    	label.setBackground(Color.white);
    	getContentPane().add(label, BorderLayout.NORTH);
    }
    
    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {

  }

  /**
   * Methode d affichage des composants du jtable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

  /*  final Object[][] ndata = new Object[300][1];


    // affichage par defaut des dernieres couples saisis:
    for (int i = 0; i < this.loiJournaliere_.size(); i++) {

      // ndata[i][0]=""+((CoupleLoiDeterministe)this.loiDeterministe_.get(i)).jour_;
      ndata[i][0] = "" + (float) ((CoupleLoiDeterministe) this.loiJournaliere_.get(i)).getTemps_();

    }
    */
	  this.tableau = new JTable(model) ;
	    
    
    

    //this.tableau = new JTable(ndata, this.titreColonnes) {
      /*
       * public boolean isCellEditable(int row,int col ){ return false; }
       */
   
    tableau.setBorder(this.borduretab);

    tableau.revalidate();
    // this.removeAll();
    this.global.add(/* ascenceur */tableau.getTableHeader(), BorderLayout.PAGE_START);
    this.global.add(tableau, BorderLayout.CENTER);

    final JPanel controlePanel = new JPanel();
    controlePanel.add(validation);

    global.add(controlePanel, BorderLayout.SOUTH);
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de verifier la pertinence des donn�es saisies: IE: v�rifie que les gares choisies en amont et
   * avales sont bien diff�rentes pour un m�me chenal
   * 
   * @return true si les donn�es sont coh�rentes sinon retourne false et surtout indique a quel endroit se situe
   *         l'erreur de logique de la saisie
   */
  boolean verificationCoherence() {

    String horaire = "";
    for (int i = 0; i < 300; i++) {
      if ((String) this.tableau.getModel().getValueAt(i, 0) != null) {
        horaire = (String) this.tableau.getModel().getValueAt(i, 0);
      } else {
        horaire = "";
      }

      if (!horaire.equals("")) {
        // test si le nombre entr�e est bien un nombre r�el:
        try {
          final float nh = Float.parseFloat(horaire);
          if (nh < 0) {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Erreur! L'horaire ligne "
                + i + " est n�gatif!").activate();
            return false;
          } else if (nh > 24) {
            // le nombre est positif et inferieur a 24
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "Erreur! L'horaire ligne "
                + i + " est invalide: sup�rieur � 24!").activate();
            return false;
          } else {
            // on ajoute al donn�e au vecteur ou on modifie selon le cas
            if (i >= this.loiJournaliere_.size()) {
              this.loiJournaliere_.add(new CoupleLoiDeterministe(1, nh));
            } else {
              this.loiJournaliere_.set(i, new CoupleLoiDeterministe(1, nh));
            }

          }
        } catch (final NumberFormatException nfe) {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT, "  l'horaire ligne " + i
              + " saisi n'existe pas!").activate();
          return false;
        }

      }// if les mots sont non vides
      else {
        new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
            "les horaires ont �t� correctement saisis: \n" + i + " horaires saisis! ").activate();
        dispose();

        if (this.composant_ != null) {
          this.composant_.requestFocus();
        }

        return true;

      }
    }

    // arriv� � ce stade de la m�thode , tous les test de non coh�rence ont �chou�, il suit que
    // les donn�es saisies sont bien coh�rentes et l'on peut les ajouter aux cheneaux.
    new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
        "les donn�es ont �t� correctement saisies: \n" + this.loiJournaliere_.size() + " horaires saisis!")
        .activate();
    dispose();
    return true;
  }

  /**
   * Methode qui permet de mettre a jour les gares saiies en amont et vaales pour chacun des cheneaux: v�rifie dans un
   * premier temps l coh�rence des donn�es:
   */

  void miseAjourSaisieDeterministe() {

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */
    this.verificationCoherence();

  }

}
