package org.fudaa.fudaa.sipor.ui.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluHtmlEditorPanel;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.html.SiporGenereNoteHtml;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe qui g�n�re un rappel des diff�rentes donn�es.
 * 
 * @version $Version$
 * @author hadoux
 */
//FIXME Fred ActionListener deja implemente par SiporInternalFrame
public class SiporFrameGenerationRappelDonnees extends SiporInternalFrame implements TreeSelectionListener {

  private static final String CHENAUX = " chenaux";

  private static final String BASSINS = " bassins";

  private static final String GARES = " gares";

  private static final String MAREE = " mar�e";

  private static final String QUAIS = " quais";

  private static final String ECLUSES = " �cluses";

  private static final String DUREES_DE_PARCOURS_CHENAUX = "dur�es de parcours chenaux";

  private static final String DUREES_DE_PARCOURS_CERCLES_EVITAGE = "dur�es de parcours cercles d'�vitage";

  private static final String CROISEMENTS_CERCLES_EVITAGE = "croisements cercles d'�vitage";

  private static final String CROISEMENTS_CHENAUX = "croisements chenaux";

  private static final String MODELE_DU_PORT = "mod�le du port";

  private static final String TOPOLOGIE_ECLUSES = "topologie �cluses";

  private static final String TOPOLOGIE_CERCLES_EVITAGE = "topologie cercles d'�vitage";

  private static final String TOPOLOGIE_CHENAUX = "topologie chenaux";

  private static final String TOPOLOGIE_BASSINS = "topologie bassins";

  private static final String EVITAGE = " cercles d'�vitage";

  private static final String SAISIE = " saisie";

  private static final String GENERALITES = "g�n�ralit�s";

  private static final String PRESENTATION = "pr�sentation";

  private static final String CAT�GORIES_DE_NAVIRES = " cat�gories de navires";

  SiporDataSimulation donnees_;

  /**
   * arbre hierarchique contenant les infos a mettre en valeur
   */
  JTree arbre_;


  final JScrollPane visionneurArbre;
  /**
   * Editeur de texte html
   */
  JEditorPane htmlPane;

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton exportationHTML_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"),
  "Exportation rapport");
  private final BuButton choisirCouleur_ = new BuButton("couleur sommaire");
  private final BuButton apercu_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_previsualiser"),"Aper�u");
  /**
   * panel contenant les boutons de commande.
   */
  BuPanel controlPanel_ = new BuPanel();

  
  public String fichierImagePort_="";
  
  
  /**
   * panel contenant les informations � selectionner ce panel se modifie en fonction du choix de l'utilisateur dans la
   * hierarchie de l'arbre
   */
  BuPanel selectionPanel_ = new BuPanel();
  JSplitPane conteneur;
  // liste des composants de personalisation du rapport
  public JTextField auteur_ = new JTextField(20);
  public JTextField titre_ = new JTextField(20);
  public JTextArea ZoneText_ = new JTextArea(2, 30);

  public JCheckBox dg_ = new JCheckBox("Ins�rer les donne�s g�n�rales", true);
  public JCheckBox dgNbJ_ = new JCheckBox("Nombre de jours de la simulation", true);
  public JCheckBox dgJour_ = new JCheckBox("Jour de d�part", true);
  public JCheckBox dgGraine_ = new JCheckBox("Graine d'initialisation", true);
  public JCheckBox dgPiedPil_ = new JCheckBox("Pourcentage du pied de pilote", true);

  public JCheckBox gare_ = new JCheckBox("Ins�rer les gares", true);
  public JCheckBox bassin_ = new JCheckBox("Ins�rer les bassins", true);
  public JCheckBox cercle_ = new JCheckBox("Ins�rer les cercles", true);

  public JCheckBox topo1_ = new JCheckBox("Ins�rer les topologies bassins", true);
  public JCheckBox topo2_ = new JCheckBox("Ins�rer les topologies chenaux", true);
  public JCheckBox topo3_ = new JCheckBox("Ins�rer les topologies cercles d'�vitage", true);
  public JCheckBox topo4_ = new JCheckBox("Ins�rer les topologies �cluses", true);
  public JCheckBox topo5_ = new JCheckBox("Ins�rer le modele du port", true);

  public JCheckBox regle1_ = new JCheckBox("Ins�rer les r�gles de croisement chenaux", true);
  public JCheckBox regle2_ = new JCheckBox("Ins�rer les r�gles de croisement cercles", true);
  public JCheckBox regle3_ = new JCheckBox("Ins�rer les durees de parcours chenaux", true);
  public JCheckBox regle4_ = new JCheckBox("Ins�rer les durees de parcours cercles", true);
  public JCheckBox regle5_ = new JCheckBox(SiporConstantes.ConstanteTitreRapportSwing, true);

  public JCheckBox nav_ = new JCheckBox("Ins�rer les cat�gories de navire", true);
  public JCheckBox navPrio_ = new JCheckBox("Priorit�", true);
  public JCheckBox navGare_ = new JCheckBox("Gare de d�part", true);
  public JCheckBox navLong_ = new JCheckBox("Longueur", true);
  public JCheckBox navLarg_ = new JCheckBox("Largeur", true);
  public JCheckBox navTE_ = new JCheckBox("Tirant d eau", true);
  public JCheckBox navQ1_ = new JCheckBox("Quai pr�f�rentiel 1", true);
  public JCheckBox navQ2_ = new JCheckBox("Quai pr�f�rentiel 2", true);
  public JCheckBox navQ3_ = new JCheckBox("Quai pr�f�rentiel 3", true);
  public JCheckBox navCr_ = new JCheckBox("Cr�neaux", false);
  public JCheckBox navCrPM_ = new JCheckBox("creneaux Pleine Mer", false);
  public JCheckBox navMode_ = new JCheckBox("mode de chargement", false);
  public JCheckBox navLoi_ = new JCheckBox("tirant d eau", false);

  public  JCheckBox chenal_ = new JCheckBox("Ins�rer les chenaux", true);
  public JCheckBox chenalPro_ = new JCheckBox("Profondeur du chenal", true);
  public JCheckBox chenalMar_ = new JCheckBox("Soumis a la maree", true);
  public JCheckBox chenalCr_ = new JCheckBox("Ins�rer les cr�neaux", true);

  public  JCheckBox ecluse_ = new JCheckBox("Ins�rer les �cluses", true);
  public JCheckBox ecltaille_ = new JCheckBox("Caract�ristique de l'�cluse", true);
  public JCheckBox eclDur_ = new JCheckBox("Dur�e d'�clus�e, fausse bassin�", true);
  public JCheckBox ecluseIndispo_ = new JCheckBox("les indisponibilit�s", true);
  public JCheckBox ecluseCr_ = new JCheckBox("Ins�rer les cr�neaux", true);

  public JCheckBox quai_ = new JCheckBox("Ins�rer les quais", true);
  public JCheckBox quaitaille_ = new JCheckBox("Caract�ristique du quai", true);
  public JCheckBox quaiIndispo_ = new JCheckBox("Les indisponibilit�s", true);
  public JCheckBox quaiCr_ = new JCheckBox("Ins�rer les cr�neaux", true);
  public JCheckBox maree_ = new JCheckBox("Ins�rer les mar�es", true);

  public Color couleurSommaire;
  public String couleurTableau = "#AACCEE";
  public String couleurLegende = "#EEEEEE";


  public CtuluHtmlEditorPanel editeur=new CtuluHtmlEditorPanel();
  /**
   * constructeur de la classe
   * 
   * @param _donnees
   */

  public SiporFrameGenerationRappelDonnees(final SiporDataSimulation _donnees) {
    super("Rappel des donn�es", true, true, true, true);

    donnees_ = _donnees;
    this.setSize(720, 630);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    /*******************************************************************************************************************
     * Affichage de l'arbre et de son contenu
     ******************************************************************************************************************/
    final DefaultMutableTreeNode sommet = new DefaultMutableTreeNode("Contenu de la note");
    arbre_ = new JTree(sommet);

    DefaultMutableTreeNode categorie;
    DefaultMutableTreeNode feuille;
    // premiere categorie de l arbre
    categorie = new DefaultMutableTreeNode(PRESENTATION);
    sommet.add(categorie);
    categorie = new DefaultMutableTreeNode(GENERALITES);
    sommet.add(categorie);

    categorie = new DefaultMutableTreeNode(SAISIE);
    sommet.add(categorie);
    feuille = new DefaultMutableTreeNode(GARES);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(BASSINS);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(CHENAUX);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(EVITAGE);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(ECLUSES);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(QUAIS);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(MAREE);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(CAT�GORIES_DE_NAVIRES);
    categorie.add(feuille);
    categorie = new DefaultMutableTreeNode("topologie du port");
    sommet.add(categorie);
    feuille = new DefaultMutableTreeNode(TOPOLOGIE_BASSINS);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(TOPOLOGIE_CHENAUX);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(TOPOLOGIE_CERCLES_EVITAGE);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(TOPOLOGIE_ECLUSES);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(MODELE_DU_PORT);
    categorie.add(feuille);
    categorie = new DefaultMutableTreeNode("R�gles de navigation");
    sommet.add(categorie);
    feuille = new DefaultMutableTreeNode(CROISEMENTS_CHENAUX);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(CROISEMENTS_CERCLES_EVITAGE);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(DUREES_DE_PARCOURS_CHENAUX);
    categorie.add(feuille);
    feuille = new DefaultMutableTreeNode(DUREES_DE_PARCOURS_CERCLES_EVITAGE);
    categorie.add(feuille);

    arbre_.expandRow(0);
    // ajout de l arbre dans un ascenceur
    visionneurArbre = new JScrollPane(arbre_);
    selectionPanel_.add(visionneurArbre, BorderLayout.WEST);

    // listener de l'arbre
    arbre_.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    arbre_.addTreeSelectionListener(this);

    /*******************************************************************************************************************
     * Affichage du panel de boutons
     ******************************************************************************************************************/
    this.quitter_.setToolTipText("cliquez sur ce bouton pour fermer la sous fen�tre");
    this.quitter_.addActionListener(this);
    exportationHTML_.setToolTipText("permet de g�n�rer les donn�es saisies au format HTML");
    exportationHTML_.setEnabled(false);
    apercu_.setToolTipText("Aper�u du rapport g�n�r� en html");
    exportationHTML_.addActionListener(this);
    apercu_.addActionListener(this);
    choisirCouleur_.addActionListener(this);
    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporFrameGenerationRappelDonnees.this.windowClosed();

      }
    });

    controlPanel_.add(quitter_);
    controlPanel_.add(apercu_);
    controlPanel_.add(exportationHTML_);
    
    // controlPanel_.add(this.choisirCouleur_);
    this.add(controlPanel_, BorderLayout.SOUTH);
    /*******************************************************************************************************************
     * remplissage panneau central
     ******************************************************************************************************************/
    // actualisation du panneau de donn�es
    auteur_.setText("Inconnu");
    this.titre_.setText("simulation d'exploitation portuaire");
    
    
    //conteneur.add(selectionPanel_);
    
    
    //-- incrustation de l'editeur dans le panel principal --//
    JPanel conteneurEditeur=new JPanel(new BorderLayout());
    
    //ajout du panel d'affichage de l'editeur
    conteneurEditeur.add(editeur,BorderLayout.CENTER);
    //ajout de la toolbar de l'editeur
    conteneurEditeur.add(editeur.getToolBar(true), BuBorderLayout.NORTH);
    //conteneur.add(conteneurEditeur);
    conteneurEditeur.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Affichage du rapport"));
    //ajout de la barre de menu de l'editeur
    setJMenuBar(editeur.getMenuBar());
    
    
    //-- splitpane --//
    conteneur = new JSplitPane(JSplitPane.VERTICAL_SPLIT, selectionPanel_,conteneurEditeur);
    conteneur.setDividerLocation(210);
    conteneur.setDividerSize(1);
    this.getContentPane().add(conteneur, BorderLayout.CENTER);
    actualiser(PRESENTATION);

  }

  /**
   * Methode d'actualisation des donn�es.
   */

  public void actualiser(final String choix) {

    TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
    "personalisation");
    final JPanel table1 = new JPanel();
    final JPanel table2 = new JPanel();
    final JPanel table3 = new JPanel();
    final JPanel table4 = new JPanel();
    final JPanel table5 = new JPanel();
    final JPanel table6 = new JPanel();
    final JPanel table7 = new JPanel();
    final JPanel table8 = new JPanel();
    final JPanel table9 = new JPanel();
    final JPanel table10 = new JPanel();
    final JPanel table11 = new JPanel();
    final JPanel table12 = new JPanel();
    final JPanel table13 = new JPanel();

    String titre = "truc";
    this.selectionPanel_.removeAll();
    this.selectionPanel_.setBorder(BorderFactory.createTitledBorder(SiporBordures.compound_, "Param�tres du rapport"));
    // this.selectionPanel_=new BuPanel();
    this.selectionPanel_.setLayout(new BorderLayout());
    selectionPanel_.add(visionneurArbre, BorderLayout.WEST);
    // format tableau pour affichage des caract�ristiques
    Box table = Box.createVerticalBox();
    this.selectionPanel_.add(new JScrollPane(table), BorderLayout.CENTER);


    dg_.addActionListener(this);
    nav_.addActionListener(this);
    bassin_.addActionListener(this);
    cercle_.addActionListener(this);
    chenal_.addActionListener(this);
    ecluse_.addActionListener(this);
    quai_.addActionListener(this);

    if (choix.equals(GENERALITES)) {

      //table.setLayout(new GridLayout(10, 1));
      table.add(dg_);
      table.add(dgNbJ_);
      table.add(dgJour_);
      table.add(dgGraine_);
      table.add(dgPiedPil_);

      bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
          "donn�es g�n�rales");
      table.setBorder(bordurea);
    } else if (choix.equals(MAREE)) {

      titre = "Param�tres mar�e";
      //table.setLayout(new GridLayout(5, 1));
      table.add(table1);
      table.add(table2);
      table.add(table3);
      table.add(table4);
      table.add(table5);
      table1.add(maree_, BorderLayout.CENTER);
      bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
      table1.setBorder(bordurea);

    } else if (choix.equals(PRESENTATION))// panel de selection des gares
    {
      titre = "presentation";

      //table.setLayout(new GridLayout(5, 1));

      table1.add(new JLabel("Titre: "));
      table1.add(this.titre_);
      table.add(table1);

      table2.add(new JLabel("Auteur:"));
      table2.add(this.auteur_);
      table.add(table2);
      table3.add(new JLabel("Commentaire:"));
      table3.add(ZoneText_);
      table.add(table3);
      bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
      table.setBorder(bordurea);

    } else if (choix.equals(GARES))// panel de selection des gares
    {
      titre = "param�tres gares";
      //table.setLayout(new GridLayout(5, 1));
      table.add(table1);
      table.add(table2);
      table.add(table3);
      table.add(table4);
      table.add(table5);
      table1.add(gare_, BorderLayout.CENTER);
      bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
      table1.setBorder(bordurea);

    } else if (choix.equals(BASSINS))// panel de selection des gares
    {
      titre = "param�tres bassins";
      //table.setLayout(new GridLayout(5, 1));
      table.add(table1);
      table.add(table2);
      table.add(table3);
      table.add(table4);
      table.add(table5);
      table1.add(bassin_, BorderLayout.CENTER);
      bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
      table1.setBorder(bordurea);

    } else if (choix.equals(CHENAUX)) {

      //table.setLayout(new GridLayout(10, 1));
      table.add(chenal_);
      table.add(chenalPro_);
      table.add(chenalMar_);
      table.add(chenalCr_);

      bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
          "Param�tres chenaux");
      table.setBorder(bordurea);
    } else

      if (choix.equals(ECLUSES)) {

        //table.setLayout(new GridLayout(10, 1));
        table.add(ecluse_);
        table.add(ecltaille_);
        table.add(eclDur_);
        table.add(ecluseIndispo_);
        table.add(ecluseCr_);

        bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
            "Param�tres �cluses");
        table.setBorder(bordurea);
      } else if (choix.equals(QUAIS)) {

        //table.setLayout(new GridLayout(10, 1));
        table.add(quai_);
        table.add(quaitaille_);
        table.add(quaiIndispo_);
        table.add(quaiCr_);

        bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
            "Param�tres quais");
        table.setBorder(bordurea);
      }

      else if (choix.equals(EVITAGE))// panel de selection des gares
      {
        titre = "Param�tres cercles d'�vitage";
        //table.setLayout(new GridLayout(5, 1));
        table.add(table1);
        table.add(table2);
        table.add(table3);
        table.add(table4);
        table.add(table5);
        table1.add(cercle_, BorderLayout.CENTER);
        bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
        table1.setBorder(bordurea);

      } else

        if (choix.equals(TOPOLOGIE_BASSINS))// panel de selection des gares
        {
          titre = TOPOLOGIE_BASSINS;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(topo1_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(TOPOLOGIE_CHENAUX))// panel de selection des gares
        {
          titre = TOPOLOGIE_CHENAUX;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(topo2_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(TOPOLOGIE_CERCLES_EVITAGE))// panel de selection des gares
        {
          titre = TOPOLOGIE_CERCLES_EVITAGE;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(topo3_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(TOPOLOGIE_ECLUSES))// panel de selection des gares
        {
          titre = TOPOLOGIE_ECLUSES;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(topo4_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(MODELE_DU_PORT))// panel de selection des gares
        {
          titre = MODELE_DU_PORT;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(topo5_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(CROISEMENTS_CERCLES_EVITAGE))// panel de selection des gares
        {
          titre = CROISEMENTS_CERCLES_EVITAGE;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(regle2_, BorderLayout.CENTER);
          //-- AHX - genes 2011 --//
          table1.add(regle5_, BorderLayout.SOUTH);
          
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(CROISEMENTS_CHENAUX))// panel de selection des gares
        {
          titre = CROISEMENTS_CHENAUX;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(regle1_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(DUREES_DE_PARCOURS_CERCLES_EVITAGE))// panel de selection des gares
        {
          titre = DUREES_DE_PARCOURS_CERCLES_EVITAGE;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(regle4_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(DUREES_DE_PARCOURS_CHENAUX))// panel de selection des gares
        {
          titre = DUREES_DE_PARCOURS_CHENAUX;
          //table.setLayout(new GridLayout(5, 1));
          table.add(table1);
          table.add(table2);
          table.add(table3);
          table.add(table4);
          table.add(table5);
          table1.add(regle3_, BorderLayout.CENTER);
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
          table1.setBorder(bordurea);

        } else if (choix.equals(CAT�GORIES_DE_NAVIRES)) {
          bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
          "Param�tres cat�gories de navires");
          table.setBorder(bordurea);
          //table.setLayout(new GridLayout(13, 1));
          table.add(nav_);
          table.add(navPrio_);
          table.add(navGare_);
          table.add(navLong_);
          table.add(navLarg_);
          table.add(navTE_);
          table.add(navQ1_);
          table.add(navQ2_);
          table.add(navQ3_);
          table.add(navCr_);
          table.add(navCrPM_);
          table.add(navMode_);
          table.add(navLoi_);
        }

    final JPanel aide = new JPanel();
    //aide.setLayout(new GridLayout(2, 1));
    final JPanel l1 = new JPanel();
    ImageIcon im = new ImageIcon();
    im = FudaaResource.FUDAA.getIcon("crystal_commentaire");
    final JLabel image = new JLabel(im);
    l1.add(image);
    l1.add(new JLabel("S�lectionnez les param�tres � faire figurer dans le rapport"));
    final JPanel l2 = new JPanel();
    l2.add(new JLabel("puis cliquez sur le bouton d'exportation."));
    aide.add(l1);
    aide.add(l2);
    this.selectionPanel_.add(aide, BorderLayout.NORTH);




    this.selectionPanel_.validate();
    this.validate();

  }

  /**
   * methode qui ecoute le listener de l'arbre
   */
  public void valueChanged(final TreeSelectionEvent e) {
    final DefaultMutableTreeNode node = (DefaultMutableTreeNode) arbre_.getLastSelectedPathComponent();

    if (node == null) {
      return;
    }

    // recupere le contenu du noeud qui est une chaine de caractere
    final String nodeInfo = (String) node.getUserObject();

    // si c est une feuille
    if (node.isLeaf()) {

      // on recherche la feuille selectionn�e et on modifie le panel en cons�quence:

      actualiser(nodeInfo);

      // displayURL(book.bookURL);
    }
  }

  public void actionPerformed(final ActionEvent ev) {

    if (ev.getSource().equals(quai_)) {
      if (quai_.isSelected()) {

        quaitaille_.setEnabled(true);
        quaiIndispo_.setEnabled(true);
        quaiCr_.setEnabled(true);

        quaitaille_.setSelected(true);
        quaiIndispo_.setSelected(true);
        quaiCr_.setSelected(false);

      } else {

        quaitaille_.setEnabled(false);
        quaiIndispo_.setEnabled(false);
        quaiCr_.setEnabled(false);

        quaitaille_.setSelected(false);
        quaiIndispo_.setSelected(false);
        quaiCr_.setSelected(false);

      }
    } else if (ev.getSource().equals(ecluse_)) {
      if (ecluse_.isSelected()) {

        ecltaille_.setEnabled(true);
        eclDur_.setEnabled(true);
        ecluseIndispo_.setEnabled(true);
        ecluseCr_.setEnabled(true);

        ecltaille_.setSelected(true);
        eclDur_.setSelected(true);
        ecluseIndispo_.setSelected(true);
        ecluseCr_.setSelected(false);

      } else {

        ecltaille_.setEnabled(false);
        eclDur_.setEnabled(false);
        ecluseIndispo_.setEnabled(false);
        ecluseCr_.setEnabled(false);

        ecltaille_.setSelected(false);
        eclDur_.setSelected(false);
        ecluseIndispo_.setSelected(false);
        ecluseCr_.setSelected(false);

      }
    } else if (ev.getSource().equals(chenal_)) {
      if (chenal_.isSelected()) {
        chenalPro_.setEnabled(true);
        chenalMar_.setEnabled(true);
        chenalCr_.setEnabled(true);
        chenalPro_.setSelected(true);
        chenalMar_.setSelected(true);
        chenalCr_.setSelected(true);

      } else {
        chenalPro_.setEnabled(false);
        chenalMar_.setEnabled(false);
        chenalCr_.setEnabled(false);
        chenalPro_.setSelected(false);
        chenalMar_.setSelected(false);
        chenalCr_.setSelected(false);

      }
    } else

      if (ev.getSource().equals(dg_)) {
        if (dg_.isSelected()) {
          dgNbJ_.setEnabled(true);
          dgJour_.setEnabled(true);
          dgGraine_.setEnabled(true);
          dgPiedPil_.setEnabled(true);
          dgNbJ_.setSelected(true);
          dgJour_.setSelected(true);
          dgGraine_.setSelected(true);
          dgPiedPil_.setSelected(true);

        } else {
          dgNbJ_.setEnabled(false);
          dgJour_.setEnabled(false);
          dgGraine_.setEnabled(false);
          dgPiedPil_.setEnabled(false);
          dgNbJ_.setSelected(false);
          dgJour_.setSelected(false);
          dgGraine_.setSelected(false);
          dgPiedPil_.setSelected(false);
        }
      } else if (ev.getSource().equals(nav_)) {
        if (nav_.isSelected()) {
          navPrio_.setEnabled(true);
          navGare_.setEnabled(true);
          navLong_.setEnabled(true);
          navLarg_.setEnabled(true);
          navTE_.setEnabled(true);
          navQ1_.setEnabled(true);
          navQ2_.setEnabled(true);
          navQ3_.setEnabled(true);
          navCr_.setEnabled(true);
          navCrPM_.setEnabled(true);
          navMode_.setEnabled(true);
          navLoi_.setEnabled(true);

          navPrio_.setSelected(true);
          navGare_.setSelected(true);
          navLong_.setSelected(true);
          navLarg_.setSelected(true);
          navTE_.setSelected(true);
          navQ1_.setSelected(true);
          navQ2_.setSelected(true);
          navQ3_.setSelected(true);
          navCr_.setSelected(false);
          navCrPM_.setSelected(false);
          navMode_.setSelected(false);
          navLoi_.setSelected(false);
        } else {
          navPrio_.setEnabled(false);
          navGare_.setEnabled(false);
          navLong_.setEnabled(false);
          navLarg_.setEnabled(false);
          navTE_.setEnabled(false);
          navQ1_.setEnabled(false);
          navQ2_.setEnabled(false);
          navQ3_.setEnabled(false);
          navCr_.setEnabled(false);
          navCrPM_.setEnabled(false);
          navMode_.setEnabled(false);
          navLoi_.setEnabled(false);
          navPrio_.setSelected(false);
          navGare_.setSelected(false);
          navLong_.setSelected(false);
          navLarg_.setSelected(false);
          navTE_.setSelected(false);
          navQ1_.setSelected(false);
          navQ2_.setSelected(false);
          navQ3_.setSelected(false);
          navCr_.setSelected(false);
          navCrPM_.setSelected(false);
          navMode_.setSelected(false);
          navLoi_.setSelected(false);
        }
      } else
        if (ev.getSource() == apercu_) {
          SiporGenereNoteHtml.apercuRapport(this, this.donnees_);
          exportationHTML_.setEnabled(true);
        }
        else

          if (ev.getSource() == exportationHTML_) {
            // creation du rapport:
            final JFileChooser fc = new JFileChooser();
            final int returnVal = fc.showSaveDialog(SiporFrameGenerationRappelDonnees.this);

            
            
            
            if (returnVal == JFileChooser.APPROVE_OPTION) {
              // creation du rapport
              //ecriture du contenu du rapport g�n�r�
               
                 try {
                  
                   //-- ETAPE 1: recuperer le contenu de l'editeur html --//
                   String contenuEditeur=editeur.getDocumentText();
                  
                   //-- Etape 2: supprimer le fichier temporaire de dessin du port  --//
                    
                   (new File(this.fichierImagePort_)).delete();
                   
                   //-- ETAPE 3: recuperer le fichier du rapport a g�n�rer --//
                  File file =CtuluLibFile.appendExtensionIfNeeded(fc.getSelectedFile(), "html");
                   
                   //-- ETAPE 4: g�n�rer l'image du port en ajoutant une extension .jpg --//
                  String nouveauCodeHtmlFichierPort=SiporGenereNoteHtml.creerModele(donnees_,this,file.getAbsolutePath()+".jpg");
                  
                  //-- ETAPE 5: realiser un matching sur le rapport de sipor: --//
                  //--        le but est de remplacer la ligne de commande d'insertion de l'image --//
                  String nouveauContenuEditeur=contenuEditeur.substring(0, contenuEditeur.indexOf("<img")-1)+nouveauCodeHtmlFichierPort+contenuEditeur.substring(contenuEditeur.indexOf(this.fichierImagePort_)+fichierImagePort_.length()+20, contenuEditeur.length());
                  
                  //-- ETAPE 6: enregistrement du nouveau fichier html r�sultata du matching --//
                   final FileWriter fw = new FileWriter(file);
                   fw.write(nouveauContenuEditeur, 0, nouveauContenuEditeur.length());
                   fw.flush();
                   fw.close();
                 } catch (final IOException _e1) {
                   donnees_.getApplication();
                  new BuDialogError(donnees_.getApplication().getApp(),SiporImplementation.INFORMATION_SOFT,
                       "Impossible d'�crire le fichier sur le disque.")
                  .activate();
                  
                   return ;
                 }
              // ouverture de la note
              final File file = CtuluLibFile.appendExtensionIfNeeded(fc.getSelectedFile(), "html");

              new BuDialogMessage(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,"Fichier correctement g�n�r� � l'emplacement\n"+file.getAbsolutePath()).activate();


            }
            
            
            
          } else if (ev.getSource() == choisirCouleur_) {
            final JColorChooser coulSommaire = new JColorChooser();

            final JDialog fen = coulSommaire.createDialog(null, "couleur du sommaire", true, coulSommaire, null, null);
            fen.show();

            couleurSommaire = coulSommaire.getColor();

            this.setBackground(coulSommaire.getColor());
            this.selectionPanel_.setBackground(coulSommaire.getColor());
            this.selectionPanel_.validate();
            this.controlPanel_.setBackground(coulSommaire.getColor());
            this.controlPanel_.validate();
            this.validate();

          }

  }

  protected void windowClosed() {

    dispose();
  }
}