/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.resultat.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.algorithmes.SiporAlgorithmeAttentesGenerales;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.factory.FonctionsSimu;
import org.fudaa.fudaa.sipor.factory.SiporTraduitHoraires;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class SiporResultatsAttenteGeneraleElement extends SiporInternalFrame {

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  /** Comboliste de selection de l element de d�part */
  String[] listeElt = { "Chenal", "Cercle", "Ecluse", "Quai" };
  JComboBox ListeTypesDepart_ = new JComboBox(listeElt);
  JComboBox ListeElementDepart_ = new JComboBox();
  /** Comboliste de selection de l element d'arrivee */
  JComboBox ListeTypesArrivee_ = new JComboBox(listeElt);
  JComboBox ListeElementArrivee_ = new JComboBox();
  /** horaire de d�part */
  String[] chaine_sens = { "entrant", "sortant", "les 2 sens" };
  JComboBox Sens_ = new JComboBox(chaine_sens);

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  JCheckBox choixNbNavires_ = new JCheckBox("Nombre navires", true);
  JCheckBox choixTotalAttente_ = new JCheckBox("Attente totale", true);

  JCheckBox choixMarees_ = new JCheckBox("Attentes mar�es", true);
  JCheckBox choixSecurite_ = new JCheckBox("Attentes s�curit�", true);
  JCheckBox choixAcces_ = new JCheckBox("Attentes acc�s", true);
  JCheckBox choixOccupation_ = new JCheckBox("Attentes occupation", true);
  JCheckBox choixPannes_ = new JCheckBox("Attentes indispo", true);

  JCheckBox[] tableauChoixNavires_;

  JCheckBox choixMax_ = new JCheckBox("Attente max", true);
  JCheckBox choixMoy_ = new JCheckBox("Attente moyenne", true);
  JCheckBox choixMin_ = new JCheckBox("Attente min", true);

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil", false);
  float valeurSeuil = 0;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Nb.nav.flotte", "Total.Mar�e", "MoyTotal.Mar�e", "NbnavAtt.Mar�e",
      "MoyAtt.Mar�e", "Total.Secu", "MoyTotal.Secu", "NbnavAtt.Secu", "MoyAtt.Secu", "Total.Acces", "MoyTotal.Acces",
      "NbnavAtt.Acces", "MoyAtt.Acces", "Total.Occup", "MoyTotal.Occup", "NbnavAtt.Occup", "MoyAtt.Occup",
      "Total.Indispos", "MoyTotal.Indispos", "NbnavAtt.Indispos", "MoyAtt.Indispos", "Total.Global", "MoyTotal.Global",
      "NbnavAtt.Global", "MoyAtt.Global" };
  // 26

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exportation image");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public SiporResultatsAttenteGeneraleElement(final SiporDataSimulation _donnees) {
    super("Attentes par Element", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le chenal 0 et le quai 0 dans l horaire entre 0 et 24

    setSize(820, 600);
    setBorder(SiporBordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);

    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipalAffichage_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    panelPrincipalAffichage_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.getCategoriesNavires_().getListeNavires_().size()];
    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom(), true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }
    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(2, 1));

    selectionPanel1 = new BuPanel();
    selectionPanel1.add(new JLabel("Attentes � cumuler pour l'�l�ment:"));
    selectionPanel1.add(this.ListeTypesDepart_);
    selectionPanel1.add(this.ListeElementDepart_);
    selectionPanel1.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel1);

    final BuPanel selectionPanel2 = new BuPanel();
    selectionPanel2.add(new JLabel("Sens du trajet: "));
    selectionPanel2.add(this.Sens_);
    selectionPanel2.add(lancerRecherche_);

    selectionPanel2.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel2);

    this.selectionPanel_.setBorder(this.compound_);

    // listener des liste box
    final ActionListener RemplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        int selection = 0;
        if (e.getSource() == ListeTypesDepart_) {
          selection = ListeTypesDepart_.getSelectedIndex();
        } else {
          selection = ListeTypesArrivee_.getSelectedIndex();
        }
        // "gare","quai","ecluse","chenal","cercle","bassin"
        switch (selection) {
        case 3:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getlQuais_().retournerQuais(i).getNom());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 2:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeEcluse_().getListeEcluses_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeEcluse_().retournerEcluse(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 0:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeChenal_().retournerChenal(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 1:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElementDepart_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
              ListeElementArrivee_.addItem(donnees_.getListeCercle_().retournerCercle(i).getNom_());
            }
            ListeElementArrivee_.validate();
          }
          break;
        }

      }
    };
    this.ListeTypesDepart_.addActionListener(RemplissageElement);
    this.ListeTypesArrivee_.addActionListener(RemplissageElement);
    this.ListeTypesDepart_.setSelectedIndex(0);
    this.ListeTypesArrivee_.setSelectedIndex(0);
    this.Sens_.setSelectedIndex(0);

    this.lancerRecherche_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    final JTabbedPane panoption = new JTabbedPane();
    this.optionPanel_.add(panoption);

    final Box bVert = Box.createVerticalBox();
    final JScrollPane pcnasc1 = new JScrollPane(bVert);
    panoption.addTab("Attentes", pcnasc1);
    bVert.add(new JLabel(""));
    bVert.add(this.choixNbNavires_);
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Options Affichage");
    bVert.setBorder(bordure1);
    bVert.add(this.choixMarees_);
    bVert.add(this.choixSecurite_);
    bVert.add(this.choixAcces_);
    bVert.add(this.choixOccupation_);
    bVert.add(this.choixPannes_);
    bVert.add(this.choixTotalAttente_);

    final Box bVert2 = Box.createVerticalBox();
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Affichage navires");
    bVert2.setBorder(bordure2);
    final JScrollPane pcnasc = new JScrollPane(bVert2);
    panoption.addTab("Navires", pcnasc);

    final Box bVert3 = Box.createVerticalBox();
    final TitledBorder bordure3 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Attente");
    bVert3.setBorder(bordure3);
    bVert3.add(this.choixMin_);
    bVert3.add(this.choixMoy_);
    bVert3.add(this.choixMax_);
    this.choixMin_.addActionListener(this);
    this.choixMoy_.addActionListener(this);
    this.choixMax_.addActionListener(this);

    this.optionPanel_.setBorder(this.compound_);

    // listener des checkbox de choix des options d affichage

    this.choixAcces_.addActionListener(this);
    this.choixMarees_.addActionListener(this);
    this.choixSecurite_.addActionListener(this);
    this.choixNbNavires_.addActionListener(this);
    this.choixTotalAttente_.addActionListener(this);
    this.choixOccupation_.addActionListener(this);
    this.choixPannes_.addActionListener(this);

    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.getCategoriesNavires_().retournerNavire(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra imprimer");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(SiporResultatsAttenteGeneraleElement.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final SiporModeleExcel modele = new SiporModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Permet de g�n�rer un fichier image � partir du graphe");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    final JScrollPane panneauHisto = new JScrollPane(this.histo_);
    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(/* this.histo_ */panneauHisto, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Permet de g�n�rer un fichier image � partir de l'histogramme");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.getApplication(), histo_);
      }
    });

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              donnees_.getApplication();
              new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                  "Erreur! La graine de la simulation est n�gative.\nIl faut entrer un entier positif.").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            donnees_.getApplication();
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "Erreur! Ce nombre n'est pas valide.\nIl faut entrer un entier.").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.controlPanelHisto_.add(new JLabel(" Seuil:"));
    this.controlPanelHisto_.add(valSeuil_);
    this.controlPanelHisto_.add(valideSeuil_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter2_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.quitter3_.setToolTipText(SiporConstantes.toolTipQuitter);
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SiporResultatsAttenteGeneraleElement.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");


    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporResultatsAttenteGeneraleElement.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // affichage du tableau

    /**
     * Recherche des donn�es associ�es � l'�l�ment choisi par l utilisateur: on recherche dasn el tableau qui stocke
     * tous les �l�ments, l indice de l �l�ment dont on veut les r�sultats:
     */
    int ELEMENTCHOISI = -1;
    for (int k = 0; k < this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
      if (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
          .getSelectedIndex()
          && this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
              .getSelectedIndex()) {
        ELEMENTCHOISI = k;
      }
    }
    if (ELEMENTCHOISI == -1) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Erreur! L'�l�ment s�lectionn� n'a pas �t� trouv�...").activate();
    }

    // operation magique qui permet de determiner ce tableau
    int indiceTbaleau = 0;
    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.getCategoriesNavires_().getListeNavires_().size()][26];

    /*
     * if(val<0) {
     */
    for (int i = 0; i < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
      if (this.tableauChoixNavires_[i].isSelected()) {
        data[indiceTbaleau][0] = this.donnees_.getCategoriesNavires_().retournerNavire(i).getNom();

        // ecriture des donn�es calcul�es pour les dur�es de parcours
        // si les cases correspondantes ont �t� coch�es:
        int indiceColonne = 1;
        if (this.choixNbNavires_.isSelected()) {
          data[indiceTbaleau][indiceColonne++] = ""
              + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal);
        }

        if (this.choixMarees_.isSelected()) {
          if (donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMareeTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMareeTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree)
                + " ("
                + (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree
                    / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMareeTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree));

          }
        }
        if (this.choixSecurite_.isSelected()) {
          if (donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu)
                + " ("
                + (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu
                    / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu));

          }
        }

        if (this.choixAcces_.isSelected()) {
          if (donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float)FonctionsSimu.diviserSimu( this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces)
                + " ("
                + (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces
                    / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces));

          }
        }

        if (this.choixOccupation_.isSelected()) {
          if (donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float)FonctionsSimu.diviserSimu( this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup)
                + " ("
                + (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup
                    / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup));

          }
        }

        if (this.choixPannes_.isSelected()) {
          if (donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne)
                + " ("
                + (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne
                    / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne));

          }
        }

        if (this.choixTotalAttente_.isSelected()) {
          // attente totale
          if (donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale == 0) {
            indiceColonne += 4;
          } else {
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale)
                + " ("
                + (float) ((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale
                    / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + SiporTraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale));

          }
        }

        indiceTbaleau++;
      }// fin du if si la cat�gorie a �t� selectionn�e pour etre affich�e
    }

    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    // etape 3.5: dimensionnement des colonnes du tableau 
   /* for(int i=0; i<tableau_.getModel().getColumnCount();i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }
    */
    ColumnAutoSizer.sizeColumnsToFit(tableau_);  
    
    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    int ELEMENTCHOISI = -1;
    for (int k = 0; k < this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
      if (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
          .getSelectedIndex()
          && this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
              .getSelectedIndex()) {
        ELEMENTCHOISI = k;
      }
    }
    if (ELEMENTCHOISI == -1) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Erreur! L'�l�ment s�lectionn� n'a pas �t� trouv�").activate();
    }

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Attente de l'�l�ment " + (String) this.ListeElementDepart_.getSelectedItem();
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens entrant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens sortant";
    } else {
      g += " dans les 2 sens";
    }
    g += " \"\n";

    if (this.choixMarees_.isSelected()) {
      g += "  sous-titre \" Attentes de Mar�es \"\n";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  sous-titre \" Attentes de S�curit� \"\n";
    } else if (this.choixAcces_.isSelected()) {
      g += "  sous-titre \" Attentes d' Acc�s \"\n";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  sous-titre \" Attentes Occupation \"\n";
    } else if (this.choixPannes_.isSelected()) {
      g += "  sous-titre \" Attentes de Pannes \"\n";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  sous-titre \" Attente totale \"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
  

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" dur�es" + "\"\n";
    g += "    unite \"" + " H.Min" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeAttentesGenerales
            .determineAttenteMax(donnees_))
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    if (this.choixMax_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es maxi";
      } else if (this.choixSecurite_.isSelected()) {
        g += "  attentes S�curit� maxi";
      } else if (this.choixAcces_.isSelected()) {
        g += "attentes Acc�s maxi";
      } else if (this.choixOccupation_.isSelected()) {
        g += "  attentes Occupations maxi";
      } else if (this.choixPannes_.isSelected()) {
        g += "  attentes Indispos maxi";
      } else if (this.choixTotalAttente_.isSelected()) {
        g += "  attentes totale maxi";
      }
      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur   BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB0000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          if (this.choixMarees_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMaxi);
          } else if (this.choixSecurite_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMaxi);
          } else if (this.choixAcces_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMaxi);
          } else if (this.choixOccupation_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMaxi);
          } else if (this.choixPannes_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMaxi);
          } else if (this.choixTotalAttente_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMaxi);
          }

          g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
              + "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
    if (this.choixMoy_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es moyenne";
      } else if (this.choixSecurite_.isSelected()) {
        g += "  attentes S�curit� moyenne";
      } else if (this.choixAcces_.isSelected()) {
        g += "attentes Acc�s moyenne";
      } else if (this.choixOccupation_.isSelected()) {
        g += "  attentes Occupations moyenne";
      } else if (this.choixPannes_.isSelected()) {
        g += "  attentes Indispos moyenne";
      } else if (this.choixTotalAttente_.isSelected()) {
        g += "  attentes totale moyenne";
      }

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB8800 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          if (this.choixMarees_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteMaree));
          } else if (this.choixSecurite_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu));
          } else if (this.choixAcces_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces));
          } else if (this.choixOccupation_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup));
          } else if (this.choixPannes_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne));
          } else if (this.choixTotalAttente_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale));
          }

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
    if (this.choixMin_.isSelected()) {

      g += "  courbe\n  {\n";
      g += "    titre \"";
      if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es mini";
      } else if (this.choixSecurite_.isSelected()) {
        g += "  attentes S�curit� mini";
      } else if (this.choixAcces_.isSelected()) {
        g += "attentes Acc�s mini";
      } else if (this.choixOccupation_.isSelected()) {
        g += "  attentes Occupations mini";
      } else if (this.choixPannes_.isSelected()) {
        g += "  attentes Indispos mini";
      } else if (this.choixTotalAttente_.isSelected()) {
        g += "  attentes totale mini";
      }

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BBC00 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;

          if (this.choixMarees_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMini);
          } else if (this.choixSecurite_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMini);
          } else if (this.choixAcces_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMini);
          } else if (this.choixOccupation_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMini);
          } else if (this.choixPannes_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMini);
          } else if (this.choixTotalAttente_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMini);
          }

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    }
    // //******************************fin histo moy************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    int ELEMENTCHOISI = -1;
    for (int k = 0; k < this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
      if (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
          .getSelectedIndex()
          && this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
              .getSelectedIndex()) {
        ELEMENTCHOISI = k;
      }
    }
    if (ELEMENTCHOISI == -1) {
      new BuDialogError(donnees_.getApplication().getApp(), donnees_.getApplication().INFORMATION_SOFT,
          "Erreur! L'�l�ment s�lectionn� n'a pas �t� trouv�.").activate();
    }

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \" Attente de l'�l�ment " + (String) this.ListeElementDepart_.getSelectedItem();/* +" \"\n"; */
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens entrant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens sortant";
    } else {
      g += " dans les 2 sens";
    }
    g += " \"\n";

    if (this.choixMarees_.isSelected()) {
      g += "  sous-titre \" Attentes de Mar�es \"\n";
    } else if (this.choixSecurite_.isSelected()) {
      g += "  sous-titre \" Attentes de S�curit� \"\n";
    } else if (this.choixAcces_.isSelected()) {
      g += "  sous-titre \" Attentes d' Acc�s \"\n";
    } else if (this.choixOccupation_.isSelected()) {
      g += "  sous-titre \" Attentes Occupation \"\n";
    } else if (this.choixPannes_.isSelected()) {
      g += "  sous-titre \" Attentes de Indispos \"\n";
    } else if (this.choixTotalAttente_.isSelected()) {
      g += "  sous-titre \" Attente totale \"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \" cat." + "\"\n";
    g += "    unite \" Categories \"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.getCategoriesNavires_().getListeNavires_().size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \" dur�es" + "\"\n";
    g += "    unite \"" + " H.Min" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + SiporTraduitHoraires.traduitMinutesEnHeuresMinutes((float) SiporAlgorithmeAttentesGenerales
            .determineAttenteMax(donnees_))
        // DETERMINE LE MAX
        + "\n";
    g += "  }\n";

    // ******************************debut histo max************************************************
    if (this.choixMax_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es maxi";
      } else if (this.choixSecurite_.isSelected()) {
        g += "  attentes S�curit� maxi";
      } else if (this.choixAcces_.isSelected()) {
        g += "attentes Acc�s maxi";
      } else if (this.choixOccupation_.isSelected()) {
        g += "  attentes Occupations maxi";
      } else if (this.choixPannes_.isSelected()) {
        g += "  attentes Indispos maxi";
      } else if (this.choixTotalAttente_.isSelected()) {
        g += "  attentes totale maxi";
      }
      // g += "dur�es maxi";
      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur   BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          if (this.choixMarees_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMaxi);
          } else if (this.choixSecurite_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMaxi);
          } else if (this.choixAcces_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMaxi);
          } else if (this.choixOccupation_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMaxi);
          } else if (this.choixPannes_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMaxi);
          } else if (this.choixTotalAttente_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMaxi);
          }

          g += "\n etiquette  \n \"" + this.donnees_.getCategoriesNavires_().retournerNavire(n).getNom() + "\" \n"/* + */
              + "\n";
        }// fin du if le navire est selectionn�
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
    if (this.choixMoy_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es moyenne";
      } else if (this.choixSecurite_.isSelected()) {
        g += "  attentes S�curit� moyenne";
      } else if (this.choixAcces_.isSelected()) {
        g += "attentes Acc�s moyenne";
      } else if (this.choixOccupation_.isSelected()) {
        g += "  attentes Occupations moyenne";
      } else if (this.choixPannes_.isSelected()) {
        g += "  attentes Indispos moyenne";
      } else if (this.choixTotalAttente_.isSelected()) {
        g += "  attentes totale moyenne";
      }

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          if (this.choixMarees_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteMaree));
          } else if (this.choixSecurite_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu));
          } else if (this.choixAcces_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces));
          } else if (this.choixOccupation_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup));
          } else if (this.choixPannes_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne));
          } else if (this.choixTotalAttente_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale));
          }

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
    if (this.choixMin_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";
      if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es mini";
      } else if (this.choixSecurite_.isSelected()) {
        g += "  attentes S�curit� mini";
      } else if (this.choixAcces_.isSelected()) {
        g += "attentes Acc�s mini";
      } else if (this.choixOccupation_.isSelected()) {
        g += "  attentes Occupations mini";
      } else if (this.choixPannes_.isSelected()) {
        g += "  attentes Indispos mini";
      } else if (this.choixTotalAttente_.isSelected()) {
        g += "  attentes totale mini";
      }

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.getCategoriesNavires_().getListeNavires_().size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;

          if (this.choixMarees_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMini);
          } else if (this.choixSecurite_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMini);
          } else if (this.choixAcces_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMini);
          } else if (this.choixOccupation_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMini);
          } else if (this.choixPannes_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMini);
          } else if (this.choixTotalAttente_.isSelected()) {
            g += SiporTraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.getParams_().ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMini);
          }

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
    if (source == this.choixTotalAttente_ || source == this.choixNbNavires_ || source == this.choixAcces_
        || source == this.choixMarees_ || source == this.choixSecurite_ || source == this.choixOccupation_
        || source == this.choixPannes_)
    /* if(source==this.lancerOptions_) */{
      // clic sur un checkBox:
      // construction de la colonne des titres

      if (!this.choixMarees_.isSelected() && !this.choixSecurite_.isSelected() && !this.choixAcces_.isSelected()
          && !this.choixOccupation_.isSelected() && !this.choixPannes_.isSelected()
          && !this.choixTotalAttente_.isSelected()) {
        this.choixTotalAttente_.setSelected(true);
      }

      int compteurColonnes = 0;
      if (this.choixAcces_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixMarees_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixSecurite_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixTotalAttente_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixOccupation_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixPannes_.isSelected()) {
        compteurColonnes += 4;
      }

      // String

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;
      this.titreTableau_[indiceColonne++] = "Cat�gorie";
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Nb.nav.flotte";
      }

      if (this.choixMarees_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Mar�e";
        this.titreTableau_[indiceColonne++] = "MoyTotal.Mar�e";
        this.titreTableau_[indiceColonne++] = "NbnavAtt.Mar�e";
        this.titreTableau_[indiceColonne++] = "MoyAtt.Mar�e";
      }
      if (this.choixSecurite_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Secu";
        this.titreTableau_[indiceColonne++] = "MoyTotal.Secu";
        this.titreTableau_[indiceColonne++] = "NbnavAtt.Secu";
        this.titreTableau_[indiceColonne++] = "MoyAtt.Secu";
      }
      if (this.choixAcces_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Acces";
        this.titreTableau_[indiceColonne++] = "MoyTotal.Acces";
        this.titreTableau_[indiceColonne++] = "NbnavAtt.Acces";
        this.titreTableau_[indiceColonne++] = "MoyAtt.Acces";
      }
      if (this.choixOccupation_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Occup";
        this.titreTableau_[indiceColonne++] = "MoyTotal.Occup";
        this.titreTableau_[indiceColonne++] = "NbnavAtt.Occup";
        this.titreTableau_[indiceColonne++] = "MoyAtt.Occup";
      }
      if (this.choixPannes_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Total.Indispo";
        this.titreTableau_[indiceColonne++] = "MoyTotal.Indispo";
        this.titreTableau_[indiceColonne++] = "NbnavAtt.Indispo";
        this.titreTableau_[indiceColonne++] = "MoyAtt.Indispo";
      }
      if (this.choixTotalAttente_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "attente totale";
        this.titreTableau_[indiceColonne++] = "moyenne totale";
        this.titreTableau_[indiceColonne++] = "Nb.nav.Att.totale";
        this.titreTableau_[indiceColonne++] = "moyenne attentes";
      }
      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.lancerRecherche_) {
      // lancement des calculs pour les dur�es de parcours:
      SiporAlgorithmeAttentesGenerales.calcul(donnees_, this.Sens_.getSelectedIndex());
      // mise a jour des affichages:
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      // }
    } else if (source == this.valideSeuil_ || source == this.choixMax_ || source == this.choixMoy_
        || source == this.choixMin_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    dispose();
  }
}
