package org.fudaa.fudaa.sipor.ui.tools;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuValueValidator;

import org.fudaa.fudaa.sipor.SiporImplementation;

public class SiporTextFieldFloat extends SiporTextField {

  public SiporTextFieldFloat() {
    super();
  }

  public SiporTextFieldFloat(int columns) {
    super(columns);
  }

  public SiporTextFieldFloat(String text, int columns) {
    super(text, columns);
  }

  public SiporTextFieldFloat(String text) {
    super(text);
  }

  /**
   * Methode destinee a etre surchargee par les classes h�ritant de SiporTextField. Permet de g�rer les contr�les apr�s
   * saisie de l'utilisateur
   **/
  protected void traitementApresFocus(FocusEvent e) {

    String contenu = this.getText();

    if (contenu.equals("")) return;
    //-- Traduction en float --//

    try {
      float valeur = Float.parseFloat(contenu);
      if (valeur < 0) {
        new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Ce nombre est n�gatif").activate();

        setText("");
        this.requestFocus();
      }

    } catch (NumberFormatException e1) {
      new BuDialogError(null, SiporImplementation.INFORMATION_SOFT, "Ce nombre n'est pas un r�el.").activate();
      setText("");
      this.requestFocus();
    }

    SiporTextFieldFloat.this.setForeground(Color.black);
  }

  protected int taille() {

    return 5;
  }

  protected void specifier_validator() {

    this.setCharValidator(BuCharValidator.FLOAT);
    this.setStringValidator(BuStringValidator.FLOAT);
    this.setValueValidator(BuValueValidator.FLOAT);

  }

}
