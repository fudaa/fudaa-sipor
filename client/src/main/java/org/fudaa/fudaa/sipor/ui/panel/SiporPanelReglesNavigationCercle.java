/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license GNU General Public Licence 2
 *@copyright (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogChoice;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * @version $Version$
 * @author hadoux
 */
public class SiporPanelReglesNavigationCercle extends SiporInternalFrame implements Observer {

  /**
   * JCombo qui permettra de choisir pour chaque chenal si les navires sont autoris�s a se croiser
   */
  JComboBox ComboChoix_;

  /**
   * ComboBox des cheneaux
   */
  JComboBox ComboChoixCercle_ = new JComboBox();

  /**
   * Tableau contenant les donn�es du tableau affich� en java (definition attribut car sert pour transformation excel)
   */
  Object[][] ndata;

  /**
   * Descriptif des elements des colonnes
   */
  public String[] titreColonnes_;

  /**
   * Tableau de type JTable qui contiendra les donnees des navires
   */

  public BuTable tableau_;

  /**
   * Bouton de validation des regles de navigations.
   */

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton oui_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ajouter"), "Oui");
  private final BuButton non_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_enlever"), "Non");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "");

  /**
   * Panel qui contiendra le tableau.
   */
  public JPanel global_ = new JPanel();

  /**
   * panel de controle
   */
  public JPanel controlPanel = new JPanel();
  /**
   * Bordure du tableau.
   */

  public Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Indice du chenal choisi (par defaut 0 => le premier chenal.
   */
  public int cercleChoisi_ = 0;

  /**
   * constructeur du panel d'affichage des bassins.
   * 
   * @param d donn�es de la simulation.
   */
  public SiporPanelReglesNavigationCercle(final SiporDataSimulation _donnees) {

    super("", true, true, true, true);

    donnees_ = _donnees;
    donnees_.addObservers(this);
    global_.setLayout(new BorderLayout());

    oui_
        .setToolTipText("Transforme la zone s�lectionn�e � la souris en \"oui\". Transforme automatiquement les zones sym�triques.");
    non_
        .setToolTipText("Transforme la zone s�lectionn�e � la souris en \"non\". Transforme automatiquement les zones sym�triques.");

    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);

    /**
     * Initialisation du nombre de cheneaux.
     */

    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();

    /**
     * Creation de la fenetre
     */

    setTitle("R�gles Navigation de croisements du cercle d'�vitage "
        + donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getNom_());
    setSize(800, 400);
    setBorder(SiporBordures.compound_);
    getContentPane().setLayout(new BorderLayout());

    //final JScrollPane ascenceur = new JScrollPane(global_);

    getContentPane().add(global_, BorderLayout.CENTER);

    // controlPanel.setLayout(new GridLayout(1,8));

    controlPanel.setBorder(this.borduretab_);
    controlPanel.add(quitter_);

    controlPanel.add(oui_);
    controlPanel.add(non_);

    controlPanel.add(new JLabel("cercle:"));
    controlPanel.add(this.ComboChoixCercle_);

    controlPanel.add(this.duplication_);
    controlPanel.add(this.impression_);

    controlPanel.add(validation_);

    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    /**
     * Listener du comboChoix de chenal: lors dde la selection du chenal, on affiche (rafraichis) le tableau de choix
     * des regles de navigations pour ce chenal
     */
    this.duplication_.setToolTipText("Permet de dupliquer les r�gles de navigation avec celles d'un autre cercle.");
    this.impression_
        .setToolTipText("G�n�re un fichier Excel du tableau. Attention, ce bouton ne g�n�re que le tableau du cercle affich�.");

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });

    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(SiporPanelReglesNavigationCercle.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.getCategoriesNavires_().getListeNavires_().size() + 2][titreColonnes_.length];

          for (int i = 0; i < titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
            modele.data_[i + 2] = ndata[i];
          }

          modele.setNbRow(donnees_.getCategoriesNavires_().getListeNavires_().size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final Exception _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });

    this.duplication_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final String[] values = new String[donnees_.getListeCercle_().getListeCercles_().size()];

        for (int i = 0; i < donnees_.getListeCercle_().getListeCercles_().size(); i++) {
          values[i] = donnees_.getListeCercle_().retournerCercle(i).getNom_();
        }

        final BuDialogChoice choix = new BuDialogChoice(donnees_.getApplication().getApp(),
            SiporImplementation.INFORMATION_SOFT, "Choix du cercle mod�le", "Le cercle"
                + donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getNom_()
                + " r�cup�re les r�gles du cercle choisi ci-dessous", values);

        final int reponse = choix.activate();
        if (reponse != -1)

        {
          final int cercle = donnees_.getListeCercle_().retourneIndice(choix.getValue());
          if (cercle != -1) {
            donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getReglesNavigation_().duplication(
                donnees_.getListeCercle_().retournerCercle(cercle).getReglesNavigation_().getVecteurDeVecteur_());
            affichage();
          } else {
            new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
                "La duplication est impossible. Le cercle choisi n'existe pas.").activate();
          }
        }

      }
    });

    this.oui_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int[] tableauColonnes = tableau_.getSelectedColumns();
        final int[] tableauLignes = tableau_.getSelectedRows();
        if (tableauColonnes.length != 0 && tableauLignes.length != 0) {

          for (int k = 0; k < tableauLignes.length; k++) {
            for (int l = 0; l < tableauColonnes.length; l++) {
              // l oppos� de (i,j) est (j-1;i+1)
              final int i = tableauLignes[k];
              final int j = tableauColonnes[l];
              if (j != 0)// pour ne pas modifier al colonens des noms de cat�gories de navires
              {
                donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getReglesNavigation_().modification(true, i,
                    j - 1);
                donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getReglesNavigation_().modification(true,
                    j - 1, i);

              }

            }
          }

          // rafraichissement:
          affichage();
        }

      }
    });

    this.non_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int[] tableauColonnes = tableau_.getSelectedColumns();
        final int[] tableauLignes = tableau_.getSelectedRows();
        if (tableauColonnes.length != 0 && tableauLignes.length != 0) {

          for (int k = 0; k < tableauLignes.length; k++) {
            for (int l = 0; l < tableauColonnes.length; l++) {
              // l oppos� de (i,j) est (j-1;i+1)
              final int i = tableauLignes[k];
              final int j = tableauColonnes[l];
              if (j != 0)// pour ne pas modifier al colonens des noms de cat�gories de navires
              {
                donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getReglesNavigation_().modification(false, i,
                    j - 1);
                donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getReglesNavigation_().modification(false,
                    j - 1, i);

              }

            }
          }

          // rafraichissement:
          affichage();
        }
      }
    });

    this.ComboChoixCercle_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent ev) {
        // 1) enregistrement des modification poru le chanl en cours:
        miseAjourReglesNavigation();

        // 2) recuperation de l indice du cercle choisi
        // et affectation dans la variable du chenal choisi
        cercleChoisi_ = ComboChoixCercle_.getSelectedIndex();
        setTitle("R�gles Navigation de croisements du cercle "
            + donnees_.getListeCercle_().retournerCercle(cercleChoisi_).getNom_());

        // 3) affichage(rafraichissement des donn�es)
        affichage();

      }

    });

    // listener du bouton de validation

    this.validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        miseAjourReglesNavigation();
        new BuDialogMessage(
            donnees_.getApplication().getApp(),
            SiporImplementation.INFORMATION_SOFT,
            "R�gles de navigation des cercles correctement saisis.\n Attention, un ajout de cat�gorie de navire implique qu'\n une ligne et une colonne suppl�mentaire � saisir \nappara�tront ici.\nPar d�faut, les valeurs de cette ligne et colonne\n suppl�mentaire sont � oui.")
            .activate();
        dispose();

      }

    });

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  public void remplissage() {

    // 1)cr�ation du combo
    this.ComboChoix_ = new JComboBox();

    // 3)remplissage du tableau de gare avec toutes les donn�es des gares

    this.ComboChoix_.addItem("oui");
    this.ComboChoix_.addItem("non");

    // 4) remplissage des cheneaux dansla jcomobobox correspondante

    this.ComboChoixCercle_.removeAllItems();

    for (int i = 0; i < this.donnees_.getListeCercle_().getListeCercles_().size(); i++) {
      this.ComboChoixCercle_.addItem(this.donnees_.getListeCercle_().retournerCercle(i).getNom_());
    }
  }

  /**
   * Methode d affichage des composants du BuTable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  public void affichage() {

    final int nbNavires = donnees_.getCategoriesNavires_().getListeNavires_().size();

    // nombre de colonnes= nombre de navire + nom du navire
    ndata = new Object[nbNavires][nbNavires + 1];

    titreColonnes_ = new String[nbNavires + 1];

    // initialisation des titres
    titreColonnes_[0] = "Cat�gories: ";
    for (int i = 0; i < nbNavires; i++) {
      titreColonnes_[i + 1] = "  " + donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
    }

    /**
     * choix du remplissage: par defaut si le nombre de donnees regles de navigation du chenal selectionn� vaut 0
     */

    /** on initialise le tableau avec les donn�es charg�es */
    for (int i = 0; i < nbNavires; i++) {

      // this.donnees_.getListeCercle_().retournerCercle(this.chenalChoisi_).getReglesNavigation_()
      // nom du navire
      ndata[i][0] = donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
      for (int j = 0; j < nbNavires; j++) {
        /*
         * if(j>i) ndata[i][j+1]="X"; else {
         */
        // recuperation de la valeur de la case i,j
        if (this.donnees_.getListeCercle_().retournerCercle(this.cercleChoisi_).getReglesNavigation_().retournerAij(i,
            j).booleanValue()) {
          ndata[i][j + 1] = "oui";
        } else {
          ndata[i][j + 1] = "non";
        }

        // }
      }// fin du pourparcours des colonnes

    }

    this.tableau_ = new BuTable(ndata, this.titreColonnes_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    tableau_.setRowSelectionAllowed(true);
    tableau_.setColumnSelectionAllowed(true);
    tableau_.setCellSelectionEnabled(true);

    tableau_.setBorder(this.borduretab_);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();

    this.global_.removeAll();

    this.global_.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    this.global_.revalidate();
    this.global_.updateUI();

    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de mettre a jour les gares saiies en amont et vaales pour chacun des cheneaux: v�rifie dans un
   * premier temps l coh�rence des donn�es:
   */

  public void miseAjourReglesNavigation() {

    final int nbNavires = donnees_.getCategoriesNavires_().getListeNavires_().size();

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */

    /**
     * MODIFICAITON DES Regles de Navigations POUR le Cercle choisi:
     */
    for (int i = 0; i < nbNavires; i++) {
      // affichage de chacune des donn�es
      System.out.println("");

      for (int j = 0; j <= i; j++) {
        // recuperer une donn�e
        final String nomchoix = (String) this.tableau_.getModel().getValueAt(i, j + 1);

        /**
         * ajout du choix saisies dans le tableau de regles de navigation du chenal choisi
         */
        if (nomchoix.equals("non")) {
          this.donnees_.getListeCercle_().retournerCercle(this.cercleChoisi_).getReglesNavigation_().modification(
              false, i, j);
          this.donnees_.getListeCercle_().retournerCercle(this.cercleChoisi_).getReglesNavigation_().modification(
              false, j, i);

        } else {
          this.donnees_.getListeCercle_().retournerCercle(this.cercleChoisi_).getReglesNavigation_().modification(true,
              i, j);
          this.donnees_.getListeCercle_().retournerCercle(this.cercleChoisi_).getReglesNavigation_().modification(true,
              j, i);
        }

      }// fin du for parcours de chaque colonnes

    }

  }// fin du if ajout des donn�es

  public void update(Observable o, Object arg) {
    if (arg.equals("cercle") || arg.equals("navire")) {}
  }

}
