/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.structures;

/**
 * FIXME FRED a quoi sert cette classe
 * 
 * @version $Id: SiporDonneesGenerales.java,v 1.3 2007-05-04 14:01:02 deniger Exp $
 * @author hadoux
 */
public class SiporDonneesGenerales {

  /**
   * nombre de jours de al simulation par defaut ce nombre est 365
   */
  int nbJours_ = 365;

  /**
   * FIXME FRED Bon courage !
   * definition des jours feries.
   */
  int joursFeries_[] = new int[365];

  /**
   * nombre de jours feries
   */
  int nbJoursFeries_ = 0;

  /**
   * entier qui correspond au jour de depart 1=> lundi 2=> mardi ... 7=> dimanche par defaut 1=> lundi
   */
  int jourDepart = 1;

  /**
   * pourcentage du pied de pilote par defaut ce pourcentage est de 10%
   */
  int piedDePilote = 10;

public int getNbJours_() {
	return nbJours_;
}

public void setNbJours_(int nbJours_) {
	this.nbJours_ = nbJours_;
}

public int[] getJoursFeries_() {
	return joursFeries_;
}

public void setJoursFeries_(int[] joursFeries_) {
	this.joursFeries_ = joursFeries_;
}

public int getNbJoursFeries_() {
	return nbJoursFeries_;
}

public void setNbJoursFeries_(int nbJoursFeries_) {
	this.nbJoursFeries_ = nbJoursFeries_;
}

public int getJourDepart() {
	return jourDepart;
}

public void setJourDepart(int jourDepart) {
	this.jourDepart = jourDepart;
}

public int getPiedDePilote() {
	return piedDePilote;
}

public void setPiedDePilote(int piedDePilote) {
	this.piedDePilote = piedDePilote;
}

}
