package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatsAttente;
import org.fudaa.dodico.corba.sipor.SParametresResultatsAttenteCategorie;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

/**
 * classe permettant de calculer l'ensemble des attentes trajet afin de s'affranchir du fichier historique.
 * 
 * @version $Version$
 * @author hadoux
 */
public class SiporAlgorithmeTOUTESAttentesTrajet {

  public static void calcul(final SiporDataSimulation _d) {

    // 0: determiner le place totale a allouer:
    _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet = new SParametresResultatsAttente[_d
        .getListeChenal_().getListeChenaux_().size()
        * (_d.getListeChenal_().getListeChenaux_().size() * 3 + _d.getListeCercle_().getListeCercles_().size() * 3
            + _d.getListeEcluse_().getListeEcluses_().size() * 3 + _d.getlQuais_().getlQuais_().size() * 3)
        + _d.getListeCercle_().getListeCercles_().size()
        * (_d.getListeCercle_().getListeCercles_().size() * 3 + _d.getListeEcluse_().getListeEcluses_().size() * 3 + _d
            .getlQuais_().getlQuais_().size() * 3)
        + _d.getListeEcluse_().getListeEcluses_().size()
        * (_d.getListeEcluse_().getListeEcluses_().size() * 3 + _d.getListeEcluse_().getListeEcluses_().size() * 3 + _d
            .getlQuais_().getlQuais_().size() * 3)

    ];
    int compteur = 0;

    // 1: chenaux et cercles dasn les 3 sens
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < _d.getListeCercle_().getListeCercles_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 1, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�e dans la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element

          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 1, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des ddonn�edans la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 1, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des dodonn�eans la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 2: chenaux et ecluses dans les 3 sens
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < _d.getListeEcluse_().getListeEcluses_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 2, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des dondonn�ens la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 2, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donndonn�es la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 2, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�donn�e la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 3: chenaux et quais dasn les 3 sens
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = 0; j < _d.getlQuais_().getlQuais_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 3, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�edonn�ela structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 3, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 3, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es donn�e structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 4: ecluses et cercles dasn les 3 sens
    for (int i = 0; i < _d.getListeEcluse_().getListeEcluses_().size(); i++) {
      for (int j = 0; j < _d.getListeCercle_().getListeCercles_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 2, i, 1, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es ddonn�estructure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 2, i, 1, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dadonn�etructure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 2, i, 1, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dandonn�eructure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }
    // 6: quais et cercles dans les 3 sens
    for (int i = 0; i < _d.getlQuais_().getlQuais_().size(); i++) {
      for (int j = 0; j < _d.getListeCercle_().getListeCercles_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 3, i, 1, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dansdonn�eucture
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 3, i, 1, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans donn�ecture
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 3, i, 1, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans ldonn�eture
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 6: quais et ecluses dans les 3 sens
    for (int i = 0; i < _d.getlQuais_().getlQuais_().size(); i++) {
      for (int j = 0; j < _d.getListeEcluse_().getListeEcluses_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 3, i, 2, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans ladonn�eure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 3, i, 2, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la donn�ere
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 3;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 3, i, 2, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la sdonn�ee
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 7: chenaux et chenaux dasn les 3 sens
    for (int i = 0; i < _d.getListeChenal_().getListeChenaux_().size(); i++) {
      for (int j = i + 1; j < _d.getListeChenal_().getListeChenaux_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 0, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la stdonn�e
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element

          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 0, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la strdonn�e        
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 0;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 0, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 8: cercles et cercles dasn les 3 sens
    for (int i = 0; i < _d.getListeCercle_().getListeCercles_().size(); i++) {
      for (int j = i + 1; j < _d.getListeCercle_().getListeCercles_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 1, i, 1, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la strucdonn�e     
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element

          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 1, i, 1, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structdonn�e     
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 1, i, 1, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structudonn�e    
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
      }
    }

    // 9: ecluses et ecluses dasn les 3 sens
    for (int i = 0; i < _d.getListeEcluse_().getListeEcluses_().size(); i++) {
      for (int j = i + 1; j < _d.getListeEcluse_().getListeEcluses_().size(); j++) {
        /*  *************** SENS 1 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 2, i, 2, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structurdonn�e   
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element

          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;
        /*  *************** SENS 2 ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 2, i, 2, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structuredonn�e  
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }
        compteur++;

        /*  *************** les 2 SENS ************************* */
        // allocation memoire
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 2;
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 2;
        // lancement des calculs du trajet correspondant:
        SiporAlgorithmeAttentesGenerales.calculTrajet(_d, 2, i, 2, j, 2);

        // allocation memoire du tableau des attentes par trajet
        _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d
            .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        for (int k = 0; k < _d.getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
          // allocation memoire de element
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteAccesTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMareeTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMareeTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteMegaTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteOccupTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attentePanneTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteSecuTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMaxi = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].attenteTotaleMini = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAtenteOccup = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteAcces = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteMaree = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteMaree;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttentePanne = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteSecu = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nbNaviresAttenteTotale = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
          _d.getParams_().ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories[k].nombreNaviresTotal = _d
              .getParams_().ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
        }

        if (i != _d.getListeEcluse_().getListeEcluses_().size() - 2
            || j != _d.getListeEcluse_().getListeEcluses_().size() - 1) {
          compteur++;
        }
      }
    }

  }// fin void calcul

}
