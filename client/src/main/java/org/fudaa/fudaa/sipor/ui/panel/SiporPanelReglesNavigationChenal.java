/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sipor.ui.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogChoice;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.SiporImplementation;
import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.modeles.SiporModeleExcel;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * @version $Version$
 * @author hadoux
 */
public class SiporPanelReglesNavigationChenal extends SiporInternalFrame implements Observer{

  /**
   * JCombo qui permettra de choisir pour chaque chenal si les navires sont autoris�s a se croiser
   */
  JComboBox ComboChoix_;

  /**
   * ComboBox des cheneaux
   */
  JComboBox ComboChoixChenal_ = new JComboBox();

  /**
   * Tableau contenant les donn�es du tableau affich� en java (definition attribut car sert pour transformation excel)
   */
  Object[][] ndata;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes_;

  /**
   * Tableau de type JTable qui contiendra les donnees des navires
   */

  BuTable tableau_;

  

  /**
   * Bouton de validation des regles de navigations.
   */

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton oui_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ajouter"), "Oui");
  private final BuButton non_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_enlever"), "Non");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "");

  /**
   * Panel qui contiendra le tableau.
   */
  JPanel global_ = new JPanel();

  /**
   * panel de controle
   */
  JPanel controlPanel = new JPanel();
  /**
   * Bordure du tableau.
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  SiporDataSimulation donnees_;

  /**
   * Indice du chenal choisi (par defaut 0 => le premier chenal.
   */
  int chenalChoisi_ = 0;

  /**
   * constructeur du panel d'affichage des bassins.
   * 
   * @param d donn�es de la simulation.
   */
  public SiporPanelReglesNavigationChenal(final SiporDataSimulation _donnees) {

    super("", true, true, true, true);

    donnees_ = _donnees;
   donnees_.addObservers(this);
    global_.setLayout(new BorderLayout());

    /**
     * Initialisation du nombre de cheneaux.
     */

    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();

    oui_
        .setToolTipText("Transforme la zone s�lectionn�e � la souris en \"oui\". Transforme automatiquement les zones sym�triques.");
    non_
        .setToolTipText("Transforme la zone s�lectionn�e � la souris en \"non\". Transforme automatiquement les zones sym�triques.");

    /**
     * Creation de la fenetre
     */

    setTitle("R�gles Navigation de croisements du chenal " + donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getNom_());
    setSize(800, 400);
    setBorder(SiporBordures.compound_);
    getContentPane().setLayout(new BorderLayout());
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);

    //final JScrollPane ascenceur = new JScrollPane(global_);

    getContentPane().add(global_, BorderLayout.CENTER);

    // controlPanel.setLayout(new GridLayout(1,8));
    controlPanel.setBorder(this.borduretab_);
    controlPanel.add(quitter_);
    controlPanel.add(oui_);
    controlPanel.add(non_);

    controlPanel.add(new JLabel("Chenal:"));
    controlPanel.add(this.ComboChoixChenal_);

    // controlPanel.add(new JLabel("validation des saisies:"));
    controlPanel.add(this.duplication_);
    controlPanel.add(this.impression_);

    controlPanel.add(validation_);

    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    /**
     * Listener du comboChoix de chenal: lors dde la selection du chenal, on affiche (rafraichis) le tableau de choix
     * des regles de navigations pour ce chenal
     */
    this.duplication_.setToolTipText("Permet de dupliquer les r�gles de navigation avec celles d'un autre chenal.");
    this.impression_
        .setToolTipText("G�n�re un fichier excel du tableau. Attention, ce bouton ne g�n�re que le tableau du chenal affich�.");

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });

    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(SiporPanelReglesNavigationChenal.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final SiporModeleExcel modele = new SiporModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.getCategoriesNavires_().getListeNavires_().size() + 2][titreColonnes_.length];

          for (int i = 0; i < titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.getCategoriesNavires_().getListeNavires_().size(); i++) {
            modele.data_[i + 2] = ndata[i];
          }

          modele.setNbRow(donnees_.getCategoriesNavires_().getListeNavires_().size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });

    this.duplication_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final String[] values = new String[donnees_.getListeChenal_().getListeChenaux_().size()];

        for (int i = 0; i < donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
          values[i] = donnees_.getListeChenal_().retournerChenal(i).getNom_();
        }

        final BuDialogChoice choix = new BuDialogChoice(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
            "Choix du chenal mod�le", "Le chenal "
                + donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getNom_()+" r�cup�re les r�gles du chenal choisi ci-dessous", values);

        final int reponse = choix.activate();
        if (reponse != -1)

        {
          if (reponse == 0) {

            final int chenal = donnees_.getListeChenal_().retourneIndice(choix.getValue());
            if (chenal != -1) {
              donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getReglesNavigation_().duplication(donnees_.getListeChenal_()
                  .retournerChenal(chenal).getReglesNavigation_().getVecteurDeVecteur_());

              affichage();
            }
          }

        } else {
          new BuDialogError(donnees_.getApplication().getApp(), SiporImplementation.INFORMATION_SOFT,
              "La duplication est impossible: le chenal choisi n'existe pas.").activate();
        }

      }
    });

    this.oui_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int[] tableauColonnes = tableau_.getSelectedColumns();
        final int[] tableauLignes = tableau_.getSelectedRows();
        if (tableauColonnes.length != 0 && tableauLignes.length != 0) {

          for (int k = 0; k < tableauLignes.length; k++) {
            for (int l = 0; l < tableauColonnes.length; l++) {
              // l oppos� de (i,j) est (j-1;i+1)
              final int i = tableauLignes[k];
              final int j = tableauColonnes[l];
              if (j != 0)// pour ne pas modifier al colonens des noms de cat�gories de navires
              {
                donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getReglesNavigation_().modification(true, i, j - 1);
                donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getReglesNavigation_().modification(true, j - 1, i);

              }

            }
          }

          // rafraichissement:
          affichage();
        }

      }
    });

    this.non_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int[] tableauColonnes = tableau_.getSelectedColumns();
        final int[] tableauLignes = tableau_.getSelectedRows();
        if (tableauColonnes.length != 0 && tableauLignes.length != 0) {

          for (int k = 0; k < tableauLignes.length; k++) {
            for (int l = 0; l < tableauColonnes.length; l++) {
              // l oppos� de (i,j) est (j-1;i+1)
              final int i = tableauLignes[k];
              final int j = tableauColonnes[l];
              if (j != 0)// pour ne pas modifier al colonens des noms de cat�gories de navires
              {
                // ndata[i][j]="non";
                // ndata[j-1][i+1]="non";
                donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getReglesNavigation_().modification(false, i, j - 1);
                donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getReglesNavigation_().modification(false, j - 1, i);

              }

            }
          }

          // rafraichissement:
          affichage();
        }
      }
    });

    this.ComboChoixChenal_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent ev) {
        // 1) enregistrement des modification poru le chanl en cours:
        miseAjourReglesNavigation();

        // 2) recuperation de l indice du chenal choisi
        // et affectation dans la variable du chenal choisi
        chenalChoisi_ = ComboChoixChenal_.getSelectedIndex();
        setTitle("Regles Navigation de croisements du chenal "
            + donnees_.getListeChenal_().retournerChenal(chenalChoisi_).getNom_());

        // 3) affichage(rafraichissement des donn�es)
        affichage();

      }

    });

    // listener du bouton de validation

    this.validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        miseAjourReglesNavigation();
        new BuDialogMessage(
            donnees_.getApplication().getApp(),
            SiporImplementation.INFORMATION_SOFT,
            "R�gles de navigation des cheneaux correctement saisies.\n Attention! un ajout de cat�gorie de navire implique\nqu'une ligne et une colonne suppl�mentaire\n � saisir apparaitront ici.\nPar d�faut, les valeurs de cette ligne\n et colonne suppl�mentaire sont initialis�es � oui.")
            .activate();
        dispose();

      }

    });

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {

    // 1)cr�ation du combo
    this.ComboChoix_ = new JComboBox();

    // 3)remplissage du tableau de gare avec toutes les donn�es des gares

    this.ComboChoix_.addItem("oui");
    this.ComboChoix_.addItem("non");

    // 4) remplissage des cheneaux dansla jcomobobox correspondante

    this.ComboChoixChenal_.removeAllItems();

    // final String[] nomCheneaux = new String[this.donnees_.getListeChenal_().getListeChenaux_().size()];
    System.out.println("nb chenaux a remplir: "+this.donnees_.getListeChenal_().getListeChenaux_().size());
    for (int i = 0; i < this.donnees_.getListeChenal_().getListeChenaux_().size(); i++) {
      this.ComboChoixChenal_.addItem(this.donnees_.getListeChenal_().retournerChenal(i).getNom_());
    }
    this.ComboChoixChenal_.repaint();
    // nomCheneaux[i]=""+this.donnees_.getListeChenal_().retournerChenal(i).getNom_();

    // creation de la jcombobox avec les donn�es des cheneaux
    // this.ComboChoixChenal_=new JComboBox(nomCheneaux);

  }

  /**
   * Methode d affichage des composants du BuTable et du tableau de combo. Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

    final int nbNavires = donnees_.getCategoriesNavires_().getListeNavires_().size();

    // nombre de colonnes= nombre de navire + nom du navire
    ndata = new Object[nbNavires][nbNavires + 1];

    titreColonnes_ = new String[nbNavires + 1];

    // initialisation des titres
    titreColonnes_[0] = "Categories: ";
    for (int i = 0; i < nbNavires; i++) {
      titreColonnes_[i + 1] = donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
    }

    /**
     * choix du remplissage: par defaut si le nombre de donnees regles de navigation du chenal selectionn� vaut 0
     */

    System.out.println("***** nb navires: " + nbNavires);
    /** on initialise le tableau avec les donn�es charg�es */
    for (int i = 0; i < nbNavires; i++) {

      // this.donnees_.getListeChenal_().retournerChenal(this.chenalChoisi_).getReglesNavigation_()
      // nom du navire
      ndata[i][0] = "  " + donnees_.getCategoriesNavires_().retournerNavire(i).getNom();
      for (int j = 0; j < nbNavires; j++) {
        /*
         * if(j>i) ndata[i][j+1]="X"; else {
         */
        // recuperation de la valeur de la case i,j
        if (this.donnees_.getListeChenal_().retournerChenal(this.chenalChoisi_).getReglesNavigation_().retournerAij(i, j)
            .booleanValue()) {
          ndata[i][j + 1] = "oui";
        } else {
          ndata[i][j + 1] = "non";
        }

        // }
      }// fin du pourparcours des colonnes

    }

    this.tableau_ = new BuTable(ndata, this.titreColonnes_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    // tableau_.setSelectionMode(selectionMode);

    tableau_.setRowSelectionAllowed(true);
    tableau_.setColumnSelectionAllowed(true);
    tableau_.setCellSelectionEnabled(true);

    // tableau_.setSelectionModel(newModel)
    tableau_.setBorder(this.borduretab_);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();

    this.global_.removeAll();

    this.global_.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    this.global_.revalidate();
    this.global_.updateUI();


    this.revalidate();
    this.updateUI();

  }

  

  void miseAjourReglesNavigation() {

    final int nbNavires = donnees_.getCategoriesNavires_().getListeNavires_().size();

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */

    System.out.println("Yes les donnees sont validees : jugee coherentes");

    /**
     * MODIFICAITON DES Regles de Navigations POUR le CHENal choisi:
     */
    for (int i = 0; i < nbNavires; i++) {
      // affichage de chacune des donn�es
      System.out.println("");

      for (int j = 0; j <= i; j++) {
        // recuperer une donn�e
        final String nomchoix = (String) this.tableau_.getModel().getValueAt(i, j + 1);

        /**
         * ajout du choix saisies dans le tableau de regles de navigation du chenal choisi
         */
        if (nomchoix.equals("non")) {
          this.donnees_.getListeChenal_().retournerChenal(this.chenalChoisi_).getReglesNavigation_().modification(false, i, j);
          this.donnees_.getListeChenal_().retournerChenal(this.chenalChoisi_).getReglesNavigation_().modification(false, j, i);
        } else {
          this.donnees_.getListeChenal_().retournerChenal(this.chenalChoisi_).getReglesNavigation_().modification(true, i, j);
          this.donnees_.getListeChenal_().retournerChenal(this.chenalChoisi_).getReglesNavigation_().modification(true, j, i);
        }

      }// fin du for parcours de chaque colonnes

    }

  }// fin du if ajout des donn�es

public void update(Observable o, Object arg) {
  if(arg.equals("chenal")||arg.equals("navire")){
  }
}



}
