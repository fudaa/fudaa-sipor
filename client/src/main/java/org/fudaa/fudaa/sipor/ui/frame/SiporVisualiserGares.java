package org.fudaa.fudaa.sipor.ui.frame;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.memoire.bu.BuButton;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sipor.structures.SiporConstantes;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelAffichageGares;
import org.fudaa.fudaa.sipor.ui.panel.SiporPanelSaisieGare;
import org.fudaa.fudaa.sipor.ui.tools.SiporBordures;
import org.fudaa.fudaa.sipor.ui.tools.SiporInternalFrame;

/**
 * Fenetre principale de saisie et d affichage des Gares on g�re toutes les donn�es relatives aux gares via ces
 * interfaces
 * 
 * @author Adrien Hadoux
 */

public class SiporVisualiserGares extends SiporInternalFrame {

  // attributs

  /*
   * Les panels
   */

  JPanel controlePanel = new JPanel();

  // panel d affichage global:
  JPanel global = new JPanel();

  /**
   * Panel de saisie des diff�rentes gares
   */
  SiporPanelSaisieGare saisiegarePanel_;

  /**
   * Panel d affichage des duff�rents bassins sous forme de Jtable necessaire pour recup�rer l indice du bateau
   * selectionn�
   */

  public SiporPanelAffichageGares affichagePanel_;

  /**
   * Ascenceur pour le jtable
   */
  JScrollPane ascenceur;
  // composants:

  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajout");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modifier");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
  public final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("detruire_22"), "Supprimer");

  public JLabel mode = new JLabel("Saisie");

  /**
   * Parametres de la simulation
   */
  SiporDataSimulation donnees_;

  
  /**
   * Constructeur de la fenetre prend en parametre les donn�es de la simulation
   * 
   * @param d donn�es de la simulation
   */
  public SiporVisualiserGares(final SiporDataSimulation d) {

    super("", true, true, true, true);

    donnees_ = d;
    setTitle("Gestion des fares");
    setSize(500, 300);
    this.getContentPane().setLayout(new BorderLayout());
    setBorder(SiporBordures.compound_);
    /**
     * tooltiptext des boutons
     */
    this.boutonSaisie_
        .setToolTipText("Permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("Permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText(SiporConstantes.toolTipQuitter);
    this.suppression_
        .setToolTipText("Permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");

    /**
     * ***************************************************************** Gestion des controles de la fenetre
     */

    this.modification_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne de la gare via la jtable
        final int numGare = affichagePanel_.tableau_.getSelectedRow();
        if (numGare == -1) {
          JOptionPane.showMessageDialog(null,
              "Erreur! Vous devez cliquer sur la gare � modifier dans le tableau d'affichage.", MessageConstants.WARN,
              JOptionPane.ERROR_MESSAGE);
        } else {

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          saisiegarePanel_.MODE_MODIFICATION(numGare);

        }// fin du if

      }
    });

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int numGare = affichagePanel_.tableau_.getSelectedRow();
        if (numGare == -1) {
          JOptionPane.showMessageDialog(null,
              "Erreur! Vous devez cliquer sur la gare � supprimer dans le tableau d'affichage.", MessageConstants.WARN,
              JOptionPane.ERROR_MESSAGE);
        } else {

          // on s occupe de la supresion des gares:
          // 1)on demande confirmation:
          final int confirmation = JOptionPane.showConfirmDialog(null, "Etes-vous certain de vouloir de supprimer la gare "
              + donnees_.getListeGare_().retournerGare(numGare) + " ?", "Suppression", JOptionPane.YES_NO_OPTION);

          if (confirmation == 0) {
        	  String nomGare =  donnees_.getListeGare_().retournerGare(numGare);
            // 2)on supprime le numero du quai correspondant a la suppression
            donnees_.getListeGare_().suppression(numGare);
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
            
            
            //-- delete element in network --//
            donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_GARE,
            		new DefaultNetworkUserObject(nomGare, true));
            
          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserGares.this.windowClosed();

      }
    });

    /**
     * ****************************************************************** Affichage des composants:
     */

    // affichage des composants:
    global.setLayout(new GridLayout(2, 1));

    // panel d affichage des donn�es du bassin
    affichagePanel_ = new SiporPanelAffichageGares(donnees_);
    this.ascenceur = new JScrollPane(this.affichagePanel_);
    global.add(this.ascenceur);

    // panel de saisie des donn�es du bassin
    this.saisiegarePanel_ = new SiporPanelSaisieGare(donnees_, this);

    final JPanel inter = new JPanel();
    inter.setLayout(new GridLayout(1, 3));
    final JPanel affichMode = new JPanel();
    affichMode.add(this.mode);
    inter.add(affichMode);

    inter.add(this.saisiegarePanel_);
    inter.add(new JLabel(""));
    inter.setBorder(SiporBordures.compound_);
    global.add(inter);

    this.getContentPane().add(global, BorderLayout.CENTER);

    // le panel de controle:

    this.controlePanel.add(quitter_);
    // this.controlePanel.add(ajout);
    this.controlePanel.add(modification_);
    // this.controlePanel.add(suppression_);

    this.controlePanel.setBorder(SiporBordures.compound_);
    this.getContentPane().add(controlePanel, BorderLayout.SOUTH);

    setVisible(true);

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    // menuFile.set.setLabel("Fichier");
    // menuOption.setLabel("Options");
    // menuFileExit.setLabel("Quitter");
    // menuInfo.setLabel("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SiporVisualiserGares.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    // gestion de la fermeture de la frame:
    /*
     * this.addWindowListener ( new WindowAdapter() { public void windowClosing(WindowEvent e) {
     * SiporVisualiserGares.this.windowClosed(); } } );
     */
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin de la fenetre de gestion des bassins!!");
    dispose();
  }

}
