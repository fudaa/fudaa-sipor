package org.fudaa.fudaa.sipor.ui.panel;

/**
 * 
 */
import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuTable;

import org.fudaa.fudaa.sipor.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;
import org.fudaa.fudaa.sipor.structures.SiporHoraire;
import org.fudaa.fudaa.sipor.structures.SiporNavire;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des Navires sous forme d un tableau: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class SiporPanelAffichageNavires extends JPanel implements Observer{

  public  String[] titreColonnes_ = { "Nom", "Priorit�", "Gare d�part", "Loi g�n�r.nav", "Nb nav.attendu", "Loi g�n�r.Erlang",
      "Long.Min", "Long.Max", "Larg.Min", "Larg.Max", "t.e.entree", "t.e.sortie",
      "Quai pref1", "Quai pref2","Quai pref3", "Mode C/D",
      "Dur�e avant shift suivant", "Tonnage minimum", "Tonnage maximum", "Ordre loi variation tonnage",
      "Cadence quai pr�f�rentiel", "Cadence autres quais", "Dur.serv.min quai pref", "Dur.serv.max quai pref",
      "Loi quai pref", "Dur.serv.min autre quai", "Dur.serv.max autre quai", "Loi quai", "Cr.ouvertur1",
      "Cr.ouvertur2","Cr.ouvertur3", "Cr.journalier1", "Cr.journalier2", "Cr.journalier3" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des quais
   */

  public  Object[][] data_ = {};

  public Object[][] ndata_;
  public  BuTable tableau_ = new BuTable(data_, titreColonnes_);
  public DefaultTableModel modele_;

  /**
   * Panel ascenceur
   */
  public JScrollPane ascenceur_;

  /**
   * Bordure du tableau
   */

  public Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  SiporDataSimulation donnees_;
  /**
   * constructeur du panel d'affichage des quais
   * 
   * @param d donn�es de la simulation
   */
  public SiporPanelAffichageNavires(final SiporDataSimulation d) {
    donnees_=d;
    //-- enregistrement de l'observer aupres de l'observable --//
    d.addObservers(this);
    setLayout(new BorderLayout());

    this.maj(d);

  }

  /**
   * Methode d'ajout d'une cellule
   */
  public void maj(final SiporDataSimulation d) {

    ndata_ = new Object[d.getCategoriesNavires_().getListeNavires_().size()][titreColonnes_.length];

    for (int i = 0; i < d.getCategoriesNavires_().getListeNavires_().size(); i++) {
      final SiporNavire nav = d.getCategoriesNavires_().retournerNavire(i);
      nav.affichage();
      int cpt=0;
      ndata_[i][cpt++] = nav.getNom();
      ndata_[i][cpt++] = "" + nav.getPriorite();
      ndata_[i][cpt++] = "" + d.getListeGare_().retournerGare(nav.getGareDepart_());
      
      if (nav.getTypeLoiGenerationNavires_() == 0) {
        ndata_[i][cpt++] = "Erlang";
        ndata_[i][cpt++] = "" + nav.getNbBateauxattendus();
        ndata_[i][cpt++] = "" + (float) nav.getLoiErlangGenerationNavire();
        // ndata_[i][33]=""+nav.loiErlangGenerationNavire;

      } else if (nav.getTypeLoiGenerationNavires_() == 1) {
        ndata_[i][cpt++] = "D�terministe";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
      } else {
        ndata_[i][cpt++] = "Journali�re";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
      }

      // ndata_[i][cpt++] = "" + nav.OrdreLoiVariationLongueur_;
      ndata_[i][cpt++] = "" + (float) nav.getLongueurMin();
      ndata_[i][cpt++] = "" + (float) nav.getLongueurMax();
      
     // ndata_[i][cpt++] = "" + nav.OrdreLoiVariationLargeur_;
      ndata_[i][cpt++] = "" + (float) nav.getLargeurMin();
      ndata_[i][cpt++] = "" + (float) nav.getLargeurMax();
      
      ndata_[i][cpt++] = "" + (float) nav.getTirantEauEntree();
      ndata_[i][cpt++] = "" + (float) nav.getTirantEauSortie();
      /*
       * if(nav.quaiPreferentiel1!=-1) ndata_[i][11]=""+d.LQuais_.retournerQuais(nav.quaiPreferentiel1).getNom_(); else
       * ndata_[i][11]="Aucun";
       */
      ndata_[i][cpt++] = d.getlQuais_().retournerQuais(nav.getQuaiPreferentiel1()).getNom();
      /*F9
      ndata_[i][cpt++] = "" + (float) nav.getDureeQuaiPref1();
	*/
      if (nav.getQuaiPreferentiel2() != -1) {
        ndata_[i][cpt++] = "" + d.getlQuais_().retournerQuais(nav.getQuaiPreferentiel2()).getNom();
      } else {
        ndata_[i][cpt++] = "Aucun";
      }

      
      /*F9
      ndata_[i][cpt++] = "" + (float) nav.getDureeQuaiPref2();;
		*/
      if (nav.getQuaiPreferentiel3() != -1) {
        ndata_[i][cpt++] = "" + d.getlQuais_().retournerQuais(nav.getQuaiPreferentiel3()).getNom();
      } else {
        ndata_[i][cpt++] = "Aucun";
      }

      /*F9
      ndata_[i][cpt++] = "" + (float) nav.getDureeQuaiPref3();
      */
      if (nav.getModeChargement() == 0) {
        ndata_[i][cpt++] = "shift";
      } else if (nav.getModeChargement() == 1) {
        ndata_[i][cpt++] = "Loi de service";
      } else {
        ndata_[i][cpt++] = "BIG ERROR!!";
      }

      // affichage du mode de chargement:
      if (nav.getModeChargement() == 0) {

        ndata_[i][cpt++] = "" + (float) nav.getDureeAttenteShiftSuivant();
        ndata_[i][cpt++] = "" + (float) nav.getTonnageMin();
        ndata_[i][cpt++] = "" + (float) nav.getTonnageMax();
        ndata_[i][cpt++] = "" + (float) nav.getOrdreLoiVariationTonnage();
        ndata_[i][cpt++] = "" + (float) nav.getCadenceQuaiPref();
        ndata_[i][cpt++] = "" + (float) nav.getCadenceQuai();

        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";

      } else if (nav.getModeChargement() == 1) {
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";
        ndata_[i][cpt++] = "";

        ndata_[i][cpt++] = "" + (float) nav.getDureeServiceMinQuaiPref();
        ndata_[i][cpt++] = "" + (float) nav.getDureeServiceMaxQuaiPref();
        ndata_[i][cpt++] = "" + nav.getOrdreLoiQuaiPref();
        ndata_[i][cpt++] = "" + (float) nav.getDureeServiceMinAutresQuais();
        ndata_[i][cpt++] = "" + (float) nav.getDureeServiceMaxAutresQuais();
        ndata_[i][cpt++] = "" + nav.getOrdreLoiAutresQuais();

      }
      ndata_[i][cpt++] = "" + SiporHoraire.format(nav.getCreneauxouverture_().semaineCreneau1HeureDep) + " � "
          + SiporHoraire.format( nav.getCreneauxouverture_().semaineCreneau1HeureArrivee);
      ndata_[i][cpt++] = "" + SiporHoraire.format( nav.getCreneauxouverture_().semaineCreneau2HeureDep) + " � "
              + SiporHoraire.format( nav.getCreneauxouverture_().semaineCreneau2HeureArrivee);
      ndata_[i][cpt++] = "" + SiporHoraire.format( nav.getCreneauxouverture_().semaineCreneau3HeureDep) + " � "
              + SiporHoraire.format( nav.getCreneauxouverture_().semaineCreneau3HeureArrivee);
          
      ndata_[i][cpt++] = "" + SiporHoraire.format( nav.getCreneauxJournaliers_().semaineCreneau1HeureDep) + " � "
          + SiporHoraire.format( nav.getCreneauxJournaliers_().semaineCreneau1HeureArrivee);
      ndata_[i][cpt++] = "" + SiporHoraire.format( nav.getCreneauxJournaliers_().semaineCreneau2HeureDep) + " � "
          + SiporHoraire.format( nav.getCreneauxJournaliers_().semaineCreneau2HeureArrivee);
      ndata_[i][cpt++] = "" + SiporHoraire.format( nav.getCreneauxJournaliers_().semaineCreneau3HeureDep) + " � "
          + SiporHoraire.format( nav.getCreneauxJournaliers_().semaineCreneau3HeureArrivee);

    }

    this.tableau_ = new BuTable(ndata_, this.titreColonnes_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    tableau_.getTableHeader();
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tableau_.setBorder(this.borduretab_);
    
    for(int i=0; i<titreColonnes_.length;i++){
    TableColumn column = tableau_.getColumnModel().getColumn(i);
           column.setPreferredWidth(150); 
    }
    
    TableColumn column = tableau_.getColumnModel().getColumn(1);
    column.setPreferredWidth(50);
    column = tableau_.getColumnModel().getColumn(2);
    column.setPreferredWidth(75);
    
    
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    this.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    this.revalidate();
    this.updateUI();
  }

public void update(Observable o, Object arg) {
  if(arg.equals("gare") || arg.equals("quai"))
    maj(donnees_);
}

}
