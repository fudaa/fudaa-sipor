package org.fudaa.fudaa.sipor.algorithmes;

import org.fudaa.dodico.corba.sipor.SParametresResultatOccupationCateg;
import org.fudaa.dodico.corba.sipor.SParametresResultatOccupationPourUnQuai;
import org.fudaa.dodico.corba.sipor.SParametresResultatsCompletSimulation;
import org.fudaa.fudaa.sipor.structures.SiporDataSimulation;

public class SiporAlgorithmeOccupationsQuais {

  /**
   * methode qui determine les occupations lineaires ainsi que les occupations � la dur�e pour chaque quais: se referer
   * a la page 31 de la notice de sipor
   * 
   * @param donnees_ donnes de la simulation
   */
  public static void calcul(final SiporDataSimulation donnees_) {

    final SParametresResultatsCompletSimulation resultats = donnees_.getParams_().ResultatsCompletsSimulation;

    // allocation memoire du tableau de taille du nombre de quais
    resultats.tableauOccupationTousQuais = new SParametresResultatOccupationPourUnQuai[donnees_.getlQuais_().getlQuais_().size()];

    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {

      // allocation memoire
      resultats.tableauOccupationTousQuais[i] = new SParametresResultatOccupationPourUnQuai();
      // allocation memoire du tableau des navires du quai i
      resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires = new SParametresResultatOccupationCateg[donnees_.getCategoriesNavires_().getListeNavires_()
          .size()];

      // initialisation des donn�es
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        // allocation memoire
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k] = new SParametresResultatOccupationCateg();
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].nbNaviresUtiliseQuai = 0;
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceMaxiAuQuai = 0;
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceMiniAuQuai = 999999;
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceTotalAuQuai = 0;
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnage = 0;
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnageMin = 999999999;
        resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnageMax = 0;

      }

    }

    for (int n = 0; n < donnees_.getListeResultatsSimu_().nombreNavires; n++) {

    	//-- on recherche l'indice du quai --//
    	int indiceTableau=-1;
    	boolean trouve=false;
    	for(int i=0;i<donnees_.getListeResultatsSimu_().listeEvenements[n].NbElemtnsParcours && !trouve;i++)
    		if(donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[i].typeElement==3)
    		{
    			indiceTableau=i;
    			trouve=true;
    		}
    	if(indiceTableau==-1){
    		System.out.println("Erreur: occupation quai: on a pas trouv�de quai! impossible...");
    	}
    	else{
    		//-- quai ttrouv�-//


    		final int quai = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].indiceElement;
    		final double duree = donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].heureSortie
    		- donnees_.getListeResultatsSimu_().listeEvenements[n].tableauTrajet[indiceTableau].heureEntree;

    		// on recupere l'indice de la cat�gorieassociee
    		final int cat = donnees_.getListeResultatsSimu_().listeEvenements[n].categorie;
    		// on incremente le nombre de navires utilisant le qui, on ajoute duree au total de temps d'occupation, et on
    		// determine le nouveau max et min.

    		resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].nbNaviresUtiliseQuai++;
    		resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tempsServiceTotalAuQuai += duree;
    		resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tonnage += donnees_.getListeResultatsSimu_().listeEvenements[n].tonnage;

    		if (donnees_.getListeResultatsSimu_().listeEvenements[n].tonnage > resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tonnageMax) {
    			resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tonnageMax = donnees_.getListeResultatsSimu_().listeEvenements[n].tonnage;
    		}

    		if (donnees_.getListeResultatsSimu_().listeEvenements[n].tonnage < resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tonnageMin) {
    			resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tonnageMin = donnees_.getListeResultatsSimu_().listeEvenements[n].tonnage;
    		}

    		if (resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tempsServiceMaxiAuQuai < duree) {
    			resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tempsServiceMaxiAuQuai = duree;
    		}

    		if (resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tempsServiceMiniAuQuai > duree) {
    			resultats.tableauOccupationTousQuais[quai].tableauOccupationQuaiParNavires[cat].tempsServiceMiniAuQuai = duree;
    		}

    	}//fin du else

    }//fin du for

    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        if (resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceMiniAuQuai == 999999) {
          resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceMiniAuQuai = 0;
        }

        if (resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnageMin == 999999999) {
          resultats.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnageMin = 0;
        }

      }
    }

  }

  
  
  
  
  /**
   * Mathode qui determine le max parmi la repartition du tonnage:
   * 
   * @param t tableau contenant les tonnages max
   * @return le maximum des tonnages max par ccat�gorie
   */
  public static float determineMaxRepartitionTonnage(final SiporDataSimulation donnees_) {
    float max = 0;

    for (int i = 0; i < donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais.length; i++) {
      for (int k = 0; k < donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires.length; k++) {
        if (donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnageMax > max) {
          max = (float) donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnageMax;
        }
      }
    }

    return (float) (max + 0.2 * max);
  }

  public static float determineNbNaviresMax(final SiporDataSimulation donnees_) {
    float max = 0;
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        if (donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].nbNaviresUtiliseQuai > max) {
          max = donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].nbNaviresUtiliseQuai;
        }
      }
    }
    return (float) (max + 0.25 * max);
  }

  public static float determineTonnageMax(final SiporDataSimulation donnees_) {
    float max = 0;
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        if (donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnage > max) {
          max = (float) donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tonnage;
        }
      }
    }
    return (float) (max + 0.25 * max);
  }

  public static float determineTempsServiceMax(final SiporDataSimulation donnees_) {
    float max = 0;
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        if (donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceMaxiAuQuai > max) {
          max = (float) donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceMaxiAuQuai;
        }
      }
    }
    return (float) (max + 0.25 * max);
  }

  public static float determineTempsServiceTotalMax(final SiporDataSimulation donnees_) {
    float max = 0;
    for (int i = 0; i < donnees_.getlQuais_().getlQuais_().size(); i++) {
      for (int k = 0; k < donnees_.getCategoriesNavires_().getListeNavires_().size(); k++) {
        if (donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceTotalAuQuai > max) {
          max = (float) donnees_.getParams_().ResultatsCompletsSimulation.tableauOccupationTousQuais[i].tableauOccupationQuaiParNavires[k].tempsServiceTotalAuQuai;
        }
      }
    }
    return (float) (max + 0.25 * max);
  }

}
