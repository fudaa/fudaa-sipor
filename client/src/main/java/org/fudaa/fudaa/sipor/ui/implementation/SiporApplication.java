/*
 * @file         SiporApplication.java
 * @creation     1999-10-01
 * @modification $Date: 2007-03-07 17:25:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sipor.ui.implementation;


import com.memoire.bu.BuApplication;

import org.fudaa.fudaa.sipor.SiporImplementation;

/**
 * L'application cliente Sipor.
 * 
 * @version $Revision: 1.5 $ $Date: 2007-03-07 17:25:03 $ by $Author: deniger $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class SiporApplication extends BuApplication {
  public SiporApplication() {
    super();
    setImplementation(new SiporImplementation());
  }
}
