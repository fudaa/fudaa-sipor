/*
 * @file         TestSiporClient.java
 * @creation     1999-09-23
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;
import org.fudaa.dodico.corba.sipor.ICalculSipor;
import org.fudaa.dodico.corba.sipor.ICalculSiporHelper;
import org.fudaa.dodico.objet.CDodico;
/**
 * Une classe de test pour un client de Sipor. Elle tente de se connecter � "un-serveur-sipor".
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 14:45:51 $ by $Author: deniger $
 * @author       Nicolas Chevalier , Bertrand Audinet
 */
public final class MainSiporClient {
  private MainSiporClient() {}
  public static void main(final String[] _args) {
    ICalculSipor sipor= null;
    System.out.println("SiporClient");
    int essai= 0;
    do {
      System.err.println("  essai " + (++essai));
      sipor=
        ICalculSiporHelper.narrow(CDodico.findServerByName("un-serveur-sipor"));
    } while (sipor == null);
    System.out.println("sipor trouv�: " + sipor);
  }
}
