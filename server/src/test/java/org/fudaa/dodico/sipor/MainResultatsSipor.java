/*
 * @file         testResultatsSipor.java
 * @creation     1999-09-23
 * @modification $Date: 2006-12-18 18:29:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;
import java.util.Date;

import org.fudaa.dodico.corba.sipor.SResultatsBateauGenere;
import org.fudaa.dodico.corba.sipor.SResultatsIterations;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-12-18 18:29:06 $ by $Author: hadouxad $
 * @author       Nicolas Chevalier , Bertrand Audinet
 */
public final class MainResultatsSipor {
  private MainResultatsSipor() {}
  public static void afficheBateau(final SResultatsBateauGenere bateau) {
    System.out.println(bateau.numeroBateau + " : " + bateau.heureGeneration);
  }
  public static void main(final String[] args) {
    int cpt= 0;
    final DResultatsSipor res= new DResultatsSipor();
    res.fichier("testResultats");
    final Date t1= new Date();
    final SResultatsIterations resIterations=null/*= res.litResultatsSipor()*/;
    final Date t2= new Date();
    for (int i= 0; i < resIterations.resultatsIterations.length; i++) {
      for (int j= 0;
        j < resIterations.resultatsIterations[i].resultatsIteration.length;
        j++) {
        //          afficheBateau( resIterations.resultatsIterations[i].resultatsIteration[j]);
        cpt++;
      }
    }
    final long temps= t2.getTime() - t1.getTime();
    System.out.println("Nombre de bateaux : " + cpt);
    System.out.println("Temps nécessaire : " + temps + " millisecondes");
  }
}
