/*
 * @file         testSipor.java
 * @creation     1999-09-23
 * @modification $Date: 2006-11-27 17:19:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;

import org.fudaa.dodico.corba.sipor.SParametresBassin;
import org.fudaa.dodico.corba.sipor.SParametresCategorie;
import org.fudaa.dodico.corba.sipor.SParametresCategories;
import org.fudaa.dodico.corba.sipor.SParametresCreneauHoraire;
import org.fudaa.dodico.corba.sipor.SParametresCreneauMaree;
import org.fudaa.dodico.corba.sipor.SParametresCreneauMareeDansBassin;
import org.fudaa.dodico.corba.sipor.SParametresCreneauxHorairesDansBassin;
import org.fudaa.dodico.corba.sipor.SParametresDonneesGenerales;
import org.fudaa.dodico.corba.sipor.SParametresEcluse;
import org.fudaa.dodico.corba.sipor.SParametresEcluseDansBassin;
import org.fudaa.dodico.corba.sipor.SParametresMaree;
import org.fudaa.dodico.corba.sipor.SParametresPeriode;
import org.fudaa.dodico.corba.sipor.SParametresPerturbateur;
import org.fudaa.dodico.corba.sipor.SParametresQuai;
import org.fudaa.dodico.corba.sipor.SParametresQuaiPreferentiel;
import org.fudaa.dodico.corba.sipor.SParametresQuais;
import org.fudaa.dodico.corba.sipor.SParametresReglesSecurite;
import org.fudaa.dodico.corba.sipor.SParametresReglesSecuriteBassin;
import org.fudaa.dodico.corba.sipor.SParametresSipor;

/**
 * Permet de tester l'�criture du fichier de param�tres. Cette classe cr�� une structure pour les param�tres de Sipor
 * avec des valeurs fixes et demande l'�criture du fichier de param�tres en appelant la m�thode correspondante de
 * CParametresSipor.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-11-27 17:19:40 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public final class MainSipor {

  private MainSipor() {}

  public static void main(final String[] _args) {
    final int quaiMax = 20;
    final int categorieMax = 30;
    final int quaiPreferentielMax = 3;
    final int periodeMax = 3;
    final SParametresSipor params = new SParametresSipor();
    // Donn�es g�n�rales pour un test d'�criture
    final SParametresDonneesGenerales dg = new SParametresDonneesGenerales();
    dg.titre1 = "Titre 1";
    dg.titre2 = "Titre 2";
    dg.titre3 = "Titre 3";
    dg.nombrePassages = 1;
    dg.graineDepart = 2;
    dg.arretProgApresRappelDonnees = true;
    dg.impressionRappelDonnees = false;
    dg.impressionPassagesIntermediaires = false;
    dg.premierJourSimulation = 0;
    dg.heureDebutReelSimulation = 10;
    dg.heureFinSimulation = 100;
    dg.ecretageTempsAttente = 60;
    dg.arrondiHeuresReglesPriorite = 6;
    params.donneesGenerales = dg;
    // bassins pour un test d'�criture
    final SParametresBassin[] bassins = new SParametresBassin[3];
    for (int i = 0; i < 3; i++) {
      final SParametresBassin bassin = new SParametresBassin();
      final SParametresEcluseDansBassin ecluseDansBassin = new SParametresEcluseDansBassin(false,
          new SParametresEcluse(6, 7, 8, 9, 10));
      final SParametresCreneauMareeDansBassin creneauMareeDansBassin = new SParametresCreneauMareeDansBassin(false,
          new SParametresCreneauMaree(11, 12));
      final SParametresCreneauHoraire[] creneauxHoraires = new SParametresCreneauHoraire[0];
      for (int j = 0; j < 0; j++) {
        creneauxHoraires[j] = new SParametresCreneauHoraire(13, 14);
      }
      final SParametresCreneauxHorairesDansBassin creneauxHorairesDansBassin = new SParametresCreneauxHorairesDansBassin(
          0, creneauxHoraires);
      bassin.profondeurChenal = 1;
      bassin.tempsChenalage = 2;
      bassin.hauteurEau = 3;
      bassin.dureeManoeuvreEntree = 4;
      bassin.dureeManoeuvreSortie = 5;
      bassin.ecluse = ecluseDansBassin;
      bassin.creneauMaree = creneauMareeDansBassin;
      bassin.creneauxHoraires = creneauxHorairesDansBassin;
      bassins[i] = bassin;
    }
    params.bassin1 = bassins[0];
    params.bassin2 = bassins[1];
    params.bassin3 = bassins[2];
    // creation de tous les quais
    final SParametresQuais q = new SParametresQuais();
    q.nombreQuais = quaiMax;
    q.quais = new SParametresQuai[quaiMax];
    for (int i = 0; i < quaiMax; i++) {
      final int j = i + 1;
      q.quais[i] = new SParametresQuai("quai n" + j, j, true, j);
    }
    // mar�e pour un test d'�criture
    final SParametresMaree maree = new SParametresMaree(15, 16, 17, 18, 19, 20, 21);
    params.maree = maree;
    // creation de 2 categories
    final SParametresCategories cat = new SParametresCategories();
    cat.nombreCategories = 30;
    cat.piedPilote = 10;
    cat.categories = new SParametresCategorie[categorieMax];
    // creation des quais preferentiels
    final SParametresQuaiPreferentiel[] qp = new SParametresQuaiPreferentiel[quaiPreferentielMax];
    for (int i = 0; i < quaiPreferentielMax; i++) {
      final int j = i + 1;
      qp[i] = new SParametresQuaiPreferentiel(j, j, j, j, j, j);
    }
    // creation des periodes
    final SParametresPeriode[] per = new SParametresPeriode[periodeMax];
    for (int i = 0; i < periodeMax; i++) {
      final int j = i + 1;
      per[i] = new SParametresPeriode(j, j, j, j, j, j, j);
    }
    // creation de 30 categories
    for (int i = 0; i < categorieMax; i++) {
      final int j = i + 1;
      cat.categories[i] = new SParametresCategorie("bateau n�" + j, j, j, j, j, j, quaiPreferentielMax, qp, periodeMax,
          per);
    }
    params.categories = cat;
    // creation du perturbateur
    final SParametresPerturbateur p = new SParametresPerturbateur(150, 2, 1, 50);
    params.quais = q;
    params.perturbateur = p;
    // r�gles de s�curit�
    final long[] entree = { 1, 2, 3, 4, 5, 6, 7, 8 };
    final long[] sortie = { 9, 10, 11, 12, 13, 14, 15, 16 };
    final SParametresReglesSecuriteBassin rsPE = new SParametresReglesSecuriteBassin(entree, sortie);
    final SParametresReglesSecuriteBassin rsB1 = new SParametresReglesSecuriteBassin(entree, sortie);
    final SParametresReglesSecuriteBassin rsB2 = new SParametresReglesSecuriteBassin(entree, sortie);
    final SParametresReglesSecuriteBassin rsB3 = new SParametresReglesSecuriteBassin(entree, sortie);
    final SParametresReglesSecurite rs = new SParametresReglesSecurite(rsPE, rsB1, rsB2, rsB3);
    params.reglesSecurite = rs;
  }
}
