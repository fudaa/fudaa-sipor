/*
 * @file         BTest.java
 * @creation     1999-09-23
 * @modification $Date: 2006-11-27 17:19:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;
import org.fudaa.dodico.corba.sipor.SParametresCategorie;
import org.fudaa.dodico.corba.sipor.SParametresCategories;
import org.fudaa.dodico.corba.sipor.SParametresPeriode;
import org.fudaa.dodico.corba.sipor.SParametresPerturbateur;
import org.fudaa.dodico.corba.sipor.SParametresQuai;
import org.fudaa.dodico.corba.sipor.SParametresQuaiPreferentiel;
import org.fudaa.dodico.corba.sipor.SParametresQuais;
import org.fudaa.dodico.corba.sipor.SParametresSipor;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-11-27 17:19:39 $ by $Author: hadouxad $
 * @author       Nicolas Chevalier , Bertrand Audinet
 */
public final class BTest {
  private BTest() {}
  public static void main(final String[] _argv) {
    final int quaiMax= 20;
    final int categorieMax= 30;
    final int quaiPreferentielMax= 3;
    final int periodeMax= 3;
    final SParametresSipor params= new SParametresSipor();
    System.out.println("\nTest ecriture fichier pour bertrand");
    // creation de tous les quais
    final SParametresQuais q= new SParametresQuais();
    q.nombreQuais= quaiMax;
    q.quais= new SParametresQuai[quaiMax];
    for (int i= 0; i < quaiMax; i++) {
      final int j= i + 1;
      q.quais[i]= new SParametresQuai("quai n�" + j, j, true, j);
    }
    // creation du perturbateur
    final SParametresPerturbateur p= new SParametresPerturbateur(150, 2, 1, 50);
    params.quais= q;
    params.perturbateur= p;
    // creation de 2 categories
    final SParametresCategories cat= new SParametresCategories();
    cat.nombreCategories= 30;
    cat.piedPilote= 10;
    cat.categories= new SParametresCategorie[categorieMax];
    //  creation des quais preferentiels
    final SParametresQuaiPreferentiel[] qp=
      new SParametresQuaiPreferentiel[quaiPreferentielMax];
    for (int i= 0; i < quaiPreferentielMax; i++) {
      final int j= i + 1;
      qp[i]= new SParametresQuaiPreferentiel(j, j, j, j, j, j);
    }
    // creation des periodes
    final SParametresPeriode[] per= new SParametresPeriode[periodeMax];
    for (int i= 0; i < periodeMax; i++) {
      final int j= i + 1;
      per[i]= new SParametresPeriode(j, j, j, j, j, j, j);
    }
    // creation de 30 categories
    for (int i= 0; i < categorieMax; i++) {
      final int j= i + 1;
      cat.categories[i]=
        new SParametresCategorie(
          "bateau n�" + j,
          j,
          j,
          j,
          j,
          j,
          quaiPreferentielMax,
          qp,
          periodeMax,
          per);
    }
    params.categories= cat;
  //  DParametresSipor.ecritParametresSipor("test", params);
    // }
  }
}
