package org.fudaa.dodico.sipor;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.dodico.corba.sipor.SParametresCerclesGenes;
import org.fudaa.dodico.corba.sipor.SParametresNavires2;
import org.fudaa.dodico.corba.sipor.SParametresSiporGene;
import org.fudaa.dodico.fortran.FortranWriter;

public class GenerationFichierGene {

  public int getNbNavires() {
    return 6;
  }

  public int getNbCercles() {
    return 3;
  }

  public static double genereValeureMatriceGene() {
    // -- AHX - g�n�ration nb inf�rieur a 99.99 --//
    return Math.min(99.99, Math.random() * 100d);
  }

  public static void generationFichier(int nbNavires, int nbCercles, java.io.File f) {
    FortranWriter writer = null;
    try {
      writer = new FortranWriter(new FileWriter(f));
      generationFichier(nbNavires, nbCercles, writer);

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (writer != null) {
        try {
          writer.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static void generationFichier(int nbNavires, int nbCercles, FortranWriter writer) {

    SParametresSiporGene structure = new SParametresSiporGene();
    try {
      writer.writeln("test generateur fichier genes");
    } catch (IOException e) {
      e.printStackTrace();
    }

    structure.navires = new SParametresNavires2();
    structure.navires.nombreNavires = nbNavires;

    structure.cercles = new SParametresCerclesGenes();
    structure.cercles.nbCercles = nbCercles;
    structure.cercles.listeCercles = new org.fudaa.dodico.corba.sipor.SParametresCercleGene[nbCercles];

    // -- g�n�ration de la matricre de donn�e random --//
    for (int i = 0; i < nbCercles; i++) {
      structure.cercles.listeCercles[i] = new org.fudaa.dodico.corba.sipor.SParametresCercleGene();
      structure.cercles.listeCercles[i].dimensionMatriceReglesNav = nbNavires;
      structure.cercles.listeCercles[i].matriceGenesNavigation = new double[nbNavires][nbNavires];
      for (int j = 0; j < nbNavires; j++)
        for (int k = 0; k < nbNavires; k++) {

          // -- AHX - g�n�ration nb inf�rieur a 99.99 --//
          double val = genereValeureMatriceGene();
          structure.cercles.listeCercles[i].matriceGenesNavigation[j][k] = val;
        }

    }

    // -- ahx - genes cercles --//
    try {
      DParametresSipor.ecritDonneesGenesCercle(structure, writer);
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public static void main(String[] args) {
    if (args == null || args.length != 3) {
      printHelp();
      return;
    }
    String file = null;
    int nbNavires = -1;
    int nbCercles = -1;
    String filePrefix = "-file=";
    String navirePrefix = "-nbNavires=";
    String cerclePrefix = "-nbCercles=";
    for (int i = 0; i < args.length; i++) {
      String arg = args[i].trim();
      String readString = getArg(filePrefix, arg);
      if (readString != null) {
        file = readString;
      }
      int readInt = getArgInt(navirePrefix, arg);
      if (readInt > 0) {
        nbNavires = readInt;
      }
      readInt = getArgInt(cerclePrefix, arg);
      if (readInt > 0) {
        nbCercles = readInt;
      }
    }
    if (file == null || nbCercles <= 0 || nbNavires <= 0) {
      printHelp();
      return;
    }
    File f = new File(file);
    if (f.exists() && !f.canWrite()) {
      System.err.println("Le fichier existe et ne peut pas �tre �cras�");
      System.err.println("Fichier = " + f.getAbsolutePath());
    }
    if (f.getParentFile() != null) {
      f.getParentFile().mkdirs();
    }
    generationFichier(nbNavires, nbCercles, f);
    System.out.println("Generation termin�e");
    System.out.println("Fichier de sortie = " + f.getAbsolutePath());
    System.out.println("Nombre de navires = " + nbNavires);
    System.out.println("Nombre de cercles = " + nbCercles);

  }

  private static int getArgInt(String prefix, String in) {
    String res = getArg(prefix, in);
    if (res != null) {
      try {
        return Integer.valueOf(res);
      } catch (NumberFormatException e) {
        return -1;
      }
    }
    return -1;
  }

  private static String getArg(String filePrefix, String in) {
    if (in.startsWith(filePrefix) && in.length() > filePrefix.length()) { return in.substring(filePrefix.length()); }
    return null;
  }

  private static void printHelp() {
    System.err.println("vous devez specifier au moins 3 param�tres");
    System.err.println("-file=<nomDuFichier> -nbNavires=<nbPositif> -nbCercles=<nbPositif>");
    System.err.println("Par exemple");
    System.err.println("-file=exemple.txt -nbNavires=5 -nbCercles=3");
  }
}
