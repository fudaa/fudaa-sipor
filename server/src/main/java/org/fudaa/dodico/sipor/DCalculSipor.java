/**
 * @file         DCalculSipor.java
 * @creation     1999-09-17
 * @modification $Date: 2007-09-03 10:18:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;
import java.io.File;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sipor.ICalculSipor;
import org.fudaa.dodico.corba.sipor.ICalculSiporOperations;
import org.fudaa.dodico.corba.sipor.IParametresSipor;
import org.fudaa.dodico.corba.sipor.IParametresSiporHelper;
import org.fudaa.dodico.corba.sipor.SParametresSiporGene;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * Encapsulation du programme sipor5 ecrit en Fortran. Permet aux classes qui
 * l'utilisent de lancer une simulation. Pour cela il faut lui transmettre les
 * param�tres, invoquer la m�thodes "calcule" et �ventuellement r�cup�rer les
 * r�sultats. Elle fonctionne correctement en local.
 *
 * @version      $Revision: 1.15 $ $Date: 2007-09-03 10:18:36 $ by $Author: hadouxad $
 * @author       Nicolas Chevalier
 */
public class DCalculSipor extends DCalcul implements ICalculSipor,ICalculSiporOperations {
  public DCalculSipor() {
    super();
    setFichiersExtensions(new String[] { ".dat", ".out", ".tmp", ".sip", });
  }
  public final  Object clone() throws CloneNotSupportedException{
    return new DCalculSipor();
  }
  public String toString() {
    return "DCalculSipor()";
  }
  public String description() {
    return "Sipor, serveur de calcul pour le trafic d'un port"
      + super.description();
  }
  public void calcul(final IConnexion c) {
	  
    final IParametresSipor params= IParametresSiporHelper.narrow(parametres(c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    
    
    log(c, "lancement du calcul");
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    final String fichier= "sipor" + c.numero();
    try {
      
     
      //-- Ecriture des fichiers sipor --//
      final SParametresSiporGene p2= new SParametresSiporGene();
      
      DParametresSipor.ecritParametresSipor(path + fichier, p2);
      System.out.println("Appel de l'executable sipor");
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd= new String[4];
        cmd[0]= path + "sipor-win.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1]= path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2]= path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[1]= "fake_cmd";
          cmd[2]= path;
        }
        cmd[3]= fichier;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
      } else {
        cmd= new String[3];
        cmd[0]= path + "sipor.sh";
        cmd[1]= path;
        cmd[2]= fichier;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2]);
      }
      final CExec ex= new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      ex.exec();
      System.out.println("Fin du calcul");
      
      log(c, "calcul termin�");
    } catch (final Exception ex) {
      log(c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    } finally {
      new File("fort.10").deleteOnExit();
    }
  }
  
  
  public  void executeSiporGenarr(String pathEtude){
	  System.out.println("Appel de l'executable sipor");
	  final String os= System.getProperty("os.name");
	  final String path= cheminServeur();

	  String[] cmd;

	  try {
		  if (os.startsWith("Windows")) {
			  cmd= new String[4];
			  cmd[0]= path + "siporGenarr-win.exe";
			  if (path.indexOf(':') != -1) {
				  // lettre de l'unite (ex: "C:")
				  cmd[1]= path.substring(0, path.indexOf(':') + 1);
				  // chemin du serveur
				  cmd[2]= path.substring(path.indexOf(':') + 1);
			  } else {
				  // si pas de lettre dans le chemin
				  cmd[1]= "fake_cmd";
				  cmd[2]= path;
			  }
			  cmd[3]= pathEtude;
			  System.out.println("**\nLa commande ex�cut�e est: \n "+cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3]+"\n**");
		  } else {
			  cmd= new String[3];
			  cmd[0]= path + "siporGenarr-linux.x";
			  cmd[1]= path;
			  cmd[2]= pathEtude;
			  System.out.println("**\nLa commande ex�cut�e est: \n "+cmd[0] + " " + cmd[1] + " " + cmd[2]+"\n**");
		  }
		  final CExec ex= new CExec();
		  ex.setCommand(cmd);
		  ex.setOutStream(System.out);
		  ex.setErrStream(System.err);
		  ex.exec();
		  System.out.println("Fin du calcul");


	  } catch (final Exception ex) {

		  CDodico.exceptionAxel(this, ex);
	  } finally {
		  new File("fort.10").deleteOnExit();
	  }


  }

  
}
