/**
 * @file         DResultatsSipor.java
 * @creation     1999-09-23
 * @modification $Date: 2007-09-03 10:18:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;
import java.io.EOFException;
import java.io.FileReader;
import java.util.ArrayList;

import com.memoire.fu.FuLog;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.sipor.IResultatsSipor;
import org.fudaa.dodico.corba.sipor.IResultatsSiporOperations;
import org.fudaa.dodico.corba.sipor.SiporResultatListeevenementsSimu;
import org.fudaa.dodico.corba.sipor.SiporResultatsDonneeTrajet;
import org.fudaa.dodico.corba.sipor.SiporResultatsSimulation;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.objet.CDodico;
/**
 * Permet d'acc�der aux r�sultats d'une simulation.
 *
 * @version      $Revision: 1.16 $ $Date: 2007-09-03 10:18:36 $ by $Author: hadouxad $
 * @author       Nicolas Chevalier
 */
public class DResultatsSipor
  extends DResultats
  implements IResultatsSipor,IResultatsSiporOperations {
  private String fichier_;
  public DResultatsSipor() {
    super();
    fichier_= "";
  }
  public final  Object clone() throws CloneNotSupportedException {
    final DResultatsSipor r= new DResultatsSipor();
    r.fichier(fichier_);
    return r;
  }
  public String toString() {
    return "DResultatsSipor(" + fichier_ + ")";
  }
  public String fichier() {
    return fichier_;
  }
  /**
   * Mutateur de fihcier. Ajoute "_histo.dat" � _fichier.
   *
   * @param _fichier
   */
  public void fichier(final String _fichier) {
    fichier_= _fichier + /*"_histo.dat"*/".his";
  }
  /**
   * Pour acc�der aux r�sultats.
   *
   * @return r�sultats
   */
  public SiporResultatListeevenementsSimu litResultatsSipor() {
    return litResultatsSipor(fichier());
  }
  /**
   * Lit un fichier de r�sultats et renvoie une instance de SResultatsIterations
   * correspondant aux donn�es lues.
   *
   * @param nomFichier
   * @return r�sultats
   */
  public static double arrondire2decimales(double val)
  {
	  val=val*100;
	  val=(int)(val+0.5);
	  val=val/100.0;
	  return val;
  }
  
  public static SiporResultatListeevenementsSimu litResultatsSipor(final String nomFichier) {
    try {
     FuLog.debug("Lecture de " + nomFichier);
    
      SiporResultatListeevenementsSimu listeResultats= new SiporResultatListeevenementsSimu();
      
      //vecteur dynamique qui servira a stocker chaque navire en attendant de connaitrez le nombre exact de navires
      ArrayList vecteurListe=new ArrayList();
      
      //structure finale qui contiendra la totalit� des navires
      FortranReader fr= new FortranReader(new FileReader(nomFichier));
     
      try {
          while (true) {
        	  fr.readFields(); 
        	  
        	  if (fr.getNumberOfFields() > 2) {
        		  	//recopiage des parametres dans le champs
        		 
        		  //allocation m�moirepour les donn�es d un anvire
        		  SiporResultatsSimulation donneesNavire=new SiporResultatsSimulation();  
        		 FuLog.debug("num cat: "+fr.intField(0)+"\n categorie: "+(fr.intField(1)-1)+"\n longueur: "+fr.doubleField(2)+"\n tonnage: "+fr.doubleField(3)+ "\n nb elements trajet: "+fr.intField(4));
        		  //recopiage des donn�es du fichier dans la struture donneesNavire
        		  donneesNavire.numero=fr.intField(0);
        		  donneesNavire.categorie=fr.intField(1)-1;
        		  donneesNavire.longueur=fr.doubleField(2);
        		  donneesNavire.tonnage=fr.doubleField(3);
        		  donneesNavire.NbElemtnsParcours=fr.intField(4);
        		  //allocation memoire pour le tableau de trajet a partir du nombre d elements:
        		  donneesNavire.tableauTrajet=new SiporResultatsDonneeTrajet[donneesNavire.NbElemtnsParcours];
        		 FuLog.debug("trajet: ");
        		  //lecture de la liste ddu trajet du navire donn�
        		  //int indiceDonnee=5;
        		      for(int i=0;i<donneesNavire.NbElemtnsParcours;i++)
        		      {
        		    	 
        		    	  
        		    	  //-- Variante suite aux modifications du fichier historique --//
        		    	  
        		    	  //-- lecture de la ligne contenant l'�l�ment parcouru
        		    	  fr.readFields(); 
        		    	  
        		    	  //allocation memoire pour un element du trajet:
        		    	  donneesNavire.tableauTrajet[i]=new SiporResultatsDonneeTrajet();
        		    	  //donnees associ�es � un �l�ment
        		    	  String typeElement=fr.stringField(0);
        		    	  if(typeElement.equals("T") ||typeElement.equals("t"))
            		    	  donneesNavire.tableauTrajet[i].typeElement=0;
        		    	  if(typeElement.equals("C") ||typeElement.equals("c"))
            		    	  donneesNavire.tableauTrajet[i].typeElement=1;
        		    	  if(typeElement.equals("E") ||typeElement.equals("e"))
            		    	  donneesNavire.tableauTrajet[i].typeElement=2;
        		    	  if(typeElement.equals("Q") ||typeElement.equals("q"))
            		    	  donneesNavire.tableauTrajet[i].typeElement=3;
        		    	 
        		    	 FuLog.debug("\n indice element: "+fr.intField(1));
          		    	  donneesNavire.tableauTrajet[i].indiceElement=fr.intField(1)-1;
        		    	  
        		    	 FuLog.debug("\n heure entree: "+fr.doubleField(2));
          		    	  donneesNavire.tableauTrajet[i].heureEntree=fr.doubleField(2);
        		    	  
          		    	 FuLog.debug("\n heure sortie: "+fr.doubleField(3));
          		    	  donneesNavire.tableauTrajet[i].heureSortie=fr.doubleField(3);
          		    	  
          		    	  donneesNavire.tableauTrajet[i].acces=fr.doubleField(4);
        		    	  
        		    	 FuLog.debug("\n attente mar�e: "+fr.doubleField(5));
        		    	  donneesNavire.tableauTrajet[i].marees=fr.doubleField(5);
        		    	  
        		    	 FuLog.debug("\n attente securite: "+fr.doubleField(6));
        		    	  donneesNavire.tableauTrajet[i].secu=fr.doubleField(6);
        		    	  
        		    	 FuLog.debug("\n attente occupation: "+fr.doubleField(7));
        		    	  donneesNavire.tableauTrajet[i].occupation=fr.doubleField(7);
        		    	  
        		    	 FuLog.debug("\n attente indisponibilit�: "+fr.doubleField(7));
        		    	  donneesNavire.tableauTrajet[i].indispo=fr.doubleField(7);
        		    	  
        		    	  
        		    	  
        		      }
        		  
        		  //ajout des donn�esnavire dans le vecteur
        		  vecteurListe.add(donneesNavire);
        		  
        	  }
        	  else
        	  {
        		  //on quitte
        		  //on recopie le contenue ainsi que le nombre de navire du vecteur dans al structure finale que lon renvoie ensuite
        		  
        		  //on connais le nombre de navires: il s agit du nombre d elements du vecteur
        		  listeResultats.nombreNavires=vecteurListe.size();
        		  
        		  //allocation memoire du tableau de navires
        		  listeResultats.listeEvenements=new SiporResultatsSimulation[vecteurListe.size()];
        		  
        		  for(int i=0;i<vecteurListe.size();i++)
        		  {
        			  listeResultats.listeEvenements[i]=(SiporResultatsSimulation) vecteurListe.get(i); 
        		  }
        		  
        		  
        		  //renvoie de la structure finale:
        		  return listeResultats;
        		  
        	  }
          }
          }
          catch (final EOFException e) {
        	 FuLog.debug("fin de fichier");
//        	on connais le nombre de navires: il s agit du nombre d elements du vecteur
    		  listeResultats.nombreNavires=vecteurListe.size();
    		  
    		  //allocation memoire du tableau de navires
    		  listeResultats.listeEvenements=new SiporResultatsSimulation[vecteurListe.size()];
    		  
    		  for(int i=0;i<vecteurListe.size();i++)
    		  {
    			  listeResultats.listeEvenements[i]=(SiporResultatsSimulation) vecteurListe.get(i); 
    		  }
    		  
    		  
    		  //renvoie de la structure finale:
    		  return listeResultats;
          
          }
         
    
    } catch (final Exception ex) {
      CDodico.exception(DResultatsSipor.class, ex);
    }
    return null;
  }
}
