/**
 * @file         DParametresSipor.java
 * @creation     1999-09-17
 * @modification $Date: 2007-09-05 08:27:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sipor;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.sipor.IParametresSipor;
import org.fudaa.dodico.corba.sipor.IParametresSiporOperations;
import org.fudaa.dodico.corba.sipor.SParametresBassin;
import org.fudaa.dodico.corba.sipor.SParametresBassins2;
import org.fudaa.dodico.corba.sipor.SParametresCategories;
import org.fudaa.dodico.corba.sipor.SParametresCerclesGenes;
import org.fudaa.dodico.corba.sipor.SParametresCheneaux2;
import org.fudaa.dodico.corba.sipor.SParametresDonneesGenerales;
import org.fudaa.dodico.corba.sipor.SParametresDonneesGenerales2;
import org.fudaa.dodico.corba.sipor.SParametresEcluses2;
import org.fudaa.dodico.corba.sipor.SParametresGares;
import org.fudaa.dodico.corba.sipor.SParametresMaree;
import org.fudaa.dodico.corba.sipor.SParametresMaree2;
import org.fudaa.dodico.corba.sipor.SParametresNavires2;
import org.fudaa.dodico.corba.sipor.SParametresPerturbateur;
import org.fudaa.dodico.corba.sipor.SParametresQuais;
import org.fudaa.dodico.corba.sipor.SParametresQuais2;
import org.fudaa.dodico.corba.sipor.SParametresReglesSecurite;
import org.fudaa.dodico.corba.sipor.SParametresSipor2;
import org.fudaa.dodico.corba.sipor.SParametresSiporGene;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Les parametres de sipor.
 * 
 * @version $Revision: 1.23 $ $Date: 2007-09-05 08:27:50 $ by $Author: hadouxad $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class DParametresSipor extends DParametres implements IParametresSipor, IParametresSiporOperations {
  private SParametresDonneesGenerales donneesGenerales_;
  private SParametresBassin bassin1_;
  private SParametresBassin bassin2_;
  private SParametresBassin bassin3_;
  private SParametresQuais quais_;
  private SParametresMaree maree_;
  private SParametresCategories categories_;
  private SParametresPerturbateur perturbateur_;
  private SParametresReglesSecurite reglesSecurite_;
  
  
  /**
   * Donn�es sipor 2006.
   */
  private SParametresSipor2 parametresSipor_;
  
  
  
  
  public DParametresSipor() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DParametresSipor();
  }

  public String toString() {
    final String s = "DParametresSipor";
    return s;
  }

  public SParametresDonneesGenerales donneesGenerales() {
    return donneesGenerales_;
  }

  /**
   * Mutateur des donn�es g�n�rales.
   * 
   * @param arg
   */
  public void donneesGenerales(final SParametresDonneesGenerales arg) {
    donneesGenerales_ = arg;
  }

  public SParametresBassin bassin1() {
    return bassin1_;
  }

  /**
   * @param _arg
   */
  public void bassin1(final SParametresBassin _arg) {
    bassin1_ = _arg;
  }

  /**
   * @return bassin 2.
   */
  public SParametresBassin bassin2() {
    return bassin2_;
  }

  /**
   * Mutateur du bassin2.
   * 
   * @param arg
   */
  public void bassin2(final SParametresBassin arg) {
    bassin2_ = arg;
  }

  /**
   * @return bassin 3.
   */
  public SParametresBassin bassin3() {
    return bassin3_;
  }

  /**
   * @param arg bassin3.
   */
  public void bassin3(final SParametresBassin arg) {
    bassin3_ = arg;
  }

  public SParametresQuais quais() {
    // Accesseur de quais
    return quais_;
  }

  public void quais(final SParametresQuais q) {
    // Mutateur de quais
    quais_ = q;
  }

  /**
   * @return Accesseur de maree.
   */
  public SParametresMaree maree() {
    return maree_;
  }

  /**
   * @param arg maree
   */
  public void maree(final SParametresMaree arg) {
    maree_ = arg;
  }

  public SParametresCategories categories() {
    // Accesseur de categories
    return categories_;
  }

  public void categories(final SParametresCategories c) {
    // Mutateur de categories
    categories_ = c;
  }

  // DESCRIPTION DU PERTURBATEUR
  public SParametresPerturbateur perturbateur() {
    // Accesseur de perturbateur
    return perturbateur_;
  }

  public void perturbateur(final SParametresPerturbateur p) {
    // Mutateur de perturbateur
    perturbateur_ = p;
  }

  /**
   * @return r�gles de s�curit�.
   */
  public SParametresReglesSecurite reglesSecurite() {
    return reglesSecurite_;
  }

  /**
   * @param arg r�gles de s�curit�.
   */
  public void reglesSecurite(final SParametresReglesSecurite arg) {
    reglesSecurite_ = arg;
  }

  
  
  
  
  /**
   * M�thode utile qui transforme un booleen en "o" ou "n". true => "o" ; false => "n"
   * 
   * @param arg
   * @return "o" : "n"
   */
  public static String enO_N(final boolean arg) {
    final String x = (arg ? "o" : "n");
    return x;
  }

  /**
   * M�thode de transformation des minutes en heures.minutes au format string. 60 => "1.00" ; 61 => "1.01" ; 90 =>
   * "1.30";
   * 
   * @param _minutes
   */
  public static String enHM(final long _minutes) {
    final Long h = new Long(_minutes / 60);
    final Long m = new Long(_minutes % 60);
    final String m_ret = m.longValue() < 10 ? "0" + m.toString() : m.toString();
    return h.toString() + CtuluLibString.DOT + m_ret;
  }

  /**
   * M�thodes d'�criture des donn�es g�n�rales dans le fichier de param�tres.
   * 
   * @param _dg
   * @param _fw
   * @exception IOException
   */
  public static void ecritDonneesGenerales(final SParametresDonneesGenerales _dg, final FortranWriter _fw)
      throws IOException {
    final int[] fmt = { 80, 101 };
    _fw.stringField(0, _dg.titre1);
    _fw.stringField(1, " Titre 1 du projet");
    _fw.writeFields(fmt);
    _fw.stringField(0, _dg.titre2);
    _fw.stringField(1, " Titre 2 du projet");
    _fw.writeFields(fmt);
    _fw.stringField(0, _dg.titre3);
    _fw.stringField(1, " Titre 3 du projet");
    _fw.writeFields(fmt);
    _fw.stringField(0, (new Integer(_dg.nombrePassages)).toString());
    _fw.stringField(1, " Nombre de passages");
    _fw.writeFields(fmt);
    _fw.stringField(0, (new Integer(_dg.graineDepart)).toString());
    _fw.stringField(1, " Graine de d�part");
    _fw.writeFields(fmt);
    _fw.stringField(0, enO_N(_dg.arretProgApresRappelDonnees));
    _fw.stringField(1, " Arret du programme apr�s le rappel des donn�es");
    _fw.writeFields(fmt);
    _fw.stringField(0, enO_N(_dg.impressionRappelDonnees));
    _fw.stringField(1, " Impression du rappel des donn�es");
    _fw.writeFields(fmt);
    _fw.stringField(0, enO_N(_dg.impressionPassagesIntermediaires));
    _fw.stringField(1, " Impression des passages interm�daires");
    _fw.writeFields(fmt);
    _fw.stringField(0, (new Integer(_dg.premierJourSimulation)).toString());
    _fw.stringField(1, " Premier jour de la simulation (0 = dimanche)");
    _fw.writeFields(fmt);
    _fw.stringField(0, enHM(_dg.heureDebutReelSimulation));
    _fw.stringField(1, " Heure de d�but r�el de la simulation");
    _fw.writeFields(fmt);
    _fw.stringField(0, enHM(_dg.heureFinSimulation));
    _fw.stringField(1, " Heure de fin de la simulation");
    _fw.writeFields(fmt);
    _fw.stringField(0, enHM(_dg.ecretageTempsAttente));
    _fw.stringField(1, " Ecretage des temps d'attente");
    _fw.writeFields(fmt);
    _fw.stringField(0, enHM(_dg.arrondiHeuresReglesPriorite));
    _fw.stringField(1, " Arrondi des heures pour les r�gles de priorit�");
    _fw.writeFields(fmt);
  }

  /**
   * M�thodes d'�criture des bassins dans le fichier de param�tres.
   * 
   * @param bassins
   * @param fw
   * @exception IOException
   */
  public static void ecritBassins(final SParametresBassin[] bassins, final FortranWriter fw) throws IOException {
    final int[] fmt = { 80, 101 };
    int i;
    int j;
    int numBassin;
    for (i = 0; i < bassins.length; i++) {
      numBassin = i + 1;
      fw.stringField(0, new Double(bassins[i].profondeurChenal).toString());
      fw.stringField(1, " Profondeur du chenal du bassin " + numBassin);
      fw.writeFields(fmt);
      fw.stringField(0, enHM(bassins[i].tempsChenalage));
      fw.stringField(1, " Temps de chenalage du bassin " + numBassin);
      fw.writeFields(fmt);
      fw.stringField(0, new Double(bassins[i].hauteurEau).toString());
      fw.stringField(1, " Hauteur d'eau dans le bassin " + numBassin);
      fw.writeFields(fmt);
      fw.stringField(0, enHM(bassins[i].dureeManoeuvreEntree));
      fw.stringField(1, " Duree de la manoeuvre d'entr�e dans la bassin " + numBassin);
      fw.writeFields(fmt);
      fw.stringField(0, enHM(bassins[i].dureeManoeuvreSortie));
      fw.stringField(1, " Duree de la manoeuvre de sortie du bassin " + numBassin);
      fw.writeFields(fmt);
      fw.stringField(0, enO_N(bassins[i].ecluse.presenceEcluse));
      fw.stringField(1, " Pr�sence d'une �cluse dans le bassin " + numBassin);
      fw.writeFields(fmt);
      if (bassins[i].ecluse.presenceEcluse) {
        fw.stringField(0, enHM(bassins[i].ecluse.ecluse.dureeEclusee));
        fw.stringField(1, " Duree d'une �clus�e dans le bassin " + numBassin);
        fw.writeFields(fmt);
        fw.stringField(0, enHM(bassins[i].ecluse.ecluse.dureeFausseBassinee));
        fw.stringField(1, " Duree d'une fausse bassin�e dans le bassin " + numBassin);
        fw.writeFields(fmt);
        fw.stringField(0, enHM(bassins[i].ecluse.ecluse.dureePassageEcluseEtale));
        fw.stringField(1, " Duree de passage de l'�cluse du bassin " + numBassin + " � l'�tale");
        fw.writeFields(fmt);
        fw.stringField(0, enHM(bassins[i].ecluse.ecluse.creneauEtaleAvantPleineMer));
        fw.stringField(1, " Cr�neau d'�tale avant la pleine mer dans le bassin " + numBassin);
        fw.writeFields(fmt);
        fw.stringField(0, enHM(bassins[i].ecluse.ecluse.creneauEtaleApresPleineMer));
        fw.stringField(1, " Cr�neau d'�tale apr�s la pleine mer dans le bassin " + numBassin);
        fw.writeFields(fmt);
      }
      fw.stringField(0, enO_N(bassins[i].creneauMaree.presenceCreneauMaree));
      fw.stringField(1, " Pr�sence de cr�neaux de mar�e dans le bassin " + numBassin);
      fw.writeFields(fmt);
      if (bassins[i].creneauMaree.presenceCreneauMaree) {
        fw.stringField(0, enHM(bassins[i].creneauMaree.creneauMaree.avantPleineMer));
        fw.stringField(1, " Cr�neau de mar�e avant la pleine mer dans le bassin " + numBassin);
        fw.writeFields(fmt);
        fw.stringField(0, enHM(bassins[i].creneauMaree.creneauMaree.apresPleineMer));
        fw.stringField(1, " Cr�neau de maree apr�s la pleine mer dans le bassin " + numBassin);
        fw.writeFields(fmt);
      }
      fw.stringField(0, (new Integer(bassins[i].creneauxHoraires.nombreCreneauxHoraires)).toString());
      fw.stringField(1, " Nombre de cr�neaux horaires pour le bassin " + numBassin);
      fw.writeFields(fmt);
      if (bassins[i].creneauxHoraires.nombreCreneauxHoraires > 0) {
        j = 0;
        while ((j < 3) && (bassins[i].creneauxHoraires.creneauxHoraires[j] != null)) {
          final int numCr = j + 1;
          if (bassins[i].creneauxHoraires.creneauxHoraires[j] != null) {
            System.out.println("Cr�neau pas null");
          }
          System.out.println("Creneau " + j);
          fw.stringField(0, enHM(bassins[i].creneauxHoraires.creneauxHoraires[j].heureDebut));
          fw.stringField(1, " Heure de d�but du cr�neau horaire " + numCr + " pour le bassin " + numBassin);
          fw.writeFields(fmt);
          fw.stringField(0, enHM(bassins[i].creneauxHoraires.creneauxHoraires[j].heureFin));
          fw.stringField(1, " Heure de fin du cr�neau horaire " + numCr + " pour le bassin " + numBassin);
          fw.writeFields(fmt);
          j++;
        }
      }
    }
  }

  /**
   * ....
   * 
   * @param q
   * @param f
   * @exception IOException
   */
  public static void ecritQuais(final SParametresQuais q, final FortranWriter f) throws IOException {
    // format du fichier : 80 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
    final int[] fmt = new int[] { 80, 1, 100 };
    // ecriture du nombre de quais
    f.stringField(0, new Integer(q.nombreQuais).toString());
    f.stringField(2, "Nombre de quais");
    f.writeFields(fmt);
    // ecriture de la structure d'un quai
    for (int i = 0; i < q.nombreQuais; i++) {
      // nom du quai
      f.stringField(0, q.quais[i].nomQuai);
      f.stringField(2, "Nom du quai numero " + (i + 1));
      f.writeFields(fmt);
      // Longueur du quai en module
      f.stringField(0, new Integer(q.quais[i].longueurQuai).toString());
      f.stringField(2, "Longueur du quai numero " + (i + 1));
      f.writeFields(fmt);
      // Utilisation du d�halage ?
      f.stringField(0, q.quais[i].dehalageQuai ? "o" : "n");
      f.stringField(2, "D�halage du quai numero " + (i + 1));
      f.writeFields(fmt);
      // Numero du bassin d'appartenance
      f.stringField(0, new Integer(q.quais[i].bassinAppartenance).toString());
      f.stringField(2, "Bassin d'appartenance du quai numero " + (i + 1));
      f.writeFields(fmt);
    }
  }

  /**
   * M�thodes d'�criture de la mar�e dans le fichier de param�tres.
   * 
   * @param maree
   * @param fw
   * @exception IOException
   */
  public static void ecritMaree(final SParametresMaree maree, final FortranWriter fw) throws IOException {
    final int[] fmt = { 80, 101 };
    fw.stringField(0, enHM(maree.periodeVivesEaux));
    fw.stringField(1, " P�riode des vives eaux");
    fw.writeFields(fmt);
    fw.stringField(0, enHM(maree.periodeMaree));
    fw.stringField(1, " P�riode de la mar�e");
    fw.writeFields(fmt);
    fw.stringField(0, new Double(maree.hauteurMoyenneMer).toString());
    fw.stringField(1, " Hauteur moyenne de la mer (en m)");
    fw.writeFields(fmt);
    fw.stringField(0, new Double(maree.moyenneNiveauHauteMerMortesEaux).toString());
    fw.stringField(1, " Moyenne du niveau de la haute mer en p�riode des mortes eaux (en m)");
    fw.writeFields(fmt);
    fw.stringField(0, new Double(maree.moyenneNiveauHauteMerVivesEaux).toString());
    fw.stringField(1, " Moyenne du niveau de la haute mer en p�riode des vives eaux (en m)");
    fw.writeFields(fmt);
    fw.stringField(0, new Double(maree.moyenneNiveauBasseMerMortesEaux).toString());
    fw.stringField(1, " Moyenne du niveau de la basse mer en p�riode des mortes eaux (en m)");
    fw.writeFields(fmt);
    fw.stringField(0, new Double(maree.moyenneNiveauBasseMerVivesEaux).toString());
    fw.stringField(1, " Moyenne du niveau de la basse mer en p�riode des vives eaux (en m)");
    fw.writeFields(fmt);
  }

  /**
   * ....
   * 
   * @param c
   * @param f
   * @exception IOException
   */
  public static void ecritCategories(final SParametresCategories c, final FortranWriter f) throws IOException {
    // format du fichier : 80 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
    final int[] fmt = new int[] { 80, 1, 100 };
    // ecriture du nombre de cat�gories
    f.stringField(0, new Integer(c.nombreCategories).toString());
    f.stringField(2, "Nombre de cat�gories");
    f.writeFields(fmt);
    // ecriture du pied de pilote commum � toutes les cat�gories
    f.stringField(0, new Integer(c.piedPilote).toString());
    f.stringField(2, "Pied de pilote");
    f.writeFields(fmt);
    // ecriture de la structure d'une cat�gorie
    for (int i = 0; i < c.nombreCategories; i++) {
      // nom de la cat�gorie
      f.stringField(0, c.categories[i].nomCategorie);
      f.stringField(2, "Nom de la cat�gorie numero " + (i + 1));
      f.writeFields(fmt);
      // Niveau de priorite
      f.stringField(0, new Integer(c.categories[i].niveauPriorite).toString());
      f.stringField(2, "Niveau de priorite de la categorie numero " + (i + 1));
      f.writeFields(fmt);
      // Longueur de la cat�gorie en modules
      f.stringField(0, new Integer(c.categories[i].longueurCategorie).toString());
      f.stringField(2, "Longueur en metres de la categorie numero " + (i + 1));
      f.writeFields(fmt);
      // Tirant d'eau en entr�e en m.cm
      f.stringField(0, new Double(c.categories[i].tirantEauEntree).toString());
      f.stringField(2, "Tirant d'eau en entree de la categorie " + (i + 1));
      f.writeFields(fmt);
      // Tirant d'eau en sortie en m.cm
      f.stringField(0, new Double(c.categories[i].tirantEauSortie).toString());
      f.stringField(2, "Tirant d'eau en sortie de la categorie " + (i + 1));
      f.writeFields(fmt);
      // Numero de la cat�gorie cr��e en sortie
      f.stringField(0, new Integer(c.categories[i].numeroCategorieSortie).toString());
      f.stringField(2, "Categorie cr��e en sortie de la categorie numero " + (i + 1));
      f.writeFields(fmt);
      // Nombre de quais pr�f�rentiels
      f.stringField(0, new Integer(c.categories[i].nombreQuaisPreferentiels).toString());
      f.stringField(2, "Nombre de quais pr�f�rentiels de la categorie numero " + (i + 1));
      f.writeFields(fmt);
      // �criture des structures des quais pr�f�rentiels
      for (int j = 0; j < c.categories[i].nombreQuaisPreferentiels; j++) {
        // Numero de quai pr�f�rentiel
        f.stringField(0, new Integer(c.categories[i].quaisPreferentiels[j].numeroQuai).toString());
        f.stringField(2, "Num�ro du quai pr�f�rentiel num�ro " + (j + 1) + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Dur�e d'attente admissible au quai pr�f�rentiel
        f.stringField(0, enHM(c.categories[i].quaisPreferentiels[j].dureeAttenteAdmissible));
        f.stringField(2, "Dur�e d'attente admissible du quai pr�f�rentiel num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Dur�e minimale des op�rations du quai pr�f�rentiel
        f.stringField(0, enHM(c.categories[i].quaisPreferentiels[j].dureeMinOperations));
        f.stringField(2, "Dur�e minimale des op�rations du quai pr�f�rentiel num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Dur�e moyenne des op�rations du quai pr�f�rentiel
        f.stringField(0, enHM(c.categories[i].quaisPreferentiels[j].dureeMoyOperations));
        f.stringField(2, "Dur�e moyenne des op�rations du quai pr�f�rentiel num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Dur�e maximale des op�rations du quai pr�f�rentiel
        f.stringField(0, enHM(c.categories[i].quaisPreferentiels[j].dureeMaxOperations));
        f.stringField(2, "Dur�e maximale des op�rations du quai pr�f�rentiel num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Ordre de la loi d'ERLANG du quai pr�f�rentiel
        f.stringField(0, new Integer(c.categories[i].quaisPreferentiels[j].ordreLoiErlang).toString());
        f.stringField(2, "Ordre de la loi d'ERLANG du quai pr�f�rentiel num�ro " + (j + 1) + " de la categorie numero "
            + (i + 1));
        f.writeFields(fmt);
      }
      // Nombre de p�riodes de la cat�gorie
      f.stringField(0, new Integer(c.categories[i].nombrePeriodes).toString());
      f.stringField(2, "Nombre de p�riodes de la cat�gorie numero " + (i + 1));
      f.writeFields(fmt);
      // �criture des structures de p�riodes
      for (int j = 0; j < c.categories[i].nombrePeriodes; j++) {
        // Date de d�but de la p�riode de la categorie
        f.stringField(0, enHM(c.categories[i].periodes[j].dateDebutPeriode));
        f.stringField(2, "Date de d�but de la p�riode num�ro " + (j + 1) + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Date de fin de la p�riode de la categorie
        f.stringField(0, enHM(c.categories[i].periodes[j].dateFinPeriode));
        f.stringField(2, "Date de fin de la p�riode num�ro " + (j + 1) + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Date de premi�re arriv�e de la categorie
        f.stringField(0, enHM(c.categories[i].periodes[j].datePremiereArrivee));
        f.stringField(2, "Date de la premi�re arriv�e de la p�riode num�ro " + (j + 1) + " de la categorie numero "
            + (i + 1));
        f.writeFields(fmt);
        // Ecart minimum entre l'arriv�e de deux categories
        f.stringField(0, enHM(c.categories[i].periodes[j].ecartMinArrivees));
        f.stringField(2, "Ecart minimum entre l'arriv�e de deux categories de la p�riode num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Ecart moyen entre l'arriv�e de deux categories
        f.stringField(0, enHM(c.categories[i].periodes[j].ecartMoyArrivees));
        f.stringField(2, "Ecart moyen entre l'arriv�e de deux categories de la p�riode num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Ecart maximum entre l'arriv�e de deux categories
        f.stringField(0, enHM(c.categories[i].periodes[j].ecartMaxArrivees));
        f.stringField(2, "Ecart maximum entre l'arriv�e de deux categories de la p�riode num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
        // Ordre de la loi d'ERLANG des arriv�es
        f.stringField(0, new Integer(c.categories[i].periodes[j].ordreLoiErlang).toString());
        f.stringField(2, "Ordre de la loi d'ERLANG des arriv�es de la p�riode num�ro " + (j + 1)
            + " de la categorie numero " + (i + 1));
        f.writeFields(fmt);
      }
    }
  }

  /**
   * ....
   * 
   * @param p
   * @param f
   * @exception IOException
   */
  public static void ecritPerturbateur(final SParametresPerturbateur p, final FortranWriter f) throws IOException {
    // format du fichier : 80 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
    final int[] fmt = new int[] { 80, 1, 100 };
    // Date d'arriv�es du premier perturbateur
    f.stringField(0, enHM(p.dateArrivee));
    f.stringField(2, "Date d'arriv�es du premier perturbateur");
    f.writeFields(fmt);
    // ecart moyen entre deux arriv�es de perturbateurs
    f.stringField(0, enHM(p.ecartmoy));
    f.stringField(2, "Ecart moyen entre deux arriv�es");
    f.writeFields(fmt);
    // Ordre de la loi d'ERLANG
    f.stringField(0, new Integer(p.ordreLoiErlang).toString());
    f.stringField(2, "Ordre de la loi d'ERLANG");
    f.writeFields(fmt);
    // temps de s�jour du perturbateur dans le port
    f.stringField(0, enHM(p.sejour));
    f.stringField(2, "Temps de s�jour du perturbateur dans le port");
    f.writeFields(fmt);
  }

  /**
   * M�thodes d'�criture des donn�es g�n�rales dans le fichier de param�tres.
   * 
   * @param rs
   * @param fw
   * @exception IOException
   */
  public static void ecritReglesSecurite(final SParametresReglesSecurite rs, final FortranWriter fw) throws IOException {
    final int[] fmt = { 80, 101 };
    int i;
    final String[] rs_membre2 = { "Entr�e perturbateur", "Sortie perturbateur", "Entr�e bassin 1", "Sortie bassin 1",
        "Entr�e bassin 2", "Sortie bassin 2", "Entr�e bassin 3", "Sortie bassin 3" };
    for (i = 0; i < 8; i++) {
      // Entr�e PE
      fw.stringField(0, enHM(rs.rsPE.entree[i]));
      fw.stringField(1, " Entr�e perturbateur - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsPE.sortie[i]));
      fw.stringField(1, " Sortie perturbateur - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsB1.entree[i]));
      fw.stringField(1, " Entr�e bassin 1 - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsB1.sortie[i]));
      fw.stringField(1, " Sortie bassin 1 - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsB2.entree[i]));
      fw.stringField(1, " Entr�e bassin 2 - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsB2.sortie[i]));
      fw.stringField(1, " Sortie bassin 2 - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsB3.entree[i]));
      fw.stringField(1, " Entr�e bassin 3 - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
    for (i = 0; i < 8; i++) {
      fw.stringField(0, enHM(rs.rsB3.sortie[i]));
      fw.stringField(1, " Sortie bassin 3 - " + rs_membre2[i]);
      fw.writeFields(fmt);
    }
  }

  
  
  
  
  /**
   * Methode d'ecriture des donn�es des cat�gories de navires:
   * @param c
   * @param f
   * @throws IOException
   */
  
  public static void ecritDonneesCategoriesNavires(final SParametresNavires2 c, String nomFichier) throws IOException {
     
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".categ"));
      
    
   
    
    
    // format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
      // ecriture du nombre de cat�gories
      f.stringField(0, new Integer(c.nombreNavires).toString());
      f.stringField(2, "Nombre de cat�gories de navires");
      f.writeFields(fmt);
     
      // ecriture de la structure d'une cat�gorie
      for (int i = 0; i < c.nombreNavires; i++) {
        //    nom de la cat�gorie
        f.stringField(0, c.listeNavires[i].nom);
        f.stringField(2, "Nom de la cat�gorie" + (i + 1));
        f.writeFields(fmt);
          
        //      Niveau de priorite
        f.stringField(0, new Integer(c.listeNavires[i].priorite).toString());
        f.stringField(2, "Niveau de priorite de la categorie" + (i + 1));
        f.writeFields(fmt);
          //       Longueur de la cat�gorie en modules
        f.stringField(0, new Float(c.listeNavires[i].longueurMin).toString());
        f.stringField(2, "Longueur Min en metres de la categorie" + (i + 1));
        f.writeFields(fmt);
          //      Longueur de la cat�gorie en modules
        f.stringField(0, new Float(c.listeNavires[i].longueurMax).toString());
        f.stringField(2, "Longueur Max en metres de la categorie" + (i + 1));
        f.writeFields(fmt);
     
          //       largeur de la cat�gorie en modules
        f.stringField(0, new Float(c.listeNavires[i].largeurMin).toString());
        f.stringField(2, "largeur Min en metres de la categorie" + (i + 1));
        f.writeFields(fmt);
          //      largeur de la cat�gorie en modules
        f.stringField(0, new Float(c.listeNavires[i].largeurMax).toString());
        f.stringField(2, "largeur Max en metres de la categorie" + (i + 1));
        f.writeFields(fmt);
        // Tirant d'eau en entr�e en m.cm
        f.stringField(0, new Double(c.listeNavires[i].tirantEauEntree).toString());
        f.stringField(2, "Tirant d'eau en entree de la categorie" + (i + 1));
        f.writeFields(fmt);
        // Tirant d'eau en sortie en m.cm
        f.stringField(0, new Double(c.listeNavires[i].tirantEauSortie).toString());
        f.stringField(2, "Tirant d'eau en sortie de la categorie" + (i + 1));
        f.writeFields(fmt);
        
        //creneaux ouvertures
        f.stringField(0, new Float(c.listeNavires[i].creneau1PleineMerDeb).toString());
        f.stringField(2, "creneau 1 debut ouverture PM de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].creneau1PleineMerFin).toString());
        f.stringField(2, "creneau 1 fin ouverture PM de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].creneau2PleineMerDeb).toString());
        f.stringField(2, "creneau 2 debut ouverture PM de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].creneau2PleineMerFin).toString());
        f.stringField(2, "creneau 2 fin ouverture PM de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].creneau3PleineMerDeb).toString());
        f.stringField(2, "creneau 3 debut ouverture PM de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].creneau3PleineMerFin).toString());
        f.stringField(2, "creneau 3 fin ouverture PM de la categorie" + (i + 1));
        f.writeFields(fmt);
        
        //creneaux journaliers
        f.stringField(0, new Float(c.listeNavires[i].h.semaineCreneau1HeureDep).toString());
        f.stringField(2, "creneau 1 debut journalier de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].h.semaineCreneau1HeureArrivee).toString());
        f.stringField(2, "creneau 1 fin journalier de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].h.semaineCreneau2HeureDep).toString());
        f.stringField(2, "creneau 2 debut journalier de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].h.semaineCreneau2HeureArrivee).toString());
        f.stringField(2, "creneau 2 fin journalier de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].h.semaineCreneau3HeureDep).toString());
        f.stringField(2, "creneau 3 debut journalier de la categorie" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeNavires[i].h.semaineCreneau3HeureArrivee).toString());
        f.stringField(2, "creneau 3 fin journalier de la categorie" + (i + 1));
        f.writeFields(fmt);
        
        
        
//        gare de depart
        f.stringField(0, new Integer(c.listeNavires[i].GareDepart + 1).toString());
        f.stringField(2, "Gare de depart de la categorie" + (i + 1));
        f.writeFields(fmt);
      
          // quai preferentiel 1
          f.stringField(0, new Integer(c.listeNavires[i].QuaiPreferentiel1 + 1).toString());
          f.stringField(2, " premier quai pr�f�rentiel  de la categorie" + (i + 1));
          f.writeFields(fmt);
          /*F9
          f.stringField(0, new Double(c.listeNavires[i].dureeQuaiPref1).toString());
          f.stringField(2, " dur�e premier quai pr�f�rentiel  de la categorie" + (i + 1));
          f.writeFields(fmt);
          */
            //        quai preferentiel 2
          f.stringField(0, new Integer(c.listeNavires[i].QuaiPreferentiel2 +1).toString());
          f.stringField(2, " deuxieme quai pr�f�rentiel  de la categorie" + (i + 1));
          f.writeFields(fmt);
          /*F9
          f.stringField(0, new Double(c.listeNavires[i].dureeQuaiPref2).toString());
          f.stringField(2, " dur�e deuxieme quai pr�f�rentiel  de la categorie" + (i + 1));
          f.writeFields(fmt);
          */
            //        quai preferentiel 3
          f.stringField(0, new Integer(c.listeNavires[i].QuaiPreferentiel3 + 1).toString());
          f.stringField(2, " troisi�me quai pr�f�rentiel  de la categorie" + (i + 1));
          f.writeFields(fmt);
          /*F9
          f.stringField(0, new Double(c.listeNavires[i].dureeQuaiPref3).toString());
          f.stringField(2, " dur�e troisi�me quai pr�f�rentiel  de la categorie" + (i + 1));
          f.writeFields(fmt);
          */
          //mode de chargement
          f.stringField(0,c.listeNavires[i].typeModeChargement);
          f.stringField(2, "mode de chargement de la categorie" + (i + 1));
          f.writeFields(fmt);
         
          
          
          /**
           * remplissage des donn�es selon type de la loi:
           */
          if(c.listeNavires[i].typeModeChargement.equals("shift"))
          {
            //duree attente avant shift suivant
            f.stringField(0,new Float(c.listeNavires[i].dureeAttenteShiftSuivant).toString());
            f.stringField(2, "duree attente avant le shift suivant de la categorie" + (i + 1));
            f.writeFields(fmt);
            //tonnage minimum
            f.stringField(0,new Float(c.listeNavires[i].tonnageMin).toString());
            f.stringField(2, "tonnage minimum de la categorie" + (i + 1));
            f.writeFields(fmt);
                //          tonnage maximum
            f.stringField(0,new Float(c.listeNavires[i].tonnageMax).toString());
            f.stringField(2, "tonnage maximum de la categorie" + (i + 1));
            f.writeFields(fmt);
                //          cadence quai preferentiels
            f.stringField(0,new Float(c.listeNavires[i].cadenceQuaiPreferentiel).toString());
            f.stringField(2, "cadence au quai preferentiel de la categorie" + (i + 1));
            f.writeFields(fmt);
                //            cadence aux autre quais
            f.stringField(0,new Float(c.listeNavires[i].cadenceAutresQuais).toString());
            f.stringField(2, "cadence aux autres quais de la categorie" + (i + 1));
            f.writeFields(fmt);
            
            //creneaux horaires de travail
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.lundiCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart lundi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.lundiCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee lundi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.lundiCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart lundi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.lundiCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee lundi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.lundiCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart lundi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.lundiCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee lundi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mardiCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart mardi horaire travail suivi de la categorie numero " + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mardiCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee mardi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mardiCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart mardi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mardiCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee mardi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mardiCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart mardi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mardiCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee mardi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mercrediCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart mercredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mercrediCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee mercredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mercrediCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart mercredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mercrediCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee mercredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mercrediCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart mercredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.mercrediCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee mercredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.jeudiCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart jeudi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.jeudiCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee jeudi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.jeudiCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart jeudi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.jeudiCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee jeudi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.jeudiCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart jeudi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.jeudiCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee jeudi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.vendrediCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart vendredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.vendrediCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee vendredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.vendrediCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart vendredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.vendrediCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee vendredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.vendrediCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart vendredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.vendrediCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee vendredi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.samediCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart samedi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.samediCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee samedi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.samediCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart samedi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.samediCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee samedi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.samediCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart samedi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.samediCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee samedi horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.dimancheCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart dimanche horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.dimancheCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee dimanche horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.dimancheCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart dimanche horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.dimancheCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee dimanche horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.dimancheCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart dimanche horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.dimancheCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee dimanche horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.ferieCreneau1HeureDep).toString());
            f.stringField(2, "creneau 1 depart ferie horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.ferieCreneau1HeureArrivee).toString());
            f.stringField(2, "creneau 1 arrivee ferie horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.ferieCreneau2HeureDep).toString());
            f.stringField(2, "creneau 2 depart ferie horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.ferieCreneau2HeureArrivee).toString());
            f.stringField(2, "creneau 2 arrivee ferie horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.ferieCreneau3HeureDep).toString());
            f.stringField(2, "creneau 3 depart ferie horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
            f.stringField(0,new Float(c.listeNavires[i].horairesTravailSuivi.ferieCreneau3HeureArrivee).toString());
            f.stringField(2, "creneau 3 arrivee ferie horaire travail suivi de la categorie" + (i + 1));
            f.writeFields(fmt);
          }//fin mode shift
          else
          {
            //mode loi
            
            // duree service minimum quai preferentiel
            f.stringField(0,new Float(c.listeNavires[i].dureeServiceMinQuaiPref).toString());
            f.stringField(2, "duree de service minimum au quai preferentiel de la categorie" + (i + 1));
            f.writeFields(fmt);
                //          duree service maximum quai preferentiel
            f.stringField(0,new Float(c.listeNavires[i].dureeServiceMaxQuaiPref).toString());
            f.stringField(2, "duree de service maximum au quai preferentiel de la categorie" + (i + 1));
            f.writeFields(fmt);
                
            
            //          duree service minimum quais
            f.stringField(0,new Float(c.listeNavires[i].dureeServiceMinAutresQuais).toString());
            f.stringField(2, "duree de service minimum aux autres quais de la categorie" + (i + 1));
            f.writeFields(fmt);
                //          duree service maximum quais 
            f.stringField(0,new Float(c.listeNavires[i].dureeServiceMaxAutresQuais).toString());
            f.stringField(2, "duree de service maximum aux autres quais de la categorie" + (i + 1));
            f.writeFields(fmt);
                //            loi desduree service autres quais
          }
          
          
//        loi de generation de navires
          if(c.listeNavires[i].typeLoiGenerationNavires==0)
          {
          f.stringField(0, "E");
          f.stringField(2, "Loi de g�n�ration de navires de la categorie" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Integer(c.listeNavires[i].loiErlangGeneration).toString());
          f.stringField(2, "ordre de la loi de generation de la categorie" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Integer(c.listeNavires[i].nbNaviresAttendus).toString());
          f.stringField(2, "nombres de navires attendus de la categorie" + (i + 1));
          f.writeFields(fmt);
          }//fin loi erlang
          else
            if(c.listeNavires[i].typeLoiGenerationNavires==1)
            {//loi deterministe
              f.stringField(0, "D");
                f.stringField(2, "Loi de g�n�ration de navires de la categorie" + (i + 1));
                f.writeFields(fmt);
                f.stringField(0, new Integer(c.listeNavires[i].nombreCouplesDeterministes).toString());
                f.stringField(2, "nombre de couples deterministes de la categorie" + (i + 1));
                f.writeFields(fmt);
              for(int k=0;k<c.listeNavires[i].nombreCouplesDeterministes;k++)
              {
                 f.stringField(0, new Integer(c.listeNavires[i].TableauloiDeterministe[k].jour).toString()+" "+new Float(c.listeNavires[i].TableauloiDeterministe[k].horaire).toString());
                f.stringField(2, "jour "+k+" du tableau de loi deterministe de la categorie" + (i + 1));
                f.writeFields(fmt);
              }
              
              
            }
            else
            {
              //loi journaliere
//              loi deterministe
              f.stringField(0, "J");
                f.stringField(2, "Loi de g�n�ration de navires de la categorie" + (i + 1));
                f.writeFields(fmt);
                
                f.stringField(0, new Integer(c.listeNavires[i].nombreHorairesJournaliers).toString());
                f.stringField(2, "nombre d'horaires journaliers de la categorie" + (i + 1));
                f.writeFields(fmt);
              for(int k=0;k<c.listeNavires[i].nombreHorairesJournaliers;k++)
              {
                f.stringField(0, new Float(c.listeNavires[i].TableauloiJournaliere[k].horaire).toString());
                f.stringField(2, "horaire "+k+" du tableau loi journaliere de la categorie" + (i + 1));
                f.writeFields(fmt);
                
              }
            }
          
          f.stringField(0, new Double(c.listeNavires[i].dureeAttenteMaxAdmissible).toString());
          f.stringField(2, "Duree attente maximale admissible de la categorie " + (i + 1));
          f.writeFields(fmt);
          
        }
      
   
      
      
      //fermeture du fichier de donn�es des cat�gories
      f.flush();
      f.close();
      
  }
  
  /**
   * Methode d'ecriture des donn�es des donn�es g�n�rales:
   * @param c
   * @param f
   * @throws IOException
   */
  
  public static void ecritDonneesGenerales(final SParametresDonneesGenerales2 dg, String nomFichier) throws IOException {
     
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".general"));

//   format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
      // ecriture du nombre de jours de la simulation
      f.stringField(0, new Integer(dg.nombreJours).toString());
      f.stringField(2, "Nombre de jours de la simulation");
      f.writeFields(fmt);
//    ecriture de la graine de la simulation
      f.stringField(0, new Integer(dg.graine).toString());
      f.stringField(2, "graine d'initialisation de la simulation");
      f.writeFields(fmt);
      
      
//    ecriture du nombre du jour de depart de la simulation
      if(dg.jourDepart==1)f.stringField(0, "lundi");
      else
        if(dg.jourDepart==2)f.stringField(0, "mardi");
        else
          if(dg.jourDepart==3)f.stringField(0, "mercredi");
          else
            if(dg.jourDepart==4)f.stringField(0, "jeudi");
            else
              if(dg.jourDepart==5)f.stringField(0, "vendredi");
              else
                if(dg.jourDepart==6)f.stringField(0, "samedi");
                else
                  f.stringField(0, "dimanche");
      f.stringField(2, "jour de depart de la simulation");
      f.writeFields(fmt);
      //pied de pilote
      f.stringField(0, new Integer(dg.piedDePilote).toString());
      f.stringField(2, "pourcentage du pied de pilote de la simulation");
      f.writeFields(fmt);
      
      f.stringField(0, new Integer(dg.nombreJoursFeries).toString());
      f.stringField(2, "Nombre de jours feries de la simulation");
      f.writeFields(fmt);
      for(int i=0;i<dg.nombreJoursFeries;i++)
      {
        f.stringField(0, new Integer(dg.tableauJoursFeries[i]).toString());
        f.stringField(2, "jour ferie num "+i+" de la simulation");
        f.writeFields(fmt);
          
      }
      
    //fermeture du fichier de donn�es g�n�rales
      f.flush();
      f.close();
    }
  
  
  
  public static void ecritDonneesEcluses(final SParametresEcluses2 ec, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".ecl"));
//   format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
   
      
      f.stringField(0, new Integer(ec.nbEcluses).toString());
      f.stringField(2, "Nombre d ecluses de la simulation");
      f.writeFields(fmt);
  
      for(int i=0;i<ec.nbEcluses;i++)
      {
        f.stringField(0,ec.listeEcluses[i].nom);
        f.stringField(2, "nom de l'ecluse"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(ec.listeEcluses[i].longueur).toString());
          f.stringField(2, "longueur de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].largeur).toString());
          f.stringField(2, "largeur de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].dureePassageEtale).toString());
          f.stringField(2, "duree de passage � l'�tale de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].tempsEclusee).toString());
          f.stringField(2, "dur�e d'�clus�e de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].tempsFausseBassinnee).toString());
          f.stringField(2, "dur�e de fausse bassin�e de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].creneauEtaleAvantPleineMerDeb).toString());
          f.stringField(2, "creneau etale avant pleine mer de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].creneauEtaleApresPleineMerFin).toString());
          f.stringField(2, "creneau etale apres pleine mer de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Integer(ec.listeEcluses[i].gareAmont+1).toString());
        f.stringField(2, "gare amont de l'ecluse" + (i + 1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(ec.listeEcluses[i].gareAval+1).toString());
        f.stringField(2, "gare aval de l'ecluse" + (i + 1));
        f.writeFields(fmt);
          
        if(ec.listeEcluses[i].typeLoi==0)
        {
          f.stringField(0, "E");
          f.stringField(2, "loi des indisponibilites de l'ecluse"+(i+1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].frequenceMoyenne).toString());
          f.stringField(2, "ecart moyen de l'ecluse"+(i+1));
          f.writeFields(fmt);
          f.stringField(0, new Integer(ec.listeEcluses[i].loiFrequence).toString());
          f.stringField(2, "loi d'erlang de la frequence de l'ecluse"+(i+1));
          f.writeFields(fmt);
                
        }//fin loi erlang
        else
          if(ec.listeEcluses[i].typeLoi==1)
          {
            //loi deterministe:
            f.stringField(0, "D");
            f.stringField(2, "loi des indisponibilites de l'ecluse"+(i+1));
            f.writeFields(fmt);
            f.stringField(0, new Integer(ec.listeEcluses[i].nombreCouplesDeterministes).toString());
            f.stringField(2, "nombre de couples deterministes de l'ecluse"+(i+1));
            f.writeFields(fmt);
            for(int k=0;k<ec.listeEcluses[i].nombreCouplesDeterministes;k++)
            {
              f.stringField(0, new Integer(ec.listeEcluses[i].TableauloiDeterministe[k].jour).toString()+" "+new Integer((int)ec.listeEcluses[i].TableauloiDeterministe[k].horaire).toString());
              f.stringField(2, " jour et horaire "+k+" de la loi deterministe de l'ecluse"+(i+1));
              f.writeFields(fmt);
            
            }
          }//fin loi deterministe
          else
          {
//            loi journaliere:
            f.stringField(0, "J");
            f.stringField(2, "loi des indisponibilites de l'ecluse"+(i+1));
            f.writeFields(fmt);
            f.stringField(0, new Integer(ec.listeEcluses[i].nombreHorairesJournaliers).toString());
            f.stringField(2, "nombre d'horaires journaliers de l'ecluse"+(i+1));
            f.writeFields(fmt);
            for(int k=0;k<ec.listeEcluses[i].nombreHorairesJournaliers;k++)
            {
              f.stringField(0, new Float(ec.listeEcluses[i].TableauloiJournaliere[k].horaire).toString());
              f.stringField(2, "horaire "+k+" de la loi journaliere de l'ecluse"+(i+1));
              f.writeFields(fmt);
              
            }
          }//fin loi journaliere
        f.stringField(0, new Float(ec.listeEcluses[i].dureeIndispo).toString());
        f.stringField(2, "dur�e moyenne d'indisponibilit� de l'ecluse"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(ec.listeEcluses[i].loiIndispo).toString());
        f.stringField(2, "loi erlang de la dur�e indispo de l'ecluse"+(i+1));
        f.writeFields(fmt);
        
          //crenaux horaires de l ecluse
          f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau1HeureDep).toString());
          f.stringField(2, "creneau 1 debut de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau1HeureArrivee).toString());
          f.stringField(2, "creneau 1 arrivee de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau2HeureDep).toString());
          f.stringField(2, "creneau 2 debut de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau2HeureArrivee).toString());
          f.stringField(2, "creneau 2 arrivee de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau3HeureDep).toString());
          f.stringField(2, "creneau 3 debut de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau3HeureArrivee).toString());
          f.stringField(2, "creneau 3 arrivee de l'ecluse" + (i + 1));
          f.writeFields(fmt);
          
          
        
      }
      
      //fermeture du fichier de donn�es des ecluses
      f.flush();
      f.close();
    }
  
  public static void ecritDonneesBassins(final SParametresBassins2 b, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".bassin"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
   
      f.stringField(0, new Integer(b.nbBassins).toString());
      f.stringField(2, "Nombre de bassins de la simulation");
      f.writeFields(fmt);
  
      for(int i=0;i<b.nbBassins;i++)
      {
        f.stringField(0, b.listeBassins[i].nom);
        f.stringField(2, "nom du bassin"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(b.listeBassins[i].gareAmont+1).toString());
        f.stringField(2, "gare amont du bassin"+(i+1));
        f.writeFields(fmt);
        
          
      }
//fermeture du fichier de donn�es des bassins
      f.flush();
      f.close();
  }
  
  public static void ecritDonneesGares(final SParametresGares g, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".gare"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
   
      f.stringField(0, new Integer(g.nbGares).toString());
      f.stringField(2, "Nombre de gares de la simulation");
      f.writeFields(fmt);
  
      for(int i=0;i<g.nbGares;i++)
      {
        f.stringField(0, g.listeGares[i].nom);
        f.stringField(2, "nom de la gare"+(i+1));
        f.writeFields(fmt);
        
        
          
      }
//fermeture du fichier de donn�es des gares
      f.flush();
      f.close();
  }
  
  
  public static void ecritDonneesMarees(final SParametresMaree2 m, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".maree"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
  
      f.stringField(0, new Float(m.periodeViveEaux).toString());
      f.stringField(2, "periode de vive eaux");
      f.writeFields(fmt);
      f.stringField(0, new Float(m.periodeMaree).toString());
      f.stringField(2, "periode de la maree");
      f.writeFields(fmt);
      f.stringField(0, new Float(m.hauteurMoyenneMer).toString());
      f.stringField(2, "hauteur moyenne de la mer");
      f.writeFields(fmt);
      f.stringField(0, new Float(m.niveauBasseMerMorteEau).toString());
      f.stringField(2, "basse mer periode morte eau");
      f.writeFields(fmt);
      f.stringField(0, new Float(m.niveauBasseMerViveEau).toString());
      f.stringField(2, "basse mer periode vive eau");
      f.writeFields(fmt);
      f.stringField(0, new Float(m.niveauHauteMerMorteEau).toString());
      f.stringField(2, "haute mer periode morte eau");
      f.writeFields(fmt);
      f.stringField(0, new Float(m.niveauHauteMerViveEau).toString());
      f.stringField(2, "haute mer periode vive eau");
      f.writeFields(fmt);
      //-- Recopie tableau des intervalles--//
      for(int i=0;i<m.tableauDouzaines.length;i++){
          f.stringField(0, new Float(m.tableauDouzaines[i]).toString());
          f.stringField(2, "Nb douziemes pour l'intervalle "+(i+1));
          f.writeFields(fmt);
      }
//    fermeture du fichier de donn�es des marees
      f.flush();
      f.close();
  }
  
    
  public static void ecritDonneesQuais(final SParametresQuais2 q, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".quai"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };

      f.stringField(0, new Integer(q.nbQuais).toString());
      f.stringField(2, "nombres de quais de la simulation");
      f.writeFields(fmt);
  
      for(int i=0;i<q.nbQuais;i++)
      {
        //ecriture des quais
        f.stringField(0, q.listeQuais[i].nom);
        f.stringField(2, "nom du quai"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(q.listeQuais[i].Bassin+1).toString());
        f.stringField(2, "bassin d'appartenance du quai"+(i+1));
        f.writeFields(fmt);
        
        
        f.stringField(0, new Float(q.listeQuais[i].longueur).toString());
        f.stringField(2, "longueur du quai"+(i+1));
        f.writeFields(fmt);
        if(q.listeQuais[i].dehalageAutorise)
        f.stringField(0, "oui");
        else f.stringField(0, "non");
        f.stringField(2, "dehalage autorise du quai"+(i+1));
        f.writeFields(fmt);
        if(q.listeQuais[i].typeLoi==0)
        {
          f.stringField(0, "E");
          f.stringField(2, "loi des indisponibilites du quai"+(i+1));
          f.writeFields(fmt);
          f.stringField(0, new Float(q.listeQuais[i].frequenceMoyenne).toString());
          f.stringField(2, "ecart moyen du quai"+(i+1));
          f.writeFields(fmt);
          f.stringField(0, new Integer(q.listeQuais[i].loiFrequence).toString());
          f.stringField(2, "loi d'erlang de la frequence du quai"+(i+1));
          f.writeFields(fmt);
                
        }//fin loi erlang
        else
          if(q.listeQuais[i].typeLoi==1)
          {
            //loi deterministe:
            f.stringField(0, "D");
            f.stringField(2, "loi des indisponibilites du quai"+(i+1));
            f.writeFields(fmt);
            f.stringField(0, new Integer(q.listeQuais[i].nombreCouplesDeterministes).toString());
            f.stringField(2, "nombre de couples deterministes du quai"+(i+1));
            f.writeFields(fmt);
            for(int k=0;k<q.listeQuais[i].nombreCouplesDeterministes;k++)
            {
              f.stringField(0, new Integer(q.listeQuais[i].TableauloiDeterministe[k].jour).toString()+" "+ new Integer((int)q.listeQuais[i].TableauloiDeterministe[k].horaire).toString());
              f.stringField(2, "jour et horaire "+k+" de la loi deterministe du quai"+(i+1));
              f.writeFields(fmt);
            }
          }//fin loi deterministe
          else
          {
//            loi journaliere:
            f.stringField(0, "J");
            f.stringField(2, "loi des indisponibilites du quai"+(i+1));
            f.writeFields(fmt);
            f.stringField(0, new Integer(q.listeQuais[i].nombreHorairesJournaliers).toString());
            f.stringField(2, "nombre d'horaires journaliers du quai"+(i+1));
            f.writeFields(fmt);
            for(int k=0;k<q.listeQuais[i].nombreHorairesJournaliers;k++)
            {
              f.stringField(0, new Float(q.listeQuais[i].TableauloiJournaliere[k].horaire).toString());
              f.stringField(2, "horaire "+k+" de la loi journaliere du quai"+(i+1));
              f.writeFields(fmt);
              
            }
          }//fin loi journaliere
        f.stringField(0, new Float(q.listeQuais[i].dureeIndispo).toString());
        f.stringField(2, "dur�e moyenne d'indisponibilit� du quai"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(q.listeQuais[i].loiIndispo).toString());
        f.stringField(2, "loi erlang de la dur�e indispo du quai"+(i+1));
        f.writeFields(fmt);
        
        
         //crenaux horaires du quai
          f.stringField(0, new Float(q.listeQuais[i].h.semaineCreneau1HeureDep).toString());
          f.stringField(2, "creneau 1 debut du quai" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(q.listeQuais[i].h.semaineCreneau1HeureArrivee).toString());
          f.stringField(2, "creneau 1 arrivee du quai" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(q.listeQuais[i].h.semaineCreneau2HeureDep).toString());
          f.stringField(2, "creneau 2 debut du quai" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(q.listeQuais[i].h.semaineCreneau2HeureArrivee).toString());
          f.stringField(2, "creneau 2 arrivee du quai" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(q.listeQuais[i].h.semaineCreneau3HeureDep).toString());
          f.stringField(2, "creneau 3 debut du quai" + (i + 1));
          f.writeFields(fmt);
          f.stringField(0, new Float(q.listeQuais[i].h.semaineCreneau3HeureArrivee).toString());
          f.stringField(2, "creneau 3 arrivee du quai" + (i + 1));
          f.writeFields(fmt);
        
        
      }
  
      f.flush();
      f.close();
  }
  
  
  public static void ecritDonneesCercles(final SParametresCerclesGenes c, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".cercle"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };

      f.stringField(0, new Integer(c.nbCercles).toString());
      f.stringField(2, "nombres de cercles de la simulation");
      f.writeFields(fmt);
  
      for(int i=0;i<c.nbCercles;i++)
      {
          f.stringField(0, c.listeCercles[i].nom);
        f.stringField(2, "nom du cercle "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(c.listeCercles[i].gareAmont+1).toString());
        f.stringField(2, "gare amont du cercle "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(c.listeCercles[i].gareAval+1).toString());
        f.stringField(2, "gare aval du cercle "+(i+1));
        f.writeFields(fmt);
        
      }
      
      f.flush();
      f.close();
  }
  
   
  public static void ecritDonneesChenal(final SParametresCheneaux2 c, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".chenal"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };

      f.stringField(0, new Integer(c.nbCheneaux).toString());
      f.stringField(2, "nombres de chenaux de la simulation");
      f.writeFields(fmt);
  
      for(int i=0;i<c.nbCheneaux;i++)
      {
          f.stringField(0, c.listeCheneaux[i].nom);
        f.stringField(2, "nom du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeCheneaux[i].profondeur).toString());
        f.stringField(2, "profondeur du chenal "+(i+1));
        f.writeFields(fmt);
        if(c.listeCheneaux[i].soumisMaree)
        f.stringField(0, "oui");
        else f.stringField(0, "non");
        f.stringField(2, "soumissions � la mar�e du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(c.listeCheneaux[i].gareAmont+1).toString());
        f.stringField(2, "gare amont du chenal"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(c.listeCheneaux[i].gareAval+1).toString());
        f.stringField(2, "gare aval du chenal"+(i+1));
        f.writeFields(fmt);
        
        if(c.listeCheneaux[i].typeLoi==0)
        {
          f.stringField(0, "E");
          f.stringField(2, "loi des indisponibilites du chenal"+(i+1));
          f.writeFields(fmt);
          f.stringField(0, new Float(c.listeCheneaux[i].frequenceMoyenne).toString());
          f.stringField(2, "ecart moyen du chenal"+(i+1));
          f.writeFields(fmt);
          f.stringField(0, new Integer(c.listeCheneaux[i].loiFrequence).toString());
          f.stringField(2, "loi d'erlang de la frequence du chenal"+(i+1));
          f.writeFields(fmt);
                
        }//fin loi erlang
        else
          if(c.listeCheneaux[i].typeLoi==1)
          {
            //loi deterministe:
            f.stringField(0, "D");
            f.stringField(2, "loi des indisponibilites du chenal"+(i+1));
            f.writeFields(fmt);
            f.stringField(0, new Integer(c.listeCheneaux[i].nombreCouplesDeterministes).toString());
            f.stringField(2, "nombre de couples deterministes du chenal"+(i+1));
            f.writeFields(fmt);
            for(int k=0;k<c.listeCheneaux[i].nombreCouplesDeterministes;k++)
            {
              f.stringField(0, new Integer(c.listeCheneaux[i].TableauloiDeterministe[k].jour).toString()+" "+new Integer((int)c.listeCheneaux[i].TableauloiDeterministe[k].horaire).toString());
              f.stringField(2, " jour et horaire "+k+" de la loi deterministe du chenal"+(i+1));
              f.writeFields(fmt);
            }
          }//fin loi deterministe
          else
          {
            //            loi journaliere:
            f.stringField(0, "J");
            f.stringField(2, "loi des indisponibilites du chenal"+(i+1));
            f.writeFields(fmt);
            f.stringField(0, new Integer(c.listeCheneaux[i].nombreHorairesJournaliers).toString());
            f.stringField(2, "nombre d'horaires journaliers du chenal"+(i+1));
            f.writeFields(fmt);
            for(int k=0;k<c.listeCheneaux[i].nombreHorairesJournaliers;k++)
            {
              f.stringField(0, new Float(c.listeCheneaux[i].TableauloiJournaliere[k].horaire).toString());
              f.stringField(2, "horaire "+k+" de la loi journaliere du chenal"+(i+1));
              f.writeFields(fmt);
              
            }
          }//fin loi journaliere
        f.stringField(0, new Float(c.listeCheneaux[i].dureeIndispo).toString());
        f.stringField(2, "dur�e moyenne d'indisponibilit� du chenal"+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Integer(c.listeCheneaux[i].loiIndispo).toString());
        f.stringField(2, "loi erlang de la dur�e indispo du chenal"+(i+1));
        f.writeFields(fmt);
        
        //-- creneaux du chenal --//
        f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau1HeureDep).toString());
        f.stringField(2, "creneau 1 depart du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau1HeureArrivee).toString());
        f.stringField(2, "creneau 1 arrivee du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau2HeureDep).toString());
        f.stringField(2, "creneau 2 depart du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau2HeureArrivee).toString());
        f.stringField(2, "creneau 2 arrivee du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau3HeureDep).toString());
        f.stringField(2, "creneau 3 depart du chenal "+(i+1));
        f.writeFields(fmt);
        f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau3HeureArrivee).toString());
        f.stringField(2, "creneau 3 arrivee du chenal "+(i+1));
        f.writeFields(fmt);
        
      }
      
      f.flush();
      f.close();
  }
   
  
  
  
  public static void ecritDonneesDureeParcoursChenal(final SParametresSiporGene s, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".durparche"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };
      
      for(int i=0;i<s.cheneaux.nbCheneaux;i++)
        for(int j=0;j<s.navires.nombreNavires;j++)
        {

            f.stringField(0, new Float(s.matriceDureeParcoursChenaux[i][j]).toString());
            f.stringField(2, "categorie "+(j+1)+" dans chenal "+(i+1)+"");
            f.writeFields(fmt);
          
          
        }
      
      f.flush();
      f.close();
  }
  
  public static void ecritDonneesRemplissageSAS(final SParametresSiporGene s, String nomFichier) throws IOException {
	    //creation du fichier des cat�gories des navires:
	    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".rempliSAS"));
	//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	      final int[] fmt = new int[] { 40, 1, 100 };
	      
	      for(int i=0;i<s.ecluses.nbEcluses;i++)
	        for(int j=0;j<s.navires.nombreNavires;j++)
	        {

	            f.stringField(0, new Float(s.matriceRemplissageSAS[i][j]).toString());
	            f.stringField(2, "categorie "+(j+1)+" dans SAS ecluse "+(i+1)+"");
	            f.writeFields(fmt);
	          
	          
	        }
	      
	      f.flush();
	      f.close();
	  }
  
  
  
  private static NumberFormat getHHMMFormatter(){
    DecimalFormat format=CtuluLib.getDecimalFormat();
    format.setMaximumFractionDigits(2);
    format.setMinimumFractionDigits(2);
    return format;
  }
  
  
  public static void ecritDonneesDureeParcoursCercle(final SParametresSiporGene s, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".durparcer"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };

      for(int i=0;i<s.cercles.nbCercles;i++)
        for(int j=0;j<s.navires.nombreNavires;j++)
        {

            f.stringField(0, new Float(s.matriceDureeParcoursCercles[i][j]).toString());
            f.stringField(2, "categorie "+(j+1)+" dans cercle "+(i+1)+"");
            f.writeFields(fmt);
          
          
        }
      
      f.flush();
      f.close();
  }
  
  public static void ecritDonneesGenesCercle(final SParametresSiporGene s, String nomFichier) throws IOException {
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".genecer"));
    
    ecritDonneesGenesCercle(s, f);
  }
  
  public static void ecritDonneesGenesCercle(final SParametresSiporGene s, final FortranWriter f) throws IOException {
    //creation du fichier des cat�gories des navires:
   
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };


      NumberFormat numberFmt=getHHMMFormatter();
      for(int i=0;i<s.cercles.nbCercles;i++)
        for(int j=0;j<s.navires.nombreNavires;j++)
          for(int k=0;k<s.navires.nombreNavires;k++)
        {

            f.stringField(0, numberFmt.format(s.cercles.listeCercles[i].matriceGenesNavigation[j][k]));
            f.stringField(2, "categorie "+(j+1)+" et categorie "+(k+1)+" dans cercle "+(i+1)+"");
            f.writeFields(fmt);
          
          
        }
      
      f.flush();
      f.close();
  }
  public static void ecritDonneesCroisementsChenal(final SParametresCheneaux2 c, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".croische"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };

  
      for(int i=0;i<c.nbCheneaux;i++)
      {
            for(int k=0;k<c.listeCheneaux[i].dimensionMatriceReglesNav;k++)
              for(int l=0;l<c.listeCheneaux[i].dimensionMatriceReglesNav;l++)
              {
                if(c.listeCheneaux[i].matriceReglesNavigation[k][l])
                f.stringField(0, "oui");
                else
                  f.stringField(0, "non");
                f.stringField(2, "categorie "+(k+1)+" et categorie "+(l+1)+" dans chenal "+(i+1));
                f.writeFields(fmt);
              }
        
  
      }
      
  f.flush();
  f.close();
  }
  
  
  
  
  public static void ecritDonneesCroisementsCercle(final SParametresCerclesGenes c, String nomFichier) throws IOException {
    //creation du fichier des cat�gories des navires:
    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".croiscer"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 40, 1, 100 };

  
      for(int i=0;i<c.nbCercles;i++)
      {
        
              
            for(int k=0;k<c.listeCercles[i].dimensionMatriceReglesNav;k++)
              for(int l=0;l<c.listeCercles[i].dimensionMatriceReglesNav;l++)
              {
                if(c.listeCercles[i].matriceReglesNavigation[k][l])
                f.stringField(0, "oui");
                else
                  f.stringField(0, "non");
                f.stringField(2, "categorie "+(k+1)+" et categorie "+(l+1)+" dans cercle "+(i+1));
                f.writeFields(fmt);
              }
        
  
      }
      
  f.flush();
  f.close();
  }
  
  
  
  /**
   * M�thode d'�criture des fichiers de param�tres de sipor.
   * @param nomFichier
   * @param params
   */
  public static void ecritParametresSipor(final String nomFichier, final SParametresSiporGene params_) {

    try {
      // cat�gories
      DParametresSipor.ecritDonneesCategoriesNavires(params_.navires, nomFichier);
      // donn�es generales
      DParametresSipor
      .ecritDonneesGenerales(params_.donneesGenerales, nomFichier);
      // ecluses
      DParametresSipor.ecritDonneesEcluses(params_.ecluses, nomFichier);
      // bassins
      DParametresSipor.ecritDonneesBassins(params_.bassins, nomFichier);
      // gares
      DParametresSipor.ecritDonneesGares(params_.gares, nomFichier);
      // mar�es
      DParametresSipor.ecritDonneesMarees(params_.maree, nomFichier);
      // quais
      DParametresSipor.ecritDonneesQuais(params_.quais, nomFichier);
      // cercles
      DParametresSipor.ecritDonneesCercles(params_.cercles, nomFichier);
      // chenal
      DParametresSipor.ecritDonneesChenal(params_.cheneaux, nomFichier);

      // durees de parcours des chenaux
      DParametresSipor.ecritDonneesDureeParcoursChenal(params_, nomFichier);

      // durees de parcours des cercles
      DParametresSipor.ecritDonneesDureeParcoursCercle(params_, nomFichier);

      // dur�es de croisement cercle
      DParametresSipor.ecritDonneesCroisementsCercle(params_.cercles, nomFichier);
      
      // d�lais remplissage SAS ecluse.
      DParametresSipor.ecritDonneesRemplissageSAS(params_, nomFichier);
      
      
      //-- ahx - genes cercles --//
      DParametresSipor.ecritDonneesGenesCercle(params_, nomFichier);

      // dur�es de croisement des chenaux:
      DParametresSipor
      .ecritDonneesCroisementsChenal(params_.cheneaux, nomFichier);
    } catch (final Exception ex) {

    }
  }

public SParametresSipor2 parametresSipor() {
  // TODO Auto-generated method stub
  return parametresSipor_;
}

public void parametresSipor(SParametresSipor2 newParametresSipor) {
  parametresSipor_=newParametresSipor;
  
}
}
